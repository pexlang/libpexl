/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  kjv-test.c                                                               */
/*                                                                           */
/*  © Copyright Jamie A. Jennings, 2023.                                     */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */
/*  AUTHOR: Luke G. Jenquin                                                  */

/* Compile with DEBUG defined to enable logging. 

   Usage example:

   ./kjv-test run captureAll longword 1 15 ~/Projects/comparisons/data/kjv10.txt
*/

#define INPUTFILENAME "data/kjv10.txt"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/param.h>		/* MAXPATHLEN */

#include "libpexl.h"

#define TRUE 1
#define FALSE 0

#define PRINTING FALSE

#define OK 0
#define ERR_COMPILE 250
#define ERR_MATCH   249
#define ERR_USAGE   248
#define ERR_NOARGS  247
#define ERR_OOM     246
#define ERR_SETUP   245

/* 
   This program supports 6 "actions" which are ways of using the named
   pattern:

   search - stop after the first match, returning the match end position
   capture - stop after the first match, returning match start and end
   find - here it is the same as capture, but uses IFind instruction
   
   Each action has an 'All' variant that goes through the whole input.

*/
enum ACTION_CODES {
  ACTION_SEARCH,
  ACTION_SEARCH_ALL,
  ACTION_CAPTURE,
  ACTION_CAPTURE_ALL,
  ACTION_FIND,
  ACTION_FIND_ALL,
  ACTION_NUM_CODES		/* sentinel */
};

static const char *ACTION_NAMES[6] = 
  {"search", "searchAll", "capture", "captureAll", "find", "findAll"};

/*
  The noop command measures time needed to start up (load dynamic
  library, bind symbols, process CLI args, and read input file).

  The setup command does all of the above, compiles the pattern, and
  prints its bytecode for inspection.

  The run command does all of the above, except for printing bytecode,
  and runs the pattern on the input.
*/

enum COMMAND_CODES {
  COMMAND_NOOP,
  COMMAND_SETUP,
  COMMAND_RUN,
  COMMAND_NUM_CODES		/* sentinel */
};

static const char *COMMAND_NAMES[3] = {"noop", "setup", "run"};

/*
  To make a cheap and easy way to specify optimizations on the command
  line, we force the user to specify an integer, which we interpret as
  a bit pattern.
*/
enum optimizers {
  PEEPHOLE_BIT = 1,
  UNROLLING_BIT,
  INLINING_BIT,
  TRO_BIT,
  SIMD_BIT
};

// Exit gracefully from main()
#define EXIT(code) do {				\
    fflush(NULL);				\
    exitStatus = code;				\
    goto quit;					\
  } while (0)

// Bail out from anywhere
#define ERROR(msg) {				\
    fprintf(stderr, msg);			\
    fputc('\n', stderr);			\
    exit(1);					\
  } while (0)

__attribute__((unused))
static pexl_Expr *make_search_capture_old (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *findexp;
  // Below, we construct P = (!exp .)* exp
  findexp = 
    pexl_seq_f(C,
     pexl_repeat_f(C,
      pexl_seq_f(C,
       pexl_neg_lookahead(C, exp),
       pexl_match_any(C, 1)),
      0, 0),
     pexl_capture(C, "anon", exp));
  return findexp;
}

// Construct P = exp / (any P), like kjv-lpeg.lua does.
static pexl_Ref make_search (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *findexp;
  pexl_Ref P = pexl_bind(C, NULL, "exp");
  if (pexl_Ref_invalid(P)) ERROR("failed to get a ref");
  findexp = 
    pexl_choice_f(C,
		  exp,
		  pexl_seq_f(C,
			     pexl_match_any(C, 1),
			     pexl_call(C, P)));
  if (pexl_rebind(C, P, findexp)) ERROR("rebind failed");
  return P;
}
//adaptation of search using the new pexl_find search command for increased performace.
static pexl_Ref make_search_find (pexl_Context *C, pexl_Expr *exp) {
  pexl_Ref P = pexl_bind(C, NULL, "exp");
  if (pexl_Ref_invalid(P)) ERROR("failed to get a ref");
  pexl_Expr *find_exp = pexl_find(C, exp);
  if (!find_exp) ERROR("failed to compile a 'find' exp!");
  if (pexl_rebind(C, P, find_exp)) ERROR("rebind failed");
  return P;
}

//This wraps the find in a capture for result verification
static pexl_Ref make_find_capture (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *capture_exp = pexl_capture(C, "capture_exp", exp);
  pexl_Ref retVal = make_search_find(C, capture_exp);
  pexl_free_Expr(exp);
  return retVal;
}
//uses the same structure as search_all by repeating the find 1 or more times.
static pexl_Ref make_find_capture_all (pexl_Context *C, pexl_Expr *exp) {
  pexl_Ref P = pexl_bind(C, NULL, "findAll");
  if (pexl_Ref_invalid(P)) ERROR("failed to get a ref");
  pexl_Ref find = make_find_capture(C, exp);
  pexl_Expr *new_exp = pexl_repeat_f(C, pexl_call(C, find), 1, 0);
  if (pexl_rebind(C, P, new_exp)) ERROR("rebind failed");
  return P;
}
// Construct P = exp / (any P), like kjv-lpeg.lua does.
static pexl_Ref make_search_capture (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *capture_exp = pexl_capture(C, "capture_exp", exp);
  pexl_Ref retVal = make_search(C, capture_exp);
  pexl_free_Expr(exp);
  return retVal;
}
//repeats the search command one or more times so the VM continues through the input to the end
static pexl_Ref make_search_all (pexl_Context *C, pexl_Expr *exp) {
  pexl_Ref P = pexl_bind(C, NULL, "searchAll");
  if (pexl_Ref_invalid(P)) ERROR("failed to get a ref");
  pexl_Ref search = make_search(C, exp);
  pexl_Expr *new_exp = pexl_repeat_f(C, pexl_call(C, search), 1, 0);
  if (pexl_rebind(C, P, new_exp)) ERROR("rebind failed");
  return P;
}
//similar to search all continues to the end of the input matching along the way
static pexl_Ref make_search_capture_all (pexl_Context *C, pexl_Expr *exp) {
  pexl_Ref P = pexl_bind(C, NULL, "captureAll");
  if (pexl_Ref_invalid(P)) ERROR("failed to get a ref");
  pexl_Ref capture = make_search_capture(C, exp);
  pexl_Expr *new_exp = pexl_repeat_f(C, pexl_call(C, capture), 1, 0);
  if (pexl_rebind(C, P, new_exp)) ERROR("rebind failed");
  return P;
}
//searches through the manually created patterns below with the
//inputted name to construct and return a pexl_Context
static pexl_Binary *compile_by_name (pexl_Context *C,
				const char *name, int action,
				pexl_Optims *optims) {
  pexl_Ref ref;
  pexl_Error err;
  pexl_Binary *pkg;
  pexl_Expr *exp;
  if (strcmp(name, "abram")==0) {
    // find:{[[a-z][A-Z]]+ " "* "Abram"}
    exp =
      pexl_seq_f(C, 
	 pexl_repeat_f(C,
            pexl_choice_f(C,
               pexl_match_range(C, 'a', 'z'),
	       pexl_match_range(C, 'A', 'Z')),
	    1, 0),
	 pexl_seq_f(C, 
	    pexl_repeat_f(C,
	       pexl_match_string(C, " "),
	       0, 0),
	    pexl_match_string(C, "Abram")));
  }
  else if (strcmp(name, "longword")==0) {
    // find:[[a-z][A-Z]]{14,}
    exp =
      pexl_repeat_f(C,
         pexl_choice_f(C,
            pexl_match_range(C, 'a', 'z'),
	    pexl_match_range(C, 'A', 'Z')),
	 14, 0);
  } 
  else if (strcmp(name, "smallword")==0) {
    // find:[[a-z][A-Z]]{5,}
    exp =
      pexl_repeat_f(C,
	 pexl_choice_f(C,
	    pexl_match_range(C, 'a', 'z'),
	    pexl_match_range(C, 'A', 'Z')),
	 5, 0);
  } 

  else if (strcmp(name, "omega")==0) {
    // find:"Omega"
    exp = pexl_match_string(C, "Omega");    
  }
  else if (strcmp(name, "the")==0) {
    // find:"@the"
    exp = pexl_match_string(C, "@the");    
  } else if (strcmp(name, "tubalcain")==0) {
    // find:"Tubalcain"
    exp = pexl_match_string(C, "Tubalcain");    
  }
  else if (strcmp(name, "psalm")==0) {
    // find:{[[1-9][:]]{0,5} "7" "6""+}
    exp =
      pexl_seq_f(C,
	 pexl_repeat_f(C,
            pexl_choice_f(C,
               pexl_match_range(C, '1', '9'),
	       pexl_match_string(C, ":")),
	    0, 5),
	 pexl_seq_f(C,
	    pexl_match_string(C, "7"),
	    pexl_repeat_f(C,
	       pexl_match_string(C, "6"),
	       1, 0)));
  }
  else if (strcmp(name, "adamant")==0) {
    // find:{"adam" [[a-z][A-Z]]*}
    exp =
      pexl_seq_f(C,
	 pexl_match_string(C, "adam"),
	 pexl_repeat_f(C,
            pexl_choice_f(C,
	       pexl_match_range(C, 'a', 'z'),
	       pexl_match_range(C, 'A', 'Z')),
	    0, 0));
  }
  else {
    exp = NULL;
  }
  if (!exp) {
    printf("failed to create expression\n");
    return NULL;
  }
  //depending on the command line input, create a reference to the action we will use
  switch (action) {
  case ACTION_CAPTURE:
    ref = make_search_capture(C, exp);
    break;
  case ACTION_CAPTURE_ALL:
    ref = make_search_capture_all(C, exp);
    break;
  case ACTION_FIND:
    ref = make_find_capture(C, exp);
    break;
  case ACTION_FIND_ALL:
    ref = make_find_capture_all(C, exp);
    break;
  case ACTION_SEARCH:
    ref = make_search(C, exp);
    break;
  case ACTION_SEARCH_ALL:
    ref = make_search_all(C, exp);
    break;
  default:
    printf("invalid action code!\n");
    return NULL;
  }
  assert((action >= 0) && (action < ACTION_NUM_CODES));
  printf("Action is '%s'\n", ACTION_NAMES[action]);
  if (pexl_Ref_invalid(ref)) {
    printf("failed to create expression\n");
    return NULL;
  }
  // Here we use the desired optimizations
  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  if (!pkg || (pexl_Error_value(err) != OK)) {
    printf("compile failed with error: %d\n", pexl_Error_value(err));
    pexl_free_Expr(exp);
    return NULL;
  }
  if (PRINTING) {
    printf("Compiled pattern %p: %s\n", (void *)pkg, name);
    pexl_print_Binary(pkg);
  }
  return pkg;
}  

//search through the input and find a match 
static int do_match (const char *pattern_name,
		     pexl_Binary *pkg,
		     const char *input_ptr, size_t input_len,
		     pexl_Match *match,
         int action) {
  int stat;
  const char *bound_name = NULL;
  switch (action) {
  case ACTION_FIND_ALL:
    bound_name = "findAll";
    break;
  case ACTION_CAPTURE_ALL:
    bound_name = "captureAll";
    break;
  case ACTION_SEARCH_ALL:
    bound_name = "searchAll";
    break;
  case ACTION_CAPTURE: case ACTION_FIND: case ACTION_SEARCH:
    bound_name = "exp";
    break;
  default:
    ERROR("unknown ACTION in do_match()");
  }
  assert(bound_name); assert(match); assert(input_ptr);
  
  /* Suppress "unused variable", for when DEBUG not defined */
  (void) pattern_name;

  if (PRINTING) {
    printf("Ready to run the vm:\n");
    printf("  matching with pattern %s\n", pattern_name);
    printf("  input starts with '%.*s'\n", 50, input_ptr);
  }

  stat = pexl_run(pkg, NULL, bound_name,
		  input_ptr, input_len, 0,
		  0,		// vmflags: allow SIMD
		  NULL, match);
  if (stat) {
    fprintf(stderr, "pexl_run failed with return status %d\n", stat);
    return FALSE;
  }
  return TRUE;
}

/* Caller must free the return value */
static char *readfile (const char *filename, size_t *filesize) {
  FILE *f = fopen(filename, "r");
  if (!f) return NULL;
  int stat = fseek(f, 0, SEEK_END);
  if (stat) {
    printf("Seek failed\n");
    fclose(f);
    return NULL;
  }
  long filelen = ftell(f);
  if (filelen < 0) {
    printf("Invalid file size %ld\n", filelen);
    fclose(f);
    return NULL;
  }
  *filesize = (size_t) filelen;
  stat = fseek(f, 0, 0);
  if (stat) {
    printf("Seek failed\n");
    fclose(f);
    return NULL;
  }
  char *contents = (char *) malloc(*filesize+1); 
  if (!contents) {
    printf("malloc failed\n");
    return NULL;
  }
  size_t n = fread(contents, (size_t) *filesize, 1, f);
  if (n != 1) {			/* one item */
    printf("fread failed\n");
    free(contents);
    contents = NULL;
  }
  contents[*filesize] = '\0';
  fclose(f);
  return contents;
}

/* 
   This is the capture with the expected String and its end position.
   Note that the position is 1-based, following Lua's convention.
*/
typedef struct TestCapture {
  const char *expected_cap;
  int64_t expected_pos;
} TestCapture;

/* 
   These are the patterns from the lpeg paper, translated to RPL.
*/
typedef struct TestPattern {
  const char *name;		// Command-line name
  const char *source;		// RPL source, which Rosie can compile
  pexl_Index expected_num;	// Expected number of matches
  TestCapture *capture;		// Some of the capture results
} TestPattern;

/*
  The following structures contain expected results.  Each test
  program in our benchmark suite has this data, and uses it to confirm
  that it found the correct set of matches.  The format is: Some
  number of captures starting with the first one, ending in a NULL,
  followed by the last match and another NULL entry.
*/

TestCapture abramCaptures[] = {
  {"begat Abram", 35837}, {"begat Abram", 35921}, {"And Abram", 36076}, //3
  {"of Abram", 36121}, {"took Abram", 36322}, {"son Abram", 36416}, //6
  {"unto Abram", 36679}, {"So Abram", 37066}, {"and Abram", 37142}, //9
  {"And Abram", 37220}, {"And Abram", 37467}, {"unto Abram", 37621}, //12
  {"And Abram", 37978}, {"and Abram", 38075}, {"when Abram", 38659}, //15
  {"entreated Abram", 38889}, {"Sarai Abram", 39104}, {"called Abram", 39144}, //18
  {"And Abram", 39514}, {"And Abram", 39625}, {"there Abram", 39908}, //21
  {"with Abram", 39982}, {"of Abram", 40223}, {"And Abram", 40343}, //24
  {"unto Abram", 41290}, {"Then Abram", 41815}, {"told Abram", 43639}, //27
  {"with Abram", 43777}, {"when Abram", 43800}, {"be Abram", 44599}, //30
  {"unto Abram", 44818}, {"And Abram", 44888}, {"made Abram", 45166}, //33
  {"unto Abram", 45388}, {"And Abram", 45491}, {"And Abram", 45626}, //36
  {"upon Abram", 46787}, {"unto Abram", 46870}, {"with Abram", 47563}, //39
  {"Sarai Abram", 47904}, {"unto Abram", 48025}, {"And Abram", 48168}, //42
  {"Sarai Abram", 48223}, {"husband Abram", 48353}, {"unto Abram", 48533}, //45
  {"But Abram", 48715}, {"bare Abram", 49961}, {"and Abram", 49978}, //48
  {"And Abram", 50045}, {"to Abram", 50111}, {"when Abram", 50133}, //51
  {"to Abram", 50191}, {"And Abram", 50378}, {"called Abram", 50577}, //54
  {"choose Abram", 1911253}, {NULL, -1}, //55
  {"choose Abram", 1911253}, {NULL, -1} 
};

TestCapture longwordCaptures[] = {
  {"Jegarsahadutha", 122441},{"interpretation", 156864}, {"interpretations", 157331},
  {"interpretation", 157841}, {"interpretation", 158446}, {"interpretation", 158759},
  {"interpretation", 160811}, {"Zaphnathpaaneah", 165192}, {NULL, -1}, 
  {"fellowservants", 4285307}, {NULL, -1} //Excluded remaining 450 results
};

TestCapture smallwordCaptures[] = {
 {"Testament", 20}, {"James", 38}, {"Version", 46}, 
 {"Bible", 59}, {"First",  73},  {"Moses",  87},  
 {"Called",  96}, {"Genesis",  104},  {"beginning",  127}, {NULL, -1},
 {"Bible", 4333039}, {NULL, -1}
};

TestCapture omegaCaptures[] = {
  {"Omega", 4269606}, {"Omega", 4270062}, {"Omega", 4326666}, {"Omega", 4331672}, {NULL, -1} ,
  {"Omega", 4331672}, {NULL, -1}
};

TestCapture theCaptures[] = {
  {NULL, -1}, {NULL, -1}
};

TestCapture tubalcainCaptures[] = {
  {"Tubalcain", 13849}, {"Tubalcain", 13930}, {NULL, -1},
  {"Tubalcain", 13930}, {NULL, -1}
};

TestCapture psalmCaptures[] = {
  {"119:176", 2279160}, {NULL, -1},
  {"119:176", 2279160}, {NULL, -1}
};

TestCapture adamantCaptures[] = {
  {"adamascus", 1647163}, {"adamant", 2900583}, {"adamant", 3304491}, {NULL, -1},
  {"adamant", 3304491}, {NULL, -1}
};

TestPattern patterns[] = {
  { "abram",     "find:{[[a-z][A-Z]]+ \" \"* \"Abram\"}",   55,  abramCaptures },
  { "longword",  "find:[[a-z][A-Z]]{14,}",                  458, longwordCaptures },
  { "smallword",  "find:[[a-z][A-Z]]{5,}",                 243604, smallwordCaptures },
  { "omega",     "find:\"Omega\"",                          4,   omegaCaptures },
  { "the",       "find:\"@the\"",                           0,   theCaptures },
  { "tubalcain", "find:\"Tubalcain\"",                      2,   tubalcainCaptures },
  { "psalm",     "find:{[[1-9][:]]{0,5} \"7\" \"6\"+}",     1,   psalmCaptures },
  { "adamant",   "find:{\"adam\"[[a-z][A-Z]]*}",            3,   adamantCaptures },
  { NULL, NULL, 0, NULL}
};

static void usage (int argc, char **argv) {
  if (argc < 1) {
    printf("ERROR\n");
    exit(ERR_NOARGS);
  }
  printf("Usage: %s {noop|setup|run} "
	 "{search|capture|find|captureAll|searchAll|findAll} "
	 "pattern reps [optim] [inputfile]\n", argv[0]);
  printf("  noop --> read input file and exit\n");
  printf("  setup --> read input file, compile pattern, print bytecode\n");
  printf("  run --> read input file, compile pattern, run pattern, check results\n");
  printf("Valid pattern names are:\n");
  int i = 0;
  TestPattern p = patterns[i];
  while (p.name) {
    printf("  %s\n", p.name);
    i++;
    p = patterns[i];
  }
  printf("Valid optimization arg is a bitmask:\n");
  printf("  bit %d --> enable peephole\n", PEEPHOLE_BIT);
  printf("  bit %d --> enable loop unroller\n", UNROLLING_BIT);
  printf("  bit %d --> enable inlining\n", INLINING_BIT);
  printf("  bit %d --> enable tro\n", TRO_BIT);
  printf("  bit %d --> enable simd\n", SIMD_BIT);
  printf("  Default is ALL enabled.\n");
}

static TestPattern find_pattern (const char *name) {
  int i = 0;
  while (patterns[i].name) {
    if (strcmp(name, patterns[i].name)==0)
      return patterns[i];
    i++;
  }
  return patterns[i];
}

static int process_args (int argc, char **argv,
			 int *command, int *action,
			 TestPattern *pattern, int *reps,
			 uint32_t *optimizers,
			 const char **inputfile) {
  //-- COMMAND
  if (argc < 2) {
    usage(argc, argv);
    return(ERR_USAGE);
  }
  *command = -1;
  for (int i=0; i < COMMAND_NUM_CODES; i++)
    if (strcmp(argv[1], COMMAND_NAMES[i])==0) *command = i;

  if (*command == -1) {
    printf("Invalid command: %s\n", argv[1]);
    usage(argc, argv);
    return(ERR_USAGE);
  }
  //-- ACTION
  if (argc < 3) {
    printf("Missing action argument\n");
    usage(argc, argv);
    return(ERR_USAGE);
  }
  *action = -1;
  for (int i=0; i < ACTION_NUM_CODES; i++)
    if (strcmp(argv[2], ACTION_NAMES[i])==0) *action = i;

  if (*action == -1) {
    printf("Invalid action: %s\n", argv[2]);
    usage(argc, argv);
    return(ERR_USAGE);
  }
  // PATTERN
  if (argc < 4) {
    printf("Missing pattern argument\n");
    usage(argc, argv);
    return(ERR_USAGE);
  }
  *pattern = find_pattern(argv[3]);
  if (!pattern->name) {
    printf("Invalid pattern '%s'\n", argv[3]);
    usage(argc, argv);
    return(ERR_USAGE);
  }
  //-- REPETITIONS
  if (argc < 5) {
    printf("Missing number of repetitions\n");
    usage(argc, argv);
    return(ERR_USAGE);
  }
  *reps = atoi(argv[4]);
  if (argc >= 6) {
    /* Bitmask for various optimizers (see usage, above) */
    *optimizers = atoi(argv[5]);
  } 
  if (argc >= 7) {
    /* Have optional input file argument from CLI */
    *inputfile = argv[6];
  }
  return OK;
}

static
int compare (pexl_MatchNode *node, TestCapture *cap, const char *input) {
  // Adjust to the 1-based indexing that this test program expects
  if (cap->expected_pos != pexl_MatchNode_end(node) + 1) {
    printf("pexl_Match capture end position %" pexl_Position_FMT
	   " did not match expected %" pexl_Position_FMT "\n",
	   pexl_MatchNode_end(node) + 1, cap->expected_pos);   
    return ERR_MATCH;
  }
  const char *data = &input[pexl_MatchNode_start(node)];
  size_t len = pexl_MatchNode_end(node) - pexl_MatchNode_start(node);
  if (PRINTING) printf("%.*s\n", (int) len, data);
  if (memcmp(cap->expected_cap, data, len) != 0) { 
    printf("pexl_Match capture data '%.*s' (at pos %"
	   pexl_Position_FMT ") did not match expected '%s'\n",
	   (int) len, data, cap->expected_pos, cap->expected_cap);   
    return ERR_MATCH;
  }
  return OK;
}

// For actions that produce no captures, like ACTION_SEARCH and
// ACTION_SEARCHALL, we have only the match end position to validate.
static
int validate_zero (pexl_Match *m, pexl_Position expected_pos) {
  // Adjust to the 1-based indexing that this test program expects
  if (expected_pos != pexl_Match_end(m) + 1) {
    printf("pexl_Match capture end position %" pexl_Position_FMT
	   " did not match expected %" pexl_Position_FMT "\n",
	   pexl_Match_end(m) + 1, expected_pos);   
    return ERR_MATCH;
  }
  return OK;
}

// For actions that produce just one match, we find that match in the
// only child of the root.  These are: ACTION_CAPTURE, ACTION_FIND.
static
int validate_one (pexl_Match *m, TestCapture *cap, const char *input) {
  pexl_MatchNode *node = NULL;
  pexl_Index i = pexl_MatchTree_child(m, PEXL_ITER_START, &node);
  // Check for no match found AND no match was expected
  if ((i == PEXL_MATCH__NOEXIST) && (!cap->expected_cap)) return OK;
  if (i < 0) {
    printf("validate_one: error getting root node of match tree\n");
    exit(ERR_MATCH);
  }
  i = pexl_MatchTree_child(m, i, &node);
  if (i < 0) {
    printf("validate_one: error getting child node of match tree\n");
    exit(ERR_MATCH);
  }
  return compare(node, cap, input);
}

// For actions that produce every match, like ACTION_CAPTURE_ALL and
// ACTION_FIND_ALL, we validate all the captures that are present in
// the test pattern's capture list.  In the PEXL results, these
// captures are all children of the root node.
static
int validate_all (pexl_Match *m, TestPattern *pattern, const char *input) {
  int status;
  pexl_Index count = 0;
  pexl_MatchNode *node;
  pexl_Index i = pexl_MatchTree_child(m, PEXL_ITER_START, &node);
  // Check for no match found, and no match expected
  if ((i == PEXL_MATCH__NOEXIST) && (!pattern->capture->expected_cap)) return OK;
  if (i < 0) {
    printf("validate_all: error getting root node of match tree\n");
    exit(ERR_MATCH);
  }
  i = pexl_MatchTree_child(m, i, &node);
  if (i < 0) {
    printf("validate_all: error getting first child of the root of match tree\n");
    exit(ERR_MATCH);
  }
  // Validate the initial set of hard-coded results
  TestCapture *cap = pattern->capture;
  while (cap->expected_cap) {
    count++;
    status = compare(node, cap, input);
    if (status != OK) return status;
    cap++;
    i = pexl_MatchTree_next(m, i, &node);
    if ((i < 0) && cap->expected_cap) {
      printf("validate_all: error getting next node of match tree\n");
      exit(ERR_MATCH);
    }
  } /* while */
  // Now validate the last capture, i.e. the last hardcoded result
  // Advance beyond the NULL entry
  cap++;
  // Skip ahead to the last child node, which may be the current one
  pexl_MatchNode *last;
  while (i >= 0) {
    // Have to count the prior success of pexl_MatchTree_next
    // from the while loop above.
    count++;
    i = pexl_MatchTree_next(m, i, &last);
    if (i < 0) break;
    node = last;
  }
  status = compare(node, cap, input);
  if (status != OK) return status;

  if (count != pattern->expected_num) {
    printf("capture count %" pexl_Index_FMT
	   " did not match expected count of %" pexl_Index_FMT
	   "\n",
	   count, pattern->expected_num);
    exit(ERR_MATCH);
  }
  return OK;
}

static
pexl_Position last_expected_position (TestCapture *cap) {
  // Find first NULL entry
  while (cap->expected_cap) cap++;
  // Move past it to the entry with the info on the last match
  cap++;
  return cap->expected_pos;
}

// Perform the match 'reps' times, then validate the results.
static int
test_Matches(int action, TestPattern *pattern, int reps, pexl_Binary *pat,
	     const char *input, int inputlen, pexl_Match *match) {
  assert(pattern && pattern->capture);
  for (int rep = 0; rep < reps; rep++) {
    if (!do_match(pattern->name, pat, input, inputlen, match, action)) {
      printf("pexl_Matching failed\n");
    }
  } // for each required rep
  if (PRINTING) pexl_print_Match(match);
  if (pexl_Match_failed(match) && pattern->capture->expected_cap) {
    printf("Found no matches where some were expected\n");
    return ERR_MATCH;
  } else if (!pexl_Match_failed(match) && !pattern->capture->expected_cap) {
    printf("Found matches where none were expected\n");
    return ERR_MATCH;
  } 
  switch (action) {
  case ACTION_CAPTURE:
  case ACTION_FIND:
    return validate_one(match, pattern->capture, input);
  case ACTION_SEARCH:
    return validate_zero(match, pattern->capture->expected_pos);
  case ACTION_SEARCH_ALL:
    return validate_zero(match, last_expected_position(pattern->capture));
  case ACTION_CAPTURE_ALL:
  case ACTION_FIND_ALL:
    return validate_all(match, pattern, input);
  default:
    printf("Invalid action in test_Matches!\n");
  }
  return ERR_USAGE;		// invalid action???
}

/* 
   The structure of the main function is as follows 
   1. read input file
   2. process command line arguments
   3. set up match and pattern structures 
   4. search for matches
   5. test actual against expected 
*/
int main (int argc, char **argv) {

  int command = 0;		    /* arg 1 */
  int action = 0;		    /* arg 2 */
  pexl_Context *pat = NULL;	    /* arg 3 */
  int reps;			    /* arg 4 (required when command is 'run') */
  uint32_t optimizer_flags = 31;    /* Optional arg 5: default is ALL */
  const char *inputfilename = NULL; /* Optional arg 6 */
  int exitStatus = 0;
  pexl_Match *match = NULL;
  TestPattern pattern;
  size_t inputlen;
  char *input = NULL;
  pexl_Binary *pkg = NULL;
  pexl_Optims *optims = NULL;
  
  int argpexl_Error = process_args(argc, argv,
			      &command, &action, &pattern, &reps,
			      &optimizer_flags, &inputfilename);
  if(argpexl_Error != OK) {
    EXIT(argpexl_Error);
  }
  
  // Always read the input file, no matter what the command-line args are
  if (!inputfilename) inputfilename = INPUTFILENAME;
  input = readfile(inputfilename, &inputlen);
  if (!input) {
    printf("Could not open input file '%s'\n", inputfilename);
    EXIT(ERR_USAGE);
  }

  if (command == COMMAND_NOOP) EXIT(OK);

  pat = pexl_new_Context();
  if (!pat) {
    printf("context_new failed\n"); 
    EXIT(ERR_SETUP);
  }

  printf("Pexl optimizer flag value: %d\n", optimizer_flags);
  if (optimizer_flags & SIMD_BIT) {
    optims = pexl_addopt_simd(optims, 1);
    printf("  SIMD/VECTOR\n");
  }
  if (optimizer_flags & TRO_BIT) {
    optims = pexl_addopt_tro(optims);
    printf("  TAIL RECURSION\n");
  }
  int loop_unroll_parameter = 5;
  if (optimizer_flags & UNROLLING_BIT) {
    optims = pexl_addopt_unroll(optims, loop_unroll_parameter);
    printf("  LOOP UNROLL(%d)\n", loop_unroll_parameter);
  }
  if (optimizer_flags & INLINING_BIT) {
    optims = pexl_addopt_inline(optims);
    printf("  INLINING\n");
  }
  if (optimizer_flags & PEEPHOLE_BIT) {
    optims = pexl_addopt_peephole(optims);
    printf("  PEEPHOLE\n");
  }
  
  //-- Setup match data structure
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  if (!match) EXIT(ERR_OOM);

  //-- Create the pattern from our preset testing suite 
  pkg = compile_by_name(pat, pattern.name, action, optims);
  if (!pkg) EXIT(ERR_COMPILE);

  //-- If we ran setup, print the bytecode and exit now
  if (command==COMMAND_SETUP) {
    printf("Compiled pattern '%s' for action '%s' %d\n", argv[3], argv[2], optimizer_flags);
    pexl_print_Binary(pkg);
    EXIT(OK);
  }

  //-- Print status message 
  printf("Running %d iterations of test '%s'\n", reps, pattern.name);
  printf("Input file: %s\n", inputfilename);
  printf("Input size: %zu bytes\n", inputlen);
  
  if (command == COMMAND_RUN) {
    test_Matches(action, &pattern, reps, pkg, input, inputlen, match);
  } /* if command is RUN */

  printf("Done.\n\n");

 quit:
  if (input) free(input);
  if (match) pexl_free_Match(match);
  if (pkg) pexl_free_Binary(pkg);
  if (pat) pexl_free_Context(pat);
  if (optims) pexl_free_Optims(optims);
  return exitStatus;
}
