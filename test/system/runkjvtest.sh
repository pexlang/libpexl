#!/bin/bash
#
# Run kjvtest using each pattern/action combination and a given
# optimization setting.

set -eo pipefail		# exit on error

optim1=$1
if [[ -z "$1" ]]; then
   printf "No optimization argument given.  Defaulting to 31.\n"
   printf "Optimization arg is a bitmask: (bit 4) SIMD,UNROLL,INLINE,TRO,PEEPHOLE (bit 0).\n"
   optim1=31;
fi

declare -a patterns=("abram" "longword" "omega" "the" "tubalcain" "psalm" "adamant" "smallword")
declare -a actions=(    "searchAll"  "search" "capture" "captureAll" "find" "findAll")
declare -i repetitions=(    1            1        1          1          1      1     ) 

for i in $(seq 0 $((${#patterns[@]} - 1))); do
    for j in $(seq 0 $((${#actions[@]} - 1))); do
        ./kjv-test run ${actions[$j]} ${patterns[$i]} ${repetitions[$j]} $optim1 kjv10.txt
    done
done

