/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  inlinepolicytest.c  TESTING inlining optimizer policies                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie Jennings, Kiran Rao                                       */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include <time.h>
#include "print.h"
#include "compile.h"
#include "vm.h"
#include "opt.h"

#include "../test.h"

#define PRINTING NO

#define ENCODER PEXL_TREE_ENCODER

static int iterations = 1000;

/*
  We will start by testing only non-recursive patterns, because our
  inlining optimizer will not inline a recursive pattern.
*/

static char* createInputString( int inputSize, const char *input) {
  TEST_ASSERT_MSG(inputSize > 0, "Non-positive input size");
  TEST_ASSERT_MSG((strlen(input) == 10), "Input string not length 10");
  char* inputString = (char *) malloc((sizeof(char) * 10) + sizeof(char));
  int size = 10;
  for (int i = 0; i < inputSize; i++) {
    if ( size <= (i * 10)) {
      size *= 2;
      inputString = (char *) realloc(inputString, (sizeof(char) * (size)) + sizeof(char));
    }
    strncpy(inputString + ((i * 10) * sizeof(char)), input, 10);
  }
  return inputString;
}

static int vmrun (const char *pattern_desc,
		  pexl_Binary *pkg,
		  pexl_Index entrypoint,
		  pexl_Optims *optims,
                  const char *input,
		  pexl_Match *match, int expected_to_match,
		  int print_details) {
  
  int stat;
  long inputlen = (long) strlen(input);

  if (print_details) {
    printf("Running vm on pattern '%s' %s SIMD.  Entrypoint is %d.  ",
	   pattern_desc,
	   optimlist_contains(optims, PEXL_OPT_SIMD) ? "with" : "without",
	   entrypoint);
    printf("Input is: \"%.*s\"\n", (int) inputlen, input);
  }
  stat = vm_start(pkg, NULL, entrypoint, 
		  input, inputlen, 0, inputlen, 0, NULL, match);
  if (stat != 0) {
    printf("vmrun: vm returned status code %d\n", stat);
    fflush(NULL);
    return stat;
  }
  if (print_details) {
    pexl_print_Match_summary(match);
    pexl_print_Match_data(match);
  }
  if ((match->end != -1) != expected_to_match) {
    if (print_details) {
        printf("vmtest: expected %s\n",
	       expected_to_match ? "success but match failed" : "fail but match succeeded");
	fflush(NULL);
    }
    return 1111;
  }
  /* We expect a match of the entire input string */
  if ((match->end != -1) && (match->end != inputlen)) {
    printf("vmtest: expected match end of %lu, got %ld\n", inputlen, match->end);
    fflush(NULL);
    return 2222;
  }
  return OK;
}

static double testFunctionSpeed(pexl_Binary *pkg,
				SymbolTableEntry *entry,
				pexl_Optims *optims,
				const char *input,
				pexl_Match *match, int expected_to_match,
				int numRuns) {
  const char *name = symboltable_entry_name(pkg->symtab, entry);
  clock_t start = clock();
  int stat;
  for ( int i = 0; i < numRuns; i++) {
    if ((stat = vmrun(name,
		      pkg,
		      entry->value,	/* entrypoint */
		      optims,
		      input,
		      match, expected_to_match,
		      PRINTING))) {
      printf("vmrun returned error %d on iteration %d\n", stat, i);
      TEST_ASSERT(stat == 0);
      break;
    }
  }
  clock_t end = clock();
  double cpu_time_used = ((double) (end - start));
  return cpu_time_used / CLOCKS_PER_SEC;
}

/* REQUIREMENT: 'entry' in symboltable and 'ref' are the same binding */
static double compareInlining(pexl_Context *C,
			      const char *pattern_name,
			      pexl_Ref ref,
			      const char *input,
			      pexl_Match *match,
			      int reps) {
  double runtime_with, runtime_without;
  pexl_Error err;
  pexl_Binary *pkg;
  SymbolTableEntry *entry;
  pexl_Optims *optims;
  
  TEST_SECTION("Compare performance effect of inlining");
  
  TEST_ASSERT(C && pattern_name && !pexl_Ref_invalid(ref));

  optims = NULL;
  printf("No optimizations: ");
  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  entry= binary_lookup_symbol(pkg, symbol_ns_id, pattern_name);
  TEST_ASSERT(entry);
  if (PRINTING) pexl_print_Binary(pkg);
  runtime_without = testFunctionSpeed(pkg, entry, optims, input, match, true, reps);
  binary_free(pkg);

  printf("With INLINE and PEEPHOLE optimization: ");
  optims = pexl_addopt_inline(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_INLINE));
  TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_PEEPHOLE));
  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  entry= binary_lookup_symbol(pkg, symbol_ns_id, pattern_name);
  TEST_ASSERT(entry);
  if (PRINTING) pexl_print_Binary(pkg);
  runtime_with = testFunctionSpeed(pkg, entry, optims, input, match, true, reps);
  binary_free(pkg);
  pexl_free_Optims(optims);

  double performance_change =  runtime_without / runtime_with;
  printf("Performance ratio of inlining/none = %4.2f\n\n", performance_change);
  return performance_change;
}

static
pexl_Binary *compile_expression (pexl_Context *C,
				 pexl_Optims *optims,
				 pexl_Expr *exp,
				 pexl_Env env,
				 pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  //confess("TEMP", "USING NO OPTIMS");
  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);

#if 0
  confess("TEMP", "setting default entrypoint manually");
  Binding *b = env_get_binding(C->bt, ref);
  TEST_ASSERT(b && (bindingtype(b) == Epattern_t));
  Pattern *pat = (Pattern *) b->val.ptr;
  TEST_ASSERT(pat);
  TEST_ASSERT(pat->entrypoint >= 0);
  pkg->ep = pat->entrypoint;
#endif
  return pkg;
}

int main(int argc, char **argv) {

  /* Declare var*/
  pexl_Match *match;
  pexl_Env env;
  pexl_Context *C;
  pexl_Optims *optims;
  pexl_Binary *pkg;
  pexl_Error err;
  SymbolTableEntry *entry;
  
  TEST_START(argc, argv);
  TEST_SECTION("Setting up");

  C = pexl_new_Context();
  TEST_ASSERT(C);
  env = 0;			// root env has id 0

  optims = pexl_default_Optims();
  
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);

  /** Create letter experission, matches a single letter*/
  pexl_Expr *letter = pexl_match_range(C, (int)'a', (int)'z');
  TEST_ASSERT(letter);

  pkg = compile_expression(C, optims, letter, env, &err);
  TEST_ASSERT(pkg);

  TEST_ASSERT(!vmrun("letter", pkg, main_entrypoint(pkg), optims,
                    "m", match, true, PRINTING));

  TEST_ASSERT(!vmrun("letter", pkg, main_entrypoint(pkg), optims,
                    "3", match, false, PRINTING));

  binary_free(pkg);

  /** Create num experission, matches a [0-9]+ */
  pexl_Expr *num;
  num = pexl_repeat_f(C, pexl_match_range(C, (int)'0', (int)'9'), 1, 0);
  TEST_ASSERT(num);

  pkg = compile_expression(C, optims, num, env, &err);
  TEST_ASSERT(pkg);

  pexl_print_Binary(pkg);

  TEST_ASSERT(!vmrun("num", pkg, main_entrypoint(pkg), optims,
                    "12345", match, true, PRINTING));
  TEST_ASSERT(!vmrun("num", pkg, main_entrypoint(pkg), optims,
                    "A", match, false, PRINTING));

  binary_free(pkg);

  /** pexl_Expr *A that matches the pattern [a-z][0-9]+ with no references*/
  pexl_Expr *A;
  pexl_Ref Aref = pexl_bind_in(C, env, NULL, "A");
  
  A = pexl_seq(C, letter, num);
  TEST_ASSERT(pexl_rebind(C, Aref, A) == OK);

  /* First, we do some unit tests on the expression itself */
  pkg = pexl_compile(C, Aref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING) pexl_print_Binary(pkg);

  TEST_ASSERT(!vmrun("A", pkg, main_entrypoint(pkg), optims,
		     "a12345", match, true, PRINTING));
  TEST_ASSERT(!vmrun("A", pkg, main_entrypoint(pkg), optims,
		     "12345", match, false, PRINTING));
  TEST_ASSERT(!vmrun("A", pkg, main_entrypoint(pkg), optims,
		     "a", match, false, PRINTING));
  entry = binary_lookup_symbol(pkg, symbol_ns_id, "A");
  double runTime = testFunctionSpeed(pkg, entry, optims, "a12345", match, true, iterations);
  printf("Time to run expression A (No references) in %d runs is %lf\n",
	 iterations, runTime);

  binary_free(pkg);

  /** Create pexl_Expr *B that matches the pattern [a-z][0-9]+ with references with no optim*/

  //Bind patterns
  pexl_Ref letRef = pexl_bind_in(C, env, NULL, "letRef");
  pexl_Ref numRef = pexl_bind_in(C, env, NULL, "numRef");

  TEST_ASSERT(pexl_rebind(C, letRef, letter) == OK);
  TEST_ASSERT(pexl_rebind(C, numRef, num) == OK);

  // Create expression and bind
  pexl_Expr *B = pexl_seq_f(C, pexl_call(C, letRef), pexl_call(C, numRef));
  
  pexl_Ref bRef = pexl_bind_in(C, env, NULL, "B");
  TEST_ASSERT(pexl_rebind(C, bRef, B) == OK);

  pexl_free_Optims(optims);
  optims = NULL;
  pkg = pexl_compile(C, bRef, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);

  // Locate B entry point
  entry = binary_lookup_symbol(pkg, symbol_ns_id, "B");
  TEST_ASSERT(entry);

  TEST_ASSERT(!vmrun("B", pkg, entry->value, optims,
		     "a12345", match, true, PRINTING));
  TEST_ASSERT(!vmrun("B", pkg, entry->value, optims,
		     "12345", match, false, PRINTING));
  TEST_ASSERT(!vmrun("B", pkg, entry->value, optims,
		     "a", match, false, PRINTING));
  runTime = testFunctionSpeed(pkg, entry, optims, "a12345", match, true, iterations);
  printf("Time to run expression B with no optim in %d runs is %lf\n",
	 iterations, runTime);

  binary_free(pkg);

  /** Recompile B with optim*/
  TEST_ASSERT_NULL(optims);
  optims = pexl_addopt_inline(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);
  pkg = pexl_compile(C, bRef, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);

  // Locate B entry point
  entry= binary_lookup_symbol(pkg, symbol_ns_id, "B");
  TEST_ASSERT(entry);

  TEST_ASSERT(!vmrun("B", pkg, entry->value, optims,
		     "a12345", match, true, PRINTING));
  TEST_ASSERT(!vmrun("B", pkg, entry->value, optims,
		     "12345", match, false, PRINTING));
  TEST_ASSERT(!vmrun("B", pkg, entry->value, optims,
		     "a", match, false, PRINTING));
  runTime = testFunctionSpeed(pkg, entry, optims, "a12345", match, true, iterations);
  printf("Time to run expression B with optim in %d runs is %lf\n",
	 iterations, runTime);

  binary_free(pkg);
  
  /** Create expression repeatpexl_Match with matches the pattern [abcde1235]+ with references*/
  pexl_Expr * letStringpexl_Match = pexl_match_string(C, "abcde");
  pexl_Expr * numStringpexl_Match = pexl_match_string(C, "12345");

  pexl_Expr * seqpexl_Match;
  pexl_Expr * repeatMatch;

  char* inputString = createInputString(iterations, "abcde12345");
  
  pexl_Ref letStringRef = pexl_bind_in(C, env, NULL, "letStringpexl_Match");
  TEST_ASSERT(pexl_rebind(C, letStringRef, letStringpexl_Match) == OK);
  pexl_Ref numStringRef = pexl_bind_in(C, env, NULL, "numStringpexl_Match");
  TEST_ASSERT(pexl_rebind(C, numStringRef, numStringpexl_Match) == OK);

  //Create seq match and ref
  seqpexl_Match = pexl_seq_f(C, pexl_call(C, letStringRef), pexl_call(C, numStringRef));

  pexl_Ref seqRef = pexl_bind_in(C, env, NULL, "seqpexl_Match");
  TEST_ASSERT(pexl_rebind(C, seqRef, seqpexl_Match) == OK);
 
  //Create repeat match and ref
  repeatMatch = pexl_repeat_f(C, pexl_call(C, seqRef), 2, 0);
  TEST_ASSERT(repeatMatch);

  pexl_Ref repeatMatchRef = pexl_bind_in(C, env, NULL, "repeatMatch");
  TEST_ASSERT(!pexl_Ref_invalid(repeatMatchRef));

  TEST_ASSERT(pexl_rebind(C, repeatMatchRef, repeatMatch) == OK);

  // Compile with no optim
  pexl_free_Optims(optims);
  optims = NULL;
  pkg = pexl_compile(C, repeatMatchRef, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);

  // Look up entry point
  entry = binary_lookup_symbol(pkg, symbol_ns_id, "repeatMatch");
  TEST_ASSERT(entry);

  // Test
  runTime = testFunctionSpeed(pkg, entry, optims, inputString, match, true, iterations);
  printf("Time to run expression repeatMatch with no optim in %d runs is %lf\n",
	 iterations, runTime);

  binary_free(pkg);
  
  // Recompile with optim
  TEST_ASSERT_NULL(optims);
  optims = pexl_addopt_inline(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);
  pkg = pexl_compile(C, repeatMatchRef, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);

  if (PRINTING) pexl_print_Binary(pkg);

  // Look up entry point
  entry = binary_lookup_symbol(pkg, symbol_ns_id, "repeatMatch");
  TEST_ASSERT(entry);

  // Test
  runTime = testFunctionSpeed(pkg, entry, optims, inputString, match, true, iterations);
  printf("Time to run expression repeatMatch with optim in %d runs is %lf\n",
	 iterations, runTime);

  binary_free(pkg);
  
  compareInlining(C, "repeatMatch", repeatMatchRef, inputString, match, iterations);

  pexl_free_Optims(optims);
  
  TEST_END();
}
