/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  packagetest.c  TESTING the package and package table structures          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"
#include "binary.h"
#include "print.h"

#include "../test.h"

#define YES 1
#define NO 0

#define FUZZING NO
#define PRINTING NO

int main(int argc, char **argv) {

  int stat;
  pexl_PackageTable *m;
  pexl_Binary *p, *p2, *p3, *pkg, *user, *prelude, *p1;
  pexl_Index probe;
  size_t i;
  char *str, *str1, *str2;
  uint32_t oldsize;

  /* ----------------------------------------------------------------------------- */
  TEST_START(argc, argv);

  m = packagetable_new(0);
  TEST_ASSERT(m);		/* giving table size 0 ==> default will be used */
  TEST_ASSERT(m->capacity == PACKAGETABLE_INITSIZE);
  packagetable_free(m);
  
  m = packagetable_new(PACKAGETABLE_MAXSIZE + 1);
  TEST_ASSERT(m);		/* size too high ==> max size will be used */
  TEST_ASSERT(m->capacity == PACKAGETABLE_MAXSIZE);
  packagetable_free(m);
  
  m = packagetable_new(PACKAGETABLE_MAXSIZE);
  TEST_ASSERT(m);
  TEST_ASSERT(m->capacity == PACKAGETABLE_MAXSIZE);
  TEST_ASSERT(m->next == 1);         /* next field starts at 1 because user pkg is in slot 0 */
  TEST_ASSERT(m->packages != NULL);
  packagetable_free(m);

  prelude = binary_new();
  TEST_ASSERT(prelude);
  TEST_ASSERT(prelude->symtab);
  binary_free(prelude); prelude = NULL;

  user = binary_new();
  TEST_ASSERT(user);            /* OK for importpath, prefix, source to be empty strings */
  TEST_ASSERT(user->symtab);
  binary_free(user); user = NULL;

  m = packagetable_new(1);
  TEST_ASSERT(m);		/* make a min sized table */
  TEST_ASSERT(m->capacity < 2);	/* size field may jump from 1 to a much larger size */
  TEST_ASSERT(m->next == 1);	/* next field starts at 1 because user pkg is in slot 0 */
  TEST_ASSERT(m->packages != NULL);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexl_Binary tests");

  str = malloc(SYMBOLTABLE_MAXLEN + 2);
  memset(str, 'A', SYMBOLTABLE_MAXLEN + 1);
  str[SYMBOLTABLE_MAXLEN + 1] = '\0';
  TEST_ASSERT(strlen(str) == SYMBOLTABLE_MAXLEN + 1); /* verifying the precondition for test */
  p = binary_new();
  TEST_ASSERT(p);		/* should succeed */

  stat = binary_set_attributes(p, str, NULL, NULL, NULL, 0, 0);
  // Warning will print the numeric value of PACKAGE_ERR_STRLEN.
  TEST_ASSERT(stat == PEXL_COMPILER__ERR_LEN);

  stat = binary_set_attributes(p, "+p_importpath+", NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);

  TEST_ASSERT(p->symtab != NULL);
  TEST_ASSERT(p->code == NULL);
  TEST_ASSERT(p->codesize == 0);
  TEST_ASSERT(package_importpath(p) == 0); /* importpath stored in first symtab entry */
  TEST_ASSERT(package_prefix(p) == -1);	   /* prefix not set */
  TEST_ASSERT(p->origin == -1);		   /* origin not set */
  TEST_ASSERT(p->major == 0);
  TEST_ASSERT(p->minor == 0);

  stat = binary_set_attributes(p, "+p_importpath+", "pre", NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(package_prefix(p) != -1); /* prefix field now set */
  TEST_ASSERT(p->origin == -1);		/* origin field still NOT set */
  
  stat = binary_set_attributes(p, "+p_importpath+", "pre", "/path/to/source", NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->origin != -1);	/* origin field is set */

  stat = binary_set_attributes(p, "+p_importpath+", "pre", "/path/to/source", "Doc", 0, 0);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->doc != -1);	/* origin field is set */

  stat = binary_set_attributes(p, "+p_importpath+", "pre", "/path/to/source", "Doc", 99, 1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->major == 99);
  TEST_ASSERT(p->minor == 1);

  pexl_BinaryAttributes *attrs = binary_get_attributes(p);
  TEST_ASSERT(attrs);
  TEST_ASSERT(0 == strncmp(attrs->importpath, "+p_importpath+", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->prefix, "pre", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->origin, "/path/to/source", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->doc, "Doc", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(p->major == 99);
  TEST_ASSERT(p->minor == 1);
  binary_free_attrs(attrs);
  
  /* Create a second package */

  p2 = binary_new();
  TEST_ASSERT(p2);
  stat = binary_set_attributes(p2, "temp importpath", "temp prefix",
				"temp origin", "temp doc", 100, 101);
  TEST_ASSERT(stat == 0);
  attrs = binary_get_attributes(p2);
  TEST_ASSERT(attrs);
  TEST_ASSERT(0 == strncmp(attrs->importpath, "temp importpath", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->prefix, "temp prefix", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->origin, "temp origin", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->doc, "temp doc", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(p2->major == 100);
  TEST_ASSERT(p2->minor == 101);
  binary_free_attrs(attrs);

  stat = binary_set_attributes(p2, "+importpath+", "+prefix+", "+source+", "Doc for p2", 0, 0);
  TEST_ASSERT(stat == 0);
  attrs = binary_get_attributes(p2);
  TEST_ASSERT(attrs);
  TEST_ASSERT(0 == strncmp(attrs->importpath, "+importpath+", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->prefix, "+prefix+", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->origin, "+source+", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(0 == strncmp(attrs->doc, "Doc for p2", PEXL_MAX_STRINGLEN));
  TEST_ASSERT(p2->major == 0);
  TEST_ASSERT(p2->minor == 0);
  binary_free_attrs(attrs);

  TEST_ASSERT(p2->symtab != NULL);
  TEST_ASSERT(p2->code == NULL);
  TEST_ASSERT(p2->codesize == 0);
  TEST_ASSERT(package_importpath(p2) != -1); /* importpath field is set */
  TEST_ASSERT(package_prefix(p2) != -1);     /* prefix field is set */
  TEST_ASSERT(p2->origin != -1);	     /* origin field is set */
  TEST_ASSERT(p2->doc != -1);	             /* doc field is set */
  TEST_ASSERT(p2->major == 0);
  TEST_ASSERT(p2->minor == 0);
  
  stat = binary_set_attributes(p, NULL, NULL, NULL, NULL, 1, 2);
  TEST_ASSERT(stat == 0);
  attrs = binary_get_attributes(p);
  TEST_ASSERT(attrs);
  TEST_ASSERT(!attrs->importpath);
  TEST_ASSERT(!attrs->prefix);
  TEST_ASSERT(!attrs->origin);
  TEST_ASSERT(!attrs->doc);
  TEST_ASSERT(p->major == 1);
  TEST_ASSERT(p->minor == 2);
  binary_free_attrs(attrs);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Import table tests");

  stat = binary_set_attributes(p, "+p1_importpath+", "p1_pre",
				"p1_/path/to/source", "Doc for p1", 99, 1);
  TEST_ASSERT(stat == 0);
  
  attrs = binary_get_attributes(p);
  printf("pexl_Binary p:\n");
  if (PRINTING) print_binary_attributes(attrs);
  binary_free_attrs(attrs);
  attrs = binary_get_attributes(p2);
  if (PRINTING) {
    printf("pexl_Binary p2:\n");
    print_binary_attributes(attrs);
  }
  binary_free_attrs(attrs);
  
  TEST_ASSERT(p->import_size == IMPORTTABLE_INITSIZE); /* should be original size */
  TEST_ASSERT(p2->import_size == IMPORTTABLE_INITSIZE); /* should be original size */
  TEST_ASSERT(p->import_next == 1);			/* should start at 1 */
  TEST_ASSERT(p2->import_next == 1);			/* should start at 1 */

  TEST_ASSERT(p->imports);	/* should always be non-NULL */
  TEST_ASSERT(p2->imports);	/* should always be non-NULL */
  TEST_ASSERT(p->imports[0].pkg == NULL); /* should be NULL */
  TEST_ASSERT(p2->imports[0].pkg == NULL); /* should be NULL */

  stat = binary_add_import(NULL, NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = binary_add_import(p, NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = binary_add_import(p, "foo", NULL);
  printf("stat = %d\n", stat);
  TEST_ASSERT(stat >= 0);
  TEST_ASSERT(p->import_next == 2); /* next should have advanced */
  if (PRINTING) print_symboltable(p->symtab);
  TEST_ASSERT(p->imports[1].importpath != -1);

  stat = binary_add_import(p, "foo", NULL);
  TEST_ASSERT(stat == PACKAGE_ERR_EXISTS); /* should fail because foo is already in the table */
    
  /* Fill the import table until maximum */
  oldsize = p->import_size;
  for (i = 0; i < IMPORTTABLE_MAXSIZE - 2; i++) {
    asprintf(&str1, "i_%-lu", i);
    assert(str1);
    asprintf(&str2, "p_%-lu", i);
    assert(str2);
    stat = binary_add_import(p, str1, str2);
    if (stat < 0) printf("stat = %d\n", stat);
    assert(stat >= 0);
    free(str1);
    free(str2);
    if (p->import_size != oldsize) {
      printf("import table size increased to %d\n", p->import_size);
      oldsize = p->import_size;
    }
    if (PRINTING && (i == 13)) {
      /* Quick visual check */
      print_importtable(p);
      print_symboltable(p->symtab);
    }
  }
  TEST_ASSERT(true);		// Add to the number of tests passed
  printf("Added %lu entries to the import table\n", i);
  
  stat = binary_add_import(p, "lalalala", "hahahaha");
  TEST_ASSERT(stat == PEXL_ERR_FULL);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexl_Binary table tests");
  
  if (PRINTING) print_packagetable(m);

  p1 = binary_new();
  TEST_ASSERT(p1);

  /* Attempt to add with no importpath set (and an importpath is required) */
  stat = packagetable_add(m, p1);
  TEST_ASSERT(stat == PACKAGE_ERR_IMPORTPATH);

  // Empty importpath is allowed, as long it's a unique key in the table
  stat = binary_set_attributes(p1, "", NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  stat = packagetable_add(m, p1);
  TEST_ASSERT(stat == PACKAGE_OK);
  stat = packagetable_add(m, p1);
  TEST_ASSERT(stat == PACKAGE_ERR_EXISTS);
  stat = packagetable_add(m, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = packagetable_add(NULL, p);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = binary_set_attributes(p1, "ABCDEFGHIJ", NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  stat = packagetable_add(m, p1);
  TEST_ASSERT(stat == PACKAGE_ERR_EXISTS);
  TEST_ASSERT(m->packages[1] == p1); /* should be at index 1 (0 is reserved) */

  TEST_ASSERT(symboltable_get_name(p2->symtab, package_importpath(p2))); /* import path should exist in symtab */
  TEST_ASSERT(str[0] != '\0');       /* precondition: importpath not empty */
  stat = packagetable_add(m, p2);
  TEST_ASSERT(stat >= 0);
  TEST_ASSERT(m->packages[2] == p2); /* next index is 2 */

  stat = packagetable_add(m, p);
  TEST_ASSERT(stat >= 0);

  free(str); str = NULL;

  if (PRINTING) print_packagetable(m);

  TEST_ASSERT(m->next == 4);	 /* we added 3 packages so far */
  TEST_ASSERT(m->capacity == 4); /* previous size was doubled in table expansion */
  
  p3 = binary_new();
  TEST_ASSERT(p3);
  stat = binary_set_attributes(p3, "+importpath+", NULL, "+p3_source+", NULL, 0, 0);
  TEST_ASSERT(stat == 0);

  TEST_ASSERT(p3->symtab != NULL);
  TEST_ASSERT(p3->code == NULL); 
  TEST_ASSERT(p3->codesize == 0);

  if (PRINTING) print_charsettable(p3->cstab);

  /* Now we pause our package table testing to add a couple of charsets */
  Charset cs;
  int status;
  charset_fill_zero(cs.cs);	/* make an empty charset */
  setchar(cs.cs, '\n');
  setchar(cs.cs, ' ');
  setchar(cs.cs, 'A');
  setchar(cs.cs, 'B');
  pexl_Index csi = charsettable_add(p3->cstab);
  TEST_ASSERT(csi >= 0);
  status = charsettable_set_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  status = charsettable_set_simd_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  if (PRINTING) print_charsettable(p3->cstab);
  csi = charsettable_add(p3->cstab);
  charset_fill_one(cs.cs);	/* make a full charset */
  status = charsettable_set_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  status = charsettable_set_simd_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  if (PRINTING) print_charsettable(p3->cstab);

  charset_fill_zero(cs.cs);	/* make an empty charset */
  csi = charsettable_add(p3->cstab);
  status = charsettable_set_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  if (PRINTING) print_charsettable(p3->cstab);

  pexl_Index cstab_cap = p3->cstab->capacity;
  while (csi < cstab_cap - 1) {
    csi = charsettable_add(p3->cstab);
    status = charsettable_set_charset(p3->cstab, csi, &cs);
    TEST_ASSERT(status == OK);
    status = charsettable_set_simd_charset(p3->cstab, csi, &cs);
    TEST_ASSERT(status == OK);
  }
  TEST_ASSERT(p3->cstab->capacity == cstab_cap);
  csi = charsettable_add(p3->cstab);
  TEST_ASSERT(p3->cstab->capacity == 2 * cstab_cap);
  status = charsettable_set_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  status = charsettable_set_simd_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  csi = charsettable_add(p3->cstab);
  TEST_ASSERT(csi == cstab_cap + 1);
  status = charsettable_set_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  status = charsettable_set_simd_charset(p3->cstab, csi, &cs);
  TEST_ASSERT(status == OK);
  
  if (PRINTING) {
    print_symboltable(p3->symtab);
    print_importtable(p3);
    print_packagetable(m);
  }

  TEST_ASSERT(package_importpath(p3) == 0);
  TEST_ASSERT(package_prefix(p3) == -1);    /* checking prefix field is unset */
  TEST_ASSERT(p3->origin == 1);		    /* checking source field (symtab index) */
  TEST_ASSERT(p3->major == 0);		    /* checking major field */
  TEST_ASSERT(p3->minor == 0);		    /* checking minor field */
  
  stat = packagetable_add(m, p3);
  TEST_ASSERT(stat == PACKAGE_ERR_EXISTS); /* should find package with same importpath */

  stat = symboltable_add(p3->symtab, "+p3_importpath+",
			 symbol_global, symbol_ns_data, symbol_type_string,
			 0, 0, NULL);
  TEST_ASSERT(stat == 2);	/* precondition for next test */
  package_importpath(p3) = 2;	/* importpath now unique */
  stat = packagetable_add(m, p3);
  TEST_ASSERT(stat == PACKAGE_OK);

  if (PRINTING) print_packagetable(m);

  TEST_SECTION("pexl_Binary table lookup tests");

  probe = packagetable_lookup(m, "");
  TEST_ASSERT(!probe);
  probe = packagetable_lookup(m, "+importpath+");
  TEST_ASSERT(probe == 2);	/* this package is in slot 2 */
  pkg = packagetable_get(m, probe);
  TEST_ASSERT(pkg == p2);
  probe = packagetable_lookup(m, "+p3_importpath+");
  TEST_ASSERT(probe == 4);
  pkg = packagetable_get(m, probe);
  TEST_ASSERT(pkg == p3);
  probe = packagetable_lookup(m, "helloworld");
  TEST_ASSERT(!probe);		/* no package with this importpath */

  fprintf(stderr, "Expect a warning here:\n");
  probe = packagetable_lookup(NULL, "");
  TEST_ASSERT(probe == PEXL_ERR_NULL); /* pkg table arg cannot be NULL */

  TEST_SECTION("Filling the package table to max size");

  if (!FUZZING) {
    printf("Skipping this test because FUZZING is disabled\n");
    goto skip;
  }

  for (i = m->next; i < PACKAGETABLE_MAXSIZE; i++) {
    stat = asprintf(&str, "%ld", i);
    assert(stat >= 0);	/* asprintf failed! */
    pkg = binary_new();
    assert(pkg);
    stat = binary_set_attributes(pkg, str, NULL, NULL, NULL, 0, 0);
    assert(stat == 0);
    free(str);
    stat = packagetable_add(m, pkg);
    if (stat < 0) {
      printf("packagetable_add returned %d for importpath %lu\n", stat, i);
    }
    assert(stat >= 0);	/* package add failed! */
  }
  TEST_ASSERT(m->next == PACKAGETABLE_MAXSIZE);
  TEST_ASSERT(m->capacity == PACKAGETABLE_MAXSIZE);

  /* Try one more package to make sure that PACKAGETABLE_MAXSIZE is respected */
  stat = asprintf(&str, "%ld", i);
  TEST_ASSERT(stat >= 0);
  pkg = binary_new();
  TEST_ASSERT(pkg);
  stat = binary_set_attributes(pkg, str, NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  free(str);
  stat = packagetable_add(m, pkg);
  TEST_ASSERT(stat == PEXL_ERR_FULL);

  /* Test packagetable_remove */
  TEST_ASSERT(pkg);		 /* pre-condition for next test */
  TEST_ASSERT(pkg->number == 0); /* pre-condition for next test */

  stat = packagetable_remove(m, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL); /* package arg required */
  stat = packagetable_remove(NULL, pkg);
  TEST_ASSERT(stat == PEXL_ERR_NULL); /* machine arg required */
  stat = packagetable_remove(NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL); /* both args required */

  binary_free(pkg);  

  TEST_ASSERT(p3->number == 4);	/* pre-condition for next test */
  stat = packagetable_remove(m, p3);
  TEST_ASSERT(stat == PACKAGE_OK);
  probe = packagetable_lookup(m, "+prefix+");
  TEST_ASSERT(probe == 0); /* should not find package, which we removed */

  binary_free(p3);
  
  assert(m->packages[0] == NULL); // reserved for vm use
  for (i = 1; i < m->next; i++)
    if (i == 4) assert(m->packages[i] == NULL); /* checking the entry for p3 itself */
    else assert(m->packages[i] != NULL);        /* checking all other entries */
  TEST_ASSERT(true);		// Add to the number of tests passed

  if (PRINTING) print_packagetable_stats(m);

  if (PRINTING)
    printf("Freeing all the packages we created to put in the package table...\n");
  packagetable_free_packages(m);
  packagetable_free(m);

 skip:
  
  TEST_END();
}

