/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  expressiontest.c  TESTING expression.c                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"

#include "../test.h"

#define YES 1
#define NO 0

int main(int argc, char **argv) {

  pexl_Context *C = NULL;
  
  size_t len;
  char *toolong;
  
  int stat;
  Charset cs;
  pexl_Expr *exp, *tstr, *calltree;
  pexl_Expr *t1, *t2, *t3, *t4; 
  pexl_Expr *A, *B, *S;

  pexl_PackageTable *m; 
  pexl_Binary *pkg;
  pexl_Env toplevel, env, g;
  SymbolTable *st;
  pexl_Ref ref, refA, refB, refS;

  TEST_START(argc, argv);
  TEST_SECTION("Building expressions");

  C = pexl_new_Context();
  TEST_ASSERT(C);
  TEST_ASSERT(C->bt);
  toplevel = 0;			// Root env
  
  /* pexl_Error checking */

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_match_set(C, NULL, 6);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here (args must be in order):\n");
  exp = pexl_match_range(C, 'B','A');
  TEST_ASSERT(!exp);

  toolong = malloc(EXP_MAXSTRING + 2);
  memset(toolong, 'A', EXP_MAXSTRING + 2);
  toolong[EXP_MAXSTRING + 1] = '\0';
  TEST_ASSERT(strlen(toolong) == EXP_MAXSTRING + 1); // precondition for next test
  tstr = pexl_match_string(C, toolong);
  TEST_ASSERT(!tstr);		// this string is too long

  /* Now make it exactly EXP_MAXSTRING long */
  toolong[EXP_MAXSTRING] = '\0';
  tstr = pexl_match_string(C, toolong);
  TEST_ASSERT(tstr);	      // this is the longest acceptable string
  pexl_free_Expr(tstr);
  free(toolong);

  tstr = pexl_match_string(C, "X");
  TEST_ASSERT(tstr->len == 1);	// should produce a 1-node expression tree

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_seq(C, NULL, tstr);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_seq(C, tstr, NULL);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_seq(C, NULL, NULL);
  TEST_ASSERT(!exp);
  
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_choice(C, NULL, tstr);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_choice(C, tstr, NULL);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_choice(C, NULL, NULL);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_neg_lookahead(C, NULL);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_lookahead(C, NULL);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_lookbehind(C, NULL);
  TEST_ASSERT(!exp);

  pexl_Expr *temp = pexl_match_string(C, "Hello");
  TEST_ASSERT(temp);
  exp = pexl_repeat(C, temp, 2, 1); // max < min
  TEST_ASSERT(!exp);

  exp = pexl_repeat(C, temp, MAX_REPETITIONS, MAX_REPETITIONS+1); // max too large
  TEST_ASSERT(!exp);

  exp = pexl_repeat(C, temp, MAX_REPETITIONS+1, MAX_REPETITIONS+1);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_repeat(C, NULL, -1, 1); // min < 0
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_repeat(C, NULL, 0, -1); // max < 0
  TEST_ASSERT(!exp);
  pexl_free_Expr(temp);

  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_capture(C, "foo", NULL);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_capture(C, NULL, tstr);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_capture(C, NULL, NULL);
  TEST_ASSERT(!exp);

  ref = -1;
  fprintf(stderr, "Expect warning here:\n");
  exp = pexl_call(C, ref);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect warning here:\n");
  stat = to_charset(tstr->node, NULL);
  TEST_ASSERT(stat == 0);

  fprintf(stderr, "Expect warning here:\n");
  stat = to_charset(NULL, &cs);
  TEST_ASSERT(stat == 0);
  
  stat = to_charset(tstr->node, &cs);
  TEST_ASSERT(stat == 1);
  
  t1 = pexl_match_any(C, 1);	// TAny
  stat = to_charset(t1->node, &cs);
  TEST_ASSERT(stat == 1);	// a single TAny node can be converted to a character set

  pexl_free_Expr(t1);
  pexl_free_Expr(tstr);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Building primitive patterns");

  tstr = pexl_match_string(C, "hello");
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  pexl_free_Expr(tstr);

  tstr = pexl_match_string(C, "");
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  TEST_ASSERT(tstr->node->tag == TTrue); /* the 1 node is TTrue, which matches epsilon */
  pexl_free_Expr(tstr);

  tstr = pexl_match_any(C, 1);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  TEST_ASSERT(tstr->node->tag == TAny); /* the node is TAny, which matches 1 byte */
  pexl_free_Expr(tstr);

  tstr = pexl_match_any(C, 0);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  TEST_ASSERT(tstr->node->tag == TTrue); /* the 1 node is TTrue, which matches epsilon */
  pexl_free_Expr(tstr);

  tstr = pexl_match_any(C, 4);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 7);	/* should produce 7-node tree */
  TEST_ASSERT(tstr->node->tag == TSeq); /* the root node is Tseq */
  pexl_free_Expr(tstr);

  tstr = pexl_match_any(C, -2);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 4);	/* should produce this size tree */
  TEST_ASSERT(tstr->node->tag == TNot); /* the root node is TNot */
  pexl_free_Expr(tstr);

  tstr = pexl_fail(C);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 1);	/* should produce this size tree */
  TEST_ASSERT(tstr->node->tag == TFalse); /* the root node is TFalse */
  pexl_free_Expr(tstr);

  tstr = pexl_match_epsilon(C);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 1);	/* should produce this size tree */
  TEST_ASSERT(tstr->node->tag == TTrue); /* the root node is TTrue */
  pexl_free_Expr(tstr);

  t3 = pexl_match_set(C, "abcdef", 6);
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t3->node->tag == TSet); /* tag should be TSet */
  pexl_free_Expr(t3);

  t4 = pexl_match_set(C, "", 0);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t4->node->tag == TSet); /* tag should be TSet */
  pexl_free_Expr(t4);

  t3 = pexl_match_range(C, 'A','Z');
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t3->node->tag == TSet); /* tag should be TSet */
  pexl_free_Expr(t3);

  printf("*** Testing range(0, 255)\n");
  t4 = pexl_match_range(C, 0, 255);
  TEST_ASSERT(t4);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t4->node->tag == TSet); /* tag should be TSet */
  pexl_free_Expr(t4);

  t4 = pexl_match_range(C, 10, 10);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t4->node->tag == TSet); /* tag should be TSet */
  pexl_free_Expr(t4);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'seq'");

  t1 = pexl_match_string(C, "ab");
  t2 = pexl_match_string(C, "c");
  tstr = pexl_seq(C, t1, t2);
  pexl_print_Expr(tstr, 0, C);
  TEST_ASSERT(tstr->len == 3);	/* wrong size tree */

  t3 = pexl_match_any(C, 2);
  t4 = pexl_seq(C, t3, tstr);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 7);	/* wrong size tree */

  TEST_SECTION("Testing boolean optimizations");

  pexl_free_Expr(t3); pexl_free_Expr(t4);
  t3 = pexl_fail(C);		/* False */
  t4 = pexl_seq(C, t3, t1);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 1);	/* wrong size tree */
  TEST_ASSERT(t4->node->tag == TFalse); /* optimization produces TFalse */
  pexl_free_Expr(t3); pexl_free_Expr(t4);

  t3 = pexl_match_epsilon(C);		/* True */
  t4 = pexl_seq(C, t3, t1);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 1);	/* wrong size tree */
  pexl_free_Expr(t3); pexl_free_Expr(t4);

  t3 = pexl_match_epsilon(C);		/* True */
  t4 = pexl_seq(C, t1, t3);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 1);	/* wrong size tree */
  pexl_free_Expr(t3); pexl_free_Expr(t4);

  pexl_free_Expr(t1); pexl_free_Expr(t2);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'choice'");

  t1 = pexl_match_string(C, "ab");
  t2 = pexl_match_string(C, "c");
  t3 = pexl_choice(C, t1, t2);
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == 3);	/* wrong size tree */
  TEST_ASSERT(t3->node->tag == TChoice); /* should get TChoice */
  pexl_free_Expr(t3);

  t3 = pexl_match_any(C, 2);
  t4 = pexl_choice(C, t3, tstr);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4->len == 7);	/* wrong size tree */
  TEST_ASSERT(t4->node->tag == TChoice); /* should get TChoice */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); pexl_free_Expr(t4); pexl_free_Expr(tstr);

  printf("Testing charset optimizations\n");

  t1 = pexl_match_set(C, "A", 1);
  t2 = pexl_match_set(C, "B", 1);
  t3 = pexl_choice(C, t1, t2);
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == 3);	/* wrong size tree */
  TEST_ASSERT(t3->node->tag == TSet); /* optimization produces another TSet */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); 

  t1 = pexl_match_set(C, "ABC", 1);
  t2 = pexl_match_set(C, "B", 1);
  t3 = pexl_choice(C, t1, t2);
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == 3);	/* wrong size tree */
  TEST_ASSERT(t3->node->tag == TSet); /* optimization produces another TSet */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); 

  /* The nofail optimization is now delayed until compilation */
/*   printf("Testing nofail optimizations\n"); */

/*   t1 = pexle_from_boolean(C, 1);		/\* "Be true; you need not fail." *\/ */
/*   t2 = pexl_match_string(C, "Hello"); */
/*   t3 = pexl_choice(C, t1, t2); */
/*   pexl_print_Expr(t3, 0, C); */
/*   TEST_ASSERT(t3->len == 1, "wrong size tree"); */
/*   TEST_ASSERT(t3->node->tag == TTrue, "optimization produces TTrue"); */
/*   pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);  */

/*   t1 = pexl_match_string(C, "");		/\* "Be true; you need not fail." *\/ */
/*   t2 = pexle_from_boolean(C, 0); */
/*   t3 = pexl_choice(C, t1, t2); */
/*   pexl_print_Expr(t3, 0, C); */
/*   TEST_ASSERT(t3->len == 1, "wrong size tree"); */
/*   TEST_ASSERT(t3->node->tag == TTrue, "optimization produces TTrue"); */
/*   pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);  */

/*   t1 = pexl_match_string(C, "A"); */
/*   t2 = pexle_from_boolean(C, 0); */
/*   t3 = pexl_choice(C, t1, t2); */
/*   pexl_print_Expr(t3, 0, C); */
/*   TEST_ASSERT(t3->len == 1, "wrong size tree"); */
/*   TEST_ASSERT(t3->node->tag == TChar, "optimization produces TChar, not TChoice"); */
/*   pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'not'");

  t1 = pexl_match_string(C, "A");
  t2 = pexl_neg_lookahead(C, t1);
  pexl_print_Expr(t2, 0, C);
  TEST_ASSERT(t2->len == 1 + t1->len); /* wrong size tree */
  TEST_ASSERT(t2->node->tag == TNot);  /* Tnot */
  pexl_free_Expr(t1); pexl_free_Expr(t2);

  /* No optimizations here */
  t1 = pexl_match_epsilon(C);
  t2 = pexl_neg_lookahead(C, t1);
  pexl_print_Expr(t2, 0, C);
  TEST_ASSERT(t2->len == 1 + t1->len); /* wrong size tree */
  TEST_ASSERT(t2->node->tag == TNot);  /* Tnot */
  pexl_free_Expr(t1); pexl_free_Expr(t2);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookahead'");

  t1 = pexl_match_string(C, "A");
  t2 = pexl_lookahead(C, t1);
  pexl_print_Expr(t2, 0, C);
  TEST_ASSERT(t2->len == 1 + t1->len); /* wrong size tree */
  TEST_ASSERT(t2->node->tag == TAhead); /* TAhead */
  pexl_free_Expr(t1); pexl_free_Expr(t2);

  /* No optimizations here */
  t1 = pexl_match_epsilon(C);
  t2 = pexl_lookahead(C, t1);
  pexl_print_Expr(t2, 0, C);
  TEST_ASSERT(t2->len == 1 + t1->len); /* wrong size tree */
  TEST_ASSERT(t2->node->tag == TAhead); /* TAhead */
  pexl_free_Expr(t1); pexl_free_Expr(t2);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'rep'");

#define repetition_node_size(min,max,expsize) \
  ((max == 0) ? ((min+1)*(expsize+1)) : ((max-min)*(expsize+1) + max*(expsize+1)))

  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 0);       // zero or more
  pexl_print_Expr(t2, 0, C);
  TEST_ASSERT(t2->len == repetition_node_size(0, 0, t1->len));
  TEST_ASSERT(t2->node->tag == TRep);  // TRep is the "or more" expr
  t3 = pexl_repeat(C, t1, 3, 0);       // 3 or more
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == repetition_node_size(3, 0, t1->len));
  TEST_ASSERT(t3->node->tag == TSeq);
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);
  
  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 1);         // at most one
  pexl_print_Expr(t2, 0, C);
  TEST_ASSERT(t2->len == repetition_node_size(0, 1, t1->len) - 1);
  TEST_ASSERT(t2->node->tag == TChoice);
  t3 = pexl_repeat(C, t1, 0, 3);	 // at most 3
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3->len == repetition_node_size(0, 3, t1->len) - 1);
  TEST_ASSERT(t3->node->tag == TChoice);
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);

  t1 = pexl_match_string(C, "X");
  t2 = pexl_repeat(C, t1, 7, 7);         // exactly 7
  pexl_print_Expr(t2, 0, C);
  //printf("rep(7, 7): t1->len is %d, t2->len is %d\n", t1->len, t2->len);
  TEST_ASSERT(t2->len == repetition_node_size(7, 7, t1->len));
  TEST_ASSERT(t2->node->tag == TSeq);
  t3 = pexl_repeat(C, t1, 2, 13);
  pexl_print_Expr(t3, 0, C);
  //printf("rep(2, 13): t1->len is %d, t3->len is %d\n", t1->len, t3->len);
  TEST_ASSERT(t3->len == repetition_node_size(2, 13, t1->len));
  TEST_ASSERT(t3->node->tag == TSeq);
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);

  t1 = pexl_match_string(C, "X");
  t2 = pexl_repeat(C, t1, 3, 5);         // at least 3, at most 5
  pexl_print_Expr(t2, 0, C);
  //printf("rep(3, 5): t1->len is %d, t2->len is %d\n", t1->len, t2->len);
  TEST_ASSERT(t2->len == repetition_node_size(3, 5, t1->len));
  TEST_ASSERT(t2->node->tag == TSeq);
  t3 = pexl_repeat(C, t1, 10, 11);
  pexl_print_Expr(t3, 0, C);
  //printf("rep(10, 11): t1->len is %d, t3->len is %d\n", t1->len, t3->len);
  TEST_ASSERT(t3->len == repetition_node_size(10, 11, t1->len));
  TEST_ASSERT(t3->node->tag == TSeq);
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookbehind'");

  printf("Look back at string literal\n");
  t1 = pexl_match_string(C, "A");
  t2 = pexl_lookbehind(C, t1);
  TEST_ASSERT(t2->len == 2);	/* wrong size tree */
  TEST_ASSERT(t2->node->tag == TBehind); /* TBehind */
  pexl_print_Expr(t2, 0, C);
  t3 = pexl_lookbehind(C, t1);
  TEST_ASSERT(t3->len == 2);	/* wrong size tree */
  TEST_ASSERT(t3->node->tag == TBehind); /* TBehind */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);

  printf("Look back at \"A\"? which has variable length\n");
  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 1); // at most 1
  pexl_print_Expr(t2, 0, C);
  t3 = pexl_lookbehind(C, t2);
  TEST_ASSERT(t3);		/* the check for fixedlen happens later */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);

  confess("expressiontest",
	  "\n\nTODO: test lookbehind at a bound pattern\n");
/*   stat = bind_entrypoint(env, "LB_TEST", vis, t3, NULL); */
/*   TEST_ASSERT(stat == 0, "bind should work"); */
/*   TEST_ASSERT(env_len(env) == 1, "first binding made"); */

/*   printf("This tree has a lookbehind to a variable-length target, which will fail during compilation: "); */
/*   pexl_print_Expr(t3, 0, C); */

  printf("Look back at [A-Z]+ which has variable length\n");
  t1 = pexl_match_range(C, 'A', 'Z');
  t2 = pexl_repeat(C, t1, 1, 0); // 1 or more
  t3 = pexl_lookbehind(C, t2);
  pexl_print_Expr(t3, 0, C);
  TEST_ASSERT(t3);		/* we do not check (now) for the acceptable lookbehind targets until compilation */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);

  printf("Look back at \"AB\"/\"CD\" which has length 2\n");
  t1 = pexl_match_string(C, "AB");
  t2 = pexl_match_string(C, "CD");
  t3 = pexl_choice(C, t1, t2);
  t4 = pexl_lookbehind(C, t3);
  pexl_print_Expr(t4, 0, C);
  TEST_ASSERT(t4);		/* we do not check (now) for the acceptable lookbehind targets until compilation */
  TEST_ASSERT(t4->node->tag == TBehind); /* TBehind */
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); pexl_free_Expr(t4);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexlCapture");

  t1 = pexl_match_string(C, "AB");
  TEST_ASSERT(t1);

  fprintf(stderr, "Expect warning here:\n");
  t2 = pexl_capture(NULL, "hi", t1);
  TEST_ASSERT(!t2);

  t2 = pexl_capture(C, "hi", t1);
  TEST_ASSERT(t2);
  pexl_print_Expr(t2, 0, C);

  t3 = pexl_seq(C, t2, t2);
  TEST_ASSERT(t3);
  pexl_print_Expr(t3, 0, C);

  t4 = pexl_capture(C, "foo", t3);
  TEST_ASSERT(t4);
  pexl_print_Expr(t4, 0, C);

  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); pexl_free_Expr(t4);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Constant captures");

  fprintf(stderr, "Expect warning here:\n");
  pexl_Expr *consttree = pexl_insert(C, NULL, NULL);
  TEST_ASSERT(!consttree);

  pexl_free_Expr(consttree);
  fprintf(stderr, "Expect warning here:\n");
  consttree = pexl_insert(C, NULL, "constant string");
  TEST_ASSERT(!consttree);

  pexl_free_Expr(consttree);
  fprintf(stderr, "Expect warning here:\n");
  consttree = pexl_insert(C, "name234", NULL);
  TEST_ASSERT(!consttree);

  pexl_free_Expr(consttree);
  fprintf(stderr, "Expect warning here:\n");
  consttree = pexl_insert(C, "", "constant string value");
  TEST_ASSERT(!consttree);

  pexl_free_Expr(consttree);
  consttree = pexl_insert(C, "name", "constant string");
  TEST_ASSERT(consttree);
  const char *tmp = context_retrieve(C, consttree->node->b.n, &len);
  TEST_ASSERT(len == 4);
  TEST_ASSERT(memcmp(tmp, "name", len) == 0); // "name" has not been interned
  tmp = context_retrieve(C, consttree->node->a.stringtab_handle, &len);
  TEST_ASSERT(len == strlen("constant string"));
  TEST_ASSERT(memcmp(tmp, "constant string", len) == 0); // not interned

  pexl_free_Expr(consttree);
  consttree = pexl_insert(C, "name234", "");
  TEST_ASSERT(consttree);
  tmp = context_retrieve(C, consttree->node->b.n, &len);
  TEST_ASSERT(len == strlen("name234"));
  TEST_ASSERT(memcmp(tmp, "name234", len) == 0); /* name string not interned */
  TEST_ASSERT(!consttree->node->a.stringtab_handle); /* expected null data string */
  tmp = context_retrieve(C, consttree->node->a.stringtab_handle, &len);
  TEST_ASSERT(len == 0);
  TEST_ASSERT(strcmp(tmp, "") == 0); /* null data string not repeatedly interned */

  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("References");

  m = packagetable_new(1);
  TEST_ASSERT(m);

  TEST_ASSERT(toplevel == 0);	/* pre-condition for next tests */
  TEST_ASSERT(C);		/* pre-condition for next tests */

  env = env_new(C->bt, toplevel);
  TEST_ASSERT(env);

  st = symboltable_new(1, 1);
  TEST_ASSERT(st);
  /* Add empty string (here represented by NULL: */
  stat = symboltable_add(st, NULL, 1, 0, 1, 0, 0, NULL);
  TEST_ASSERT(stat == 0);	/* returned index should be 0 */
  stat = symboltable_add(st, "foo", 1, 0, 1, 0, 0, NULL);
  TEST_ASSERT(stat == 1);	/* returned index should be 1 */

  pkg = binary_new();
  TEST_ASSERT(pkg);		/* Ok to have importpath, prefix, source be empty string */
  binary_free(pkg);

  ref = env_bind(C->bt, env, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  ref = env_bind(C->bt, env, 1, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == 1);	/* second binding is at index 1 */

  calltree = pexl_call(C, ref);
  pexl_print_Expr(calltree, 0, C);
  TEST_ASSERT(calltree);
  TEST_ASSERT(calltree->node->a.bt == C->bt);
  TEST_ASSERT(calltree->node->b.index == ref);
  pexl_free_Expr(calltree);


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Backreferences");

  
  fprintf(stderr, "Expect warning here:\n");
  pexl_Expr *backreftree = pexl_backref(C, NULL);
  TEST_ASSERT(!backreftree);	/* cannot pass NULL */

  backreftree = pexl_backref(C, "target");
  TEST_ASSERT(backreftree);	/* backreference should work */
  pexl_print_Expr(backreftree, 0, C);
  pexl_free_Expr(backreftree);

  backreftree = pexl_backref(C, "foo");
  TEST_ASSERT(backreftree);	/* backreference should work; does not ensure existence of 'foo' */
  pexl_print_Expr(backreftree, 0, C);
  


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Non-recursive block using anonymous exps");

  /* New env for grammar */
  g = env_new(C->bt, env);
  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refS = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  print_BindingTable(C);

  A = pexl_call(C, refS);	/* A -> S */
  printf("A is: "); pexl_print_Expr(A, 0, NULL);
  S = pexl_match_epsilon(C);	/* S -> true */
  printf("S is: "); pexl_print_Expr(S, 0, NULL);

  print_BindingTable(C);

  pexl_free_Expr(A);
  pexl_free_Expr(S);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Recursive block using anonymous exps");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  g = env_new(C->bt, env);
  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  A = pexl_call(C, ref);		/* A -> A */
  pexl_print_Expr(A, 0, NULL);
  printf("pexl_Expr *at refA: "); pexl_print_Expr(A, 0, NULL);
  pexl_free_Expr(A);
  
  printf("-----\n");

  printf("Trying mutually left-recursive rules that are not accessible from start"); 
  g = env_new(C->bt, env);
  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refB = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refB));
  A = pexl_call(C, refB);		/* A -> B */
  B = pexl_call(C, refA);		/* B -> A */

  printf("Reminder: expression building does not check for left recursion.\n");
  
  printf("pexl_Expr *at refA: "); pexl_print_Expr(A, 0, NULL);
  printf("pexl_Expr *at refB: "); pexl_print_Expr(B, 0, NULL);

  pexl_free_Expr(A); pexl_free_Expr(B);
  
  symboltable_free(st);
  packagetable_free(m);
  pexl_free_Context(C);
  

  puts("\n\n");
  confess("TODO", "Write tests for the new pexl_match_complement()");
  puts("");
  

  TEST_END();
}

