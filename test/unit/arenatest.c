/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  arenatest.c                                                              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdlib.h>
#include "../test.h"

#include "arena.h"

#define PRINTING 0
#define FUZZING 0

#define PRINT(a) do {					\
    if (PRINTING) {					\
      printf("Arena " #a ": start = %p, end = %p\n",	\
	     a.start, a.end);				\
    }							\
  } while (0);


int main(int argc, char **argv) {

  TEST_START(argc, argv);
  printf("ARENA_MINSIZE is %d bytes\n", ARENA_MINSIZE);

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Error checking");

  void *tmp, *tmp2;
  arena save, a0, a1;

  a0 = arena_create(0);
  PRINT(a0);
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);
  
  arena_destroy(&a0);
  PRINT(a0);
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);

  // No warning, because arena arg is not valid
  arena_destroy(NULL);

  // No warning, because arena itself is not valid
  arena_destroy(&a0);
  // Ensure that a0 did not change
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);

  printf("Expect a warning for invalid arg:\n");
  tmp = arena_allocate(&a0, 1, 1, 1, 0);
  // Ensure that a0 did not change
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);
  // Ensure appropriate return value NULL
  TEST_ASSERT_NULL(tmp);

  printf("Expect a warning for invalid arg:\n");
  tmp = arena_allocate(&a0, 1, 2, 1, 0);
  // Ensure that a0 did not change
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);
  // Ensure appropriate return value NULL
  TEST_ASSERT_NULL(tmp);

  a0 = arena_create(0);
  TEST_ASSERT(arena_size(a0) == 0);
  save = a0;

  // Both size and count are zero
  printf("Expect a warning for invalid arg:\n");
  tmp = arena_allocate(&a0, 0, 0, 1, 0);
  // Ensure that a0 did not change
  TEST_ASSERT(a0.start == save.start);
  TEST_ASSERT(a0.end == save.end);
  // Ensure appropriate return value when asking for zero bytes
  TEST_ASSERT_NULL(tmp);

  // Size is zero, but not count: we ask for zero bytes
  printf("Expect a warning for invalid arg:\n");
  tmp = arena_allocate(&a0, 0, 123, 1, 0);
  PRINT(a0);
  // Ensure that a0 did not change
  TEST_ASSERT(a0.start == save.start);
  TEST_ASSERT(a0.end == save.end);
  // Asking for zero bytes from a 0-sized arena returns NULL
  TEST_ASSERT_NULL(tmp);

  // Size is nonzero, but count is zero: we ask for zero bytes
  printf("Expect a warning for invalid arg:\n");
  tmp = arena_allocate(&a0, 200, 0, 1, 0);
  PRINT(a0);
  // Ensure that a0 did not change
  TEST_ASSERT(a0.start == save.start);
  TEST_ASSERT(a0.end == save.end);
  // Asking for zero bytes from a 0-sized arena returns NULL
  TEST_ASSERT_NULL(tmp);

  // Ask for 1 byte from 0-sized arena which is an invalid arena
  printf("Expect a warning for invalid arg:\n");
  tmp = arena_allocate(&a0, 1, 1, 1, ARENA_SOFTFAIL);
  PRINT(a0);
  // Ensure that a0 did not change
  TEST_ASSERT(a0.start == save.start);
  TEST_ASSERT(a0.end == save.end);
  // Asking for too much returns NULL (with ARENA_SOFTFAIL)
  TEST_ASSERT_NULL(tmp);

  arena_destroy(&a0);
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Very small arenas");

  a0 = arena_create(1);
  save = a0;
  PRINT(a0);
  TEST_ASSERT(arena_size(a0) == ARENA_MINSIZE);

  tmp = arena_allocate(&a0, 0, 0, 0, 0);
  PRINT(a0);
  // Ensure that a0 did not change
  TEST_ASSERT(a0.start == save.start);
  TEST_ASSERT(a0.end == save.end);
  // Asking for zero bytes returns NULL
  TEST_ASSERT_NULL(tmp);

  // Repeatedly ask for one byte from a one-byte sized arena
  a1 = a0;
  int limit = arena_size(a1);
  for (int i = 0; i < limit; i++) {
    tmp = arena_allocate(&a1, 1, 1, 1, 0);
    PRINT(a0);
    PRINT(a1);
    // Check the change in a1
    TEST_ASSERT((a1.start - a0.start) == (ptrdiff_t) i+1);
    // NOTE: The test below exposes the impl of the ORIGINAL flag
    TEST_ASSERT(a1.end == a0.end-1);
    // Asking for 1 byte returns a valid pointer
    TEST_ASSERT_NOT_NULL(tmp);
    TEST_ASSERT(tmp == a1.start - 1);
  }

  // Ask for another byte, when the arena is full
  printf("Expect a warning here, that the arena is out of space:\n");
  tmp2 = arena_allocate(&a1, 1, 1, 1, ARENA_SOFTFAIL);
  PRINT(a1);
  // Ensure that a1 did not change
  TEST_ASSERT(a1.start == a0.start+arena_size(a0));
    // NOTE: The test below exposes the impl of the ORIGINAL flag
  TEST_ASSERT(a1.end == a0.end-1);
  TEST_ASSERT_NULL(tmp2);

  save = a1;
  printf("Expect a warning here, that a derived arena cannot be freed:\n");
  arena_destroy(&a1);
  PRINT(a1);
  // Ensure that a1 did not change
  TEST_ASSERT(a1.start == save.start);
  TEST_ASSERT(a1.end == save.end);
  arena_destroy(&a0);
  TEST_ASSERT_NULL(a0.start);
  TEST_ASSERT_NULL(a0.end);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Medium sized arenas");

  static const size_t max_arena_size_iter = FUZZING ? 30000 : 1000;

  for (size_t i = 1; i < max_arena_size_iter; i++) {
    a0 = arena_create(i);
    TEST_ASSERT((size_t) arena_size(a0) == (((i-1)/16 + 1) * 16));
    arena_destroy(&a0);
  }

  int combo_count = 0;
  int alloc_count = 0;
  int chunks, max_chunks;
  size_t chunksize, max_chunksize;

  // Set FUZZING to 0 in order to run in a memory-limited process,
  // such as in a container.
  static const int MAX_CHUNKS = FUZZING ? 1305 : 368;
  static const size_t LIMIT = (size_t) (FUZZING ? 1<<30 : 1<<24);

  for (chunks = 1; chunks < MAX_CHUNKS; chunks += 131) {
    for (chunksize = 137; (chunks * chunksize) < LIMIT; chunksize *= 79) {
      a0 = arena_create(chunks * chunksize);
      arena a0orig = a0;
      a1 = arena_create(chunks * chunksize);
      arena a1orig = a1;
      TEST_ASSERT(arena_size(a0) == arena_size(a1));
      if (PRINTING) printf("arena size = %zu\n", arena_size(a0));
        for (int i = 0; i < chunks; i++) {
	  // Request chunksize chunks of 1 byte
	  tmp = arena_allocate(&a0, 1, chunksize, 1, 0);
	  TEST_ASSERT((a0.start - a0orig.start) == (ptrdiff_t) (chunksize * (i+1)));
	  TEST_ASSERT_NOT_NULL(tmp);
	  memset(tmp, 65, chunksize); // Just to know we can write into tmp
	  alloc_count++;
	  // Request 1 chunk of chunksize bytes
	  tmp = arena_allocate(&a1, chunksize, 1, 1, 0);
	  TEST_ASSERT((a1.start - a1orig.start) == (ptrdiff_t) (chunksize * (i+1)));
	  TEST_ASSERT_NOT_NULL(tmp);
	  memset(tmp, 66, chunksize); // Just to know we can write into tmp
	  alloc_count++;
	}
	arena_destroy(&a0orig);
	arena_destroy(&a1orig);
	max_chunksize = chunksize;
	max_chunks = chunks;
	combo_count++;
    } // for various chunksizes
  } // for various numbers of chunks
  size_t largest_arena_size = max_chunks * max_chunksize;
  printf("%d combinations of chunks and chunksizes tested\n", combo_count);
  printf("%d successful allocations made\n", alloc_count);
  printf("  max number of chunks = %d\n", max_chunks);
  printf("  max chunksize = %zu bytes (%.1lf KB)\n",
	 max_chunksize, (double) max_chunksize / 1024);
  printf("  max arena size requested = %zu bytes (%.1lf MB)\n",
	 largest_arena_size, (double) largest_arena_size / (1024 * 1024));
  printf("  max arena memory held at once (two at max size) = %zu bytes (%.1lf MB)\n",
	 (2 * largest_arena_size), (double) (2 * largest_arena_size) / (1024 * 1024));

  puts("");
  // Finish by creating one large arena, doing nothing with it, then freeing it
  static const size_t bigsize = FUZZING ? (1<<30) : (1<<22);
  arena a0orig = arena_create(bigsize);
  TEST_ASSERT(arena_size(a0orig) >= (ptrdiff_t)bigsize);
  printf("Created a single arena of size %.1lf MB\n", (double)bigsize / (1024 * 1024));
  a0 = a0orig;
  // Allocate from a0 here, for additional testing?

  arena_destroy(&a0orig);

  TEST_END();
}
