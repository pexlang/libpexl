Saturday, December 24, 2022

Hardware Overview:

  Model Name:	MacBook Pro
  Model Identifier:	MacBookPro12,1
  Processor Name:	Dual-Core Intel Core i5
  Processor Speed:	2.9 GHz
  Number of Processors:	1
  Total Number of Cores:	2
  L2 Cache (per Core):	256 KB
  L3 Cache:	3 MB
  Hyper-Threading Technology:	Enabled
  Memory:	8 GB
  System Firmware Version:	430.140.3.0.0
  SMC Version (system):	2.28f7

~/Projects/libpexl/test/unit$ arch
i386
~/Projects/libpexl/test/unit$ uname -a
Darwin Jamies-Compabler.local 20.6.0 Darwin Kernel Version 20.6.0: Tue Jun 21 20:50:28 PDT 2022; root:xnu-7195.141.32~1/RELEASE_X86_64 x86_64
~/Projects/libpexl/test/unit$ echo $CC
clang
~/Projects/libpexl/test/unit$ ./findtest 
$$$ START ./findtest ------------------------------------------------------------

$$$ SECTION Bad arguments
✔✔✔✔✔✔✔✔✔✔✔✔
$$$ SECTION Empty and full charsets
 []

✔✔✔ [(00-ff)]

✔✔✔
$$$ SECTION Single character charsets
 [(41)]

✔✔✔✔✔✔✔✔✔✔ [(4a)]

✔✔✔✔ [(5a)]

✔✔✔✔ [(ff)]

✔✔✔✔
$$$ SECTION Charsets with between 1 and 8 characters
 [(41)(5a)]

✔✔✔✔✔ [(4a-51)]

✔✔✔✔
$$$ SECTION Charsets with between 8 and 255 characters
 [(4a-51)(ff)]

✔✔✔✔✔✔✔
$$$ SECTION Finding newlines in /usr/dict/words
 [(0a)]

Charset has one char: 0x0A
Non-simd processing of file 80 times took    471.5 ms (     5.9 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 235886
✔SIMD processing of file 80 times took    982.0 ms (    12.3 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 235886
✔
** SIMD SPEEDUP:   0.5x **


$$$ SECTION Finding @ in /usr/dict/words
 [(40)]

Charset has one char: 0x40
Non-simd processing of file 80 times took     12.2 ms (     0.2 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 0
✔SIMD processing of file 80 times took    172.5 ms (     2.2 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 0
✔
** SIMD SPEEDUP:   0.1x **


$$$ SECTION Finding etaionsh (8 most freq chars) in /usr/dict/words
 [(61)(65)(68-69)(6e-6f)(73-74)]

Charset has 2-8 chars
Non-simd processing of file 8 times took    460.5 ms (    57.6 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 1311678
✔SIMD processing of file 8 times took    977.4 ms (   122.2 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 1311678
✔
** SIMD SPEEDUP:   0.5x **


$$$ SECTION Finding pbvkjxqz (8 least freq chars) in /usr/dict/words
 [(62)(6a-6b)(70-71)(76)(78)(7a)]

Charset has 2-8 chars
Non-simd processing of file 8 times took    329.6 ms (    41.2 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 171748
✔SIMD processing of file 8 times took    167.9 ms (    21.0 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 171748
✔
** SIMD SPEEDUP:   2.0x **


$$$ SECTION Finding (+[^|{}] (8 not appearing) in /usr/dict/words
 [(28)(2b)(5b)(5d-5e)(7b-7d)]

Charset has 2-8 chars
Non-simd processing of file 8 times took    304.1 ms (    38.0 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 0
✔SIMD processing of file 8 times took     73.0 ms (     9.1 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 0
✔
** SIMD SPEEDUP:   4.2x **


$$$ SECTION Finding etaoinshrdl (12 most freq) in /usr/dict/words
 [(61)(64-65)(68-69)(6c)(6e-6f)(72-74)]

Charset has 2-255 chars
Non-simd processing of file 8 times took    475.8 ms (    59.5 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 1668578
✔SIMD processing of file 8 times took   1188.3 ms (   148.5 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 1668578
✔
** SIMD SPEEDUP:   0.4x **


$$$ SECTION Finding wfgypbvkjxqz (12 least freq) in /usr/dict/words
 [(62)(66-67)(6a-6b)(70-71)(76-7a)]

Charset has 2-255 chars
Non-simd processing of file 8 times took    351.4 ms (    43.9 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 306580
✔SIMD processing of file 8 times took    241.8 ms (    30.2 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 306580
✔
** SIMD SPEEDUP:   1.5x **


$$$ SECTION Finding ()*+[\]^_`{|}~ (15 not appearing) in /usr/dict/words
 [(28-2b)(5b-60)(7b-7e)]

Charset has 2-255 chars
Non-simd processing of file 8 times took    304.5 ms (    38.1 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 0
✔SIMD processing of file 8 times took     64.8 ms (     8.1 per iteration)
FYI: /usr/share/dict/words has 2493109 bytes
FYI: match count is 0
✔
** SIMD SPEEDUP:   4.7x **


$$$ SECTION Testing span_char
✔✔✔✔✔✔✔✔✔✔✔✔
$$$ SECTION Testing span_charset with a single character set
✔✔✔✔✔✔✔✔✔✔✔✔
$$$ SECTION Testing span_charset with a small character set
✔ptr index is 2
✔✔✔✔✔✔✔✔✔✔✔✔✔
$$$ SECTION Testing span_charset with a larger character sets
✔ptr index is 9
✔✔✔✔✔✔✔✔✔✔✔
$$$ END ./findtest --------------------------------------------------------------

~/Projects/libpexl/test/unit$
