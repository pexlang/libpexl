/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vmtest.c  TESTING vm.c                                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "binary.h"
#include "vm.h"

#include "../test.h"

#define PRINTING NO


static int vmtest (const char *patname,
		   pexl_Binary *pkg, pexl_PackageTable *pt,
		   pexl_Index entrypoint, 
		   const char *input, size_t inputlen,
		   pexl_Match *match, bool matched, ssize_t end) {
  int stat;
  printf("Running vm on pattern '%s' and input: ", patname);
  if (inputlen > 70) 
    printf("\"%.*s\"...\n", 70, input);
  else
    printf("\"%.*s\"\n", (int) inputlen, input);
  if (PRINTING) printf("Entrypoint is %d\n", entrypoint);
  stat = vm_start(pkg, pt, entrypoint, input, inputlen, 0, inputlen, 0, NULL, match);
  if (stat != 0) {
    if (PRINTING)
      printf("vmtest: vm returned status code %d\n", stat);
    return stat;
  }
  if ((match->end != -1) != matched) {
    printf("vmtest: expected %s\n", matched ? "succcess but match failed" : "fail but match succeeded");
    stat = 1;
    return stat;
  }
  if (matched && !pexl_Match_failed(match)) {
    if (match->end != end) {
      printf("vmtest: expected match end of %lu, got %lu\n", end, match->end);
      stat = 1;
      return stat;
    }
  }
  return OK;
}

#define matchtree_string(m, offset) \
  (m->data ? &(((pexl_MatchTree *)m->data)->block[offset]) : NULL)

static int check_match_tree (pexl_Match *match, size_t n,
			     pexl_Position startpos[],
			     pexl_Position endpos[],
			     const char *prefixes[],
			     const char *typenames[],
			     const char *datavalues[]) {
  pexl_Index i;
  pexl_MatchNode *node;
  pexl_MatchTree *tree = (pexl_MatchTree *)match->data;
  if (tree->size != n) return -1; /* error */
  i = PEXL_ITER_START;
  while ((i = pexl_MatchTree_iter(match, i, &node)) >= 0) {
    if (node->start != startpos[i]) {
      printf("at node [%d]: expected startpos %" pexl_Position_FMT " to be %" pexl_Position_FMT "\n",
	     i, node->start, startpos[i]);
      return -1;
    }
    if (node->end != endpos[i]) {
      printf("at node [%d]: expected endpos %" pexl_Position_FMT " to be %" pexl_Position_FMT "\n",
	     i, node->end, endpos[i]);
      return -1;
    }
    if (node->prefix == -1) {
      if (prefixes[i] != NULL) {
	printf("at node [%d]: expected prefix '%s' to be NULL\n", i, prefixes[i]);
	return -1;
      }
    } else {
      const char *prefix = matchtree_string(match, node->prefix);
      if (!prefixes[i] || memcmp(prefix, prefixes[i], strlen(prefixes[i])) != 0) {
	printf("at node [%d]: prefix '%s' does not match expected '%s'\n",
	       i, prefix, prefixes[i]);
	return -1;
      }
    }
    if (node->type == -1) {
      if (typenames[i] != NULL) {
	printf("at node [%d]: expected type '%s' to be NULL\n", i, typenames[i]);
	return -1;
      }
    } else {
      const char *type = matchtree_string(match, node->type);
      if (!typenames[i] || memcmp(type, typenames[i], strlen(typenames[i])) != 0) {
	printf("at node [%d]: type '%s' does not match expected '%s'\n",
	       i, type, typenames[i]);
	return -1;
      }
    }
    if (node->inserted_data == -1) {
      if (datavalues[i] != NULL) {
	printf("at node [%d]: expected data '%s' to be NULL\n", i, datavalues[i]);
	return -1;
      }
    } else {
      const char *data = matchtree_string(match, node->inserted_data);
      if (!datavalues[i] || memcmp(data, datavalues[i], strlen(datavalues[i])) != 0) {
	printf("at node [%d]: data '%s' does not match expected '%s'\n",
	       i, data, datavalues[i]);
	return -1;
      }
    }
  } /* while */
  return PEXL_OK;
}

static
pexl_Binary *compile_expression (pexl_Context *C,
				 pexl_Optims *optims,
				 pexl_Expr *exp,
				 pexl_Env env,
				 pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);

#if 0
  confess("TEMP", "setting default entrypoint manually");
  Binding *b = env_get_binding(C->bt, ref);
  TEST_ASSERT(b && (bindingtype(b) == Epattern_t));
  Pattern *pat = (Pattern *) b->val.ptr;
  TEST_ASSERT(pat);
  TEST_ASSERT(pat->entrypoint >= 0);
  pkg->ep = pat->entrypoint;
#endif
  return pkg;
}

int main(int argc, char **argv) {

  int stat;
  pexl_Match *match;
  
  pexl_Context *C;
  pexl_Optims *optims = NULL;
  pexl_Binary *pkg;
  pexl_Error err;
  
  pexl_Env env; 

  pexl_Expr *exp, *A, *B, *S;
  pexl_Ref ref, refA, refB, refS, ref_anon;

  const char *input;
  size_t inputlen;
  pexl_PackageTable *pt;
  pexl_Index id;

  SymbolTableEntry *entry;
  char refname[2];
  refname[0] = '\0';
  refname[1] = '\0';
  
  TEST_START(argc, argv);

  print_vm_sizes();

  TEST_SECTION("Setting up");

  match = pexl_new_Match(PEXL_DEBUG_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  TEST_ASSERT(match_encoder(match) == PEXL_DEBUG_ENCODER); 
  pexl_free_Match(match);
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  TEST_ASSERT(match_encoder(match) == PEXL_TREE_ENCODER); 
  /* Do not free the tree encoder match struct.  We'll use it later. */

  C = pexl_new_Context();
  TEST_ASSERT_NOT_NULL(C);
  env = 0;			// root env id is 0

  TEST_ASSERT_NULL(optims);
  optims = pexl_default_Optims();
  TEST_ASSERT(optims);
  
  // NULL for unspecified value, and NULL for anonymous binding
  refS = pexl_bind_in(C, env, NULL, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  A = pexl_call(C, refS);	/* A -> S */
  exp = pexl_capture_f(C,	/* exp -> capture("foo", "H") */
			"foo",
			pexl_match_string(C, "H")); 
  S = pexl_seq_f(C,		/* S -> exp A */
		  exp,
		  pexl_insert(C, "constcapname", "constcapvalue")); 

  
  TEST_ASSERT(pexl_rebind(C, refS, S) == OK);
  pexl_free_Expr(S);

  pkg = compile_expression(C, optims, A, env, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == OK);
  pexl_free_Expr(A);

  if (PRINTING) pexl_print_Binary(pkg);

  input = "Hello";
  inputlen = strlen(input);
  
  pt = packagetable_new(0);
  TEST_ASSERT_NOT_NULL(pt);

  /* Error checking.  Note: PackageTable, Optims, and Stats can be NULL. */
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(NULL, pt, 0, input, inputlen, 0, inputlen, 0, NULL, match);
  TEST_ASSERT(stat == PEXL_MATCH__ERR_NULL); /* package arg is null */
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(pkg, pt, 0,  NULL, 0, 0, inputlen, 0, NULL, match);
  TEST_ASSERT(stat == PEXL_MATCH__ERR_NULL); /* input arg is null */
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(pkg, pt, 0, input, inputlen, 0, inputlen, 0, NULL, NULL);
  TEST_ASSERT(stat == PEXL_MATCH__ERR_NULL); /*  match arg is null */

  /* One last set of error checks */
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(pkg, pt, -1, input, inputlen, 0, inputlen, 0, NULL, match);
  TEST_ASSERT(stat == PEXL_MATCH__ERR_ENTRYPOINT); /* expected to catch error that entrypoint is invalid */
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(pkg, pt, pkg->codenext, input, inputlen, 0, inputlen, 0, NULL, match);
  TEST_ASSERT(stat == PEXL_MATCH__ERR_ENTRYPOINT); /* expected to catch error that entrypoint is invalid */

  pexl_Stats timing;
  pexl_Index ep;

  /* Pre-condition */
  TEST_ASSERT_NOT_NULL(match);
  TEST_ASSERT(match_encoder(match) == PEXL_TREE_ENCODER); 
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);

  /* Now a few simple tests to ensure the vm works in basic ways */
  stat = vm_start(pkg, pt, ep, input, inputlen, 0, inputlen, 0, NULL, match);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(match->end != -1);
  TEST_ASSERT(match->encoder_id == PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match->data);

  timing.total_time = 0;
  timing.match_time = 0;
  stat = vm_start(pkg, pt, ep, input, inputlen, 0, inputlen, 0, &timing, match);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(match->end != -1);
  TEST_ASSERT(match->encoder_id == PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match->data);
  TEST_ASSERT(timing.total_time >= timing.match_time);

  pexl_free_Match(match);
  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  stat = vm_start(pkg, pt, ep, input, inputlen, 0, inputlen, 0, NULL, match);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(match->end != -1);
  TEST_ASSERT(match->encoder_id == PEXL_NO_ENCODER);
  TEST_ASSERT(match->data == NULL);

  timing.total_time = 0;
  timing.match_time = 0;
  stat = vm_start(pkg, pt, ep, input, inputlen, 0, inputlen, 0, &timing, match);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(match->end != -1);
  TEST_ASSERT(match->encoder_id == PEXL_NO_ENCODER);
  TEST_ASSERT(match->data == NULL);
  printf("TOTAL TIME: %d, MATCH TIME: %d\n", timing.total_time, timing.match_time);
  TEST_ASSERT(timing.total_time >= timing.match_time);
  pexl_free_Match(match);

  /*
    An invalid output encoder is the same as PEXL_NO_ENCODER, except
    that a warning is printed
  */
  match = pexl_new_Match(-1);
  TEST_ASSERT_NOT_NULL(match);
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(pkg, pt, ep, input, inputlen, 0, inputlen, 0, &timing, match);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(match->end != -1);

  pexl_free_Match(match);
  match = pexl_new_Match(100000); /* Assuming this is not a valid encoder number */
  TEST_ASSERT_NOT_NULL(match);
  fprintf(stderr, "Expect a warning here:\n");
  stat = vm_start(pkg, pt, ep, input, inputlen, 0, inputlen, 0, &timing, match);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(match->end != -1);
  pexl_free_Match(match);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing the tree encoder, without and with a prefix");

  TEST_ASSERT(pkg);

  match = pexl_new_Match(PEXL_DEBUG_ENCODER);
  TEST_ASSERT_NOT_NULL(match);

  /* Print the capture stack (and test the debug encoder) */
  TEST_ASSERT(!vmtest("capture(\"foo\", 'H')", pkg, NULL, ep, 
		      "Hello", 5, match, true, 1));

  pexl_free_Match(match);
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  // Try printing a new match struct with no data, just to test out
  // the print functions
  pexl_print_Match(match);

  /* First, with no prefix */
  stat = binary_set_attributes(pkg, NULL, NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(!vmtest("capture(\"foo\", 'H') const(\"constcapname\", \"constcapvalue\")",
		      pkg, NULL, ep, 
		      "Hello", 5, match, true, 1));

  TEST_ASSERT_NOT_NULL((pexl_MatchTree *)match->data);
  if (PRINTING) pexl_print_Match(match->data);
  /* Tree should contain root with 2 children: foo='H', const constcapname='constcapvalue' */
  TEST_ASSERT(((pexl_MatchTree *)match->data)->size == 3);
  /* Check typename, startpos, endpos, prefix, data */
  stat = check_match_tree(match, 3,
			  (pexl_Position []){0, 0, 1},	      /* startpos */
			  (pexl_Position []){1, 1, 1},	      /* endpos */
			  (const char *[]){NULL, NULL, NULL},             /* prefixes */
			  (const char *[]){NULL, "foo", "constcapname"},  /* typenames */
			  (const char *[]){NULL, NULL, "constcapvalue"}); /* data */
  TEST_ASSERT(stat == 0);
  // We want to print at least once, just to test out the print functions
  pexl_print_Match(match);

  /* Now with a prefix */
  stat = binary_set_attributes(pkg, NULL, "PREFIX", NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);

  TEST_ASSERT(!vmtest("w/PREFIX: capture(\"foo\", 'H') const(\"constcapname\", \"constcapvalue\")",
		      pkg, NULL, ep, 
		      "Hello", 5, match, true, 1));

  TEST_ASSERT_NOT_NULL((pexl_MatchTree *)match->data);
  if (PRINTING) pexl_print_Match_data(match->data);
  TEST_ASSERT(((pexl_MatchTree *)match->data)->size == 3);
  /* Check typename, startpos, endpos, prefix, data */
  stat = check_match_tree(match, 3,
			  (pexl_Position []){0, 0, 1}, // startpos
			  (pexl_Position []){1, 1, 1}, // endpos 
			  (const char *[]){NULL, "PREFIX", "PREFIX"},     // prefixes
			  (const char *[]){NULL, "foo", "constcapname"},  // typenames
			  (const char *[]){NULL, NULL, "constcapvalue"}); // cap data
  TEST_ASSERT(stat == 0);
  if (PRINTING) pexl_print_Match(match);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing that encoders see package prefixes");

  TEST_ASSERT(pkg);		/* pre-requisite for next test: reusing package */

  stat = binary_set_attributes(pkg, NULL, "Prefix here!", NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);
  
  TEST_ASSERT(!vmtest("w/Prefix here!: capture(\"foo\", 'H') const(\"constcapname\", \"constcapvalue\")",
		      pkg, NULL, ep, 
		      "Hello", 5, match, true, 1));

  TEST_ASSERT_NOT_NULL((pexl_MatchTree *)match->data);
  if (PRINTING) pexl_print_Match(match);

  TEST_ASSERT(((pexl_MatchTree *)match->data)->size == 3);
  /* Check typename, startpos, endpos, prefix, data */
  stat = check_match_tree(match, 3,
			  (pexl_Position []){0, 0, 1},	      /* startpos */
			  (pexl_Position []){1, 1, 1},	      /* endpos */
			  (const char *[]){NULL, "Prefix here!", "Prefix here!"}, /* prefixes */
			  (const char *[]){NULL, "foo", "constcapname"},  /* typenames */
			  (const char *[]){NULL, NULL, "constcapvalue"}); /* data */
  TEST_ASSERT(stat == 0);
  if (PRINTING) pexl_print_Match(match);
  binary_free(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();

  id = context_intern(C, "foo", 3); /* for later use */
  TEST_ASSERT(id >= 0);

  ref = pexl_bind_in(C, env, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C,		/* A -> "a" A "b" / epsilon */
		 pexl_seq_f(C,
			 pexl_match_string(C, "a"),
			 pexl_seq_f(C, 
				 pexl_call(C, ref),
				 pexl_match_string(C, "b"))),
		 pexl_match_epsilon(C));

  TEST_ASSERT(A);		/* A is a valid expression */
  printf("A is: \n"); 
  pexl_print_Expr(A, 0, C); 

  TEST_ASSERT(pexl_rebind(C, ref, A) == OK);
  pexl_free_Expr(A);

  // IMPORTANT: We cannot call compile_expression here, because it
  // will create a NEW pattern referring to the expression A.  We end
  // up with multiple distinct patterns sharing an expr.  It works ok
  // until we need to free the patterns (in pexl_free_Context), where
  // we double-free the shared expr, A.

  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == OK);
  ep = main_entrypoint(pkg);
  /* do not free A because A is bound */

  if (PRINTING) pexl_print_Binary(pkg);

  printf("Testing that pattern fails on input 'Hello'...\n");
  TEST_ASSERT(!vmtest("A -> 'a' A 'b' / eps", 
		      pkg, NULL, ep, 
		      "Hello", 5, match, true, 0));
  printf("... ok. \n");

  printf("Testing that pattern succeeds on appropriate input...\n");
  TEST_ASSERT(!vmtest("A -> 'a' A 'b' / eps", 
		      pkg, NULL, ep, 
		      "aaabbb", 6, match, true, 6));
  printf("... ok. \n");
  
  printf("Testing that pattern matches a prefix of the input...\n");
  TEST_ASSERT(!vmtest("A -> 'a' A 'b' / eps",
		      pkg, NULL, ep, 
		      "abc", 3, match, true, 2));
  printf("... ok. \n");
  binary_free(pkg);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Now testing entrypoint argument to vm_start()");

  print_BindingTable(C);fflush(NULL);

  pexl_free_Context(C);
  C = pexl_new_Context();
  
  refA = pexl_bind_in(C, env, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(refA));

  /* Here we use the variable B to hold the anonymous expression */
  /* <anon> -> "x */
  B = pexl_match_string(C, "x");
  TEST_ASSERT(B);

  ref_anon = pexl_bind_in(C, env, NULL, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(ref_anon));
  TEST_ASSERT(pexl_rebind(C, ref_anon, B) == OK);
  pexl_free_Expr(B);


  /* Here we reuse the variable B to make the exp we will bind to "B */
  /* B -> "a"+ <anon> == "a"+ "x */
  B = pexl_seq_f(C, 
	 pexl_repeat_f(C, pexl_match_string(C, "a"), 1, 0), /* a"+ */
	 pexl_call(C, ref_anon));			    /* x"  */

  TEST_ASSERT(B);

  refB = pexl_bind_in(C, env, NULL, "B");
  TEST_ASSERT(!pexl_Ref_invalid(refB));
  TEST_ASSERT(pexl_rebind(C, refB, B) == OK);

  /* A -> B "b" == "a"+ "x" "b */
  A = pexl_seq_f(C, 
	      pexl_call(C, refB),
	      pexl_match_string(C, "b"));

  TEST_ASSERT(A);

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  TEST_ASSERT(pexl_rebind(C, refA, A) == OK);

  pkg = pexl_compile(C, refA, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == 0);
  ep = main_entrypoint(pkg);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);
  
  entry = binary_lookup_symbol(pkg, symbol_ns_id, "A");
  TEST_ASSERT(entry);		/* should find the symbol 'A' */
  printf("Found 'A' in symbol table.  Entrypoint is %d\n", entry->value);

  /* Set up */
  pexl_free_Match(match);
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  TEST_ASSERT(match_encoder(match) == PEXL_TREE_ENCODER);

  input = "aaaxb"; 
  printf("Testing a+xb pattern on input '%s'...\n", input); 
  TEST_ASSERT(!vmtest("'a'+ 'x' 'b'",
		      pkg, NULL, entry->value, 
		      input, strlen(input), match, true, strlen(input)));
  printf("... ok. \n");
  
  input = "aaaaaaaaaaxb";
  TEST_ASSERT(!vmtest("'a'+ 'x' 'b'",
		      pkg, NULL, entry->value, 
		      input, strlen(input), match, true, strlen(input)));
  printf("... ok. \n");
  
  input = "aaax";
  TEST_ASSERT(!vmtest("'a'+ 'x' 'b'",
		      pkg, NULL, entry->value, 
		      input, strlen(input), match, false, -1));
  printf("... ok. \n");
  
  input = "aaaxb123";
  TEST_ASSERT(!vmtest("'a'+ 'x' 'b'",
		      pkg, NULL, entry->value, 
		      input, strlen(input), match, true, strlen(input)-3));
  printf("... ok. \n");
  pexl_free_Binary(pkg);
  pexl_free_Expr(A);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Misc patterns");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();

  exp = pexl_match_set(C, "abcABC", 6);
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) {
    pexl_print_Binary(pkg);
    print_symboltable(pkg->symtab);
  }

  TEST_ASSERT(!vmtest("[abcABC]", pkg, NULL, ep, 
		      "A", 1, match, true, 1));

  binary_free(pkg);

  exp = pexl_match_bytes(C, "AB\0", 3);
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  /* Pre-condition */
  TEST_ASSERT(match_encoder(match) == PEXL_TREE_ENCODER);

  pexl_print_Binary(pkg);

  TEST_ASSERT(!vmtest("AB\\0",
		      pkg, NULL, ep, 
		      "AB\0zzz", 6, match, true, 3));

  binary_free(pkg);

  exp = pexl_match_any(C, 3);	/* any 3 bytes */
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest(".{3}", pkg, NULL, ep, 
		      "abc", 3, match, true, 3));
  TEST_ASSERT(!vmtest(".{3}", pkg, NULL, ep, 
		      "abcde", 5, match, true, 3));
  TEST_ASSERT(!vmtest(".{3}", pkg, NULL, ep, 
		      "ab", 2, match, false, -1));
  TEST_ASSERT(!vmtest(".{3}", pkg, NULL, ep, 
		      "a", 1, match, false, -1));
  TEST_ASSERT(!vmtest(".{3}", pkg, NULL, ep, 
		      "", 0, match, false, -1));

  binary_free(pkg);

  exp = pexl_match_any(C, -3); /* NOT 3 bytes */
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("!.{3}", pkg, NULL, ep,  
		      "", 0, match, true, 0));
  TEST_ASSERT(!vmtest("!.{3}", pkg, NULL, ep,  
		      "a", 1, match, true, 0));
  TEST_ASSERT(!vmtest("!.{3}", pkg, NULL, ep,  
		      "ab", 2, match, true, 0));
  TEST_ASSERT(!vmtest("!.{3}", pkg, NULL, ep,  
		      "abc", 3, match, false, -1));
  TEST_ASSERT(!vmtest("!.{3}", pkg, NULL, ep, 
		      "abcd", 4, match, false, -1));
  TEST_ASSERT(!vmtest("!.{3}", pkg, NULL, ep, 
		      "abcde", 5, match, false, -1));

  binary_free(pkg);

  exp = pexl_match_any(C, 0);	/* same as true (== epsilon)*/
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("true", pkg, NULL, ep, 
		      "abcde", 5, match, true, 0));
  TEST_ASSERT(!vmtest("true", pkg, NULL, ep,  
		      "", 0, match, true, 0));

  binary_free(pkg);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_match_range(C, 65, 64);  /* pexl_Error checking */
  TEST_ASSERT(!exp);		      /* error when from > to */

  exp = pexl_match_range(C, 65, 64 + 26);	/* [A-Z] */
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("[A-Z]", pkg, NULL, ep,  
		      "abcde", 5, match, false, -1));
  TEST_ASSERT(!vmtest("[A-Z]", pkg, NULL, ep,  
		      "", 0, match, false, -1));
  TEST_ASSERT(!vmtest("[A-Z]", pkg, NULL, ep,  
		      "A", 1, match, true, 1));
  TEST_ASSERT(!vmtest("[A-Z]", pkg, NULL, ep, 
		      "OP", 2, match, true, 1));
  TEST_ASSERT(!vmtest("[A-Z]", pkg, NULL, ep, 
		      "\0", 1, match, false, -1));

  binary_free(pkg);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_neg_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_neg_lookahead_f(C, pexl_match_string(C, "JAJ"));
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("!'JAJ'", pkg, NULL, ep, 
		      "", 0, match, true, 0));
  TEST_ASSERT(!vmtest("!'JAJ'", pkg, NULL, ep, 
		      "jaj", 3, match, true, 0));
  TEST_ASSERT(!vmtest("!'JAJ'", pkg, NULL, ep, 
		      "JAJ", 3, match, false, -1));

  binary_free(pkg);

  exp = pexl_neg_lookahead_f(C, pexl_seq_f(C,
			       pexl_match_string(C, "JAJ"),
			       pexl_match_any(C, -1) /* $ */
			       ));
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("!('JAJ'$)", pkg, NULL, ep, 
		      "", 0, match, true, 0));
  TEST_ASSERT(!vmtest("!('JAJ'$)", pkg, NULL, ep, 
		      "jaj", 3, match, true, 0));
  TEST_ASSERT(!vmtest("!('JAJ'$)", pkg, NULL, ep, 
		      "JAJ", 3, match, false, -1));
  TEST_ASSERT(!vmtest("!('JAJ'$)", pkg, NULL, ep, 
		      " JAJ", 4, match, true, 0));
  TEST_ASSERT(!vmtest("!('JAJ'$)", pkg, NULL, ep, 
		      "JAJ3", 4, match, true, 0));

  binary_free(pkg);

  exp = pexl_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_lookahead_f(C, pexl_match_string(C, "JAJ"));
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest(">'JAJ'", pkg, NULL, ep, 
		      "", 0, match, false, -1));
  TEST_ASSERT(!vmtest(">'JAJ'", pkg, NULL, ep, 
		      "jaj", 3, match, false, -1));
  TEST_ASSERT(!vmtest(">'JAJ'", pkg, NULL, ep, 
		      "JAJ", 3, match, true, 0));
  TEST_ASSERT(!vmtest(">'JAJ'", pkg, NULL, ep, 
		      "JAJ123", 6, match, true, 0));

  binary_free(pkg);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_lookbehind(C, NULL);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_lookbehind_f(C, pexl_match_string(C, "JAJ"));
  TEST_ASSERT(exp);
  
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);

  TEST_ASSERT(!vmtest("<'JAJ'", pkg, NULL, ep, 
		      "", 0, match, false, -1));
  TEST_ASSERT(!vmtest("<'JAJ'", pkg, NULL, ep, 
		      "jaj", 3, match, false, -1));
  TEST_ASSERT(!vmtest("<'JAJ'", pkg, NULL, ep, 
		      "JAJ", 3, match, false, -1));
  TEST_ASSERT(!vmtest("<'JAJ'", pkg, NULL, ep, 
		      "JAJ123", 6, match, false, -1));
  
  binary_free(pkg);
  
  exp = pexl_seq_f(C,
		    pexl_match_string(C, "JAJ"),
		    pexl_lookbehind_f(C,
				     pexl_match_string(C, "JAJ")));
  TEST_ASSERT(exp);

  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("'JAJ'<'JAJ'", pkg, NULL, ep, 
		      "", 0, match, false, -1));
  TEST_ASSERT(!vmtest("'JAJ'<'JAJ'", pkg, NULL, ep, 
		      "jaj", 3, match, false, -1));
  TEST_ASSERT(!vmtest("'JAJ'<'JAJ'", pkg, NULL, ep, 
		      "JAJ", 3, match, true, 3));
  TEST_ASSERT(!vmtest("'JAJ'<'JAJ'", pkg, NULL, ep, 
		      "JAJ12345", 8, match, true, 3));

  binary_free(pkg);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_repeat_f(C, NULL, 0, 0);
  TEST_ASSERT(!exp);

  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0);
  TEST_ASSERT(exp);

  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("a*", pkg, NULL, ep, 
		      "", 0, match, true, 0));
  TEST_ASSERT(!vmtest("a*", pkg, NULL, ep, 
		      "a", 1, match, true, 1));
  TEST_ASSERT(!vmtest("a*", pkg, NULL, ep, 
		      "aaaaaaK", 6, match, true, 6));
  TEST_ASSERT(!vmtest("a*", pkg, NULL, ep, 
		      "K", 1, match, true, 0));

  binary_free(pkg);

  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 4, 0);
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("a{4,}", pkg, NULL, ep, 
		      "", 0, match, false, -1));
  TEST_ASSERT(!vmtest("a{4,}", pkg, NULL, ep, 
		      "a", 1, match, false, -1));
  TEST_ASSERT(!vmtest("a{4,}", pkg, NULL, ep, 
		      "aaaaK", 5, match, true, 4));
  TEST_ASSERT(!vmtest("a{4,}", pkg, NULL, ep, 
		      "aaaaaaaaaaaaKJ", 14, match, true, 12));

  binary_free(pkg);

  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 4);
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("a{,4}", pkg, NULL, ep, 
		      "", 0, match, true, 0));
  TEST_ASSERT(!vmtest("a{,4}", pkg, NULL, ep, 
		      "a", 1, match, true, 1));
  TEST_ASSERT(!vmtest("a{,4}", pkg, NULL, ep, 
		      "aaaaK", 5, match, true, 4));
  TEST_ASSERT(!vmtest("a{,4}", pkg, NULL, ep, 
		      "aaaaaKJ", 7, match, true, 4));

  binary_free(pkg);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_capture_f(C, "This is a long capture name", NULL);
  TEST_ASSERT(!exp);

  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_capture_f(C, NULL, pexl_match_string(C, "x"));
  TEST_ASSERT(!exp);

  exp = pexl_capture_f(C, "This is a long capture name", pexl_match_string(C, "a"));
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("capture(long name, 'a')", pkg, NULL, ep, 
		      "", 0, match, false, -1));
  TEST_ASSERT(!vmtest("capture(long name, 'a')", pkg, NULL, ep, 
		      "a", 1, match, true, 1));
  if (PRINTING) pexl_print_Match(match);
  TEST_ASSERT(!vmtest("capture(long name, 'a')", pkg, NULL, ep, 
		      "aXYZ", 4, match, true, 1));
  if (PRINTING) pexl_print_Match(match);

  binary_free(pkg);
  
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_insert(C, "foo", NULL);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_insert(C, NULL, "foo");
  TEST_ASSERT(!exp);
  exp = pexl_insert(C, "This is a long capture name", "And a long value name");
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);

  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  TEST_ASSERT(!vmtest("constant_capture(long name, long value)",
		      pkg, NULL, ep, 
		      "XYZ", 3, match, true, 0));
  if (PRINTING) pexl_print_Match(match);
  binary_free(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Checking a high number of captures");
  exp = pexl_repeat_f(C,
	     pexl_capture_f(C,
                "pexlCaptureName",
		 pexl_match_string(C, "a")),
        1, 0);
  TEST_ASSERT(exp);
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  size_t number_of_as = 1 + CAPSTACK_MAX_CAPACITY / 2;
  char *some_as = malloc(number_of_as + 1);
  assert(some_as);
  memset(some_as, 'a', number_of_as);
  some_as[number_of_as] = '\0';
  TEST_ASSERT(strlen(some_as) == number_of_as);
  TEST_ASSERT(vmtest("a+", pkg, NULL, ep, 
		     some_as, strlen(some_as),
		     match, true, 0) == PEXL_MATCH__ERR_CAPLIMIT);

  /* Now shorten the input so that the number of captures will fit in the limit */
  some_as[number_of_as - 1] = '\0';
  TEST_ASSERT(!vmtest("a+", pkg, NULL, ep, 
		      some_as, strlen(some_as), match, true, number_of_as-1));
  if (PRINTING) pexl_print_Match_summary(match);
  /* Number of tree nodes: 1 for each a, plus 1 for root node. */
  TEST_ASSERT(((pexl_MatchTree *)match->data)->size == number_of_as);

  binary_free(pkg);

  free(some_as);
  some_as = NULL;

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Checking a high number of choices");
  unsigned char c1=0, c2=0, c3=0;
  char str[3];
  int depth = 0;
  exp = NULL;
  /* First check that max depth is caught and reported */
  for (c1 = 0; c1 < 255; c1++)
    for (c2 = 0; c2 < 25; c2++)
      for (c3 = 0; c3 < 255; c3++) {
	str[0] = c1; str[1] = c2; str[2] = c3;
	if (!exp)
	  exp = pexl_match_bytes(C, str, 3);
	else
	  exp = pexl_choice_f(C, exp, pexl_match_bytes(C, str, 3));
	depth++;
	if (depth <= PEXL_MAX_EXPDEPTH) {
	  assert(exp);
	} else {
	  printf("pexl_Expr *depth is %d\n", depth);
	  TEST_ASSERT(!exp);
	  TEST_ASSERT(C->err = PEXL_EXP__ERR_DEPTH);
	  goto nextdepthtest;
      }
      }
  TEST_ASSERT(false);
 nextdepthtest:
  /* Check that up to max depth can be created and compiled */
  depth = 0;
  for (c1 = 0; c1 < 255; c1++)
    for (c2 = 0; c2 < 25; c2++)
      for (c3 = 0; c3 < 255; c3++) {
	str[0] = c1; str[1] = c2; str[2] = c3;
	if (!exp)
	  exp = pexl_match_bytes(C, str, 3);
	else
	  exp = pexl_choice_f(C, exp, pexl_match_bytes(C, str, 3));
	depth++;
	if (depth == PEXL_MAX_EXPDEPTH) {
	  printf("pexl_Expr *depth is %d\n", depth);
	  TEST_ASSERT(exp);
	  goto compiledeepexp;
      }
      }
  TEST_ASSERT(false);
 compiledeepexp:
  pkg = compile_expression(C, optims, exp, env, &err);
  TEST_ASSERT(pkg);
  ep = main_entrypoint(pkg);
  pexl_free_Expr(exp);
  
  printf("Pkg code vector has %u instructions\n", pkg->codenext);

  TEST_ASSERT(vmtest("<many choices>",
		     pkg, NULL, ep, 
		     str, 3,
		     match, true, 3) == PEXL_OK);

  binary_free(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using pexl_compile() API");

#define bind(exp, name) do {				\
    ref = pexl_bind(C, exp, (name));			\
    TEST_ASSERT(!pexl_Ref_invalid(ref));		\
    pexl_free_Expr(exp);				\
    name[0]++;						\
  } while (0)

  /* New env */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  refname[0] = 'a';
  refname[1] = '\0';
  exp = pexl_match_set(C, "abcABC", 6);
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* b */
  exp = pexl_match_bytes(C, "AB\0", 3);
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* c */
  exp = pexl_match_any(C, 3);	/* any 3 bytes */
  TEST_ASSERT(exp);
  bind(exp, refname);
  
  /* d */
  exp = pexl_match_any(C, -3); /* NOT 3 bytes */
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* e */
  exp = pexl_match_any(C, 0);	/* same as true (== epsilon)*/
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* f */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_match_range(C, 65, 64);
  TEST_ASSERT(!exp);			  /* error when from > to */
  exp = pexl_match_range(C, 65, 64 + 26); /* [A-Z] */
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* g */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_neg_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_neg_lookahead_f(C, pexl_match_string(C, "JAJ"));
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* h */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_lookahead_f(C, pexl_match_string(C, "JAJ"));
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* i */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_repeat_f(C, NULL, 0, 0);
  TEST_ASSERT(!exp);
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0);
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* j */
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 4, 0);
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* k */
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 4);
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* l */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_capture_f(C, "This is a long capture name", NULL);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_capture_f(C, NULL, pexl_match_string(C, "x"));
  TEST_ASSERT(!exp);
  exp = pexl_capture_f(C, "This is a long capture name", pexl_match_string(C, "a"));
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* m */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_insert(C, "foo", NULL);
  TEST_ASSERT(!exp);
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_insert(C, NULL, "foo");
  TEST_ASSERT(!exp);
  exp = pexl_insert(C, "foo", "bar");
  TEST_ASSERT(exp);
  bind(exp, refname);
  
  /* n */
  fprintf(stderr, "Expect a warning here:\n");
  exp = pexl_lookbehind(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_lookbehind_f(C, pexl_match_string(C, "JAJ"));
  TEST_ASSERT(exp);
  bind(exp, refname);

  /* o */
  exp = pexl_lookbehind_f(C,
			 pexl_lookahead_f(C,
					   pexl_match_any(C, 5)));
  TEST_ASSERT(exp);
  bind(exp, refname);

  if (PRINTING) pexl_print_Env(env, C);

  pexl_free_Optims(optims);
  optims = pexl_addopt_simd(NULL, 1);
  TEST_ASSERT(optims);
  optims = pexl_addopt_inline(optims);
  TEST_ASSERT(optims);
  // Peephole must be after unroll, inline, tro, so this will
  // create an error that we'll see if we catch...
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_unroll(optims, 2);
  TEST_ASSERT(optims);
  
  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(!pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_COMPILER__ERR_OPTIMS);

  pexl_free_Optims(optims);
  optims = pexl_addopt_simd(NULL, 1);
  TEST_ASSERT(optims);
  optims = pexl_addopt_tro(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_inline(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_unroll(optims, 2);
  TEST_ASSERT(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);

  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == OK);
  ep = main_entrypoint(pkg);
  
  if (PRINTING) pexl_print_Binary(pkg);
  if (PRINTING) print_symboltable(pkg->symtab);

  /* Spot checks of just a few entrypoints */

  refname[0] = 'a';
  TEST_ASSERT(refname[1] == '\0');
  entry = binary_lookup_symbol(pkg, symbol_ns_id, &refname[0]);
  TEST_ASSERT(entry);
  TEST_ASSERT(!vmtest("ref('a')", pkg, NULL, entry->value, 
		      "abcABC", 6, match, true, 1));
  
  refname[0] = 'm';
  entry = binary_lookup_symbol(pkg, symbol_ns_id, &refname[0]);
  TEST_ASSERT(entry);
  TEST_ASSERT(!vmtest("constant_capture('foo', 'bar')",
		      pkg, NULL, entry->value, 
		      "ABC", 3, match, true, 0));
  if (PRINTING) pexl_print_Match(match);

  refname[0] = 'f';
  entry = binary_lookup_symbol(pkg, symbol_ns_id, &refname[0]);
  TEST_ASSERT(entry);
  TEST_ASSERT(!vmtest("[A-Z]",
		      pkg, NULL, entry->value, 
		      "JKL", 3, match, true, 1));
  TEST_ASSERT(!vmtest("[A-Z]",
		      pkg, NULL, entry->value, 
		      "....", 4, match, false, -1));
  TEST_ASSERT(!vmtest("[A-Z]",
		      pkg, NULL, entry->value, 
		      "Z", 1, match, true, 1));

  pexl_free_Binary(pkg);
  pexl_free_Match(match);
  pexl_free_Context(C);
  pexl_free_Optims(optims);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Backreferences");

  /* New env */
  C = pexl_new_Context();
  TEST_ASSERT(C);
  optims = pexl_default_Optims();
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  
  /*
       seq
     /     \
    A      backref:A
  
    A = capture(one_x, "A")
    one_x = pexl_match_string(C, "x")

  */
  
  A = pexl_capture_f(C, "A", pexl_match_string(C, "x"));
  TEST_ASSERT(A);

  S = pexl_seq_f(C, A, pexl_backref(C, "A"));

  //  if (PRINTING) {
  if (1) {
    printf("S is: \n"); 
    pexl_print_Expr(S, 0, C); 
  }
  
  refA = pexl_bind(C, S, NULL); // anonymous

  pkg = pexl_compile(C, refA, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  //  if (PRINTING)
  pexl_print_Binary(pkg);

  TEST_ASSERT(!vmtest("{A backref:A} where A = \"x\"",
		      pkg, NULL, main_entrypoint(pkg), 
		      "xx", 2, match, true, 2));

  TEST_ASSERT(!vmtest("{A backref:A} where A = \"x\"",
		      pkg, NULL, main_entrypoint(pkg), 
		      "xxxx", 4, match, true, 2));

  TEST_ASSERT(!vmtest("{A backref:A} where A = \"x\"",
		      pkg, NULL, main_entrypoint(pkg), 
		      "x", 1, match, false, 0));

  TEST_ASSERT(!vmtest("{A backref:A} where A = \"x\"",
		      pkg, NULL, main_entrypoint(pkg), 
		      "", 0, match, false, 0));

  pexl_free_Binary(pkg);



  pexl_free_Match(match);
  pexl_free_Context(C);
  pexl_free_Optims(optims);


  TEST_END();

  return 0;
}

