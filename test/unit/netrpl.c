/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  wheretest.c  TESTING implementation of 'where' feature                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Kayla Sanderson                              */

#include "preamble.h"

#include "binary.h"
#include "compile.h"
#include "print.h"
#include "vm.h"
#include <stdio.h>
#include <stdlib.h> /* exit() */

#include "../test.h"

#define PRINTING NO

static char *readFile(const char *filename) {
  FILE *f = fopen(filename, "rt");
  TEST_ASSERT(f);
  fseek(f, 0, SEEK_END);
  long length = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buffer = (char *)malloc(length + 1);
  buffer[length] = '\0';
  fread(buffer, 1, length, f);
  fclose(f);
  return buffer;
}

static int vmtest(const char *patname, pexl_Binary *pkg, pexl_PackageTable *pt,
                  pexl_Index entrypoint, const char *input, size_t inputlen,
                  pexl_Match *match, bool matched, ssize_t end) {
  int stat;
  printf("Running vm on pattern '%s' and input: ", patname);
  if (inputlen > 70)
    printf("\"%.*s\"...\n", 70, input);
  else
    printf("\"%.*s\"\n", (int)inputlen, input);
  if (PRINTING)
    printf("Entrypoint is %d\n", entrypoint);
  stat = vm_start(pkg, pt, entrypoint, input, inputlen, 0, inputlen, 0, NULL,
                  match);
  if (stat != 0) {
    if (PRINTING)
      printf("vmtest: vm returned status code %d\n", stat);
    return stat;
  }
  if ((match->end != -1) != matched) {
    printf("vmtest: expected %s\n",
           matched ? "succcess but match failed" : "fail but match succeeded");
    stat = 1;
    return stat;
  }
  if (matched && !pexl_Match_failed(match)) {
    if (match->end != end) {
      printf("vmtest: expected match end of %lu, got %lu\n", end, match->end);
      stat = 1;
      return stat;
    }
  }
  printf("...OK\n");
  return OK;
}

#define matchtree_string(m, offset)                                            \
  (m->data ? &(((pexl_MatchTree *)m->data)->block[offset]) : NULL)


int main(int argc, char **argv) {

  
  pexl_Match *match;

  pexl_Context *C;
  pexl_Optims *optims = NULL;
  pexl_Binary *pkg;
  pexl_Error err;

  pexl_Ref ref;

  const char *input;
  const char *patname;
  const char *inputfile;
  
  pexl_Index ep;

  TEST_START(argc, argv);

  /*
    -----------------------------------------------------------------------------
  */
  TEST_SECTION("ip tests");

  C = pexl_new_Context();
  TEST_ASSERT(C);

  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  optims = pexl_default_Optims();

  ref = pexl_bind_in(C, C->env, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  pexl_Expr *numExp = pexl_match_range(C, '0', '9'); //[0-9]
                                                     // TestNumExp
  TEST_ASSERT(numExp);
  printf("NumExp is: \n");
  pexl_print_Expr(numExp, 0, C);

  TEST_ASSERT(pexl_rebind(C, ref, numExp) == OK);

  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == OK);
  ep = main_entrypoint(pkg);
  /* do not free A because A is bound */

  if (PRINTING)
    pexl_print_Binary(pkg);

  printf("Testing that pattern fails on input 'Hello'...\n");
  TEST_ASSERT(!vmtest("numExp [0-9] / eps", pkg, NULL, ep, "Hello", 5, match,
                      false, 0));
  // printf("... ok. \n");

  printf("Testing that pattern succeeds on appropriate input...\n");
  TEST_ASSERT(
      !vmtest("numExp [0-9]  / eps", pkg, NULL, ep, "8", 1, match, true, 1));
  // printf("... ok. \n");

  printf("Testing that pattern matches a prefix of the input...\n");
  TEST_ASSERT(!vmtest("numExp [0-9]  / eps", pkg, NULL, ep, "888888", 6, match,
                      true, 1));

  // printf("... ok. \n");

  pexl_free_Binary(pkg);

  pexl_Expr *numRange = pexl_match_range(C, '0', '9'); //[0-9]
  TEST_ASSERT(numRange);

  pexl_Ref refNumRange = pexl_bind(C, numRange, NULL); // anonymous

  pkg = pexl_compile(C, refNumRange, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  if (PRINTING)
    pexl_print_Binary(pkg);

  TEST_ASSERT(!vmtest("[0-9]", pkg, NULL, main_entrypoint(pkg), "hello", 5,
                      match, false, 0));

  TEST_ASSERT(!vmtest("[0-9]", pkg, NULL, main_entrypoint(pkg), "1", 1, match,
                      true, 1));

  TEST_ASSERT(!vmtest("[0-9]", pkg, NULL, main_entrypoint(pkg), "12", 2, match,
                      true, 1));

  TEST_ASSERT(!vmtest("[0-9]", pkg, NULL, main_entrypoint(pkg), "", 0, match,
                      false, 0));

  pexl_Expr *numRangeRep = pexl_repeat(C, numRange, 1, 0);
  TEST_ASSERT(numRangeRep);

  pexl_Ref refNumRangeRep = pexl_bind(C, numRangeRep, NULL); // anonymous
  pexl_free_Binary(pkg);
  pkg = pexl_compile(C, refNumRangeRep, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  if (PRINTING)
    pexl_print_Binary(pkg);

  TEST_ASSERT(!vmtest("[0-9]+", pkg, NULL, main_entrypoint(pkg), "hello", 5,
                      match, false, 0));

  TEST_ASSERT(!vmtest("[0-9]+", pkg, NULL, main_entrypoint(pkg), "1", 1, match,
                      true, 1));

  TEST_ASSERT(!vmtest("[0-9]+", pkg, NULL, main_entrypoint(pkg), "12", 2, match,
                      true, 2));
  TEST_ASSERT(!vmtest("[0-9]+", pkg, NULL, main_entrypoint(pkg), "1234567", 7,
                      match, true, 7));
  TEST_ASSERT(!vmtest("[0-9]+", pkg, NULL, main_entrypoint(pkg), "1234567b", 8,
                      match, true, 7));

  TEST_ASSERT(!vmtest("[0-9]+", pkg, NULL, main_entrypoint(pkg), "", 0, match,
                      false, 0));

  pexl_free_Binary(pkg);

  pexl_Expr *A_ZExp = pexl_match_range(C, 'A', 'Z'); //[A-Z]
  TEST_ASSERT(A_ZExp);

  pexl_Ref refA_ZRange = pexl_bind(C, A_ZExp, NULL); // anonymous
  pkg = pexl_compile(C, refA_ZRange, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  patname = "[A-Z]";
  input = "G";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *A_ZExpRep = pexl_repeat(C, A_ZExp, 1, 0); //[A-Z]+
  TEST_ASSERT(A_ZExpRep);

  pexl_Ref refA_ZRangeRep = pexl_bind(C, A_ZExpRep, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refA_ZRangeRep, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "[A-Z]+";

  input = "G";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *a_zExp = pexl_match_range(C, 'a', 'z'); //[a-z]
  TEST_ASSERT(a_zExp);
  pexl_Ref refa_zExp = pexl_bind(C, a_zExp, NULL);
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refa_zExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "[a-z]";
  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "G";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *letter = pexl_choice(C, A_ZExp, a_zExp); //[A-Z]
  TEST_ASSERT(letter);

  pexl_Ref refLetter = pexl_bind(C, letter, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refLetter, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "letter";

  input = "G";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  //

  pexl_Expr *A_FExp = pexl_match_range(C, 'A', 'F'); //[A-F]
  TEST_ASSERT(letter);

  pexl_Ref refA_FExp = pexl_bind(C, A_FExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refA_FExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "A-F";

  input = "F";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "f";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "FQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  //
  pexl_Expr *a_fExp = pexl_match_range(C, 'a', 'f'); //[a-f]
  TEST_ASSERT(letter);

  pexl_Ref refa_fExp = pexl_bind(C, a_fExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refa_fExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "a_f";

  input = "F";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "f";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "fqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  //

  pexl_Expr *hexletter = pexl_choice(C, A_FExp, a_fExp); //[A-Z]
  TEST_ASSERT(hexletter);

  pexl_Ref refHexLetter = pexl_bind(C, letter, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refHexLetter, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "hex-letter";

  input = "F";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "f";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *xdigit = pexl_choice(C, hexletter, numExp); //[A-Fa-f0-9]
  TEST_ASSERT(letter);

  pexl_Ref refxLetter = pexl_bind(C, xdigit, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refxLetter, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "xletter";

  input = "F";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "f";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "FQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "fqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  //

  pexl_Expr *Ee = pexl_match_set(C, "Ee", 2); //[E/e]
  TEST_ASSERT(Ee);

  pexl_Ref refEe = pexl_bind(C, Ee, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refEe, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "E/e";

  input = "F";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "e";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "E";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *alphaNum = pexl_choice(C, letter, numRange); //[A-Z]
  TEST_ASSERT(alphaNum);

  pexl_Ref refAlphaNum = pexl_bind(C, alphaNum, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refAlphaNum, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "[A-z,0-9]";

  input = "G";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *alphaNumRep = pexl_repeat(C, alphaNum, 1, 0);

  TEST_ASSERT(alphaNumRep);

  pexl_Ref refAlphaNumRep = pexl_bind(C, alphaNumRep, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refAlphaNumRep, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "[A-z,0-9]+";

  input = "G";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "GQZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "gqz";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "GQZgqz123aB3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *atExp = pexl_match_set(C, "@", 1);

  TEST_ASSERT(atExp);

  pexl_Ref refatExp = pexl_bind(C, atExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refatExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "@";

  input = "G";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "@";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *hypExp = pexl_match_set(C, "-", 1);

  TEST_ASSERT(hypExp);

  pexl_Ref refhypExp = pexl_bind(C, hypExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refhypExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "-";

  input = "@";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "-";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *dotExp = pexl_match_set(C, ".", 1);

  TEST_ASSERT(dotExp);

  pexl_Ref refdotExp = pexl_bind(C, dotExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refdotExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = ".";

  input = "@";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = ".";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *dotOptExp = pexl_repeat(C, dotExp, 0, 1);

  TEST_ASSERT(dotOptExp);

  pexl_Ref refdotOptExp = pexl_bind(C, dotOptExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refdotOptExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = ".?";

  input = "@";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  input = ".";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *colonExp = pexl_match_set(C, ":", 1);
  pexl_Expr *doublecolon = pexl_match_string(C, "::");

  TEST_ASSERT(colonExp);

  pexl_Ref refcolonExp = pexl_bind(C, colonExp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refcolonExp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = ":";

  input = "@";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = ":";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  /*local alias ipv4_component =  [0] /
                               {[1] [0-9]? [0-9]?} /
                               {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2]
     [0-9]?} /
                               {[3-9] [0-9]?}
  */
  pexl_Expr *numOpt = pexl_repeat(C, numExp, 0, 1);
  TEST_ASSERT(numOpt);

  pexl_Ref refnumOpt = pexl_bind(C, numOpt, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refnumOpt, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "Num Opt";

  input = "g";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  input = "1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *seq_2_range0_4 =
      pexl_seq_f(C, pexl_match_set(C, "2", 1), pexl_match_range(C, '0', '4'));
  pexl_Expr *m2 = pexl_match_set(C, "2", 1);
  pexl_Expr *m3_9 = pexl_match_range(C, '3', '9');

  pexl_Expr *ipv4_component = pexl_capture_f(
      C, "ipv4_component",
      pexl_choice_f(
          C, pexl_match_set(C, "0", 1), //[0] /
          pexl_choice_f(
              C,
              pexl_seq_f(C, pexl_match_set(C, "1", 1),
                         pexl_repeat(C, numOpt, 0, 2)), // [1] [0-9]?[0-9]?
              pexl_choice_f(
                  C,
                  pexl_seq(C, seq_2_range0_4,
                           numExp), //[2][0-4]
                  pexl_choice_f(C,
                                pexl_seq_f(C, pexl_match_string(C, "25"),
                                           pexl_match_range(C, '0', '5')),
                                pexl_choice_f(C, pexl_seq(C, m2, numOpt),
                                              pexl_seq(C, m3_9, numOpt)))))));

  TEST_ASSERT(ipv4_component);

  pexl_Ref refipv4_component = pexl_bind(C, ipv4_component, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refipv4_component, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ipv4_component";

  input = "0";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "234";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *ipv6_component =
      pexl_capture_f(C, "ipv6_component", pexl_repeat(C, xdigit, 1, 4));

  TEST_ASSERT(ipv6_component);

  pexl_Ref refipv6_component = pexl_bind(C, ipv6_component, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refipv6_component, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ipv6_component";

  input = "0";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "234";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  /*
    -----------------------------------------------------------------------------
  */

  TEST_SECTION("ipv4_address");
  pexl_Expr *eofExp = pexl_neg_lookahead_f(C, pexl_match_any(C, 1));
  pexl_Expr *end_ipv4_add =
      pexl_seq_f(C, pexl_repeat_f(C, pexl_seq(C, dotExp, ipv4_component), 3, 3),
                 pexl_neg_lookahead(C, numExp));
  pexl_Expr *ipv4_address = pexl_capture_f(
      C, "ipv4_address", pexl_seq(C, ipv4_component, end_ipv4_add));
  TEST_ASSERT(ipv4_address);
  printf("ipv4_address is....\n");
  pexl_print_Expr(ipv4_address, 0, C);

  pexl_Ref refipv4_address = pexl_bind(C, ipv4_address, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refipv4_address, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ipv4_address";

  input = "0";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "234";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  //-- test ipv4 accepts "0.0.0.0", "1.2.234.123",
  input = "0.0.0.0";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "1.2.234.123";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  //-- test ipv4 rejects "1234.1.2.3", "1.2.3", "111.222.333.",
  //"111.222.333..444"

  input = "1234.1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "111.222.333.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "111.222.333..444";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  //-- test ipv4 rejects "999.999.999.999", "256.1.1.1", "1.256.1.1",
  //"1.1.256.1", "1.1.1.256"
  input = "999.999.999.999";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "256.1.1.1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.256.1.1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.1.256.1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.1.1.256";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  pexl_Expr *not_ipv4 = pexl_neg_lookahead(C, ipv4_address);
  pexl_Expr *notipv4_ipv6_comp = pexl_seq(C, not_ipv4, ipv6_component);

  pexl_Expr *ipv6_rest =
      pexl_capture_f(C, "ipv6_rest", pexl_seq(C, colonExp, notipv4_ipv6_comp));

  TEST_ASSERT(ipv6_rest);

  pexl_Ref refipv6_rest = pexl_bind(C, ipv6_rest, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refipv6_rest, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ipv6_rest";

  input = ":0";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = ":23";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = ":234";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *r7_ipv6_Rest = pexl_repeat(C, ipv6_rest, 7, 7);
  pexl_Expr *r4Opt_ipv6_Rest = pexl_repeat(C, ipv6_rest, 0, 4);

  pexl_Expr *r2_ipv6_Rest = pexl_repeat(C, ipv6_rest, 2, 2);
  pexl_Expr *r2Opt_ipv6_Rest = pexl_repeat(C, ipv6_rest, 0, 2);
  pexl_Expr *ipv6compr2Opt_ipv6_Rest =
      pexl_seq(C, ipv6_component, r2Opt_ipv6_Rest);
  pexl_Expr *r3_ipv6_Rest = pexl_repeat(C, ipv6_rest, 3, 3);
  pexl_Expr *ipv6_Opt = pexl_repeat(C, ipv6_rest, 0, 1);

  pexl_Expr *ipv6_comp_rest_Opt = pexl_seq(C, ipv6_component, ipv6_Opt);
  pexl_Expr *r4_ipv6_Rest = pexl_repeat(C, ipv6_rest, 4, 4);
  pexl_Expr *r5_ipv6_Rest = pexl_repeat(C, ipv6_rest, 5, 5);
  pexl_Expr *ipv6_comp_r5_rest = pexl_seq(C, ipv6_component, r5_ipv6_Rest);
  pexl_Expr *r5Opt_rest = pexl_repeat(C, ipv6_rest, 0, 5);

  pexl_Expr *ipv6_address = pexl_capture_f(
      C, "ip_address_v6",
      pexl_choice_f(
          C,
          pexl_seq(C, ipv6_component,
                   r7_ipv6_Rest), //{ ipv6_component ipv6_rest{7} } /
          pexl_choice_f(
              C,
              pexl_seq_f(C, pexl_seq(C, ipv6_component, doublecolon),
                         pexl_seq(C, ipv6_component,
                                  r4Opt_ipv6_Rest)), //{ ipv6_component "::"
                                                     // ipv6_component
                                                     // ipv6_rest{0,4} } /
              pexl_choice_f(
                  C,
                  pexl_seq_f(
                      C, pexl_seq(C, ipv6_component, ipv6_rest),
                      pexl_seq_f(C, pexl_seq(C, doublecolon, ipv6_component),
                                 pexl_repeat(
                                     C, ipv6_rest, 0,
                                     3))), // { ipv6_component ipv6_rest{1} "::"
                                           // ipv6_component ipv6_rest{0,3} }
                  pexl_choice_f(
                      C,
                      pexl_seq_f(
                          C, pexl_seq(C, ipv6_component, r2_ipv6_Rest),
                          pexl_seq(
                              C, doublecolon,
                              ipv6compr2Opt_ipv6_Rest)), //{ ipv6_component
                                                         // ipv6_rest{2}
                                                         // "::"
                                                         // ipv6_component
                                                         // ipv6_rest{0,2} }
                      pexl_choice_f(
                          C,
                          pexl_seq_f(
                              C, pexl_seq(C, ipv6_component, r3_ipv6_Rest),
                              pexl_seq(
                                  C, doublecolon,
                                  ipv6_comp_rest_Opt)), //{ ipv6_component
                                                        // ipv6_rest{3} "::"
                                                        // ipv6_component
                                                        // ipv6_rest{0,1} }
                                                        // /
                          pexl_choice_f(
                              C,
                              pexl_seq_f(
                                  C, pexl_seq(C, ipv6_component, r4_ipv6_Rest),
                                  pexl_seq(
                                      C, doublecolon,
                                      ipv6_component)), // { ipv6_component
                                                        // ipv6_rest{4} "::"
                                                        // ipv6_component }
                              pexl_choice_f(
                                  C,
                                  pexl_seq(C, ipv6_comp_r5_rest,
                                           doublecolon), //{ ipv6_component
                                                         // ipv6_rest{5} "::" }
                                  pexl_choice_f(
                                      C,
                                      pexl_seq_f(
                                          C, pexl_repeat(C, doublecolon, 1, 1),
                                          pexl_seq(
                                              C, ipv6_component,
                                              r5Opt_rest)), //{ "::"
                                                            // ipv6_component
                                                            // ipv6_rest{0,5}
                                                            //}
                                      pexl_repeat(C, doublecolon, 1,
                                                  1) // this is kinda really
                                                     // dumb attempt to redirect
                                                     // so I can free
                                      )))))))));
  TEST_ASSERT(ipv6_address);
  pexl_Expr *ipv6_address_test = pexl_seq(C, ipv6_address, eofExp);
  pexl_Ref refip_address_v6 =
      pexl_bind(C, ipv6_address_test, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refip_address_v6, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ip_address_v6";

  input = "::192.9.5.5";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "::FFFF:129.144.52.38";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *c_ipv4_ipv6 = pexl_choice(C, ipv4_address, ipv6_component);

  pexl_Expr *ipv6_end =
      pexl_capture_f(C, "ipv6_end", pexl_seq(C, colonExp, c_ipv4_ipv6));

  TEST_ASSERT(ipv6_end);
  pexl_Expr *r3Opt_ipv6_Rest = pexl_repeat(C, ipv6_rest, 0, 3);
  pexl_Expr *ipv6_com_r3Opt_ipv6_Rest =
      pexl_seq(C, ipv6_component, r3Opt_ipv6_Rest);
  pexl_Expr *r6_ipv6_rest = pexl_repeat(C, ipv6_rest, 6, 6);

  pexl_Expr *r6_ipv6_rest_end = pexl_seq(C, r6_ipv6_rest, ipv6_end);
  pexl_Expr *r2Opt_ipv6_Rest_end = pexl_seq(C, r2Opt_ipv6_Rest, ipv6_end);
  pexl_Expr *ipv6_com_r2Opt_ipv6_Rest_end =
      pexl_seq(C, ipv6_component, r2Opt_ipv6_Rest_end);
  // pexl_Expr *r2_ipv6_Rest =pexl_repeat(C, ipv6_rest, 2, 2);
  pexl_Expr *ipv6_comp_r2_Rest = pexl_seq(C, ipv6_component, r2_ipv6_Rest);
  pexl_Expr *ipv6_rest_opt = pexl_repeat(C, ipv6_rest, 0, 1);
  pexl_Expr *ipv6_comp_end = pexl_seq(C, ipv6_component, ipv6_end);
  // pexl_Expr *r4Opt_ipv6_Rest = pexl_repeat(C, ipv6_rest, 0, 4);
  pexl_Expr *r4Opt_ipv6_Rest_End = pexl_seq(C, r4Opt_ipv6_Rest, ipv6_end);

  pexl_Expr *ipv6_comp_r4Opt_ipv6_Rest_End =
      pexl_seq(C, ipv6_component, r4Opt_ipv6_Rest_End);

  pexl_Expr *r3_ipv6_Rest_seq_col_end =
      pexl_seq_f(C, pexl_repeat(C, ipv6_rest, 3, 3),
                 pexl_seq(C, doublecolon, ipv6_comp_end));
  pexl_Expr *r4_ipv6_Rest_seq_col_end =
      pexl_seq_f(C, pexl_repeat(C, ipv6_rest, 4, 4),
                 pexl_seq(C, doublecolon, ipv4_address));

  // pexl_Expr *ipv6_comp_rest_Opt =pexl_seq(C, ipv6_component,   ipv6_rest_opt
  // ) pexl_free_Expr()

  // pexl_Expr *r2Opt_ipv6_Rest = pexl_repeat(C, ipv6_rest, 0, 2)
  pexl_Expr *ipv6_mixed = pexl_capture_f(
      C, "ipv6_mixed",
      pexl_choice_f(
          C,
          pexl_seq(
              C, ipv6_component,
              r6_ipv6_rest_end), //{ ipv6_component ipv6_rest{6} ipv6_end } //
          pexl_choice_f(
              C,
              pexl_seq_f(
                  C, pexl_seq(C, ipv6_component, doublecolon),
                  pexl_seq(C, ipv6_com_r3Opt_ipv6_Rest,
                           ipv6_end)), //{ ipv6_component "::" ipv6_component
                                       // ipv6_rest{0,3} ipv6_end} /
              pexl_choice_f(
                  C,
                  pexl_seq_f(
                      C, pexl_seq(C, ipv6_component, ipv6_rest),
                      pexl_seq(
                          C, doublecolon,
                          ipv6_com_r2Opt_ipv6_Rest_end)), //{ ipv6_component
                                                          // ipv6_rest{1}
                                                          //"::" ipv6_component
                                                          // ipv6_rest{0,2}
                                                          // ipv6_end} /
                  pexl_choice_f(
                      C,
                      pexl_seq_f(
                          C, pexl_seq(C, ipv6_comp_r2_Rest, doublecolon),
                          pexl_seq(C, ipv6_comp_rest_Opt,
                                   ipv6_end)), //{ ipv6_component ipv6_rest{2}
                                               //"::" ipv6_component
                                               // ipv6_rest{0,1} ipv6_end} /
                      pexl_choice_f(
                          C,
                          pexl_seq(C, ipv6_component, r3_ipv6_Rest_seq_col_end

                                   ), //{ ipv6_component
                                      // ipv6_rest{3} "::"
                                      // ipv6_component
                                      // ipv6_end } /
                          pexl_choice_f(
                              C,
                              pexl_seq(
                                  C, ipv6_component,
                                  r4_ipv6_Rest_seq_col_end), //{ ipv6_component
                                                             // ipv6_rest{4}
                                                             // "::" ipv4 } /
                              pexl_choice_f(
                                  C,
                                  pexl_seq(
                                      C, doublecolon,
                                      ipv6_comp_r4Opt_ipv6_Rest_End), //{ "::"
                                                                      // ipv6_component
                                                                      // ipv6_rest{0,4}
                                                                      // ipv6_end}
                                                                      // /
                                  pexl_seq(C, doublecolon,
                                           ipv4_address) //{ "::" ipv4 }
                                  ))))))));

  TEST_ASSERT(ipv6_mixed);

  pexl_Expr *ipv6 =
      pexl_capture_f(C, "ipv6", pexl_choice(C, ipv6_mixed, ipv6_address));

  pexl_Expr *ipv6_test = pexl_seq(C, ipv6, eofExp);

  TEST_ASSERT(ipv6);

  pexl_Ref refipv6 = pexl_bind(C, ipv6, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refipv6, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ipv6";

  input = "2010:836B:4179::836B:4179";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::face:b00c";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::192.9.5.5";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::FFFF:129.144.52.38";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "2001:0db8:0000:0000:0000:ff00:0042:8329";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "2001:db8:0:0:0:ff00:42:8329";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "2001:db8::ff00:42:8329";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "1080:0:0:0:8:800:200C:4171";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "3ffe:2a00:100:7031::1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "1080::8:800:200C:417A";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::192.9.5.5";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "::FFFF:129.144.52.38";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  /*
    -----------------------------------------------------------------------------
  */
  TEST_SECTION("Domain Names");

  pexl_Expr *port = pexl_capture(C, "port", numRangeRep);
  TEST_ASSERT(port);

  patname = "port";

  pexl_Ref refport = pexl_bind(C, port, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refport, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 2));

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = ".12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *portSpec =
      pexl_capture_f(C, "port spec", pexl_seq(C, colonExp, port));

  TEST_ASSERT(portSpec);

  patname = "portSpec";

  pexl_Ref refportSpec = pexl_bind(C, portSpec, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refportSpec, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = ":12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *s_hyp_alphaNum = pexl_seq(C, hypExp, alphaNum);
  pexl_Expr *let_dig_hyp = pexl_choice(C, alphaNum, s_hyp_alphaNum);

  TEST_ASSERT(let_dig_hyp);

  patname = "let_dig_hyp";

  pexl_Ref reflet_dig_hyp = pexl_bind(C, let_dig_hyp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, reflet_dig_hyp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "-12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 2));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  pexl_Expr *r62Opt_let_dig_hyp = pexl_repeat(C, let_dig_hyp, 0, 62);
  pexl_Expr *subdomain =
      pexl_capture_f(C, "subdomain", pexl_seq(C, alphaNum, r62Opt_let_dig_hyp));
  //
  TEST_ASSERT(subdomain);

  patname = "subdomain";

  pexl_Ref refsubdomain = pexl_bind(C, subdomain, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refsubdomain, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "ags-ajf";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "-12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));


  pexl_Expr *plusmin = pexl_match_set(C, "+-", 2);

  TEST_ASSERT(plusmin);

  patname = "plusmin";

  pexl_Ref refplusmin = pexl_bind(C, plusmin, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refplusmin, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "+";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "-";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *plusminOpt = pexl_repeat(C, plusmin, 0, 1);

  TEST_ASSERT(plusminOpt);

  patname = "plusminOpt";

  pexl_Ref refplusminOpt = pexl_bind(C, plusminOpt, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refplusminOpt, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "+";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "-";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  pexl_Expr *e_plus_min_opt = pexl_seq(C, Ee, plusminOpt);

  TEST_ASSERT(e_plus_min_opt);

  patname = "e_plus_min_opt";

  pexl_Ref refe_plus_min_opt = pexl_bind(C, e_plus_min_opt, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refe_plus_min_opt, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "E+";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "e-";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "e12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *exponent =
      pexl_capture_f(C, "exponent", pexl_seq(C, e_plus_min_opt, numRangeRep));

  TEST_ASSERT(exponent);

  patname = "exponent";

  pexl_Ref refexponent = pexl_bind(C, exponent, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refexponent, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "E+12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "e-21111111";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "e12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *mantissa = pexl_capture_f(
      C, "mantissa",
      pexl_seq_f(
          C, pexl_seq(C, plusminOpt, numRangeRep),
          pexl_seq_f(C,
                     pexl_repeat_f(C, pexl_seq(C, dotExp, numRangeRep), 0, 1),
                     pexl_choice_f(C, pexl_lookahead(C, exponent),
                                   pexl_neg_lookahead(C, hexletter)))));

  TEST_ASSERT(mantissa);

  patname = "mantissa";

  pexl_Ref refmantissa = pexl_bind(C, mantissa, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refmantissa, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "+12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "-21111111";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *ExpOpt = pexl_repeat(C, exponent, 0, 1);
  pexl_Expr *float_exp =
      pexl_capture_f(C, "float", pexl_seq(C, mantissa, ExpOpt));

  TEST_ASSERT(float_exp);

  patname = "float_exp";

  pexl_Ref reffloat_exp = pexl_bind(C, float_exp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, reffloat_exp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12e+12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "-1e-21111111";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "12E12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *repeatdotdomain =
      pexl_repeat_f(C, pexl_seq(C, dotExp, subdomain), 1, 0);
  pexl_Expr *seq_repeatdotdomain_dotOpt =
      pexl_seq(C, repeatdotdomain, dotOptExp);
  pexl_Expr *fqdn_strict = pexl_capture_f(
      C, "fqdn strict", pexl_seq(C, subdomain, seq_repeatdotdomain_dotOpt));

  TEST_ASSERT(fqdn_strict);

  patname = "fqdn_strict";

  pexl_Ref reffqdn_strict = pexl_bind(C, fqdn_strict, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, reffqdn_strict, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12e.12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "1e.21111111.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "12.12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "3.1415926536";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "1.2.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "1.2+ ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 3));
  input = "-3.14";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "6.02e23";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "1.2.3.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *not_let_dig_hyp = pexl_neg_lookahead(C, let_dig_hyp);

  TEST_ASSERT(not_let_dig_hyp);

  patname = "not_let_dig_hyp";

  pexl_Ref refnot_let_dig_hyp =
      pexl_bind(C, not_let_dig_hyp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refnot_let_dig_hyp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12e.12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1e.21111111.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "12.12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = ";3.1415926536";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));
  input = "@1.2.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));
  input = "1.2+ ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "-3.14";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "+6.02e23";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  input = "-1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "(1.2.3.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  pexl_Expr *num_float_not_let_dig_hyp =
      pexl_seq(C, float_exp, not_let_dig_hyp);

  TEST_ASSERT(num_float_not_let_dig_hyp);

  patname = "num_float_not_let_dig_hyp";

  pexl_Ref refnum_float_not_let_dig_hyp =
      pexl_bind(C, num_float_not_let_dig_hyp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refnum_float_not_let_dig_hyp, NULL, NULL,
                     PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12e+12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "1e-21-111111.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "12.12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 5));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = ";3.1415926536";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "@1.2.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "1.2+e12 ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 3));
  input = "-3.14";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "602e+23";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "-1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 4));
  input = "(1.2.3.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *notthat = pexl_neg_lookahead(C, num_float_not_let_dig_hyp);

  TEST_ASSERT(notthat);

  patname = "notthat";

  pexl_Ref refnotthat = pexl_bind(C, notthat, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refnotthat, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  input = "12e+12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1e-21-111111.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  input = "12.12";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "!ko";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = ";3.1415926536";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));
  input = "@1.2.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));
  input = "1.2+e12 ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "-3.14";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "602e+23";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "-1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "(1.2.3.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 0));

  pexl_Expr *fqdn_practical =
      pexl_capture_f(C, "fqdn practical", pexl_seq(C, notthat, fqdn_strict));

  TEST_ASSERT(fqdn_practical);

  pexl_Ref reffqdn_practical = pexl_bind(C, fqdn_practical, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, reffqdn_practical, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "fqdn_practical";

  input = "1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "3.1415926536";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "1.2.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "1.2+ ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "-3.14";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "6.02e23";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "1.2.3";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));
  input = "1.2.3.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  /*
    -----------------------------------------------------------------------------
  */
  TEST_SECTION("URI");

  /*
    -- URI (https://tools.ietf.org/html/rfc3986)
    -- Notes:
    -- (1) The "future ip literal" is not implemented (page 19, https://tools.ietf.org/html/rfc3986)
    -- (2) Relative URIs of the form "/foo/bar.txt" will match 'path' but not 'uri'
  */
  const char *sub_delims_string = "!$&'()*+,;=";
  pexl_Expr *sub_delims =
      pexl_match_set(C, sub_delims_string, strlen(sub_delims_string));

  //char *gen_delims_string = ":/?#[]@";
  //pexl_Expr *gen_delims =
  //   pexl_match_set(C, gen_delims_string, strlen(gen_delims_string));
  pexl_Expr *pct = pexl_match_set(C, "%", 1);
  pexl_Expr *doublexdigit = pexl_repeat(C, xdigit, 2, 2);
  pexl_Expr *pct_encoded = pexl_seq(C, pct, doublexdigit);

  const char *unreserved_symbol_string = "-_~";
  pexl_Expr *unreserved_symbol = pexl_match_set(
      C, unreserved_symbol_string, strlen(unreserved_symbol_string));
  pexl_Expr *unreserved = pexl_choice(C, alphaNum, unreserved_symbol);
  pexl_Expr *colonAt = pexl_choice(C, colonExp, atExp);
  pexl_Expr *pchar = pexl_choice_f(C, pexl_choice(C, unreserved, pct_encoded),
                                   pexl_choice(C, sub_delims, colonAt));
  pexl_Expr *pct_sub = pexl_choice(C, pct_encoded, sub_delims);
  pexl_Expr *un_pct_sub = pexl_choice(C, unreserved, pct_sub);

  pexl_Expr *r_unreserved_pct_sub = pexl_repeat(C, un_pct_sub, 0, 0);
  pexl_Expr *regname =
      pexl_capture_f(C, "regname", pexl_seq(C, alphaNum, r_unreserved_pct_sub));

  pexl_Expr *dotReg = pexl_seq(C, dotExp, regname);
  pexl_Expr *r_dotReg = pexl_repeat(C, dotReg, 0, 0);
  pexl_Expr *reg_r_dot_reg = pexl_seq(C, regname, r_dotReg);
  pexl_Expr *registered_name = pexl_capture_f(
      C, "registered_name", pexl_seq(C, reg_r_dot_reg, dotOptExp));
  pexl_Expr *openBracket = pexl_match_set(C, "[", 1);

  TEST_ASSERT(openBracket);

  pexl_Ref refopenBracket = pexl_bind(C, openBracket, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refopenBracket, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "openBracket";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "[";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  pexl_Expr *closeBracket = pexl_match_set(C, "]", 1);

  TEST_ASSERT(ipv6);

  pexl_Ref refipv62 = pexl_bind(C, ipv6, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refipv62, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ipv6";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "3ffe:2a00:100:7031::1";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  TEST_ASSERT(closeBracket);

  pexl_Ref refcloseBracket = pexl_bind(C, closeBracket, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refcloseBracket, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "closeBracket";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  //

  pexl_Expr *openIp = pexl_seq(C, openBracket, ipv6);

  TEST_ASSERT(openIp);

  pexl_Ref refopenIp = pexl_bind(C, openIp, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refopenIp, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "openIp";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "[FEDC:BA98:7654:3210:FEDC:BA98:7654:3210";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *ip_literal =
      pexl_capture_f(C, "ip_literal", pexl_seq(C, openIp, closeBracket));
  TEST_ASSERT(ip_literal);

  pexl_Ref refip_literal = pexl_bind(C, ip_literal, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refip_literal, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "ip_literal";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "[FEDC:BA98:7654:3210:FEDC:BA98:7654:3210]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "[1080:0:0:0:8:800:200C:417A]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *ip_host = pexl_choice(C, ip_literal, ipv4_address);
  pexl_Expr *host =
      pexl_capture_f(C, "host", pexl_choice(C, ip_host, registered_name));

  TEST_ASSERT(host);
  pexl_Expr *host_test = pexl_seq(C, host, eofExp);
  pexl_Ref refhost = pexl_bind(C, host_test, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refhost, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "host";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "abc";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "ZZZZZZ";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "Z-9";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "a.edu";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "A.BC.EDU";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "X-Y.CS.CORNELL.EDU";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "SRI-NIC.ARPA";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "ibm.com.";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "9in.nails.band";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "1AAA.SRI-NIC.ARPA";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "XY-.CS.CORNELL.EDU";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "[FEDC:BA98:7654:3210:FEDC:BA98:7654:3210]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "[1080:0:0:0:8:800:200C:417A]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "[3ffe:2a00:100:7031::1]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "[1080::8:800:200C:417A]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "[::192.9.5.5]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "[1080:0:0:0:8:800:200C:417A]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "[::FFFF:129.144.52.38]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "[1080::8:800:200C:417A]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "[2010:836B:4179::836B:4179]";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "ibm.com:443";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "example.com.:80";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = ".EDU";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "ibm.com:";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "ibm.com:x";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *userinfo = pexl_capture_f(
      C, "userinfo",
      pexl_repeat_f(C, pexl_choice(C, un_pct_sub, colonExp), 0, 0));
  pexl_Expr *portSpecOpt = pexl_repeat(C, portSpec, 0, 1);
  pexl_Expr *authority = pexl_capture_f(
      C, "authority",
      pexl_seq_f(C, pexl_repeat_f(C, pexl_seq(C, userinfo, atExp), 0, 1),
                 pexl_repeat_f(C, pexl_seq(C, host, portSpecOpt), 0, 1)));
  TEST_ASSERT(authority);

  pexl_Ref refauthority = pexl_bind(C, authority, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refauthority, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "authority";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

//   pexl_Expr *segment_nz_nc =
//       pexl_repeat_f(C, pexl_seq(C, un_pct_sub, atExp), 1, 0);
  pexl_Expr *segment_nz = pexl_repeat_f(C, pexl_seq(C, pchar, dotOptExp), 1, 0);
  TEST_ASSERT(segment_nz);

  pexl_Ref refsegment_nz = pexl_bind(C, segment_nz, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refsegment_nz, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "segment_nz";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "John.Doe@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *segment = pexl_repeat_f(C, pexl_seq(C, pchar, dotOptExp), 0, 0);
//   pexl_Expr *path_empty = pexl_match_set(C, "", 0);
  pexl_Expr *path_div = pexl_match_set(C, "/", 1);
  pexl_Expr *divpath_star =
      pexl_repeat_f(C, pexl_seq(C, path_div, segment), 0, 0);

  pexl_Expr *path_rootless =
      pexl_capture_f(C, "rootless path", pexl_seq(C, segment_nz, divpath_star));
  TEST_ASSERT(path_rootless);

  pexl_Ref refpath_rootless = pexl_bind(C, path_rootless, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refpath_rootless, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "path_rootless";

  input = "a";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "John.Doe@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

//   pexl_Expr *path_noscheme =
//     pexl_capture_f(C, "schemeless path", pexl_seq(C, segment_nz_nc, divpath_star));
  pexl_Expr *r_pathRootless = pexl_repeat(C, path_rootless, 0, 0);
  pexl_Expr *path_absolute =
      pexl_capture_f(C, "absolute path", pexl_seq(C, path_div, r_pathRootless));
  pexl_Expr *path_abempty = pexl_capture(C, "abempty path", divpath_star);
  pexl_Expr *divpath_plus =
      pexl_repeat_f(C, pexl_seq(C, path_div, segment), 1, 0);
  pexl_Expr *rootlessish = pexl_seq(C, segment_nz, divpath_plus);
  pexl_Expr *path =
      pexl_capture_f(C, "path", pexl_choice(C, divpath_plus, rootlessish));

  TEST_ASSERT(path);

  pexl_Ref refpath = pexl_bind(C, path, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refpath, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "path";

  input = "/other/link.html";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "/";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "/";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "/a/b";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "a/b";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "a.b/";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "a/b/c.d";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *doublepathdiv = pexl_repeat(C, path_div, 2, 2);
  pexl_Expr *doublepath_auth = pexl_seq(C, doublepathdiv, authority);
  pexl_Expr *doublepath_auth_abempty =
      pexl_seq(C, doublepath_auth, path_abempty);
  pexl_Expr *aborroot = pexl_choice(C, path_absolute, path_rootless);
  pexl_Expr *authpath = pexl_capture_f(
      C, "authpath", pexl_choice(C, doublepath_auth_abempty, aborroot));

  TEST_ASSERT(aborroot);

  pexl_Ref refauthpath = pexl_bind(C, aborroot, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refauthpath, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "aborroot";

  input = "/other/link.html";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "John.Doe@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *quest = pexl_match_set(C, "?", 1);
  pexl_Expr *divquest = pexl_choice(C, path_div, quest);
  pexl_Expr *extension = pexl_capture_f(
      C, "extension", pexl_repeat_f(C, pexl_choice(C, pchar, divquest), 0, 0));
  pexl_Expr *query = pexl_capture_f(
      C, "query", pexl_repeat_f(C, pexl_seq(C, quest, extension), 0, 1));
  pexl_Expr *hash = pexl_match_set(C, "#", 1);
  pexl_Expr *fragment = pexl_capture_f(
      C, "fragment", pexl_repeat_f(C, pexl_seq(C, hash, extension), 0, 1));
  const char *scheme_symb_string = "+.-";
  pexl_Expr *scheme_sym =
      pexl_match_set(C, scheme_symb_string, strlen(scheme_symb_string));
  pexl_Expr *scheme_alnumsymb =
      pexl_repeat_f(C, pexl_choice(C, alphaNum, scheme_sym), 0, 0);
  pexl_Expr *scheme =
      pexl_capture_f(C, "scheme", pexl_seq(C, letter, scheme_alnumsymb));
  pexl_Expr *authpathOpt = pexl_repeat(C, authpath, 0, 1);
  pexl_Expr *uri_end = pexl_seq(C, query, fragment);
  pexl_Expr *uri_rest = pexl_seq(C, authpathOpt, uri_end);
  TEST_ASSERT(uri_rest);

//   pexl_Expr *uri_rest_test = pexl_seq(C, uri_rest, eofExp);

  pexl_Ref refuri_rest = pexl_bind(C, uri_rest, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refuri_rest, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "uri_rest";

  input = "John.Doe@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "mailto:";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *uri_begin = pexl_seq(C, scheme, colonExp);
  TEST_ASSERT(uri_begin);

  pexl_Expr *uri_begin_test = pexl_seq(C, uri_begin, eofExp);
  pexl_Ref refuri_begin = pexl_bind(C, uri_begin_test, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refuri_begin, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "uri_begin";

  input = "ftp:";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "mailto:";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  pexl_Expr *uri = pexl_capture_f(C, "uri", pexl_seq(C, uri_begin, uri_rest));
  TEST_ASSERT(uri);
//   pexl_Expr *uri_test = pexl_seq(C, uri, eofExp);

  pexl_Ref refuri = pexl_bind(C, uri, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refuri, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "uri";

  input = "ftp://ftp.is.co.za/rfc/rfc1808.txt";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "http://www.ietf.org/rfc/rfc2396.txt";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "ldap://[2001:db8::7]/c=GB?objectClass?one";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "mailto:John.Doe@example.com";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "news:comp.infosystems.www.servers.unix";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "tel:+1-816-555-1212";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "telnet://192.0.2.16:80/";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "urn:oasis:names:specification:docbook:dtd:xml:4.1.2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "http://www.google.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "http://google.com/";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "https://www.gitlab.com/rosie-pattern-language";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "ftp://some.ftp.net/path/to/file.zip";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "http://example.com/mypage.html";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "ftp://example.com/download.zip";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "mailto:user@example.com";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "file:///home/user/file.txt";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "tel:1-888-555-5555";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "telnet://192.0.2.16:80/";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "http://example.com/resource?foo=bar#fragment";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "foo:";

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "foo";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  /*
    -----------------------------------------------------------------------------
  */
  TEST_SECTION("URL");

  pexl_Expr *notcolon = pexl_neg_lookahead(C, colonExp);
  pexl_Expr *url_strict_begin = pexl_seq(C, uri_begin, notcolon);
  pexl_Expr *doubleforward = pexl_lookahead_f(C, pexl_match_string(C, "//"));
  pexl_Expr *url_begin = pexl_seq(C, uri_begin, doubleforward);
  pexl_Expr *url_rest = pexl_seq(C, authpath, uri_end);
  pexl_Expr *url_strict =
      pexl_capture_f(C, "url strict", pexl_seq(C, url_strict_begin, url_rest));
  pexl_Expr *url = pexl_capture_f(C, "url", pexl_seq(C, url_begin, url_rest));

  // url_strict = { scheme ":" !":" authpath { "?" query }? { "#" fragment }? }
  // url = { scheme ":" >"//" authpath { "?" query }? { "#" fragment }? }

  /*
    -- test url_strict accepts ""
    -- test url_strict rejects "", ""
  */

  TEST_ASSERT(url_strict);

  pexl_Ref refurl_strict = pexl_bind(C, url_strict, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refurl_strict, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "url_strict";

  input = "https://rosie-lang.org";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "ftp://example.net/Rosie/The/Riveter#WorldWar2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "a:b";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "a:b?c";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "a:b?c#d";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "bold:net.*=red:net.ipv6=red;";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "foo:";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "xyz::w";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  TEST_ASSERT(url);

  pexl_Ref refurl = pexl_bind(C, url, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refurl, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "url";

  input = "https://rosie-lang.org";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "ftp://example.net/Rosie/The/Riveter#WorldWar2";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "a:b";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "a:b?c";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "a:b?c#d";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "bold:net.*=red:net.ipv6=red;";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "foo:";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "xyz::w";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  /*
    -----------------------------------------------------------------------------
  */
  TEST_SECTION("Email Addresses");

  const char *symb = "!#$%&'*+-/=?^_`{|}~";

  pexl_Expr *nameSymb = pexl_match_set(C, symb, strlen(symb));

  TEST_ASSERT(nameSymb);

  pexl_Ref refnameSymb = pexl_bind(C, nameSymb, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refnameSymb, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "nameSymb";

  input = "!#$%&'*+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "#$%&'*+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "$%&'*+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "&'*+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "'*+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "*+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "+-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "-/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));

  input = "/=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "=?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "?^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "^_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "_`.{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "`{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "{|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "|}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "}~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "~";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, 1));
  input = "";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  pexl_Expr *name_char = pexl_choice(C, alphaNum, nameSymb);
  pexl_Expr *name_char_rep = pexl_repeat(C, name_char, 1, 64);
  pexl_Expr *dot_name_char_rep = pexl_seq(C, dotExp, name_char_rep);
  pexl_Expr *dot_name_char_rep_star =
      pexl_repeat(C, dot_name_char_rep, 0,
                  64);
  // kind of an arbitary number here but basically hinting
  // at should not be more than 64 char in name this may be
  // a good use of where or if and similiar predicate exp
  // as opposed to some kind of choice between 1->64 look behind
  // going to differ from rpl here in an attempt to fix this.
  // what if unquoted name is namechar +{"." namechar+}*

  pexl_Expr *unquoted_name =
    pexl_capture_f(C,
		   "unquoted_name",
		   pexl_seq(C, name_char_rep, dot_name_char_rep_star));
  pexl_Expr *quote = pexl_match_set(C, "\"", 1);
  pexl_Expr *negquote = pexl_neg_lookahead(C, quote);
  pexl_Expr *anything = pexl_match_any(C, 1);
  pexl_Expr *anythingbutquote = pexl_seq(C, negquote, anything);
  pexl_Expr *anythingbutquoteRep = pexl_repeat(C, anythingbutquote, 1, 0);
  pexl_Expr *quote_anythingbutquoteRep = pexl_seq(C, quote, anythingbutquoteRep);
  pexl_Expr *quoted_name =
    pexl_capture_f(C, "quoted_name", pexl_seq(C, quote_anythingbutquoteRep, quote));

  pexl_Expr *name =
      pexl_capture_f(C, "name", pexl_choice(C, quoted_name, unquoted_name));

  pexl_Expr *nameAt = pexl_seq(C, name, atExp);
  pexl_Expr *email = pexl_capture_f(C, "email", pexl_seq(C, nameAt, host));

  /*
    -- test email accepts "me@here.com", "you+filter@somewhere.org", "k!!{}@example.com", "9name@gmail.com"
    -- test email accepts "customer/department=shipping@example.com", "$A12345@example.com"
    -- test email accepts "!def!xyz%abc@example.com", "_somename@example.com", "\"Joe.\\Blow\"@example.com"
    -- test email accepts "\"John Doe\"@example.com", "\"Can put @ signs here!\"@example.com"
    -- test email rejects "here.com", "foo@bar@example.com"
  */

  pexl_Expr *test_email = pexl_seq(C, email, eofExp);

  TEST_ASSERT(email);

  pexl_Ref refemail = pexl_bind(C, test_email, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refemail, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "email";

  input = "me@here.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "you+filter@somewhere.org";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "k!!{}@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "9name@gmail.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "customer/department=shipping@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "$A12345@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "!def!xyz%abc@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "_somename@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  input = "\"Joe.\\Blow\"@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "\"John Doe\"@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "\"Can put @ signs here!\"@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "here.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  input = "foo@bar@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, false, 0));

  /*
    -----------------------------------------------------------------------------
  */
  TEST_SECTION("HTTP commands");

//   pexl_Expr *http_command_name = pexl_capture_f(
//       C, "command name",
//       pexl_choice_f(
//           C, pexl_match_string(C, "GET"),
//           pexl_choice_f(
//               C, pexl_match_string(C, "HEAD"),
//               pexl_choice_f(
//                   C, pexl_match_string(C, "PUT"),
//                   pexl_choice_f(
//                       C, pexl_match_string(C, "POST"),
//                       pexl_choice_f(
//                           C, pexl_match_string(C, "DELETE"),
//                           pexl_choice_f(
//                               C, pexl_match_string(C, "TRACE"),
//                               pexl_choice_f(
//                                   C, pexl_match_string(C, "OPTIONS"),
//                                   pexl_choice_f(
//                                       C, pexl_match_string(C, "CONNECT"),
//                                       pexl_match_string(C, "PATCH"))))))))));
//   pexl_Expr *http_command =
//       pexl_capture_f(C, "http_command", pexl_seq(C, http_command_name, url));
//   pexl_Expr *http_version =
//       pexl_capture_f(C, "version",
//                      pexl_seq_f(C,
//                                 pexl_seq_f(C, pexl_match_string(C, "HTTP/"),
//                                            pexl_seq(C, numRangeRep, dotExp)),
//                                 numRangeRep));

  /* -----------------------------------------------------------------------------
   */
  TEST_SECTION("MAC addresses");

  /*
    MAC_cisco = { {[:xdigit:]{4} "."}{2} [:xdigit:]{4} }
    MAC_windows = { {[:xdigit:]{2} "-"}{5} [:xdigit:]{2} }
    MAC_common = { {[:xdigit:]{2} ":"}{5} [:xdigit:]{2} }
  */

  pexl_Expr *r4xdigit = pexl_repeat(C, xdigit, 4, 4);
  pexl_Expr *r4xdigitdot = pexl_seq(C, r4xdigit, dotExp);
  pexl_Expr *r2_r4xdigitdot = pexl_repeat(C, r4xdigitdot, 2, 2);
  pexl_Expr *MAC_cisco =
      pexl_capture_f(C, "MAC cisco", pexl_seq(C, r2_r4xdigitdot, r4xdigit));

  pexl_Expr *r2xdigit = pexl_repeat(C, xdigit, 2, 2);
  pexl_Expr *r2xdigithyp = pexl_seq(C, r2xdigit, hypExp);
  pexl_Expr *r5_r2xdigithyp = pexl_repeat(C, r2xdigithyp, 5, 5);
  pexl_Expr *MAC_windows =
      pexl_capture_f(C, "MAC_windows", pexl_seq(C, r5_r2xdigithyp, r2xdigit));

  pexl_Expr *r2xdigitcolon = pexl_seq(C, r2xdigit, colonExp);
  pexl_Expr *r5_r2xdigitcolon = pexl_repeat(C, r2xdigitcolon, 5, 5);
  pexl_Expr *MAC_common =
      pexl_capture_f(C, "MAC_common", pexl_seq(C, r5_r2xdigitcolon, r2xdigit));

  pexl_Expr *MACbegin = pexl_choice(C, MAC_cisco, MAC_windows);
  pexl_Expr *MAC = pexl_choice(C, MACbegin, MAC_common);

  pexl_Expr *ip = pexl_choice(C, ipv4_address, ipv6);
  pexl_Expr *any =
      pexl_choice_f(C, pexl_choice(C, ip, MAC),
                    pexl_choice_f(C, pexl_choice(C, fqdn_practical, email),
                                  pexl_choice(C, url, path)));

  pexl_Expr *notAny = pexl_neg_lookahead(C, any);
  pexl_Expr *byte1 = pexl_match_any(C, 1);
  pexl_Expr *notanynext = pexl_seq(C, notAny, byte1);
  pexl_Expr *anyorchar = pexl_choice(C, any, notanynext);
  pexl_Expr *anyandall = pexl_repeat(C, anyorchar, 0, 0);

  TEST_ASSERT(any);

  pexl_Ref refany = pexl_bind(C, anyandall, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refany, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "any";

  input = "me@here.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "you+filter@somewhere.org";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "k!!{}@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  inputfile = readFile("cwe.txt");
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), inputfile,
                      strlen(inputfile), match, true, strlen(inputfile)));

  pexl_Expr *notURL = pexl_neg_lookahead(C, url);
  pexl_Expr *noturlNext = pexl_seq(C, notURL, byte1);
  pexl_Expr *urlornot = pexl_choice(C, url, noturlNext);
  pexl_Expr *allurl = pexl_repeat(C, urlornot, 0, 0);

  TEST_ASSERT(allurl);

  pexl_Ref refallurl = pexl_bind(C, allurl, NULL); // anonymous
  pexl_free_Binary(pkg);

  pkg = pexl_compile(C, refallurl, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  patname = "any";

  input = "me@here.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "you+filter@somewhere.org";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));
  input = "k!!{}@example.com";
  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), input,
                      strlen(input), match, true, strlen(input)));

  TEST_ASSERT(!vmtest(patname, pkg, NULL, main_entrypoint(pkg), inputfile,
                      strlen(inputfile), match, true, strlen(inputfile)));

  pexl_free_Expr(name_char_rep);
  pexl_free_Expr(dot_name_char_rep);
  pexl_free_Expr(dot_name_char_rep_star);
  pexl_free_Expr(quote);
  pexl_free_Expr(negquote);
  pexl_free_Expr(anything);
  pexl_free_Expr(anythingbutquote);
  pexl_free_Expr(anythingbutquoteRep);
  pexl_free_Expr(quote_anythingbutquoteRep);
  pexl_free_Expr(quoted_name);

  pexl_free_Expr(name_char);

  pexl_free_Expr(nameSymb);

  pexl_free_Expr(r2Opt_ipv6_Rest_end);
  pexl_free_Expr(ipv6_com_r2Opt_ipv6_Rest_end);
  pexl_free_Expr(r6_ipv6_rest);
  pexl_free_Expr(r6_ipv6_rest_end);
  pexl_free_Expr(ipv6_com_r3Opt_ipv6_Rest);
  pexl_free_Expr(r3Opt_ipv6_Rest);
  pexl_free_Expr(ipv6compr2Opt_ipv6_Rest);
  pexl_free_Expr(r2Opt_ipv6_Rest);
  pexl_free_Expr(r2_ipv6_Rest);
  pexl_free_Expr(r4Opt_ipv6_Rest);
  pexl_free_Expr(r7_ipv6_Rest);
  pexl_free_Expr(ipv4_component);

  pexl_free_Expr(ipv6_address);
  pexl_free_Expr(ipv4_address);
  pexl_free_Expr(r4Opt_ipv6_Rest_End);
  pexl_free_Expr(ipv6_comp_r4Opt_ipv6_Rest_End);
  pexl_free_Expr(ipv6_mixed);
  pexl_free_Expr(ipv6_end);
  pexl_free_Expr(c_ipv4_ipv6);
  pexl_free_Expr(ipv6_component);
  pexl_free_Expr(doublecolon);
  pexl_free_Expr(ipv6_rest);
  pexl_free_Expr(fqdn_practical);
  pexl_free_Expr(notthat);
  pexl_free_Expr(num_float_not_let_dig_hyp);
  pexl_free_Expr(not_let_dig_hyp);
  pexl_free_Expr(r62Opt_let_dig_hyp);
  pexl_free_Expr(e_plus_min_opt);
  pexl_free_Expr(seq_2_range0_4);
  pexl_free_Expr(ipv6_comp_rest_Opt);
  pexl_free_Expr(ipv6_Opt);
  pexl_free_Expr(r3_ipv6_Rest);
  pexl_free_Expr(r4_ipv6_Rest);
  pexl_free_Expr(ipv6_comp_r5_rest);
  pexl_free_Expr(r5_ipv6_Rest);
  pexl_free_Expr(r5Opt_rest);
  pexl_free_Expr(m2);
  pexl_free_Expr(m3_9);
  pexl_free_Expr(ipv6_comp_r2_Rest);
  pexl_free_Expr(s_hyp_alphaNum);
  pexl_free_Expr(ExpOpt);
  pexl_free_Expr(not_ipv4);
  pexl_free_Expr(notipv4_ipv6_comp);
  pexl_free_Expr(ipv6_comp_end);
  pexl_free_Expr(ipv6_rest_opt);
  pexl_free_Expr(end_ipv4_add);
  pexl_free_Expr(r3_ipv6_Rest_seq_col_end);
  pexl_free_Expr(r4_ipv6_Rest_seq_col_end);
  pexl_free_Expr(repeatdotdomain);
  pexl_free_Expr(seq_repeatdotdomain_dotOpt);
  pexl_free_Expr(eofExp);
  pexl_free_Expr(ipv6_test);
  pexl_free_Expr(fqdn_strict);
  pexl_free_Binary(pkg);
  pexl_free_Match(match);
  pexl_free_Context(C);
  pexl_free_Optims(optims);

  TEST_END();
  return 0;
}
// I am still leaking a lot of mem *SUMMARY: AddressSanitizer: 552136 byte(s)
// leaked in 43 allocation(s).* however it is all in large complex expressions
// and probably should not be a top priority. If/When I fix it i can do one of
// two things (that I have currently considered). Pull them all out so they can
// be manually freed or wrap them in repeats so I can free the outer
// expressions.)
