/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  interfacetest.c  TESTING interface.c                                     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "binary.h"

#include "../test.h"

#define YES 1
#define NO 0

#define PRINTING NO

#define ANONYMOUS 0

#define HAS_CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (BindingTable *bt, pexl_Ref ref) {
  Binding *b = env_get_binding(bt, ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

int main(int argc, char **argv) {

  int stat;
  pexl_Context *C;
  pexl_Optims *optims = NULL;
  pexl_Index id;
  pexl_Binary *pkg;
  pexl_Error err;
  Binding *b;
  Pattern *p; 
  pexl_Env toplevel, genv, env, a_random_env;
  pexl_Expr *exp, *A, *B, *S;
  pexl_Ref ref, refA, refB, refC, refD, refS, ref_anon;
  char name[2];

  TEST_START(argc, argv);

  toplevel = 0;			// root env has id 0

  /* Testing the error-checking */
  TEST_ASSERT(toplevel == 0);

  C = pexl_new_Context();
  TEST_ASSERT(C);
  
  fprintf(stderr, "Expect a warning:\n");
  ref = pexl_bind_in(NULL, toplevel, NULL, "number 1");
  TEST_ASSERT(pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == PEXL_ERR_NULL);
  fprintf(stderr, "Expect a warning:\n");
  ref = pexl_bind_in(C, -1, NULL, "number 2");
  TEST_ASSERT(pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == PEXL_COMPILER__ERR_SCOPE);
  
  /* Can make anonymous binding of unspecified value */
  ref = pexl_bind_in(C, toplevel, NULL, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == 0);	/* first binding will be in slot 0 */

  exp = pexl_match_epsilon(C);
  TEST_ASSERT(pexl_rebind(C, ref, exp) == PEXL_OK);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(pexl_rebind(C, ref, NULL) == PEXL_ERR_NULL);
  ref = -1;			/* negative value is not valid */
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(pexl_rebind(C, ref, exp) == PEXL_COMPILER__ERR_BADREF);

  pexl_free_Expr(exp);
  exp = NULL;
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Non-recursive block using anonymous exps");

  TEST_ASSERT(C->env == PEXL_ROOT_ENV); // Prerequisite for next test
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 1);               // First child env must be env 1
  TEST_ASSERT(C->env == 1);	       // Current env has been updated

  // anonymous binding of unspecified value
  refS = pexl_bind_in(C, env, NULL, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  A = pexl_call(C, refS);			       /* A -> S */
  S = pexl_capture_f(C, "foo", pexl_match_epsilon(C)); /* S -> capture("foo", true) */

  TEST_ASSERT_NULL(optims);	// prerequisite for next step
  optims = pexl_default_Optims();
  TEST_ASSERT(optims);
  
  // Set up test by assuring we have an invalid env to use
  a_random_env = MAX_ENV_NODES - 1;
  TEST_ASSERT(env_get_parent(C->bt, a_random_env) == -1);
  
  TEST_ASSERT(a_random_env);
  // Try to bind A in a_random_env, which produces an error
  fprintf(stderr, "Expect a warning:\n");
  ref = pexl_bind_in(C, a_random_env, A, NULL);
  TEST_ASSERT(pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == PEXL_COMPILER__ERR_SCOPE);

  // Try to compile everything anyway, even though there's no pattern for A
  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  // Code has no patterns, consists of one IRet instruction
  TEST_ASSERT(pkg->codenext == 1);
  pexl_free_Binary(pkg);

  // Bind A
  refA = pexl_bind_in(C, env, A, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  b = env_get_binding(C->bt, refA);
  TEST_ASSERT(b);
  TEST_ASSERT(b->val.type == Epattern_t);

  b = env_get_binding(C->bt, refS);
  TEST_ASSERT(b);
  TEST_ASSERT(b->val.type == Eunspecified_t);

  /* Can compile but refS does not contain a pattern, so we get no code */
  pkg = pexl_compile(C, refS, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  // Code has no patterns, consists of one IRet instruction
  TEST_ASSERT(pkg->codenext == 1);

  stat = pexl_rebind(C, refS, S);
  TEST_ASSERT(stat == PEXL_OK);
  b = env_get_binding(C->bt, refS);
  TEST_ASSERT(b);
  TEST_ASSERT(b->val.type == Epattern_t);

  pexl_free_Binary(pkg);

  /* Now compilation should succeed */
  pkg = pexl_compile(C, refS, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  if (PRINTING) pexl_print_Binary(pkg);

  pexl_free_Binary(pkg);
  pexl_free_Expr(A);
  pexl_free_Expr(S);

  env = pexl_scope_pop(C);
  TEST_ASSERT(env == PEXL_ROOT_ENV);	// parent of env is root
  TEST_ASSERT(C->env == PEXL_ROOT_ENV);	// Current env has been updated
  
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Recursive block using anonymous exps");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 

  // Use internal 'env_new'
  genv = env_new(C->bt, toplevel);
  TEST_ASSERT(genv);
  refA = pexl_bind_in(C, genv, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  A = pexl_call(C, refA);		/* A -> A */
  TEST_ASSERT(pexl_rebind(C, refA, A) == PEXL_OK);
  
  // No bindings at top level, so codegen produces no code
  pkg = pexl_compile(C, refA, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  TEST_ASSERT(pkg->codenext == 1);
  pexl_free_Binary(pkg);

  // New binding for A in top level.  But A calls the other A in genv.
  ref = pexl_bind(C, A, "toplevel_A");
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(!pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_EXP__ERR_SCOPE);

  // Build a new A that calls itself in toplevel
  pexl_free_Expr(A);
  A = pexl_call(C, ref);		/* A -> A */
  TEST_ASSERT(pexl_rebind(C, ref, A) == PEXL_OK);
  pexl_free_Expr(A);

  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(!pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_EXP__ERR_LEFT_RECURSIVE);

  printf("Trying mutually left-recursive rules\n"); 
  refA = pexl_bind_in(C, genv, NULL, "A");
  TEST_ASSERT(pexl_Ref_invalid(refA));
  TEST_ASSERT(refA == PEXL_ENV__ERR_EXISTS); /* binding with name A already exists */

  refA = pexl_bind_in(C, genv, NULL, "");    /* Anonymous is ok */
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refB = pexl_bind_in(C, genv, NULL, "");    /* Anonymous is ok */
  TEST_ASSERT(!pexl_Ref_invalid(refB));
  A = pexl_call(C, refB);	         /* A -> B */
  B = pexl_call(C, refA);		 /* B -> A */

  TEST_ASSERT(pexl_rebind(C, refA, A) == PEXL_OK);
  TEST_ASSERT(pexl_rebind(C, refB, B) == PEXL_OK);
  pexl_free_Expr(A);
  pexl_free_Expr(B);

  if (PRINTING) pexl_print_Env(genv, C);

  /* Compile B */
  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err); 
  TEST_ASSERT(!pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_EXP__ERR_LEFT_RECURSIVE); /* should detect left recursion */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  id = context_intern(C, "foo", 3); /* for later use */
  TEST_ASSERT(id >= 0);

  ref = pexl_bind_in(C, toplevel, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C,		/* A -> "a" A "b" / epsilon */
		 pexl_seq_f(C,
			 pexl_match_string(C, "a"),
			 pexl_seq_f(C, 
				 pexl_call(C, ref),
				 pexl_match_string(C, "b"))),
		 pexl_match_epsilon(C));

  TEST_ASSERT(A);

  if (PRINTING) {
    printf("A is: \n"); 
    pexl_print_Expr(A, 0, C); 
  }
  
  // The only binding at this point is A --> Unspecified.
  if (PRINTING) print_BindingTable(C);
  
  /*
    If we try to compile now, we'll get an empty package with no code
    in it, error because A calls a binding to Unspecified
  */
  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  TEST_ASSERT(pkg->codenext == 0);
  pexl_free_Binary(pkg);
  
  /* Now modify the "A" binding's value to the pattern based on the expression A */
  TEST_ASSERT(pexl_rebind(C, ref, A) == PEXL_OK);
  pexl_free_Expr(A);

  //  pkg = compile_expression(C, optims, A, toplevel, &val, &err);
  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  TEST_ASSERT(pkg->codenext > 0);

  if (PRINTING) pexl_print_Binary(pkg);
  pexl_free_Binary(pkg);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("No mutual left recursion in this example");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  refA = pexl_bind(C, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(refA));

  /* Here we use the variable B to hold the anonymous expression */
  /* <anon> -> "x" / A */
  B = pexl_choice_f(C,
		 pexl_match_string(C, "x"),
		 pexl_call(C, refA));

  TEST_ASSERT(B);

  ref_anon = pexl_bind(C, B, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(ref_anon));
  pexl_free_Expr(B);
  
  /* Here we reuse the variable B for no good reason */
  /* B -> "a"+ <anon> */
  B = pexl_seq_f(C, 
		 pexl_repeat_f(C, pexl_match_string(C, "a"), 1, 0), // "a"+ 
		 pexl_call(C, ref_anon));
  TEST_ASSERT(B);

  refB = pexl_bind(C, B, "B");
  TEST_ASSERT(!pexl_Ref_invalid(refB));
  
  /* A -> B "b" / epsilon */
  A = pexl_choice_f(C,
		 pexl_seq_f(C, 
			 pexl_call(C, refB),
			 pexl_match_string(C, "b")),
		 pexl_match_epsilon(C));

  TEST_ASSERT(A);

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  TEST_ASSERT(pexl_rebind(C, refA, A) == PEXL_OK);
  pexl_free_Expr(A);

  p = binding_pattern_value(C->bt, ref_anon); 
  TEST_ASSERT(p);
  if (PRINTING) {
    printf("<anon> is: \n");  
    print_pattern(p, 0, C);  
  }
  
  p = binding_pattern_value(C->bt, refB); 
  TEST_ASSERT(p);
  if (PRINTING) {
    printf("B is: \n");  
    print_pattern(p, 0, C);  
  }
  
  p = binding_pattern_value(C->bt, refA); 
  TEST_ASSERT(p);
  if (PRINTING) {
    printf("A is: \n");
    print_pattern(p, 0, C);  
  }
  
  pexl_free_Expr(B);

  pkg = pexl_compile(C, refA, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == 0);

  //  if (PRINTING) {
  if (1) {
    pexl_print_Env(toplevel, C);
    pexl_print_Binary(pkg);
  
    printf("Pattern at refB:\n");
    print_pattern(binding_pattern_value(C->bt, refB), 4, C);
  }
  
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with chain of recursive rules");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  id = context_intern(C, "foo", 3); /* for later use */
  TEST_ASSERT(id >= 0);

  refA = pexl_bind(C, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  refB = pexl_bind(C, NULL, "B");
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  refC = pexl_bind(C, NULL, "C");
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  refD = pexl_bind(C, NULL, "D");
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  /* A -> B */
  pexl_Expr *Aexp = pexl_call(C, refB);
  TEST_ASSERT(pexl_rebind(C, refA, Aexp) == PEXL_OK);
  pexl_free_Expr(Aexp);
  
  /* B -> C */
  pexl_Expr *Bexp = pexl_call(C, refC);
  TEST_ASSERT(pexl_rebind(C, refB, Bexp) == PEXL_OK);
  pexl_free_Expr(Bexp);

  /* C -> D */
  pexl_Expr *Cexp = pexl_call(C, refD);
  TEST_ASSERT(pexl_rebind(C, refC, Cexp) == PEXL_OK);
  pexl_free_Expr(Cexp);

  /* D -> "hi" A */
  pexl_Expr *Dexp = pexl_seq_f(C,
			       pexl_match_string(C, "hi"),
			       pexl_call(C, refA));
  TEST_ASSERT(pexl_rebind(C, refD, Dexp) == PEXL_OK);
  pexl_free_Expr(Dexp);

  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  if (PRINTING) {
    pexl_print_Binary(pkg);
    print_symboltable(pkg->symtab);
  }
  
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Misc patterns");

#define compile_exp(exp) do {					\
    TEST_ASSERT(exp);						\
    ref = pexl_bind(C, exp, NULL);				\
    TEST_ASSERT(!pexl_Ref_invalid(ref));			\
    pexl_free_Expr(exp);						\
    pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);	\
    TEST_ASSERT(pkg);						\
    if (PRINTING) {						\
      pexl_print_Binary(pkg);					\
      print_symboltable(pkg->symtab);				\
    }								\
    pexl_free_Binary(pkg);					\
  } while (0);
  
  //  printf("err is %d\n", pexl_Error_value(err));
  printf("=-=-=-=-=-=\n");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  exp = pexl_match_set(C, "abcABC", 6);
  compile_exp(exp);

  exp = pexl_match_bytes(C, "AB\0", 3);
  compile_exp(exp);

  exp = pexl_match_any(C, 3);	/* any 3 bytes */
  compile_exp(exp);

  exp = pexl_match_any(C, -3);	/* NOT 3 bytes */
  compile_exp(exp);
  
  exp = pexl_match_any(C, 0);	/* same as TRUE (== epsilon)*/
  compile_exp(exp);
  
  /* Range error because 'from' > 'to' */
  exp = pexl_match_range(C, 65, 64);
  TEST_ASSERT(!exp);

  exp = pexl_match_range(C, 65, 64 + 26); /* [A-Z] */
  compile_exp(exp);
  
  exp = pexl_neg_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);

  exp = pexl_neg_lookahead_f(C, pexl_match_string(C, "JAJ"));
  compile_exp(exp);

  exp = pexl_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);

  exp = pexl_lookahead_f(C, pexl_match_string(C, "JAJ"));
  compile_exp(exp);
  
  exp = pexl_lookbehind(C, NULL); 
  TEST_ASSERT(!exp); 

  exp = pexl_lookbehind_f(C, pexl_match_string(C, "JAJ")); 
  compile_exp(exp);
  
  exp = pexl_repeat_f(C, NULL, 0, 0);
  TEST_ASSERT(!exp);

  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0);
  compile_exp(exp);
  
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 4, 0);
  compile_exp(exp);
  
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 4);
  compile_exp(exp);
  
  exp = pexl_capture_f(C, "This is a long capture name", NULL);
  TEST_ASSERT(!exp);

  exp = pexl_capture_f(C, NULL, pexl_match_string(C, "x"));
  TEST_ASSERT(!exp);

  exp = pexl_capture_f(C, "This is a long capture name", pexl_match_string(C, "a"));
  TEST_ASSERT(exp); 
  compile_exp(exp);
  
  exp = pexl_insert(C, "foo", NULL);
  TEST_ASSERT(!exp);

  exp = pexl_insert(C, NULL, "foo");
  TEST_ASSERT(!exp);

  exp = pexl_insert(C, "This is a long capture name", "And a long value name");
  compile_exp(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using compile_environment");

#define bind(C, exp, env, name) do {				\
    TEST_ASSERT(exp);						\
    ref = pexl_bind_in((C), (env), (exp), (name));		\
    pexl_free_Expr(exp);					\
    TEST_ASSERT(!pexl_Ref_invalid(ref));			\
    name[0]++;							\
  } while (0)

  /* New env */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  name[0] = 'a';
  name[1] = '\0';
  exp = pexl_match_set(C, "abcABC", 6);
  bind(C, exp, toplevel, name);

  /* b */
  exp = pexl_match_bytes(C, "AB\0", 3);
  bind(C, exp, toplevel, name);

  /* c */
  exp = pexl_match_any(C, 3);	/* any 3 bytes */
  bind(C, exp, toplevel, name);
  
  /* d */
  exp = pexl_match_any(C, -3); /* NOT 3 bytes */
  bind(C, exp, toplevel, name);

  /* e */
  exp = pexl_match_any(C, 0);	/* same as TRUE (== epsilon)*/
  bind(C, exp, toplevel, name);

  /* f */
  exp = pexl_match_range(C, 65, 64);  /* pexl_Error checking */
  TEST_ASSERT(!exp);		      /* "error when from > to" */
  exp = pexl_match_range(C, 65, 64 + 26);	/* [A-Z] */
  bind(C, exp, toplevel, name);

  /* g: removed halt instruction */

  /* h */
  exp = pexl_neg_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_neg_lookahead_f(C, pexl_match_string(C, "JAJ"));
  bind(C, exp, toplevel, name);

  /* i */
  exp = pexl_lookahead_f(C, NULL);
  TEST_ASSERT(!exp);
  exp = pexl_lookahead_f(C, pexl_match_string(C, "JAJ"));
  bind(C, exp, toplevel, name);

  /* j */
  exp = pexl_repeat_f(C, NULL, 0, 0);
  TEST_ASSERT(!exp);
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0);
  bind(C, exp, toplevel, name);

  /* k */
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 4, 0);
  bind(C, exp, toplevel, name);

  /* l */
  exp = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 4);
  bind(C, exp, toplevel, name);

  /* m */
  exp = pexl_capture_f(C, "This is a long capture name", NULL);
  TEST_ASSERT(!exp);
  exp = pexl_capture_f(C, NULL, pexl_match_string(C, "x"));
  TEST_ASSERT(!exp);
  exp = pexl_capture_f(C, "This is a long capture name", pexl_match_string(C, "a"));
  bind(C, exp, toplevel, name);

  /* n */
  exp = pexl_insert(C, "foo", NULL);
  TEST_ASSERT(!exp);
  exp = pexl_insert(C, NULL, "foo");
  TEST_ASSERT(!exp);
  exp = pexl_insert(C, "This is a long capture name", "And a long value name");
  bind(C, exp, toplevel, name);
  
  /* o */
  exp = pexl_lookbehind_f(C, pexl_match_string(C, "JAJ")); 
  bind(C, exp, toplevel, name);

  if (PRINTING) pexl_print_Env(toplevel, C);

  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);
  
  if (PRINTING) {
    pexl_print_Binary(pkg);
    print_symboltable(pkg->symtab);
  }
  
  // Keep the pkg we just compiled, for a subsequent test

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using xcall into a compiled package");

  TEST_ASSERT(pkg);		/* prerequisite for next test */
  pexl_Binary *other_package = pkg;
  TEST_ASSERT(other_package);

  pexl_Index symbol;
  SymbolTableEntry *entry;

  symbol = PEXL_ITER_START;
  entry = symboltable_search(other_package->symtab, symbol_ns_id, "k", &symbol);
  TEST_ASSERT(entry);       // pattern named 'k' was created in prior test
  TEST_ASSERT(symbol >= 0); // return value is index of 'k' in symbol table

  exp = pexl_xcall(C, other_package, symbol);
  TEST_ASSERT_NULL(exp);    // other_package is not in package table

  stat = packagetable_add(C->packages, other_package);
  TEST_ASSERT(stat < 0);    // importpath not set for other_package

  stat = binary_set_attributes(other_package, "/other/pkg", NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat == 0);

  stat = packagetable_add(C->packages, other_package);
  TEST_ASSERT(stat == 0);

  // Now expect success
  exp = pexl_xcall(C, other_package, symbol);
  TEST_ASSERT(exp); 
  //printf("exp is xcall(%p, %d)\n", (void *) other_package, symbol);

  ref = pexl_bind(C, exp, "xcall_test");
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  /*
    pexl_Error check: This attempt to compile will fail because
    other_package is not in the package table.  It is currently there,
    but we are about to remove it.
  */

  stat = packagetable_remove(C->packages, other_package);
  TEST_ASSERT(stat == 0);

  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(!pkg);
  printf("pexl_Error_value(err) = %d\n", pexl_Error_value(err));
  TEST_ASSERT(pexl_Error_value(err) == PEXL_CODEGEN__ERR_NO_PACKAGE);
  printf("Passed the test where compilation fails due to NULL package\n");

  pexl_Index index;
  
  stat = binary_set_attributes(other_package, "abc", NULL, NULL, NULL, 0, 0);
  TEST_ASSERT(stat >= 0);
  stat = packagetable_add(C->packages, other_package);
  TEST_ASSERT(stat >= 0);
  index = packagetable_getnum(C->packages, other_package);
  TEST_ASSERT(index >= 0);

  if (PRINTING) {
    print_packagetable_stats(C->packages);
    print_packagetable(C->packages);
    printf("About to compile...\n");
    fflush(NULL);
  }
  
  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  printf("pexl_Error_value(err) = %d\n", pexl_Error_value(err));
  TEST_ASSERT(pkg);

  if (PRINTING) {
    pexl_print_Binary(pkg);
    print_symboltable(pkg->symtab);
  }
  
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing size limit of env tree");

  // For the tests below, we need either a fresh context, or one in
  // which no child envs have been created.
  TEST_ASSERT(C->bt->parent[1] < 0);
  TEST_ASSERT(C->env == PEXL_ROOT_ENV);
  TEST_ASSERT(PEXL_ROOT_ENV == 0);
  TEST_ASSERT(env_size(C->bt) == 1);	// Always have a root env

  // Create two children of root env (1, 2)
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 1);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_pop(C);
  TEST_ASSERT(env == 0);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 2);
  TEST_ASSERT(C->env == env);

  TEST_ASSERT(env_size(C->bt) == 3);	// Root and two children
  

  // Create 3 children of second child of root (3, 4, 5)
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 3);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_pop(C);
  TEST_ASSERT(env == 2);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 4);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_pop(C);
  TEST_ASSERT(env == 2);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 5);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_pop(C);
  TEST_ASSERT(env == 2);
  TEST_ASSERT(C->env == env);

  // Pop back to root
  env = pexl_scope_pop(C);
  TEST_ASSERT(env == PEXL_ROOT_ENV);
  TEST_ASSERT(C->env == env);

  // Add one more child to the root
  env = pexl_scope_push(C);
  TEST_ASSERT(env == 6);
  TEST_ASSERT(C->env == env);
  env = pexl_scope_pop(C);
  TEST_ASSERT(env == PEXL_ROOT_ENV);
  TEST_ASSERT(C->env == env);

  // Error checking
  env = pexl_scope_pop(C);
  TEST_ASSERT(invalid_env(env));
  TEST_ASSERT(env == PEXL_ENV__NOT_FOUND);
  TEST_ASSERT(C->env == PEXL_ROOT_ENV);
  env = pexl_scope_pop(C);
  TEST_ASSERT(invalid_env(env));
  TEST_ASSERT(env == PEXL_ENV__NOT_FOUND);
  TEST_ASSERT(C->env == PEXL_ROOT_ENV);

  TEST_ASSERT(env_size(C->bt) == 7); // based on tests above

  print_env_tree(C);

  // Now fill the env tree
  env = env_size(C->bt);
  for (pexl_Index i = env; i < MAX_ENV_NODES; i++) 
    env = pexl_scope_push(C);
  TEST_ASSERT(!invalid_env(env));
  TEST_ASSERT(env_size(C->bt) == MAX_ENV_NODES);
  TEST_ASSERT(C->env == MAX_ENV_NODES - 1);
  
  // Try to add beyond the maximum
  env = pexl_scope_push(C);
  TEST_ASSERT(invalid_env(env));
  TEST_ASSERT(env_size(C->bt) == MAX_ENV_NODES);
  TEST_ASSERT(C->env == MAX_ENV_NODES - 1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Compiling backreferences");

  /* New env */
  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(C);

  /*
       seq
     /     \
    A      backref:A
  
    A = capture(one_x, "A")
    one_x = pexl_match_string(C, "x")

  */
  
  A = pexl_capture_f(C, "A", pexl_match_string(C, "x"));
  TEST_ASSERT(A);

  S = pexl_seq_f(C, A, pexl_backref(C, "A"));

  //  if (PRINTING) {
    printf("S is: \n"); 
    pexl_print_Expr(S, 0, C); 
    //  }
  
    refA = pexl_bind(C, S, NULL); // anonymous

  pkg = pexl_compile(C, PEXL_NO_MAIN, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  //  if (PRINTING)
    pexl_print_Binary(pkg);

  pexl_free_Binary(pkg);

  pexl_free_Context(C);
  TEST_END();
}

