/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  astresolvetest.c                                                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "print.h"
#include "ast.h"
#include "ast-print.h"
#include "sexp-reader.h"
#include "sexp-writer.h"
#include "sexp-to-ast.h"
#include "ast-typecheck.h"
#include "ast-resolve.h"

#include "../test.h"

#define PRINTING NO


static const char *query_st_contents(SymbolTable *st, int n, ...) {
  assert(st);
  assert(n > 0);

  const char *name;
  va_list names;
  va_start(names, n);

  SymbolTableEntry *entry;
  for (int i = 0; i < n; i++) {
    name = va_arg(names, const char *);
    entry = symboltable_search(st, symbol_ns_id, name, NULL);
    if (!entry) {
      va_end(names);
      return name;
    }
  }

  va_end(names);
  return NULL;
}

#define newline() do { puts(""); } while(0)

static bool all_whitespace(const char *ptr) {
  while (*ptr && whitespacep(ptr)) ptr++;
  return !*ptr;
}

static Sexp *sexp_from_literal(const string *s){
  if (!s) return read_sexp(NULL);
  string *tmp = strdup(s);
  const string *ptr = tmp;
  Sexp *sexp = read_sexp(&ptr);
  if (sexp_errorp(sexp)) {
    free(tmp);
    return sexp;
  }
  // Else no error in reading the sexp.  Now check that we read the
  // entire string as one s-expression, returning NULL if we did not.
  // If the sexp ends before the end of the input string and there is
  // only whitespace remaining, that's ok for our tests.
  if (!all_whitespace(ptr)) {
    if (PRINTING)
      printf("Failed to parse entire string as one s-expression: '%s'\n", s);
    free(tmp);
    free_sexp(sexp);
    return NULL;
  }
  // Successfully read the entire string as one s-expression:
  free(tmp);
  return sexp;
}

static void print_ast_as_sexpr(ast_expression *exp) {
  Sexp *s = ast_to_sexp(exp);
  if (!s) {
    printf("NULL S-expression (in print_ast_as_sexpr)\n");
    return;
  }
  print_sexp(s);
  free_sexp(s);
}

static bool non_nullp(Sexp *s) {
  return s && !sexp_nullp(s);
}

static bool notest(ast_expression *exp) {
  UNUSED(exp);
  if (PRINTING) {
    printf("Did not expect error expression!  Received this:\n");
    ast_print_tree(exp);
  }
  return false;
}

int main(int argc, char **argv){

  int stat;
  Sexp *s = NULL;
  SymbolTable *st = NULL;
  ast_expression *exp, *error_exp, *tmp;
  const char *str;

  TEST_START(argc, argv);

  printf("Size of ast_expression is %zu bytes\n", sizeof(ast_expression));

// Note: sexp_from_literal() is a convenience function defined in this file.
// Note: MAKE_EXP sets the variable 'exp', which must be freed later.
#define MAKE_EXP(string, sexp_test, asttype) do {		\
    if (PRINTING) printf("Input string:     %s\n", (string));	\
    s = sexp_from_literal(string);				\
    TEST_ASSERT(s);						\
    if (PRINTING) {						\
      printf("S-expression:     ");				\
      print_sexp(s); newline();					\
    }								\
    TEST_ASSERT(sexp_test(s));					\
    exp = sexp_to_ast(s);					\
    TEST_ASSERT(exp);						\
    if (PRINTING) {						\
      printf("AST tree (below):\n");				\
      ast_print_tree(exp);					\
      printf("New S-expression: ");				\
      print_ast_as_sexpr(exp);					\
      newline();						\
    }								\
    TEST_ASSERT(exp->type == (asttype));			\
    free_sexp(s);						\
  } while (0)

#define EXPECT_WELL_TYPED(string, asttype) do {			\
    MAKE_EXP((string), non_nullp, (asttype));			\
    if ((asttype) == AST_MODULE) {				\
      TEST_ASSERT(typecheck_mod(exp, &error_exp));		\
      if (PRINTING)						\
	printf("Module contains %d forms\n\n",			\
	       ast_child_count(exp));				\
    }								\
    else if ((asttype) == AST_DEFINITION)			\
      TEST_ASSERT(typecheck_def(exp, &error_exp));		\
    else							\
      TEST_ASSERT(typecheck_exp(exp, &error_exp));		\
  } while (0)

#define TEST_RESOLVER(string, asttype, expectation, error_test) \
  do {								\
    EXPECT_WELL_TYPED(string, asttype);				\
    st = NULL;							\
    stat = ast_resolve(exp, &st, &error_exp);			\
    TEST_ASSERT(stat == expectation);				\
    /* if an error is expected, then st will be NULL */		\
    if (stat >= 0)						\
      TEST_ASSERT(st);						\
    else							\
      TEST_ASSERT_NULL(st);					\
    if (PRINTING && st) {					\
      print_symboltable(st);					\
      newline();						\
      printf("AST tree resolved (below):\n");			\
      ast_print_tree(exp);					\
      newline();						\
    }								\
    if (stat != true) {						\
      TEST_ASSERT(error_exp);					\
      if (PRINTING) {						\
	printf("Offending expression: ");			\
	print_ast_as_sexpr(error_exp);				\
	newline();						\
      }								\
      TEST_ASSERT(error_test(error_exp));			\
    } else {							\
      TEST_ASSERT_NULL(error_exp);				\
    }								\
  } while (0)    

#define CLEANUP do {				\
    free_expression(exp);			\
    symboltable_free(st);			\
  } while (0);


#define EXPECT_RESOLVED(string, asttype) do {	\
    TEST_RESOLVER(string, asttype, true, notest);		\
  } while (0)    

#define EXPECT_UNRESOLVED(string, asttype, error_test) do {	\
    TEST_RESOLVER(string, asttype, false, error_test);		\
  } while (0)    

#define EXPECT_ERROR(string, asttype, error_code, error_test) do {	\
    TEST_RESOLVER(string, asttype, error_code, error_test);		\
  } while (0)    
  
  /* ------------------------------------------------------------------ */
  TEST_SECTION("Resolving expressions that are not definitions");
  /* ------------------------------------------------------------------ */

  /* ------------------------------------------------------------------ */
  /* Atoms                                                              */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("#\\A", AST_CHAR);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("#\\newline", AST_CHAR);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("\"Hello, world!\"", AST_BYTESTRING);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  /* ------------------------------------------------------------------ */
  /* Sequences and choices                                              */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(seq #\\A #\\B)", AST_SEQ);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(choice #\\nul)", AST_CHAR);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(choice #\\A #\\B #\\C #\\D)", AST_CHOICE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  // These will NOT resolve because identifier 'a' is not defined
  EXPECT_UNRESOLVED("(choice a #\\A \"string\")", AST_CHOICE, ast_identifierp);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_UNRESOLVED("(choice #\\A a \"string\")", AST_CHOICE, ast_identifierp);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_UNRESOLVED("(choice #\\A \"string\" a)", AST_CHOICE, ast_identifierp);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* Repetitions                                                        */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(repeat 0 0 #\\A)", AST_REPETITION);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(repeat 1 2 #\\x0a)", AST_REPETITION);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* 'Find' expressions                                                 */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(find #\\x00)", AST_FIND);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(find \"Hi\")", AST_FIND);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(find (repeat 0 1 \"Hi\"))", AST_FIND);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  /* ------------------------------------------------------------------ */
  /* 'Capure' and 'Insert' expressions                                  */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(capture \"capture name\" #\\x00)", AST_CAPTURE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(capture \"another capture name \" \"Hi\")", AST_CAPTURE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(capture \"foo\" (repeat 0 1 \"Hi\"))", AST_CAPTURE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* Backrefs                                                           */
  /* ------------------------------------------------------------------ */

  EXPECT_UNRESOLVED("(backref abc)", AST_BACKREF, ast_identifierp);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_UNRESOLVED("(backref abc.def)", AST_BACKREF, ast_identifierp);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  // Bad identifier:
  TEST_EXPECT_WARNING;
  EXPECT_ERROR("(backref abc.def.)", AST_BACKREF, PEXL_ERR_INTERNAL, ast_identifierp);
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* Predicates                                                         */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(> #\\A)", AST_LOOKAHEAD);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(!> #\\A)", AST_NEGLOOKAHEAD);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(< #\\A)", AST_LOOKBEHIND);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(!< #\\A)", AST_NEGLOOKBEHIND);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  /* ------------------------------------------------------------------ */
  /* Byte ranges and sets                                               */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(brange #\\Z #\\Z)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(brange #\\u0000 #\\u0000)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(brange #\\x0A #\\u0000)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(brange #\\x0A #\\U00000000)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(brange #\\x0A #\\uFFFF)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(brange #\\Z #\\A)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(brange #\\A #\\B)", AST_BYTERANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\A #\\u0000)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\B #\\U00000000)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\u0000)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\U00000000)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\A)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\A #\\B)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(bset #\\Z #\\A #\\B #\\x00 #\\C)", AST_BYTESET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* Character ranges and sets                                          */
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(range #\\u0000 #\\u0000)", AST_RANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(range #\\x0A #\\u0000)", AST_RANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(range #\\x0A #\\U00000000)", AST_RANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(range #\\Z #\\A)", AST_RANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(range #\\A #\\B)", AST_RANGE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(set #\\A)", AST_SET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(set #\\A #\\u0000)", AST_SET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(set #\\B #\\U00000000)", AST_SET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(set #\\u0000 #\\u0001 #\\u0002)", AST_SET);  
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(set #\\u0001 #\\u0000 #\\u0002)", AST_SET);  
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(set #\\A #\\B #\\C)", AST_SET);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* More complex expressions (that are not definitions)                */
  /* ------------------------------------------------------------------ */

  EXPECT_UNRESOLVED("(choice (seq \"A bytestring\" (backref abc.def)) X)",
		    AST_CHOICE, ast_identifierp);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  
  // From the benchmarks (comparisons) repository:
  //   'abram' regex: /[A-Za-z]+ *Abram/
  //   'abram' RPL:   {[[A-Z][a-z]]+ " "* "Abram"}
  EXPECT_RESOLVED("(seq "
		     "  (repeat 1 0 (choice (brange #\\A #\\Z) (brange #\\a #\\z))) "
		     "  (repeat 0 0 \" \") "
		     "  \"Abram\")",
		  AST_SEQ);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  
  // 'longword' regex: [[a-z][A-Z]]{14,}
  EXPECT_RESOLVED("(repeat 14 0 "
		     "  (choice (brange #\\A #\\Z) (brange #\\a #\\z)))",
		  AST_REPETITION);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'smallword' regex: [[a-z][A-Z]]{5,}
  EXPECT_RESOLVED("(repeat 5 0 "
		     "  (choice (brange #\\A #\\Z) (brange #\\a #\\z)))",
		  AST_REPETITION);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'omega' regex: Omega
  EXPECT_RESOLVED("\"Omega\"", AST_BYTESTRING);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'the' regex: @the
  EXPECT_RESOLVED("\"@the\"", AST_BYTESTRING);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'tubalcain' regex: Tubalcain
  EXPECT_RESOLVED("\"Tubalcain\"", AST_BYTESTRING);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'pass' regex starts with a space: [ ]it came to pass
  EXPECT_RESOLVED("\" it came to pass\"", AST_BYTESTRING);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'psalm' regex: {[0-9]{1,3}[:][0-9]{1,3}}
  EXPECT_RESOLVED("(seq "
		     "  (repeat 1 3 (brange #\\0 #\\9)) "
		     "  #\\: "
		     "  (repeat 1 3 (brange #\\0 #\\9))) ",
		  AST_SEQ);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // 'adamant' regex: "adam" [[a-z][A-Z]]*
  EXPECT_RESOLVED("(seq "
		     "  \"adam\" "
		     "  (repeat 0 0 "
		     "    (choice (brange #\\A #\\Z) (brange #\\a #\\z)))) ",
		  AST_SEQ);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  /* ------------------------------------------------------------------ */
  TEST_SECTION("Resolving expressions with definitions");
  /* ------------------------------------------------------------------ */

  //
  // CHECK_ST_CONTENTS is a macro because we want any failed
  // TEST_ASSERTs to print out the source line number of the actual
  // failure, not a source line number within query_st_contents.
  //
#define CHECK_ST_CONTENTS(st, n, ...) do {			\
    TEST_ASSERT(symboltable_len(st) == (size_t) n);		\
    str = query_st_contents(st, n, __VA_ARGS__);		\
    TEST_ASSERT_MSG(str == NULL, "Failed to find '%s'", str);	\
  } while (0);


  /* 'verses' is a pattern is designed to have several CALLs.
       RPL: 
	 num = [0-9]
	 nums = num+
	 colon = ":"
	 chapt_and_verse = {nums colon nums}
	 And = "And"
	 spaces = " "*
	 verses = {chapt_and_verse spaces And}
  */

  EXPECT_RESOLVED("(def num (brange #\\0 #\\9))", AST_DEFINITION); // [0-9]
  CHECK_ST_CONTENTS(st, 1, "num");
  CLEANUP;

  // Here, 'num' is not defined
  EXPECT_UNRESOLVED("(def nums (repeat 1 0 num))", AST_DEFINITION, ast_identifierp); // num+
  CHECK_ST_CONTENTS(st, 1, "nums");
  CLEANUP;

  // This definition of 'nums' does not reference 'num'
  EXPECT_RESOLVED("(def nums (repeat 1 0 (brange #\\0 #\\9)))", AST_DEFINITION); // num+
  CHECK_ST_CONTENTS(st, 1, "nums");
  CLEANUP;

  EXPECT_RESOLVED("(def colon #\\:)", AST_DEFINITION);
  CHECK_ST_CONTENTS(st, 1, "colon");
  CLEANUP;

  // Here, 'nums' and 'colon' are not defined
  EXPECT_UNRESOLVED("(def chapt_and_verse (seq nums colon nums))", AST_DEFINITION, ast_identifierp);
  CHECK_ST_CONTENTS(st, 1, "chapt_and_verse");
  CLEANUP;

  // This definition does not reference 'nums' or 'colon'
  EXPECT_RESOLVED("(def chapt_and_verse (seq #\\A #\\B #\\C))", AST_DEFINITION);
  CHECK_ST_CONTENTS(st, 1, "chapt_and_verse");
  CLEANUP;

  EXPECT_RESOLVED("(def space \" \")", AST_DEFINITION);
  CHECK_ST_CONTENTS(st, 1, "space");
  CLEANUP;

  // Here, 'space' is not defined
  EXPECT_UNRESOLVED("(def spaces (repeat 0 0 space))", AST_DEFINITION, ast_identifierp);
  CHECK_ST_CONTENTS(st, 1, "spaces");
  CLEANUP;

  // This definition does not reference 'space'
  EXPECT_RESOLVED("(def spaces (repeat 0 0 \" \"))", AST_DEFINITION);
  CHECK_ST_CONTENTS(st, 1, "spaces");
  CLEANUP;

  EXPECT_RESOLVED("(def And \"And\")", AST_DEFINITION);
  CHECK_ST_CONTENTS(st, 1, "And");
  CLEANUP;

  /* ------------------------------------------------------------------ */
  /* Modules                                                            */
  /* ------------------------------------------------------------------ */
  
  EXPECT_RESOLVED("(mod)", AST_MODULE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;


  // The sequence refers to undefined names
  EXPECT_UNRESOLVED("(mod "
		    "  (def verses "
		    "    (seq chapt_and_verse spaces and)))",
		    AST_MODULE, ast_identifierp);
  CHECK_ST_CONTENTS(st, 1, "verses");
  CLEANUP;

  // This definition does not reference any undefined names
  EXPECT_RESOLVED("(mod "
		     "  (def verses "
		     "    (seq verses (repeat 0 0 #\\space) \"And\")))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st, 1, "verses");
  TEST_ASSERT(exp->defns->head->type == AST_DEFINITION);
  TEST_ASSERT(exp->defns->head->child->type == AST_SEQ);
  TEST_ASSERT(exp->defns->head->child->child->type == AST_IDENTIFIER);
  // The only entry in the symbol table is "verses" at index 0
  TEST_ASSERT(exp->defns->head->child->child->idx == 0);
  CLEANUP;


  // First definition below refers to undefined names
  EXPECT_UNRESOLVED("(mod "
		    "  (def verses "
		    "    (seq chapt_and_verse spaces and))"
		    "  (def verses "
		    "    (seq chapt_and_verse spaces and)))",
		    AST_MODULE, ast_identifierp);
  CHECK_ST_CONTENTS(st, 1, "verses", "verses");
  CLEANUP;

  // Duplicate definitions for same name
  EXPECT_UNRESOLVED("(mod "
		    "  (def verses "
		    "    (seq chapt_and_verse spaces and))"
		    "  (def verses "
		    "    (seq chapt_and_verse spaces and)))",
		    AST_MODULE, ast_identifierp);
  CHECK_ST_CONTENTS(st, 1, "verses");
  CLEANUP;

  // Second definition refers to undefined identifiers
  EXPECT_UNRESOLVED("(mod "
		     "  (def verses1 "
		     "    (seq #\\A #\\B #\\C #\\D))"
		     "  (def verses2 "
		     "    (seq chapt_and_verse spaces and)))",
		    AST_MODULE, ast_identifierp);
  CHECK_ST_CONTENTS(st, 2, "verses2", "verses1");
  CLEANUP;

  // These definitions do not refer to any undefined identifiers
  EXPECT_RESOLVED("(mod "
		  "  (def verses1 "
		  "    (seq #\\A #\\B #\\C #\\D)) "
		  "  (def verses2 "
		  "    (seq #\\A #\\B #\\C #\\D)))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st, 2, "verses2", "verses1");
  CLEANUP;

  EXPECT_RESOLVED("(mod "
		  "  (def num (brange #\\0 #\\9))"
		  "  (def nums (repeat 1 0 num))"
		  "  (def colon #\\:)"
		  "  (def chapt_and_verse (seq nums colon nums))"
		  "  (def space \" \")"
		  "  (def spaces (repeat 0 0 space))"
		  "  (def And \"And\")"
		  "  (def verses (seq chapt_and_verse spaces And)))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st,
		    8,
		    "verses", "And", "spaces", "space",
		    "chapt_and_verse", "colon", "num", "nums");
  // Symbol table entry for 'num' is at index 0, and it is referenced
  // in the definition of 'nums', which is the second definition
  TEST_ASSERT(exp->defns->tail->head->child->child->type == AST_IDENTIFIER);
  TEST_ASSERT(exp->defns->tail->head->child->child->idx == 0);
  // Check the references in the defn of chapt_and_verse:
  //   'nums' is at index 1, and is referenced twice
  //   'colon' is at index 2, and is referenced once
  // Select the exp on the right-hand-side of the chapt_and_verse defn:
  tmp = exp->defns->tail->tail->tail->head->child;
  TEST_ASSERT(tmp->type == AST_SEQ);
  TEST_ASSERT(tmp->child->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->child->idx == 1); // 'nums'
  tmp = tmp->child2;
  TEST_ASSERT(tmp->type == AST_SEQ);
  TEST_ASSERT(tmp->child->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->child->idx == 2); // 'colon'
  TEST_ASSERT(tmp->child2->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->child2->idx == 1); // 'nums'
  CLEANUP;


  /* ------------------------------------------------------------------ */
  TEST_SECTION("Nested scopes");
  /* ------------------------------------------------------------------ */

  EXPECT_RESOLVED("(mod "
		  "  (mod "
		  "    (def num (brange #\\0 #\\9))"
		  "    (def nums (repeat 1 0 num))"
		  "    (def colon #\\:)"
		  "    (def space \" \")"
		  "    (def spaces (repeat 0 0 space))))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st, 5, "spaces", "space", "colon", "num", "nums");
  CLEANUP;

  // Duplicate definitions for 'num' in the same scope
  EXPECT_UNRESOLVED("(mod "
		    "  (mod "
		    "    (def num (brange #\\0 #\\9))"
		    "    (def num (repeat 1 0 num))"
		    "    (def colon #\\:)"
		    "    (def space \" \")"
		    "    (def spaces (repeat 0 0 space))))",
		    AST_MODULE, ast_definitionp);
  CHECK_ST_CONTENTS(st, 2, "num", "num");
  CLEANUP;

  // Duplicate definitions for 'num' in the same scope
  EXPECT_UNRESOLVED("(mod "
		    "  (def num (brange #\\0 #\\9))"
		    "  (mod "
		    "    (def space \" \")"
		    "    (def spaces (repeat 0 0 space)))"
		    "  (def num (repeat 1 0 num))"
		    "  (def colon #\\:))",
		    AST_MODULE, ast_definitionp);
  CHECK_ST_CONTENTS(st, 4, "num", "num", "space", "spaces");
  CLEANUP;

  // Definitions for 'num' in different scopes is OK
  EXPECT_RESOLVED("(mod "
		  "  (def num (brange #\\0 #\\9))"
		  "  (mod "
		  "    (def num (repeat 1 0 num))"
		  "    (def colon #\\:)"
		  "    (def space \" \")"
		  "    (def spaces (repeat 0 0 space))))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st, 5, "num", "num", "colon", "space", "spaces");
  CLEANUP;

  // Definitions for 'num' in different scopes is OK
  EXPECT_RESOLVED("(mod "
		  "  (def num (brange #\\0 #\\9))"
		  "  (mod "
		  "    (def num (repeat 1 0 num))"
		  "    (def colon #\\:)"
		  "    (def space \" \")"
		  "    (mod "
		  "      (def num (repeat 1 0 num)))"
		  "    (def spaces (repeat 0 0 space))))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st, 6, "num", "num", "colon", "space", "spaces", "num");
  CLEANUP;

  EXPECT_RESOLVED("(mod "
		  "  (mod "
		  "    (mod "
		  "      (mod))))",
		  AST_MODULE);
  TEST_ASSERT(symboltable_len(st) == 0);
  CLEANUP;

  EXPECT_RESOLVED("(mod                                  " // outer
		  "  (def num (repeat 0 0 num))          "
		  "  (mod                                " // first nested
		  "    (def num (repeat 1 1 num))        "
		  "    (mod                              " // second nested
		  "      (def num (repeat 2 2 num))      "
		  "      (mod                            " // third nested
		  "        (def num (repeat 3 3 num))))))",
		  AST_MODULE);
  CHECK_ST_CONTENTS(st, 4, "num", "num", "num", "num");

  // Check each reference to 'num'.  Should be recursive, i.e. it
  // should point to its own definition.

  // Outermost module:
  TEST_ASSERT(exp->type == AST_MODULE);
  tmp = exp->defns->head;
  TEST_ASSERT(tmp->type == AST_DEFINITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_REPETITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->idx == 0);	// the 1st symtab entry
  
  // First nested module:
  tmp = exp->defns->tail->head;
  TEST_ASSERT(tmp->type == AST_MODULE);
  tmp = tmp->defns->head;
  TEST_ASSERT(tmp->type == AST_DEFINITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_REPETITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->idx == 1);	// the 2nd symtab entry

  // Second nested module:
  tmp = exp->defns->tail->head->defns->tail->head;
  TEST_ASSERT(tmp->type == AST_MODULE);
  tmp = tmp->defns->head;
  TEST_ASSERT(tmp->type == AST_DEFINITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_REPETITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->idx == 2);	// the 3rd symtab entry

  // Third nested module:
  tmp = exp->defns->tail->head->defns->tail->head->defns->tail->head;
  ast_print_tree(tmp); newline();
  TEST_ASSERT(tmp->type == AST_MODULE);
  tmp = tmp->defns->head;
  TEST_ASSERT(tmp->type == AST_DEFINITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_REPETITION);
  tmp = tmp->child;
  TEST_ASSERT(tmp->type == AST_IDENTIFIER);
  TEST_ASSERT(tmp->idx == 3);	// the 4th symtab entry
  CLEANUP;

  confess("TEMP",
	  "Change the following test to EXPECT_RESOLVED after we lift the fixed limit on nesting");
  EXPECT_ERROR("(mod "
	       "  (mod "
	       "    (mod "
	       "      (mod "
	       "        (mod "
	       "          (mod))))))",
	       AST_MODULE,
	       PEXL_ERR_INTERNAL,
	       ast_modulep);



  puts("\n\n");
  confess("TODO", "Write FUZZING (style) tests to build large and varied expressions");

  TEST_END();
}

