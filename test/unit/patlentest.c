/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  patlentest.c  TESTING the new patlen code in analyze.c                 */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "old-analyze.h"
#include "compile.h"

#include "../test.h"

#define PRINTING NO

#define has_flag(flags, flagname) ((flags)&(flagname))

static int check_patlen (pexl_Binary *pkg,
			 int nullable,
			 int unbounded,
			 uint32_t expected_min,
			 uint32_t expected_max) {
  SymbolTableEntry *entry;
  uint32_t min, max;
  int stat;

  assert(pkg && pkg->symtab);

  entry = symboltable_get(pkg->symtab, pkg->main);
  TEST_ASSERT(entry);

  stat = has_flag(metadata_flags(entry->meta), PATTERN_NULLABLE);
  if ((stat==0) != (nullable==0)) {
    printf("error: expected %snullable pattern\n", nullable ? "" : "NON-");
    return 2;
  }
  stat = has_flag(metadata_flags(entry->meta), PATTERN_UNBOUNDED);
  if ((stat == 0) != (unbounded == 0)) {
    printf("pattern is %s, which was not expected\n", stat ? "unbounded" : "bounded");
    return 4;
  }
  min = metadata_minlen(entry->meta);
  max = metadata_maxlen(entry->meta);
  if ((min != expected_min) || ((stat == EXP_BOUNDED) && (max != expected_max))) {
    if (min != expected_min)
      printf("min len = %u, expected = %u\n", min, expected_min);
    if ((stat == EXP_BOUNDED) && (max != expected_max))
      printf("max len = %u, expected = %u\n", max, expected_max);
    return 5;
  } 
  return OK;
}

static
pexl_Binary *compile_expression (pexl_Context *C,
				 pexl_Optims *optims,
				 pexl_Expr *exp,
				 pexl_Env env,
				 pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);
  TEST_ASSERT(pkg && (pexl_Error_value(*err) == PEXL_OK));
  return pkg;
}

int main(int argc, char **argv) {

  size_t n;
  char *str;
  uint32_t j, min;
  
  pexl_Context *C;
  pexl_Optims *optims;
  pexl_Binary *pkg;
  pexl_Error err;
  
  pexl_Env toplevel; 
  pexl_Expr *A, *B, *Choice;
  
  int i;
  int unbounded;
  uint32_t sizes[] = {1000,
                      1024,
		      1024 + 511,
		      1024 + 512,
		      64 * 1024 - 1,
		      1024 * 1024,
		      PEXL_MAX_STRINGLEN,
		      PEXL_MAX_STRINGLEN+1,
		      PATTERN_MAXLEN,
		      PATTERN_MAXLEN+1,
		      0};	/* 0 is END MARKER */

  TEST_START(argc, argv);

  C = context_new();
  TEST_ASSERT(C);
  toplevel = 0;			// root env has id 0
  
  optims = pexl_default_Optims();
  
  /* ----------------------------------------------------------------------------- */

  pexl_free_Optims(optims);
  optims = pexl_addopt_tro(NULL);
  TEST_ASSERT(optims);
  optims = pexl_addopt_inline(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_unroll(optims, 2);
  TEST_ASSERT(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);

  TEST_SECTION("Testing patlen on fixed-lengths");

  i = 0;
  while ((n = sizes[i++])) {
    if (PRINTING) printf("\nTesting size %lu\n", n);
    str = malloc(n * sizeof(char));
    A = pexl_match_bytes(C, str, n);
    if (n <= PEXL_MAX_STRINGLEN) {
      TEST_ASSERT(A);
      pkg = compile_expression(C, optims, A, toplevel, &err);
      TEST_ASSERT(pkg && pexl_Error_value(err) == OK);
      pexl_free_Expr(A);
    } else {
      TEST_ASSERT(!A);		       /* should fail -- bytestring too long */
      free(str);
      continue;			       /* nothing to compile, so try another size */
    }
    /*
      Check to see if the min/max lengths and the fixedlen flag are
      printing correctly
    */
    if (PRINTING) print_symboltable(pkg->symtab);
    /* patterns longer than this are simply marked UNBOUNDED */
    unbounded = (n > PATTERN_MAXLEN);
    TEST_ASSERT(!check_patlen(pkg, false,
			      unbounded, unbounded ? PATTERN_MAXLEN : n, n));

    binary_free(pkg);

    TEST_SECTION("Testing patlen on choices");

    for (j = 0; j < 101; j = (j==0) ? j+1 : j * 10) {

      if (PRINTING) printf("\nTesting CHOICE of size %lu and size %lu\n", n-j, n);
      A = pexl_match_bytes(C, str, n);
      B = pexl_match_bytes(C, str, n-j);
      Choice = pexl_choice(C, A, B); /* Do not free A, B */
      pkg = compile_expression(C, optims, Choice, toplevel, &err);
      TEST_ASSERT(pkg && pexl_Error_value(err) == OK);
      pexl_free_Expr(Choice);

      if (PRINTING) print_symboltable(pkg->symtab);
      /* patterns longer than this are simply marked UNBOUNDED */
      unbounded = (n > PATTERN_MAXLEN);
      min = ((n - j) > PATTERN_MAXLEN) ? PATTERN_MAXLEN : (n - j);
      TEST_ASSERT(!check_patlen(pkg, false, unbounded, min, n));
      binary_free(pkg);

      /* Same as above, but choice is reversed */
      if (PRINTING) printf("\nTesting CHOICE of size %lu and size %lu\n", n, n-j);
      Choice = pexl_choice(C, B, A); /* Do not free A, B */
      pkg = compile_expression(C, optims, Choice, toplevel, &err);
      TEST_ASSERT(pkg && pexl_Error_value(err) == OK);
      pexl_free_Expr(Choice);

      if (PRINTING) print_symboltable(pkg->symtab);
      /* patterns longer than this are simply marked UNBOUNDED */
      unbounded = (n > PATTERN_MAXLEN);
      min = ((n - j) > PATTERN_MAXLEN) ? PATTERN_MAXLEN : (n - j);
      TEST_ASSERT(!check_patlen(pkg, false, unbounded, min, n));
      binary_free(pkg);
      
      /* Same as above, but an additional choice with epsilon to make it nullable */
      if (PRINTING) printf("\nTesting CHOICE of size %lu and size %lu and EPSILON\n", n, n-j);
      Choice = pexl_choice_f(C, B, A);
      Choice = pexl_choice_f(C, Choice, pexl_match_epsilon(C));
      pkg = compile_expression(C, optims, Choice, toplevel, &err);
      TEST_ASSERT(pkg && pexl_Error_value(err) == OK);
      pexl_free_Expr(Choice);

      if (PRINTING) print_symboltable(pkg->symtab);
      /* patterns longer than this are simply marked UNBOUNDED */
      unbounded = (n > PATTERN_MAXLEN);
      TEST_ASSERT(!check_patlen(pkg, true, unbounded, 0, n));
      pexl_free_Binary(pkg);

    } /* for */

    free(str);

  } /* while */

  TEST_SECTION("Checking environment for unexpected contents");

  Binding *binding;
  pexl_Index ii = PEXL_ITER_START;
  while ((binding = env_local_iter(C->bt, toplevel, &ii))) {
    switch (bindingtype(binding)) {
    case Epattern_t:
      break;
    default:
      TEST_FAIL("Unexpected binding in env to value of type %d\n", bindingtype(binding));
    }
  }

  pexl_free_Context(C);
  pexl_free_Optims(optims);

  TEST_END();
  
}

