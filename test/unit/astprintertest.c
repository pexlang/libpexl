/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  asttest.c  Testing the AST structures                                    */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: James Deucher                                                   */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
#include "ast-print.h"

#include "../test.h"

#define newline() do { puts("\n"); } while(0)

FILE *fp; 
FILE *testFile; 
ast_expression *expr;

/**
 * Resets the test mode, allowing output to the console (stdout).
 */
static void clearTest(void) {
  fclose(fp);
  fclose(testFile);
  freopen("/dev/tty", "a", stdout); 
}

/**
 * Takes into test mode. Writes console output to a file so that comparison of text is easier.
 */
static void activeTest(void) {
  testFile = fopen("output.txt", "r");
  fp = freopen("output.txt", "w", stdout);
}

/**
 * Just pass this lil guy a string of what you want the test to be and he will check the output file and see if the
 * contents equal the string you gave him. DO NOT USE WHEN CLEARTEST HAS BEEN USED.
 */
static void testHelper(const char * wants, const char * label) {
  fflush(fp);
  
  fseek(testFile, 0, SEEK_END);
  long fileSize = ftell(testFile);
  fseek(testFile, 0, SEEK_SET); 
  
  char * result = malloc(sizeof(char) * (fileSize + 1));//[__INT8_MAX__] = "";

  fread(result, sizeof(char), fileSize, testFile);

  result[fileSize] = '\0';
  TEST_ASSERT_EQ_string(result, wants);
  free(result);

  clearTest();
  printf("%s: sexpr\n", label);
  ast_print_sexpr(expr); newline();
  printf("%s: print_tree\n", label); 
  ast_print_tree(expr); newline();
  activeTest();
}

int main(int argc, char **argv) {

  // Some comments provide areas where different tree node types are
  // tested. Another way to find what you need would be to run the
  // test and find the title from the printed test sections.
  
  TEST_START(argc, argv);

  fp = freopen("output.txt", "w", stdout);
  testFile = fopen("output.txt", "r");

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Basic expressions");

  // Test Byte expression
  
  expr = make_byte('W');
  ast_print_tree(expr);
  testHelper("Byte [ 87 ]\n", "BYTE");

  free_expression(expr);

  // Test ByteRange expression
  expr = make_byterange(1, 255);
  ast_print_tree(expr);
  testHelper("ByteRange [ Min: 1, Max: 255 ]\n", "BYTERANGE");

  free_expression(expr);

  // Test Byteset expression
  expr = make_byteset();
  int i;
  for (i = 0; i < 256; i++)
    byteset_add(expr->con.byteset, (ast_byte) i);
  ast_print_tree(expr);
  testHelper("ByteSet [  Range [ Min: 0, Max: 255 ]]\n", "BYTESET");

  free_expression(expr);

  expr = make_byteset();
  for (i = 0; i < 256; i += 3)
    byteset_add(expr->con.byteset, (ast_byte) i);
  ast_print_tree(expr);
  testHelper("ByteSet [  0  3  6  9  12  15  18  21  24  27  30  33  36  39  42  45  48  51  54  57  60  63  66  69  72  75  78  81  84  87  90  93  96  99  102  105  108  111  114  117  120  123  126  129  132  135  138  141  144  147  150  153  156  159  162  165  168  171  174  177  180  183  186  189  192  195  198  201  204  207  210  213  216  219  222  225  228  231  234  237  240  243  246  249  252  255]\n", "BYTESET");

  free_expression(expr);

  // Codepoint test
  expr = make_codepoint(0xFEFE);
  ast_print_tree(expr);
  testHelper("Codepoint [ 65278 ]\n", "CODEPOINT");

  free_expression(expr);

  // Codepointrange
  expr = make_codepointrange(0, 0);
  ast_print_tree(expr);
  testHelper("CodepointRange [ Min: 0, Max: 0 ]\n", "CODEPOINTRANGE");

  free_expression(expr);

  expr = make_codepointrange(INT32_MAX, 123);
  ast_print_tree(expr);
  testHelper("CodepointRange [ Min: 2147483647, Max: { ]\n", "CODEPOINTRANGE");

  free_expression(expr);

  // Codepointset
  expr = make_codepointset(0);
  for (i = 0; i < 301; i++) {
    codepointset_add(expr->con.cpset, (ast_codepoint) i*5);
  }
  for (i = 0; i < 4+300*5; i++) {
    if ((i - 5*(i/5)) == 0) {
      codepointset_mem(expr->con.cpset, (ast_codepoint) i);
    } else {
      codepointset_mem(expr->con.cpset, (ast_codepoint) i);
    }
  }
  ast_print_tree(expr);
  testHelper("CodepointSet [ 0 5 10 15 20 25 30 # ( - 2 7 < A F K P U Z _ d i n s x } 130 135 140 145 150 155 160 165 170 175 180 185 190 195 200 205 210 215 220 225 230 235 240 245 250 255 260 265 270 275 280 285 290 295 300 305 310 315 320 325 330 335 340 345 350 355 360 365 370 375 380 385 390 395 400 405 410 415 420 425 430 435 440 445 450 455 460 465 470 475 480 485 490 495 500 505 510 515 520 525 530 535 540 545 550 555 560 565 570 575 580 585 590 595 600 605 610 615 620 625 630 635 640 645 650 655 660 665 670 675 680 685 690 695 700 705 710 715 720 725 730 735 740 745 750 755 760 765 770 775 780 785 790 795 800 805 810 815 820 825 830 835 840 845 850 855 860 865 870 875 880 885 890 895 900 905 910 915 920 925 930 935 940 945 950 955 960 965 970 975 980 985 990 995 1000 1005 1010 1015 1020 1025 1030 1035 1040 1045 1050 1055 1060 1065 1070 1075 1080 1085 1090 1095 1100 1105 1110 1115 1120 1125 1130 1135 1140 1145 1150 1155 1160 1165 1170 1175 1180 1185 1190 1195 1200 1205 1210 1215 1220 1225 1230 1235 1240 1245 1250 1255 1260 1265 1270 1275 1280 1285 1290 1295 1300 1305 1310 1315 1320 1325 1330 1335 1340 1345 1350 1355 1360 1365 1370 1375 1380 1385 1390 1395 1400 1405 1410 1415 1420 1425 1430 1435 1440 1445 1450 1455 1460 1465 1470 1475 1480 1485 1490 1495 1500 ]\n", "CODEPOINTSET");

  free_expression(expr);

  expr = make_codepointset(0);
  for (i = 300; i >= 0; i--) {
    codepointset_add(expr->con.cpset, (ast_codepoint) i*5);
  }
  /* Test in same ascending order as earlier */
  for (i = 0; i < 4+300*5; i++) {
    if ((i - 5*(i/5)) == 0) {
      codepointset_mem(expr->con.cpset, (ast_codepoint) i);
    } else {
      codepointset_mem(expr->con.cpset, (ast_codepoint) i);
    }
  }
  ast_print_tree(expr);
  testHelper("CodepointSet [ 0 5 10 15 20 25 30 # ( - 2 7 < A F K P U Z _ d i n s x } 130 135 140 145 150 155 160 165 170 175 180 185 190 195 200 205 210 215 220 225 230 235 240 245 250 255 260 265 270 275 280 285 290 295 300 305 310 315 320 325 330 335 340 345 350 355 360 365 370 375 380 385 390 395 400 405 410 415 420 425 430 435 440 445 450 455 460 465 470 475 480 485 490 495 500 505 510 515 520 525 530 535 540 545 550 555 560 565 570 575 580 585 590 595 600 605 610 615 620 625 630 635 640 645 650 655 660 665 670 675 680 685 690 695 700 705 710 715 720 725 730 735 740 745 750 755 760 765 770 775 780 785 790 795 800 805 810 815 820 825 830 835 840 845 850 855 860 865 870 875 880 885 890 895 900 905 910 915 920 925 930 935 940 945 950 955 960 965 970 975 980 985 990 995 1000 1005 1010 1015 1020 1025 1030 1035 1040 1045 1050 1055 1060 1065 1070 1075 1080 1085 1090 1095 1100 1105 1110 1115 1120 1125 1130 1135 1140 1145 1150 1155 1160 1165 1170 1175 1180 1185 1190 1195 1200 1205 1210 1215 1220 1225 1230 1235 1240 1245 1250 1255 1260 1265 1270 1275 1280 1285 1290 1295 1300 1305 1310 1315 1320 1325 1330 1335 1340 1345 1350 1355 1360 1365 1370 1375 1380 1385 1390 1395 1400 1405 1410 1415 1420 1425 1430 1435 1440 1445 1450 1455 1460 1465 1470 1475 1480 1485 1490 1495 1500 ]\n", "CODEPOINTSET");

  free_expression(expr);
  
  expr = make_codepointset(1001);
  codepointset_add(expr->con.cpset, (ast_codepoint) 123456);
  codepointset_add(expr->con.cpset, (ast_codepoint) 1);
  ast_print_tree(expr);
  testHelper("CodepointSet [ 1 123456 ]\n", "CODEPOINTSET");

  free_expression(expr);

  // TODO: Literal

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Sequence expressions");

  expr = make_sequence(make_byte(65), NULL);
  ast_print_tree(expr);
  testHelper("Sequence \n└── Byte [ 65 ]\n", "SEQUENCE");

  free_expression(expr);

  expr = make_sequence(make_byte(65), make_byte(65));
  for(int j = 0; j < 10; j++) {
    expr = make_sequence(make_byte(65), expr);
  }
  ast_print_tree(expr);
  testHelper( "Sequence \n"
              "├── Byte [ 65 ]\n"
              "└── Sequence \n"
              "    ├── Byte [ 65 ]\n"
              "    └── Sequence \n"
              "        ├── Byte [ 65 ]\n"
              "        └── Sequence \n"
              "            ├── Byte [ 65 ]\n"
              "            └── Sequence \n"
              "                ├── Byte [ 65 ]\n"
              "                └── Sequence \n"
              "                    ├── Byte [ 65 ]\n"
              "                    └── Sequence \n"
              "                        ├── Byte [ 65 ]\n"
              "                        └── Sequence \n"
              "                            ├── Byte [ 65 ]\n"
              "                            └── Sequence \n"
              "                                ├── Byte [ 65 ]\n"
              "                                └── Sequence \n"
              "                                    ├── Byte [ 65 ]\n"
              "                                    └── Sequence \n"
              "                                        ├── Byte [ 65 ]\n"
              "                                        └── Byte [ 65 ]\n", "SEQUENCE");

  free_expression(expr);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Choice expressions");

  char astr[2] = {'A', '\0'};
  ast_expression *exp1 = make_bytestring((uint8_t *)strdup(astr), strlen(astr));
  TEST_ASSERT(exp1);
  ast_expression *exp2 = make_bytestring((uint8_t *)strdup(astr), strlen(astr));
  TEST_ASSERT(exp2);
  expr = make_choice(exp1, exp2);
  TEST_ASSERT(expr);
  ast_print_tree(expr);
  testHelper("Choice \n"
             "├── ByteString [ A ]\n"
             "└── ByteString [ A ]\n", "CHOICE");

  free_expression(expr);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Repitition expressions");

  expr = make_repetition(INT32_MIN, INT32_MAX, make_byte('x'));
  ast_print_tree(expr);
  testHelper("Repetition [ Min: NegIninity, Max: Infinity ]\n"
             "└── Byte [ 120 ]\n", "REPITITION");

  free_expression(expr);

  expr = make_repetition(1, 4, make_byte('x'));
  ast_print_tree(expr);
  testHelper("Repetition [ Min: 1, Max: 4 ]\n"
             "└── Byte [ 120 ]\n", "REPITITION");

  free_expression(expr);

  expr = make_repetition(2, INT32_MAX, make_byte('x'));
  ast_print_tree(expr);
  testHelper("Repetition [ Min: 2, Max: Infinity ]\n"
             "└── Byte [ 120 ]\n", "REPITITION");

  free_expression(expr);

  expr = make_repetition(INT32_MIN, 2, make_byte('x'));
  ast_print_tree(expr);
  testHelper("Repetition [ Min: NegIninity, Max: 2 ]\n"
             "└── Byte [ 120 ]\n", "REPITITION");

  free_expression(expr);


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Predicate expressions");

  expr = make_predicate(AST_LOOKAHEAD, make_byte('A'));
  ast_print_tree(expr);
  testHelper("Predicate [ LookAhead ]\n"
             "└── Byte [ 65 ]\n", "PREDICATE");

  free_expression(expr);

  expr = make_predicate(AST_NEGLOOKAHEAD, make_byte('A'));
  ast_print_tree(expr);
  testHelper("Predicate [ NegLookAhead ]\n"
             "└── Byte [ 65 ]\n", "PREDICATE");

  free_expression(expr);

  expr = make_predicate(AST_LOOKBEHIND, make_byte('A'));
  ast_print_tree(expr);
  testHelper("Predicate [ LookBehind ]\n"
             "└── Byte [ 65 ]\n", "PREDICATE");

  free_expression(expr);

  expr = make_predicate(AST_NEGLOOKBEHIND, make_byte('A'));
  ast_print_tree(expr);
  testHelper("Predicate [ NegLookBehind ]\n"
             "└── Byte [ 65 ]\n", "PREDICATE");

  free_expression(expr);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Multi Depth Trees");

  // Final section for multi depth trees, have yet to create a standard for testing although experimentation with treees such as the one below seemed to be working as intended.
  // exp = make_choice(NULL, NULL);
  // exp = make_choice(make_choice(make_byte(65), make_byte(65)), make_choice(make_choice(make_byte(65), make_byte(65)),make_choice(make_byte(65), make_byte(65))));
  // free_expression(exp);
  // exp = make_sequence(NULL, make_choice(make_byte(65), make_byte(65)));
  // ast_print_tree(exp);

  // clearTest();
  // ast_print_tree(exp);
  // activeTest();
  
  // free_expression(exp);	


  /* ----------------------------------------------------------------------------- */

  // if (remove("output.txt") != 0) {
  //     perror("Error deleting file"); 
  // }

  fclose(testFile);
  fclose(fp);
  
  TEST_END();
}

