/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  hashtabletest.c  Tests for the string-keyed hash table                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <time.h>
#include "hashtable.h"

#include "../test.h"

#define FUZZING NO

// In range 0..max-1
static uint32_t random_in(uint32_t max) {
  return random() % max;
}

static void print_hashtable_stats(HashTable *ht) {
  pexl_Index size = Table_capacity(ht->entries);
  printf("Hash table %p: %" pexl_Index_FMT "/%" pexl_Index_FMT
	 " used, load factor %" pexl_Index_FMT "%%."
	 "  Block %lu/%lu bytes used.\n",
	 (void *)ht, ht->count, size, ht->count*100/size,
	 StringBuffer_len(ht->block), StringBuffer_capacity(ht->block));
}

static void print_hashtable(HashTable *ht) {
  int j;
  const char *name;
  HashTableEntry entry;
  pexl_Index i, len, count = 0;
  pexl_Index cap = Table_capacity(ht->entries);
  printf("Hash table:\n");
  for (i = 0; i < cap; i++) {
    entry = hashtable_get(ht, i);
    if (entry.block_offset >= 0) {
      name = hashtable_get_key(ht, entry.block_offset, &len);
      printf("%3" pexl_Index_FMT " -> [%9d]  ", i, entry.block_offset);
      for (j = 0; j < 8; j++) printf("%d ", entry.data.chars[j]); 
      printf("  %.*s\n", len, name);
      count++;
    }
  } /* for */
  printf("TOTAL %" pexl_Index_FMT " strings found\n", count);
  if (ht->count != count)
    printf("ERROR in string table count, which is %" pexl_Index_FMT
	   "\n", ht->count);
}

// This must be larger than any single word in /usr/share/dict/words
#define MAXLEN 100

static int32_t length (const char *str) {
  return (int32_t) strlen(str);
}

int main(int argc, char **argv) {

  fpos_t startpos;
  
  int i;
  size_t expected_max;
  const char *probe;
  char *s;
  
  pexl_Index index;
  long offset, pos, startindex;

  HashTableEntry entry, entry2, saveentry, tmp;
  HashTableData data;
  HashTable *st1;
  
  FILE *words;

  char buf[MAXLEN];
  int numwords;
  
  srandom(time(NULL));

  TEST_START(argc, argv);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Storing a few strings");

  st1 = hashtable_new(0, 0);	/* Request default sizes */
  TEST_ASSERT(st1);
  printf("entries table: %p\n", (void *) st1->entries);
  printf("entries[0,1,2] = %d, %d, %d\n",
	 hashtable_get(st1, 0).block_offset,
	 hashtable_get(st1, 1).block_offset,
	 hashtable_get(st1, 2).block_offset);
  print_hashtable_stats(st1);

  for (i = 0; i < 8; i++) data.chars[i] = i + 65; /* "ABCDEFGH" */
  index = hashtable_add(st1, "foo", length("foo"), data);
  TEST_ASSERT(index >= 0);

  index = hashtable_add(st1, "network", length("network"), data);
  TEST_ASSERT(index >= 0);
  index = hashtable_add(st1, "indefatigable", length("indefatigable"), data);
  TEST_ASSERT(index >= 0);

  print_hashtable(st1);

  /* Save the entry for use in later tests */
  saveentry = hashtable_search(st1, "indefatigable", length("indefatigable"));
  printf("status = %d\n", saveentry.block_offset);
  TEST_ASSERT(saveentry.block_offset >= 0);
  
  index = hashtable_add(st1, NULL, 0, data);
  TEST_ASSERT(index == HASHTABLE_ERR_NULL);

  index = hashtable_add(st1, "", 0, data);
  TEST_ASSERT(index >= 0);

  memset(data.chars, 'X', 8);
  index = hashtable_add(st1, "A\0Z", 3, data);
  TEST_ASSERT(index >= 0);

  entry = hashtable_search(st1, "A\0Z", 3);
  TEST_ASSERT(entry.block_offset >= 0);	      /* valid hashtable entry */
  TEST_ASSERT(memcmp(entry.data.chars, data.chars, 8) == 0);

  memset(data.chars, 'Y', 8);
  index = hashtable_add(st1, "\0", 1, data);
  TEST_ASSERT(index >= 0);

  entry = hashtable_search(st1, "\0", 1);
  TEST_ASSERT(entry.block_offset >= 0);	      /* "should get valid entry" */
  TEST_ASSERT(memcmp(entry.data.chars, data.chars, 8) == 0);

  print_hashtable_stats(st1);
  print_hashtable(st1);
    
  i = 0;
  index = PEXL_ITER_START;
  int32_t ptrlen;
  const char *ptr;
  while ((tmp = hashtable_iter(st1, &index)), tmp.block_offset >= 0) {
    ptr = hashtable_get_key(st1, hashtable_get_keyhandle(tmp), &ptrlen);
    printf(">> %.*s\n", ptrlen, ptr);
    i++;
  }
  TEST_ASSERT(i == 6);	  // empty string counts as one of the strings

  probe = hashtable_get_key(st1, hashtable_get_keyhandle(saveentry), &ptrlen);
  TEST_ASSERT(probe);		/* "expected a string" */
  printf("GET on saved entry with offset %d produced: %s\n",
	 hashtable_get_keyhandle(saveentry), probe);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Storing a string that is already there");
  
  print_hashtable_stats(st1);
  print_hashtable(st1);
  
  index = hashtable_add(st1, "indefatigable", length("indefatigable"), data);
  entry2 = hashtable_search(st1, "indefatigable", length("indefatigable"));
  TEST_ASSERT(entry2.block_offset == saveentry.block_offset); /* "should find same string again" */
  TEST_ASSERT(entry2.data.ptr == saveentry.data.ptr);	      /* "should find same data again" */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Storing a string that is too long");

  s = malloc(sizeof(char) * (HASHTABLE_MAX_KEYLEN+2));
  TEST_ASSERT(s);		/* "OOM?" */
  memset(s, 'a', HASHTABLE_MAX_KEYLEN + 1);
  s[HASHTABLE_MAX_KEYLEN + 1] = '\0';
  // Pre-condition for next test: key length too long
  TEST_ASSERT(length(s) == HASHTABLE_MAX_KEYLEN + 1);
  
  index = hashtable_add(st1, s, length(s), data);
  TEST_ASSERT(index == HASHTABLE_ERR_KEYLEN);

  /* Now try storing a string that is exactly the longest length allowed */
  s[HASHTABLE_MAX_KEYLEN] = '\0';
  index = hashtable_add(st1, s, length(s), data);
  TEST_ASSERT(index >= 0);
  entry = hashtable_search(st1, s, length(s));
  TEST_ASSERT(entry.block_offset > 0);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Searching the table");

  s[HASHTABLE_MAX_KEYLEN] = 'a'; /* Now the string is one char too long */
  entry = hashtable_search(st1, s, length(s));
  TEST_ASSERT(entry.block_offset <= 0); /* "should NOT find this too-long string" */

  s[HASHTABLE_MAX_KEYLEN] = '\0';
  entry = hashtable_search(st1, s, length(s));
  TEST_ASSERT(entry.block_offset > 0);
  probe = hashtable_get_key(st1, hashtable_get_keyhandle(entry), &ptrlen);
  TEST_ASSERT(probe);
  TEST_ASSERT(memcmp(probe, s, HASHTABLE_MAX_KEYLEN) == 0);

  entry = hashtable_search(st1, "indefatigable", length("indefatigable"));
  TEST_ASSERT(entry.block_offset == saveentry.block_offset);
  TEST_ASSERT(entry.data.ptr == saveentry.data.ptr);
  entry = hashtable_search(st1, "foo", length("foo"));
  TEST_ASSERT(entry.block_offset > 0);
  entry = hashtable_search(st1, "foobar", length("foobar"));
  TEST_ASSERT(entry.block_offset == HASHTABLE_NOT_FOUND);

  entry = hashtable_search(st1, NULL, 0);
  TEST_ASSERT(entry.block_offset < 0); /* "string cannot be NULL" */

  free(s);

  /* ----------------------------------------------------------------------------- */

  // There are around 200,000 words in /usr/dict/words
  const int NUMWORDS = FUZZING? 200000 : 100;
  TEST_ASSERT(FUZZING || (NUMWORDS <= 100));
  const char *static_words[100] =
    {"jabbed", "jabber", "jabberer", "jabbering", "jabberingly", "jabberment",
     "Jabberwock", "jabberwockian", "Jabberwocky", "jabbing", "jabbingly", "jabble",
     "jabers", "jabia", "jabiru", "jaborandi", "jaborine", "jabot", "jaboticaba",
     "jabul", "jacal", "Jacaltec", "Jacalteca", "jacamar", "Jacamaralcyon",
     "jacameropine", "Jacamerops", "jacami", "jacamin", "Jacana", "jacana", "Jacanidae",
     "Jacaranda", "jacare", "jacate", "jacchus", "jacent", "jacinth", "jacinthe",
     "Jack", "jack", "jackal", "jackanapes", "jackanapish", "jackaroo", "jackass",
     "jackassery", "jackassification", "jackassism", "jackassness", "jackbird", "jackbox",
     "jackboy", "jackdaw", "jackeen", "jacker", "jacket", "jacketed", "jacketing",
     "jacketless", "jacketwise", "jackety", "jackfish", "jackhammer", "jackknife",
     "jackleg", "jackman", "jacko", "jackpudding", "jackpuddinghood", "jackrod", "jacksaw",
     "jackscrew", "jackshaft", "jackshay", "jacksnipe", "Jackson", "Jacksonia",
     "Jacksonian", "Jacksonite", "jackstay", "jackstone", "jackstraw", "jacktan",
     "jackweed", "jackwood", "Jacky", "Jackye", "Jacob", "jacobaea", "jacobaean",
     "Jacobean", "Jacobian", "Jacobic", "Jacobin", "Jacobinia", "Jacobinic", "Jacobinical",
     "Jacobinically", "Jacobinism"};
  UNUSED(static_words);
  
#define WORDSFILE "/usr/share/dict/words"
  
  TEST_SECTION("Storing more strings");
  printf("Number of words: %d\n", NUMWORDS);
  int stat;
  printf("Current hash table status:\n");
  print_hashtable_stats(st1);

  if (FUZZING) {
    printf("Attempting to open dictionary file %s\n", WORDSFILE);
    words = fopen(WORDSFILE, "r");
    TEST_ASSERT_MSG(words, "Could not open dictionary file '%s'", WORDSFILE);
    stat = fseek(words, 0, SEEK_END);
    offset = ftell(words);

    pos = random_in(offset);
    if ((offset - pos) < (NUMWORDS * 10)) pos = offset - (NUMWORDS * 10);
    stat = fseek(words, pos, SEEK_SET);
    printf("Random seek returned %ld/%ld, which is %ld%% into file\n",
	   pos, offset, pos*100/offset);
    s = fgets(buf, MAXLEN, words); // read past a partial word
    stat = fgetpos(words, &startpos);
    TEST_ASSERT(stat == 0);
  } else {
    printf("Using statically defined list of strings\n");
    pos = random_in(NUMWORDS);
    TEST_ASSERT((pos >= 0) && (pos < NUMWORDS));
    printf("Random start index is %ld/%d, which is %ld%% into wordlist\n",
	   pos, NUMWORDS, pos*100/NUMWORDS);
    startindex = pos;
  }    
  
  for (numwords=0; numwords < NUMWORDS; numwords++) {
    if (FUZZING) {
      // Fuzz test reads from the unix dictionary file
      s = fgets(buf, MAXLEN, words);
      if (!s) {
	// On eof, start reading again at the start of the file
	rewind(words);
	printf("Need more words (have %d at EOF), rewinding to start of file.\n", numwords);
	s = fgets(buf, MAXLEN, words);
	TEST_ASSERT(s);
      }
      while((*s!='\0') && (*s!='\n')) s++;
      *s = '\0';		// overwrite the newline
    } else {
      // When not fuzzing, use the words in static_words
      assert(pos < NUMWORDS);
      memcpy(buf, static_words[pos], strlen(static_words[pos])+1);
      pos++;
      if (pos >= NUMWORDS) {
	printf("Rewinding after %d words to start of word list.\n", numwords);
	pos = 0;
      }
    }
    index = hashtable_add(st1, buf, length(buf), data);
    if (index < 0) {
      printf("Hashtable error.  Stats are:\n");
      print_hashtable_stats(st1);
      TEST_FAIL("Error %d from hashtable_add on string '%s'", index, buf);
    }
  } /* for */
  if (FUZZING)
    printf("%d strings read and added from " WORDSFILE "\n", numwords);
  else
    printf("%d strings read from statically defined word list\n", numwords);
    
  print_hashtable_stats(st1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Retrieving the same strings");

  if (FUZZING) {
    stat = fsetpos(words, &startpos);
    TEST_ASSERT(stat == 0);	/* fsetpos failed!? */
  } else {
    pos = startindex;
  }
  for (numwords=0; numwords < NUMWORDS; numwords++) {
    if (FUZZING) {
      s = fgets(buf, MAXLEN, words);
      if (!s) break;
      while((*s!='\0') && (*s!='\n')) s++;
      *s = '\0';			/* overwrite newline */
    } else {
      memcpy(buf, static_words[pos], strlen(static_words[pos])+1);
      pos++;
      if (pos >= NUMWORDS) pos = 0;
    }
    entry = hashtable_search(st1, buf, length(buf));
    if (entry.block_offset < 0) {
      printf("pexl_Error %d from hashtable_search.  pexl_Stats are:\n", entry.block_offset);
      print_hashtable_stats(st1);
      TEST_FAIL("Expected to find '%s' in hash table", buf);
    }
    // Ensure that both search() and get() succeed
    probe = hashtable_get_key(st1, hashtable_get_keyhandle(entry), &ptrlen);
    TEST_ASSERT(probe);
    assert(strlen(buf) == (size_t) ptrlen);
    assert(memcmp(buf, probe, strlen(buf)) == 0);
    
  } /* for */
  if (FUZZING)
    printf("%d strings verified from " WORDSFILE "\n", numwords);
  else
    printf("%d strings verified from statically defined word list\n", numwords);

  print_hashtable_stats(st1);

  hashtable_free(st1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing max number of entries with good load factor");

  if (!FUZZING) {
    printf("Skipping max entries test because FUZZING is not set\n");
    goto skip;
  }

  if ((HASHTABLE_MAX_BLOCKSIZE / HASHTABLE_MAX_SLOTS) < (4)) {
    printf("SKIPPING test of max number of entries because max block size"
	   " is not large enough to store expected number of strings\n");
    printf("HASHTABLE_MAX_SLOTS is %d\n", HASHTABLE_MAX_SLOTS);
    printf("HASHTABLE_MAX_BLOCKSIZE is %d\n", HASHTABLE_MAX_BLOCKSIZE);
    printf("Therefore, average string size must be less than %3.1f chars"
	   " (including NUL terminator)\n",
	   (float) HASHTABLE_MAX_BLOCKSIZE / (float) HASHTABLE_MAX_SLOTS);
    goto skip;
  }
  
  st1 = hashtable_new(10, 20);
  TEST_ASSERT(st1);

  expected_max = ((size_t) HASHTABLE_MAX_SLOTS) * HASHTABLE_EXPAND_LOAD / 100;
  printf("Current number of entries is %" pexl_Index_FMT "\n", st1->count);
  printf("Expected maximum number of entries is %lu\n", expected_max);
  for (numwords=st1->count; ((size_t) numwords) < expected_max; numwords++) {
    stat = asprintf(&s, "%x", numwords);
    index = hashtable_add(st1, s, length(s), data);
    free(s);
    if (index < 0) {
      if (index == HASHTABLE_ERR_BLOCKFULL) {
	printf("String table (block) is full\n");
	break;
      }
      if (index == HASHTABLE_ERR_FULL) {
	printf("String table (hash table) is full\n");
	break;
      }
      printf("hashtable_add failed with code %d at word %d\n", index, numwords);
    }
    assert(index >= 0);
  }
  // The loop above should finish WITHOUT an error
  printf("%d entries added\n", numwords);
  print_hashtable_stats(st1);
  TEST_ASSERT(index >= 0);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing max number of entries with high load factor");

  expected_max = ((size_t) HASHTABLE_MAX_SLOTS) * (HASHTABLE_MAX_LOAD + 1) / 100;
  printf("Currently, hashtable has %" pexl_Index_FMT " entries\n", st1->count);
  printf("Expected maximum entries under max load is %lu\n", expected_max);
  printf("So we should be able to add %lu more entries\n", expected_max - numwords);
  i = numwords;
  for (; ((size_t) numwords) < expected_max; numwords++) {
    stat = asprintf(&s, "%x", numwords);
    index = hashtable_add(st1, s, length(s), data);
    free(s);
    if (index < 0) {
      print_hashtable_stats(st1);
      printf("hashtable_add() failed with code %d at word %d\n", stat, numwords);
    }
    if (index == HASHTABLE_ERR_BLOCKFULL) {
      printf("String table (block) is full\n");
      break;
    }
    if (index == HASHTABLE_ERR_FULL) {
      printf("String table (hash table) is full\n");
      break;
    }
    assert(index >= 0);
  }
  TEST_ASSERT(true);		/* print the check mark */
  printf("%d entries added\n", numwords - i);
  print_hashtable_stats(st1);

  printf("Counting how many additional entries before FULL error\n");
  i = 0;
  while (1) {
    stat = asprintf(&s, "%x", numwords);
    numwords++;			/* must put unique strings into table! */
    index = hashtable_add(st1, s, length(s), data);
    free(s);
    if (index < 0) break;
    i++;
  }
  TEST_ASSERT(true);		/* print the check mark */
  printf("%d additional entries added\n", i);
  print_hashtable_stats(st1);

  if (index == HASHTABLE_ERR_BLOCKFULL) {
    printf("String table (block) is full\n");
  } else {
    TEST_ASSERT(i == 1);
    TEST_ASSERT(index == HASHTABLE_ERR_FULL);
  }

  hashtable_free(st1);

 skip:

  TEST_END();
}

