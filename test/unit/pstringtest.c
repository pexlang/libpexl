/*  -*- Mode: C/l; -*-                                                       */
/*                                                                           */
/*  pstringtest.c                                                            */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include "pstring.h"
#include "../test.h"

#define PRINTING true

int main (int argc, char **argv) {

  String *s1, *s2;
  StringSlice slice1, slice2;
  StringBuffer *b1;
  char *tmp;
  int err;
  
  string buf[] = "Hello!";

  string buflong[] = "zealful\nzealless\nzeallessness\nzealot\n"	\
    "zealotic\nzealotical\nzealotism\nzealotist\nzealotry\nzealous\n"	\
    "zealously\nzealousness\nzealousy\nzealproof\nzebra\nzebraic\n"	\
    "zebralike\nzebrass\nzebrawood\nZebrina\nzebrine\nzebrinny\n"	\
    "zebroid\nzebrula\nzebrule\nzebu\nzebub\nZebulunite\nzeburro\n"	\
    "zecchini\nzecchino\nzechin\nZechstein\nzed\nzedoary\nzee\nzeed\n"	\
    "Zeelander\nZeguha\nzehner\nZeidae\nzein\nzeism\nzeist\nZeke\n"	\
    "zel\nZelanian\nzelator\nzelatrice\nzelatrix\n";

  TEST_START(argc, argv);

  // -----------------------------------------------------------------------------
  TEST_SECTION("String_from and String_dup");

  s1 = String_from(NULL, 0);
  TEST_ASSERT_NULL(s1);

  s1 = String_from(buf, 0);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->ptr);
  TEST_ASSERT(s1->len == 0);
  String_free(s1);
  
  s1 = String_from(buf, 1);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->ptr);
  TEST_ASSERT(s1->len == 1);
  TEST_ASSERT(s1->ptr[0] == 'H');
  String_free(s1);

  s1 = String_from(buf, 2);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->ptr);
  TEST_ASSERT(s1->len == 2);
  TEST_ASSERT(s1->ptr[0] == 'H');
  TEST_ASSERT(s1->ptr[1] == 'e');
  String_free(s1);

  s1 = String_from(buf, strlen(buf));
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->ptr);
  TEST_ASSERT(s1->len == strlen(buf));
  TEST_ASSERT(memcmp(s1->ptr, buf, s1->len) == 0);
  String_free(s1);

  s1 = String_from(buflong, strlen(buflong));
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->ptr);
  TEST_ASSERT(s1->len == strlen(buflong));
  TEST_ASSERT(memcmp(s1->ptr, buflong, s1->len) == 0);

  s2 = String_dup(s1);
  TEST_ASSERT(s2);
  TEST_ASSERT(s2->ptr);
  TEST_ASSERT(s2->len == s1->len);
  TEST_ASSERT(String_cmp(s1, s2) == 0);
  String_free(s2);

  // Make s2 shorter, then compare
  s2 = String_from(buflong, strlen(buflong) - 1);
  TEST_ASSERT(String_cmp(s1, s2) > 0);

  // Make s1 shorter than s2, then compare again
  String_free(s1);
  s1 = String_from(buflong, strlen(buflong) - 2);
  TEST_ASSERT(String_cmp(s1, s2) < 0);
  String_free(s2);
  
  // Change s2 into something that sorts BEFORE s1 due to its first
  // character, but is otherwise the same (though also longer)
  tmp = strdup(buflong);
  tmp[0] = 'a';
  s2 = String_from(tmp, strlen(tmp));
  TEST_ASSERT(String_cmp(s1, s2) > 0);
  String_free(s1);
  String_free(s2);
  free(tmp);

  // -----------------------------------------------------------------------------
  TEST_SECTION("String_fill and String_from_literal");

  s1 = String_fill(0, 0);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 0);
  String_free(s1);

  s1 = String_fill(0, 1);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 1);
  TEST_ASSERT(s1->ptr[0] == '\0');
  String_free(s1);

  s1 = String_fill('A', 0);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 0);
  String_free(s1);

  s1 = String_fill('A', 1);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 1);
  TEST_ASSERT(s1->ptr[0] == 'A');
  String_free(s1);

  s1 = String_fill('A', 10 * 1000);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 10 * 1000);
  for (size_t i=0; i < s1->len; i++)
    if (s1->ptr[i] != 'A') TEST_FAIL("wrong string contents");
  String_free(s1);

  s1 = String_from_literal(NULL);
  TEST_ASSERT_NULL(s1);

  s1 = String_from_literal("");
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 0);
  String_free(s1);

  s1 = String_from_literal("Hello");
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 5);
  TEST_ASSERT(memcmp(s1->ptr, "Hello", 5) == 0);
  String_free(s1);

  // -----------------------------------------------------------------------------
  TEST_SECTION("String_len and String_slice");

  s1 = String_from_literal("");
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 0);
  TEST_ASSERT(String_len(s1) == 0);
  String_free(s1);

  s1 = String_from_literal("Hello");
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == 5);
  TEST_ASSERT(String_len(s1) == 5);

  slice1 = String_slice(s1, 0, 0);
  TEST_ASSERT(StringSlice_is_valid(slice1));
  TEST_ASSERT(slice1.len == 0);
  TEST_ASSERT(StringSlice_len(slice1) == 0);
  
  slice1 = String_slice(s1, 0, 0);
  TEST_ASSERT(StringSlice_is_valid(slice1));
  TEST_ASSERT(slice1.len == 0);
  TEST_ASSERT(StringSlice_len(slice1) == 0);

  slice1 = String_slice(s1, 0, 1);
  TEST_ASSERT(StringSlice_is_valid(slice1));
  TEST_ASSERT(slice1.len == 1);
  TEST_ASSERT(StringSlice_len(slice1) == 1);
  TEST_ASSERT(slice1.ptr[0] == 'H');
  TEST_ASSERT(*StringSlice_ptr(slice1) == 'H');

  slice1 = String_slice(s1, 1, 1);
  TEST_ASSERT(StringSlice_is_valid(slice1));
  TEST_ASSERT(slice1.len == 1);
  TEST_ASSERT(StringSlice_len(slice1) == 1);
  TEST_ASSERT(slice1.ptr[0] == 'e');
  TEST_ASSERT(*StringSlice_ptr(slice1) == 'e');

  slice1 = String_slice(s1, 2, 3);
  TEST_ASSERT(StringSlice_is_valid(slice1));
  TEST_ASSERT(slice1.len == 3);
  TEST_ASSERT(StringSlice_len(slice1) == 3);
  TEST_ASSERT(memcmp(slice1.ptr, "llo", 3) == 0);

  fprintf(stderr, "Expect a warning here:\n");
  slice1 = String_slice(s1, 3, 3);
  TEST_ASSERT(!StringSlice_is_valid(slice1));

  slice1 = String_slice(s1, 2, 2);
  TEST_ASSERT(StringSlice_is_valid(slice1));
  TEST_ASSERT(StringSlice_len(slice1) == 2);
  TEST_ASSERT(memcmp(slice1.ptr, "ll", 2) == 0);
  s2 = String_from_literal("ll");
  TEST_ASSERT(s2);
  slice2 = String_slice(s2, 0, 2);
  TEST_ASSERT(StringSlice_is_valid(slice2));
  TEST_ASSERT(StringSlice_len(slice2) == 2);
  TEST_ASSERT(memcmp(StringSlice_ptr(slice2), "ll", 2) == 0);

  TEST_ASSERT(StringSlice_cmp(slice1, slice2) == 0);

  slice2 = String_slice(s2, 0, 1);
  TEST_ASSERT(StringSlice_is_valid(slice2));
  TEST_ASSERT(StringSlice_len(slice2) == 1);
  TEST_ASSERT(*StringSlice_ptr(slice2) == 'l');
  TEST_ASSERT(StringSlice_cmp(slice1, slice2) > 0);
  TEST_ASSERT(StringSlice_cmp(slice2, slice1) < 0);

  String_free(s1);
  String_free(s2);


  // -----------------------------------------------------------------------------
  TEST_SECTION("StringBuffer");

  b1 = StringBuffer_new(0);
  TEST_ASSERT(b1);
  TEST_ASSERT(b1->capacity == STRINGBUFFER_MINSIZE);
  TEST_ASSERT(b1->len == 0);
  TEST_ASSERT(StringBuffer_len(b1) == 0);

  tmp = strdup(buflong);
  err = StringBuffer_add(NULL, tmp, 0);
  TEST_ASSERT(err);
  err = StringBuffer_add(b1, NULL, 0);
  TEST_ASSERT(err);

  err = StringBuffer_add(b1, tmp, 0);
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == 0);

  err = StringBuffer_add(b1, tmp, 1);
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == 1);
  TEST_ASSERT(b1->ptr[0] == tmp[0]);

  // Add the same char again
  err = StringBuffer_add(b1, tmp, 1);
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == 2);
  TEST_ASSERT(b1->ptr[0] == tmp[0]);
  TEST_ASSERT(b1->ptr[1] == tmp[0]);

  // Add until we get past STRINGBUFFER_MINSIZE, to test expansion
  while (StringBuffer_len(b1) < STRINGBUFFER_MINSIZE)
    TEST_ASSERT(!StringBuffer_add_string(b1, buflong));

  if (PRINTING)
    printf("Current len = %zu, capacity = %zu, and min size is %d\n",
	   b1->len, b1->capacity, STRINGBUFFER_MINSIZE);

  // Add more until capacity passes 3 * current capacity
  size_t goal = 3 * b1->capacity;
  while (StringBuffer_len(b1) < goal)
    TEST_ASSERT(!StringBuffer_add_string(b1, buflong));

  if (PRINTING)
    printf("Current len = %zu, capacity = %zu, and min size is %d\n",
	   b1->len, b1->capacity, STRINGBUFFER_MINSIZE);

  s1 = StringBuffer_toString(b1);
  TEST_ASSERT(s1);
  TEST_ASSERT(s1->len == b1->len);
  TEST_ASSERT(memcmp(s1->ptr, b1->ptr, s1->len) == 0);

  size_t old_len = StringBuffer_len(b1);
  TEST_ASSERT(old_len > 0);
  slice1 = String_slice(s1, 0, String_len(s1));
  TEST_ASSERT(StringSlice_is_valid(slice1));
  err = StringBuffer_add_slice(b1, slice1);
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == old_len + StringSlice_len(slice1));

  old_len = StringBuffer_len(b1);
  TEST_ASSERT(old_len > 0);
  err = StringBuffer_add_String(b1, s1);
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == old_len + String_len(s1));
  
  free(tmp);
  tmp = strdup("Hello");
  
  old_len = StringBuffer_len(b1);
  TEST_ASSERT(old_len > 0);
  err = StringBuffer_add_string(b1, tmp);
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == old_len + strlen(tmp));

  old_len = StringBuffer_len(b1);
  TEST_ASSERT(old_len > 0);
  err = StringBuffer_add_string(b1, "foobar");
  TEST_ASSERT(!err);
  TEST_ASSERT(StringBuffer_len(b1) == old_len + 6);

  String_free(s1);

  size_t old_capacity = b1->capacity;
  StringBuffer_reset(b1);
  TEST_ASSERT(StringBuffer_len(b1) == 0);
  TEST_ASSERT(b1->capacity == old_capacity);

  StringBuffer_free(b1);
  free(tmp);

  TEST_END();
}

