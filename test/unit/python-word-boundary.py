#  -*- Mode: Python; -*-                                                   
#  -*- coding: utf-8; -*-
# 
#  python-word-boundary.py
# 
#  AUTHOR: Jamie A. Jennings


import re

def btest():
    b = ( '(?:' + '(?:^[A-Za-z0-9_])' + '|'
          + '(?:$(?<=[A-Za-z0-9_]))' + '|'
          + '(?:(?<=[A-Za-z0-9_])(?=[^A-Za-z0-9_]))' + '|'
          + '(?:(?<=[^A-Za-z0-9_])(?=[A-Za-z0-9_]))'
          + ')' )
    assert(    re.match('abc' + b, 'abc!'))
    assert(not re.match('abc' + b, 'abcd'))
    assert(    re.match('ab%' + b, 'ab%d'))
    assert(    re.match(b, 'abc'))
    assert(not re.match(b, '!abc'))
    assert(    re.match('abc' + b, 'abc'))
    assert(not re.match('abc%' + b, 'abc%'))
    return True

def anchortest():
    s = '(?<!.)'                # Not looking behind at any char
    assert(    re.match(s, "abc"))
    assert(not re.match("a" + s, "abc"))
    assert(not re.match("abc" + s, "abc"))
    e = '(?!.)'
    assert(not re.match(e, "abc"))
    assert(not re.match("a" + e, "abc"))
    assert(    re.match("abc" + e, "abc"))
    return True

if btest(): print("All boundary tests passed")
if anchortest(): print("All anchor tests passed")
