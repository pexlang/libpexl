/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/* ip_patterns.h  for testing complex hand-written patterns                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Vivek Reddy Karri                                               */

#include <string.h>
#include <stdlib.h>		/* exit() */
#include <errno.h>		/* errno */

#include "libpexl.h"		/* Compiler optimization masks */

#include "compile.h"
#include "vm.h"
#include "binary.h"
#include "opt.h"

#include "../test.h"

/* 
   A Generic Template for writing a Pattern
    
   Points to Consider before writing one:
   1) A Pattern can be Composed of multiple pexl_Expr *s/pexl_References.
   2) This is handled by having an array of pexl_Expr *s & pexl_References.
   3) Make sure the First pexl_Expr *in the array is the one which 
      is the one to be compiled to generate a package.

*/
typedef struct generic_pattern {
  pexl_Context *C;
  pexl_Binary *pkg;
  size_t num_expr;
  pexl_Expr **exp_arr; /* Array of pexl_Expr *s */
  pexl_Ref *ref_arr;        /* Array of pexl_References */
} pattern;

typedef struct _vm_match {
  const char *input;
  size_t inputlen;
  pexl_Match *match;
  SymbolTableEntry *entry;
} vm_match;

/* Methods to operate on Patern & vm_match Struct */

void compile_generic_pattern(pattern* pt, pexl_Optims *optims);
void generic_pattern_free(pattern *);
void vm_match_free(vm_match* );

// Unfortunately, as written, this can only be called ONCE on a given pt
void compile_generic_pattern (pattern* pt, pexl_Optims *optims) {
  pexl_Error err; 
  assert(pt->C);
  if (!pt || !pt->ref_arr) {
    printf("No pexl_Expr *found to compile, \
                         Please Create an Entry Point pexl_Expr */pexl_Reference at pexl_Index 0 \
                                of the pexl_Expr *Array/pexl_Reference Array");
    TEST_FAIL("invalid arguments to compile_generic_pattern");
  }
  printf("Compiling with peephole %s, inlining %s\n",
	 (optimlist_contains(optims, PEXL_OPT_PEEPHOLE)) ? "enabled" : "disabled",
	 (optimlist_contains(optims, PEXL_OPT_INLINE)) ? "enabled" : "disabled");

  pexl_Binary *pkg = pexl_compile(pt->C, pt->ref_arr[0],
				  optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  pt->pkg = pkg;
}


void generic_pattern_free(pattern* pt){
    if (!pt) return ;
    if (pt->C) pexl_free_Context(pt->C);
    if (pt->pkg) pexl_free_Binary(pt->pkg);
    if (pt->ref_arr) free(pt->ref_arr);
    if (pt->exp_arr) {
      for (size_t i = 0; i < pt->num_expr; i++)
	pexl_free_Expr(pt->exp_arr[i]);
      free(pt->exp_arr);
    }
    free(pt);
}

void vm_match_free(vm_match* vmm){
    if (!vmm) return ;
    if (vmm->match) {
      pexl_free_Match(vmm->match);
    }
    free(vmm);
}

static vm_match* vm_match_new(pattern* pt,
			      const char *input, size_t len,
			      const char* prefix,
			      pexlEncoderID encoder_id) {
    vm_match* vm_m;
    if (!pt || !input) return NULL;
    vm_m = (vm_match*)malloc(sizeof(vm_match));

    if (vm_m == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    vm_m->input = input;
    vm_m->inputlen = len;

    TEST_ASSERT(pt->pkg);

    TEST_ASSERT(!binary_set_attributes(pt->pkg, NULL, prefix, NULL, NULL, 0, 0));

    vm_m->match = pexl_new_Match(encoder_id);
    TEST_ASSERT(vm_m->match);
    
    return vm_m;
}

/*
  SIMPLEST POSSIBLE PATTERNS TO TEST WITH AND WITHOUT CERTAIN
  OPTIMIZATIONS.  Once created, the pt object OWNS the context arg,
  and will free it.  Passing in a pexl_Context struct allows the caller to
  configure whatever optimizations they want to use before building
  the pattern.
*/

// A -> a*
__attribute__((unused)) static pattern* a_star_no_call(void){
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }
    pt->num_expr = 1;
    pt->C = pexl_new_Context();
    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    pt->ref_arr[0] = pexl_bind(pt->C, NULL, "a_star_no_call"); 

    pt->exp_arr[0] = pexl_repeat_f(pt->C,		
				   pexl_match_string(pt->C, "a"),
				   0, 0);
    i = pt->num_expr-1;
    for (; i >= 0; i--) {
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }  
    return pt;
}

/*
  a_star -> A*
  A -> a
*/
__attribute__((unused)) static pattern* a_star(void){
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }
    pt->num_expr = 2;
    pt->C = pexl_new_Context();

    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    pt->ref_arr[0] = pexl_bind(pt->C, NULL, "a_star"); 
    pt->ref_arr[1] = pexl_bind(pt->C, NULL, "A"); 

    pt->exp_arr[0] = pexl_capture_f(pt->C,
				     "capture of a*",
				     pexl_repeat_f(pt->C,
						   pexl_call(pt->C, pt->ref_arr[1]),
						   0, 0));
    pt->exp_arr[1] = pexl_match_string(pt->C, "a");

    i = pt->num_expr-1;
    for (; i >= 0; i--){
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }  
    return pt;
}




/* Example Pattern functions Implemented */
static pattern* create_ipv6(void);
static pattern* create_ipv4(void);
static pattern* create_ipaddr(void);

/* 
An Example usage of pattern template to create a  pattern.

                    Production Rules for ipv6 Pattern:

xdigit = [0-9a-fA-F]

ipv6_component = [xdigit] [xdigit]{0,3}

ipv6_rest = { ":" !>ipv4 ipv6_component }
Note: First version of this file used this simplified rule: ipv6_rest = { ":" ipv6_component}

ip_address_v6 = { ipv6_component ipv6_rest{7} } /
			    { ipv6_component "::" ipv6_component ipv6_rest{0,4} } /
			    { ipv6_component ipv6_rest{1} "::" ipv6_component ipv6_rest{0,3} } /
			    { ipv6_component ipv6_rest{2} "::" ipv6_component ipv6_rest{0,2} } /
			    { ipv6_component ipv6_rest{3} "::" ipv6_component ipv6_rest{0,1} } /
			    { ipv6_component ipv6_rest{4} "::" ipv6_component } /
			    { ipv6_component ipv6_rest{5} "::" } /
			    { "::" ipv6_component ipv6_rest{0,5} } /
			    { "::" } -- undefined address

ipv6_strict = ip_address_v6

                    Production Rules for ipv4 Pattern:

ipv4_component =  [0] /
			     {[1] [0-9]? [0-9]?} / 
			     {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2] [0-9]?} /
			     {[3-9] [0-9]?} 
			     
ip_address_v4 = { ipv4_component {"." ipv4_component}{3} }


*/


/* ---------------------------  Primitive Chunks for IPv6    ------------------------- */

static const char xdigit_set[] = "0123456789abcdefABCDEF";

static pexl_Expr * exp_xdigit(pattern* ip){
    return pexl_match_set(ip->C, xdigit_set, strlen(xdigit_set));
}

static pexl_Expr * expr_ipv6_component(pattern* ip){
  return pexl_capture_f(ip->C,
			 "IPV6component",
			 pexl_seq_f(ip->C, 
				    exp_xdigit(ip), 
				    pexl_repeat_f(ip->C, exp_xdigit(ip), 0, 3)));
}

static pexl_Expr * expr_ipv6_rest(pattern* ip, pexl_Ref ipv4_ref){
  return pexl_seq_f(ip->C,
		   pexl_match_string(ip->C, ":"),
		   pexl_seq_f(ip->C,
			     pexl_neg_lookahead_f(ip->C, pexl_call(ip->C, ipv4_ref)),
			     expr_ipv6_component(ip)));
}

static pexl_Expr * expr_ipv6_rest_2(pattern* ip, pexl_Ref ipv4_ref){
  return pexl_seq_f(ip->C,  expr_ipv6_rest(ip, ipv4_ref), expr_ipv6_rest(ip, ipv4_ref));
}

static pexl_Expr * expr_ipv6_rest_4(pattern* ip, pexl_Ref ipv4_ref){
  return pexl_seq_f(ip->C,  expr_ipv6_rest_2(ip, ipv4_ref), expr_ipv6_rest_2(ip, ipv4_ref));
}

/* ---------------------------- Simple Patterns ----------------------------  */

//   A -> "a" A "bc" D / "c" B 
//   D -> [1-9]{0,3} 
//   B -> A 

/* Used in peepholetest but not inlineoptimizertest.  Mark as unused to suppress compiler warning. */
__attribute__((unused)) static pattern* recursive_pattern4(void){
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    printf("Building recursive_pattern4\n");

    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 3;
    pt->C = pexl_new_Context();
    assert(pt->C);

    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    pt->ref_arr[2] = pexl_bind(pt->C, NULL, "D"); 
    pt->ref_arr[1] = pexl_bind(pt->C, NULL, "B"); 
    pt->ref_arr[0] = pexl_bind(pt->C, NULL, "A"); 

    pt->exp_arr[2] = pexl_repeat_f(pt->C,
				   pexl_match_set(pt->C, "123456789", 9),
				   0, 3); 
    pt->exp_arr[1] = pexl_call(pt->C, pt->ref_arr[0]);
    pt->exp_arr[0] = pexl_choice_f(pt->C, 
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "a"),
                                pexl_seq_f(pt->C, 
                                    pexl_call(pt->C, pt->ref_arr[0]),
                                    pexl_seq_f(pt->C,
                                        pexl_match_string(pt->C, "bc"),
                                        pexl_call(pt->C,  pt->ref_arr[2])))),
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "c"),
                                pexl_call(pt->C, pt->ref_arr[1]))); 

    i = pt->num_expr-1;
    for (; i >= 0; i--){
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }  
    return pt;
}

//   A -> "a" A "b" D / "c" B
//   D -> A 
//   B -> D 

/* Used in peepholetest but not inlineoptimizertest */
__attribute__((unused)) static pattern* recursive_pattern3(void){
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    printf("Building recursive_pattern3\n");

    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 3;
    pt->C = pexl_new_Context();
    assert(pt->C);

    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    pt->ref_arr[2] = pexl_bind(pt->C, NULL, "D"); 
    pt->ref_arr[1] = pexl_bind(pt->C, NULL, "B"); 
    pt->ref_arr[0] = pexl_bind(pt->C, NULL, "A"); 

    printf("D stored at binding slot #%d\n", pt->ref_arr[2]);
    printf("B stored at binding slot #%d\n", pt->ref_arr[1]);
    printf("A stored at binding slot #%d\n", pt->ref_arr[0]);

    pt->exp_arr[2] = pexl_call(pt->C, pt->ref_arr[0]); // D -> A
    pt->exp_arr[1] = pexl_call(pt->C, pt->ref_arr[2]); // B -> D
    // A does all the work:
    pt->exp_arr[0] = pexl_choice_f(pt->C,		
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "a"),
                                pexl_seq_f(pt->C, 
                                    pexl_call(pt->C, pt->ref_arr[0]),
                                    pexl_seq_f(pt->C,
                                        pexl_match_string(pt->C, "bc"),
                                        pexl_call(pt->C, pt->ref_arr[2])))),
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "c"),
                                pexl_call(pt->C,  pt->ref_arr[1]))); 

    i = pt->num_expr-1;
    for (; i >= 0; i--){
      if (PRINTING) {
	printf("pexl_Expr *bound to ref index %d:\n", pt->ref_arr[i]);
	pexl_print_Expr(pt->exp_arr[i], 0, pt->C);
      }
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }
    if (PRINTING) print_BindingTable(pt->C);
    return pt;
}

//  A -> "a" A "b" / "c" B 
//  D -> A 
//  B -> D 
static pattern* recursive_pattern2(void){
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    printf("Building recursive_pattern2\n");

    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 3;
    pt->C = pexl_new_Context();
    assert(pt->C);

    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    pt->ref_arr[2] = pexl_bind(pt->C, NULL, "D"); 
    pt->ref_arr[1] = pexl_bind(pt->C, NULL, "B"); 
    pt->ref_arr[0] = pexl_bind(pt->C, NULL, "A"); 

    pt->exp_arr[2] = pexl_call(pt->C, pt->ref_arr[0]);
    pt->exp_arr[1] = pexl_call(pt->C, pt->ref_arr[2]);
    pt->exp_arr[0] = pexl_choice_f(pt->C,		/* A -> "a" A "b" / "c" B */
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "a"),
                                pexl_seq_f(pt->C, 
                                    pexl_call(pt->C, pt->ref_arr[0]),
                                    pexl_match_string(pt->C, "b"))),
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "c"),
                                pexl_call(pt->C, pt->ref_arr[1]))); 

    i = pt->num_expr-1;
    for (; i >= 0; i--){
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }  
    return pt;
}

//  A -> "a" A "b" / "c" B 
//  B -> Epsilon
static pattern* recursive_pattern(void){
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));

    printf("Building recursive_pattern\n");

    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    pt->num_expr = 2;
    pt->C = pexl_new_Context();
    assert(pt->C);

    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    pt->ref_arr[1] = pexl_bind(pt->C, NULL, "B"); 
    pt->ref_arr[0] = pexl_bind(pt->C, NULL, "A"); 

    pt->exp_arr[1] = pexl_match_epsilon(pt->C);
    pt->exp_arr[0] = pexl_choice_f(pt->C,		
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "a"),
                                pexl_seq_f(pt->C, 
                                    pexl_call(pt->C, pt->ref_arr[0]),
                                    pexl_match_string(pt->C, "b"))),
                            pexl_seq_f(pt->C,
                                pexl_match_string(pt->C, "c"),
                                pexl_call(pt->C, pt->ref_arr[1])));
    i = pt->num_expr-1;
    for (; i >= 0; i--){
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }  
    return pt;
}

static pattern* create_ipaddr(void){
    int i;
    pexl_Expr *ipv6_choice_1, *ipv6_choice_2, *ipv6_choice_3, *ipv6_choice_4;
    pexl_Expr *ipv6_choice_5, *ipv6_choice_6, *ipv6_choice_7, *ipv6_choice_8;
    pexl_Expr *ipv4_choice_1, *ipv4_choice_2, *ipv4_choice_3, *ipv4_choice_4, *ipv4_choice_5;
    pexl_Ref ipv4_ref;

    pattern* ip = (pattern*)malloc(sizeof(pattern));

    if (ip == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    ip->num_expr = 11;
    ip->C = pexl_new_Context();

    /* Allocate Space for pexl_Expr *s & pexl_References */
    ip->exp_arr = (pexl_Expr **)malloc(ip->num_expr*sizeof(pexl_Expr *));
    ip->ref_arr = (pexl_Ref *)malloc(ip->num_expr*sizeof(pexl_Ref));

    ip->ref_arr[0] = pexl_bind(ip->C, NULL, "ipv6 / ipv4");
    /* pexl_References to Ipv6 Related pexl_Expr *s */
    ip->ref_arr[1] = pexl_bind(ip->C, NULL, "ipv6_strict");
    ip->ref_arr[2] = pexl_bind(ip->C, NULL, "ipv6_rest{4}");
    ip->ref_arr[3] = pexl_bind(ip->C, NULL, "ipv6_rest{2}");
    ip->ref_arr[4] = pexl_bind(ip->C, NULL, "ipv6_rest{1}");    
    ip->ref_arr[5] = pexl_bind(ip->C, NULL, "ipv6_component");
    /*  pexl_References to Ipv4 Related pexl_Expr *s */
    ip->ref_arr[6] = pexl_bind(ip->C, NULL, "ipv4");
    ip->ref_arr[7] = pexl_bind(ip->C, NULL, "Seq {'.' ipv4_component}"); 
    ip->ref_arr[8] = pexl_bind(ip->C, NULL, "ipv4_component");
    ip->ref_arr[9] = pexl_bind(ip->C, NULL, "[0-9]?"); 
    ip->ref_arr[10] = pexl_bind(ip->C, NULL, "[0-9]");
    
    /* -----------------------------------------  IPv4 Pattern ------------------------  */

    /* Helpful pexl_Expr *s */
    ip->exp_arr[10] = pexl_match_set(ip->C, "0123456789", 10);
    ip->exp_arr[9] = pexl_repeat_f(ip->C,
				   pexl_match_set(ip->C, "0123456789", 10),
				   0, 1); /* [0-9]? */
    
    /* {[1] [0-9]? [0-9]?} */
    /* Some Issue With Changing 1st Child of Seq */
    ipv4_choice_1 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "1"),
			      pexl_seq_f(ip->C, 
					pexl_repeat_f(ip->C,
						      pexl_match_set(ip->C, "0123456789", 10),
						      0, 1),
					pexl_call(ip->C, ip->ref_arr[9])));

    /* {[2] [0-4] [0-9]} */
    ipv4_choice_2 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "2"),
			      pexl_seq_f(ip->C, 
					pexl_match_set(ip->C, "01234", 5),
					pexl_call(ip->C, ip->ref_arr[10])));

    /* {[2] [5] [0-5]} */
    ipv4_choice_3 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "2"),
			      pexl_seq_f(ip->C, 
					pexl_match_string(ip->C, "5"),
					pexl_match_set(ip->C, "012345", 6)));
    /* {[2] [0-9]?} */
    ipv4_choice_4 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "2"),
			      pexl_call(ip->C, ip->ref_arr[9]));
    
    /* {[3-9] [0-9]?} */
    ipv4_choice_5 = pexl_seq_f(ip->C, 
			      pexl_match_set(ip->C, "3456789", 7),
			      pexl_call(ip->C, ip->ref_arr[9]));

    /* [0] /    {[1] [0-9]? [0-9]?} / 
			     {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2] [0-9]?} /
			     {[3-9] [0-9]?} 
    */

    ip->exp_arr[8] =
      pexl_capture_f(ip->C,
		      "IPV4component",
		      pexl_choice_f(ip->C, 
				     pexl_match_string(ip->C, "0"),
				     pexl_choice_f(ip->C, 
						    ipv4_choice_1,
						    pexl_choice_f(ip->C, 
								   ipv4_choice_2,
								   pexl_choice_f(ip->C, 
										  ipv4_choice_3,
										  pexl_choice_f(ip->C, 
												 ipv4_choice_4,
												 ipv4_choice_5))))));

    ip->exp_arr[7] = pexl_seq_f(ip->C, pexl_match_string(ip->C, "."),  pexl_call(ip->C, ip->ref_arr[8]));

    /* ip_address_v4 = { ipv4_component {"." ipv4_component}{3} } */
    ip->exp_arr[6] =
      pexl_capture_f(ip->C,
		      "ipv4",
		      pexl_seq_f(ip->C, pexl_call(ip->C, ip->ref_arr[8]), 
				  pexl_seq_f(ip->C,
					      pexl_seq_f(ip->C, 
							  pexl_call(ip->C, ip->ref_arr[7]), 
							  pexl_call(ip->C, ip->ref_arr[7])),
					      pexl_call(ip->C, ip->ref_arr[7]))));


    ipv4_ref = ip->ref_arr[6];	/* refer to "ipv4" pattern */

    ip->exp_arr[5] = expr_ipv6_component(ip);
    ip->exp_arr[4] = expr_ipv6_rest(ip, ipv4_ref); /* refer to ipv4 */
    ip->exp_arr[3] = expr_ipv6_rest_2(ip, ipv4_ref);
    ip->exp_arr[2] = expr_ipv6_rest_4(ip, ipv4_ref);

    /* -----------------------------------------  IPv6 Pattern ------------------------ */

    /* ipv6_component ipv6_rest{4} ipv6_rest{2} ipv6_rest{1} */
    ipv6_choice_1 = pexl_seq_f(ip->C, 
			      pexl_call(ip->C, ip->ref_arr[5]), 
			      pexl_seq_f(ip->C, 
                                        pexl_seq_f(ip->C, 
						  pexl_call(ip->C, ip->ref_arr[2]), 
						  pexl_call(ip->C, ip->ref_arr[3])),
					pexl_call(ip->C, ip->ref_arr[4])));
    
    /* ipv6_component "::" ipv6_component ipv6_rest{0,4} */
    ipv6_choice_2 = pexl_seq_f(ip->C,
			      pexl_seq_f(ip->C, 
                                        pexl_call(ip->C, ip->ref_arr[5]),
                                        pexl_match_string(ip->C, "::")),
			      pexl_seq_f(ip->C, 
                                        pexl_call(ip->C, ip->ref_arr[5]),
                                        pexl_repeat_f(ip->C,
						      pexl_call(ip->C, ip->ref_arr[4]),
						      0, 4)));
                                        
    /*  ipv6_component ipv6_rest{1} "::" ipv6_component ipv6_rest{0,3} } */
    ipv6_choice_3 = pexl_seq_f(ip->C,    
			      pexl_seq_f(ip->C,
                                        pexl_seq_f(ip->C, 
						  pexl_call(ip->C, ip->ref_arr[5]),
						  pexl_call(ip->C, ip->ref_arr[4])),
                                        pexl_seq_f(ip->C,
						  pexl_match_string(ip->C, "::"),
						  pexl_call(ip->C, ip->ref_arr[5]))),
			      pexl_repeat_f(ip->C,
					    pexl_call(ip->C, ip->ref_arr[4]),
					    0, 3));
    
    /*  { ipv6_component ipv6_rest{2} "::" ipv6_component ipv6_rest{0,2} } } */
    ipv6_choice_4 = pexl_seq_f(ip->C,    
			      pexl_seq_f(ip->C,
                                        pexl_seq_f(ip->C, 
						  pexl_call(ip->C, ip->ref_arr[5]),
						  pexl_call(ip->C, ip->ref_arr[3])),
                                        pexl_seq_f(ip->C,
						  pexl_match_string(ip->C, "::"),
						  pexl_call(ip->C, ip->ref_arr[5]))),
			      pexl_repeat_f(ip->C,
					    pexl_call(ip->C, ip->ref_arr[4]),
					    0, 2));
    
    /* { ipv6_component ipv6_rest{3} "::" ipv6_component ipv6_rest{0,1} } */
    ipv6_choice_5 = pexl_seq_f(ip->C,    
			      pexl_seq_f(ip->C,
                                        pexl_seq_f(ip->C, 
						  pexl_call(ip->C, ip->ref_arr[5]),
						  pexl_seq_f(ip->C,
							    pexl_call(ip->C, ip->ref_arr[3]),
							    pexl_call(ip->C, ip->ref_arr[4]))),
                                        pexl_seq_f(ip->C,
						  pexl_match_string(ip->C, "::"),
						  pexl_call(ip->C, ip->ref_arr[5]))),
			      pexl_repeat_f(ip->C,
					    pexl_call(ip->C, ip->ref_arr[4]),
					    0, 1));

    /* { ipv6_component ipv6_rest{4} "::" ipv6_component } / */
    ipv6_choice_6 = pexl_seq_f(ip->C,
			      pexl_seq_f(ip->C, 
                                        pexl_call(ip->C, ip->ref_arr[5]),
                                        pexl_call(ip->C, ip->ref_arr[2])),
			      pexl_seq_f(ip->C, 
                                        pexl_match_string(ip->C, "::"),
                                        pexl_call(ip->C, ip->ref_arr[5])));

    /* { "::" ipv6_component ipv6_rest{0,5} } /*/
    ipv6_choice_7 = pexl_seq_f(ip->C,
                                    pexl_seq_f(ip->C, 
                                        pexl_match_string(ip->C, "::"),
                                        pexl_call(ip->C, ip->ref_arr[5])),
                                    pexl_repeat_f(ip->C,
						  pexl_call(ip->C, ip->ref_arr[4]),
						  0, 5));

    /* ipv6_component ipv6_rest{7} */
    ipv6_choice_8 = pexl_seq_f(ip->C,
                                    pexl_call(ip->C, ip->ref_arr[5]),  
                                        pexl_seq_f(ip->C, 
                                            pexl_call(ip->C, ip->ref_arr[2]), 
                                                pexl_seq_f(ip->C, 
                                                    pexl_call(ip->C, ip->ref_arr[3]), 
                                                    pexl_call(ip->C, ip->ref_arr[4]))));
    
    /* Final pexl_Expr *for ip_address_strict */
    
    ip->exp_arr[1] =
      pexl_capture_f(ip->C,
		      "ipv6",
		      pexl_choice_f(ip->C, pexl_match_string(ip->C, "::"), 
				     pexl_choice_f(ip->C, 
						    pexl_choice_f(ip->C,
								   pexl_choice_f(ip->C, ipv6_choice_1, ipv6_choice_2), 
								   pexl_choice_f(ip->C, 
										  pexl_choice_f(ip->C, ipv6_choice_3, ipv6_choice_4), 
										  pexl_choice_f(ip->C, ipv6_choice_5, ipv6_choice_6))),
						    pexl_choice_f(ip->C, ipv6_choice_7, ipv6_choice_8))));

    /* ipv4 /ipv6 */
    ip->exp_arr[0] =
      pexl_capture_f(ip->C,
		      "ip",
		      pexl_choice_f(ip->C,
				     pexl_call(ip->C, ip->ref_arr[6]),
				     pexl_call(ip->C, ip->ref_arr[1]))); 

    i = 0;
    for (; i <= 10; i++){
      TEST_ASSERT(pexl_rebind(ip->C, ip->ref_arr[i], ip->exp_arr[i]) == OK);
    } 
    return ip;
}

static pattern* create_ipv4(void){
    int i;
    pexl_Expr *ipv4_choice_1, *ipv4_choice_2, *ipv4_choice_3, *ipv4_choice_4, *ipv4_choice_5;
    pattern* ip = (pattern*)malloc(sizeof(pattern));

    if (ip == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    ip->num_expr = 5;
    ip->C = pexl_new_Context();

    /* Allocate Space for pexl_Expr *s & pexl_References */
    ip->exp_arr = (pexl_Expr **)malloc(ip->num_expr*sizeof(pexl_Expr *));
    ip->ref_arr = (pexl_Ref *)malloc(ip->num_expr*sizeof(pexl_Ref));

    ip->ref_arr[4] = pexl_bind(ip->C, NULL, "[0-9]");
    ip->ref_arr[3] = pexl_bind(ip->C, NULL, "[0-9]?");
    ip->ref_arr[2] = pexl_bind(ip->C, NULL, "ipv4_component");    
    ip->ref_arr[1] = pexl_bind(ip->C, NULL, "Seq {'.' ipv4_component}");
    ip->ref_arr[0] = pexl_bind(ip->C, NULL, "ipv4"); 

    /*  pexl_References to Ipv4 Related pexl_Expr *s*/

    /* Helpful pexl_Expr *s */
    ip->exp_arr[4] = pexl_match_set(ip->C, "0123456789", 10);
    ip->exp_arr[3] = pexl_repeat_f(ip->C,
				   pexl_match_set(ip->C, "0123456789", 10),
				   0, 1); /* [0-9]? */
    
    /* {[1] [0-9]? [0-9]?} */
    ipv4_choice_1 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "1"),
			      pexl_seq_f(ip->C, 
					pexl_call(ip->C, ip->ref_arr[3]),
					pexl_call(ip->C, ip->ref_arr[3])));

    /* {[2] [0-4] [0-9]} */
    ipv4_choice_2 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "2"),
			      pexl_seq_f(ip->C, 
					pexl_match_set(ip->C, "01234", 5),
					pexl_call(ip->C, ip->ref_arr[4])));

    /* {[2] [5] [0-5]} */
    ipv4_choice_3 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "2"),
			      pexl_seq_f(ip->C, 
					pexl_match_string(ip->C, "5"),
					pexl_match_set(ip->C, "012345", 6)));
    /* {[2] [0-9]?} */
    ipv4_choice_4 = pexl_seq_f(ip->C, 
			      pexl_match_string(ip->C, "2"),
			      pexl_call(ip->C, ip->ref_arr[3]));
    
    /* {[3-9] [0-9]?} */
    ipv4_choice_5 = pexl_seq_f(ip->C, 
			      pexl_match_set(ip->C, "3456789", 7),
			      pexl_call(ip->C, ip->ref_arr[3]));

    /* [0] /    {[1] [0-9]? [0-9]?} / 
			     {[2] [0-4] [0-9]} / {[2] [5] [0-5]} / {[2] [0-9]?} /
			     {[3-9] [0-9]?} 
    */

    ip->exp_arr[2] = pexl_choice_f(ip->C, 
				  pexl_match_string(ip->C, "0"),
				  pexl_choice_f(ip->C, 
					       ipv4_choice_1,
					       pexl_choice_f(ip->C, 
							    ipv4_choice_2,
							    pexl_choice_f(ip->C, 
									 ipv4_choice_3,
									 pexl_choice_f(ip->C, 
										      ipv4_choice_4,
										      ipv4_choice_5)))));

    ip->exp_arr[1] = pexl_seq_f(ip->C,
				 pexl_match_string(ip->C, "."),
				 pexl_call(ip->C, ip->ref_arr[2]));

    /* ip_address_v4 = { ipv4_component {"." ipv4_component}{3} } */
    ip->exp_arr[0] = pexl_seq_f(ip->C,
				 pexl_call(ip->C, ip->ref_arr[2]), 
				 pexl_seq_f(ip->C,
					     pexl_seq_f(ip->C, 
							 pexl_call(ip->C, ip->ref_arr[1]), 
							 pexl_call(ip->C, ip->ref_arr[1])),
					     pexl_call(ip->C, ip->ref_arr[1])));

    /* ip->exp_arr[0] = pexl_call(ip->C, ip->ref_arr[2]); */

    i = ip->num_expr-1;
    for (; i >= 0; i--){
      TEST_ASSERT(pexl_rebind(ip->C, ip->ref_arr[i], ip->exp_arr[i]) == OK);
    }

    return ip;
}

static pattern* create_ipv6(void){
    int i;
    pexl_Expr *ipv6_choice_1, *ipv6_choice_2, *ipv6_choice_3, *ipv6_choice_4;
    pexl_Expr *ipv6_choice_5, *ipv6_choice_6, *ipv6_choice_7, *ipv6_choice_8;
    pexl_Ref ipv4_ref;
    pattern *ipv4 = create_ipv4();
    pattern *ipv6 = (pattern*)malloc(sizeof(pattern));

    if (ipv6 == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1); 
    }

    ipv6->num_expr = 5;
    ipv6->C = ipv4->C;

    /* Allocate Space for pexl_Expr *s & pexl_References */
    ipv6->exp_arr = (pexl_Expr **)malloc(ipv6->num_expr*sizeof(pexl_Expr *));
    ipv6->ref_arr = (pexl_Ref *)malloc(ipv6->num_expr*sizeof(pexl_Ref));

    /* pexl_References to the pexl_Expr *s */
    ipv6->ref_arr[0] = pexl_bind(ipv6->C, NULL, "ipv6_strict");
    ipv6->ref_arr[1] = pexl_bind(ipv6->C, NULL, "ipv6_rest{4}");
    ipv6->ref_arr[2] = pexl_bind(ipv6->C, NULL, "ipv6_rest{2}");
    ipv6->ref_arr[3] = pexl_bind(ipv6->C, NULL, "ipv6_rest{1}");    
    ipv6->ref_arr[4] = pexl_bind(ipv6->C, NULL, "ipv6_component");


    /* pexl_Expr *s to use them directly in Final Sequence */

    /* ipv6_component ipv6_rest{4} ipv6_rest{2} ipv6_rest{1} */
    ipv6_choice_1 = pexl_seq_f(ipv6->C, 
			      pexl_call(ipv6->C, ipv6->ref_arr[4]), 
			      pexl_seq_f(ipv6->C, 
                                        pexl_seq_f(ipv6->C, 
						  pexl_call(ipv6->C, ipv6->ref_arr[1]), 
						  pexl_call(ipv6->C, ipv6->ref_arr[2])),
					pexl_call(ipv6->C, ipv6->ref_arr[3])));

    /* ipv6_component "::" ipv6_component ipv6_rest{0,4} */
    ipv6_choice_2 = pexl_seq_f(ipv6->C,
			      pexl_seq_f(ipv6->C, 
                                        pexl_call(ipv6->C, ipv6->ref_arr[4]),
                                        pexl_match_string(ipv6->C, "::")),
			      pexl_seq_f(ipv6->C, 
                                        pexl_call(ipv6->C, ipv6->ref_arr[4]),
                                        pexl_repeat_f(ipv6->C,
						      pexl_call(ipv6->C, ipv6->ref_arr[3]),
						      0, 4)));
                                        
    /*  ipv6_component ipv6_rest{1} "::" ipv6_component ipv6_rest{0,3} } */
    ipv6_choice_3 = pexl_seq_f(ipv6->C,    
			      pexl_seq_f(ipv6->C,
                                        pexl_seq_f(ipv6->C, 
						  pexl_call(ipv6->C, ipv6->ref_arr[4]),
						  pexl_call(ipv6->C, ipv6->ref_arr[3])),
                                        pexl_seq_f(ipv6->C,
						  pexl_match_string(ipv6->C, "::"),
						  pexl_call(ipv6->C, ipv6->ref_arr[4]))),
			      pexl_repeat_f(ipv6->C,
					    pexl_call(ipv6->C, ipv6->ref_arr[3]),
					    0, 3));

    /*  { ipv6_component ipv6_rest{2} "::" ipv6_component ipv6_rest{0,2} } } */
    ipv6_choice_4 = pexl_seq_f(ipv6->C,    
			      pexl_seq_f(ipv6->C,
                                        pexl_seq_f(ipv6->C, 
						  pexl_call(ipv6->C, ipv6->ref_arr[4]),
						  pexl_call(ipv6->C, ipv6->ref_arr[2])),
                                        pexl_seq_f(ipv6->C,
						  pexl_match_string(ipv6->C, "::"),
						  pexl_call(ipv6->C, ipv6->ref_arr[4]))),
			      pexl_repeat_f(ipv6->C,
					    pexl_call(ipv6->C, ipv6->ref_arr[3]),
					    0, 2));
    
    /* { ipv6_component ipv6_rest{3} "::" ipv6_component ipv6_rest{0,1} } */
    ipv6_choice_5 = pexl_seq_f(ipv6->C,    
			      pexl_seq_f(ipv6->C,
                                        pexl_seq_f(ipv6->C, 
						  pexl_call(ipv6->C, ipv6->ref_arr[4]),
						  pexl_seq_f(ipv6->C,
							    pexl_call(ipv6->C, ipv6->ref_arr[2]),
							    pexl_call(ipv6->C, ipv6->ref_arr[3]))),
                                        pexl_seq_f(ipv6->C,
						  pexl_match_string(ipv6->C, "::"),
						  pexl_call(ipv6->C, ipv6->ref_arr[4]))),
			      pexl_repeat_f(ipv6->C,
					    pexl_call(ipv6->C, ipv6->ref_arr[3]),
					    0, 1));

    /* { ipv6_component ipv6_rest{4} "::" ipv6_component } / */
    ipv6_choice_6 = pexl_seq_f(ipv6->C,
			      pexl_seq_f(ipv6->C, 
                                        pexl_call(ipv6->C, ipv6->ref_arr[4]),
                                        pexl_call(ipv6->C, ipv6->ref_arr[1])),
			      pexl_seq_f(ipv6->C, 
                                        pexl_match_string(ipv6->C, "::"),
                                        pexl_call(ipv6->C, ipv6->ref_arr[4])));

    /* { "::" ipv6_component ipv6_rest{0,5} } /*/
    ipv6_choice_7 = pexl_seq_f(ipv6->C,
			      pexl_seq_f(ipv6->C, 
                                        pexl_match_string(ipv6->C, "::"),
                                        pexl_call(ipv6->C, ipv6->ref_arr[4])),
			      pexl_repeat_f(ipv6->C,
					    pexl_call(ipv6->C, ipv6->ref_arr[3]),
					    0, 5));

    /* ipv6_component ipv6_rest{7} */
    ipv6_choice_8 = pexl_seq_f(ipv6->C,
			      pexl_call(ipv6->C, ipv6->ref_arr[4]),  
			      pexl_seq_f(ipv6->C, 
					pexl_call(ipv6->C, ipv6->ref_arr[1]), 
					pexl_seq_f(ipv6->C, 
						  pexl_call(ipv6->C, ipv6->ref_arr[2]), 
						  pexl_call(ipv6->C, ipv6->ref_arr[3]))));
    
    /* Final pexl_Expr *for ip_address_strict */
    
    ipv6->exp_arr[0] =  pexl_choice_f(ipv6->C, pexl_match_string(ipv6->C, "::"), 
                                pexl_choice_f(ipv6->C, 
                                    pexl_choice_f(ipv6->C,
                                        pexl_choice_f(ipv6->C, ipv6_choice_1, ipv6_choice_2), 
                                        pexl_choice_f(ipv6->C, 
                                            pexl_choice_f(ipv6->C, ipv6_choice_3, ipv6_choice_4), 
                                            pexl_choice_f(ipv6->C, ipv6_choice_5, ipv6_choice_6))),
                                    pexl_choice_f(ipv6->C, ipv6_choice_7, ipv6_choice_8)));
    

    /* ipv6->exp_arr[0] =  ipv6_choice_8; */

    ipv4_ref = ipv4->ref_arr[0]; /* refer to "ipv4" pattern */

    /* Create pexl_Expr *s for using them as callee's in the final expression */
    ipv6->exp_arr[4] = expr_ipv6_component(ipv6);
    ipv6->exp_arr[3] = expr_ipv6_rest(ipv6, ipv4_ref);
    ipv6->exp_arr[2] = expr_ipv6_rest_2(ipv6, ipv4_ref);
    ipv6->exp_arr[1] = expr_ipv6_rest_4(ipv6, ipv4_ref);

    i = ipv6->num_expr-1;
    for (; i >= 0; i--){
      TEST_ASSERT(pexl_rebind(ipv6->C, ipv6->ref_arr[i], ipv6->exp_arr[i]) == OK);
    }

    /* Free the parts of ipv4 that are now part of ipv6 */
    for (size_t ii = 0; ii < ipv4->num_expr; ii++) pexl_free_Expr(ipv4->exp_arr[ii]);
    free(ipv4->exp_arr);
    free(ipv4->ref_arr);
    free(ipv4);

    return ipv6;
}

// Used in inlineoptimizertest
__attribute__((unused)) static pattern* create_ipaddr_star(void){
    pexl_Ref ip_any_ref;
    pattern *ip_any = create_ipaddr();
    pattern *ip_star = (pattern*)malloc(sizeof(pattern));

    if (ip_star == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }

    ip_any_ref = ip_any->ref_arr[0]; /* refer to "ip" pattern == ipv4/ipv6 */

    ip_star->C = ip_any->C;
    ip_star->num_expr = 1;

    /* Allocate Space for pexl_Expr *s & pexl_References */
    ip_star->exp_arr = (pexl_Expr **)malloc(ip_any->num_expr*sizeof(pexl_Expr *));
    ip_star->ref_arr = (pexl_Ref *)malloc(ip_any->num_expr*sizeof(pexl_Ref));


    ip_star->exp_arr[0] = pexl_capture_f(ip_star->C,
					  "IP_STAR",
					  pexl_repeat_f(ip_star->C,
							pexl_call(ip_star->C, ip_any_ref),
							0, 0));
    
    /* pexl_References to the pexl_Expr *s */
    ip_star->ref_arr[0] = pexl_bind(ip_star->C, NULL, "ip_star");

    TEST_ASSERT(pexl_rebind(ip_star->C, ip_star->ref_arr[0], ip_star->exp_arr[0]) == OK);

    for (size_t i = 0; i < ip_any->num_expr; i++)
      pexl_free_Expr(ip_any->exp_arr[i]);
    free(ip_any->exp_arr);
    free(ip_any->ref_arr);
    free(ip_any);

    return ip_star;
}
