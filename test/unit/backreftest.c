/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  backreftest.c                                                            */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "libpexl.h"

#include "../test.h"

#define YES 1
#define NO 0

#define PRINTING NO


static int vmtest (const char *patname,
		   pexl_Binary *pkg, 
		   const char *input, size_t inputlen,
		   pexl_Match *match,
		   bool matched, ssize_t end) {
  int stat;
  printf("Running vm on pattern '%s' and input: ", patname);
  if (inputlen > 70) 
    printf("\"%.*s\"...\n", 70, input);
  else
    printf("\"%.*s\"\n", (int) inputlen, input);
  stat = pexl_run(pkg, NULL, patname, input, inputlen, 0, 0, NULL, match);
  if (stat != 0) {
    printf("vmtest: vm returned status code %d\n", stat);
    return false;
  }
  if (pexl_Match_failed(match) == matched) {
    printf("vmtest: expected %s\n", matched ? "succcess but match failed" : "fail but match succeeded");
    return false;
  } else {
    printf("OK ----- VM %s, as expected\n\n", matched ? "matched" : "did not match");
  }
  if (matched && !pexl_Match_failed(match)) {
    if (pexl_Match_end(match) != end) {
      printf("vmtest: expected match end of %ld, got %" pexl_Position_FMT "\n",
	     end, pexl_Match_end(match));
      return false;
    }
  }
  return true;
}

int main(int argc, char **argv) {

  pexl_Match *match;
  
  pexl_Context *C;
  pexl_Optims *optims;
  pexl_Binary *pkg;
  pexl_Error err;
  
  pexl_Expr *tagname, *matching_tagname,
            *starttag, *endtag, *starttag_xml_endtag,
            *xml, *xml_fullmatch;
  pexl_Ref xml_ref, xml_fullmatch_ref;

  const char *input;
  size_t len;

  TEST_START(argc, argv);

  TEST_SECTION("Building an XML-like tag matching pattern");

  C = pexl_new_Context();
  TEST_ASSERT_NOT_NULL(C);

  //  printf("\nTEMP: without optims!\n\n"); optims = NULL;
  optims = pexl_default_Optims();
  TEST_ASSERT(optims);
  
  /* See notes/xml-lite.rpl */
  
  tagname = pexl_capture_f(C,
			   "tagname",
			   pexl_repeat_f(C, 
					 pexl_match_range(C, 'a', 'z'),
					 1, 0));
  TEST_ASSERT(tagname);
  matching_tagname = pexl_backref(C, "tagname");
  TEST_ASSERT(matching_tagname);
  starttag = pexl_seq_f(C,
			 pexl_match_string(C, "<"),
			 pexl_seq_f(C,
				    tagname,
				    pexl_match_string(C, ">")));
  TEST_ASSERT(starttag);
  endtag = pexl_seq_f(C,
		       pexl_match_string(C, "</"),
		       pexl_seq_f(C,
				  matching_tagname,
				  pexl_match_string(C, ">")));
  TEST_ASSERT(endtag);
  xml_ref = pexl_bind(C, NULL, "xml");

  // Intermediate expression for convenience
  starttag_xml_endtag = pexl_seq_f(C,
				   starttag,
				   pexl_seq_f(C,
					      pexl_repeat_f(C,
							    pexl_call(C, xml_ref),
							    0, 1),
					      endtag));
  // Do not free starttag_xml_endtag because we will reuse it in a later test
  xml = pexl_repeat_f(C,
		      pexl_capture(C,
				   "xml",
				   starttag_xml_endtag),
		      1, 0);
  TEST_ASSERT(xml);
  TEST_ASSERT(pexl_rebind(C, xml_ref, xml) >= 0);
  
  if (PRINTING) {
    printf("xml expression is: \n"); 
    pexl_print_Expr(xml, 0, C); 
  }

  // Match 'xml' then end-of-input ($ in RPL) so that our tests can be
  // written to either match the entire input string or fail.
  xml_fullmatch = pexl_seq_f(C, pexl_call(C, xml_ref), pexl_match_any(C, -1));
  TEST_ASSERT(xml_fullmatch);
  xml_fullmatch_ref = pexl_bind(C, xml_fullmatch, "xml_fullmatch");
  TEST_ASSERT(!pexl_Ref_invalid(xml_fullmatch_ref));

  if (PRINTING) {
    printf("xml_fullmatch expression is: \n"); 
    pexl_print_Expr(xml_fullmatch, 0, C); 
  }

  pkg = pexl_compile(C, xml_fullmatch_ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  /* ------------------------------------------------------------------ */
  TEST_SECTION("Running the pattern");

  match = pexl_new_Match(PEXL_TREE_ENCODER);

  if (PRINTING)
    pexl_print_Binary(pkg);

  // Writing RUNTEST as a macro instead of a function allows
  // TEST_ASSERT to print the actual line of the file where a failure
  // occurs, instead of always printing a line number inside a
  // function.

#define RUNTEST(string, accepts) do {					\
    input = (string);							\
    len = strlen(string);						\
    TEST_ASSERT(vmtest("xml_fullmatch", pkg,				\
		       input, len, match,				\
		       accepts, len));					\
  } while(0);

  // Simple tag pair matches
  RUNTEST("<foo></foo>", true);

  // Sequences of tag pairs match
  RUNTEST("<foo></foo><bar></bar>", true);
  RUNTEST("<foo></foo><foo></foo>", true);

  // Nested tag pairs match
  RUNTEST("<foo><bar></bar></foo>", true);
  RUNTEST("<foo><foo></foo></foo>", true);
  RUNTEST("<foo><bar><baz><qux></qux></baz></bar></foo>", true);

  // Nested pairs with sequences of pairs match
  RUNTEST("<foo><bar></bar><bat></bat><qux></qux></foo>", true);
  RUNTEST("<foo><bar><bat></bat><qux></qux></bar></foo>", true);

  // Empty string does not match
  RUNTEST("", false);

  // End tag not same as start tag, so fails to match
  RUNTEST("<foo></bar>", false);
  RUNTEST("<foo></foo><bar></bar2>", false);
  RUNTEST("<foo></foo><bar2></bar>", false);
  RUNTEST("<foo><bar></bar></bar>", false);

  // Improper nesting fails to match
  RUNTEST("<foo><foo></foo>", false);
  RUNTEST("<foo><bar></foo></bar>", false);
  RUNTEST("<foo></foo></foo><bar>", false);

  pexl_free_Binary(pkg);

  /* ------------------------------------------------------------------ */
  TEST_SECTION("Compiling a variant of the same pattern, without a needed capture");

  pexl_free_Expr(xml);
  // NOT capturing "xml" in this version of the pattern
  xml = pexl_repeat_f(C, starttag_xml_endtag, 1, 0);
  TEST_ASSERT(xml);
  TEST_ASSERT(pexl_rebind(C, xml_ref, xml) >= 0);
  
  if (PRINTING) {
    printf("xml expression is: \n"); 
    pexl_print_Expr(xml, 0, C); 
  }

  // Match 'xml' then end-of-input ($ in RPL) so that our tests can be
  // written to either match the entire input string or fail.
  pexl_free_Expr(xml_fullmatch);
  xml_fullmatch = pexl_seq_f(C, pexl_call(C, xml_ref), pexl_match_any(C, -1));
  TEST_ASSERT(xml_fullmatch);
  TEST_ASSERT(pexl_rebind(C, xml_fullmatch_ref, xml_fullmatch) >= 0);

  if (PRINTING) {
    printf("xml_fullmatch expression is: \n"); 
    pexl_print_Expr(xml_fullmatch, 0, C); 
  }

  pkg = pexl_compile(C, xml_fullmatch_ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == PEXL_OK);

  if (PRINTING)
    pexl_print_Binary(pkg);

  TEST_SECTION("Running the variant, without a needed capture");

  // Simple tag pair matches
  RUNTEST("<foo></foo>", true);

  // Sequences of tag pairs match
  RUNTEST("<foo></foo><bar></bar>", true);
  RUNTEST("<foo></foo><foo></foo>", true);

  // Nested tag pairs match
  printf("The next test fails to match, though we would expect it to match...\n");
  RUNTEST("<foo><bar></bar></foo>", false); // WE WANTED A MATCH!
  RUNTEST("<foo><foo></foo></foo>", true);
  printf("The next test fails to match, though we would expect it to match...\n");
  RUNTEST("<foo><bar><baz><qux></qux></baz></bar></foo>", false); // WE WANTED A MATCH!

  // Nested pairs with sequences of pairs match
  printf("The next test fails to match, though we would expect it to match...\n");
  RUNTEST("<foo><bar></bar><bat></bat><qux></qux></foo>", false); // WE WANTED A MATCH!
  printf("The next test fails to match, though we would expect it to match...\n");
  RUNTEST("<foo><bar><bat></bat><qux></qux></bar></foo>", false); // WE WANTED A MATCH!

  // Empty string does not match
  RUNTEST("", false);

  // End tag not same as start tag, so fails to match
  RUNTEST("<foo></bar>", false);
  RUNTEST("<foo></foo><bar></bar2>", false);
  RUNTEST("<foo></foo><bar2></bar>", false);
  printf("The next test matches, though we would expect it to fail...\n");
  RUNTEST("<foo><bar></bar></bar>", true); // WE WANTED NO MATCH!

  // Improper nesting fails to match
  RUNTEST("<foo><foo></foo>", false);
  RUNTEST("<foo><bar></foo></bar>", false);
  RUNTEST("<foo></foo></foo><bar>", false);

  pexl_free_Binary(pkg);

  pexl_free_Match(match);
  pexl_free_Context(C);
  pexl_free_Optims(optims);


  TEST_END();

  return 0;
}

