/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  simdtest.c                                                               */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2022.                                      */
/*  AUTHOR: Jamie A. Jennings                                                */

#include "preamble.h"

//#include "libpexl.h"
#include <time.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "find.h"		// We use internal PEXL functions here

/* Some tests to run:
      ./simdtest "A" ~/Projects/comparisons/data/kjv10.txt
      ./simdtest "Z" ~/Projects/comparisons/data/kjv10.txt
      ./simdtest '!@' ~/Projects/comparisons/data/kjv10.txt
      ./simdtest '!@J' ~/Projects/comparisons/data/kjv10.txt
      ./simdtest 'aeiou' ~/Projects/comparisons/data/kjv10.txt 
      ./simdtest "ETAS" ~/Projects/comparisons/data/kjv10.txt
      ./simdtest "ETASRetasr" ~/Projects/comparisons/data/kjv10.txt
      ./simdtest "VKJXQZvkjxqz" ~/Projects/comparisons/data/kjv10.txt
      ./simdtest "YPBVKJXQZypbvkjxqz" ~/Projects/comparisons/data/kjv10.txt

      ./simdtest '!@J' /usr/share/dict/words
      ./simdtest "ETASRetasr" /usr/share/dict/words
      ./simdtest "YPBVKJXQZypbvkjxqz" /usr/share/dict/words
      
    Example results:

      ~/Projects/libpexl/test/bench$ ./simdtest ETASetas ~/Projects/comparisons/data/kjv10.txt
      Bytes in set: ETASetas
       [(41)(45)(53-54)(61)(65)(73-74)]

      Using find_charset() and find_simd8()
      Reading input file: /Users/jennings/Projects/comparisons/data/kjv10.txt
      Charset has 2-8 chars
      Non-simd processing of file 1 times took     35.0 ms (    35.0 per iteration)
      FYI: /usr/share/dict/words has 4333040 bytes
      FYI: match count is 1190784
      SIMD processing of file 1 times took     24.4 ms (    24.4 per iteration)
      FYI: /usr/share/dict/words has 4333040 bytes
      FYI: match count is 1190784

      ** SIMD SPEEDUP:   1.4x **

      ~/Projects/libpexl/test/bench$
*/

/*
  Caveats when comparing this program to egrep:
  (1)
    - This simd program is finding EVERY occurrence of the bytes in the 
      given set in the input file, e.g. the 4.1MB kjv10.txt file.
    - The egrep -c option is printing number of matching lines, so
      egrep stops matching on a line after the first match, in other
      words it processes less of each line.
  (2)
    - The GNU egrep 3.3 most likely does a better job with i/o than
      this SIMD test program.  We read the entire file into memory,
      and when timing is done externally, e.g. using hyperfine or the
      unix 'time' utility, this technique may actually be slow.
      Pretty sure that egrep is reading a chunk at a time (and then
      processes line by line within each chunk).
*/


/* Caller must free the return value */
static const char *readfile (const char *filename, size_t *filesize) {
  if (strncmp(filename, "-", 2)==0) filename = "/dev/stdin";
  FILE *f = fopen(filename, "r");
  if (!f) return NULL;
  int stat = fseek(f, 0, SEEK_END);
  if (stat) {
    fprintf(stderr, "Failed fopen\n");
    fclose(f);
    return NULL;
  }
  long filelen = ftell(f);
  if (filelen < 0) {
    fprintf(stderr, "Invalid file size %ld\n", filelen);
    fclose(f);
    return NULL;
  }
  *filesize = (size_t) filelen;
  stat = fseek(f, 0, 0);
  if (stat) {
    fprintf(stderr, "Failed fseek\n");
    fclose(f);
    return NULL;
  }
  char *contents = (char *) malloc(*filesize+1); 
  if (!contents) {
    fprintf(stderr, "malloc failed\n");
    return NULL;
  }
  size_t n = fread(contents, (size_t) *filesize, 1, f);
  if (n != 1) {			/* one item */
    fprintf(stderr, "fread failed\n");
    free(contents);
    contents = NULL;
  }
  contents[*filesize] = '\0';
  fclose(f);
  return contents;
}

static int
prep_simd_charset (Charset *cs, Charset *simd_cs, uint8_t *simd_char1) {
  SetMask_t simd_set;
  int simd_size = charset_to_simd_mask(cs, &simd_set, simd_char1); 
  if (simd_size < 0)
    fprintf(stderr, "error %d from charset_to_simd_mask\n", simd_size);
  else if (simd_size == 0)
    fprintf(stderr, "error: empty charset");
  memcpy(&(simd_cs->cs), &simd_set, 32);
  return simd_size;
}

/*
  Compare searching the input using find_char() or find_charset(),
  which are the NON SIMD ROUTINES, versus find_simd().
*/
static void AB_test_find (const char *input, size_t len,
			  Charset *cs,
			  int reps) {
  assert(input);
  assert(cs);
  char char1;
  CharSetType cs_type = charsettype(cs, &char1);
  switch (cs_type) {
  case CharSetEmpty: printf("Charset is empty!\n"); break;
  case CharSetFull: printf("Charset is full!\n"); break;
  case CharSet1: printf("Charset has one char: 0x%02X\n", (unsigned) char1); break;
  case CharSet8: printf("Charset has 2-8 chars\n"); break;
  case CharSet: printf("Charset has 2-255 chars\n"); break;
  default:
    printf("Charset type unknown!\n");
    assert(0);			/* should not happen */
  }
  int count = 0;
  const char *ptr;
  clock_t t0 = clock();
  int i;
  for (i = 0; i < reps; i++) {
    ptr = input;
    count = 0;
    while (1) {
      switch (cs_type) {
      case CharSet1:
	ptr = find_char(char1, ptr, input + len); break;
      case CharSet8: case CharSet:
	ptr = find_charset(cs, ptr, input + len); break;
      default:
	assert(0);
      }	
      if (ptr == input+len) break;
      count++;
      ptr++;
    } /* while */
  } /* for reps */
  clock_t t1 = clock();
  long double duration = (long double) (t1 - t0) / CLOCKS_PER_SEC * 1000;
  printf("Non-simd processing of file %d times took %8.1Lf ms (%8.1Lf per iteration)\n",
	 reps,
	 duration,
	 duration / (long double) reps);
  printf("FYI: input has %zu bytes\n", len);
  printf("FYI: match count is %d\n", count);
  /* The line count on MACOS 11.6 is 235886. Other platforms may vary. */

  if (!pexl_simd_available()) {
    printf("SKIPPING SIMD TESTS (SIMD not available)\n");
    return;
  } 
  uint8_t simd_char1 = '\0';
  int non_simd_count = count;
  long double non_simd_duration = duration;
  Charset simd_cs;
  int simd_set_size = prep_simd_charset(cs, &simd_cs, &simd_char1); 
  switch (cs_type) {
  case CharSet1: assert(simd_set_size == 1); break;
  case CharSet8: assert((simd_set_size >= 2) && (simd_set_size <= 8)); break;
  case CharSetEmpty: assert(simd_set_size == 0); break;
  case CharSetFull: assert(simd_set_size == 256); break;
  case CharSet: assert((simd_set_size > 1) && (simd_set_size < 256)); break;
  default:
    printf("Charset type unknown!\n");
    assert(0);			/* should not happen */
  }
  t0 = clock();
  for (i = 0; i < reps; i++) {
    ptr = input;
    count = 0;
    while (1) { 
      switch (cs_type) {
      case CharSet1:
	ptr = find_simd1(simd_char1, ptr, input + len); break;
      case CharSet8:
	ptr = find_simd8(&simd_cs, cs, ptr, input + len); break;
      case CharSet:
	ptr = find_simd(&simd_cs, cs, ptr, input + len); break;
      default:
	assert(0);
      }
      if (ptr == input+len) break;
      count++;
      ptr++; 
    } /* while */
  } /* for reps */
  t1 = clock();
  duration = (long double) (t1 - t0) / CLOCKS_PER_SEC * 1000;
  printf("SIMD processing of file %d times took %8.1Lf ms (%8.1Lf per iteration)\n",
	 reps,
	 duration,
	 duration / (long double) reps);
  printf("FYI: input has %zu bytes\n", len);
  printf("FYI: match count is %d\n", count);
  printf("FYI: bytes per match is %0.1Lf\n", ((long double) len) / ((long double) count));

  // Cheap test to ensure same results between SIMD and regular:
  assert(count == non_simd_count);

#ifdef NODEBUG
  printf("\n** SIMD SPEEDUP: %5.1Lfx **\n\n", non_simd_duration / duration);
#else
  printf("The purpose of this unit test is to call the SIMD and regular\n"
	 "implementations of various 'find' functions directly, to ensure\n"
	 "they produce the same results.\n"
	 "This program was compiled with debugging options enabled,\n"
	 "including assertions and likely also address santizing.  These\n"
	 "slow down our SIMD implementation considerably, so it is\n"
	 "probable that the SIMD functions will perform worse than the\n"
	 "regular ones.\n"
	 "To test relative speed of the SIMD functions in isolation of\n"
	 "the vm, compile this program in the production mode, with no\n"
	 "debugging or sanitizing, and with -O2 or -O3.\n");
  printf("SIMD speedup: %5.1Lfx \n\n", non_simd_duration / duration);
#endif  
  return;
}

static void print_charset(const uint32_t *st)
{
  int i, first;
  printf(" [");
  for (i = 0; i <= UCHAR_MAX; i++)
  {
    first = i;
    while (i <= UCHAR_MAX && testchar(st, i))
      i++;
    if (i - 1 == first) /* unary range? */
      printf("(%02x)", first);
    else if (i - 1 > first) /* non-empty range? */
      printf("(%02x-%02x)", first, i - 1);
  }
  printf("]");
}

int main (int argc, char **argv) {
  const char *set;
  const char *filename;
  if (argc < 2) {
    printf("Usage: %s <set_contents> <filename> [n]\n", argv[0]);
    printf("If a third argument is given, the set will also include NUL.\n");
    printf("When filename is '-', standard input is read.\n");
    printf("\nNo command-line arguments given, so defaulting to:\n");
    set = "0123456789";
    filename = "../system/kjv10.txt";
    printf("%s '%s' %s\n", argv[0], set, filename);
  } else {
    set = strndup(argv[1], 256);
    filename = strndup(argv[2], 4096);
  }
  Charset cs;
  charset_fill_zero(cs.cs);	/* erase all chars */
  printf("Bytes in set: %s", set);
  size_t len = strnlen(set, 256);
  if (argc == 4) {
    printf(" and NUL");
    len++;
    setchar(cs.cs, '\0');	/* add NUL char to set */
  }
  puts("");

  SetMask_t simdset;
  uint8_t one_char;
  int i = 0;
  while ((one_char = set[i++]))
    setchar(cs.cs, one_char);	/* add char to set */
  print_charset(cs.cs);
  puts("\n");
    
  int set_size = charset_to_simd_mask(&cs, &simdset, &one_char);
  
  switch (set_size) {
  case 1:
    assert(len == 1);
    assert(one_char == set[0]);
    printf("Using find_char()\n");
    break;
  case 8:
    assert((len >= 2) && (len <= 8));
    printf("Using find_charset() and find_simd8()\n");
    break;
  case 255:
    assert((len >= 9) && (len <= 255));
    printf("Using find_charset() and find_simd()\n");
    break;
  default:
    printf("Charset size of %d unsupported in this test program\n", set_size);
  }
      
  printf("Reading input file: %s\n", filename);
  size_t inputlen;
  const char *input = readfile(filename, &inputlen);

  AB_test_find(input, inputlen, &cs, 1);

  exit(0);
}


