/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  inlineoptimizertest.c  TESTING inlining optimizer                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Vivek Reddy                                                     */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"

#define PRINTING NO
#include "ip_patterns.h"	// Uses PRINTING setting


static int assert_match(pexl_Match* y, pexl_Match* y_pred, const char* input){
    int crct = OK;
    if ((y->end != -1) != (y_pred->end != -1)) {
      printf("pexl_Error: pexl_Match Failed for Input: %s \n\t No inline: %d, inline: %d\n",
	     input, (y->end != -1), (y_pred->end != -1));
      crct = !OK;	
    }
    if (y->end != y_pred->end){
      printf("pexl_Error: End position Failed for Input: %s \n\t No inline: %lu, inline: %lu\n",
	     input, y->end, y_pred->end);
      crct = !OK;	
    }
    return crct;
}

static pexl_Index lookup_entrypoint (const char *patname, pattern *pt) {
  SymbolTableEntry *entry;
  assert(pt->pkg);
  /* Find entrypoint for 'patname' */
  entry = symboltable_search(pt->pkg->symtab, symbol_ns_id, patname, NULL);
  if (!entry) {
    print_symboltable(pt->pkg->symtab);
    TEST_FAIL("Search of symbol table for '%s' FAILED\n", patname);
  }
  assert(entry->type == symbol_type_pattern);
  return entry->value;
}

#define ENCODER PEXL_TREE_ENCODER

/* Return OK if we got the same result with and without inlining enabled */
static int check_inline_opt(const char *input,
			    const char *patname,
			    pattern* (*generate_pt)(void)){
    
    vm_match *vm_m, *vm_m_il;
    pattern *pt1, *pt2;
    int stat, stat1, ret;
    pexl_Optims *optims = NULL;
    optims = pexl_addopt_tro(optims);
    optims = pexl_addopt_peephole(optims);
    optims = pexl_addopt_simd(optims, 1);
    
    printf("\nTest case: %s\n", input);

    /* Without Inlining */
    TEST_ASSERT(!optimlist_contains(optims, PEXL_OPT_INLINE));
    pt1 = generate_pt();
    compile_generic_pattern(pt1, optims);

    /* Switch to PEXL_DEBUG_ENCODER to get a printout of capture stack */
    vm_m = vm_match_new(pt1, input, strlen(input), NULL, PEXL_TREE_ENCODER);
    stat = vm_start(pt1->pkg, NULL, lookup_entrypoint(patname, pt1),
		    vm_m->input, vm_m->inputlen,
		    0, vm_m->inputlen,
		    0,	 	         // vmflags
		    NULL, vm_m->match);

    /* With Inlining */
    pexl_free_Optims(optims);
    optims = pexl_addopt_tro(NULL);
    optims = pexl_addopt_inline(optims);
    optims = pexl_addopt_peephole(optims);
    optims = pexl_addopt_simd(optims, 1);
    TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_INLINE));
    pt2 = generate_pt();
    compile_generic_pattern(pt2, optims); 

    /* Switch to PEXL_DEBUG_ENCODER to get a printout of capture stack */
    vm_m_il = vm_match_new(pt2, input, strlen(input), NULL, PEXL_TREE_ENCODER);
    stat1 = vm_start(pt2->pkg, NULL, lookup_entrypoint(patname, pt2),
		     vm_m_il->input, vm_m_il->inputlen,
		     0, vm_m_il->inputlen,
		     0,	 	         // vmflags
		     NULL, vm_m_il->match);

    TEST_ASSERT_MSG(stat == stat1,
		    "ERROR: vm returned %d with no inlining, and %d with inlining\n",
		    stat, stat1);

    ret = assert_match(vm_m->match, vm_m_il->match, input);

    // Print everything if 'ret' is non-zero (error).  Otherwise, it's too much.
    if (ret && PRINTING) {
      printf("-------------------- Without inlining: --------------------\n");
      if (vm_m->match) {
	pexl_print_Match_summary(vm_m->match);
	pexl_print_Match_data(vm_m->match);
      } else {
	printf("NO MATCH.\n");
      }
      pexl_print_Binary(pt1->pkg);
      printf("--------------------- With inlining: ----------------------\n");
      if (vm_m->match) {
	pexl_print_Match_summary(vm_m_il->match);
	pexl_print_Match_data(vm_m_il->match);
      } else {
	printf("NO MATCH.\n");
      }
      pexl_print_Binary(pt2->pkg);
    }
      
    vm_match_free(vm_m);
    vm_match_free(vm_m_il);

    generic_pattern_free(pt1);
    generic_pattern_free(pt2);
    
    pexl_free_Optims(optims);
    return ret;
}

#define TOPLEVEL (0)		// root env id

static pattern* generate_verse_pattern (void) {
    int i;
    pattern* pt = (pattern*)malloc(sizeof(pattern));
    if (pt == NULL) {
        fprintf(stderr, "pexl_Error Allocating Memory: %s\n", strerror(errno));
        exit(-1);	
    }
    pt->num_expr = 8;
    pt->C = pexl_new_Context();
    /* Allocate Space for pexl_Expr *s & pexl_References */
    pt->exp_arr = (pexl_Expr **)malloc(pt->num_expr*sizeof(pexl_Expr *));
    pt->ref_arr = (pexl_Ref *)malloc(pt->num_expr*sizeof(pexl_Ref));

    /* This pattern was written by Meles Meles, and is designed to have MANY calls.
       RPL: 
	 num = [0-9]
	 nums = num+
	 colon = ":"
	 chapt_and_verse = {nums colon nums}
	 And = "And"
	 spaces = " "*
	 verse = {chapt_and_verse spaces And}
     */

    pt->ref_arr[7] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "And");
    pt->exp_arr[7] = pexl_match_string(pt->C, "And");

    pt->ref_arr[6] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "num");
    pt->exp_arr[6] = pexl_match_range(pt->C, '0', '9');

    pt->ref_arr[5] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "nums");
    pt->exp_arr[5] = pexl_repeat_f(pt->C,
				   pexl_call(pt->C, pt->ref_arr[6]),
				   1, 0);

    pt->ref_arr[4] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "colon");
    pt->exp_arr[4] = pexl_match_string(pt->C, ":");

    pt->ref_arr[3] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "chapt_and_verse");
    pt->exp_arr[3] = pexl_seq_f(pt->C,
				 pexl_call(pt->C, pt->ref_arr[6]), /* call nums */
				 pexl_seq_f(pt->C,
					     pexl_call(pt->C, pt->ref_arr[4]), /* call colon */
					     pexl_call(pt->C, pt->ref_arr[5]))); /* call nums */

    pt->ref_arr[2] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "space");
    pt->exp_arr[2] = pexl_match_string(pt->C, " ");

    pt->ref_arr[1] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "spaces");
    pt->exp_arr[1] = pexl_repeat_f(pt->C,
				   pexl_call(pt->C, pt->ref_arr[2]),
				   0, 0);

    pt->ref_arr[0] = pexl_bind_in(pt->C, TOPLEVEL, NULL, "verse"); 
    pt->exp_arr[0] = pexl_seq_f(pt->C,
				 pexl_call(pt->C, pt->ref_arr[3]), /* call chapt_and_verse */
				 pexl_seq_f(pt->C,
					     pexl_call(pt->C, pt->ref_arr[1]), /* call spaces */
					     pexl_call(pt->C, pt->ref_arr[7]))); /* call And */

    for (i = pt->num_expr-1; i >= 0; i--) {
      TEST_ASSERT(pt->exp_arr[i]);
      TEST_ASSERT(!pexl_Ref_invalid(pt->ref_arr[i]));
      TEST_ASSERT(!pexl_Ref_notfound(pt->ref_arr[i]));
      TEST_ASSERT(pexl_rebind(pt->C, pt->ref_arr[i], pt->exp_arr[i]) == OK);
    }  
    return pt;
}

int main(int argc, char **argv) {

  TEST_START(argc, argv);

  TEST_SECTION("Patterns without any calls (no opportunity to inline)");
  TEST_ASSERT(check_inline_opt("", "a_star_no_call", &a_star_no_call)== OK);
  TEST_ASSERT(check_inline_opt("a", "a_star_no_call", &a_star_no_call)== OK);
  TEST_ASSERT(check_inline_opt("aaaaaaaa", "a_star_no_call", &a_star_no_call)== OK);

  TEST_SECTION("Patterns with one call to inline");
  TEST_ASSERT(check_inline_opt("", "a_star", &a_star)== OK);
  TEST_ASSERT(check_inline_opt("a", "a_star", &a_star)== OK);
  TEST_ASSERT(check_inline_opt("aaaaaaaa", "a_star", &a_star)== OK);


  TEST_SECTION("Simple recursive patterns");
  TEST_ASSERT(check_inline_opt("aaabbb", "A", &recursive_pattern)== OK);
  TEST_ASSERT(check_inline_opt("aaabbb", "A", &recursive_pattern)== OK);
  TEST_ASSERT(check_inline_opt("aaacbbbb", "A", &recursive_pattern)== OK);
    
  TEST_ASSERT(check_inline_opt("aaabbb", "A", &recursive_pattern2)== OK);
  TEST_ASSERT(check_inline_opt("aaacbbbb", "A", &recursive_pattern2)== OK);
  TEST_ASSERT(check_inline_opt("aaacaabcbcbcbcbc123", "A", &recursive_pattern2)== OK);
  /*
    TEST_ASSERT(check_inline_opt("aaacaabcbcbcbcbc", 6, 6, &recursive_pattern3)== OK);
    TEST_ASSERT(check_inline_opt("aaacaabcbcbcbcbc123", 6, 6, &recursive_pattern3)== OK);
    TEST_ASSERT(check_inline_opt("aaacaabcbcbcbcbc13", 6, 6, &recursive_pattern3)== OK);
  */

  TEST_SECTION("IPV6 pattern");
  TEST_ASSERT(check_inline_opt("2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("2001:db8::ff00:42:8329", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("1080:0:0:0:8:800:200C:4171", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("3ffe:2a00:100:7031::1", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("::192.9.5.5", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("FFFF:129.144.52.38", "ipv6_strict", &create_ipv6)== OK);
  TEST_ASSERT(check_inline_opt("A:AA:ABC:ABCD:B", "ipv6_strict", &create_ipv6)== OK);

  TEST_SECTION("IPV4 pattern");
  TEST_ASSERT(check_inline_opt("255.255.255.255", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("0.0.0.0", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("1.2.234.123", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("255.0.0.255", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255.255.255.255.255", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("1.1.1.256", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("1234.1.2.3", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("111.222.333.", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("111.222.333..444", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("999.999.999.999", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("256.1.1.1", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("1.256.1.1", "ipv4", &create_ipv4)== OK);
  TEST_ASSERT(check_inline_opt("1.1.256.1", "ipv4", &create_ipv4)== OK);

  /* Ipv4-Ipv6 Tests Different Entry Points */
  TEST_SECTION("ipv6 or ipv4 pattern, different entry points but same package");
  TEST_ASSERT(check_inline_opt("255.255.255.255", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("0.0.0.0", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.2.234.123", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.0.0.255", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255.255.255.255.255", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.1.1.256", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1234.1.2.3", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("111.222.333.", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("111.222.333..444", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("999.999.999.999", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("256.1.1.1", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.256.1.1", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.1.256.1", "ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("2001:db8::ff00:42:8329", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1080:0:0:0:8:800:200C:4171", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("3ffe:2a00:100:7031::1", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("::192.9.5.5", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("FFFF:129.144.52.38", "ipv6_strict", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("A:AA:ABC:ABCD:B", "ipv6_strict", &create_ipaddr)== OK);

  /* Ipv4-Ipv6 Tests Same Entry Point */
  TEST_SECTION("IP = ipv6 / ipv4 pattern");
  TEST_ASSERT(check_inline_opt("2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("2001:db8:0:0:0:ff00:42:8329", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("2001:db8::ff00:42:8329", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1080:0:0:0:8:800:200C:4171", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("3ffe:2a00:100:7031::1", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("::192.9.5.5", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("FFFF:129.144.52.38", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("A:AA:ABC:ABCD:B", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("0.0.0.0", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.2.234.123", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.0.0.255", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("255.255.255.255.255.255.255.255", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.1.1.256", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1234.1.2.3", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("111.222.333.", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("111.222.333..444", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("999.999.999.999", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("256.1.1.1", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.256.1.1", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1.1.256.1", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("100.100.100.122", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("0.00.00.000", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("254.256.255.255", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("AB.2.1.AB", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("1001:db8:::::", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("0:0:0:0:8:800:", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("3ffe:2a00:::0:0", "ipv6 / ipv4", &create_ipaddr)== OK);
  TEST_ASSERT(check_inline_opt("ABCD:abcd:ABcd:abCD:00:00:00", "ipv6 / ipv4", &create_ipaddr)== OK);

  /* The tests below make use of the negative lookahead for an ipv4 pattern in ipv6_rest */
  TEST_SECTION("Checking specifically that the negative lookahead in ipv6 works");
  TEST_ASSERT(check_inline_opt("::192.9.5.5", "ipv6_strict", &create_ipaddr) == OK);
  TEST_ASSERT(check_inline_opt("::FFFF:129.144.52.38", "ipv6_strict", &create_ipaddr) == OK);

  /* pexl_Match multiple ip addresses in a row */
  TEST_SECTION("Checking IP*");
  TEST_ASSERT(check_inline_opt("255.0.0.255192.0.0.1", "ip_star", &create_ipaddr_star)== OK);

  /*
    This test was added on Tuesday, July 11, 2023 because the pattern
    contains several patterns that are 1 word long (a 1-word
    instruction followed by 'return').  This is an edge case for the
    inlining optimizer because we are asking it to inline a 1-word
    instruction.  That 1-word instruction replaces a 'call', which is
    a 2-word instruction.  As a result, the instruction vector gets
    SHORTER after inlining such a short pattern.
  */
  TEST_SECTION("Checking 'verse' pattern with 1-instruction patterns to be inlined");
  TEST_ASSERT(check_inline_opt("10:2 And", "verse", &generate_verse_pattern) == OK);
  TEST_ASSERT(check_inline_opt("10:2 and", "verse", &generate_verse_pattern) == OK);


  TEST_END();
}
