/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  asttest.c  Testing the AST structs and sexp reader/writer                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Jack Deucher                                 */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
#include "ast-print.h"
#include "sexp-reader.h"
#include "sexp-writer.h"
#include "sexp-to-ast.h"
#include "ast-xform.h"
#include "ast-typecheck.h"

#include "../test.h"

#define PRINTING YES

#define newline() do { puts(""); } while(0)

static bool all_whitespace(const char *ptr) {
  while (*ptr && whitespacep(ptr)) ptr++;
  return !*ptr;
}

static Sexp *sexp_from_literal(const string *s){
  if (!s) return read_sexp(NULL);
  string *tmp = strdup(s);
  const string *ptr = tmp;
  Sexp *sexp = read_sexp(&ptr);
  if (sexp_errorp(sexp)) {
    free(tmp);
    return sexp;
  }
  // Else no error in reading the sexp.  Now check that we read the
  // entire string as one s-expression, returning NULL if we did not.
  // If the sexp ends before the end of the input string and there is
  // only whitespace remaining, that's ok for our tests.
  if (!all_whitespace(ptr)) {
    if (PRINTING)
      printf("Failed to parse entire string as one s-expression: '%s'\n", s);
    free(tmp);
    free_sexp(sexp);
    return NULL;
  }
  // Successfully read the entire string as one s-expression:
  free(tmp);
  return sexp;
}

static void print_ast_as_sexpr(ast_expression *exp) {
  Sexp *s = ast_to_sexp(exp);
  if (!s) {
    printf("NULL S-expression (in print_ast_as_sexpr)\n");
    return;
  }
  print_sexp(s);
  free_sexp(s);
}

static bool non_nullp(Sexp *s) {
  return s && !sexp_nullp(s);
}

int main(int argc, char **argv){

  Sexp *s;
  string *tmp1, *tmp2;
  String *str;
  ast_expression *id;
  ast_expression *n;
  ast_expression *b;
  ast_expression *error_exp;
  ast_expression *exp, *exp1, *exp2;

  TEST_START(argc, argv);

  printf("Size of ast_expression is %zu bytes\n", sizeof(ast_expression));

  // -----------------------------------------------------------------------------
  TEST_SECTION("Identifiers");
  // -----------------------------------------------------------------------------

  // Error checking
  TEST_EXPECT_WARNING;
  id = make_identifier_from(NULL);
  TEST_ASSERT_NULL(id);
  id = make_identifier_from("hello");
  TEST_ASSERT_NOT_NULL(id);
  TEST_ASSERT(String_string_cmp(id->name, "hello", 5) == 0);
  if (PRINTING) ast_print_tree(id);
  free_expression(id);

  id = make_identifier_from("");
  TEST_ASSERT_NOT_NULL(id);
  TEST_ASSERT(String_string_cmp(id->name, "", 0) == 0);
  if (PRINTING) ast_print_tree(id);
  free_expression(id);

  str = String_from_literal("");
  id = make_identifier(str);
  TEST_ASSERT_NOT_NULL(id);
  TEST_ASSERT(String_cmp(id->name, str) == 0);
  if (PRINTING) ast_print_tree(id);
  free_expression(id);
  
  // This low-level interface does NOT check the contents of the id
  id = make_identifier_from(".");
  TEST_ASSERT_NOT_NULL(id);
  TEST_ASSERT(String_string_cmp(id->name, ".", 1) == 0);
  if (PRINTING) ast_print_tree(id);
  free_expression(id);

  id = make_identifier_from("...");
  TEST_ASSERT_NOT_NULL(id);
  TEST_ASSERT(String_string_cmp(id->name, "...", 3) == 0);
  if (PRINTING) ast_print_tree(id);
  free_expression(id);

  str = String_from_literal("foobarbazqux");
  id = make_identifier(str);
  TEST_ASSERT_NOT_NULL(id);
  TEST_ASSERT(String_cmp(id->name, str) == 0);
  if (PRINTING) ast_print_tree(id);
  free_expression(id);
  
  // TODO: Convert to S-expression, ensure it is correct, print it

  // -----------------------------------------------------------------------------
  TEST_SECTION("Numbers");
  // -----------------------------------------------------------------------------

  n = make_number(0);
  TEST_ASSERT(n->n == 0);
  if (PRINTING) ast_print_tree(n); 
  free_expression(n);
  n = make_number(1000);
  TEST_ASSERT(n->n == 1000);
  if (PRINTING) ast_print_tree(n);
  free_expression(n);
  n = make_number(-1);
  TEST_ASSERT(n->n == -1);
  if (PRINTING) ast_print_tree(n);
  free_expression(n);

  n = make_number(pexl_Index_MAX);
  TEST_ASSERT(n->n == pexl_Index_MAX);
  if (PRINTING) ast_print_tree(n);
  free_expression(n);
  n = make_number(pexl_Index_MIN);
  TEST_ASSERT(n->n == pexl_Index_MIN);
  if (PRINTING) ast_print_tree(n);
  free_expression(n);

  n = make_number(pexl_Number_MAX);
  TEST_ASSERT(n->n == pexl_Number_MAX);
  if (PRINTING) ast_print_tree(n);
  free_expression(n);
  n = make_number(pexl_Number_MIN);
  TEST_ASSERT(n->n == pexl_Number_MIN);
  if (PRINTING) ast_print_tree(n);
  free_expression(n);

  // TODO: Convert to S-expression, ensure it is correct, print it

  // -----------------------------------------------------------------------------
  TEST_SECTION("Bytes");
  // -----------------------------------------------------------------------------

  b = make_byte(0);
  TEST_ASSERT(b->codepoint == 0);
  if (PRINTING) ast_print_tree(b);
  free_expression(b);
  b = make_byte(255);
  TEST_ASSERT(b->codepoint == 255);
  if (PRINTING) ast_print_tree(b);
  free_expression(b);
  b = make_byte(-2);
  TEST_ASSERT(b->codepoint == 254);
  if (PRINTING) ast_print_tree(b);
  free_expression(b);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Bytestrings");
  // -----------------------------------------------------------------------------

#define COMPARE_BYTESTRING(str, literal) do {				\
    TEST_ASSERT(memcmp(String_ptr(str),					\
		       (literal),					\
		       strlen(literal)) == 0);				\
    TEST_ASSERT(String_len(str) == strlen(literal));			\
  } while (0);    

  b = make_bytestring_from((const string *)"foo", 3);
  TEST_ASSERT(b);
  TEST_ASSERT(b->bytes);
  COMPARE_BYTESTRING(b->bytes, "foo");
  if (PRINTING) ast_print_tree(b);
  free_expression(b);
  b = make_byte(255);
  TEST_ASSERT(b->codepoint == 255);
  if (PRINTING) ast_print_tree(b);
  free_expression(b);
  b = make_byte(-2);
  TEST_ASSERT(b->codepoint == 254);
  if (PRINTING) ast_print_tree(b);
  free_expression(b);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Expressions");
  // -----------------------------------------------------------------------------

  exp = make_char(255);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_CHAR);
  TEST_ASSERT(ast_expressionp(exp));
  TEST_ASSERT(exp->codepoint == 255);
  free_expression(exp);
  exp = make_char(0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->codepoint == 0);
  free_expression(exp);
  
  exp2 = make_byte(255);
  exp = make_find(exp2);
  TEST_ASSERT(exp->type == AST_FIND);
  TEST_ASSERT(exp->child == exp2);
  free_expression(exp);

  str = String_from_literal("hello");
  exp = make_byte(114);
  exp2 = make_capture(str, exp);
  TEST_ASSERT(exp && exp2);
  TEST_ASSERT(exp2->type == AST_CAPTURE);
  TEST_ASSERT_NOT_NULL(exp2->name);
  TEST_ASSERT_NOT_NULL(exp2->child);
  if (PRINTING) ast_print_tree(exp2);
  TEST_ASSERT(exp2->child == exp);
  TEST_ASSERT(String_cmp(exp2->name, str) == 0);
  free_expression(exp2);

  exp = make_byterange(0, 0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTERANGE);
  TEST_ASSERT(exp->low == 0);
  TEST_ASSERT(exp->high == 0);
  free_expression(exp);

  exp = make_byterange(255, 1);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTERANGE);
  TEST_ASSERT(exp->low == 255);
  TEST_ASSERT(exp->high == 1);
  free_expression(exp);
  
  exp = make_byteset();
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTESET);
  int i;
  for (i = 0; i < 256; i++)
    assert(!byteset_member(exp->byteset, (pexl_Byte) i));
  TEST_ASSERT(true);		/* output check mark after loop */
  for (i = 0; i < 256; i++)
    byteset_add(exp->byteset, (pexl_Byte) i);
  for (i = 0; i < 256; i++)
    assert(byteset_member(exp->byteset, (pexl_Byte) i));
  TEST_ASSERT(true);		/* output check mark after loop */

  free_expression(exp);
  exp = make_byteset();
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTESET);
  for (i = 0; i < 256; i += 3)
    byteset_add(exp->byteset, (pexl_Byte) i);
  for (i = 0; i < 256; i++)
    if ((i - 3*(i / 3)) == 0) {
      assert(byteset_member(exp->byteset, (pexl_Byte) i));
    } else {
      assert(!byteset_member(exp->byteset, (pexl_Byte) i));
    }
  TEST_ASSERT(true);		/* output check mark after loop */
  free_expression(exp);
  
  exp = make_char(0xFEFE);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_CHAR);
  TEST_ASSERT(exp->codepoint == 0xFEFE);
  free_expression(exp);
  
  exp = make_range(0, 0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_RANGE);
  TEST_ASSERT(exp->low == 0);
  TEST_ASSERT(exp->high == 0);
  free_expression(exp);

  exp = make_range(pexl_Index_MAX, 123);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_RANGE);
  TEST_ASSERT(exp->low == pexl_Index_MAX);
  TEST_ASSERT(exp->high == 123);
  free_expression(exp);
  
  /* Add to the codepointset in ascending order */
  exp = make_codepointset(0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_SET);
  TEST_ASSERT(exp->codepointset->capacity > 0); /* some minimum size */
  TEST_ASSERT(exp->codepointset->size == 0);    /* empty */
  for (i = 0; i < 301; i++) {
    codepointset_add(exp->codepointset, (pexl_Codepoint) i*5);
  }
  TEST_ASSERT(exp->codepointset->size == (size_t) i);
  for (i = 0; i < 4+300*5; i++) {
    if ((i - 5*(i/5)) == 0) {
      assert(codepointset_mem(exp->codepointset, (pexl_Codepoint) i));
    } else {
      assert(!codepointset_mem(exp->codepointset, (pexl_Codepoint) i));
    }
  }
  TEST_ASSERT(true);		/* output check mark after loop */
  free_expression(exp);

  /* Add to the codepointset in descending order */
  exp = make_codepointset(0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_SET);
  TEST_ASSERT(exp->codepointset->capacity > 0); /* some minimum size */
  TEST_ASSERT(exp->codepointset->size == 0);    /* empty */
  for (i = 300; i >= 0; i--) {
    codepointset_add(exp->codepointset, (pexl_Codepoint) i*5);
  }
  TEST_ASSERT(exp->codepointset->size == (size_t) 301);
  /* Test in same ascending order as earlier */
  for (i = 0; i < 4+300*5; i++) {
    if ((i - 5*(i/5)) == 0) {
      assert(codepointset_mem(exp->codepointset, (pexl_Codepoint) i));
    } else {
      assert(!codepointset_mem(exp->codepointset, (pexl_Codepoint) i));
    }
  }
  TEST_ASSERT(true);		/* output check mark after loop */
  free_expression(exp);

  exp = make_codepointset(1001);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_SET);
  TEST_ASSERT(exp->codepointset->capacity == 1001); /* assumes minimum size <= 1001 */
  TEST_ASSERT(exp->codepointset->size == 0);	    /* empty */
  TEST_ASSERT(!codepointset_mem(exp->codepointset, (pexl_Codepoint) 0));
  codepointset_add(exp->codepointset, (pexl_Codepoint) 123456);
  TEST_ASSERT(!codepointset_mem(exp->codepointset, (pexl_Codepoint) 0));
  TEST_ASSERT(codepointset_mem(exp->codepointset, (pexl_Codepoint) 123456));
  codepointset_add(exp->codepointset, (pexl_Codepoint) 1);
  TEST_ASSERT(!codepointset_mem(exp->codepointset, (pexl_Codepoint) 0));
  TEST_ASSERT(codepointset_mem(exp->codepointset, (pexl_Codepoint) 123456));
  TEST_ASSERT(codepointset_mem(exp->codepointset, (pexl_Codepoint) 1));
  TEST_ASSERT(exp->codepointset->size == (size_t) 2);
  free_expression(exp);

  /* Arbitrary byte strings as patterns */
  exp = make_bytestring_from(NULL, 0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTESTRING);
  TEST_ASSERT(ast_expressionp(exp));
  free_expression(exp);

  exp = make_bytestring_from("", 0);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTESTRING);
  TEST_ASSERT(ast_expressionp(exp));
  free_expression(exp);
  
  exp = make_bytestring_from("\n", 1);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_BYTESTRING);
  TEST_ASSERT(ast_expressionp(exp));
  free_expression(exp);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Sequence expressions");
  // -----------------------------------------------------------------------------

  exp2 = make_byte(65);
  TEST_ASSERT(exp2);

  TEST_EXPECT_WARNING;
  exp = make_seq(NULL, NULL);
  TEST_ASSERT_NULL(exp);

  TEST_EXPECT_WARNING;
  exp = make_seq(exp2, NULL);
  TEST_ASSERT_NULL(exp);

  TEST_EXPECT_WARNING;
  exp = make_seq(NULL, exp2);
  TEST_ASSERT_NULL(exp);

  // We catch superficial errors in structure sharing
  TEST_EXPECT_WARNING;
  exp = make_seq(exp2, exp2);
  TEST_ASSERT_NULL(exp);

  exp = make_seq(exp2, make_true());
  TEST_ASSERT(exp);
  if (PRINTING) ast_print_tree(exp);
  TEST_ASSERT(exp->type == AST_SEQ);
  TEST_ASSERT(exp->child && exp->child->type == AST_BYTE);
  TEST_ASSERT(exp->child2 && exp->child2->type == AST_TRUE);
  free_expression(exp);

  // Here we build up a sequence of 10 items.
  char string_A[2] = {'A', '\0'};
  exp = NULL;
  for (i = 0; i < 10; i++) {
    exp = build_seq(exp, make_bytestring_from((string *)string_A,
					      strlen(string_A)));
    TEST_ASSERT(exp);
  }
  if (PRINTING) ast_print_tree(exp);
  free_expression(exp);

  // Sequence containing a sequence
  exp = build_seq(NULL, make_byte('H'));
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_BYTE);
  TEST_ASSERT(exp->child == NULL);

  exp = build_seq(exp, make_seq(make_byte('i'), make_true()));
  if (PRINTING) {ast_print_tree(exp); newline();}
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_SEQ);
  TEST_ASSERT(exp->child->type == AST_BYTE);
  TEST_ASSERT(exp->child2->type == AST_SEQ);
  TEST_ASSERT(exp->child2->child->type == AST_BYTE);
  TEST_ASSERT(exp->child2->child2->type == AST_TRUE);
  TEST_ASSERT(ast_expressionp(exp));
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  ast_print_tree(exp); newline();
  free_expression(exp);		// frees all constituent exps as well

  // -----------------------------------------------------------------------------
  TEST_SECTION("Choice expressions");
  // -----------------------------------------------------------------------------

  exp2 = make_byte(65);
  TEST_ASSERT(exp2);

  TEST_EXPECT_WARNING;
  exp = make_choice(NULL, NULL);
  TEST_ASSERT_NULL(exp);

  TEST_EXPECT_WARNING;
  exp = make_choice(exp2, NULL);
  TEST_ASSERT_NULL(exp);

  TEST_EXPECT_WARNING;
  exp = make_choice(NULL, exp2);
  TEST_ASSERT_NULL(exp);

  // We catch superficial errors in structure sharing
  TEST_EXPECT_WARNING;
  exp = make_choice(exp2, exp2);
  TEST_ASSERT_NULL(exp);

  exp = make_choice(exp2, make_false());
  TEST_ASSERT(exp);
  if (PRINTING) ast_print_tree(exp);
  TEST_ASSERT(exp->type == AST_CHOICE);
  TEST_ASSERT(exp->child && exp->child->type == AST_BYTE);
  TEST_ASSERT(exp->child2 && exp->child2->type == AST_FALSE);
  free_expression(exp);

  exp = build_choice(NULL, make_byte(65));
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_BYTE);
  TEST_ASSERT(exp->child == NULL);
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  free_expression(exp);

  exp1 = make_bytestring_from((string *)string_A, strlen(string_A));
  TEST_ASSERT(exp1);
  exp2 = make_bytestring_from((string *)string_A, strlen(string_A));
  TEST_ASSERT(exp2);
  exp = make_choice(exp1, exp2);
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_CHOICE);
  TEST_ASSERT(exp->child == exp1);
  TEST_ASSERT(exp->child->type == AST_BYTESTRING);
  TEST_ASSERT(ast_expressionp(exp));
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  free_expression(exp);		/* frees exp1, exp2 as well */

  /* Choice containing a choice */
  exp = build_choice(make_byte('X'), make_byte('Y'));
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_CHOICE);
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  exp = build_choice(exp, make_byte('Z'));
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_CHOICE);
  TEST_ASSERT(exp->child && (exp->child->type == AST_BYTE));
  TEST_ASSERT(exp->child->codepoint = 'X');
  TEST_ASSERT(exp->child2 && (exp->child2->type == AST_CHOICE));
  TEST_ASSERT(exp->child2->child && (exp->child2->child->type == AST_BYTE));
  TEST_ASSERT(exp->child2->child->codepoint = 'Y');
  TEST_ASSERT(exp->child2->child2 && (exp->child2->child2->type == AST_BYTE));
  TEST_ASSERT(exp->child2->child2->codepoint = 'Z');
  TEST_ASSERT(ast_expressionp(exp));
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  free_expression(exp);		/* frees exp1, exp2 as well */

  TEST_SECTION("Repetition expressions");
  exp = make_repetition(pexl_Index_MIN, pexl_Index_MAX, make_byte('x'));
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_REPETITION);
  TEST_ASSERT(exp->child->type == AST_BYTE);
  TEST_ASSERT(exp->min == pexl_Index_MIN);
  TEST_ASSERT(exp->max == pexl_Index_MAX);
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  free_expression(exp);		/* frees internal exp as well */
  exp = make_repetition(0, 0, make_byte('y'));
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_REPETITION);
  TEST_ASSERT(exp->child->type == AST_BYTE);
  TEST_ASSERT(exp->min == 0);
  TEST_ASSERT(exp->max == 0);
  TEST_ASSERT(ast_expressionp(exp));
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  free_expression(exp);		/* frees internal exp as well */

  // -----------------------------------------------------------------------------
  TEST_SECTION("Predicate expressions");
  // -----------------------------------------------------------------------------

  TEST_EXPECT_WARNING;
  exp = make_predicate(AST_LOOKAHEAD, NULL);
  TEST_ASSERT_NULL(exp);	/* exp cannot be null */
  TEST_EXPECT_WARNING;
  exp = make_predicate(AST_LOOKAHEAD + 100, NULL);
  TEST_ASSERT_NULL(exp);	/* invalid predicate type */

  exp = make_predicate(AST_LOOKAHEAD, make_byte('A'));
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_LOOKAHEAD);
  TEST_ASSERT(exp->child != NULL);
  TEST_ASSERT(exp->child->type == AST_BYTE);
  TEST_ASSERT(ast_expressionp(exp));
  if (PRINTING) {print_ast_as_sexpr(exp); newline();}
  free_expression(exp);		/* frees internal exp as well */
 
  // -----------------------------------------------------------------------------
  TEST_SECTION("Parsing s-expression \"atoms\"");
  // -----------------------------------------------------------------------------

// Note: sexp_from_literal() is a convenience function defined in this file.
#define EXPECT_ERROR(string, sexp_test) do {		\
    if (PRINTING) {					\
      printf("Expecting error from '%s'\n",		\
	     (string) ? (string) : "(NULL)");		\
      fflush(NULL);					\
    }							\
    s = sexp_from_literal(string);			\
    if (PRINTING) {					\
      print_sexp_repr(s); newline();			\
      print_sexp(s); newline();				\
    }							\
    TEST_ASSERT(sexp_test(s));				\
    TEST_EXPECT_WARNING;				\
    exp = sexp_to_ast(s);				\
    TEST_ASSERT_NULL(exp);				\
    free_sexp(s);					\
  } while (0)
  
  // Special case: read_sexp("") ==> NULL
  s = sexp_from_literal("");
  TEST_ASSERT_NULL(s);

  EXPECT_ERROR(NULL, sexp_errorp);
  EXPECT_ERROR(")", sexp_extraclosep);
  EXPECT_ERROR("(", sexp_incomplete_listp);
  EXPECT_ERROR("+", sexp_errorp);
  EXPECT_ERROR("()", sexp_nullp);

// Note: sexp_from_literal() is a convenience function defined in this file.
// Note: MAKE_EXP sets the variable 'exp', which must be freed later.
#define MAKE_EXP(string, sexp_test, asttype) do {		\
    if (PRINTING) printf("Input string: %s\n", (string));	\
    s = sexp_from_literal(string);				\
    TEST_ASSERT(s);						\
    if (PRINTING) {print_sexp_repr(s); newline();}		\
    TEST_ASSERT(sexp_test(s));					\
    exp = sexp_to_ast(s);					\
    TEST_ASSERT(exp);						\
    if (PRINTING) ast_print_tree(exp);				\
    TEST_ASSERT(exp->type == (asttype));			\
    free_sexp(s);						\
  } while (0)

#define EXPECT_EXP(string, sexp_test, asttype) do {		\
    MAKE_EXP((string), (sexp_test), (asttype));			\
    free_expression(exp);					\
  } while (0)

#define EXPECT_WELL_TYPED(string, asttype) do {			\
    MAKE_EXP((string), non_nullp, (asttype));			\
    if ((asttype) == AST_MODULE) {				\
      TEST_ASSERT(typecheck_mod(exp, &error_exp));		\
      if (PRINTING)						\
	printf("Module contains %d forms\n",			\
	       ast_child_count(exp));				\
    }								\
    else if ((asttype) == AST_DEFINITION)			\
      TEST_ASSERT(typecheck_def(exp, &error_exp));		\
    else							\
      TEST_ASSERT(typecheck_exp(exp, &error_exp));		\
    free_expression(exp);					\
  } while (0)

#define EXPECT_ILL_TYPED(string, asttype) do {			\
    MAKE_EXP((string), non_nullp, (asttype));			\
    TEST_EXPECT_WARNING;					\
    if ((asttype) == AST_MODULE) {				\
      TEST_ASSERT(!typecheck_mod(exp, &error_exp));		\
    }								\
    else if ((asttype) == AST_DEFINITION)			\
      TEST_ASSERT(!typecheck_def(exp, &error_exp));		\
    else							\
      TEST_ASSERT(!typecheck_exp(exp, &error_exp));		\
    free_expression(exp);					\
  } while (0)

#define EXPECT_NUMBER(string) do {				\
    MAKE_EXP((string), sexp_intp, AST_NUMBER);			\
    free_expression(exp);					\
  } while (0)

  // ------------------------------------------------------------------
  // Codepoints and bytes
  // ------------------------------------------------------------------

  EXPECT_EXP("#\\A", sexp_charp, AST_CHAR);
  EXPECT_EXP("#\\x0a", sexp_charp, AST_CHAR);
  EXPECT_EXP("#\\x0A", sexp_charp, AST_CHAR);
  EXPECT_EXP("#\\u000A", sexp_charp, AST_CHAR);
  // Snowman: ⛄
  EXPECT_EXP("#\\u26C4", sexp_charp, AST_CHAR);
  // Rainbow: 🌈
  EXPECT_EXP("#\\U0001F308", sexp_charp, AST_CHAR);

  // Note: The value 0xFFFFFFFF is out of the range of valid
  // codepoints.  The read_sexp() parses it because it is
  // SYNTACTICALLY VALID.  The fact that the VALUE denoted is not a
  // valid codepoint is a SEMANTIC error.  Later, when the value is
  // checked, the error will be reported.
  EXPECT_ILL_TYPED("#\\UFFFFFFFF", AST_CHAR);
  EXPECT_WELL_TYPED("#\\U0010FFFF", AST_CHAR);

  EXPECT_EXP("(byte #\\A)", sexp_listp, AST_BYTE);
  EXPECT_EXP("(byte #\\x0a)", sexp_listp, AST_BYTE);
  EXPECT_EXP("(byte #\\x0A)", sexp_listp, AST_BYTE);
  EXPECT_EXP("(byte #\\u000A)", sexp_listp, AST_BYTE);
  // Range 0-255 for a byte is not checked until later
  EXPECT_EXP("(byte #\\uAAAA)", sexp_listp, AST_BYTE);

  // ------------------------------------------------------------------
  // Ints
  // ------------------------------------------------------------------

  EXPECT_NUMBER("0");
  EXPECT_NUMBER("-0");
  EXPECT_NUMBER("1");
  EXPECT_NUMBER("-1");
  // pexl_Index_MAX
  EXPECT_NUMBER("2147483647");
  // pexl_Index_MIN
  EXPECT_NUMBER("-2147483648");
  // pexl_Number_MAX
  EXPECT_NUMBER("9223372036854775807");
  // pexl_Number_MIN
  EXPECT_NUMBER("-9223372036854775808");

  // ------------------------------------------------------------------
  // Identifiers
  // ------------------------------------------------------------------
  
  // Identifiers can contain: .!*/:<=>?@^_~$%&

  EXPECT_EXP("a", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("abcdefg", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("A", sexp_identifierp, AST_IDENTIFIER);

  // Qualified identifiers have a module name in front.  At this
  // point, though, we are not checking the contents (format) of the
  // identifier.
  EXPECT_EXP("a.b", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("abcdefghijklmnopqrstuvwxyz.&", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("*.abcdefghijklmnopqrstuvwxyz", sexp_identifierp, AST_IDENTIFIER);

  // The AST identifier rules also limits the possible first chars.
  EXPECT_ERROR("1a", sexp_errorp);
  EXPECT_ERROR("+a", sexp_errorp);
  EXPECT_ERROR("-a", sexp_errorp);

  EXPECT_EXP("/a", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("=a", sexp_identifierp, AST_IDENTIFIER);

  EXPECT_EXP("/", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("a-b", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("!@^_~$%&", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("a!*/:<=>?@^_~$%&", sexp_identifierp, AST_IDENTIFIER);

  EXPECT_EXP(".a", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP(".b", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("a.b.", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("a.b.c", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("b.", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("a.b.", sexp_identifierp, AST_IDENTIFIER);
  EXPECT_EXP("a.b.c.", sexp_identifierp, AST_IDENTIFIER);

  // ------------------------------------------------------------------
  // Bytestrings
  // ------------------------------------------------------------------

  EXPECT_EXP("\"\"", sexp_bytestringp, AST_BYTESTRING);
  EXPECT_EXP("\"Hello, world!\"", sexp_bytestringp, AST_BYTESTRING);
  EXPECT_EXP("\"\\n\"", sexp_bytestringp, AST_BYTESTRING);
  EXPECT_EXP("\"On\\nseparate\\nlines\n\"", sexp_bytestringp, AST_BYTESTRING);
  EXPECT_EXP("\"Has hex char \\x0a\"", sexp_bytestringp, AST_BYTESTRING);

  // These are NOT allowed in bytestrings.  Do we need a Unicode
  // string literal type?  If so, then it must be tagged with its
  // encoding, e.g. UTF-8 or UTF-16 or UTF-32.
  // EXPECT_EXP("\"Has unicode char \\u000a\"", sexp_bytestringp, AST_BYTESTRING);
  // EXPECT_EXP("\"Has long unicode char \\U0000000a\"", sexp_bytestringp, AST_BYTESTRING);
  // EXPECT_EXP("\"Has long unicode char \\U0010FFFF\"", sexp_bytestringp, AST_BYTESTRING);


  // -----------------------------------------------------------------------------
  TEST_SECTION("Parsing s-expressions with single \"applications\"");
  // -----------------------------------------------------------------------------

  // ------------------------------------------------------------------
  // Sequences
  // ------------------------------------------------------------------

  error_exp = NULL;
 
  EXPECT_ERROR("(0", sexp_incomplete_listp);
  EXPECT_ERROR("(#\\A", sexp_incomplete_listp);
  EXPECT_ERROR("(seq #\\A", sexp_incomplete_listp);
  EXPECT_ERROR("(choice #\\A", sexp_incomplete_listp);

  EXPECT_ERROR("(0)", sexp_listp);
  EXPECT_ERROR("(\"a bytestring\"", sexp_incomplete_listp);
  EXPECT_ERROR("(\"a bytestring\")", sexp_listp);

  // A sequence or choice must have at least one expression in it
  EXPECT_ERROR("(seq)", sexp_listp);
  EXPECT_ERROR("(choice)", sexp_listp);

  // A sequence of one expression is just that expression
  MAKE_EXP("(seq 0)", sexp_listp, AST_NUMBER);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);
  
  // A sequence of one expression is just that expression
  EXPECT_EXP("(seq #\\A)", sexp_listp, AST_CHAR);

  EXPECT_EXP("(seq #\\A #\\B)", sexp_listp, AST_SEQ);

  MAKE_EXP("(seq 0 #\\A #\\B #\\C #\\D)", sexp_listp, AST_SEQ);
  
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);
  
  MAKE_EXP("(seq #\\A #\\B #\\C #\\D 5)", sexp_listp, AST_SEQ);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(seq #\\A #\\B #\\C #\\D #\\E)", sexp_listp, AST_SEQ);

  EXPECT_EXP("(seq \"string\")", sexp_listp, AST_BYTESTRING);
  EXPECT_EXP("(seq a)", sexp_listp, AST_IDENTIFIER);
  EXPECT_EXP("(seq a b c)", sexp_listp, AST_SEQ);

  EXPECT_EXP("(seq a #\\A \"string\")", sexp_listp, AST_SEQ);
  MAKE_EXP("(seq a 1 \"string\")", sexp_listp, AST_SEQ);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(seq \"string\" id1 id2 id3 #\\A #\\B)", sexp_listp, AST_SEQ);

  // ------------------------------------------------------------------
  // Choices
  // ------------------------------------------------------------------

  MAKE_EXP("(choice 0)", sexp_listp, AST_NUMBER);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  MAKE_EXP("(choice #\\A 0)", sexp_listp, AST_CHOICE);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(choice #\\nul)", sexp_listp, AST_CHAR);
  EXPECT_EXP("(choice (byte #\\nul))", sexp_listp, AST_BYTE);
  EXPECT_EXP("(choice #\\A #\\A)", sexp_listp, AST_CHOICE);

  MAKE_EXP("(choice 0 #\\A #\\B #\\C #\\D)", sexp_listp, AST_CHOICE);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  MAKE_EXP("(choice #\\A #\\B #\\C #\\D 5)", sexp_listp, AST_CHOICE);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(choice #\\A #\\B #\\C #\\D #\\E)", sexp_listp, AST_CHOICE);

  EXPECT_EXP("(choice \"string\")", sexp_listp, AST_BYTESTRING);
  EXPECT_EXP("(choice a)", sexp_listp, AST_IDENTIFIER);
  EXPECT_EXP("(choice a b c)", sexp_listp, AST_CHOICE);

  EXPECT_EXP("(choice a #\\A \"string\")", sexp_listp, AST_CHOICE);

  MAKE_EXP("(choice a 1 \"string\")", sexp_listp, AST_CHOICE);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(choice \"string\" id1 id2 id3 #\\A #\\B)", sexp_listp, AST_CHOICE);

  // ------------------------------------------------------------------
  // Repetitions
  // ------------------------------------------------------------------

  EXPECT_ERROR("(repeat)", sexp_listp);
  EXPECT_ERROR("(repeat 0)", sexp_listp);
  EXPECT_ERROR("(repeat 0 0)", sexp_listp);
  EXPECT_ERROR("(repeat \"foo\")", sexp_listp);
  EXPECT_ERROR("(repeat \"foo\" 0)", sexp_listp);
  EXPECT_ERROR("(repeat \"foo\" 0 0)", sexp_listp);
  EXPECT_ERROR("(repeat 0 \"foo\")", sexp_listp);
  EXPECT_ERROR("(repeat 0 \"foo\" 0)", sexp_listp);
  EXPECT_ERROR("(repeat -100 \"foo\" 0)", sexp_listp);

  EXPECT_EXP("(repeat -2147483648  2147483647 #\\x0a)", sexp_listp, AST_REPETITION);
  EXPECT_EXP("(repeat  2147483647 -2147483648 #\\x0a)", sexp_listp, AST_REPETITION);

  // pexl_Number_MIN, pexl_Number_MAX are valid numbers in an
  // s-expression, but too large to be a pexl_Index which is the
  // parameter type for the repeat expression.
  EXPECT_EXP("(repeat -9223372036854775808 9223372036854775807 #\\x0a)", sexp_listp, AST_REPETITION);

  EXPECT_ILL_TYPED("(repeat -2147483648  2147483647 #\\x0a)", AST_REPETITION);
  EXPECT_ILL_TYPED("(repeat  2147483647 -2147483648 #\\x0a)", AST_REPETITION);
  EXPECT_ILL_TYPED("(repeat -9223372036854775808 9223372036854775807 #\\x0a)", AST_REPETITION);

  MAKE_EXP("(repeat 0 0 0)", sexp_listp, AST_REPETITION);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(repeat 0 0 #\\A)", sexp_listp, AST_REPETITION);

  // NOTE: Min/max values are checked only for range 0..PEXL_REPETITION_MAX.
  // They are not compared, nor is the expression argument examined.
  EXPECT_EXP("(repeat 200 300 #\\A)", sexp_listp, AST_REPETITION);
  EXPECT_EXP("(repeat 0 0 \"string\")", sexp_listp, AST_REPETITION);
  EXPECT_EXP("(repeat 0 0 #\\x0a)", sexp_listp, AST_REPETITION);
  EXPECT_EXP("(repeat 1 2 #\\x0a)", sexp_listp, AST_REPETITION);
  EXPECT_EXP("(repeat 100000 200000 #\\x0a)", sexp_listp, AST_REPETITION);


  StringBuffer *tmpbuf;
  char *tmpstr, *string_from_number;
  
#define MAKE_REPEAT_EXP(min_amt, max_amt, target_exp) do {	\
    asprintf(&string_from_number, "%llu", (long long) min_amt);	\
    TEST_ASSERT_NOT_NULL(string_from_number);			\
    tmpbuf = StringBuffer_new(0);				\
    StringBuffer_add_string(tmpbuf, "(repeat ");		\
    StringBuffer_add_string(tmpbuf, string_from_number);	\
    free(string_from_number);					\
    StringBuffer_add_string(tmpbuf, " ");			\
    asprintf(&string_from_number, "%llu", (long long) max_amt);	\
    TEST_ASSERT_NOT_NULL(string_from_number);			\
    StringBuffer_add_string(tmpbuf, string_from_number);	\
    free(string_from_number);					\
    StringBuffer_add_string(tmpbuf, " ");			\
    StringBuffer_add_string(tmpbuf, (target_exp));		\
    StringBuffer_add_string(tmpbuf, ") ");			\
    tmpstr = StringBuffer_tostring(tmpbuf);			\
    StringBuffer_free(tmpbuf);					\
    if (PRINTING)						\
      printf("Generated repeat expression is '%s'\n", tmpstr);	\
} while (0)
  
  // Build a repeat expression that uses the maximum values for both
  // numeric arguments (min and max):
  MAKE_REPEAT_EXP(PEXL_REPETITION_MAX, PEXL_REPETITION_MAX, "#\\x0a");
  EXPECT_EXP(tmpstr, sexp_listp, AST_REPETITION);
  free(tmpstr);

  // Build a repeat expression that uses the maximum values for only
  // the numeric argument 'min':
  MAKE_REPEAT_EXP(PEXL_REPETITION_MAX, 0, "#\\x0a");
  EXPECT_EXP(tmpstr, sexp_listp, AST_REPETITION);
  free(tmpstr);
  
  // Build a repeat expression that uses the maximum values for only
  // the numeric argument 'max':
  MAKE_REPEAT_EXP(0, PEXL_REPETITION_MAX, "#\\x0a");
  EXPECT_EXP(tmpstr, sexp_listp, AST_REPETITION);
  free(tmpstr);

  // Build a repeat expression that exceeds the maximum value for the
  // numeric argument 'min:
  MAKE_REPEAT_EXP(PEXL_REPETITION_MAX+1, PEXL_REPETITION_MAX, "#\\x0a");
  EXPECT_EXP(tmpstr, sexp_listp, AST_REPETITION);
  EXPECT_ILL_TYPED(tmpstr, AST_REPETITION);
  free(tmpstr);

  // Build a repeat expression that exceeds the maximum value for the
  // numeric argument 'max':
  MAKE_REPEAT_EXP(PEXL_REPETITION_MAX, PEXL_REPETITION_MAX+1, "#\\x0a");
  EXPECT_EXP(tmpstr, sexp_listp, AST_REPETITION);
  EXPECT_ILL_TYPED(tmpstr, AST_REPETITION);
  free(tmpstr);
  
  // Build a repeat expression that exceeds the maximum values for both
  // numeric arguments:
  MAKE_REPEAT_EXP(PEXL_REPETITION_MAX+1, PEXL_REPETITION_MAX+1, "#\\x0a");
  EXPECT_EXP(tmpstr, sexp_listp, AST_REPETITION);
  EXPECT_ILL_TYPED(tmpstr, AST_REPETITION);
  free(tmpstr);

  // Repeat min is greater than max, so the expression is not well-formed:
  MAKE_EXP("(repeat 3 2 #\\x0a)", sexp_listp, AST_REPETITION);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  TEST_ASSERT(error_exp == exp);
  free_expression(exp);

  // ------------------------------------------------------------------
  // 'Find' expressions
  // ------------------------------------------------------------------

  EXPECT_ERROR("(find)", sexp_listp);
  EXPECT_ERROR("(find 0 0)", sexp_listp);
  EXPECT_ERROR("(find a b c)", sexp_listp);

  MAKE_EXP("(find 0)", sexp_listp, AST_FIND);
  TEST_ASSERT(!typecheck_exp(exp, &error_exp));
  free_expression(exp);

  EXPECT_EXP("(find #\\x00)", sexp_listp, AST_FIND);
  EXPECT_EXP("(find \"Hi\")", sexp_listp, AST_FIND);
  EXPECT_EXP("(find (repeat 0 1 \"Hi\"))", sexp_listp, AST_FIND);

  // ------------------------------------------------------------------
  // 'Capure' and 'Insert' expressions
  // ------------------------------------------------------------------

  EXPECT_ERROR("(capture)", sexp_listp);
  EXPECT_ERROR("(capture 0)", sexp_listp);
  EXPECT_ERROR("(capture a b c)", sexp_listp);
  EXPECT_ERROR("(capture 0 #\\A)", sexp_listp);

  EXPECT_EXP("(capture \"capture name\" #\\x00)", sexp_listp, AST_CAPTURE);
  EXPECT_EXP("(capture \"another capture name \" \"Hi\")", sexp_listp, AST_CAPTURE);
  EXPECT_EXP("(capture \"foo\" (repeat 0 1 \"Hi\"))", sexp_listp, AST_CAPTURE);

  EXPECT_ERROR("(insert)", sexp_listp);
  EXPECT_ERROR("(insert 0)", sexp_listp);
  EXPECT_ERROR("(insert a b c)", sexp_listp);
  EXPECT_ERROR("(insert \"name\" #\\A)", sexp_listp);
  EXPECT_ERROR("(insert \"foo\" (repeat 0 1 \"Hi\"))", sexp_listp);

  EXPECT_EXP("(insert \"name\" \"value\")", sexp_listp, AST_INSERT);
  EXPECT_EXP("(insert \"another name \" \"another value\")", sexp_listp, AST_INSERT);
  
  // ------------------------------------------------------------------
  // Backrefs
  // ------------------------------------------------------------------

  // Missing the identifier arg
  EXPECT_ERROR("(backref)", sexp_listp);
  // Arg not an identifier
  EXPECT_ERROR("(backref 0)", sexp_listp);
  EXPECT_ERROR("(backref #\\x00)", sexp_listp);
  EXPECT_ERROR("(backref \"Hi\")", sexp_listp);
  EXPECT_ERROR("(backref (repeat 0 1 \"Hi\"))", sexp_listp);
  // Too many args
  EXPECT_ERROR("(backref 0 0)", sexp_listp);
  EXPECT_ERROR("(backref a b c)", sexp_listp);


  // An identifier is like a symbol in Scheme or Lisp
  EXPECT_EXP("(backref abc.def.)", sexp_listp, AST_BACKREF);
  EXPECT_EXP("(backref abc)", sexp_listp, AST_BACKREF);
  EXPECT_EXP("(backref abc.def)", sexp_listp, AST_BACKREF);

  // ------------------------------------------------------------------
  // Predicates
  // ------------------------------------------------------------------

#define TEST_PREDICATE(op, predtype)		          \
  EXPECT_ERROR("(" op ")", sexp_listp);			  \
  EXPECT_ERROR("(" op " 0 0)", sexp_listp);		  \
  EXPECT_ERROR("(" op " #\\x01 0)", sexp_listp);	  \
  EXPECT_ERROR("(" op " \"foo\" 0)", sexp_listp);	  \
  EXPECT_EXP("(" op " 0)", sexp_listp, predtype);	  \
  EXPECT_EXP("(" op " #\\x01)", sexp_listp, predtype);	  \
  EXPECT_EXP("(" op " \"foo\")", sexp_listp, predtype);

  //  confess("TEMP", "\nTEMPORARILY NOT TESTING PREDICATES\n\n");
  TEST_PREDICATE(">",  AST_LOOKAHEAD);
  TEST_PREDICATE("!>", AST_NEGLOOKAHEAD);
  TEST_PREDICATE("<",  AST_LOOKBEHIND);
  TEST_PREDICATE("!<", AST_NEGLOOKBEHIND);

  // ------------------------------------------------------------------
  TEST_SECTION("Parsing byte ranges and sets");
  // ------------------------------------------------------------------

  // Correct types of args:
  EXPECT_WELL_TYPED("(brange #\\Z #\\Z)", AST_BYTERANGE);
  EXPECT_WELL_TYPED("(brange #\\u0000 #\\u0000)", AST_BYTERANGE);
  EXPECT_WELL_TYPED("(brange #\\x0A #\\u0000)", AST_BYTERANGE);
  EXPECT_WELL_TYPED("(brange #\\x0A #\\U00000000)", AST_BYTERANGE);

  EXPECT_WELL_TYPED("(brange #\\x0A #\\uFFFF)", AST_BYTERANGE);

  // Wrong number of args:
  EXPECT_ERROR("(brange)", sexp_listp);
  EXPECT_ERROR("(brange 1)", sexp_listp);
  EXPECT_ERROR("(brange #\\A)", sexp_listp);
  EXPECT_ERROR("(brange #\\A #\\B #\\C)", sexp_listp);

  // Wrong type(s) of args:
  EXPECT_ERROR("(brange 0 0)", sexp_listp);
  EXPECT_ERROR("(brange #\\A \"Hi\")", sexp_listp);
  EXPECT_ERROR("(brange \"Hi\" #\\Z)", sexp_listp);
  EXPECT_ERROR("(brange #\\A (seq #\\Z))", sexp_listp);

  // Empty range is allowed
  EXPECT_WELL_TYPED("(brange #\\Z #\\A)", AST_BYTERANGE);
  // Proper range
  EXPECT_WELL_TYPED("(brange #\\A #\\B)", AST_BYTERANGE);

  EXPECT_WELL_TYPED("(bset #\\A #\\u0000)", AST_BYTESET);
  EXPECT_WELL_TYPED("(bset #\\B #\\U00000000)", AST_BYTESET);
  EXPECT_WELL_TYPED("(bset #\\u0000)", AST_BYTESET);
  EXPECT_WELL_TYPED("(bset #\\U00000000)", AST_BYTESET);

  // Wrong type (or range) of args:
  EXPECT_ERROR("(bset 0)", sexp_listp);
  EXPECT_ERROR("(bset #\\A 0)", sexp_listp);
  EXPECT_ERROR("(bset #\\A #\\B #\\C 0)", sexp_listp);
  EXPECT_ERROR("(bset #\\A \"Hi\")", sexp_listp);
  EXPECT_ERROR("(bset \"Hi\" #\\Z)", sexp_listp);
  EXPECT_ERROR("(bset #\\A (seq #\\Z))", sexp_listp);

  // Correct type of args:
  EXPECT_WELL_TYPED("(bset #\\A)", AST_BYTESET);
  EXPECT_WELL_TYPED("(bset #\\A #\\B)", AST_BYTESET);
  EXPECT_WELL_TYPED("(bset #\\Z #\\A #\\B #\\x00 #\\C)", AST_BYTESET);

  // Empty byteset:
  TEST_EXPECT_WARNING; // empty byteset
  EXPECT_EXP("(bset)", sexp_listp, AST_BYTESET);
  
  // ------------------------------------------------------------------
  TEST_SECTION("Parsing ranges and sets");
  // ------------------------------------------------------------------

  // Correct types of args:
  EXPECT_WELL_TYPED("(range #\\u0000 #\\u0000)", AST_RANGE);

  // Wrong number of args:
  EXPECT_ERROR("(range)", sexp_listp);
  EXPECT_ERROR("(range 1)", sexp_listp);
  EXPECT_ERROR("(range #\\A)", sexp_listp);
  EXPECT_ERROR("(range #\\A #\\B #\\C)", sexp_listp);

  EXPECT_WELL_TYPED("(range #\\x0A #\\u0000)", AST_RANGE);
  EXPECT_WELL_TYPED("(range #\\x0A #\\U00000000)", AST_RANGE);
  EXPECT_WELL_TYPED("(range #\\Z #\\A)", AST_RANGE);
  EXPECT_WELL_TYPED("(range #\\A #\\B)", AST_RANGE);

  // Wrong type(s) of args:
  EXPECT_ERROR("(range 0 0)", sexp_listp);
  EXPECT_ERROR("(range #\\A \"Hi\")", sexp_listp);
  EXPECT_ERROR("(range \"Hi\" #\\Z)", sexp_listp);
  EXPECT_ERROR("(range #\\A (seq #\\Z))", sexp_listp);

  // Wrong type of args:
  EXPECT_ERROR("(set 0)", sexp_listp);
  EXPECT_ERROR("(set #\\A 0)", sexp_listp);
  EXPECT_ERROR("(set #\\A #\\B #\\C 0)", sexp_listp);
  EXPECT_ERROR("(set #\\A \"Hi\")", sexp_listp);
  EXPECT_ERROR("(set \"Hi\" #\\Z)", sexp_listp);
  EXPECT_ERROR("(set #\\A (seq #\\Z))", sexp_listp);

  // Correct type of args:
  EXPECT_WELL_TYPED("(set #\\A #\\u0000)", AST_SET);
  EXPECT_WELL_TYPED("(set #\\B #\\U00000000)", AST_SET);
  EXPECT_WELL_TYPED("(set #\\u0000)", AST_SET);
  EXPECT_WELL_TYPED("(set #\\U00000000)", AST_SET);
  EXPECT_WELL_TYPED("(set #\\u0000 #\\u0001 #\\u0002)", AST_SET);  
  EXPECT_WELL_TYPED("(set #\\u0001 #\\u0000 #\\u0002)", AST_SET);  
  EXPECT_WELL_TYPED("(set #\\A)", AST_SET);
  EXPECT_WELL_TYPED("(set #\\A #\\B #\\C)", AST_SET);

  // Empty set:
  TEST_EXPECT_WARNING; // empty byteset
  EXPECT_EXP("(set)", sexp_listp, AST_SET);
  // Non-empty sets:
  EXPECT_ERROR("(set (byte #\\A))", sexp_listp);
  EXPECT_ERROR("(set #\\A #\\B (byte #\\C))", sexp_listp);
  
  // ------------------------------------------------------------------
  TEST_SECTION("Parsing more complicated s-expressions");
  // ------------------------------------------------------------------

  MAKE_EXP("(choice (backref abc.def))", sexp_listp, AST_BACKREF);
  TEST_ASSERT(exp && exp->child);
  TEST_ASSERT(exp->child->type == AST_IDENTIFIER);
  free_expression(exp);

  MAKE_EXP("(choice (seq #\\A #\\B) (backref abc.def))", sexp_listp, AST_CHOICE);
  TEST_ASSERT(exp && exp->child && exp->child2);
  TEST_ASSERT(exp->child->type == AST_SEQ);
  TEST_ASSERT(exp->child2->type == AST_BACKREF);
  free_expression(exp);

  MAKE_EXP("(choice (seq \"A bytestring\" (backref abc.def)) X)", sexp_listp, AST_CHOICE);
  TEST_ASSERT(exp && exp->child && exp->child2);
  TEST_ASSERT(exp->child->type == AST_SEQ);
  TEST_ASSERT(exp->child->child && (exp->child->child->type == AST_BYTESTRING));
  TEST_ASSERT(exp->child->child2 && (exp->child->child2->type == AST_BACKREF));
  TEST_ASSERT(exp->child2->type == AST_IDENTIFIER); // X
  free_expression(exp);
  
  // From the benchmarks (comparisons) repository:
  //   'abram' regex: /[A-Za-z]+ *Abram/
  //   'abram' RPL:   {[[A-Z][a-z]]+ " "* "Abram"}
  EXPECT_WELL_TYPED("(seq "
		     "  (repeat 1 0 (choice (brange #\\A #\\Z) (brange #\\a #\\z))) "
		     "  (repeat 0 0 \" \") "
		     "  \"Abram\")",
		     AST_SEQ);
  
  // 'longword' regex: [[a-z][A-Z]]{14,}
  EXPECT_WELL_TYPED("(repeat 14 0 "
		     "  (choice (brange #\\A #\\Z) (brange #\\a #\\z)))",
		     AST_REPETITION);

  // 'smallword' regex: [[a-z][A-Z]]{5,}
  EXPECT_WELL_TYPED("(repeat 5 0 "
		     "  (choice (brange #\\A #\\Z) (brange #\\a #\\z)))",
		     AST_REPETITION);

  // 'omega' regex: Omega
  EXPECT_EXP("\"Omega\"", sexp_bytestringp, AST_BYTESTRING);

  // 'the' regex: @the
  EXPECT_EXP("\"@the\"", sexp_bytestringp, AST_BYTESTRING);

  // 'tubalcain' regex: Tubalcain
  EXPECT_EXP("\"Tubalcain\"", sexp_bytestringp, AST_BYTESTRING);

  // 'pass' regex starts with a space: [ ]it came to pass
  EXPECT_EXP("\" it came to pass\"", sexp_bytestringp, AST_BYTESTRING);

  // 'psalm' regex: {[0-9]{1,3}[:][0-9]{1,3}}
  EXPECT_WELL_TYPED("(seq "
		     "  (repeat 1 3 (brange #\\0 #\\9)) "
		     "  #\\: "
		     "  (repeat 1 3 (brange #\\0 #\\9))) ",
		     AST_SEQ);

  // 'adamant' regex: "adam" [[a-z][A-Z]]*
  EXPECT_WELL_TYPED("(seq "
		     "  \"adam\" "
		     "  (repeat 0 0 "
		     "    (choice (brange #\\A #\\Z) (brange #\\a #\\z)))) ",
		     AST_SEQ);

  /* 'verses' is a pattern is designed to have several CALLs.
       RPL: 
	 num = [0-9]
	 nums = num+
	 colon = ":"
	 chapt_and_verse = {nums colon nums}
	 And = "And"
	 spaces = " "*
	 verses = {chapt_and_verse spaces And}
  */

  EXPECT_WELL_TYPED("(def num (brange #\\0 #\\9))", AST_DEFINITION); // [0-9]
  EXPECT_WELL_TYPED("(def nums (repeat 1 0 num))", AST_DEFINITION); // num+
  EXPECT_WELL_TYPED("(def colon #\\:)", AST_DEFINITION);
  EXPECT_WELL_TYPED("(def chapt_and_verse (seq nums colon nums))", AST_DEFINITION);
  EXPECT_WELL_TYPED("(def space \" \")", AST_DEFINITION);
  EXPECT_WELL_TYPED("(def spaces (repeat 0 0 space))", AST_DEFINITION);
  EXPECT_WELL_TYPED("(def spaces \"And\")", AST_DEFINITION);

  EXPECT_WELL_TYPED("(def verses "
		     "  (seq chapt_and_verse spaces and))",
		     AST_DEFINITION);


  // Modules
  
  EXPECT_WELL_TYPED("(mod)", AST_MODULE);

  EXPECT_WELL_TYPED("(mod "
		     "  (def verses "
		     "    (seq chapt_and_verse spaces and)))",
		     AST_MODULE);

  EXPECT_WELL_TYPED("(mod "
		     "  (def verses "
		     "    (seq chapt_and_verse spaces and))"
		     "  (def verses "
		     "    (seq chapt_and_verse spaces and)))",
		     AST_MODULE);

  MAKE_EXP("(mod "
	   "  (def verses 4))",
	   sexp_listp,
	   AST_MODULE);
  TEST_ASSERT(!typecheck_mod(exp, &error_exp));
  TEST_ASSERT(error_exp);
  TEST_ASSERT(error_exp->type==AST_NUMBER);
  free_expression(exp);

  EXPECT_ERROR("(mod "
	       "  (def a #\\A)"
	       "  (def b +)"    // Illegal character '+'
	       "  (seq c d))",
	       sexp_errorp);

  MAKE_EXP("(mod "
	   "  (def a #\\A)"
	   "  (def b $)"
	   "  (choice c d))",	// Expressions not allowed here
	   sexp_listp,
	   AST_MODULE);
  TEST_ASSERT(exp && !typecheck_mod(exp, &error_exp));
  TEST_ASSERT(error_exp && (error_exp->type==AST_CHOICE));
  free_expression(exp);

  MAKE_EXP("(mod "
	   "  (def a #\\A)"
	   "  (def b #\\B)"
	   "  (seq a b c)"	// Expressions not allowed here
	   "  (def d #\\D))",
	   sexp_listp,
	   AST_MODULE);
  ast_explist *ls = exp->defns;
  while (ls) {
    ast_print_tree(ls->head); newline();
    ls = ls->tail;
  }
  
  TEST_ASSERT(exp && !typecheck_mod(exp, &error_exp));
  TEST_ASSERT(error_exp);
  TEST_ASSERT(error_exp->type==AST_SEQ);
  free_expression(exp);
  
  MAKE_EXP("(mod "
	   "  \"Hi\""		// Expressions not allowed here
	   "  (def a #\\A)"
	   "  (def b #\\B)"
	   "  (choice 1 2))",	// Expressions not allowed here
	   sexp_listp,
	   AST_MODULE);
  ast_print_tree(exp);
  TEST_ASSERT(!typecheck_mod(exp, &error_exp));
  TEST_ASSERT(error_exp);
  ast_print_tree(error_exp);
  
  TEST_ASSERT(error_exp->type==AST_BYTESTRING);
  free_expression(exp);

  // ------------------------------------------------------------------
  TEST_SECTION("Converting ASTs into S-expression format");
  // ------------------------------------------------------------------

#define ROUND_TRIP(string, sexp_test, asttype) do {		\
    if (PRINTING) printf("Input string:    %s\n", (string));	\
    s = sexp_from_literal(string);				\
    TEST_ASSERT(s);						\
    tmp1 = write_sexp(s);					\
    if (PRINTING) printf("Sexp from input: %s\n", tmp1);	\
    TEST_ASSERT(sexp_test(s));					\
    exp = sexp_to_ast(s);					\
    TEST_ASSERT(exp);						\
    TEST_ASSERT(exp->type == (asttype));			\
    if (PRINTING) {						\
      printf("AST tree:\n");					\
      ast_print_tree(exp);					\
    }								\
    free_sexp(s);						\
    s = ast_to_sexp(exp);					\
    TEST_ASSERT(s);						\
    tmp2 = write_sexp(s);					\
    if (PRINTING) printf("Sexp from AST:   %s\n", tmp2);	\
    TEST_ASSERT(strcmp(tmp1, tmp2) == 0);			\
    free_sexp(s);						\
    free_expression(exp);					\
    free(tmp1); free(tmp2);					\
    if (PRINTING) newline();					\
  } while(0);

  // Snowman: ⛄
  ROUND_TRIP("#\\u26C4", sexp_charp, AST_CHAR);
  // Rainbow: 🌈
  ROUND_TRIP("#\\U0001F308", sexp_charp, AST_CHAR);

  ROUND_TRIP("0", sexp_intp, AST_NUMBER);
  ROUND_TRIP("-0", sexp_intp, AST_NUMBER);
  ROUND_TRIP("1", sexp_intp, AST_NUMBER);
  ROUND_TRIP("-1", sexp_intp, AST_NUMBER);
  // pexl_Index_MAX
  ROUND_TRIP("2147483647", sexp_intp, AST_NUMBER);
  // pexl_Index_MIN
  ROUND_TRIP("-2147483648", sexp_intp, AST_NUMBER);
  // pexl_Number_MAX
  ROUND_TRIP("9223372036854775807", sexp_intp, AST_NUMBER);
  // pexl_Number_MIN
  ROUND_TRIP("-9223372036854775808", sexp_intp, AST_NUMBER);

  ROUND_TRIP("a.b", sexp_identifierp, AST_IDENTIFIER);
  ROUND_TRIP("abcdefghijklmnopqrstuvwxyz.&", sexp_identifierp, AST_IDENTIFIER);
  ROUND_TRIP("*.abcdefghijklmnopqrstuvwxyz", sexp_identifierp, AST_IDENTIFIER);

  ROUND_TRIP("\"\"", sexp_bytestringp, AST_BYTESTRING);
  ROUND_TRIP("\"Hello, world!\"", sexp_bytestringp, AST_BYTESTRING);
  ROUND_TRIP("\"\\n\"", sexp_bytestringp, AST_BYTESTRING);
  ROUND_TRIP("\"On\\nseparate\\nlines\n\"", sexp_bytestringp, AST_BYTESTRING);
  ROUND_TRIP("\"Has hex char \\x0a\"", sexp_bytestringp, AST_BYTESTRING);


  ROUND_TRIP("(seq #\\A #\\B)", sexp_listp, AST_SEQ);
  ROUND_TRIP("(choice a 1 \"string\")", sexp_listp, AST_CHOICE);

  ROUND_TRIP("(repeat 200 300 #\\A)", sexp_listp, AST_REPETITION);
  ROUND_TRIP("(repeat 0 0 \"string\")", sexp_listp, AST_REPETITION);

  ROUND_TRIP("(find #\\x00)", sexp_listp, AST_FIND);
  ROUND_TRIP("(find \"Hi\")", sexp_listp, AST_FIND);
  ROUND_TRIP("(find (repeat 0 1 \"Hi\"))", sexp_listp, AST_FIND);

  ROUND_TRIP("(capture \"capture name\" #\\x00)", sexp_listp, AST_CAPTURE);
  ROUND_TRIP("(capture \"another capture name \" \"Hi\")", sexp_listp, AST_CAPTURE);
  ROUND_TRIP("(capture \"foo\" (repeat 0 1 \"Hi\"))", sexp_listp, AST_CAPTURE);

  ROUND_TRIP("(insert \"name\" \"value\")", sexp_listp, AST_INSERT);
  ROUND_TRIP("(insert \"another name \" \"another value\")", sexp_listp, AST_INSERT);

  ROUND_TRIP("(backref abc.def)", sexp_listp, AST_BACKREF);

  ROUND_TRIP("(brange #\\Z #\\A)", sexp_listp, AST_BYTERANGE);

  ROUND_TRIP("(brange #\\A #\\B)", sexp_listp, AST_BYTERANGE);

  // Can only easily test sets where the bytes are in order
  ROUND_TRIP("(bset #\\x00 #\\A #\\B #\\C #\\Z)", sexp_listp, AST_BYTESET);
  ROUND_TRIP("(bset #\\nul #\\newline #\\space)", sexp_listp, AST_BYTESET);

  ROUND_TRIP("(set #\\u0000 #\\u0001 #\\u0002)", sexp_listp, AST_SET);

  ROUND_TRIP("(choice (seq \"A bytestring\" (backref abc.def)) X)", sexp_listp, AST_CHOICE);

  // 'longword' regex: [[a-z][A-Z]]{14,}
  ROUND_TRIP("(repeat 14 0 "
		     "  (choice (brange #\\A #\\Z) (brange #\\a #\\z)))",
	     sexp_listp,
	     AST_REPETITION);

  ROUND_TRIP("(def num (brange #\\0 #\\9))", sexp_listp, AST_DEFINITION); // [0-9]

  ROUND_TRIP("(mod)", sexp_listp, AST_MODULE);

  ROUND_TRIP("(mod "
		     "  (def verses "
		     "    (seq chapt_and_verse spaces and)))",
	     sexp_listp,
	     AST_MODULE);

  ROUND_TRIP("(mod "
		     "  (def verses "
		     "    (seq chapt_and_verse spaces and))"
		     "  (def verses2 "
		     "    (seq foo bar baz qux)))",
	     sexp_listp,
	     AST_MODULE);




  puts("\n\n");
  confess("TODO", "Write FUZZING (style) tests to build large and varied expressions");

  TEST_END();
}

