/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vmfindtest.c  TESTING pexl_find()                                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "vm.h"
#include "opt.h"

#include "../test.h"

#define PRINTING YES

static
pexl_Position runtest (pexl_Context *C, const char *patname,
		       const char *input, pexl_Position start) {
  int status;
  pexl_Error err;
  pexl_Binary *pkg;
  size_t inputlen;
  pexl_Optims *optims;
  
  // compile pattern with SIMD support
  optims = pexl_default_Optims();
  TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_SIMD));

  TEST_ASSERT(C);
  TEST_ASSERT(optims);

  pexl_Match *match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);

  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg && pexl_Error_value(err) == OK);
  if (PRINTING) pexl_print_Binary(pkg);

  inputlen = strlen(input);
  
  // vmflags bit 0 disables SIMD at runtime when set (1)
  status = pexl_run(pkg, NULL, patname,
		    input, inputlen, start,
		    0,		// vmflags
		    NULL, match);
  TEST_ASSERT(status == 0);

  pexl_Position retval = match->end;
  printf("Test retval (match->end) = %" pexl_Position_FMT "\n", retval);

  pexl_free_Match(match);
  binary_free(pkg);
  pexl_free_Optims(optims);
  
  return retval;
}

int main(int argc, char **argv) {

  pexl_Context *C;
  pexl_Expr *exp;
  pexl_Position pos;
  
  TEST_START(argc, argv);

  TEST_SECTION("Setting up");

  C = pexl_new_Context();
  TEST_ASSERT_NOT_NULL(C);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Find 'a' (should emit IFind1 instruction)");
  
  exp = pexl_find_f(C, pexl_match_string(C, "a"));
  TEST_ASSERT(!pexl_Ref_invalid(pexl_bind(C, exp, "a")));
  pexl_free_Expr(exp);

  pos = runtest(C, "a", "Hello", 0);
  TEST_ASSERT(pos == -1);

  pos = runtest(C, "a", "aHello", 0);
  TEST_ASSERT(pos == 1);

  pos = runtest(C, "a", "Helloa", 0);
  TEST_ASSERT(pos == 6);


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Find '[s-z]' (should emit IFind8 instruction)");
  
  exp = pexl_find_f(C, pexl_match_range(C, 's', 'z'));
  TEST_ASSERT(!pexl_Ref_invalid(pexl_bind(C, exp, "s-z")));
  pexl_free_Expr(exp);

  pos = runtest(C, "s-z", "Hello", 0);
  TEST_ASSERT(pos == -1);

  pos = runtest(C, "s-z", "sHello", 0);
  TEST_ASSERT(pos == 1);

  pos = runtest(C, "s-z", "Hellowxyz", 0);
  TEST_ASSERT(pos == 6);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Find '[q-z]' (should emit IFind instruction)");
  
  exp = pexl_find_f(C, pexl_match_range(C, 'q', 'z'));
  TEST_ASSERT(!pexl_Ref_invalid(pexl_bind(C, exp, "q-z")));
  pexl_free_Expr(exp);

  pos = runtest(C, "q-z", "Hello", 0);
  TEST_ASSERT(pos == -1);

  pos = runtest(C, "q-z", "zHello", 0);
  TEST_ASSERT(pos == 1);

  pos = runtest(C, "q-z", "Hellowxyz", 0);
  TEST_ASSERT(pos == 6);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Multiple charsets in use: Find [a-c] then [a-i] then [a-z]");
  
  exp = pexl_seq_f(C, 
		    pexl_find_f(C, pexl_match_range(C, 'a', 'c')),
		    pexl_seq_f(C,
				pexl_find_f(C, pexl_match_range(C, 'a', 'i')),
				pexl_find_f(C, pexl_match_range(C, 'a', 'z'))));
  TEST_ASSERT(!pexl_Ref_invalid(pexl_bind(C, exp, "multiple")));
  pexl_free_Expr(exp);
		    
  pos = runtest(C, "multiple", "Hello", 0);
  TEST_ASSERT(pos == -1);

  pos = runtest(C, "multiple", "BarHello", 0);
  TEST_ASSERT(pos == 6);

  pos = runtest(C, "multiple", "Bar---i---z---", 0);
  TEST_ASSERT(pos == 11);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Find '[a-z]' repeatedly (should emit IFind instruction)");
  
  exp = pexl_repeat_f(C,
		      pexl_find_f(C, pexl_match_range(C, 'a', 'z')),
		      0, 0);
  TEST_ASSERT(!pexl_Ref_invalid(pexl_bind(C, exp, "a-z repeatedly")));
  pexl_free_Expr(exp);

  const char *input = "    Hello, world!!!!!";
  pexl_Position inputlen = strlen(input);
  printf("Input of length %" pexl_Position_FMT " is: %s\n", inputlen, input);
  pos = runtest(C, "a-z repeatedly", input, 0);
  /* Should end at position one past the last lowercase letter */
  TEST_ASSERT(pos == 16);

  /* ----------------------------------------------------------------------------- */

  pexl_free_Context(C);

  TEST_END();

  return 0;
}

