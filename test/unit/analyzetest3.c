/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest3.c  TESTING analyze.c                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "old-analyze.h"

#include "../test.h"

#define FALSE 0
#define YES 1
#define NO 0

#define CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

#define assert_unspecified_value_at(bt, ref)				\
  do {									\
    b999 = env_get_binding(bt, ref);					\
    /* how can this happen? */					\
    TEST_ASSERT(bindingtype(b999) == Eunspecified_t);			\
  } while (0)

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (BindingTable *bt, pexl_Ref ref) {
  Binding *b = env_get_binding(bt, ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

static void check_node (Pattern *p,
			int hascaptures,
			int itself_hascaptures,
			int nullable,
			int nofail,
			int headfail,
			int unbounded,
			uint32_t expected_min,
			uint32_t expected_max,
			int needfollowflag,
			Charset firstset,
			int getfirstYES,
			int lineno) {
  uint32_t min, max;
  int stat;
  Charset cs;
  unsigned char c;
  Node *node;
  if (!p->fixed) {
    TEST_FAIL_AT_LINE(lineno,
		      "pexl_Error in check_node: pattern %p has no 'fixed' expression tree",
		      (void *) p);
  }
  node = p->fixed->node;
  printf("check_node called on pattern %p from line %d\n", (void *)p, lineno);
  TEST_ASSERT_AT_LINE(lineno, (exp_hascaptures(node) ? 1 : 0) == hascaptures);
  TEST_ASSERT_AT_LINE(lineno, (exp_itself_hascaptures(node) ? 1 : 0) == itself_hascaptures);
  stat = exp_patlen(node, &min, &max);
  if (stat != unbounded) {
    if ((stat != EXP_BOUNDED) && (stat != EXP_UNBOUNDED))
      printf("return value from exp_patlen is error: %d\n", stat);
    else
      printf("pattern is %s, which was not expected", (stat==EXP_BOUNDED) ? "bounded" : "unbounded");
  }
  TEST_ASSERT_AT_LINE(lineno, stat == unbounded);
  if (min != expected_min)
    printf("min len = %u, expected = %u\n", min, expected_min);
  if ((stat == EXP_BOUNDED) && (max != expected_max))
    printf("max len = %u, expected = %u\n", max, expected_max);
  TEST_ASSERT_AT_LINE(lineno, min == expected_min);
  TEST_ASSERT_AT_LINE(lineno, (stat != EXP_BOUNDED) || (max == expected_max));
  TEST_ASSERT_AT_LINE(lineno, (exp_nullable(node) ? 1 : 0) == nullable);
  TEST_ASSERT_AT_LINE(lineno, (exp_nofail(node) ? 1 : 0) == nofail);
  TEST_ASSERT_AT_LINE(lineno, (exp_headfail(node) ? 1 : 0) == headfail);
  TEST_ASSERT_AT_LINE(lineno, (exp_needfollow(node) ? 1 : 0) == needfollowflag);
  stat = exp_getfirst(node, fullset, &cs);
  /* Below: "stat==0 means firstset can be used as test */
  TEST_ASSERT_AT_LINE(lineno, ((stat == 0) == getfirstYES));
  if (stat == 0) {
    /* Now check the contents of cs for 'h' */
    for (c = 0; c < 255; c++)
      if (testchar(firstset.cs, c)) {
	if (!testchar(cs.cs, c)) printf("charset missing %c\n", c);
	TEST_ASSERT_AT_LINE(lineno, testchar(cs.cs, c));
      } else {
	if (testchar(cs.cs, c)) printf("cs should not have %c", c);
	TEST_ASSERT_AT_LINE(lineno, !testchar(cs.cs, c));
      }
  } /* if stat==0 */
}

int main(int argc, char **argv) {

  size_t len;
  uint32_t min, max;
  Binding *b999;
  pexl_Context *C;

  int stat; 
  CompState *cst; 
  Pattern *p;		  /* USE ONLY FOR REFS, not new allocations */
  Pattern *pstr, *p1, *p2, *p3, *p4, *p5; 
  pexl_Env toplevel, env;
  Node *node; 

  pexl_Index i; 

  Charset cs;  
  unsigned char c;  

  pexl_Expr *tstr; 
  pexl_Expr *t1, *t2, *t3, *t4, *t5;  
  pexl_Expr *A, *B, *S;

  pexl_PackageTable *m;  
  pexl_Expr *calltree;
  pexl_Ref ref, refKEEP, refA, refB, refS;

  TEST_START(argc, argv);
  TEST_SECTION("Testing set_metadata");

#define NEW_CONTEXT(envvar) do {		\
    if (C) pexl_free_Context(C);		\
    C = pexl_new_Context();			\
    TEST_ASSERT(C);				\
    envvar = 0;					\
  } while (0);
  
  C = NULL;
  NEW_CONTEXT(toplevel);

  /* pexl_Error checking */

  fprintf(stderr, "Expect a warning:\n");
  stat = set_metadata(NULL);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL);
  tstr = pexl_match_string(C, "hello");
  TEST_ASSERT(tstr);
  pstr = pattern_new(tstr, C->bt, toplevel, &stat);
  TEST_ASSERT(pstr);
  TEST_ASSERT(stat == 0);
  free_expr(tstr);

  TEST_ASSERT_NULL(C->cst);

  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = pstr;
  fprintf(stderr, "Expect a warning:\n");
  stat = set_metadata(cst);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL);  // null graph 

  pstr->env = toplevel;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(pstr, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  fprintf(stderr, "Expect a warning:\n");
  stat = set_metadata(cst);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); // null ordered graph
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Set non-recursive metadata");

  printf("Using literal string 'hello'\n");
  tstr = pexl_match_string(C, "hello");
  TEST_ASSERT(tstr);
  pattern_free(pstr);
  pstr = pattern_new(tstr, C->bt, toplevel, &stat); // will be freed later
  TEST_ASSERT(pstr);
  TEST_ASSERT(stat == 0);
  free_expr(tstr);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = pstr;
  pstr->env = toplevel;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(pstr, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 1); // dependency graph should have 1 vertex

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  charset_fill_zero(&cs);
  setchar(cs.cs, 'h');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);

  printf("Calling pattern for literal string 'hello'\n");
  TEST_ASSERT(pstr);		// precondition for next test
  ref = env_bind(C->bt, toplevel, 123, env_new_value(Epattern_t, 0, (void *)pstr));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  t1 = pexl_call(C, ref);
  TEST_ASSERT(tstr);

  refKEEP = ref;

  /* Wrap the call in a new pattern struct */
  p1 = pattern_new(t1, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  free_expr(t1);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;
  p1->env = toplevel;

  cfgraph_free(cst->g);

  printf("Ensuring that build_cfgraph catches certain errors\n");
  fprintf(stderr, "Expect a warning:\n");
  stat = build_cfgraph(p1, &(cst->g));
  // Have not called fix_open_calls() which sets p1->fixed, so expect an error
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL);

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

/*   printf("Note: toplevel is %p\n", (void *) toplevel); */
/*   printf("Note: toplevel->package is %p\n", (void *) toplevel->package); */
/*   printf("Note: p1->env is %p\n", (void *) p1->env); */
/*   printf("Note: cst->pat->env is %p\n", (void *) cst->pat->env); */

  printf("Using build_cfgraph correctly to operate on TCall\n");
  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); // size of dep. graph should be 2 (p1 and its call target)
  /* Check the contents of the deplist for p1 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p1);   // vertex p1
  TEST_ASSERT(cst->g->deplists[0][1] == p);    // call target
  TEST_ASSERT(cst->g->deplists[0][2] == NULL); // NULL terminator

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p);    // vertex p
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); // NULL terminator

  check_node(p, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'seq'");

  t1 = pexl_call(C, refKEEP);		  /* hello */
  t2 = pexl_match_string(C, "a");	  /* a */
  t3 = pexl_seq(C, t1, t2);		  /* hello" "a */
  TEST_ASSERT(t3);

  /* Wrap the call in a new pattern struct */
  p3 = pattern_new(t3, C->bt, toplevel, &stat);
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  // keep t3 around for later use

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); // size of dep. graph should be 2 (p3 and its call target)
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p3);   // vertex p3
  TEST_ASSERT(cst->g->deplists[0][1] == p);    // call target
  TEST_ASSERT(cst->g->deplists[0][2] == NULL); // NULL terminator

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p);    // vertex p
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); // NULL terminator

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     6, 6, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'choice'");

  /* Choice of t2 and t3, where t3 is a seq that makes a call to p */
  t4 = pexl_choice(C, t2, t3);	/* a" / {"hello" "a"} */
  TEST_ASSERT(t4->node->tag == TChoice);
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  /* Wrap the call in a new pattern struct */
  p4 = pattern_new(t4, C->bt, toplevel, &stat);
  TEST_ASSERT(p4);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p4->env == toplevel);
  free_expr(t4);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); // size of dep. graph should be 2 (p4 and its call target)
  /* Check the contents of the deplist */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p4);   // vertex p4
  TEST_ASSERT(cst->g->deplists[0][1] == p);    // call target
  TEST_ASSERT(cst->g->deplists[0][2] == NULL); // NULL terminator

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p);
  TEST_ASSERT(cst->g->deplists[1][1] == NULL);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  setchar(cs.cs, 'a');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     1, 6, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  /* Choice of t1 and t3, where t1 calls p and p3 is a seq that makes a call to p */
  t4 = pexl_choice(C, t1, t3);		 /* hello" / {"hello" "a"} */
  TEST_ASSERT(t4->node->tag == TChoice);
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  /* Wrap the call in a new pattern struct */
  pattern_free(p4);
  p4 = pattern_new(t4, C->bt, toplevel, &stat);
  TEST_ASSERT(p4);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p4->env == toplevel);
  // keep t4 around for later use

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); // size of dep. graph should be 2 (p4 and its call target)
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
/*     print_pattern(p); */
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p4);   // vertex p4
  TEST_ASSERT(cst->g->deplists[0][1] == p);    // call target
  TEST_ASSERT(cst->g->deplists[0][2] == p);    // second instance of same call target
  TEST_ASSERT(cst->g->deplists[0][3] == NULL); // NULL terminator

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);		/* should get our call target pattern back */

  TEST_ASSERT(cst->g->deplists[1][0] == p);
  TEST_ASSERT(cst->g->deplists[1][1] == NULL);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'h');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     5, 6, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'not'");

  /* t5 = not t4 */
  t5 = pexl_neg_lookahead(C, t4);	      /* !{"hello" / {"hello" "a"}} */
  TEST_ASSERT(t5->node->tag == TNot);
  node = t5->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  free_expr(t4);		// not needed anymore

  /* Wrap the call in a new pattern struct */
  p5 = pattern_new(t5, C->bt, toplevel, &stat);
  TEST_ASSERT(p5);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p5->env == toplevel);
  free_expr(t5);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p5;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p5, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); // size of dep. graph should be 2 (p4 and its call target)
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p5);   // vertex p5
  TEST_ASSERT(cst->g->deplists[0][1] == p);    // call target
  TEST_ASSERT(cst->g->deplists[0][2] == p);    // second instance of same call target
  TEST_ASSERT(cst->g->deplists[0][3] == NULL); // NULL terminator

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p);    /* vertex p */
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); /* NULL terminator */

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);
  pattern_free(p4);
  pattern_free(p5);

  pexl_free_Expr(t1); pexl_free_Expr(t2);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing dynamic expansion of cfgraph (ensure_vertex_space)");

  NEW_CONTEXT(toplevel);
  cst = compstate_new();
  C->cst = cst;

  /* Construct a long call chain that ends in a call to tstr */
  tstr = pexl_match_string(C, "hello"); // the previous exp (which we'll call)
  TEST_ASSERT(tstr);
  p1 = pattern_new(tstr, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);
  free_expr(tstr);

  ref = env_bind(C->bt, toplevel, 900, env_new_value(Epattern_t, 0, (void *)p1));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  for (i = 1; i < (3 * EXP_CFG_INITIAL_SIZE) + 1; i++) {
    t1 = pexl_call(C, ref);
    TEST_ASSERT(t1);
    p1 = pattern_new(t1, C->bt, toplevel, &stat);
    TEST_ASSERT(p1);
    TEST_ASSERT(stat == 0);
    TEST_ASSERT(p1->env == toplevel);
    free_expr(t1);
    ref = env_bind(C->bt, toplevel, i+900, env_new_value(Epattern_t, 0, (void *)p1));
    TEST_ASSERT(!pexl_Ref_invalid(ref));
  }
  t1 = pexl_call(C, ref);
  TEST_ASSERT(t1);
  p1 = pattern_new(t1, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  
  int verbose = 0;
  if (verbose) {
    printf("Toplevel env is: "); pexl_print_Env(toplevel, C);
  }
  printf("pexl_Expr *is: "); pexl_print_Expr(t1, 1, NULL);
  if (verbose) printf("Entire call chain is: \n");
  i = 1;
  t4 = t1;
  while (t4->node->tag == TOpenCall) {
    set_ref_from_node(ref, t4->node);
    p = binding_pattern_value(C->bt, ref);
    if (verbose) printf("%5d: exp %p calls exp %p\n", i, (void *) t4, (void *) p->tree);
    t4 = p->tree; i++;
  }
  if (t4->node->tag == TBytes) 
    printf("%5d: exp %p has tag TBytes\n", i, (void *) t4);
  else {
    printf("%5d: exp %p has unexpected tag %d", i, (void *) t4, t4->node->tag);
    TEST_FAIL("Unexpected tag at the first non-call");
  }
  TEST_ASSERT(i == (3 * EXP_CFG_INITIAL_SIZE) + 2); /* Wrong length of call chain */

  p2 = p1;

  cfgraph_free(cst->g);
  cst->g = NULL;
  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p2;
  reset_compilation_state(cst->pat);
  stat = fix_open_calls(cst);
  printf("fix_open_calls stat = %d\n", stat);
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p2, &(cst->g));
  printf("stat = %d\n", stat);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  printf("size = %u\n", cst->g->next);
  /* check for wrong size of dependency graph */
  TEST_ASSERT(cst->g->next == (3 * EXP_CFG_INITIAL_SIZE) + 2);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  /* patlen 5 is length of "hello */
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED,
	     5, 5, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  free_expr(t3);
  free_expr(t1);
  
  pattern_free(p1);
  cfgraph_free(cst->g);
  cst->g = NULL;

  p1 = NULL; p2 = NULL; p3 = NULL; p4 = NULL; p5 = NULL;
  pstr = NULL; p = NULL;
  t1 = NULL; t2 = NULL; t3 = NULL; t4 = NULL; t5 = NULL;
  tstr = NULL; 

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing dynamic expansion of deplist (ensure_deplist_space)");

  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(toplevel == 0);

  cst = C->cst;

  /* Construct a pattern that calls tstr many times */
  tstr = pexl_match_string(C, "hello");
  TEST_ASSERT(tstr);
  pstr = pattern_new(tstr, C->bt, toplevel, &stat); /* will be freed later */
  TEST_ASSERT(pstr);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(pstr->env == toplevel);
  free_expr(tstr);

  ref = env_bind(C->bt, toplevel, i+900, env_new_value(Epattern_t, 0, (void *)pstr));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  t2 = pexl_call(C, ref);
  TEST_ASSERT(t2);
  for(i = 0; i < (3 * EXP_DEPLIST_INITIAL_SIZE); i++) {
    t2 = pexl_seq_f(C, pexl_call(C, ref), t2);
    TEST_ASSERT(t2);
  }
  printf("Toplevel env is: ");
  pexl_print_Env(toplevel, C);
  if (verbose) {
    printf("pexl_Expr *is: ");
    pexl_print_Expr(t2, 1, NULL);
  }
  p2 = pattern_new(t2, C->bt, toplevel, &stat);	/* must free this explicitly */
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  free_expr(t2);

  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p2;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2);
  i = 1;
  while (cst->g->deplists[0][i]) i++;
  i--;			   /* do not count dl[0], the vertex itself */
  TEST_ASSERT(i == (3 * EXP_DEPLIST_INITIAL_SIZE) + 1); /* wrong size of dependency list */

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  if (verbose) print_pattern(cst->pat, 0, C);
  /* patlen 5 is length of "hello */
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED,
	     5 * (3 * EXP_DEPLIST_INITIAL_SIZE + 1), /* min */
	     5 * (3 * EXP_DEPLIST_INITIAL_SIZE + 1), /* max */
	     NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p2);  
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookahead'");

  t1 = pexl_match_string(C, "A");
  t2 = pexl_lookahead(C, t1);
  TEST_ASSERT(t2->len == 1 + t1->len); /* check for wrong size tree */
  TEST_ASSERT(t2->node->tag == TAhead); /* TAhead */
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  /* p2 is (lookahead (fromstring "A")) */
  p2 = pattern_new(t2, C->bt, toplevel, &stat);	/* will be freed later */
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  free_expr(t2);

  ref = env_bind(C->bt, toplevel, 85000, env_new_value(Epattern_t, 0, (void *)p2));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  TEST_ASSERT(!p2->fixed);	/* not yet run fix_open_calls, which is precondition for next test */

  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(p2, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     CAN_FAIL, HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  t3 = pexl_call(C, ref);
  TEST_ASSERT(t3);
  
  p3 = pattern_new(t3, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  free_expr(t3);

  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2);
  i = 1;
  while (cst->g->deplists[0][i]) i++;
  i--;			   /* do not count dl[0], the vertex itself */
  TEST_ASSERT(i == 1);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     CAN_FAIL, HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'rep'");

  pexl_free_Expr(t1);

#define repetition_node_size(min,max,expsize) \
  ((max == 0) ? ((min+1)*(expsize+1)) : ((max-min)*(expsize+1) + max*(expsize+1)))

  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 0); /* "A"* */
  TEST_ASSERT(t2->len == repetition_node_size(0, 0, t1->len));
  TEST_ASSERT(t2->node->tag == TRep);
  node = t2->node;
  TEST_ASSERT(node);		 /* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED); /* expected UNBOUNDED */
  TEST_ASSERT((min == 0));
  TEST_ASSERT(exp_nullable(node) == 1);
  TEST_ASSERT(exp_nofail(node) == 1);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(exp_needfollow(node));	/* exp would benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 1);	/* firstset cannot be used as test, exp accepts epsilon */
  /* Now check the contents of cs, which should be a full set */
  for (c = 0; c < 255; c++) 
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all chars */

  p2 = pattern_new(t2, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  free_expr(t2);

  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     NOFAIL, NO_HEADFAIL, EXP_UNBOUNDED,
	     0, 123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  t3 = pexl_repeat(C, t1, 3, 0);       /* "A"{3,} */
  TEST_ASSERT(t3->len == repetition_node_size(3, 0, t1->len));
  TEST_ASSERT(t3->node->tag == TSeq);
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED);
  TEST_ASSERT((min == 3));
  TEST_ASSERT(exp_nullable(node) == 0);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would benefit from follow set */
  TEST_ASSERT(exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset can be used as test, does not accept epsilon */
  TEST_ASSERT(stat == 0);

  p3 = pattern_new(t3, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  free_expr(t3);

  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NOT_HEADFAIL, EXP_UNBOUNDED,
	     3, 123, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pexl_free_Expr(t1);
  
  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 1);       /* "A"? ==> "A" / TTrue */
  TEST_ASSERT(t2->len == 2 + t1->len);
  TEST_ASSERT(t2->node->tag == TChoice);
  node = t2->node;
  TEST_ASSERT(node);		       /* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 1));
  TEST_ASSERT(exp_nullable(node) == 1);
  TEST_ASSERT(exp_nofail(node) == 1);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* exp would benefit from follow set */
  TEST_ASSERT(exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++) 
    TEST_ASSERT(testchar(cs.cs, c));

  pattern_free(p2);
  pattern_free(p3);

  p2 = pattern_new(t2, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  free_expr(t2);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     NOFAIL, NOT_HEADFAIL, EXP_BOUNDED,
	     0, 1, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  t3 = pexl_repeat(C, t1, 2, 6);	/* "A"{2,6} */
  TEST_ASSERT(t3->len == repetition_node_size(2, 6, t1->len));
  TEST_ASSERT(t3->node->tag == TSeq);
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 6));
  TEST_ASSERT(exp_nullable(node) == 0);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would benefit from follow set */
  TEST_ASSERT(exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset can be used as test, exp does not accept epsilon */
  TEST_ASSERT(stat == 0);
  /* Now check the contents of cs, which should contain only A */
  for (c = 0; c < 255; c++) 
    if (c == 'A') 
      TEST_ASSERT(testchar(cs.cs, c));
    else 
      TEST_ASSERT(!testchar(cs.cs, c));
  pattern_free(p2);

  p3 = pattern_new(t3, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  free_expr(t3);
  
  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);

  charset_fill_zero(&cs);
  setchar(cs.cs, 'A');

  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     2, 6, NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p3);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookbehind'");

  pexl_free_Expr(t1);

  printf("Look back at string literal\n");
  t1 = pexl_match_string(C, "A");
  t2 = pexl_lookbehind_f(C, t1);
  TEST_ASSERT(t2->len == 2);
  TEST_ASSERT(t2->node->tag == TBehind);
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(exp_nullable(node) == 1);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++) 
    TEST_ASSERT(testchar(cs.cs, c));

  p2 = pattern_new(t2, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  free_expr(t2);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  printf("Look back at \"A\"? which has variable length\n");
  t3 = pexl_lookbehind_f(C, pexl_repeat_f(C, pexl_match_string(C, "A"), 0, 1));
  TEST_ASSERT(t3);		/* the check for fixedlen happens later */
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  printf("This exp will fail to compile, because the target of the lookbehind has variable length.\n");

  TEST_ASSERT(exp_nullable(node) == 1);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++) 
    TEST_ASSERT(testchar(cs.cs, c));

  pattern_free(p2);
  
  p3 = pattern_new(t3, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  free_expr(t3);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  charset_fill_zero(&cs);
  setchar(cs.cs, 'A');
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);

  printf("Look back at \"AB\"/\"CD\" which has length 2\n");
  t3 = pexl_choice_f(C, pexl_match_string(C, "AB"), pexl_match_string(C, "CD"));
  t4 = pexl_lookbehind_f(C, t3);
  TEST_ASSERT(t4); /* Ths check for acceptable lookbehind targets is at compile time */
  TEST_ASSERT(t4->node->tag == TBehind);
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));

  printf("This exp will compile, because the target of the lookbehind has fixed length.\n");
  stat = exp_patlen(child1(node), &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 2));

  TEST_ASSERT(exp_nullable(node) == 1);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++) 
    TEST_ASSERT(testchar(cs.cs, c));

  p4 = pattern_new(t4, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p4);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p4->env == toplevel);
  free_expr(t4);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p4, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     CAN_FAIL, NO_HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);
  
  pattern_free(p4);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexlCapture");

  t1 = pexl_match_string(C, "AB");
  t2 = pexl_capture_f(C, "hi", t1);
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(node));
  TEST_ASSERT(exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 2));

  TEST_ASSERT(exp_nullable(node) == 0);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset can be used as test, does not accept epsilon */
  TEST_ASSERT(stat == 0);
  /* Now check the contents of cs, which should contain only A */
  for (c = 0; c < 255; c++) 
    if (c == 'A') 
      TEST_ASSERT(testchar(cs.cs, c));
    else 
      TEST_ASSERT(!testchar(cs.cs, c));

  p2 = pattern_new(t2, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  // keep t2 around for later use

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, CAPTURES, CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED,
	     2, 2, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  t3 = pexl_seq(C, t2, t2);
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(node));
  TEST_ASSERT(exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 4) && (max == 4));

  TEST_ASSERT(exp_nullable(node) == 0);
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset can be used as test, does not accept epsilon */
  TEST_ASSERT(stat == 0);
  /* Now check the contents of cs, which should contain only A */
  for (c = 0; c < 255; c++) 
    if (c == 'A') 
      TEST_ASSERT(testchar(cs.cs, c));
    else 
      TEST_ASSERT(!testchar(cs.cs, c));

  pattern_free(p2);

  p3 = pattern_new(t3, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  free_expr(t3);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  check_node(cst->pat, CAPTURES, CAPTURES, NOT_NULLABLE,
	     CAN_FAIL, NOT_HEADFAIL, EXP_BOUNDED,
	     4, 4, NO_NEEDFOLLOW, cs, FIRSTSET, __LINE__);

  pattern_free(p3);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Constant captures");

  pexl_Expr *consttree = pexl_insert(C, "name", "constant string");
  TEST_ASSERT(consttree);
  const char *tmp = context_retrieve(C, consttree->node->b.n, &len);
  TEST_ASSERT(len == 4);
  TEST_ASSERT(memcmp(tmp, "name", len) == 0); /* name string interned */
  tmp = context_retrieve(C, consttree->node->a.stringtab_handle, &len);
  TEST_ASSERT(len == strlen("constant string"));
  TEST_ASSERT(memcmp(tmp, "constant string", len) == 0); /* data string interned */

  p3 = pattern_new(consttree, C->bt, toplevel, &stat);	/* must free explicitly */
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p3->env == toplevel);
  free_expr(consttree);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  print_pattern(cst->pat, 0, C);
  memset(&cs, 0, CHARSETSIZE);
  check_node(cst->pat, CAPTURES, CAPTURES, NULLABLE,
	     NOFAIL, NOT_HEADFAIL, EXP_BOUNDED,
	     0, 0, NO_NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  pattern_free(p3);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexl_References");

  m = packagetable_new(1);
  TEST_ASSERT(m);

  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT(toplevel == 0);

  cst = compstate_new();
  C->cst = cst;

  ref = env_bind(C->bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  refKEEP = ref;

  ref = env_bind(C->bt, toplevel, 1, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(ref == 1);	    /* second binding is at index 1 */

  calltree = pexl_call(C, ref);
  TEST_ASSERT(calltree);
  TEST_ASSERT(calltree->node->a.bt == C->bt);
  TEST_ASSERT(calltree->node->b.index == ref);

  /* Check that we follow the reference correctly */
  env = env_new(C->bt, toplevel);
  p1 = pattern_new(calltree, C->bt, env, &stat); /* must free explicitly */
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == env); /* compilation env is child of 'toplevel' */
  free_expr(calltree);

  printf("\nCreating pattern %p that references a binding in a parent env\n",
	 (void *) p1);
  printf("ref = %d\n", ref);
  print_pattern(p1, 0, C); 
  
  TEST_ASSERT(!cst->pat);	/* should be NULL in a fresh compstate */
  cst->pat = p1;

  reset_compilation_state(cst->pat);  

  fprintf(stderr, "Expect a warning:\n");
  stat = fix_open_calls(cst);  
  /* Error is that ref points to unspecified (not a pattern) */
  TEST_ASSERT(stat == PEXL_EXP__ERR_NO_PATTERN);

  printf("Target of call is %p:\n", (void *) binding_pattern_value(C->bt, ref));
  print_pattern(binding_pattern_value(C->bt, ref), 0, C);

  /* Change the target of the ref to a pattern (from an unspecified value) */
  tstr = pexl_match_string(C, "hello");
  pstr = pattern_new(tstr, C->bt, toplevel, &stat); /* will be freed later */
  TEST_ASSERT(pstr);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(pstr->env == toplevel);
  free_expr(tstr);

  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)pstr));
  TEST_ASSERT(stat == 0);

  printf("Target of call AFTER REBIND is %p:\n", (void *) binding_pattern_value(C->bt, ref));
  print_pattern(binding_pattern_value(C->bt, ref), 0, C);

  calltree = pexl_call(C, ref);
  TEST_ASSERT(calltree);

  p2 = pattern_new(calltree, C->bt, env, &stat);
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == env);
  free_expr(calltree);

  printf("New pattern in toplevel is %p:\n", (void *) p2);
  print_pattern(p2, 0, C);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;

  cst->pat = p2;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  /* Ref points to a pattern, so the open call resolves OK */
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  printf("\nCompiling pattern %p in toplevel environment %d:\n", cst->pat, cst->pat->env);
  print_pattern(cst->pat, 0, C);
  printf("which calls this pattern %p (in environment %d):\n",
	 (void *) binding_pattern_value(C->bt, ref), binding_pattern_value(C->bt, ref)->env);
  print_pattern(binding_pattern_value(C->bt, ref), 0, C);

  /* Should succeed because the tstr pattern has metadata set */
  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  pattern_free(p2);

  /* Now try calling a pattern that calls "hello" in another pkg */
  t1 = pexl_call(C, ref);
  TEST_ASSERT(t1);
  pattern_free(p1);
  p1 = pattern_new(t1, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);
  free_expr(t1);

  assert_unspecified_value_at(C->bt, refKEEP); 
  stat = env_rebind(C->bt, refKEEP, env_new_value(Epattern_t, 0, (void *)p1));
  TEST_ASSERT(stat == 0);

  /*
    p1 is a pattern (to compile in toplevel) that calls refKEEP (in pkg),
    which calls ref (in pkg) which is "hello"
  */
  calltree = pexl_call(C, refKEEP);
  pexl_print_Expr(calltree, 0, NULL); 
  TEST_ASSERT(calltree);

  /*
    p1 is bound, and will be freed with toplevel, so we can reuse
    the p1 variable here
  */
  p1 = pattern_new(calltree, C->bt, env, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == env);	/* env is child of 'toplevel' */
  free_expr(calltree);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;

  printf("\nCompiling pattern %p in toplevel environment %d:\n", (void *) cst->pat, cst->pat->env);
  print_pattern(cst->pat, 0, C);
  printf("which calls this pattern %p (in environment %d):\n",
	 (void *) binding_pattern_value(C->bt, refKEEP), binding_pattern_value(C->bt, refKEEP)->env);
  print_pattern(binding_pattern_value(C->bt, refKEEP), 0, C);
  printf("which calls this pattern %p (in environment %d):\n",
	 (void *) binding_pattern_value(C->bt, ref), binding_pattern_value(C->bt, ref)->env);
  print_pattern(binding_pattern_value(C->bt, ref), 0, C);

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  /* Ref points to a pattern, so the open call resolves OK */
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  /* Should succeed because the tstr pattern has metadata set */
  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  /* We are going to continue to use p1, below */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Scope test (reusing results of prior test)");

  // Prep for test: env is a child of toplevel
  TEST_ASSERT(env_get_parent(C->bt, env) == toplevel);

  /* Bind a pattern in 'env' */
  ref = env_bind(C->bt, env, 0, env_new_value(Epattern_t, 0, (void *)p1));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  /* Now create a call to the pattern in 'env' */
  calltree = pexl_call(C, ref);
  TEST_ASSERT(calltree);

  /*
    Bind the new pattern in 'toplevel'.  Later, we should discover
    that we have a pattern in toplevel that is calling a pattern in a
    child environment, which is not allowable under lexical scoping
    rules.
  */
  p1 = pattern_new(calltree, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);
  free_expr(calltree);

  /* Prepare for fix_open_calls, where our error should be discovered */
  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  /* Should produce a scope violation error */
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == PEXL_EXP__ERR_SCOPE);

  pattern_free(p1);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Non-recursive block using anonymous exps");

  pexl_free_Context(C);
  C = pexl_new_Context();

  refA = env_bind(C->bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refS = env_bind(C->bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  A = pexl_call(C, refS);	/* A -> S */
  S = pexl_match_epsilon(C);	/* S -> true */

  node = A->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_patlen(node, &min, &max) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nullable(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nofail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_needfollow(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  stat = exp_getfirst(node, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);

  p1 = pattern_new(A, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);
  free_expr(A);

  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  fprintf(stderr, "Expect a warning:\n");
  stat = fix_open_calls(cst);
  /* Error is that ref points to unspecified (not a pattern */
  TEST_ASSERT(stat == PEXL_EXP__ERR_NO_PATTERN);

  pattern_free(p1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Recursive block using anonymous exps");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  pexl_free_Context(C);
  C = pexl_new_Context();

  refA = env_bind(C->bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  A = pexl_call(C, refA);     /* A -> A */
  
  p1 = pattern_new(A, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);
  free_expr(A);

  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  fprintf(stderr, "Expect a warning:\n");
  stat = fix_open_calls(cst);
  /* Error is that ref points to unspecified (not a pattern) */
  TEST_ASSERT(stat == PEXL_EXP__ERR_NO_PATTERN);

  fprintf(stderr, "Expect a warning:\n");
  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  
  pattern_free(p1);

  printf("Trying mutually left-recursive rules that are not accessible from start"); 

  pexl_free_Context(C);
  C = pexl_new_Context();

  refA = env_bind(C->bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refB = env_bind(C->bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refB));
  A = pexl_call(C, refB);	/* A -> B */
  B = pexl_call(C, refA);	/* B -> A */

  printf("Reminder, there is no left recursion check yet.\n");
  
  printf("pexl_Expr *at refA: "); pexl_print_Expr(A, 0, NULL);
  node = A->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_patlen(node, &min, &max) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nullable(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nofail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_needfollow(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  stat = exp_getfirst(node, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  
  printf("pexl_Expr *at refB: "); pexl_print_Expr(B, 0, NULL);
  node = B->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_patlen(node, &min, &max) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nullable(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nofail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_needfollow(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  stat = exp_getfirst(node, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);

  p1 = pattern_new(B, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);

  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  fprintf(stderr, "Expect a warning:\n");
  stat = fix_open_calls(cst);
  /* Error is that ref points to unspecified (not a pattern */
  TEST_ASSERT(stat == PEXL_EXP__ERR_NO_PATTERN);

  fprintf(stderr, "Expect a warning:\n");
  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  
  fprintf(stderr, "Expect a warning:\n");
  stat = sort_cfgraph(cst->g);
  /* Error is that graph does not exist */
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL);

  fprintf(stderr, "Expect a warning:\n");
  stat = set_metadata(cst);
  /* Error is that graph does not exist */
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL);

  /* Bind an actual pattern at refB */
  assert_unspecified_value_at(C->bt, refB);
  stat = env_rebind(C->bt, refB, env_new_value(Epattern_t, 0, (void *)p1));
  TEST_ASSERT(stat == 0);

  /* This is the only left recursive result in this test file */
  printf("Now binding a pattern at A, so that we get mutual left recursion A->B and B->A\n");
  p2 = pattern_new(A, C->bt, toplevel, &stat);
  TEST_ASSERT(p2);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p2->env == toplevel);
  free_expr(A);

  assert_unspecified_value_at(C->bt, refA);
  stat = env_rebind(C->bt, refA, env_new_value(Epattern_t, 0, (void *)p2));

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p2;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p2, &(cst->g));
  TEST_ASSERT(stat == 0);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);

  /* p1, p2 are bound and will be freed with toplevel */

  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();

  stat = context_intern(C, "A", 1);
  TEST_ASSERT(stat >= 0);
  ref = env_bind(C->bt, toplevel, stat, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		   pexl_seq_f(C, 
			     pexl_match_string(C, "a"),
			     pexl_seq_f(C, 
				       pexl_call(C, ref),
				       pexl_match_string(C, "b"))),
		   pexl_match_epsilon(C));

  TEST_ASSERT(A);
  printf("A is: \n"); 
  pexl_print_Expr(A, 0, C); 

  node = A->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  TEST_ASSERT(exp_patlen(node, &min, &max) == PEXL_EXP__ERR_OPENFAIL);
  /* This exp is nullable, and we never get to the OPEN CALL */
  TEST_ASSERT(exp_nullable(node) == 1);
  TEST_ASSERT(exp_nofail(node) == 1);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  /* This exp benefits from needfollow, and we never get to the OPEN CALL */
  TEST_ASSERT(exp_needfollow(node) == 1);
  stat = exp_getfirst(node, fullset, &cs); 
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p1 = pattern_new(A, C->bt, toplevel, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p1->env == toplevel);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p1));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  memset(&cs, 0, CHARSETSIZE);
  setchar(cs.cs, 'A');
  /*
    Recursive patterns automatically set to unbounded (conservative),
    with min of 0 (also conservative)
  */
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE,
	     NOFAIL, NOT_HEADFAIL, EXP_UNBOUNDED,
	     0, 123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__);

  /* p1 will be freed with Context */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with one binding to unspecified");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();

  stat = context_intern(C, "A", 1);
  TEST_ASSERT(stat >= 0);
  ref = env_bind(C->bt, toplevel, stat, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref)); /* bind of unspecified value should work */

  stat = context_intern(C, "A", 1);
  TEST_ASSERT(stat >= 0);
  pexl_free_Expr(S);
  S = pexl_call(C, ref);  /* S -> A */
  TEST_ASSERT(S);         /* Succeeds because value bound to A is never examined */

  node = S->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_patlen(node, &min, &max) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nullable(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_nofail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  TEST_ASSERT(exp_needfollow(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning:\n");
  stat = exp_getfirst(node, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);

  pexl_free_Expr(S);
  packagetable_free(m); 
  pexl_free_Context(C);

  /* ----------------------------------------------------------------------------- */
  printf("\nTODO: Add a test that includes a call to another package, one which is already compiled\n");

  TEST_END();

  return 0;
}

