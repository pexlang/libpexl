/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  repeat_nullable_test.c                                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>             /* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"

#include "../test.h"

#define PRINTING NO

/* Return 0 if match components agree with expected values */
static int check_match(pexl_Match* m, int matched, ssize_t end, 
                       int match_data_expected, const char* input) {
    int result = 0;
    if ((m->end != -1) != matched) {
      printf("pexl_Error: expected match=%s, found match=%s, for input: %s\n",
             (matched ? "true" : "false"),
             ((m->end != -1) ? "true" : "false"), input);
      result = 1;       
    }
    if (m->end != -1) {
      if (m->end != end) {
        printf("pexl_Error: expected end position=%lu, found end=%lu, for input: %s\n",
               end, m->end, input);
        result = 2;     
      }
    }
    switch (m->encoder_id) {
    case PEXL_TREE_ENCODER: {
      if (!m->data) {
        if (match_data_expected != 0) {
          confess("test", "expected non-empty tree for input %s", input);
          result = 5;
        }
      } else {
        if (((pexl_MatchTree *)m->data)->size != (size_t) match_data_expected) {
          printf("pexl_Error: expected %d nodes, got tree with %zu nodes, for input: %s\n",
                 match_data_expected, ((pexl_MatchTree *)m->data)->size, input);
          result = 5;   
        }
      }
      break;
    }
    default:
      break;
    } /* switch */
    return result;
}

static
pexl_Binary *compile_no_bind (pexl_Context *C,
			      pexl_Ref ref,
			      pexl_Optims *optims,
			      pexl_Error *err) {

  assert(err);
  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);
  TEST_ASSERT(pkg && (pexl_Error_value(*err) == PEXL_OK));

#if 0
  // Set entrypoint based on the ref we were given
  Binding *b = env_get_binding(C->bt, ref);
  TEST_ASSERT(b && (bindingtype(b) == Epattern_t));
  Pattern *pat = (Pattern *) b->val.ptr;
  TEST_ASSERT(pat);
  TEST_ASSERT(pat->entrypoint >= 0);
  pkg->ep = pat->entrypoint;
#endif
  return pkg;
}

static
pexl_Binary *compile_and_destroy (pexl_Context *C,
				  pexl_Optims *optims,
				  pexl_Expr *exp,
				  pexl_Env env,
				  pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  pexl_free_Expr(exp);
  return compile_no_bind(C, ref, optims, err);
}
			 
static void test (pexl_Binary *pkg,
		  const char *input, 
		  int match_expected,
		  int leftover,
		  int match_nodes_expected) {

  int stat;
  pexl_Match *match;
  ssize_t end_expected = strlen(input) - leftover;

  size_t inputlen = strlen(input);
  printf("INPUT: \"%s\"\n", input);

  TEST_ASSERT(pkg);

  if (PRINTING) {
    /* Run with debug encoder, to see what captures were generated  */
    match = pexl_new_Match(PEXL_DEBUG_ENCODER);
    TEST_ASSERT(match);

    stat = vm_start(pkg, NULL, main_entrypoint(pkg),
                    input, inputlen, 0, inputlen, 0,
                    NULL, match);
    TEST_ASSERT(stat == 0); 
    pexl_free_Match(match);
  }
  
  /* Run again with tree encoder, so we can debug it */
  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT(match);

  stat = vm_start(pkg, NULL, main_entrypoint(pkg), 
                  input, inputlen, 0, inputlen, 0,
                  NULL, match);
  TEST_ASSERT(stat == 0);

  if (PRINTING) {
    pexl_print_Match_summary(match);
    pexl_print_Match_data(match);
    printf("\n");
  }
  stat = check_match(match, match_expected, end_expected, match_nodes_expected, input);
  TEST_ASSERT(stat == 0);

  pexl_free_Match(match);
}

static pexl_Binary *bind_and_compile (pexl_Context *C,
				      pexl_Expr *exp) {
  pexl_Error err;
  pexl_Binary *pkg;
  pexl_Optims *optims;

  TEST_ASSERT(exp);

  optims = pexl_addopt_simd(NULL, 1);
  TEST_ASSERT(optims);
  optims = pexl_addopt_tro(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_inline(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_unroll(optims, 2);
  TEST_ASSERT(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);

  // Compile in root env (id 0)
  pkg = compile_and_destroy(C, optims, exp, 0, &err);
  TEST_ASSERT(pkg && (pexl_Error_value(err) == 0));

  pexl_free_Optims(optims);
  if (PRINTING) pexl_print_Binary(pkg);

  return pkg;
}


int main(int argc, const char **argv) {

  int n;
  pexl_Context *C;
  pexl_Env env; 

  pexl_Expr *exp;
  pexl_Ref ref;
  pexl_Binary *pkg;

  TEST_START(argc, argv);

  C = pexl_new_Context();
  TEST_ASSERT(C);

  /* First let's test repeat of a non-nullable exp */

  TEST_SECTION("Checking repetitions of 0 or more occurrences, i.e. star");

  printf("--- Case (0): repeated exp converts to charset; ISpan issued\n\n");
  printf("--- Test case \"X\"*\n");
  exp = pexl_repeat_f(C, pexl_match_string(C, "X"), 0, 0);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXXXXXX", true, 0, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(\"X\"*)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes", pexl_repeat_f(C, pexl_match_string(C, "X"), 0, 0));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXXXXXX", true, 0, 2);

  pexl_free_Binary(pkg);

  /* Now try nullable expressions */

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon)*\n");
  //  pexl_free_Expr(exp);
  exp = pexl_repeat_f(C, pexl_choice_f(C, pexl_match_string(C, "X"), pexl_match_epsilon(C)), 0, 0);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXXXXXX", true, 0, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon)*)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
              pexl_repeat_f(C,
                 pexl_choice_f(C,
                    pexl_match_string(C, "X"),
                    pexl_match_epsilon(C)),
                 0, 0));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXXXXXX", true, 0, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon)* / epsilon \n");
  //  pexl_free_Expr(exp);
  exp = pexl_choice_f(C,
            pexl_repeat_f(C,
               pexl_choice_f(C,
                  pexl_match_string(C, "X"),
                  pexl_match_epsilon(C)),
               0, 0),
            pexl_match_epsilon(C));

  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXXXXXX", true, 0, 0);

  pexl_free_Binary(pkg);
  
  printf("--- Test case CAPTURE((\"X\"/epsilon)* / epsilon)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
           pexl_choice_f(C,
              pexl_repeat_f(C,
                 pexl_choice_f(C,
                    pexl_match_string(C, "X"),
                    pexl_match_epsilon(C)),
                 0, 0),
              pexl_match_epsilon(C)));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXXXXXX", true, 0, 2);

  pexl_free_Binary(pkg);
  //  pexl_free_Expr(exp);
  
  /*************************************************************************/
  /* Now with repetitions of "up to n occurrences" of nullable expressions */
  /*************************************************************************/

  TEST_SECTION("Checking repetitions of at most n occurrences");

  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case \"X\"{0,2}\n");
  exp = pexl_repeat_f(C, pexl_match_string(C, "X"), 0, 2);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 2, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(\"X\"{0,2})\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
                       pexl_repeat_f(C, pexl_match_string(C, "X"), 0, 2));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 2, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon){0,2}\n");
  //  pexl_free_Expr(exp);
  exp = pexl_repeat_f(C,
                      pexl_choice_f(C,
                                    pexl_match_string(C, "X"),
                                    pexl_match_epsilon(C)),
                      0, 2);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 2, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon){0,2})\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
                       pexl_repeat_f(C,
                                     pexl_choice_f(C,
                                                   pexl_match_string(C, "X"),
                                                   pexl_match_epsilon(C)),
                                     0, 2));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 2, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon){0,2} / epsilon \n");
  //  pexl_free_Expr(exp);
  exp = pexl_choice_f(C,
                     pexl_repeat_f(C,
                                   pexl_choice_f(C,
                                                 pexl_match_string(C, "X"),
                                                 pexl_match_epsilon(C)),
                                   0, 2),
                     pexl_match_epsilon(C));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 2, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon){0,2} / epsilon)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
           pexl_choice_f(C,
              pexl_repeat_f(C,
                 pexl_choice_f(C,
                    pexl_match_string(C, "X"),
                    pexl_match_epsilon(C)),
              0, 2),
           pexl_match_epsilon(C)));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 2, 2);

  pexl_free_Binary(pkg);
  //  pexl_free_Expr(exp);

  /************************************************************************/
  /* Now with repetitions of "up to 1 occurrence" of nullable expressions */
  /************************************************************************/

  TEST_SECTION("Checking repetitions of at most 1 occurrence");

  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case \"X\"?\n");
  exp = pexl_repeat_f(C, pexl_match_string(C, "X"), 0, 1);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 4, 0);
  test(pkg, "XXXX", true, 3, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(\"X\")?\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
                       pexl_repeat_f(C,
                                     pexl_match_string(C, "X"),
                                     0, 1));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 4, 2);
  test(pkg, "XXXX", true, 3, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon)?\n");
  //  pexl_free_Expr(exp);
  exp = pexl_repeat_f(C,
                      pexl_choice_f(C,
                                    pexl_match_string(C, "X"),
                                    pexl_match_epsilon(C)),
                      0, 1);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 4, 0);
  test(pkg, "XXXX", true, 3, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon))?\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
                       pexl_repeat_f(C,
                                     pexl_choice_f(C,
                                                   pexl_match_string(C, "X"),
                                                   pexl_match_epsilon(C)),
                                     0, 1));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 4, 2);
  test(pkg, "XXXX", true, 3, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon)? / epsilon \n");
  //  pexl_free_Expr(exp);
  exp = pexl_choice_f(C,
                     pexl_repeat_f(C,
                                   pexl_choice_f(C,
                                                 pexl_match_string(C, "X"),
                                                 pexl_match_epsilon(C)),
                                   0, 1),
                      pexl_match_epsilon(C));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 4, 0);
  test(pkg, "XXXX", true, 3, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon) / epsilon)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
               pexl_choice_f(C,
                             pexl_repeat_f(C,
                                           pexl_choice_f(C,
                                                         pexl_match_string(C, "X"),
                                                         pexl_match_epsilon(C)),
                                           0, 1),
                             pexl_match_epsilon(C)));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 4, 2);
  test(pkg, "XXXX", true, 3, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);
  //pexl_free_Expr(exp);

  /***************************************************************************/
  /* Now with repetitions of "n or more occurrences" of nullable expressions */
  /***************************************************************************/

  TEST_SECTION("Checking repetitions of n or more occurrences");

  n = 3;
  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case \"X\"{n,}\n");
  exp = pexl_repeat_f(C, pexl_match_string(C, "X"), n, 0);
  pkg = bind_and_compile(C, exp);
  TEST_ASSERT(pkg);

  test(pkg, "", false, 0, 0);
  test(pkg, "X", false, 1, 0);
  test(pkg, "XXabc", false, 5, 0);
  test(pkg, "XXXX", true, 0, 0);
  test(pkg, "a", false, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(\"X\"){n,}\n");
  //pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes", pexl_repeat_f(C, pexl_match_string(C, "X"), n, 0));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", false, 0, 0);
  test(pkg, "X", false, 1, 0);
  test(pkg, "XXabc", false, 5, 0);
  test(pkg, "XXXX", true, 0, 2);
  test(pkg, "a", false, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (\"X\"/epsilon){n,}\n");
  //pexl_free_Expr(exp);
  exp = pexl_repeat_f(C,
                      pexl_choice_f(C,
                                    pexl_match_string(C, "X"),
                                    pexl_match_epsilon(C)),
                      n, 0);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 0, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon)){n,}\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
                       pexl_repeat_f(C,
                                     pexl_choice_f(C,
                                                   pexl_match_string(C, "X"),
                                                   pexl_match_epsilon(C)),
                                     n, 0));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 0, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (\"X\"/epsilon){n,} / epsilon \n");
  //  pexl_free_Expr(exp);
  exp = pexl_choice_f(C,
                     pexl_repeat_f(C,
                                  pexl_choice_f(C,
                                               pexl_match_string(C, "X"),
                                               pexl_match_epsilon(C)),
                                  n, 0),
                     pexl_match_epsilon(C));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 0, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE((\"X\"/epsilon) / epsilon)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
                      pexl_choice_f(C,
                                   pexl_repeat_f(C,
                                                pexl_choice_f(C,
                                                             pexl_match_string(C, "X"),
                                                             pexl_match_epsilon(C)),
                                                n, 0),
                                   pexl_match_epsilon(C)));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 0, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);
  //  pexl_free_Expr(exp);

  /***************************************/
  /* Now with repetitions of repetitions */
  /***************************************/

  TEST_SECTION("Checking repetitions of repetitions");

  n = 3;
  printf("--- Case (0): repeated exp converts to charset; ITestChar issued\n\n");
  printf("--- Test case ('X'?)*\n");
  exp = pexl_repeat_f(C,
                      pexl_repeat_f(C, pexl_match_string(C, "X"), 0, 1),
                      0, 0);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 0, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(('X'?)*)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes", 
           pexl_repeat_f(C,
              pexl_repeat_f(C, 
                 pexl_match_string(C, "X"), 
                 0, 1),
           0, 0));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 0, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (1): special case does not apply because exp is neither headfail nor non-nullable\n\n");
  printf("--- Case (2): general case, no optimizations\n\n");
  printf("--- Test case (('X'/epsilon){n,})*\n");
  //  pexl_free_Expr(exp);
  exp = pexl_repeat_f(C, 
             pexl_repeat_f(C,
                pexl_choice_f(C, 
                   pexl_match_string(C, "X"), 
                   pexl_match_epsilon(C)), 
                n, 0),
             0, 0);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 0, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(\"exes\", ('X'/epsilon){n,})*\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes", 
           pexl_repeat_f(C, 
              pexl_repeat_f(C,
                 pexl_choice_f(C,
                    pexl_match_string(C, "X"),
                    pexl_match_epsilon(C)), 
                 n, 0),
              0, 0));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 0, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);

  printf("--- Case (3): general case, optimization due to choice on stack\n\n");
  printf("--- Test case (('X'/epsilon){n,})* / epsilon \n");
  //  pexl_free_Expr(exp);
  exp = pexl_choice_f(C, 
           pexl_repeat_f(C, 
              pexl_repeat_f(C, 
                 pexl_choice_f(C, 
                    pexl_match_string(C, "X"), 
                    pexl_match_epsilon(C)), 
                 n, 0),
              0, 0),
        pexl_match_epsilon(C));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 0);
  test(pkg, "X", true, 0, 0);
  test(pkg, "XXabc", true, 3, 0);
  test(pkg, "XXXX", true, 0, 0);
  test(pkg, "a", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE(\"exes\", (('X'/epsilon){n,})* / epsilon)\n");
  //  pexl_free_Expr(exp);
  exp = pexl_capture_f(C, "exes",
           pexl_choice_f(C, 
              pexl_repeat_f(C, 
                 pexl_repeat_f(C, 
                    pexl_choice_f(C, 
                       pexl_match_string(C, "X"), 
                       pexl_match_epsilon(C)), 
                    n, 0),
                 0, 0),
              pexl_match_epsilon(C)));
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "X", true, 0, 2);
  test(pkg, "XXabc", true, 3, 2);
  test(pkg, "XXXX", true, 0, 2);
  test(pkg, "a", true, 1, 2);

  pexl_free_Binary(pkg);
  //  pexl_free_Expr(exp);

  /********************************/
  /* Now with a recursive grammar */
  /********************************/

  TEST_SECTION("Checking a grammar that repeats a nullable using recursion");

  pexl_free_Context(C);
  C = pexl_new_Context();
  env = 0;                      // root env has id 0

  printf("--- Test case ('A -> {\"a\" A}/epsilon; expression = A*\n");

  ref = pexl_bind_in(C, env, NULL, NULL);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  exp = pexl_choice_f(C, 
                       pexl_seq_f(C, pexl_match_string(C, "a"), pexl_call(C, ref)),
                       pexl_match_epsilon(C));
  TEST_ASSERT(pexl_rebind(C, ref, exp) == OK);

  exp = pexl_repeat_f(C, exp, 0, 0);
  pexl_Optims *optims = pexl_default_Optims();
  pexl_Error err;
  pkg = compile_no_bind(C, ref, optims, &err);
  TEST_ASSERT(pkg && (pexl_Error_value(err) == 0));
  pexl_free_Optims(optims);

  test(pkg, "", true, 0, 0);
  test(pkg, "a", true, 0, 0);
  test(pkg, "aaXYZ", true, 3, 0);
  test(pkg, "aaaa", true, 0, 0);
  test(pkg, "aaaa ", true, 1, 0);

  pexl_free_Binary(pkg);

  printf("--- Test case CAPTURE('foo', A*)\n");
  exp = pexl_capture_f(C, "foo", exp);
  pkg = bind_and_compile(C, exp);

  test(pkg, "", true, 0, 2);
  test(pkg, "a", true, 0, 2);
  test(pkg, "aaXYZ", true, 3, 2);
  test(pkg, "aaaa", true, 0, 2);
  test(pkg, "aaaa ", true, 1, 2);

  pexl_free_Binary(pkg);
  //  pexl_free_Expr(exp);
  pexl_free_Context(C);
  
  /* ----------------------------------------------------------------------------- */
  TEST_END();
}

