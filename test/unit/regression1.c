//  -*- Mode: C; -*-                                                       
// 
//  regression1.c  -- A few tests to ensure we don't reintroduce bugs
//                    that we've already found and fixed.
//
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "libpexl.h"
#include "compile.h"

#include "../test.h"

#define PRINTING NO

int main(int argc, char **argv) {

  pexl_Error err;    
  pexl_Env env = 0;		// root env has id 0
  pexl_Context *C;
  pexl_Optims *optims;
  pexl_Binary *pkg;

  TEST_START(argc, argv);

  TEST_SECTION("Checking that lookup_placeholder in codegen works correctly");
  
  /*
    A -> "a" A "b" D / "c" B
    D -> A 
    B -> D

    TODO:
    X -> "x" X / eps
    Y -> X

    Z -> Y / B
    
  */

  C = pexl_new_Context();
  optims = pexl_default_Optims();

  pexl_Ref Dref = pexl_bind(C, NULL, "D"); 
  pexl_Ref Bref = pexl_bind(C, NULL, "B"); 
  pexl_Ref Aref = pexl_bind(C, NULL, "A"); 

  if (PRINTING) {
    printf("D stored at binding slot #%d\n", Dref);
    printf("B stored at binding slot #%d\n", Bref);
    printf("A stored at binding slot #%d\n", Aref);
  }

  pexl_Expr *Dexp = pexl_call(C, Aref); // D -> A
  pexl_Expr *Bexp = pexl_call(C, Dref); // B -> D
  pexl_Expr *Aexp =
    pexl_choice_f(C,
		   pexl_seq_f(C,
			       pexl_match_string(C, "a"),
			       pexl_seq_f(C, 
					   pexl_call(C, Aref),
					   pexl_seq_f(C,
						       pexl_match_string(C, "b"),
						       pexl_call(C, Dref)))),
		   pexl_seq_f(C,
			       pexl_match_string(C, "c"),
			       pexl_call(C, Bref))); 

#define bind(ref, exp) do {					\
    if (PRINTING) {						\
      printf("Binding ref index %d to:\n", ref);		\
      pexl_print_Expr(exp, 4, C);				\
    }								\
    TEST_ASSERT(pexl_rebind(C, ref, exp) == PEXL_OK);		\
    pexl_free_Expr(exp);					\
  } while (0);

  bind(Aref, Aexp);
  bind(Bref, Bexp);
  bind(Dref, Dexp);

  if (PRINTING) pexl_print_Env(env, C);

  printf("Compiling with default optimizations\n");
  pkg = pexl_compile(C, Aref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  printf("  Successful compilation.\n");
  if (PRINTING) pexl_print_Binary(pkg);
  pexl_free_Binary(pkg);

  printf("Compiling with no optimizations\n");
  pkg = pexl_compile(C, Aref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  printf("  Successful compilation.\n");
  if (PRINTING) pexl_print_Binary(pkg);
  pexl_free_Binary(pkg);

  pexl_free_Optims(optims);
  pexl_free_Context(C);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing correct code generation for Find expression");

  C = pexl_new_Context();
  optims = pexl_default_Optims();

  pexl_Expr *exp =
    pexl_repeat_f(C,
		  pexl_find_f(C,
			      pexl_match_string(C, "Omega")),
		  0, 0);
  pexl_Ref ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  pexl_Match *match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  
  printf("Testing pattern execution\n");
  int status = pexl_run(pkg, NULL, NULL,
			"OmeOmegaABC", 11, 0,
			0,		// vmflags: allow SIMD
			NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 8);
  printf("  Successful match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing code generation for Find of complete char set");

  char set[256];
  for (int i=0; i < 256; i++) set[i] = i;

  exp = pexl_find_f(C,
		    pexl_seq_f(C,
			       pexl_match_set(C, set, 256),
			       pexl_match_string(C, "x")));
  ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  printf("Testing pattern execution\n");
  
  status = pexl_run(pkg, NULL, NULL,
		    "O", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "Ox", 2, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 2);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "\0x", 2, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 2);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "", 0, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing code generation for Find of empty char set");

  exp = pexl_find_f(C, pexl_match_set(C, "", 0));
  ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  
  printf("Testing pattern execution\n");
  status = pexl_run(pkg, NULL, NULL,
		    "O", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "", 0, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing code generation for Find of empty string");

  exp = pexl_find_f(C, pexl_match_string(C, ""));
  ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  printf("Testing pattern execution\n");
  
  status = pexl_run(pkg, NULL, NULL,
		    "a", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 0);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "", 0, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 0);
  printf("  Successful match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing code generation for Find of EOI");

  exp = pexl_find_f(C, pexl_neg_lookahead_f(C, pexl_match_any(C, 1)));
  ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  if (PRINTING)
    pexl_print_Expr(exp, 0, C);

  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  printf("Testing pattern execution\n");
  
  status = pexl_run(pkg, NULL, NULL,
		    "a", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 1);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "abcdef", 6, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 6);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "", 0, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 0);
  printf("  Successful match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing code generation for nested Find expressions");

  exp = pexl_find_f(C,
		    pexl_find_f(C,
				pexl_match_string(C, "x")));
  ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  printf("Testing pattern execution\n");
  
  status = pexl_run(pkg, NULL, NULL,
		    "a", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "x", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 1);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "", 0, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing code generation for find with a guard instruction");

  printf("\nTODO! TODO! TODO!\n");
  // Need to contrive a pattern in which there will be an ITestChar
  // instruction in position to guard the IFind.  Or not bother, and
  // just remove the check for a guard in codefind().

#if 0
  exp = pexl_repeat_f(C, pexl_find_f(C, pexl_match_string(C, "x")), 1, 3);
  ref = pexl_bind(C, exp, NULL); 
  pkg = pexl_compile(C, ref, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  if (PRINTING)
    pexl_print_Binary(pkg);

  match = pexl_new_Match(PEXL_NO_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  printf("Testing pattern execution\n");
  
  status = pexl_run(pkg, NULL, NULL,
		    "a", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "x", 1, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_end(match) == 1);
  printf("  Successful match.\n");

  status = pexl_run(pkg, NULL, NULL,
		    "", 0, 0, 0,		// vmflags: allow SIMD
		    NULL, match);
  TEST_ASSERT(status == 0);
  TEST_ASSERT(pexl_Match_failed(match));
  printf("  Successful non-match.\n");

  pexl_free_Expr(exp);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);
#endif
  
  /* ----------------------------------------------------------------------------- */
  
  pexl_free_Optims(optims);
  pexl_free_Context(C);

  TEST_END();

  return 0;
}
