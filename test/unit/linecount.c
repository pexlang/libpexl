/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  linecount.c  Count newlines in a file using find.c                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* 
   cc -flto -O3 -I ../../src -o linecount linecount.c ../../src/find.o

   On my 2015 MacBook Pro, 'wc' needs only about 70% of the time
   needed by this program, even though 'wc' compares one character at
   a time to '\n'.  Another difference is that BSD 'wc' uses open/read
   instead of fopen/fread.

   TODO: We should verify that cc is inlining the functions from
   find.o (via link time optimization), else that will also make this
   program slower.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "find.h"

/* Caller must free the return value */
static const char *readfile (const char *filename, size_t *filesize) {
  FILE *f = fopen(filename, "r");
  if (!f) return NULL;
  int stat = setvbuf(f, NULL, _IOFBF, 1024*32); // large buffer
  if (stat) printf("setvbuf returned %d\n", stat);
  stat = fseek(f, 0, SEEK_END);
  if (stat) printf("fseek returned %d\n", stat);
  long filelen = ftell(f);
  if (filelen < 0) {
    fprintf(stderr, "Invalid file size %ld\n", filelen);
    fclose(f);
    return NULL;
  }
  *filesize = (size_t) filelen;
  stat = fseek(f, 0, 0);
  if (stat) printf("fseek returned %d\n", stat);
  char *contents = (char *) malloc(*filesize+1); 
  if (!contents) {
    fprintf(stderr, "malloc failed\n");
    return NULL;
  }
  size_t n = fread(contents, (size_t) *filesize, 1, f);
  if (n != 1) {			/* one item */
    fprintf(stderr, "fread failed\n");
    free(contents);
    contents = NULL;
  }
  contents[*filesize] = '\0';
  fclose(f);
  return contents;
}


int main(int argc, char **argv) {

  const char *ptr;

  if (argc < 2) {
    printf("Usage: %s <filename>\n", argv[0]);
    exit(-1);
  }

  size_t inputlen;
  const char *input = readfile(argv[1], &inputlen);

  if (!input) {
    printf("Could not open file\n");
    exit(-2);
  }

  /* count the number of newlines in the input */
  ptr = input;
  int linecount = 0;
  while (ptr) {
    ptr = find_char('\n', ptr, input + inputlen);
    if (ptr) {
      linecount++; ptr++;
    }
  }
  printf("%d\n", linecount);

  return 0;
}

