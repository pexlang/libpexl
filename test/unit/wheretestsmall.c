/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  wheretest.c  TESTING implementation of 'where' feature                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Kayla Sanderson                              */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "binary.h"
#include "vm.h"

#include "../test.h"

#define PRINTING NO

static char *readFile(char *filename) {
  FILE *f = fopen(filename, "rt");
  TEST_ASSERT(f);
  fseek(f, 0, SEEK_END);
  long length = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *buffer = (char *)malloc(length + 1);
  buffer[length] = '\0';
  fread(buffer, 1, length, f);
  fclose(f);
  return buffer;
}

static int vmtest (const char *patname,
		   pexl_Binary *pkg, pexl_PackageTable *pt,
		   pexl_Index entrypoint, 
		   const char *input, size_t inputlen,
		   pexl_Match *match, bool matched, ssize_t end) {
  int stat;
  printf("Running vm on pattern '%s' and input: ", patname);
  if (inputlen > 70) 
    printf("\"%.*s\"...\n", 70, input);
  else
    printf("\"%.*s\"\n", (int) inputlen, input);
  if (PRINTING) printf("Entrypoint is %d\n", entrypoint);
  stat = vm_start(pkg, pt, entrypoint, input, inputlen, 0, inputlen, 0, NULL, match);
  if (stat != 0) {
    if (PRINTING)
      printf("vmtest: vm returned status code %d\n", stat);
    return stat;
  }
  if ((match->end != -1) != matched) {
    printf("vmtest: expected %s\n", matched ? "succcess but match failed" : "fail but match succeeded");
    stat = 1;
    return stat;
  }
  if (matched && !pexl_Match_failed(match)) {
    if (match->end != end) {
      printf("vmtest: expected match end of %lu, got %lu\n", end, match->end);
      stat = 1;
      return stat;
    }
  }
  return OK;
}

#define matchtree_string(m, offset) \
  (m->data ? &(((pexl_MatchTree *)m->data)->block[offset]) : NULL)

static int check_match_tree (pexl_Match *match, size_t n,
			     pexl_Position startpos[],
			     pexl_Position endpos[],
			     const char *prefixes[],
			     const char *typenames[],
			     const char *datavalues[]) {
  pexl_Index i;
  pexl_MatchTreeNode node;
  pexl_MatchTree *tree = (pexl_MatchTree *)match->data;
  if (tree->size != n) return -1; /* error */
  i = pexl_MatchTree_iter(match, PEXL_ITER_START, &node);
  while (i >= 0) {
    if (node.start != startpos[i]) {
      printf("at node [%d]: expected startpos %" pexl_Position_FMT " to be %" pexl_Position_FMT "\n",
	     i, node.start, startpos[i]);
      return -1;
    }
    if (node.end != endpos[i]) {
      printf("at node [%d]: expected endpos %" pexl_Position_FMT " to be %" pexl_Position_FMT "\n",
	     i, node.end, endpos[i]);
      return -1;
    }
    if (node.prefix == -1) {
      if (prefixes[i] != NULL) {
	printf("at node [%d]: expected prefix '%s' to be NULL\n", i, prefixes[i]);
	return -1;
      }
    } else {
      const char *prefix = matchtree_string(match, node.prefix);
      if (!prefixes[i] || memcmp(prefix, prefixes[i], strlen(prefixes[i])) != 0) {
	printf("at node [%d]: prefix '%s' does not match expected '%s'\n",
	       i, prefix, prefixes[i]);
	return -1;
      }
    }
    if (node.type == -1) {
      if (typenames[i] != NULL) {
	printf("at node [%d]: expected type '%s' to be NULL\n", i, typenames[i]);
	return -1;
      }
    } else {
      const char *type = matchtree_string(match, node.type);
      if (!typenames[i] || memcmp(type, typenames[i], strlen(typenames[i])) != 0) {
	printf("at node [%d]: type '%s' does not match expected '%s'\n",
	       i, type, typenames[i]);
	return -1;
      }
    }
    if (node.inserted_data == -1) {
      if (datavalues[i] != NULL) {
	printf("at node [%d]: expected data '%s' to be NULL\n", i, datavalues[i]);
	return -1;
      }
    } else {
      const char *data = matchtree_string(match, node.inserted_data);
      if (!datavalues[i] || memcmp(data, datavalues[i], strlen(datavalues[i])) != 0) {
	printf("at node [%d]: data '%s' does not match expected '%s'\n",
	       i, data, datavalues[i]);
	return -1;
      }
    }
    i = pexl_MatchTree_iter(match, i, &node);
  } /* while */
  return PEXL_OK;
}

static
pexl_Binary *compile_expression (pexl_Context *C,
				 pexl_Optims *optims,
				 pexl_Expr *exp,
				 pexl_Env env,
				 pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);

#if 0
  confess("TEMP", "setting default entrypoint manually");
  Binding *b = env_get_binding(C->bt, ref);
  TEST_ASSERT(b && (bindingtype(b) == Epattern_t));
  Pattern *pat = (Pattern *) b->val.ptr;
  TEST_ASSERT(pat);
  TEST_ASSERT(pat->entrypoint >= 0);
  pkg->ep = pat->entrypoint;
#endif
  return pkg;
}

int main(int argc, char **argv) {

  int stat;
  pexl_Match *match;
  
  pexl_Context *C;
  pexl_Optims *optims = NULL;
  pexl_Binary *pkg;
  pexl_Error err;
  
  pexl_Env env; 

  pexl_Expr *exp, *A, *B, *S;
  pexl_Ref ref, refA, refB, refS, ref_anon;

  const char *input;
  size_t inputlen;
  pexl_PackageTable *pt;
  pexl_Index id;

  SymbolTableEntry *entry;
  char refname[2];
  refname[0] = '\0';
  refname[1] = '\0';
  pexl_Stats timing;
  pexl_Index ep;
  TEST_START(argc, argv);

  /* -----------------------------------------------------------------------------
   */

  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();

  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT_NOT_NULL(match);
  TEST_ASSERT(match_encoder(match) == PEXL_TREE_ENCODER); 

  id = context_intern(C, "foo", 3); /* for later use */
  TEST_ASSERT(id >= 0);

  ref = pexl_bind_in(C, env, NULL, "A");
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C,		/* A -> "a" A "b" / epsilon */
		 pexl_seq_f(C,
			 pexl_match_string(C, "a"),
			 pexl_seq_f(C, 
				 pexl_call(C, ref),
				 pexl_match_string(C, "b"))),
		 pexl_match_epsilon(C));

  TEST_ASSERT(A);		/* A is a valid expression */
  printf("A is: \n"); 
  pexl_print_Expr(A, 0, C); 

  TEST_ASSERT(pexl_rebind(C, ref, A) == OK);
  pexl_free_Expr(A);

  // IMPORTANT: We cannot call compile_expression here, because it
  // will create a NEW pattern referring to the expression A.  We end
  // up with multiple distinct patterns sharing an expr.  It works ok
  // until we need to free the patterns (in pexl_free_Context), where
  // we double-free the shared expr, A.
  binary_free(pkg);
  pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT(pkg);
  TEST_ASSERT(pexl_Error_value(err) == OK);
  ep = main_entrypoint(pkg);
  /* do not free A because A is bound */

  if (PRINTING) pexl_print_Binary(pkg);

  printf("Testing that pattern fails on input 'Hello'...\n");
  TEST_ASSERT(!vmtest("A -> 'a' A 'b' / eps", 
		      pkg, NULL, ep, 
		      "Hello", 5, match, true, 0));
  printf("... ok. \n");

  printf("Testing that pattern succeeds on appropriate input...\n");
  TEST_ASSERT(!vmtest("A -> 'a' A 'b' / eps", 
		      pkg, NULL, ep, 
		      "aaabbb", 6, match, true, 6));
  printf("... ok. \n");
  
  printf("Testing that pattern matches a prefix of the input...\n");
  TEST_ASSERT(!vmtest("A -> 'a' A 'b' / eps",
		      pkg, NULL, ep, 
		      "abc", 3, match, true, 2));
  printf("... ok. \n");
  binary_free(pkg);




  pexl_free_Match(match);
  pexl_free_Context(C);
  pexl_free_Optims(optims);


  TEST_END();

  return 0;
}