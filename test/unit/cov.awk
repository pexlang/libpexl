# Read gcov output and generate an output file containing:
# (1) coverage information with one file per line, and
# (2) exactly ONE summary line of cumulative statistics.

function basename(path)
{
    sub(".*/", "", path)
    return path
}

function noskip(filename)
{
    return (filename != "") && !match(filename, /logging.h/)
}

BEGIN {
    FS = "[: ]";
};

/^File/ {
    fcount++
    current_file = $2
    if (noskip(current_file))
    {
	printf("%-20s ", basename(substr($2, 2, length($2)-2)))
    }
};

/^Lines/ {
    # Skip the final 'Lines' that gcc's gcov puts into the data file
    if (noskip(current_file))
    {
	printf("%7s of %5d   ", $3, $5)
	lcount += $5
	percentage = substr($3, 0, length($3)-1)
	lcov += percentage * $5 / 100.0
    }
};

/^Branches/ {
    if (noskip(current_file))
    {
	printf("%7s of %5d\n",$3,$5)
	bcount += $5
	percentage = substr($3, 0, length($3)-1)
	bcov += percentage * $5 / 100.0
    }
    current_file = ""		# This is the last line we process
};

/^No branches/ {
    if (noskip(current_file))
    {
	printf("no branches\n")
    }
    current_file = ""		# This is the last line we process
};
END {
    lpercentage = 100.0 * lcov / lcount 
    bpercentage = 100.0 * bcov / bcount 
    printf("%2d files %12s %5.1f%% of %5d    ", fcount, "", lpercentage, lcount)
    printf("%5.1f%% of %5d\n", bpercentage, bcount)
};

