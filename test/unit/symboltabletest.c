/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  symboltabletest.c  TESTS of a simple string table                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "libpexl.h"
#include "symboltable.h"
#include "print.h"

#include "../test.h"

#define PRINTING false
#define FUZZING false

#define MAXLEN 100

static void print_block (SymbolTable *st) {
  size_t len = StringBuffer_len(st->block);
  const char *block = StringBuffer_ptr(st->block);
  size_t i = 0;
  while (i < len) {
    printf("[%5ld] %s\n", i, block + i);
    i += 1 + strlen(block + i);
  }
}

int main(int argc, char **argv) {

  char *s;
  int stat;
  pexl_Index i, index, saveindex;
  SymbolTableEntry  *entry;
  long offset, rando, pos;
  const char *tmp;
  FILE *words;
  char buf[SYMBOLTABLE_MAXLEN + 2];
  int nwords;
  SymbolTable *st1;
  
  TEST_START(argc, argv);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing error checks");

  st1 = symboltable_new(0, 100);
  TEST_ASSERT(st1); // initial size <= 0, which means use the default size
  TEST_ASSERT(Table_capacity(st1->entries) == SYMBOLTABLE_INITSIZE);
  symboltable_free(st1);

  st1 = symboltable_new(100, 0);
  TEST_ASSERT(st1);    // initial block size <= 0, so use default size
  TEST_ASSERT(StringBuffer_capacity(st1->block) == SYMBOLTABLE_INITBLOCKSIZE);
  symboltable_free(st1);

  st1 = symboltable_new(SYMBOLTABLE_MAXSIZE + 1, 100);
  TEST_ASSERT(st1); // initial size > SYMBOLTABLE_MAXSIZE, so set use MAX SIZE
  TEST_ASSERT(Table_capacity(st1->entries) == SYMBOLTABLE_MAXSIZE);
  symboltable_free(st1);

  st1 = symboltable_new(100, SYMBOLTABLE_MAXHANDLE * 10);
  TEST_ASSERT(st1); // No limit on initial block size
  TEST_ASSERT(StringBuffer_capacity(st1->block) >= SYMBOLTABLE_MAXHANDLE);
  symboltable_free(st1);

  st1 = symboltable_new(1, 1);
  TEST_ASSERT(st1);		// size 1 is allowed 
  TEST_ASSERT(symboltable_len(st1) == 0);
  TEST_ASSERT(symboltable_blocklen(st1) == 1);
  TEST_ASSERT(Table_capacity(st1->entries) == 1);
  // A string buffer has some minimum size, likely much larger than 1
  TEST_ASSERT(StringBuffer_capacity(st1->block) >= 1);

  fprintf(stderr, "Expect warning here:\n");
  stat = symboltable_add(NULL, NULL, 0, 0, 0, 0, 0, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  fprintf(stderr, "Expect warning here:\n");
  entry = symboltable_get(NULL, 0);
  TEST_ASSERT(!entry);
  fprintf(stderr, "Expect warning here:\n");
  tmp = symboltable_get_name(NULL, 0);
  TEST_ASSERT(!tmp);
  fprintf(stderr, "Expect warning here:\n");
  entry = symboltable_iter(NULL, &index);
  TEST_ASSERT(!entry);
  fprintf(stderr, "Expect warning here:\n");
  tmp = symboltable_block_iter(NULL, &index);
  TEST_ASSERT(!tmp);

  // We are allowed to omit a string arg, which is same as empty string 
  stat = symboltable_add(st1, NULL, 0, 0, 0, 0, 0, NULL);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(symboltable_len(st1) == 1);

  memset(buf, 'A', SYMBOLTABLE_MAXLEN + 1);
  buf[SYMBOLTABLE_MAXLEN + 1] = '\0';
  TEST_ASSERT(strlen(buf) == SYMBOLTABLE_MAXLEN + 1); // checking the arg length
  stat = symboltable_add(st1, buf, 0, 0, 0, 0, 0, NULL);
  TEST_ASSERT(stat == PEXL_ST__ERR_STRINGLEN); // strings have a length limit
  // Failed attempts should not affect table
  TEST_ASSERT(symboltable_len(st1) == 1);
  TEST_ASSERT(StringBuffer_len(st1->block) == 1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Storing a few strings");

  symboltable_free(st1);

  st1 = symboltable_new(1, 15);
  TEST_ASSERT(st1);
  TEST_ASSERT(symboltable_len(st1) == 0); // empty table

  // The empty string is always in the string storage block
  TEST_ASSERT(symboltable_blocklen(st1) == 1);
  TEST_ASSERT(Table_capacity(st1->entries) == 1);
  // A string buffer has some minimum size, likely much larger than 15
  TEST_ASSERT(StringBuffer_capacity(st1->block) >= 15);

  index = symboltable_add(st1, "foo", 1, symbol_ns_data, 0, 0, -54321, NULL);
  printf("index = %d\n", index);
  // Implementation dependent: first entry in new table should be at index 0
  TEST_ASSERT(index == 0);

  entry = symboltable_get(st1, index);
  TEST_ASSERT(entry != NULL);	              /* "get index 0 should succeed" */
  TEST_ASSERT(entry->handle == 1);            /* "foo is first string, should start at block index 1" */
  TEST_ASSERT(entry->visibility == 1);        /* "vis was set when this entry was added" */
  TEST_ASSERT(entry->namespace == 1);         /* ns set to 1 when this entry was added */
  TEST_ASSERT(entry->type == 0);
  TEST_ASSERT(entry->value == -54321);	      /* "value should be initialized correctly" */

  index = symboltable_add(st1, "network", 0, symbol_ns_data, 1, 0, 123, NULL);
  TEST_ASSERT(index == 1);	/* "2nd string should be at index 1" */

  entry = symboltable_get(st1, index);
  TEST_ASSERT(entry != NULL);	              /* "get index 0 should succeed" */
  TEST_ASSERT(entry->handle == 5);            /* "network is after foo in block" */
  TEST_ASSERT(entry->visibility == 0);        /* "vis not set when this entry was added" */
  TEST_ASSERT(entry->namespace == 1);         /* ns set to 1 when this entry was added */
  TEST_ASSERT(entry->type == 1);	      /* "stype set when this entry was added" */
  TEST_ASSERT(entry->value == 123);	      /* "value should be initialized" */

  index = symboltable_add(st1, "indefatigable", 0, symbol_ns_data, 0, 1010, -1, NULL);
  TEST_ASSERT(index == 2);	/* "3rd string should be at index 2" */
  entry = symboltable_get(st1, index);
  TEST_ASSERT(entry != NULL);	    /* "get index 0 should succeed" */
  TEST_ASSERT(entry->handle == 13); /* "indefatigable is after network in block" */
  TEST_ASSERT(entry->visibility == 0);   /* "vis not set when this entry was added" */
  TEST_ASSERT(entry->type == 0);	       /* "stype not set when this entry was added" */
  TEST_ASSERT(entry->value == -1);	       /* "value should be initialized" */
  TEST_ASSERT(entry->size == 1010);	       /* "size should be initialized" */
  saveindex = index;

  index = symboltable_add(st1, NULL, 1, symbol_ns_data, 1, 0, 0, NULL);
  TEST_ASSERT(index == 3);	/* "now at index 3" */

  index = symboltable_add(st1, "", 1, symbol_ns_data, 1, 0, 0, NULL);
  TEST_ASSERT(index == 4);	/* "now at index 4" */

  printf("Block storage:\n");
  print_block(st1);
  print_symboltable(st1);

  printf("Testing iterator for block storage:\n");
  fprintf(stderr, "Expect warning here:\n");
  tmp = symboltable_block_iter(st1, NULL);
  TEST_ASSERT(!tmp);		/* "index arg cannot be null" */
  i = 0;
  index = PEXL_ITER_START;
  while ((tmp = symboltable_block_iter(st1, &index))) { printf(">> %s\n", tmp); i++; }
  TEST_ASSERT(i == 3);		/* "incorrect number of strings (empty strings count)" */

  printf("Testing iterator for symbol table entries:\n");
  fprintf(stderr, "Expect warning here:\n");
  entry = symboltable_iter_ns(st1, symbol_ns_data, NULL);
  TEST_ASSERT(!entry);		/* "index arg cannot be null" */
  i = 0;
  index = PEXL_ITER_START;
  while ((entry = symboltable_iter_ns(st1, symbol_ns_data, &index))) {
    printf(">> [%5d] %s\n", entry->handle, symboltable_get_name(st1, index));
    i++;
  }
  TEST_ASSERT(i == 5);		/* "incorrect number of entries" */


  entry = symboltable_get(st1, saveindex);
  TEST_ASSERT(entry);		               /* "expected an entry" */
  TEST_ASSERT(entry->handle == 13); /* "indefatigable is after network in block" */
  TEST_ASSERT(entry->visibility == 0);   /* "vis not set when this entry was added" */
  TEST_ASSERT(entry->namespace == symbol_ns_data);
  TEST_ASSERT(entry->type == 0);	       /* "stype not set when this entry was added" */
  TEST_ASSERT(entry->value == -1);	       /* "value should be what was set earlier" */
  TEST_ASSERT(entry->size == 1010);	       /* "size should be what was set earlier" */
  

  printf("Testing max string length\n");
  /* Now try storing a string that is exactly the longest length allowed */
  buf[SYMBOLTABLE_MAXLEN] = '\0';
  index = symboltable_add(st1, buf, 0, symbol_ns_data, 0, 0, 0, NULL);
  printf("index = %d\n", index);
  TEST_ASSERT(index > 0);	/* "should work" */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Block iterator");

  i = 0;
  index = PEXL_ITER_START;
  /* This is not a great test.  Does not ensure we get every index once. */
  while ((tmp = symboltable_block_iter(st1, &index))) {
    switch (index) {
    case 1:
      printf("found entry %d\n", index);
      TEST_ASSERT(strcmp(tmp, "foo") == 0); /* "entry 1 wrong" */
      break;
    case 5:
      printf("found entry %d\n", index);
      TEST_ASSERT(strcmp(tmp, "network") == 0); /* "entry 2 wrong" */
      break;
    case 13:
      printf("found entry %d\n", index);
      TEST_ASSERT(strcmp(tmp, "indefatigable") == 0); /* "entry 3 wrong" */
      break;
    case 27:
      printf("found entry %d\n", index);
      TEST_ASSERT(strncmp(tmp, "AAAAA", 5) == 0); /* "entry 4 wrong" */
      TEST_ASSERT(strlen(tmp) == SYMBOLTABLE_MAXLEN); /* "last string should have max length" */
      break;
    default:
      printf("index = %d\n", index);
      TEST_FAIL("iterator stopped at invalid position");
    }
    i++;
  }
  TEST_ASSERT(i == 4);		/* "wrong number of strings" */

  symboltable_free(st1);
  st1 = symboltable_new(1, 1);

  /* ----------------------------------------------------------------------------- */

  // There are around 200,000 words in /usr/dict/words
  const int NUMWORDS = FUZZING? 21000 : 100;

  TEST_SECTION("Storing many more strings");

  printf("Loading from /usr/share/dict/words\n");
  words = fopen("/usr/share/dict/words", "r");
  stat = fseek(words, 0, SEEK_END);
  offset = ftell(words);
  printf("stat is %d, last is %ld\n", stat, offset);

  rando = rand();		/* 32 bits */
  pos = ((rando >> 22) * offset) / (1<<10);
  if ((offset - pos) < (NUMWORDS * 10)) pos = offset - (NUMWORDS * 10);
  stat = fseek(words, pos, SEEK_SET);
  printf("%ld/%ld, random seek chose a place %ld%% into file\n", pos, offset, pos*100/offset);
  s = fgets(buf, MAXLEN, words); /* read past a partial word */
  for (nwords=0; nwords < NUMWORDS; nwords++) {
    s = fgets(buf, MAXLEN, words);
    if (!s) break;
    while((*s!='\0') && (*s!='\n')) s++;
    *s = '\0';			/* overwrite newline */
    index = symboltable_add(st1, buf, 0, symbol_ns_data, 0, 0, 0, NULL);
    if (index <= 0) {
      printf("Error from symboltable_add(, 0, 0, NULL):\npexl_Stats are:\n");
      print_symboltable_stats(st1);
    }
  } /* for */
  printf("%d strings processed from /usr/dict/words\n", nwords);
  print_symboltable_stats(st1);

  i = 0;
  index = PEXL_ITER_START;
  while ((entry = symboltable_iter_ns(st1, symbol_ns_data, &index))) i++;
  TEST_ASSERT(i == NUMWORDS);	/* "wrong number of symbols in table" */

  symboltable_free(st1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Filling the table to its string block max");

  st1 = symboltable_new(1, 1);
  memset(buf, 'A', SYMBOLTABLE_MAXLEN);
  buf[SYMBOLTABLE_MAXLEN] = '\0';

  // Precondition for test
  TEST_ASSERT(strlen(buf) >= SYMBOLTABLE_MAXLEN);

  pexl_Index last_valid_index = -1;
  i = 0;
  while (true) {
    index = symboltable_add(st1, buf, 0, symbol_ns_data, 0, 0, 0, NULL);
    if (index < 0) break;
    last_valid_index = index;
    // Modify the string so it won't match one already in the block
    buf[i % SYMBOLTABLE_MAXLEN]++;
    i++;
  }
  printf("%d strings of length %ld added to new string table\n", i, strlen(buf));
  printf("last valid string table entry index is %" pexl_Index_FMT "\n", last_valid_index);
  printf("symbol table block size is %ld\n", StringBuffer_len(st1->block));
  fflush(NULL);

  // The error that stopped the loop above should be this one:
  TEST_ASSERT(index == PEXL_ST__ERR_BLOCKFULL);

  // The handle of the last string added must be SYMBOLTABLE_MAXHANDLE or less
  entry = symboltable_get(st1, last_valid_index);
  TEST_ASSERT_NOT_NULL(entry);
  TEST_ASSERT(entry->handle > 0);
  TEST_ASSERT(entry->handle <= SYMBOLTABLE_MAXHANDLE);


  TEST_SECTION("Filling the table to its max entries");

  if (FUZZING) {

    // Pre-requisite for next test
    TEST_ASSERT(Table_capacity(st1->entries) < SYMBOLTABLE_MAXSIZE);
  
    printf("st1 has %" pexl_Index_FMT " entries, and SYMBOLTABLE_MAXSIZE is %d\n",
	   Table_size(st1->entries), SYMBOLTABLE_MAXSIZE);
    fflush(stdout);
    fprintf(stderr, "Expect warning here about symbol table filling up:\n");
    i = Table_size(st1->entries);
    while ((index = symboltable_add(st1, NULL, 0, symbol_ns_data, 0, 0, 0, NULL)) >= 0) {
      if (PRINTING)
	if ((i%1000)==0) {printf("%d\n", i); fflush(stdout);}
      i++;
    }
    printf("%d entries in symbol table\n", i); fflush(stdout);
    TEST_ASSERT(index == PEXL_ERR_FULL);

    TEST_ASSERT(Table_size(st1->entries) == Table_capacity(st1->entries));
    TEST_ASSERT(Table_size(st1->entries) == SYMBOLTABLE_MAXSIZE);

    print_symboltable_stats(st1);
  }

  symboltable_free(st1);

  TEST_END();
}

