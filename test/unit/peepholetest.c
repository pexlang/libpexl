/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  peepholetest.c  TESTING peephole optimizer                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Vivek Reddy                                                     */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "compile.h"
#include "vm.h"

#define PRINTING NO
#include "ip_patterns.h"	// Uses PRINTING setting

// Change this to PEXL_DEBUG_ENCODER if you need
// all the match data to be printed out
#define ENCODER PEXL_TREE_ENCODER

/* Helper Functions */
static vm_match *compile_and_run (pattern *pt,
				  pexl_Optims *optims,
				  const char* input,
				  const char *patname) {

  int stat;
  
  compile_generic_pattern(pt, optims);
  TEST_ASSERT(pt->pkg);

  vm_match *vm_m = vm_match_new(pt, input, strlen(input), NULL, ENCODER);

  /* Find entrypoint for 'patname' */
  SymbolTableEntry *entry = binary_lookup_symbol(pt->pkg, symbol_ns_id, patname);
  TEST_ASSERT_MSG(entry, "Search of symbol table for '%s' FAILED\n", patname);
  TEST_ASSERT(entry->type == symbol_type_pattern);

  pexl_Index entrypoint = entry->value;

  stat = vm_start(pt->pkg, NULL, entrypoint, 
		  vm_m->input, vm_m->inputlen,
		  0, vm_m->inputlen, 0,
		  NULL, vm_m->match);

  TEST_ASSERT_MSG(stat == 0, "Failed VM execution for input: %s\n", input);

  return vm_m;
}

static int check_match_is_as_expected (vm_match *vm_m, const char *input,
				       short matched, int leftover) {
  ssize_t end = strlen(input) - leftover;
  if ((vm_m->match->end != -1) != matched){
    printf("Failed: pexl_Match Failed for Input: %s \n\t Expected: %d, Found: %d\n",
	   input, matched, (vm_m->match->end != -1));
    return false;
  }
  if (vm_m->match->end != -1) {
    if (vm_m->match->end != end){
      printf("Failed: End position for Input: %s \n\t Expected: %lu, Found: %lu\n",
	     input, end, vm_m->match->end);
      return false;
    }
  }
  return true;
}

static int compare_matches (vm_match *vmm1, vm_match *vmm2) {
  pexl_Match *m1 = vmm1->match;
  pexl_Match *m2 = vmm2->match;
  if (!m1 && !m2) return true;
  if ((!m1 && m2) || (m1 && !m2)) {
    printf("pexl_Error comparing matches.  One is null: %p, %p\n", m1, m2);
    return true;
  }
  if (PRINTING) {
    pexl_print_Match_data(m1);
    pexl_print_Match_data(m2);
  }
  if ((m1->encoder_id != PEXL_TREE_ENCODER) ||
      (m2->encoder_id != PEXL_TREE_ENCODER)) {
    printf("SKIPPING match data comparison (tree encoder not used)\n");
    return true;
  }
  pexl_MatchTree *t1 = m1->data;
  pexl_MatchTree *t2 = m2->data;
  if (!t1 && !t2) return true;
  if ((!t1 && t2) || (t1 && !t2)) {
    printf("pexl_Error comparing match trees.  One is null: %p, %p\n", m1, m2);
    return true;
  }
  if (t1->size != t2->size) {
      printf("pexl_Error comparing matches.  Sizes don't match: %zu, %zu\n",
	     t1->size, t2->size);
      return false;
  }
  for (size_t i = 0; i < t1->size; i++) {
    if (memcmp(&(t1->nodes[i]), &(t2->nodes[i]), sizeof(pexl_MatchNode)) != 0) {
      printf("pexl_Error comparing matches.  Mismatch at node %zu\n", i);
      return false;
    }
  }
  return true;
}

static int verify_match (pattern* (*generate_pt)(void),
			 const char* input, const char *patname,
			 short matched, int leftover) {
    
    pattern *pt;
    pexl_Optims *optims = NULL;
    
    printf("\nTest case: %s\n", input);

    pt = generate_pt();
    TEST_ASSERT(pt);

    // (1) Compile with no optimizations at all.  Save match data.

    TEST_ASSERT_NULL(optims);
    vm_match *vm_m1 = compile_and_run(pt, optims, input, patname);
    TEST_ASSERT(check_match_is_as_expected(vm_m1, input, matched, leftover));
    generic_pattern_free(pt);

    // (2) Compile with ONLY peephole.  Ensure same match data.

    pt = generate_pt();
    TEST_ASSERT(pt);

    TEST_ASSERT_NULL(optims);
    optims = pexl_addopt_peephole(optims);
    TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_PEEPHOLE));
    vm_match *vm_m2 = compile_and_run(pt, optims, input, patname);
    TEST_ASSERT(check_match_is_as_expected(vm_m2, input, matched, leftover));
    TEST_ASSERT(compare_matches(vm_m2, vm_m1));
    generic_pattern_free(pt);

    // (3) Compile with ONLY inlining.  Ensure same match data.

    pt = generate_pt();
    TEST_ASSERT(pt);

    pexl_free_Optims(optims);
    optims = pexl_addopt_inline(NULL);
    TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_INLINE));
    vm_match *vm_m3 = compile_and_run(pt, optims, input, patname);
    TEST_ASSERT(check_match_is_as_expected(vm_m3, input, matched, leftover));
    TEST_ASSERT(compare_matches(vm_m3, vm_m1));
    generic_pattern_free(pt);

    // (4) Compile with inlining AND peephole.  Ensure same match data.

    pt = generate_pt();
    TEST_ASSERT(pt);

    pexl_free_Optims(optims);
    optims = pexl_addopt_inline(NULL);
    optims = pexl_addopt_peephole(optims);
    TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_INLINE));
    TEST_ASSERT(optimlist_contains(optims, PEXL_OPT_PEEPHOLE));
    vm_match *vm_m4 = compile_and_run(pt, optims, input, patname);
    TEST_ASSERT(check_match_is_as_expected(vm_m4, input, matched, leftover));
    TEST_ASSERT(compare_matches(vm_m4, vm_m1));
    generic_pattern_free(pt);

    // (5) Compile with default optimizations.  Ensure same match data.

    pt = generate_pt();
    TEST_ASSERT(pt);

    pexl_free_Optims(optims);
    optims = pexl_default_Optims();
    vm_match *vm_m5 = compile_and_run(pt, optims, input, patname);
    TEST_ASSERT(check_match_is_as_expected(vm_m5, input, matched, leftover));
    TEST_ASSERT(compare_matches(vm_m5, vm_m1));
    generic_pattern_free(pt);

    pexl_free_Optims(optims);
    vm_match_free(vm_m5);
    vm_match_free(vm_m4);
    vm_match_free(vm_m3);
    vm_match_free(vm_m2);
    vm_match_free(vm_m1);
    return OK;
}

/* Helper Functions */
static int verify_compilation(pattern* (*generate_pt)(void)) {
  pattern* pt = generate_pt();
  printf("Verifying that the test pattern compiles with NO optimizations\n");
  compile_generic_pattern(pt, NULL);
  if (PRINTING) pexl_print_Binary(pt->pkg);
  generic_pattern_free(pt);
  printf("Verifying that the test pattern compiles with default optimizations\n");
  pt = generate_pt();
  pexl_Optims *optims = pexl_default_Optims();
  compile_generic_pattern(pt, optims);
  if (PRINTING) pexl_print_Binary(pt->pkg);
  generic_pattern_free(pt);
  pexl_free_Optims(optims);
  return OK;
}



int main(int argc, char **argv) {

  TEST_START(argc, argv);
  
  TEST_SECTION("Test optimizeICall");

  printf("Verifying for Call -> Ret Optimization\n");
  TEST_ASSERT(verify_compilation(&recursive_pattern) == OK);

  printf("Verifying for Call -> Call Optimization\n");
  TEST_ASSERT(verify_compilation(&recursive_pattern2) == OK);

  printf("Verifying for Call -> Jmp Optimization\n");
  TEST_ASSERT(verify_compilation(&recursive_pattern3) == OK);
  TEST_ASSERT(verify_compilation(&recursive_pattern4) == OK);
    
  TEST_SECTION("Testing optimizer on larger patterns");

  printf("Testing Ipv6_Strict Pattern\n");
  TEST_ASSERT(verify_match(&create_ipv6, "2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "2001:db8::ff00:42:8329", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "1080:0:0:0:8:800:200C:4171", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "3ffe:2a00:100:7031::1", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "::192.9.5.5", "ipv6_strict", 1, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipv6, "FFFF:129.144.52.38", "ipv6_strict", 0, 18) == OK);

  printf("Testing Ipv4 Pattern\n");
  TEST_ASSERT(verify_match(&create_ipv4, "255.255.255.255", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "0.0.0.0", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "1.2.234.123", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "255.0.0.255", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "255.255.255.255.255.255.255.255", "ipv4", 1, 16) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "1.1.1.256", "ipv4", 1, 1) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "1234.1.2.3", "ipv4", 0, 10) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "111.222.333.", "ipv4", 0, 12) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "111.222.333..444", "ipv4", 0, 16) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "999.999.999.999", "ipv4", 0, 15) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "256.1.1.1", "ipv4", 0, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "1.256.1.1", "ipv4", 0, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipv4, "1.1.256.1", "ipv4", 0, 9) == OK);     

  printf("Testing Ipv4 / Ipv6 Pattern\n");
  TEST_ASSERT(verify_match(&create_ipaddr, "2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "2001:db8:0:0:0:ff00:42:8329", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "2001:db8::ff00:42:8329", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1080:0:0:0:8:800:200C:4171", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "3ffe:2a00:100:7031::1", "ipv6_strict", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "::192.9.5.5", "ipv6_strict", 1, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "FFFF:129.144.52.38", "ipv6_strict", 0, 18) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "255.255.255.255", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "0.0.0.0", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.2.234.123", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "255.0.0.255", "ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "255.255.255.255.255.255.255.255", "ipv4", 1, 16) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.1.1.256", "ipv4", 1, 1) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1234.1.2.3", "ipv4", 0, 10) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "111.222.333.", "ipv4", 0, 12) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "111.222.333..444", "ipv4", 0, 16) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "999.999.999.999", "ipv4", 0, 15) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "256.1.1.1", "ipv4", 0, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.256.1.1", "ipv4", 0, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.1.256.1", "ipv4", 0, 9) == OK); 

  printf("Testing the Common Entry Point for Ipv6 and IPv4\n "); 
  TEST_ASSERT(verify_match(&create_ipaddr, "2001:0db8:0000:0000:0000:ff00:0042:8329", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "2001:db8:0:0:0:ff00:42:8329", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "2001:db8::ff00:42:8329", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "FEDC:BA98:7654:3210:FEDC:BA98:7654:3210", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1080:0:0:0:8:800:200C:4171", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "3ffe:2a00:100:7031::1", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "::192.9.5.5", "ipv6 / ipv4", 1, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "FFFF:129.144.52.38", "ipv6 / ipv4", 0, 18) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "255.255.255.255", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "0.0.0.0", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.2.234.123", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "255.0.0.255", "ipv6 / ipv4", 1, 0) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "255.255.255.255.255.255.255.255", "ipv6 / ipv4", 1, 16) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.1.1.256", "ipv6 / ipv4", 1, 1) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1234.1.2.3", "ipv6 / ipv4", 0, 10) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "111.222.333.", "ipv6 / ipv4", 0, 12) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "111.222.333..444", "ipv6 / ipv4", 0, 16) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "999.999.999.999", "ipv6 / ipv4", 0, 15) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "256.1.1.1", "ipv6 / ipv4", 0, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.256.1.1", "ipv6 / ipv4", 0, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "1.1.256.1", "ipv6 / ipv4", 0, 9) == OK);

  /* The tests below make use of the negative lookahead for an ipv4 pattern in ipv6_rest */
  TEST_ASSERT(verify_match(&create_ipaddr, "::192.9.5.5", "ipv6_strict", 1, 9) == OK);
  TEST_ASSERT(verify_match(&create_ipaddr, "::FFFF:129.144.52.38", "ipv6_strict", 1, 18) == OK);

  confess("peepholetest",
	  "TODO: Add tests from net.rpl that depend on having the negative lookahead in ipv6_component");

  TEST_END();
  
}
