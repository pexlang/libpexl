/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  contexttest.c  Testing compiler context                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "env.h"
#include "context.h"
#include "binary.h"

#include "../test.h"

#define NO 0

static int next_power2 (int n) {
  int i = 0;
  TEST_ASSERT(n > 0);		/* "bad arg to next_power2" */
  while((n = n >> 1)) i++;
  return 1 << (i+1);
}

int main(int argc, char **argv) {

  pexl_Context *C;

  TEST_START(argc, argv);

  /* ----------------------------------------------------------------------------- */

  C = context_new();
  TEST_ASSERT(C);

  TEST_ASSERT(C->bt);
  TEST_ASSERT(C->bt->capacity == ENV_INIT_SIZE);

  TEST_ASSERT(C->stringtab);
  TEST_ASSERT(Table_capacity(C->stringtab->entries) ==
	      next_power2(HASHTABLE_MIN_ENTRIES * 100 / HASHTABLE_MAX_LOAD));
  TEST_ASSERT(StringBuffer_capacity(C->stringtab->block) ==
	      HASHTABLE_MIN_ENTRIES * HASHTABLE_AVG_LEN);

  TEST_ASSERT(C->packages);
  TEST_ASSERT(C->packages->capacity == PACKAGETABLE_INITSIZE);

  context_free(C);

  TEST_END();
}

