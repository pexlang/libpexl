/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  trotest.c  Testing tail recursion optimization                           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include "libpexl.h"
#include "print.h"

#include "../test.h"

#define PRINTING NO


static bool has_recursion (pexl_Binary *pkg, const char *patname) {
  SymbolTableEntry *entry =
    symboltable_search(pkg->symtab, symbol_ns_id, patname, NULL);
  TEST_ASSERT(entry);
  pexl_Index entrypoint = entry->value;
  pexl_Index endpoint = entry->value + entry->size;
  Instruction *code = pkg->code;
  for (pexl_Index i = entrypoint; i < endpoint; i += sizei(&code[i])) 
    if ((opcode(&code[i]) == ICall) &&
        (getaddr(&code[i]) >= entrypoint && getaddr(&code[i]) < endpoint))
      return true;
  return false;
}

static void check_match (pexl_Match *m,
                         int expectation, int leftover,
                         const char *input) {

  ssize_t expected_end = strlen(input) - leftover;
  
  TEST_ASSERT(m->encoder_id == PEXL_TREE_ENCODER);

  if ((m->end == -1) != (expectation == 0)) {
    printf("pexl_Match error on input '%s': expected %s\n",
     input,
     expectation ? "a match" : "no match");
    TEST_ASSERT(false);
  }
  if (m->end != -1) {
    if (m->end != expected_end) {
      printf("pexl_Match length error on input \"%s\": expected %lu end, found %lu\n",
       input, expected_end, m->end);
      TEST_ASSERT(false);
    }
  }
}

static void run_tests (pexl_Binary *pkg, pexl_Index entrypoint, 
                       const char *inputs[], int matches[], int leftovers[]) {

  const char *input;
  size_t inputlen;
  pexl_Match *match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT(match);
  TEST_ASSERT(pkg);

  int stat, i = 0;
  while ((input = inputs[i])) {
    printf("\t input is \"%s\"\n", input);
    inputlen = strlen(input);
    stat = vm_start(pkg, NULL, entrypoint, 
                    input, inputlen,
                    0, inputlen,
                    0,          // vmflags
                    NULL, match);
    if (stat) {
      printf("vm error: %d\n", stat);
      TEST_FAIL("Compilation failed");
    }
    check_match(match, matches[i], leftovers[i], input);
    i++;
  } /* while */
  pexl_free_Match(match);
}

static void AB_test (pexl_Context *C, const char *patname, const char *recursive_pattern,
                     const char *inputs[], int matches[], int leftovers[]) {
  pexl_Index entrypoint;
  pexl_Binary *pkg;
  pexl_Error err;
  pexl_Optims *optims;

  printf("Running pattern: %s\n", patname);
  printf("Comparing recursive vs iterative for pattern: %s\n", recursive_pattern);

  /* No optimizations at all */
  pkg = pexl_compile(C, PEXL_NO_MAIN, NULL, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT_MSG(pkg && (pexl_Error_value(err) == OK),
                  "Compilation failed with error %d, location %d\n",
                  pexl_Error_value(err), pexl_Error_location(err));
  
  SymbolTableEntry *entry =
    symboltable_search(pkg->symtab, symbol_ns_id, patname, NULL);
  TEST_ASSERT(entry);
  entrypoint = entry->value;

  if (PRINTING) pexl_print_Binary(pkg); 
  TEST_ASSERT(has_recursion(pkg, recursive_pattern));
  
  run_tests(pkg, entrypoint, inputs, matches, leftovers);
  binary_free(pkg);

  /* With TRO */
  optims = pexl_addopt_tro(NULL);
  TEST_ASSERT(optims);
  pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  TEST_ASSERT_MSG(pkg && (pexl_Error_value(err) == OK),
                  "Compilation failed with error %d, location %d\n",
                  pexl_Error_value(err), pexl_Error_location(err));

  if (PRINTING) pexl_print_Binary(pkg); 
  TEST_ASSERT(!has_recursion(pkg, recursive_pattern));
  
  run_tests(pkg, entrypoint, inputs, matches, leftovers);
  binary_free(pkg);
  pexl_free_Optims(optims);
}

#define IntList(...) (int[]){__VA_ARGS__}
#define StrList(...) (const char *[]){__VA_ARGS__, NULL}
#define TRUE 1
#define FALSE 0

int main(int argc, char **argv) {

  pexl_Context *C;

  TEST_START(argc, argv);

  TEST_SECTION("Setting up");

  C = pexl_new_Context();
  TEST_ASSERT(C);
  
  TEST_SECTION("Simple tail recursive patterns");

  // Reminder: We do not have to free bound expressions.  They will be
  // destroyed when we free the env.

  // {[[a-z][A-Z]]+ " "* "Abram"} from kjv-libpexl.c
  pexl_Expr *exp =
    pexl_seq_f(C, 
            pexl_repeat_f(C,
                          pexl_choice_f(C,
                                        pexl_match_range(C, 'a', 'z'),
                                        pexl_match_range(C, 'A', 'Z')),
                          1, 0),
            pexl_seq_f(C, 
                       pexl_repeat_f(C,
                                     pexl_match_string(C, " "),
                                     0, 0),
                       pexl_match_string(C, "Abram")));

  const char *searchAbram = "search:Abram";
  TEST_ASSERT(!pexl_Ref_invalid(pexl_bind(C, exp, "a")));

  pexl_Ref searchAbrampexl_Ref = pexl_bind(C, NULL, searchAbram);
  TEST_ASSERT(!pexl_Ref_invalid(searchAbrampexl_Ref));
  pexl_Expr *temp = pexl_seq_f(C,
                                 pexl_match_any(C, 1),
                                 pexl_call(C, searchAbrampexl_Ref));
  pexl_Expr *findexp = pexl_choice(C, exp, temp);
  TEST_ASSERT(pexl_rebind(C, searchAbrampexl_Ref, findexp) == OK);

  AB_test(C, searchAbram, searchAbram,
          StrList("",   "a",   "a Abra", "a Abram", "abc   Abram", "a b c AbramX", "a Abram z Abram"),
          IntList(FALSE, FALSE, FALSE,    TRUE,      TRUE,          TRUE,           TRUE),
          IntList(0,     1,     6,        0,         0,             1,              8));


  const char *searchAllAbram = "searchAll:Abram";
  pexl_Ref searchAllAbrampexl_Ref = pexl_bind(C, NULL, searchAllAbram);
  TEST_ASSERT(!pexl_Ref_invalid(searchAllAbrampexl_Ref));
  pexl_Expr *searchAll_exp =
    pexl_repeat_f(C,
                  pexl_call(C, searchAbrampexl_Ref),
                  1, 0);
  TEST_ASSERT(pexl_rebind(C, searchAllAbrampexl_Ref, searchAll_exp) == OK);

  AB_test(C, searchAllAbram, searchAbram,
          StrList("",   "a",   "a Abram", "abc   Abram", "a b c AbramX", "a Abram z Abram"),
          IntList(FALSE, FALSE, TRUE,      TRUE,          TRUE,           TRUE),
          IntList(0,     1,     0,         0,             1,              0));

  confess("trotest", "Need more patterns to test");

  pexl_free_Context(C);
  TEST_END();
}
