/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest4.c  TESTING analyze.c                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "old-analyze.h"

#include "../test.h"

#define YES 1
#define NO 0

#define CAPTURES 1
#define HEADFAIL 1
#define NULLABLE 1
#define NOFAIL 1
#define FIRSTSET 1
#define NEEDFOLLOW 1
#define HEADFAIL 1

#define NO_CAPTURES 0
#define NOT_HEADFAIL 0
#define NOT_NULLABLE 0
#define CAN_FAIL 0
#define NO_FIRSTSET 0
#define NO_NEEDFOLLOW 0
#define NO_HEADFAIL 0

#define BOOL(exp) ((exp) ? 1 : 0)
#define has_flag(flags, flagname) ((flags)&(flagname))

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (BindingTable *bt, pexl_Ref ref) {
  Binding *b = env_get_binding(bt, ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

#define assert_unspecified_value_at(bt, ref)				\
  do {									\
    b999 = env_get_binding(bt, ref);					\
    /* "how can this happen?" */					\
    TEST_ASSERT(bindingtype(b999) == Eunspecified_t);			\
  } while (0)

#define free_pattern_at(bt, ref)					\
  do {									\
    b999 = env_get_binding(bt, ref);					\
    if(bindingtype(b999) == Epattern_t) {				\
      pattern_free((Pattern *) b999->val.ptr);				\
      stat = env_rebind(bt, ref, env_new_value(Eunspecified_t, 0, NULL)); \
      TEST_ASSERT(stat >= 0);						\
    } else {								\
      printf("free_pattern_at: no pattern to free\n");			\
    }									\
  } while (0)

static void check_node (Pattern *p,
			int hascaptures,
			int itself_hascaptures,
			int nullable,
			int nofail,
			int headfail,
			int unbounded,
			uint32_t expected_min,
			uint32_t expected_max,
			int needfollowflag,
			Charset firstset,
			int getfirstYES,
			size_t lineno) {
  uint32_t min, max;
  int stat;
  Charset cs;
  unsigned char c;
  Node *node;
  if (!p->fixed) {
    TEST_FAIL("pexl_Error in check_node: pattern %p has no 'fixed' expression tree\n",
	      (void *) p);
  }
  node = p->fixed->node;
  printf("check_node called on pattern %p from line %lu\n", (void *)p, lineno);
  TEST_ASSERT(BOOL(exp_hascaptures(node)) == hascaptures);
  TEST_ASSERT(BOOL(exp_itself_hascaptures(node)) == itself_hascaptures);
  stat = exp_patlen(node, &min, &max);
  if (stat != unbounded) {
    if ((stat != EXP_BOUNDED) && (stat != EXP_UNBOUNDED))
      printf("return value from exp_patlen is error: %d\n", stat);
    else
      printf("pattern is %s, which was not expected", (stat==EXP_BOUNDED) ? "bounded" : "unbounded");
  }
  TEST_ASSERT(stat == unbounded);
  if (min != expected_min)
    printf("min len = %u, expected = %u\n", min, expected_min);
  if ((stat == EXP_BOUNDED) && (max != expected_max))
    printf("max len = %u, expected = %u\n", max, expected_max);
  TEST_ASSERT(min == expected_min);
  TEST_ASSERT((stat != EXP_BOUNDED) || (max == expected_max));
  TEST_ASSERT(BOOL(exp_nullable(node)) == nullable);
  TEST_ASSERT(BOOL(exp_nofail(node)) == nofail);
  TEST_ASSERT(BOOL(exp_headfail(node)) == headfail);
  TEST_ASSERT(BOOL(exp_needfollow(node)) == needfollowflag);
  stat = exp_getfirst(node, fullset, &cs);
  /* stat==0 means firstset can be used as test */
  TEST_ASSERT((stat == 0) == getfirstYES);
  if (stat == 0) {
    /* Now check the contents of cs for 'h' */
    for (c = 0; c < 255; c++)
      if (testchar(firstset.cs, c)) {
	if (!testchar(cs.cs, c)) printf("charset missing %c\n", c);
	TEST_ASSERT(testchar(cs.cs, c));
      } else {
	if (testchar(cs.cs, c)) printf("cs should not have %c", c);
	TEST_ASSERT(!testchar(cs.cs, c));
      }
  } /* if stat==0 */
}

int main(int argc, char **argv) {

  Binding *b999;
  
  pexl_Context *C;

  int stat; 
  CompState *cst; 
  Pattern *p;
  pexl_Env toplevel, env, genv; 
  Node *node; 
  Charset cs;

  const char *str;

  pexl_Expr *A, *B, *S;

  pexl_Ref ref, refA, refB, refS, ref_anon;

  TEST_START(argc, argv);

  TEST_SECTION("Testing exp_nullable_rec inside set_recursive_metadata");

  C = context_new();
  TEST_ASSERT(C);
  toplevel = 0;			// root env has id 0
  
  stat = context_intern(C, "foo", 3);
  TEST_ASSERT(stat == 5);	/* "first interned string should be at offset 5" */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Non-recursive block using anonymous exps");

  env = env_new(C->bt, toplevel);

  refA = env_bind(C->bt, env, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refS = env_bind(C->bt, env, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  A = pexl_call(C, refS);		/* A -> S */
  S = pexl_match_epsilon(C);		/* S -> true */

  p = pattern_new(S, C->bt, env, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == env);

  assert_unspecified_value_at(C->bt, refS);
  stat = env_rebind(C->bt, refS, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat == 0);
  printf("S is: "); 
  pexl_print_Expr(S, 0, NULL); 
  printf("A is: "); 
  pexl_print_Expr(A, 0, NULL);  
  printf("refS points to: ");
  print_pattern(binding_pattern_value(C->bt, refS), 0, C);
  
  node = A->node;
  TEST_ASSERT(node);		/* "precondition for next tests" */

  p = pattern_new(A, C->bt, env, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == env);

  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  printf("stat = %d\n", stat);
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);

  stat = set_metadata(cst); 
  TEST_ASSERT(stat == 0);

  pattern_free(p);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Recursive block using anonymous exps");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  genv = env_new(C->bt, env);
  refA = env_bind(C->bt, genv, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  A = pexl_call(C, refA);		/* A -> A */

  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);

  free_expr(A);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;

  assert_unspecified_value_at(C->bt, refA);
  stat = env_rebind(C->bt, refA, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat == 0);

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);

  stat = set_metadata(cst); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);


  printf("Trying mutually left-recursive rules\n"); 
  genv = env_new(C->bt, env);
  refA = env_bind(C->bt, genv, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refB = env_bind(C->bt, genv, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refB));

  A = pexl_call(C, refB);		/* A -> B */
  B = pexl_call(C, refA);		/* B -> A */

  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);

  assert_unspecified_value_at(C->bt, refA);
  stat = env_rebind(C->bt, refA, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat >= 0);
  printf("Pattern A is: "); print_pattern(p, 0, C);

  p = pattern_new(B, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  assert_unspecified_value_at(C->bt, refB);
  stat = env_rebind(C->bt, refB, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat >= 0);
  printf("Pattern B is: "); print_pattern(p, 0, C);

  /* Compile B */
  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;

  free_expr(A);
  free_expr(B);

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);

  print_sorted_cfgraph(cst->g);

  stat = set_metadata(cst); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  genv = env_new(C->bt, env);

  stat = context_intern(C, "A", 1);
  TEST_ASSERT(stat >= 0);	/* "symbol add should succeed" */
  ref = env_bind(C->bt, genv, stat, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		 pexl_seq_f(C, 
			   pexl_match_set(C, "a", 1),
			   pexl_seq_f(C, 
				     pexl_call(C, ref),
				     pexl_match_set(C, "b", 1))),
		   pexl_match_epsilon(C));

  TEST_ASSERT(A);
  printf("A is: \n"); 
  pexl_print_Expr(A, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(stat == 0);
  free_expr(A);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  memset(&cs, 0, CHARSETSIZE); 
  setchar(cs.cs, 'A'); 
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL,
	     EXP_UNBOUNDED, 0, 123123, NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__); 
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with left recursion with nullable sub-pattern");

  A = pexl_choice_f(C, 		/* A -> "a"? A "b" / epsilon */
		   pexl_seq_f(C, 
			      pexl_repeat_f(C, pexl_match_set(C, "a", 1), 0, 1),
			      pexl_seq_f(C, 
					 pexl_call(C, ref),
					 pexl_match_set(C, "b", 1))),
		   pexl_match_epsilon(C));

  TEST_ASSERT(A);
  printf("A is: \n"); 
  pexl_print_Expr(A, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(A);

  free_pattern_at(C->bt, ref);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(stat == 0);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with left recursion and CALLED nullable sub-pattern");

  size_t len;
  str = context_retrieve(C, 5, &len); /* We happen to KNOW that the first entry is at offset 5 */
  printf("len is %zu\n", len);
  TEST_ASSERT(len == 3);
  TEST_ASSERT(memcmp(str, "foo", len) == 0); /* "symbol table entry 1 is 'foo'" */
  
  B = pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 1); /* B -> "a"? */
  TEST_ASSERT(B);
  p = pattern_new(B, C->bt, genv, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(B);

  refB = env_bind(C->bt, genv, 1, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(refB));

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 

  
  A = pexl_choice_f(C, 		/* A -> foo A "b" / epsilon */
		 pexl_seq_f(C, 
			   pexl_call(C, refB),
			   pexl_seq_f(C, 
				     pexl_call(C, ref),
				     pexl_match_string(C, "b"))),
		   pexl_match_epsilon(C));

  TEST_ASSERT(A);

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(A);

  free_pattern_at(C->bt, ref);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(stat == 0);

  printf("A is: \n"); 
  print_pattern(p, 0, C); 

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with mutual left recursion and CALLED nullable sub-pattern");

  /* B -> "a"? A */
  B = pexl_seq_f(C, 
		 pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 1), /* "a"? */
		 pexl_call(C, ref)                                  /* A */
		 );
  TEST_ASSERT(B);
  p = pattern_new(B, C->bt, genv, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  // keep B around for printing later

  free_pattern_at(C->bt, refB);
  assert_unspecified_value_at(C->bt, refB);
  stat = env_rebind(C->bt, refB, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat == 0);
  
  A = pexl_choice_f(C, 		/* A -> foo "b" / epsilon */
		 pexl_seq_f(C, 
			   pexl_call(C, refB),
			   pexl_match_range(C, 'b', 'b')),
		   pexl_match_epsilon(C)); 

  TEST_ASSERT(A);
  printf("A is: \n"); 
  pexl_print_Expr(A, 0, C); 
  printf("foo is: \n"); 
  pexl_print_Expr(B, 0, C); 

  free_expr(B);

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(A);

  free_pattern_at(C->bt, ref);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(stat == 0);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Add another non-terminal, and remove the mutual left recursion");

  /* <anon> -> "x" / A */
  B = pexl_choice_f(C, pexl_match_range(C, 'x', 'x'),
		   pexl_call(C, ref));

  TEST_ASSERT(B);
  p = pattern_new(B, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(B);

  ref_anon = env_bind(C->bt, genv, 0, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_invalid(ref_anon));

  /* B -> "a"+ <anon> */
  B = pexl_seq_f(C, 
		 pexl_repeat_f(C, pexl_match_range(C, 'a', 'a'), 1, 0), /* "a"+ */
		 pexl_call(C, ref_anon)		   /* A */
		 );
  TEST_ASSERT(B);
  p = pattern_new(B, C->bt, genv, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(B);

  free_pattern_at(C->bt, refB);
  assert_unspecified_value_at(C->bt, refB);
  stat = env_rebind(C->bt, refB, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat == 0);

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 

  A = pexl_choice_f(C, 		/* A -> B "b" / epsilon */
		   pexl_seq_f(C, 
			     pexl_call(C, refB),
			     pexl_match_range(C, 'b', 'b')),
		   pexl_match_epsilon(C));

  TEST_ASSERT(A);

  printf("<anon> is: \n"); 
  p = binding_pattern_value(C->bt, ref_anon);
  TEST_ASSERT(p);
  print_pattern(p, 0, C); 

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(A);

  free_pattern_at(C->bt, ref);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(stat == 0);

  printf("A is: \n"); 
  print_pattern(p, 0, C); 

  pexl_print_Env(genv, C);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  print_pattern(cst->pat, 0, C);

  stat = set_metadata(cst);
  TEST_ASSERT(stat == 0);

  memset(&cs, 0, CHARSETSIZE); 
  setchar(cs.cs, 'A'); 
  check_node(cst->pat, NO_CAPTURES, NO_CAPTURES, NULLABLE, NOFAIL, NOT_HEADFAIL,
	     EXP_UNBOUNDED, 0, 123123,
	     NEEDFOLLOW, cs, NO_FIRSTSET, __LINE__); 

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Make the previous grammar mutually left recursive");

  /* <anon> -> "x" / A */
  B = pexl_choice_f(C, pexl_match_range(C, 'x', 'x'),
		   pexl_call(C, ref));

  TEST_ASSERT(B);
  p = pattern_new(B, C->bt, genv, &stat);	/* bound */
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(B);

  ref_anon = env_bind(C->bt, genv, 0, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_invalid(ref_anon));

  printf("<anon> is: \n"); 
  p = binding_pattern_value(C->bt, ref_anon);
  TEST_ASSERT(p);
  print_pattern(p, 0, C); 

  /* B -> "a"* <anon> */
  B = pexl_seq_f(C, 
		 pexl_repeat_f(C, pexl_match_set(C, "a", 1), 0, 0), /* "a"* */
		 pexl_call(C, ref_anon)			            /* A */
		);
  TEST_ASSERT(B);
  p = pattern_new(B, C->bt, genv, &stat);
  TEST_ASSERT(p);
  p->env = genv;
  free_pattern_at(C->bt, refB);
  assert_unspecified_value_at(C->bt, refB);
  free_expr(B);

  stat = env_rebind(C->bt, refB, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(stat == 0);

  printf("foo is: \n"); 
  print_pattern(p, 0, C); 
  
  A = pexl_choice_f(C, 		/* A -> B "b" / epsilon */
		 pexl_seq_f(C, 
			   pexl_call(C, refB),
			   pexl_match_set(C, "b", 1)),
		   pexl_match_epsilon(C));

  TEST_ASSERT(A);

  /* Now modify the "A" binding's value to the pattern based on the expression A */
  p = pattern_new(A, C->bt, genv, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(p->env == genv);
  free_expr(A);

  free_pattern_at(C->bt, ref);
  assert_unspecified_value_at(C->bt, ref);
  stat = env_rebind(C->bt, ref, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(stat == 0);

  pexl_print_Env(genv, C);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  
  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g->order);

  printf("At refB is: ");
  print_pattern(binding_pattern_value(C->bt, refB), 0, C);
  printf("<anon> is: ");
  print_pattern(binding_pattern_value(C->bt, ref_anon), 0, C);
  printf("A is: \n");
  print_pattern(binding_pattern_value(C->bt, ref), 0, C);

  pexl_print_Env(genv, C);
  print_sorted_cfgraph(cst->g);

  printf("THIS TEST SHOULD CORRESPOND TO:\n");
  printf("  A -> (B 'b') / true\n");
  printf("  B -> 'a'* <anon>\n");
  printf("  <anon> -> 'x' / A\n");

  stat = set_metadata(cst); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_LEFT_RECURSIVE);

  compstate_free(cst);
  context_free(C);

  TEST_END();
}

