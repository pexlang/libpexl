/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest2.c  TESTING analyze.c AND dependency.c                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "old-analyze.h"

#include "../test.h"

#define YES 1
#define NO 0

#define PRINTING NO

/* Borrowed from analyze.c */
/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (BindingTable *bt, pexl_Ref ref) {
  Binding *b = env_get_binding(bt, ref);
  if (!b) return NULL;		/* invalid ref */
  if (b->val.type != Epattern_t) return NULL;
  return (Pattern *) b->val.ptr;
}

int main(int argc, char **argv) {

  uint32_t min, max;
  pexl_Context *C = NULL;
  
  int stat;
  CompState *cst;
  Pattern *p, *p1, *p3, *p4, *p5;
  pexl_Index i;
  CFgraph *graph;

  Charset cs;
  Node *node;
  unsigned char c;

  pexl_Expr *tstr, *tnum;
  pexl_Expr *t1, *t2, *t3, *t4, *t5; 
  pexl_Expr *A;

  pexl_Env env = 0;		// top level (root env)
  pexl_Ref ref, refKEEP;

  TEST_START(argc, argv);
  TEST_SECTION("fix open calls, compute metadata, deplists");

  /* pexl_Error checking */
  fprintf(stderr, "Expect a warning:\n");
  stat = fix_open_calls(NULL);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); /* caught null compstate arg */

  fprintf(stderr, "Expect a warning:\n");
  p = pattern_new(NULL, NULL, env, &stat);
  TEST_ASSERT(!p && (stat == PEXL_ERR_INTERNAL));

  C = context_new();
  TEST_ASSERT(C);
  TEST_ASSERT(C->bt);

  fprintf(stderr, "Expect a warning:\n");
  p = pattern_new(NULL, C->bt, env, &stat);
  TEST_ASSERT(!p && (stat == PEXL_ERR_INTERNAL));
  
  A = pexl_match_epsilon(C);
  TEST_ASSERT(A);
  p = pattern_new(A, C->bt, env, &stat);
  TEST_ASSERT(p);
  TEST_ASSERT(stat == 0);

  fprintf(stderr, "Expect a warning:\n");
  stat = build_cfgraph(NULL, &graph);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); /* expected to catch null arg error */
  tstr = pexl_match_string(C, "foo");
  pattern_free(p);

  p = pattern_new(tstr, C->bt, env, &stat);
  TEST_ASSERT(p);
  free_expr(tstr);
  
  fprintf(stderr, "Expect a warning:\n");
  stat = build_cfgraph(p, NULL);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); /* expected to catch null arg error */
  fprintf(stderr, "Expect a warning:\n");
  stat = build_cfgraph(NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); /* expected to catch null arg error */

  graph = NULL;
  stat = sort_cfgraph(graph);
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); /* expected to catch null arg error */

  pattern_free(p);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Fixing open calls");

  printf("Using literal string 'hello'\n");
  tstr = pexl_match_string(C, "hello");
  TEST_ASSERT(tstr);
  p = pattern_new(tstr, C->bt, env, &stat);
  TEST_ASSERT(p);
  // Do not free tstr, because we use it later

  pexl_free_Context(C);
  C = pexl_new_Context();
  TEST_ASSERT_NULL(C->cst);
  cst = compstate_new();
  C->cst = cst;
  cst->pat = p;

  p->env = env;
  /* pexl_Error check */
  fprintf(stderr, "Expect a warning about a NULL fixed-call tree:\n");
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == PEXL_ERR_INTERNAL); /* p->fixed is null because we have not set it yet */

  TEST_ASSERT(p->tree);
  p->fixed = pexl_copy_Expr(p->tree);
  TEST_ASSERT(p->fixed);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 1); /* size of dependency graph should be 1 vertex */

  stat = sort_cfgraph(cst->g);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 1); /* size of toposort should be 1 vertex */
  print_sorted_cfgraph(cst->g);
  print_cfgraph(cst->g); 
  TEST_ASSERT(cst->g->order->top == 1); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == 0); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -1); /* correct component number */

  TEST_ASSERT(cst->pat);	/* precondition for next test */

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;		// Otherwise, we'll attempt to free it again later
  
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  node = tstr->node;
  TEST_ASSERT(!exp_hascaptures(node)); /* this exp does not capture */
  TEST_ASSERT(!exp_itself_hascaptures(node)); /* this exp does not capture */
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected patlen to return BOUNDED */
  TEST_ASSERT((min == 5) && (max == 5)); /* wrong min/max */
  TEST_ASSERT(!exp_nullable(node));	 /* literal non-empty string not nullable */
  TEST_ASSERT(!exp_nofail(node));	 /* literal non-empty string can fail */
  TEST_ASSERT(exp_headfail(node) == 0);	 /* literal multi-char string is not headfail */
  TEST_ASSERT(!exp_needfollow(node));	 /* literal non-empty string does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);   /* firstset can be used as test, does not accept epsilon */
  /* Now check the contents of cs for 'h' */
  for (c = 0; c < 255; c++)
    if (c == 'h')
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only 'h' */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only 'h' */


  printf("Calling pattern for literal string 'hello'\n");
  
  TEST_ASSERT(p);		/* precondition for next test */
  ref = env_bind(C->bt, env, 123, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  TEST_ASSERT(ref == 0);	// First binding goes into slot 0 of table

  t1 = pexl_call(C, ref);
  TEST_ASSERT(t1);

  refKEEP = ref;

  /* Wrap the call in a new pattern struct */
  p1 = pattern_new(t1, C->bt, env, &stat);
  TEST_ASSERT(p1);
  TEST_ASSERT(stat == 0);
  free_expr(t1);

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p1;
  p1->env = env;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  TEST_SECTION("Ensuring that build_cfgraph catches certain errors");

  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL); /* found TOpenCall */

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Note: bt is %p, env is %d\n", C->bt, env);
  printf("Note: p1->bt is %p, p1->env is %d\n", p1->bt, p1->env);
  printf("Note: cst->pat->bt is %p\n", cst->pat->bt);

  printf("Using build_cfgraph correctly to operate on TCall\n");
  stat = build_cfgraph(p1, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  /* size of dependency graph should be 2 (p1 and its call target) */
  TEST_ASSERT(cst->g->next == 2);
  /* Check the contents of the deplist for p1 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p1); /* vertex p1 */
  TEST_ASSERT(cst->g->deplists[0][1] == p);  /* call target */
  TEST_ASSERT(cst->g->deplists[0][2] == NULL); /* NULL terminator */

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p); /* vertex p */
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); /* NULL terminator */

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 2); /* wrong size of toposort result */
  print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == 2); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == 1); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -2); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[1].vnum == 0);  /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[1].cnum == -1); /* correct component number */

  pattern_free(p1);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'seq'");

  t1 = pexl_call(C, refKEEP);		  /* hello */
  t2 = pexl_match_bytes(C, "a", 1);	  /* a */
  t3 = pexl_seq(C, t1, t2);
  TEST_ASSERT(t3);

  /* Wrap the call in a new pattern struct */
  p3 = pattern_new(t3, C->bt, env, &stat);
  TEST_ASSERT(p3);
  TEST_ASSERT(stat == 0);
  p3->env = env;

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p3;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p3, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); /* size of dependency graph should be 2 (p3 and its call target) */
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p3); /* vertex p3 */
  TEST_ASSERT(cst->g->deplists[0][1] == p);  /* call target */
  TEST_ASSERT(cst->g->deplists[0][2] == NULL); /* NULL terminator */

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p);    /* vertex p */
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); /* NULL terminator */

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 2);
  print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == 2); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == 1); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -2); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[1].vnum == 0);  /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[1].cnum == -1); /* correct component number */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'choice'");

  /* Choice of t2 and t3, where t3 is a seq that makes a call to p */
  t4 = pexl_choice(C, t2, t3);
  TEST_ASSERT(t4->node->tag == TChoice);
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  /* Wrap the call in a new pattern struct */
  p4 = pattern_new(t4, C->bt, env, &stat);
  TEST_ASSERT(p4);
  TEST_ASSERT(stat == 0);
  free_expr(t4);
  p4->env = env;

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p4;
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g); 

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2);
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p4); /* vertex p4 */
  TEST_ASSERT(cst->g->deplists[0][1] == p);  /* call target */
  TEST_ASSERT(cst->g->deplists[0][2] == NULL); /* NULL terminator */

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);		/* should get our call target pattern back */

  TEST_ASSERT(cst->g->deplists[1][0] == p); /* vertex p */
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); /* NULL terminator */

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 2); /* wrong size of toposort result */
  print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == 2); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == 1); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -2); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[1].vnum == 0);  /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[1].cnum == -1); /* correct component number */

  /* Choice of t1 and t3, where t1 calls p and p3 is a seq that makes a call to p */
  t4 = pexl_choice(C, t1, t3);
  TEST_ASSERT(t4->node->tag == TChoice);
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  /* Wrap the call in a new pattern struct */
  pattern_free(p4);
  p4 = pattern_new(t4, C->bt, env, &stat);
  TEST_ASSERT(p4);
  TEST_ASSERT(stat == 0);
  // do not free t4, because we use it later
  p4->env = env;

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p4;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p4, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); /* size of dependency graph should be 2 (p4 and its call target) */
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p4); /* vertex p4 */
  TEST_ASSERT(cst->g->deplists[0][1] == p);  /* call target */
  TEST_ASSERT(cst->g->deplists[0][2] == p);  /* second instance of same call target */
  TEST_ASSERT(cst->g->deplists[0][3] == NULL); /* NULL terminator */

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p); /* vertex p */
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); /* NULL terminator */

  print_cfgraph(cst->g);  

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 2); /* wrong size of toposort result */
  print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == 2); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == 1); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -2); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[1].vnum == 0);  /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[1].cnum == -1); /* correct component number */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'not'");

  /* t5 = not t4 */
  t5 = pexl_neg_lookahead(C, t4);
  TEST_ASSERT(t5->node->tag == TNot);
  node = t5->node;
  TEST_ASSERT(node);		/* precondition for next tests */
  free_expr(t4);
  
  /* Wrap the call in a new pattern struct */
  p5 = pattern_new(t5, C->bt, env, &stat);
  TEST_ASSERT(p5);
  TEST_ASSERT(stat == 0);
  free_expr(t5);
  p5->env = env;

  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p5;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  printf("Fixing open calls\n");
  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);

  printf("Building dependency graph\n");
  stat = build_cfgraph(p5, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); /* size of dependency graph should be 2 (p5 and its call target) */
  /* Check the contents of the deplist for p3 */
  i = 0;
  do {
    p = cst->g->deplists[0][i++];
    printf("[0][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[0][0] == p5); /* vertex p5 */
  TEST_ASSERT(cst->g->deplists[0][1] == p);  /* call target */
  TEST_ASSERT(cst->g->deplists[0][2] == p);  /* second instance of same call target */
  TEST_ASSERT(cst->g->deplists[0][3] == NULL); /* NULL terminator */

  /* Check the contents of the deplist for the call target, p */
  i = 0;
  do {
    p = cst->g->deplists[1][i++];
    printf("[1][%2d]  pattern %p\n", i, (void *) p);
  } while (p);

  p = binding_pattern_value(C->bt, refKEEP);
  TEST_ASSERT(p);

  TEST_ASSERT(cst->g->deplists[1][0] == p);    /* vertex p */
  TEST_ASSERT(cst->g->deplists[1][1] == NULL); /* NULL terminator */

  print_cfgraph(cst->g); 

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 2);
  print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == 2);
  TEST_ASSERT(cst->g->order->elements[0].vnum == 1); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -2); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[1].vnum == 0);  /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[1].cnum == -1); /* correct component number */

  pattern_free(p3);
  pattern_free(p4);
  pattern_free(p5);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing dynamic expansion of cfgraph (ensure_vertex_space)'");

  pexl_free_Context(C);
  C = pexl_new_Context();
  cst = C->cst;
  TEST_ASSERT(C->bt->next == 0);

  pexl_free_Expr(t1); pexl_free_Expr(t2);
  
  /* Construct a long call chain that ends in a call to tnum */
  tnum = pexl_match_any(C, 3);
  TEST_ASSERT(tnum);
  t1 = tnum;
  for(i = 0; i < (3 * EXP_CFG_INITIAL_SIZE); i++) {
    p = pattern_new(t1, C->bt, env, &stat);
    TEST_ASSERT(p);
    free_expr(t1);

    ref = env_bind(C->bt, env, i+300, env_new_value(Epattern_t, 0, (void *)p));
    TEST_ASSERT(!pexl_Ref_notfound(ref));
    TEST_ASSERT(ref == i);
    p->bt = C->bt;
    p->env = env;

    Pattern *temp = binding_pattern_value(C->bt, ref);
    TEST_ASSERT(temp);

    t2 = pexl_call(C, ref);
    TEST_ASSERT(t2);
    t1 = t2;
  }
  if (PRINTING) {
    printf("bt is: "); print_BindingTable(C);
    printf("pexl_Expr *is: "); pexl_print_Expr(t2, 1, NULL);
    printf("Entire call chain is: \n");
  }
  i = 1;
  t4 = t2;
  while (t4->node->tag == TOpenCall) {
    set_ref_from_node(ref, t4->node);
    p = binding_pattern_value(C->bt, ref);
    assert(p);
    assert(p->tree);
    if (PRINTING) printf("%5d: exp %p calls exp %p\n", i, (void *) t4, (void *) p->tree);
    t4 = p->tree; i++;
  }
  if (t4->node->tag == TSeq) 
    printf("%5d: exp %p has tag TSeq\n", i, (void *) t4);
  else {
    printf("%5d: exp %p has unexpected tag %d", i, (void *) t4, t4->node->tag);
    TEST_FAIL("Unexpected tag at the first non-call");
  }
  TEST_ASSERT(i == (3 * EXP_CFG_INITIAL_SIZE) + 1); /* Wrong length of call chain */

  printf("expansion done\n");
  fflush(NULL);


  p = pattern_new(t2, C->bt, env, &stat);
  TEST_ASSERT(p);
  p->env = env;		/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  C->cst = cst;
  cst->pat = p;

  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);
  cst->g = NULL;

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  printf("stat = %d\n", stat);
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  printf("size = %u\n", cst->g->next);
  TEST_ASSERT(cst->g->next == (3 * EXP_CFG_INITIAL_SIZE) + 1); /* wrong size of dependency graph */

  if (PRINTING) print_cfgraph(cst->g);

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == (3 * EXP_CFG_INITIAL_SIZE) + 1); /* wrong size of toposort result */
  if (PRINTING) print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == (3 * EXP_CFG_INITIAL_SIZE) + 1); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == (3 * EXP_CFG_INITIAL_SIZE)); /* first vertex in first component */
  TEST_ASSERT(cst->g->order->elements[0].cnum == - ((3 * EXP_CFG_INITIAL_SIZE) + 1)); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[3 * EXP_CFG_INITIAL_SIZE].vnum == 0); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[3 * EXP_CFG_INITIAL_SIZE].cnum == -1); /* correct component number */

  pattern_free(p);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing dynamic expansion of deplist (ensure_deplist_space)'");

  pexl_free_Context(C);
  C = pexl_new_Context();
  cst = C->cst;

  free_expr(tstr);
  /* Construct a pattern that calls tstr many times */
  tstr = pexl_match_string(C, "hello");
  TEST_ASSERT(tstr);
  p = pattern_new(tstr, C->bt, env, &stat);
  TEST_ASSERT(p);
  free_expr(tstr);

  p->env = env;
  ref = env_bind(C->bt, env, i+900, env_new_value(Epattern_t, 0, (void *)p));
  TEST_ASSERT(!pexl_Ref_notfound(ref));
  t2 = pexl_call(C, ref);
  TEST_ASSERT(t2);
  for(i = 0; i < (3 * EXP_DEPLIST_INITIAL_SIZE); i++) {
    t2 = pexl_seq_f(C, t2, pexl_call(C, ref)); /* also tests correctassociativity */
    TEST_ASSERT(t2);
  }
  if (PRINTING) {
    printf("pexl_Env env is: "); print_BindingTable(C);
    printf("pexl_Expr *is: "); pexl_print_Expr(t2, 1, NULL);
  }
  p = pattern_new(t2, C->bt, env, &stat);
  TEST_ASSERT(p);
  p->env = env;			/* compilation env */
  compstate_free(cst);
  cst = compstate_new();
  TEST_ASSERT(cst);
  cst->pat = p;
  
  reset_compilation_state(cst->pat);
  cfgraph_free(cst->g);

  stat = fix_open_calls(cst);
  TEST_ASSERT(stat == 0);
  stat = build_cfgraph(p, &(cst->g));
  TEST_ASSERT(stat == 0);
  TEST_ASSERT(cst->g);
  TEST_ASSERT(cst->g->next == 2); /* wrong size of dependency graph */
  i = 1;
  while (cst->g->deplists[0][i]) i++;
  i--;			   /* do not count dl[0], the vertex itself */
  TEST_ASSERT(i == (3 * EXP_DEPLIST_INITIAL_SIZE) + 1); /* wrong size of dependency list */

  print_cfgraph(cst->g); 

  stat = sort_cfgraph(cst->g);
  printf("stat = %d\n", stat);
  TEST_ASSERT(!stat);
  TEST_ASSERT(cst->g->order);
  TEST_ASSERT(cst->g->order->size == 2); /* wrong size of toposort result */
  print_sorted_cfgraph(cst->g);
  TEST_ASSERT(cst->g->order->top == 2); /* size of order array */
  TEST_ASSERT(cst->g->order->elements[0].vnum == 1); /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[0].cnum == -2); /* correct component number */
  TEST_ASSERT(cst->g->order->elements[1].vnum == 0);  /* first component, one vertex */
  TEST_ASSERT(cst->g->order->elements[1].cnum == -1); /* correct component number */

  pattern_free(p);
  compstate_free(cst);

  /* ----------------------------------------------------------------------------- */

  context_free(C);

  TEST_END();

  return 0;
}

