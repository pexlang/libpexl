/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest.c  TESTING analyze.c                                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "print.h"
#include "ast-print.h"
#include "analyzenew.h"
#include "ast.h"

#include "../test.h"

#define YES 1
#define NO 0

#define PRINTING YES


int main(int argc, char **argv) {

  int stat, nullable;
  Charset cs;
  ast_expression *exp, *exp1, *exp2, *exp3;
  unsigned char c;
  uint32_t min, max;
  String *str, *str2, *epsilon;

/*   Value val; */
  // pexl_Expr *tstr, *calltree;
  // pexl_Expr *consttree; 
  // pexl_Expr *t1, *t2, *t3, *t4; 
  // pexl_Expr *A, *S;

  // pexl_PackageTable *m; 
  // pexl_Binary *pkg;
  // pexl_Env toplevel, env, g;
  // SymbolTable *st;
  // pexl_Ref ref, refA, refS;

/*   size_t bufsize = 16;	/\* 15 chars and a NULL byte *\/ */
/*   char *buf = (char *)alloca(bufsize); */


  TEST_START(argc, argv);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Computing expression properties");
  // -----------------------------------------------------------------------------

  TEST_EXPECT_WARNING;
  stat = exp_hascaptures_new(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  TEST_EXPECT_WARNING;
  stat = exp_itself_hascaptures_new(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  // stat = exp_patlen(NULL, &min, &max);
  // TEST_ASSERT(stat == PEXL_ERR_NULL);

  // stat = exp_nofail(NULL);
  // TEST_ASSERT(stat == PEXL_ERR_NULL);

  TEST_EXPECT_WARNING;
  stat = exp_nullable_new(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  TEST_EXPECT_WARNING;
  stat = exp_headfail_new(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_needfollow_new(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  TEST_EXPECT_WARNING;
  stat = exp_getfirst_new(NULL, NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  TEST_EXPECT_WARNING;
  stat = exp_getfirst_new(NULL, NULL, &cs);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  TEST_EXPECT_WARNING;
  stat = exp_getfirst_new(NULL, fullset, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  TEST_EXPECT_WARNING;
  stat = exp_getfirst_new(NULL, fullset, &cs);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  // C = context_new();
  // TEST_ASSERT(C);
  // TEST_ASSERT(C->bt);
  // toplevel = 0;			// root env has id 0

  // tstr = pexl_match_string(C, "hello");
  // TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  // node = tstr->node;
  // TEST_ASSERT(node);		/* precondition for next tests */

  // stat = exp_getfirst(node, NULL, NULL);
  // TEST_ASSERT(stat == PEXL_ERR_NULL);
  // stat = exp_getfirst(node, NULL, &cs);
  // TEST_ASSERT(stat == PEXL_ERR_NULL);
  // stat = exp_getfirst(node, fullset, NULL);
  // TEST_ASSERT(stat == PEXL_ERR_NULL);


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Analyzing primitive patterns");

  printf("Checking literal string 'hello'\n");

  exp = make_bytestring_from("hello", 5);
  TEST_ASSERT(exp->type == AST_BYTESTRING);
  
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 5) && (max == 5));
  nullable = exp_nullable_new(exp);
  TEST_ASSERT(!nullable);
  TEST_ASSERT(!exp_nofail_new(exp));
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  // literal string pattern can fail
  TEST_ASSERT(!exp_nofail_new(exp));
  // literal string pattern is NOT headfail, unless the string has length 1
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  // literal non-empty string cannot be optimized using a follow set
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset can be used in a test, so return value is 1
  TEST_ASSERT(stat == 1);
  print_charset(cs.cs);
  // Now check the contents of cs for 'h'
  for (c = 0; c < 255; c++)
    if (c == 'h')
      TEST_ASSERT(testchar(cs.cs, c)); // cs should contain 'h'
    else
      TEST_ASSERT(!testchar(cs.cs, c)); // and only 'h'
  // TODO: printf("Checking True created via empty literal string\n");
  free_expression(exp);

  exp = make_true();
  TEST_ASSERT(exp);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  nullable = exp_nullable_new(exp);
  // True pattern is nullable
  TEST_ASSERT(nullable);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0)));
  // True pattern is nofail
  TEST_ASSERT(exp_nofail_new(exp));

  // TTrue is not headfail
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  // TTrue does not need follow set
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == 0);	// firstset cannot be used as test
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); // set should contain every char
  free_expression(exp);
  printf("Checking ANYBYTE\n");
  exp = make_anybyte();
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 1) && (max == 1));
  nullable = exp_nullable_new(exp);
  TEST_ASSERT(!nullable);
  TEST_ASSERT(!exp_nofail_new(exp));
  TEST_ASSERT(exp_headfail_new(exp) == YES);
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == 1);	// firstset can be used in a test
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); // should contain every char
// TODO:
//  pexl_free_Expr(exp);
//  printf("Checking long bytestring\n");
  free_expression(exp);

  uint32_t anybyte_sequence_length = 10000;
  printf("Checking long sequence of ANYBYTE\n");

  confess("TEMP", "After converting transform_AST() to defunc. CPS, increase this number");

  exp = NULL;
  for (uint32_t i = 0; i < anybyte_sequence_length; i++)
    exp = build_seq(exp, make_anybyte());
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_nodecount(exp) == 2 * anybyte_sequence_length - 1);
  TEST_ASSERT(exp->type == AST_SEQ);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  // A bounded-length pattern should have accurate min and max values
  TEST_ASSERT(min == anybyte_sequence_length);
  TEST_ASSERT(max == anybyte_sequence_length);
  TEST_ASSERT(!exp_nullable_new(exp));
  TEST_ASSERT(!exp_nofail_new(exp));
  TEST_ASSERT(!exp_headfail_new(exp));
  printf("needfollow is %d\n", exp_needfollow_new(exp));
  TEST_ASSERT(!exp_needfollow_new(exp));	 /* exp does not need follow set */

  // The firstset can be used in a test
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == YES);
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); // should contain every char
  free_expression(exp);

  printf("Checking NEG LOOKAHEAD at ANYBYTE\n");
  exp = make_predicate(AST_NEGLOOKAHEAD, make_anybyte());
  TEST_ASSERT(exp);
  if (PRINTING) {
    ast_print_tree(exp);
    printf("nodecount is %u\n", exp_nodecount(exp));
  }
  TEST_ASSERT(exp_nodecount(exp) == 2);
  TEST_ASSERT(exp->type == AST_NEGLOOKAHEAD);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(exp_nullable_new(exp));
  TEST_ASSERT(!exp_nofail_new(exp));
  TEST_ASSERT(!exp_headfail_new(exp));
  TEST_ASSERT(!exp_needfollow_new(exp));	/* exp does not need follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  // firstset cannot be used as test
  TEST_ASSERT(stat == NO);
  for (c = 0; c < 255; c++)
    TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain no chars */
  free_expression(exp);

  printf("Checking False\n");
  exp = make_false();
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(!exp_nullable_new(exp));
  TEST_ASSERT(exp_nofail_new(exp) == 0);
  TEST_ASSERT(exp_headfail_new(exp) == YES);
  TEST_ASSERT(!exp_needfollow_new(exp));	/* exp does not need follow set */
  // firstset can be used as test
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == YES);
  for (c = 0; c < 255; c++)
    TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain no chars */
  free_expression(exp);

  printf("Checking BYTESET\n");
  exp = make_byteset();
  TEST_ASSERT(exp);
  TEST_ASSERT(exp->type == AST_BYTESET);
  TEST_ASSERT(byteset_emptyp(exp->byteset));
  byteset_add(exp->byteset, 'A');
  byteset_add(exp->byteset, 'C');
  byteset_add(exp->byteset, 'E');
  TEST_ASSERT(!byteset_emptyp(exp->byteset));
  if (PRINTING) print_charset(exp->byteset->i32);

  // TODO: Some analyses, like 'nullable', assume that a byteset
  // cannot be empty, else it is equivalent to False.  But an empty
  // byteset is (and needs to be) representable.  'byteElision' should
  // be one of a series of passes in which we lower the AST, and must
  // happen BEFORE analyses like 'nullable'.

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 1) && (max == 1));
  TEST_ASSERT(exp_nullable_new(exp) == NO);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == YES);

  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == YES);	// firstset can be used in a test
  if (PRINTING) print_charset(cs.cs);
  for (c = 0; c < 255; c++)
    if (( c == 'A') || (c == 'C') || (c == 'E'))
      TEST_ASSERT(testchar(cs.cs, c));  // firstset should contain only A, C, E
    else
      TEST_ASSERT(!testchar(cs.cs, c)); // and no other chars
  free_expression(exp);

  exp = make_byteset();
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 1) && (max == 1));
  nullable = exp_nullable_new(exp);
  TEST_ASSERT(nullable == NO);
  // Ensure that nullable() agrees with patlen()
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0)));
  TEST_ASSERT(exp_nofail_new(exp) == 0);
  TEST_ASSERT(exp_headfail_new(exp) == YES);
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == YES);	      // firstset can be used in a test
  for (c = 0; c < 255; c++)
    TEST_ASSERT(!testchar(cs.cs, c)); // firstset should be empty
  free_expression(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'seq'");

  exp1 = make_bytestring_from("a", 1);   // "a"
  exp2 = make_anybyte();
  exp3 = make_repetition(0, 0, exp2);	 // .*
  exp = make_seq(exp1, exp3);		 // "a" .*
  TEST_ASSERT(exp);
  TEST_ASSERT(ast_child_count(exp) == 2);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED);
  TEST_ASSERT(min == 1);
  TEST_ASSERT(max == 0);
  TEST_ASSERT(exp_nullable_new(exp) == NO);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == YES);
  TEST_ASSERT(exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset can be used in a test
  TEST_ASSERT(stat == YES);

  for (c = 0; c < 255; c++)
    if (c == 'a')
      TEST_ASSERT(testchar(cs.cs, c));  // firstset should contain 'a'
    else
      TEST_ASSERT(!testchar(cs.cs, c)); // and nothing else

  // Free 'exp' and all of its subexpressions
  free_expression(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'choice'");

  exp1 = make_bytestring_from("ab", 2);
  exp2 = make_bytestring_from("c", 1);
  exp = make_choice(exp1, exp2);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 1) && (max == 2));
  TEST_ASSERT(exp_nullable_new(exp) == NO);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  TEST_ASSERT(exp_needfollow_new(exp));	/* exp would benefit from follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset can be used in a test
  TEST_ASSERT(stat == YES);
  for (c = 0; c < 255; c++)
    if (c == 'a' || c == 'c')
      TEST_ASSERT(testchar(cs.cs, c));  // firstset should contain a, c
    else
      TEST_ASSERT(!testchar(cs.cs, c)); // and nothing else
  free_expression(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'negative lookahead'");

  exp1 = make_bytestring_from("A", 1);
  exp = make_predicate(AST_NEGLOOKAHEAD, exp1);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  TEST_ASSERT(!exp_needfollow_new(exp));	/* exp would NOT benefit from follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == NO);
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(!testchar(cs.cs, c)); // firstset should contain all but A
    else
      TEST_ASSERT(testchar(cs.cs, c));
  free_expression(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookahead'");

  exp1 = make_bytestring_from("A", 1);
  exp = make_predicate(AST_LOOKAHEAD, exp1);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == YES);
  TEST_ASSERT(!exp_needfollow_new(exp));	/* exp would NOT benefit from follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == YES);	// firstset can be used in a test
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c));
    else
      TEST_ASSERT(!testchar(cs.cs, c));
  free_expression(exp);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookbehind'");

  if (PRINTING) printf("Look back at string literal\n");

  exp1 = make_bytestring_from("A", 1);
  exp = make_predicate(AST_LOOKBEHIND, exp1);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  /* The firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == NO);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c));
  free_expression(exp);
  
  if (PRINTING) printf("Look back at \"A\"? which has variable length\n");

  exp1 = make_bytestring_from("A", 1);
  exp2 = make_repetition(0, 1, exp1);
  exp = make_predicate(AST_LOOKBEHIND, exp2);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  // Bounded because it *consumes* exactly zero bytes
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  /* The firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == NO);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c));
  free_expression(exp);

  if (PRINTING) printf("Look back at \"AB\"/\"CD\" which has length 2\n");

  exp1 = make_bytestring_from("AB", 2);
  exp2 = make_bytestring_from("CD", 2);
  exp3 = make_choice(exp1, exp2);
  exp = make_predicate(AST_LOOKBEHIND, exp3);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);

  // First examine the choice expression
  stat = exp_patlen_new(exp3, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 2));

  // Now the full expression, which is a lookbehind
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));

  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset cannot be used as test, exp accepts epsilon
  TEST_ASSERT(stat == NO);
  // The byteset cs should contain all chars
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c));
  free_expression(exp);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'rep'");

  exp1 = make_bytestring_from("A", 1);
  exp = make_repetition(0, 0, exp1);
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED);
  TEST_ASSERT(min == 0);
  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == YES);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  TEST_ASSERT(exp_needfollow_new(exp));	/* exp would benefit from follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset cannot be used in a test
  TEST_ASSERT(stat == NO);
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all chars */
  free_expression(exp);
  
  exp1 = make_bytestring_from("A", 1);
  exp = make_repetition(3, 0, exp1); // "A"{3,}
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED);
  TEST_ASSERT(min == 3);
  TEST_ASSERT(exp_nullable_new(exp) == NO);
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  TEST_ASSERT(exp_needfollow_new(exp));	/* exp would benefit from follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == YES);	// firstset can be used in a test
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c));  // firstset should contain A
    else
      TEST_ASSERT(!testchar(cs.cs, c)); // and nothing else
  free_expression(exp);
  
  exp1 = make_bytestring_from("ABC", 3);
  exp = make_repetition(0, 1, exp1); // "ABC"?
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 3));
  TEST_ASSERT(exp_nullable_new(exp) == YES);
  TEST_ASSERT(exp_nofail_new(exp) == YES);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  TEST_ASSERT(exp_needfollow_new(exp));	/* exp would benefit from follow set */
  stat = exp_getfirst_new(exp, fullset, &cs);
  TEST_ASSERT(stat == NO);	// firstset cannot be used in a test
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); // cs should contain all chars
  free_expression(exp);
  
  exp1 = make_bytestring_from("ABC", 3);
  exp = make_repetition(0, 3, exp1); // "ABC"{0,3}
  TEST_ASSERT(exp);
  TEST_ASSERT(exp_hascaptures_new(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == NO);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 9));
  nullable = exp_nullable_new(exp);
  TEST_ASSERT(nullable == YES);
  // Ensure that nullable and patlen agree with each other
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0)));
  TEST_ASSERT(exp_nofail_new(exp) == YES);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  /* This exp would benefit from follow set */
  TEST_ASSERT(exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset cannot be used as test
  TEST_ASSERT(stat == NO);
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all chars */
  free_expression(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Capture");

  exp1 = make_bytestring_from("AB", 2);
  str = String_from_literal("hi");
  exp = make_capture(str, exp1);
  TEST_ASSERT(exp);

  TEST_ASSERT(exp_hascaptures_new(exp1) == NO);
  TEST_ASSERT(exp_itself_hascaptures_new(exp1) == NO);

  TEST_ASSERT(exp_hascaptures_new(exp) == YES);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == YES);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 2));
  nullable = exp_nullable_new(exp);
  TEST_ASSERT(nullable == NO);
  // Check that nullable and patlen agree
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0)));
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset can be used as test
  TEST_ASSERT(stat == YES);
  // The byteset cs should contain only A
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c));
    else
      TEST_ASSERT(!testchar(cs.cs, c));
  free_expression(exp);

  str = String_from_literal("hi");
  exp1 = make_bytestring_from("AB", 2);
  exp2 = make_capture(str, exp1);
  TEST_ASSERT(exp2);
  exp3 = copyAST(exp2);
  TEST_ASSERT(exp3 && (exp3 != exp2));
  exp = make_seq(exp2, exp3);
  TEST_ASSERT(exp);

  ast_print_tree(exp);

  TEST_ASSERT(exp_hascaptures_new(exp) == YES);
  TEST_ASSERT(exp_itself_hascaptures_new(exp) == YES);
  stat = exp_patlen_new(exp, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 4) && (max == 4));
  nullable = exp_nullable_new(exp);
  TEST_ASSERT(nullable == NO);
  // Check that nullable and patlen agree
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0)));
  TEST_ASSERT(exp_nofail_new(exp) == NO);
  TEST_ASSERT(exp_headfail_new(exp) == NO);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow_new(exp));
  stat = exp_getfirst_new(exp, fullset, &cs);
  // The firstset can be used as test
  TEST_ASSERT(stat == YES);
  // Now check the contents of cs, which should contain only A
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c));
    else
      TEST_ASSERT(!testchar(cs.cs, c));
  free_expression(exp);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Insert, or \"constant captures\"");

  str = String_from_literal("abc");
  TEST_EXPECT_WARNING;
  exp = make_insert(NULL, str);
  TEST_ASSERT_NULL(exp);	// name cannot be NULL

  TEST_EXPECT_WARNING;
  exp = make_insert(str, NULL);
  TEST_ASSERT_NULL(exp);	// data (String) cannot be NULL

  epsilon = String_from_literal("");
  TEST_EXPECT_WARNING;
  exp = make_insert(epsilon, str);
  TEST_ASSERT_NULL(exp);	// name cannot be empty string

  str2 = String_from_literal("x");
  exp = make_insert(str2, str);
  TEST_ASSERT(exp);
  printf("Printing an insert AST:\n");
  ast_print_tree(exp);
  free_expression(exp);		// frees str2 and str

  str = String_from_literal("foobar");
  exp = make_insert(str, epsilon); // data can be empty string
  TEST_ASSERT(exp);
  free_expression(exp);
  
#if 0
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexl_References");

  m = packagetable_new(1);
  TEST_ASSERT(m);

  context_free(C);
  C = context_new();
  TEST_ASSERT(C);
  env = 0;

  st = symboltable_new(1, 1);
  TEST_ASSERT(st);
  /* Add empty string to symbol table using NULL instead of "" */
  stat = symboltable_add(st, NULL, 1, 0, 1, 0, 0, NULL);
  TEST_ASSERT(stat == 0);
  stat = symboltable_add(st, "foo", 1, 0, 1, 0, 0, NULL);
  TEST_ASSERT(stat == 1);

  pkg = binary_new();
  TEST_ASSERT(pkg);
  binary_free(pkg);

  /* Anonymous binding has string ID 0 */
  ref = env_bind(C->bt, env, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  id = context_intern(C, "foobar", 6);
  TEST_ASSERT(id >= 0);		// non-negative means success
  ref = env_bind(C->bt, env, id, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == 1);	// second binding in bt is at index 1

  calltree = pexl_call(C, ref);
  TEST_ASSERT(calltree);
  TEST_ASSERT(calltree->child->a.bt == C->bt);
  TEST_ASSERT(calltree->child->b.index == ref);
  pexl_free_Expr(calltree);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Non-recursive block using anonymous exps");

  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refS = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  A = pexl_call(C, refS);	/* A -> S */
  S = pexl_match_epsilon(C);	/* S -> true */
  exp = A->child;
  TEST_ASSERT(exp);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures(exp) == NO);
  TEST_EXPECT_WARNING; // about encountering an open call
  stat = exp_patlen(exp, &min, &max);
  TEST_EXPECT_WARNING; // about encountering an open call
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  TEST_EXPECT_WARNING; // about encountering an open call
  TEST_ASSERT(exp_nullable(exp) == PEXL_EXP__ERR_OPENFAIL);
  TEST_EXPECT_WARNING; // about encountering an open call
  TEST_ASSERT(exp_nofail(exp) == PEXL_EXP__ERR_OPENFAIL);
  TEST_EXPECT_WARNING; // about encountering an open call
  TEST_ASSERT(exp_headfail(exp) == PEXL_EXP__ERR_OPENFAIL);
  TEST_ASSERT(exp_needfollow_new(exp) == PEXL_EXP__ERR_OPENFAIL);
  TEST_EXPECT_WARNING; // about encountering an open call
  stat = exp_getfirst_new(exp, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);

  pexl_free_Expr(A); pexl_free_Expr(S);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Recursive block using anonymous exps");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  A = pexl_call(C, ref);	/* A -> A */

  print_BindingTable(C); 
  printf("pexl_Expr *at refA: ");
  
  pexl_free_Expr(A);
  printf("-----\n");
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  id = context_intern(C, "A", 1);
  TEST_ASSERT(id >= 0);
  ref = env_bind(C->bt, g, id, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		 pexl_seq_f(C, 
			 make_bytestring_from(C, "a"),
			 pexl_seq_f(C, 
				 pexl_call(C, ref),
				 make_bytestring_from(C, "b"))),
		 pexl_match_epsilon(C));

  TEST_ASSERT(A);		/* A will contain a TOpenCall */

  exp = A->child;
  TEST_ASSERT(exp);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures(exp) == NO);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_patlen(exp, &min, &max);
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  /* This exp is nullable, and we never get to the OPEN CALL */
  TEST_ASSERT(exp_nullable(exp) == 1);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nofail(exp) == 1);
  /* This exp has an OPEN CALL */
  TEST_ASSERT(exp_headfail(exp) == PEXL_EXP__ERR_OPENFAIL);
  /* This exp benefits from needfollow, and we never get to the OPEN CALL */
  TEST_ASSERT(exp_needfollow_new(exp) == 1);
  stat = exp_getfirst_new(exp, fullset, &cs); 
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);

  pexl_free_Expr(A);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with one binding to unspecified");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  id = context_intern(C, "A", 1);
  TEST_ASSERT(id >= 0);
  ref = env_bind(C->bt, g, id, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));

  id = context_intern(C, "A", 1);
  TEST_ASSERT(id >= 0);
  S = pexl_call(C, ref);	/* S -> A */
  TEST_ASSERT(S);

  exp = S->child;
  TEST_ASSERT(exp);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(exp) == NO);
  TEST_ASSERT(exp_itself_hascaptures(exp) == NO);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_patlen(exp, &min, &max);
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nullable(exp) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nofail(exp) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_headfail(exp) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_needfollow_new(exp) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_getfirst_new(exp, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  #endif

  // pexl_free_Expr(S);

  // symboltable_free(st);
  // packagetable_free(m);
  // context_free(C);

  TEST_END();

  return 0;
}

