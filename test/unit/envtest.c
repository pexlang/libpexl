/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  envtest.c  TESTING the new env data structures                           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "env.h"
#include "print.h"

#include "../test.h"

#define YES 1
#define NO 0

int main(int argc, char **argv) {
  pexl_Env toplevel, env2, env3;
  BindingTable *bt;
  Binding *binding; 
  pexl_Index i;
  pexl_Ref ref;
  int stat;
  
  TEST_START(argc, argv);

  bt = new_BindingTable();
  TEST_ASSERT(bt);

  TEST_ASSERT(bt->capacity == ENV_INIT_SIZE);
  TEST_ASSERT(bt->next == 0);
  // Root env has no parent. Return value does not indicate an error,
  // only that the parent of the root is NOT FOUND
  pexl_Env env = env_get_parent(bt, 0);
  TEST_ASSERT(env == PEXL_ENV__NOT_FOUND);
  TEST_ASSERT(invalid_env(env));
  for (i = 1; i < MAX_ENV_NODES; i++)
    if (env_get_parent(bt, i) != -1)
      TEST_FAIL("Parent table entry not initialized properly at index %d", i);
  TEST_ASSERT(true);		// Add to our count of passed tests :)

  toplevel = 0;			// Id of root env
  env2 = env_new(bt, toplevel);
  printf("env2 is %d.  parent is %d.\n", env2, env_get_parent(bt, env2));
  TEST_ASSERT(env2);
  TEST_ASSERT(env_get_parent(bt, env2) == toplevel);
  
  /* ----------------------------------------------------------------------------- */
  printf("Printing the empty top level environment: (should produce empty table)\n");
  print_env_internal(toplevel, bt, NULL);

  i = PEXL_ITER_START;
  binding = env_local_iter(bt, toplevel, &i);
  TEST_ASSERT(!binding);	/* empty env should yield NULL on first iteration */

  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in top level\n");

  ref = env_bind(bt, toplevel, 1, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(ref == 0); /* first binding should be at index 0 of bindings array */

  ref = env_bind(bt, toplevel, 1, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(ref == PEXL_ENV__ERR_EXISTS); /* cannot create another binding with same name */

  /* The next binding should end up in index 1 of binding table. */
  ref = env_bind(bt, toplevel, 111, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(ref == 1);	    /* second binding should be at index 1 of bindings array */

  ref = env_bind(bt, toplevel, 2, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  ref = env_bind(bt, toplevel, 3, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  print_env_internal(toplevel, bt, NULL);

  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding->val.type == Eunspecified_t); /* checking the value we bound is correct */
  TEST_ASSERT(binding->val.data == 0);		    /* checking the value we bound is correct */
  TEST_ASSERT(binding->val.ptr == NULL);	    /* checking the value we bound is correct */

  TEST_ASSERT(bt->next == 4);
  pexl_Index old_size = bt->next;
  
  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in env2 (child of top level)\n");

  env2 = env_new(bt, toplevel);
  TEST_ASSERT(env_get_parent(bt, env2) == toplevel);	

  ref = env_bind(bt, env2, 100, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding->env == env2);

  ref = env_bind(bt, env2, 101, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding->env == env2);

  ref = env_bind(bt, env2, 102, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding->env == env2);
  
  ref = env_bind(bt, env2, 100, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(ref == PEXL_ENV__ERR_EXISTS);

  print_env_internal(env2, bt, NULL);

  TEST_ASSERT(bt->next == old_size + 3); /* wrong number of bindings? */
  old_size = bt->next;

  /* ----------------------------------------------------------------------------- */
  printf("Creating some recursive bindings in env2\n");

  ref = env_bind(bt, env2, 200, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  ref = env_bind(bt, env2, 201, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  ref = env_bind(bt, env2, 202, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  
  ref = env_bind(bt, env2, 203, env_new_value(Eunspecified_t, 0, NULL)); 
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  
  print_env_internal(env2, bt, NULL);

  TEST_ASSERT(bt->next == 4 + old_size);
  old_size = bt->next;
  
  /* ----------------------------------------------------------------------------- */
  printf("Creating some bindings in env3 (child of env2)\n");

  env3 = env_new(bt, env2);
  TEST_ASSERT(env3);		
  
  ref = env_bind(bt, env3, 300, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  ref = env_bind(bt, env3, 301, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  print_env_internal(env3, bt, NULL);

  TEST_ASSERT(bt->next == old_size + 2); /* wrong number of bindings? */
  
  /* ----------------------------------------------------------------------------- */
  printf("Checking that the 'compiled' flag of a pattern is reset with rebind, unbind\n");

  ref = 1;
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding);
  /* Should get the binding with name 111 */
  TEST_ASSERT(binding->namehandle == 111);
  
  stat = env_rebind(bt, ref, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!stat);
  TEST_ASSERT(binding->val.type == Eunspecified_t);
  TEST_ASSERT(binding->val.data == 0);
  TEST_ASSERT(binding->val.ptr == NULL);
  
  stat = env_rebind(bt, ref, env_new_value(Eunspecified_t, 1234, NULL));
  TEST_ASSERT(!stat);
  TEST_ASSERT(binding->val.type == Eunspecified_t);
  TEST_ASSERT(binding->val.data == 1234);
  TEST_ASSERT(binding->val.ptr == NULL);
  
  ref = 100;		/* DOES NOT EXIST */
  stat = env_rebind(bt, ref, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(stat == PEXL_ENV__NOT_FOUND);
  
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(!binding);

  /* ----------------------------------------------------------------------------- */
  printf("Checking unbind\n");

  env_unbind(bt, ref);
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(!binding);
  
  ref = 1;
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding);
  TEST_ASSERT(binding->namehandle == 111);
  TEST_ASSERT(binding->val.type == Eunspecified_t);
  TEST_ASSERT(binding->val.data == 1234);
  TEST_ASSERT(binding->val.ptr == NULL);

  env_unbind(bt, ref);
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(!binding);

  ref = 2;
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding);

  env_unbind(bt, ref);
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(!binding);

  /* Make sure that rebind fails on a deleted (unbound) binding */
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(!binding);
  stat = env_rebind(bt, ref, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(stat == PEXL_ENV__NOT_FOUND);

  print_env_internal(toplevel, bt, NULL);

  /* ----------------------------------------------------------------------------- */
  printf("Testing env_lookup and env_local_lookup\n");

  /* env3 */
  ref = env_local_lookup(bt, env3, 300);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env3));
  
  ref = env_local_lookup(bt, env3, 301);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env3));

  // Cannot look up an anonymous binding (name handle 0) by name
  ref = env_local_lookup(bt, env3, 0);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_local_lookup(bt, env3, 333);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  /* env2 */
  ref = env_local_lookup(bt, env2, 203);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  ref = env_local_lookup(bt, env2, 100);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  ref = env_local_lookup(bt, env2, 0);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_local_lookup(bt, env2, 300);
  TEST_ASSERT(pexl_Ref_invalid(ref));


  /* env2 */
  ref = env_local_lookup(bt, env2, 100);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  ref = env_local_lookup(bt, env2, 102);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  // Cannot look up an anonymous binding (name handle 0) by name
  ref = env_local_lookup(bt, env2, 0);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_local_lookup(bt, env2, 111);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  /* top level */
  ref = env_local_lookup(bt, toplevel, 1);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == toplevel));

  ref = env_local_lookup(bt, toplevel, 3);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == toplevel));

  // Cannot look up an anonymous binding (name handle 0) by name
  ref = env_local_lookup(bt, toplevel, 0);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_local_lookup(bt, toplevel, 999);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  // This binding was deleted, so we should not find it
  ref = env_local_lookup(bt, toplevel, 111);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_local_lookup(bt, toplevel, 1);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == toplevel));
  
  /* env_lookup in env2 should find env2 and top bindings */

  ref = env_lookup(bt, env2, 3);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == toplevel));

  ref = env_local_lookup(bt, env2, 999);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  /* env_lookup in env2 should find env2 and toplevel bindings */

  ref = env_lookup(bt, env2, 1);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == toplevel));

  // Binding 2 was deleted earlier
  ref = env_lookup(bt, env2, 2);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_lookup(bt, env2, 100);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  ref = env_lookup(bt, env2, 999);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  /* env_lookup in env3 should find env3, env2, and toplevel bindings */

  ref = env_lookup(bt, env3, 1);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == toplevel));

  // Cannot look up an anonymous binding (name handle 0) by name
  ref = env_lookup(bt, env3, 0);
  TEST_ASSERT(pexl_Ref_invalid(ref));

  ref = env_lookup(bt, env3, 200);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  ref = env_local_lookup(bt, env3, 301);
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env3));

  /* ----------------------------------------------------------------------------- */
  printf("Testing env_local_iter\n");

  pexl_Ref last = -1;
  i = PEXL_ITER_START;
  while ((binding = env_local_iter(bt, toplevel, &i))) {
    last = i;
    printf("toplevel has binding named %d\n", binding->namehandle);
  }
  printf("last = %d\n", last);
  TEST_ASSERT(last == 3); /* last binding in toplevel is at index 3 */

  last = -1;
  i = PEXL_ITER_START;
  while ((binding = env_local_iter(bt, env3, &i))) {
    last = i;
    printf("env3 has binding named %d\n", binding->namehandle);
  }
  TEST_ASSERT(last == 12);	/* last binding is at index 12 */
  
  env2 = env_new(bt, toplevel);

  last = -1;
  i = PEXL_ITER_START;
  while ((binding = env_local_iter(bt, env2, &i))) {
    last = i;
    printf("THIS MESSAGE SHOULD NOT BE PRINTED!  %d\n", binding->namehandle);
  }
  TEST_ASSERT(last == -1);		/* no bindings in this fresh env */

  ref = env_bind(bt, env2, 666, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  env_unbind(bt, ref);
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(!binding);

  print_env_internal(env2, bt, NULL);

  i = PEXL_ITER_START;
  last = -1;
  while ((binding = env_local_iter(bt, env2, &i))) {
    last = 99999;
    printf("THIS MESSAGE SHOULD NOT BE PRINTED!  %d\n", binding->namehandle);
  }
  TEST_ASSERT(last == -1);     /* no undeleted bindings in this env */

  /* ----------------------------------------------------------------------------- */
  printf("Testing anon bindings\n");

  ref = env_bind(bt, env2, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));
  pexl_Ref anon1 = ref;

  ref = env_bind(bt, env2, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env2));

  print_env_internal(env2, bt, NULL);

  ref = env_bind(bt, env3, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  binding = env_get_binding(bt, ref);
  TEST_ASSERT(binding && (binding->env == env3));
  TEST_ASSERT(binding->namehandle == 0);            /* should be anon */
  TEST_ASSERT(binding->val.type == Eunspecified_t);
  TEST_ASSERT(binding->val.data == 0);

  // Go back and retrieve anon1, to ensure it is still there
  binding = env_get_binding(bt, anon1);
  TEST_ASSERT(binding);
  TEST_ASSERT(binding->env == env2);
  TEST_ASSERT(binding->namehandle == 0);    /* should be anon */
  TEST_ASSERT(binding->val.type == Eunspecified_t); /* did we get the right binding */
  TEST_ASSERT(binding->val.data == 0);		    /* did we get the right binding */
  
  /* ----------------------------------------------------------------------------- */
  printf("Testing dynamic expansion of binding table\n");
  printf("  Starting size of bt is %u/%u\n", bt->next, bt->capacity);
  for (i = bt->next; i < bt->capacity; i++) {
    ref = env_bind(bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
    if (pexl_Ref_invalid(ref))
      TEST_FAIL("Failed to fill existing capacity (%d) of binding table", bt->capacity);
  }
  printf("  Capacity of bt is now %u/%u\n", bt->next, bt->capacity);
  TEST_ASSERT(bt->capacity == bt->next); /* table should be full */

  ref = env_bind(bt, toplevel, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  printf("  Size of bt is now %u/%u\n", bt->next, bt->capacity);
  TEST_ASSERT(bt->capacity > bt->next); /* table should have expanded */

  free_BindingTable(bt);
  TEST_END();
}

