/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  findest.c  TESTING simd code in find.c                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>		/* exit() */
#include "find.h"
#include "time.h"
#include "print.h"

#include "../test.h"

#define YES 1
#define NO 0

#define PRINTING NO

/* Caller must free the return value */
static const char *readfile (const char *filename, size_t *filesize) {
  FILE *f = fopen(filename, "r");
  if (!f) return NULL;
  int stat = fseek(f, 0, SEEK_END);
  if (stat) {
    fprintf(stderr, "Failed fseek\n");
    fclose(f);
    return NULL;
  }
  long filelen = ftell(f);
  if (filelen < 0) {
    fprintf(stderr, "Invalid file size %ld\n", filelen);
    fclose(f);
    return NULL;
  }
  *filesize = (size_t) filelen;
  stat = fseek(f, 0, 0);
  char *contents = (char *) malloc(*filesize+1); 
  if (!contents) {
    fprintf(stderr, "malloc failed\n");
    return NULL;
  }
  size_t n = fread(contents, (size_t) *filesize, 1, f);
  if (n != 1) {			/* one item */
    fprintf(stderr, "fread failed\n");
    free(contents);
    contents = NULL;
  }
  contents[*filesize] = '\0';
  fclose(f);
  return contents;
}

static int
prep_simd_charset (Charset *cs, Charset *simd_cs, uint8_t *simd_char1) {
  SetMask_t simd_set;
  int simd_size = charset_to_simd_mask(cs, &simd_set, simd_char1); 
  if (simd_size < 0) {
    fprintf(stderr, "error %d from charset_to_simd_mask\n", simd_size);
    return simd_size;
  }
  if (simd_size != 0) {
    memcpy(&(simd_cs->cs), &simd_set, sizeof(SetMask_t));
  }
  return simd_size;
}

/*
  Compare searching the input using find_char() or find_charset(),
  which are the NON SIMD ROUTINES, versus find_simd().
*/
static void AB_test_find (const char *inputname,
			  const char *input, size_t len,
			  Charset *cs,
			  int reps) {
  TEST_ASSERT_NOT_NULL(input);
  TEST_ASSERT_NOT_NULL(cs);
  char char1;
  CharSetType cs_type = charsettype(cs, &char1);
  switch (cs_type) {
  case CharSetEmpty: printf("Charset is empty!\n"); break;
  case CharSetFull: printf("Charset is full!\n"); break;
  case CharSet1: printf("Charset has one char: 0x%02X\n", (unsigned) char1); break;
  case CharSet8: printf("Charset has 2-8 chars\n"); break;
  case CharSet: printf("Charset has 9-255 chars\n"); break;
  default:
    printf("Charset type unknown!\n");
    assert(0);			/* should not happen */
  }
  int count = 0;
  const char *ptr;
  clock_t t0 = clock();
  int i;
  for (i = 0; i < reps; i++) {
    ptr = input;
    count = 0;
    switch (cs_type) {
    case CharSet1:
      while (1) {
	ptr = find_char(char1, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++;
      }
      break;
    case CharSet8: case CharSet:
      while (1) {
	ptr = find_charset(cs, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++;
      }
      break;
    default:
      assert(0);
    } /* switch */
  } /* for reps */
  clock_t t1 = clock();
  long double duration = (long double) (t1 - t0) / CLOCKS_PER_SEC * 1000;
  printf("Non-simd processing of file %d times took %0.1Lf ms (%0.1Lf per iteration)\n",
	 reps,
	 duration,
	 duration / (long double) reps);

  if (PRINTING) {
    printf("FYI: %s has %zu bytes\n", inputname, len);
    printf("FYI: match count is %d\n", count);
    printf("FYI: a matched byte occured around once per %0.1Lf input bytes\n",
	   ((long double) len) / ((long double) count));
    printf("FYI: time per match is %0.3Lf ms\n",
	   (duration / (long double) reps) * ((long double) count) / ((long double) len));
    fflush(NULL);
  }
  
  uint8_t simd_char1 = '\0';
  int non_simd_count = count;
  Charset simd_cs;
  int simd_size = prep_simd_charset(cs, &simd_cs, &simd_char1);
  if (!pexl_simd_available()) {
    TEST_ASSERT(simd_size == PEXL_ERR_INTERNAL);
    printf("SKIPPING SIMD TESTS because feature not available\n");
    return;
  }
  switch (cs_type) {
  case CharSet1:
    TEST_ASSERT(simd_size == 1); break;
  case CharSet8:
    TEST_ASSERT((simd_size >= 2) && (simd_size <= 8)); break;
  case CharSetEmpty:
    TEST_ASSERT(simd_size == 0); break;
  case CharSetFull:
    TEST_ASSERT(simd_size == 256); break;
  case CharSet:
    TEST_ASSERT((simd_size > 1) && (simd_size < 256)); break;
  default:
    printf("Charset type unknown!\n");
    assert(0);			/* should not happen */
  }
  t0 = clock();
  for (i = 0; i < reps; i++) {
    ptr = input;
    count = 0;
    switch (cs_type) {
    case CharSet1:
      while (1) { 
	ptr = find_simd1(simd_char1, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++; 
      }
      break;
    case CharSet8:
      while (1) {
	ptr = find_simd8(&simd_cs, cs, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++; 
      }
      break;
    case CharSet:
      while (1) {
	ptr = find_simd(&simd_cs, cs, ptr, input + len); 
	if (ptr == input+len) break;
	count++;
	ptr++; 
      }
      break;
    default:
      assert(0);
    } /* switch */
  } /* for reps */
  t1 = clock();
  long double non_simd_duration = duration;
  duration = (long double) (t1 - t0) / CLOCKS_PER_SEC * 1000;
  printf("SIMD processing of file %d times took %0.1Lf ms (%0.1Lf per iteration)\n",
	 reps,
	 duration,
	 duration / (long double) reps);

  if (PRINTING) {
    printf("FYI: %s has %zu bytes\n", inputname, len);
    printf("FYI: match count is %d\n", count);
    printf("FYI: a matched byte occured around once per %0.1Lf input bytes\n",
	   ((long double) len) / ((long double) count));
    printf("FYI: time per match is %0.3Lf ms\n",
	   (duration / (long double) reps) * ((long double) count) / ((long double) len));
    fflush(NULL);
  }
  
  TEST_ASSERT(count == non_simd_count);

#if defined(DEBUG)
  UNUSED(non_simd_duration);
  printf("\nCompiled with DEBUG.  Measured SIMD performance is meaningless.\n");
#else
  printf("\n** SIMD SPEEDUP: %6.2Lfx **\n\n", non_simd_duration / duration);
#endif
  return;
}

int main(int argc, char **argv) {

  int reps = 0;
  Charset cs;
  const char *ptr;
  const char *eps = "";
  const char *A = "A";
  const char *abc = "abc";
  const char *AtoN = "ABCDEFGHIJKLMN"; /* len 15 */
  const char *AtoO = "ABCDEFGHIJKLMNO"; /* len 16 */
  const char *AtoP = "ABCDEFGHIJKLMNOP"; /* len 17 */
  const char *AtoZ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const char *atoz = "abcdefghijklmnopqrstivwxyz";
  const char *Ax15 = "AAAAAAAAAAAAAAA";
  const char *Ax16 = "AAAAAAAAAAAAAAAA";
  const char *Ax32 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  const char *Ax33 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  
  TEST_START(argc, argv);

#if defined(DEBUG)
    printf("\n"
	   "***************************************************************************\n"
	   "*** NOTE: This program was compiled with debugging options enabled.     ***\n"
	   "***       Reported performance for SIMD operations will be much slower  ***\n"
	   "***       than they would be with a 'release mode' compilation.  To     ***\n"
	   "***       test a 'release mode' compilation, compile and run this       ***\n"
	   "***       program in the benchmark directory.                           ***\n"
	   "***************************************************************************\n\n");
    reps = 1;
#else
    reps = 10;
#endif

  size_t wordslen;
  const char *words = readfile("/usr/share/dict/words", &wordslen);
  if (!words) 
    printf("SKIPPING TESTS on /usr/share/dict/words (file not found)\n");
  size_t kjvlen;
  const char *kjv = (argc > 1) ? readfile(argv[1], &kjvlen) : NULL;
  if (!kjv)
    printf("SKIPPING TESTS on %s (CLI-supplied file not found)\n", argv[1]);
    
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Bad arguments");

  fprintf(stderr, "Expect warning here:\n");
  ptr = find_char('A', NULL, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_char('A', A, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_char('A', NULL, A);
  TEST_ASSERT_NULL(ptr);

  charset_fill_zero(cs.cs);	/* make an empty charset */
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_charset(NULL, NULL, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_charset(&cs, NULL, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_charset(&cs, A, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_charset(&cs, NULL, A);
  TEST_ASSERT_NULL(ptr);

  simd_cache_initialize();

  Charset cs_memo;
  uint8_t char1;
  int simd_size = prep_simd_charset(&cs, &cs_memo, &char1);

  if (pexl_simd_available())
    TEST_ASSERT(simd_size == 0);
  else
    TEST_ASSERT(simd_size == PEXL_ERR_INTERNAL);

  charset_fill_one(cs.cs);	/* make a full charset */
  simd_size = prep_simd_charset(&cs, &cs_memo, &char1);

  if (pexl_simd_available())
    TEST_ASSERT(simd_size == 256);
  else
    TEST_ASSERT(simd_size == PEXL_ERR_INTERNAL);

  fprintf(stderr, "Expect warning here:\n");
  ptr = find_simd(&cs_memo, NULL, NULL, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_simd(&cs_memo, NULL, A, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_simd(&cs_memo, NULL, NULL, A);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_simd(&cs_memo, &cs, NULL, A);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_simd(&cs_memo, &cs, A, NULL);
  TEST_ASSERT_NULL(ptr);
  fprintf(stderr, "Expect warning here:\n");
  ptr = find_simd(&cs_memo, &cs, A, A);

  if (pexl_simd_available())
    TEST_ASSERT_NOT_NULL(ptr);
  else
    TEST_ASSERT_NULL(ptr);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Empty and full charsets");

  charset_fill_zero(cs.cs);	/* make an empty charset */
  print_charset(cs.cs);
  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A+strlen(A));
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc+strlen(abc));

  charset_fill_one(cs.cs);	/* make a full charset */
  print_charset(cs.cs);
  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A);
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Single character charsets");

  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'A');		/* add char to set */
  print_charset(cs.cs);
  
  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A);
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ);
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc+strlen(abc));

  /* find last char in 15 char input */
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'N');
  ptr = find_charset(&cs, AtoN, AtoN+strlen(AtoN));
  TEST_ASSERT(ptr == AtoN+strlen(AtoN)-1);
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'O');
  ptr = find_charset(&cs, AtoN, AtoN+strlen(AtoN));
  TEST_ASSERT(ptr == AtoN+strlen(AtoN));

  /* find last char in 16 char input */
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'O');
  ptr = find_charset(&cs, AtoO, AtoO+strlen(AtoO));
  TEST_ASSERT(ptr == AtoO + strlen(AtoO)-1);
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'P');
  ptr = find_charset(&cs, AtoO, AtoO+strlen(AtoO));
  TEST_ASSERT(ptr == AtoO+strlen(AtoO));

  /* find last char in 17 char input */
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'P');
  ptr = find_charset(&cs, AtoP, AtoP+strlen(AtoP));
  TEST_ASSERT(ptr == AtoP + strlen(AtoP)-1);
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'Q');
  print_charset(cs.cs);
  ptr = find_charset(&cs, AtoP, AtoP+strlen(AtoP));
  TEST_ASSERT(ptr == AtoP+strlen(AtoP));
  
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'J');	     /* add char to set */
  print_charset(cs.cs);
  
  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A+strlen(A));
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ + ('J' - 'A'));
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc+strlen(abc));

  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'Z');		/* add char to set */
  print_charset(cs.cs);
  
  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A+strlen(A));
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ + ('Z' - 'A'));
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc+strlen(abc));
  
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 0xff);		/* add char to set */
  print_charset(cs.cs);
  
  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A+strlen(A));
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ+strlen(AtoZ));
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc+strlen(abc));

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Charsets with between 1 and 8 characters");

  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'A');		/* add char to set */
  setchar(cs.cs, 'Z');		/* add char to set */
  print_charset(cs.cs);

  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr);
  /* find the A */
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ);
  /* find the Z */
  ptr = find_charset(&cs, AtoZ+1, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ+strlen(AtoZ)-1);
  ptr = find_charset(&cs, abc, abc+strlen(abc));
  TEST_ASSERT(ptr == abc+strlen(abc));

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'J');	     /* add char to set */
  setchar(cs.cs, 'K');	     /* add char to set */
  setchar(cs.cs, 'L');	     /* add char to set */
  setchar(cs.cs, 'M');	     /* add char to set */
  setchar(cs.cs, 'N');	     /* add char to set */
  setchar(cs.cs, 'O');	     /* add char to set */
  setchar(cs.cs, 'P');	     /* add char to set */
  setchar(cs.cs, 'Q');	     /* add char to set */
  print_charset(cs.cs);

  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A+strlen(A));
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ + ('J' - 'A'));
  ptr = find_charset(&cs, atoz, atoz+strlen(atoz));
  TEST_ASSERT(ptr == atoz+strlen(atoz));

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Charsets with between 8 and 255 characters");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'J');	     /* add char to set */
  setchar(cs.cs, 'K');	     /* add char to set */
  setchar(cs.cs, 'L');	     /* add char to set */
  setchar(cs.cs, 'M');	     /* add char to set */
  setchar(cs.cs, 'N');	     /* add char to set */
  setchar(cs.cs, 'O');	     /* add char to set */
  setchar(cs.cs, 'P');	     /* add char to set */
  setchar(cs.cs, 'Q');	     /* add char to set */
  setchar(cs.cs, 0xff);	     /* add char to set */
  print_charset(cs.cs);

  ptr = find_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = find_charset(&cs, A, A+strlen(A));
  TEST_ASSERT(ptr == A+strlen(A));
  ptr = find_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ + ('J' - 'A'));
  /* find the 'K' */
  ptr = find_charset(&cs, AtoZ+10, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ + 10);
  /* find the 'J' at the end of the input */
  ptr = find_charset(&cs, AtoZ, AtoZ+10);
  TEST_ASSERT(ptr == AtoZ + 9);
  TEST_ASSERT(ptr[0] == 'J');
  ptr = find_charset(&cs, atoz, atoz+strlen(atoz));
  TEST_ASSERT(ptr == atoz+strlen(atoz));

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Finding newlines");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '\n');	     /* add char to set */
  print_charset(cs.cs);

  if (words)
    AB_test_find("words", words, wordslen, &cs, reps);
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv)
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
  else
    printf("Skipping CLI-supplied input file: none given\n");
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Finding @ (a single rarely-occurring char)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '@');	     /* add char to set */
  print_charset(cs.cs);

  if (words)
    AB_test_find("words", words, wordslen, &cs, reps);
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv)
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
  else
    printf("Skipping CLI-supplied input file: none given\n");
  
  /* ----------------------------------------------------------------------------- */
  /* etaoinshrdlcumwfgypbvkjxqz */
  TEST_SECTION("Finding etaionsh (8 most freq chars in English)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'e');	     /* add char to set */
  setchar(cs.cs, 't');	     /* add char to set */
  setchar(cs.cs, 'a');	     /* add char to set */
  setchar(cs.cs, 'i');	     /* add char to set */
  setchar(cs.cs, 'o');	     /* add char to set */
  setchar(cs.cs, 'n');	     /* add char to set */
  setchar(cs.cs, 's');	     /* add char to set */
  setchar(cs.cs, 'h');	     /* add char to set */
  print_charset(cs.cs);

  if (words)
    AB_test_find("words", words, wordslen, &cs, reps);
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv)
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
  else
    printf("Skipping CLI-supplied input file: none given\n");
      

  TEST_SECTION("Finding pbvkjxqz (8 least freq chars in English)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'p');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */
  print_charset(cs.cs);

  if (words)
    AB_test_find("words", words, wordslen, &cs, reps);
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv)
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
  else
    printf("Skipping CLI-supplied input file: none given\n");


  TEST_SECTION("Finding (+[^|{}] (8 chars likely to not appear at all)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, '+');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, '^');	     /* add char to set */
  setchar(cs.cs, '|');	     /* add char to set */
  setchar(cs.cs, '{');	     /* add char to set */
  setchar(cs.cs, '}');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  print_charset(cs.cs);

  if (words)
    AB_test_find("words", words, wordslen, &cs, reps);
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv)
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
  else
    printf("Skipping CLI-supplied input file: none given\n");

  /* ----------------------------------------------------------------------------- */
  /* etaoinshrdlcumwfgypbvkjxqz */
  TEST_SECTION("Finding etaoinshrdl (12 most freq chars in English)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'e');	     /* add char to set */
  setchar(cs.cs, 't');	     /* add char to set */
  setchar(cs.cs, 'a');	     /* add char to set */
  setchar(cs.cs, 'o');	     /* add char to set */
  setchar(cs.cs, 'i');	     /* add char to set */
  setchar(cs.cs, 'n');	     /* add char to set */
  setchar(cs.cs, 's');	     /* add char to set */
  setchar(cs.cs, 'h');	     /* add char to set */
  setchar(cs.cs, 'r');	     /* add char to set */
  setchar(cs.cs, 'd');	     /* add char to set */
  setchar(cs.cs, 'l');	     /* add char to set */
  print_charset(cs.cs);

  const char *alphabet = "abcdefghijklmnopqrstuvwxyz";
  size_t alphabet_len = strlen(alphabet);
  simd_cache_initialize();
  AB_test_find("alphabet", alphabet, alphabet_len, &cs, 1);
  simd_cache_print_stats();

  if (words) {
    simd_cache_initialize();
    AB_test_find("words", words, wordslen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv) {
    simd_cache_initialize();
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping CLI-supplied input file: none given\n");


  TEST_SECTION("Finding wfgypbvkjxqz (12 least freq chars in English)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'w');	     /* add char to set */
  setchar(cs.cs, 'f');	     /* add char to set */
  setchar(cs.cs, 'g');	     /* add char to set */
  setchar(cs.cs, 'y');	     /* add char to set */
  setchar(cs.cs, 'p');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */
  print_charset(cs.cs);

  if (words) {
    simd_cache_initialize();
    AB_test_find("words", words, wordslen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv) {
    simd_cache_initialize();
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping CLI-supplied input file: none given\n");

  
  TEST_SECTION("Finding ()*+[\\]^_`{|}~ (15 not likely appear at all)");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '*');	     /* add char to set */
  setchar(cs.cs, '+');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, '\\');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '^');	     /* add char to set */
  setchar(cs.cs, '_');	     /* add char to set */
  setchar(cs.cs, '`');	     /* add char to set */
  setchar(cs.cs, '{');	     /* add char to set */
  setchar(cs.cs, '|');	     /* add char to set */
  setchar(cs.cs, '}');	     /* add char to set */
  setchar(cs.cs, '~');	     /* add char to set */
  print_charset(cs.cs);

  if (words) {
    simd_cache_initialize();
    AB_test_find("words", words, wordslen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv) {
    simd_cache_initialize();
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping CLI-supplied input file: none given\n");

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Finding a-zA-Z");

  charset_fill_zero(cs.cs);  /* erase all chars */
  int j;
  for (j = 65; j < 65 + 26; j++) setchar(cs.cs, j);
  for (j = 65+32; j < 65 + 32 + 26; j++) setchar(cs.cs, j);
  print_charset(cs.cs);

  if (words) {
    simd_cache_initialize();
    AB_test_find("words", words, wordslen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping /usr/share/dict/words: file not found\n");

  if (kjv) {
    simd_cache_initialize();
    AB_test_find(argv[1], kjv, kjvlen, &cs, reps);
    simd_cache_print_stats();
  }
  else
    printf("Skipping CLI-supplied input file: none given\n");


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing span_char");

  ptr = span_char('A', eps, eps);
  TEST_ASSERT(ptr == eps);

#define ABtest_span1(c, input) do {				\
    ptr = span_char(c, input, input+strlen(input));		\
    if (c == 'A') TEST_ASSERT(ptr == input+strlen(input));	\
    else TEST_ASSERT(ptr == input);				\
      ptr = span_simd1(c, input, input+strlen(input));		\
      if (!pexl_simd_available()) {					\
	TEST_ASSERT(ptr == NULL);				\
      } else {							\
	if (c == 'A') {						\
	  TEST_ASSERT(ptr == input+strlen(input));		\
	} else {						\
	  TEST_ASSERT(ptr == input);				\
	}							\
      }								\
  } while (0);
  
  for (uint8_t c = 'A'; c < 'C'; c++) {
    ABtest_span1(c, A);
    ABtest_span1(c, Ax15);
    ABtest_span1(c, Ax16);
    ABtest_span1(c, Ax32);
    ABtest_span1(c, Ax33);
  } /* for chars 'A' and 'B' */
  
  ptr = span_char('A', AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ+1);
  ptr = span_simd1('A', AtoZ, AtoZ+strlen(AtoZ));
  if (pexl_simd_available()) 
    TEST_ASSERT(ptr == AtoZ+1);
  else
    TEST_ASSERT(ptr == NULL);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing span_charset with a single character set");

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'A');	     /* add char to set */
  ptr = span_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);

  uint8_t c1;
  Charset simd_cs;
  simd_size = prep_simd_charset(&cs, &simd_cs, &c1);
  if (pexl_simd_available())
    TEST_ASSERT(simd_size == 1);
  else
    TEST_ASSERT(simd_size < 0);
  ptr = span_simd(&simd_cs, &cs, eps, eps);
  if (pexl_simd_available())
    TEST_ASSERT(ptr == eps);
  else
    TEST_ASSERT(ptr == NULL);

  ptr = span_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ+1);
  /*
    NOTE: We cannot do this test using span_simd or span_simd8,
    because we do not make simd charsets for those functions when the
    set contains only 1 char (or no chars, or all chars).
  */
  ptr = span_simd1(c1, AtoZ, AtoZ+strlen(AtoZ));
  if (pexl_simd_available()) 
    TEST_ASSERT(ptr == AtoZ+1);
  else
    TEST_ASSERT(ptr == NULL);

#define ABtest_charset1(c, input) do {				\
    charset_fill_zero(cs.cs);  /* erase all chars */		\
    setchar(cs.cs, c);	       /* add char to set */		\
    ptr = span_charset(&cs, input, input+strlen(input));	\
    if (c == 'A') TEST_ASSERT(ptr == input+strlen(input));	\
    else TEST_ASSERT(ptr == input);				\
    simd_size = prep_simd_charset(&cs, &simd_cs, &c1);		\
    if (!pexl_simd_available()) {					\
      TEST_ASSERT(simd_size < 0);				\
    } else {							\
      TEST_ASSERT(simd_size == 1);				\
    }								\
    ptr = span_simd1(c, input, input+strlen(input));		\
    if (!pexl_simd_available()) {					\
      TEST_ASSERT(ptr == NULL);					\
    } else {							\
      if (c == 'A') TEST_ASSERT(ptr == input+strlen(input));	\
      else TEST_ASSERT(ptr == input);				\
    }								\
  } while (0);
  
  for (uint8_t c = 'A'; c < 'C'; c++) {
    ABtest_charset1(c, A);
    ABtest_charset1(c, Ax15);
    ABtest_charset1(c, Ax16);
    ABtest_charset1(c, Ax32);
    ABtest_charset1(c, Ax33);
  } /* for chars 'A' and 'B' */
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing span_charset with a small character set");

  charset_fill_zero(cs.cs);	/* erase all chars */ 
  setchar(cs.cs, 'A');
  setchar(cs.cs, 'B');
  ptr = span_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = span_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  printf("ptr index is %ld\n", ptr - AtoZ);
  TEST_ASSERT(ptr == AtoZ+2);

#define ABtest_charset2(c, input) do {				\
    charset_fill_zero(cs.cs);  /* erase all chars */		\
    setchar(cs.cs, c);						\
    setchar(cs.cs, c+1);					\
    ptr = span_charset(&cs, input, input+strlen(input));	\
    if (c == 'A')						\
      TEST_ASSERT(ptr == input+strlen(input));			\
    else								\
      TEST_ASSERT(ptr == input);					\
    simd_size = prep_simd_charset(&cs, &simd_cs, &c1);			\
    if (pexl_simd_available()) {						\
      TEST_ASSERT(simd_size == 8);					\
    } else {								\
      TEST_ASSERT(simd_size < 0);					\
    }									\
    ptr = span_simd8(&simd_cs, &cs, input, input+strlen(input));	\
    if (pexl_simd_available()) {						\
      if (c == 'A')							\
	TEST_ASSERT(ptr == input+strlen(input));			\
      else								\
	TEST_ASSERT(ptr == input);					\
    } else {								\
      TEST_ASSERT(ptr == NULL);						\
    }									\
 } while (0);
  
  for (uint8_t c = 'A'; c < 'C'; c++) {
    ABtest_charset2(c, A);
    ABtest_charset2(c, Ax15);
    ABtest_charset2(c, Ax16);
    ABtest_charset2(c, Ax32);
    ABtest_charset2(c, Ax33);
  } /* for chars 'A' and 'B' */
  
  charset_fill_zero(cs.cs);	/* erase all chars */
  setchar(cs.cs, 'A');
  setchar(cs.cs, 'B');
  ptr = span_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ+2);

  setchar(cs.cs, 'C');
  ptr = span_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  TEST_ASSERT(ptr == AtoZ+3);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing span_charset with a larger character sets");

  charset_fill_zero(cs.cs);	/* erase all chars */
  for (uint8_t c = 'A'; c < 'J'; c++) setchar(cs.cs, c);
  ptr = span_charset(&cs, eps, eps);
  TEST_ASSERT(ptr == eps);
  ptr = span_charset(&cs, AtoZ, AtoZ+strlen(AtoZ));
  printf("ptr index is %ld\n", ptr - AtoZ);
  TEST_ASSERT(ptr == AtoZ + ('J' - 'A'));

#define ABtest_charset14(c, input) do {				\
    charset_fill_zero(cs.cs);  /* erase all chars */		\
    for (uint8_t i = c; i < c + 14; i++) setchar(cs.cs, i);	\
    ptr = span_charset(&cs, input, input+strlen(input));	\
    if (c == 'A') TEST_ASSERT(ptr == input+strlen(input));	\
    else TEST_ASSERT(ptr == input);				\
    simd_size = prep_simd_charset(&cs, &simd_cs, &c1);		\
    if (pexl_simd_available()) {					\
      TEST_ASSERT(simd_size == 255);					\
    } else {								\
      TEST_ASSERT(simd_size < 0);					\
    }									\
    ptr = span_simd(&simd_cs, &cs, input, input+strlen(input));		\
    if (pexl_simd_available()) {					\
      if (c == 'A')							\
	TEST_ASSERT(ptr == input+strlen(input));			\
      else								\
	TEST_ASSERT(ptr == input);					\
    } else {								\
      TEST_ASSERT(ptr == NULL);						\
    }									\
  } while (0);
  
  for (uint8_t c = 'A'; c < 'C'; c++) {
    ABtest_charset14(c, A);
    ABtest_charset14(c, Ax15);
    ABtest_charset14(c, Ax16);
    ABtest_charset14(c, Ax32);
    ABtest_charset14(c, Ax33);
  } /* for chars 'A' and 'B' */
  

  /* ----------------------------------------------------------------------------- */
  TEST_END();

  return 0;
}

