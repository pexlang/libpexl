/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  lookaroundtest.c  Testing the new lookahead/lookbehind capabilities      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>    /* exit() */
#include "print.h"
#include "compile.h"
#include "old-analyze.h"

#include "../test.h"

#define YES 1
#define NO 0

#define PRINTING NO


static int check_match (pexl_Match *m,
			int expectation, int leftover, int64_t exp_match_len,
			const char *input) {

  ssize_t expected_end = strlen(input) - leftover;
  
  if ((m->end == -1) != (expectation == 0)) {
    printf("pexl_Match error on input '%s': expected %s\n",
     input,
     expectation ? "a match" : "no match");
    return 1;
  }
  if (m->end != -1) {
    if (m->end != expected_end) {
      printf("pexl_Match error on input \"%s\": expected %lu end, found %lu\n",
       input, expected_end, m->end);
      return 2;
    }
    switch (m->encoder_id) {
    case PEXL_TREE_ENCODER:
      if (exp_match_len != -1) {
  if (!m->data) {
    if (exp_match_len != 0) {
      confess("test", "expected non-empty tree for input %s", input);
      return 5;
    }
  } else {
    if (((pexl_MatchTree *)m->data)->size != (size_t) exp_match_len) {
      printf("pexl_Error: expected %" PRId64 " nodes,"
       " got tree with %zu nodes, for input: %s\n",
       exp_match_len, ((pexl_MatchTree *)m->data)->size, input);
      return 5;  
    }
  }
      }
      break;
    default:
      break;
    } /* switch on encoder type */
  } /* end of tests done only if there was a match */
  return 0;
}

static
pexl_Binary *compile_expression (pexl_Context *C,
				 pexl_Optims *optims,
				 pexl_Expr *exp,
				 pexl_Env env,
				 pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);
  TEST_ASSERT(pkg && (pexl_Error_value(*err) == PEXL_OK));

  return pkg;
}

static int test_expression (pexl_Context *C, const char *desc, pexl_Expr *e,
			    const char *inputs[],
			    int matches[], int leftovers[],
			    int64_t exp_match_len) {
  pexl_Binary *pkg = NULL;
  pexl_Match *match = NULL;
  const char *input = NULL;
  pexl_Error err;
  int i, stat;
  size_t inputlen;
  pexl_Optims *optims;
  pexl_Env toplevel = 0;	// root env has id 0
  
  printf("pexl_Expr *: %s\n", desc);
  
  optims = pexl_addopt_simd(NULL, 1);
  TEST_ASSERT(optims);
  optims = pexl_addopt_tro(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_inline(optims);
  TEST_ASSERT(optims);
  optims = pexl_addopt_unroll(optims, 2);
  TEST_ASSERT(optims);
  optims = pexl_addopt_peephole(optims);
  TEST_ASSERT(optims);

  pkg = compile_expression(C, optims, e, toplevel, &err);
  TEST_ASSERT(pkg && pexl_Error_value(err) == OK);
  if (PRINTING) pexl_print_Binary(pkg); 

  pexl_free_Expr(e);

  i = 0;
  while ((input = inputs[i])) {
    if (PRINTING) printf("\t input is \"%s\"\n", input);
    inputlen = strlen(input);
    match = pexl_new_Match(PEXL_TREE_ENCODER);
    TEST_ASSERT(match);
    stat = vm_start(pkg, NULL, main_entrypoint(pkg),
		    input, inputlen,
		    0, inputlen, 0,
		    NULL, match);
    if (stat) {
      printf("vm error: %d\n", stat);
      goto done;
    }
    if (check_match(match, matches[i], leftovers[i], exp_match_len, input)) {
      stat = 3;
      goto done;
    }
    i++;
    if (PRINTING) printf("PASSED.\n");
    pexl_free_Match(match);
    match = NULL;
  } /* while */
  stat = 0;
  
 done:
  binary_free(pkg);
  pexl_free_Optims(optims);
  if (match) pexl_free_Match(match);
  return stat;
}

#define IntList(...) (int[]){__VA_ARGS__}
#define StrList(...) (const char *[]){__VA_ARGS__, NULL}

#define DOTEST(expname, exp, inputs, exp_matches, exp_ends, exp_match_len) do { \
    TEST_ASSERT(!test_expression(C, (expname), (exp),			\
				 (inputs), (exp_matches),		\
				 (exp_ends), (exp_match_len)));		\
  } while (0)

int main(int argc, char **argv) {

  pexl_Context *C;
  const char **inputs;  

  TEST_START(argc, argv);

  /* Check the macros that help with signed aux fields */
  { int64_t i;
    for (i = - (1<<23); i < (1<<23); i++)
      if (!checksignedaux(i)) {
  TEST_FAIL("checksignedaux(%" PRId64 ") failed", i);
      }
    i = - ((1<<23) + 1);
    if (checksignedaux(i)) {
      TEST_FAIL("checksignedaux(%" PRId64 ") passed but should have failed"
    " on negative end of range",
    i);
    }
    i = (1<<23);
    if (checksignedaux(i)) {
      TEST_FAIL("checksignedaux(%" PRId64 ") passsed but should have failed"
    " on positive end of range",
    i); 
    }
  }

  C = context_new();
  TEST_ASSERT(C);

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Testing lookahead with no captures in target pattern");

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  DOTEST(">epsilon", pexl_lookahead_f(C, pexl_match_epsilon(C)),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  /* TEMP: This test result must be examined visually (for now) */
  DOTEST("> >epsilon", pexl_lookahead_f(C, pexl_lookahead_f(C, pexl_match_epsilon(C))),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST(">a", pexl_lookahead_f(C, pexl_match_string(C, "a")),
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST(">(a*)", pexl_lookahead_f(C, pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST(">(a+)", pexl_lookahead_f(C, pexl_repeat_f(C, pexl_match_string(C, "a"), 1, 0)),
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("a*>b", pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_lookahead_f(C, pexl_match_string(C, "b"))),
       inputs,
       IntList( false, false, false, true, true, true, true ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       0);

  TEST_SECTION("Testing lookahead WITH captures in target pattern");
  /* 
     Note: lookarounds are performed with captures suspended, so in
     each test in this section, there should be NO captures returned.
  */
  DOTEST("> capture('foo', epsilon)",
       pexl_lookahead_f(C, pexl_capture_f(C, "foo", pexl_match_epsilon(C))),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("> capture('astar', a*)",
       pexl_lookahead_f(C, 
	  pexl_capture_f(C, "astar", 
             pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0))), 
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("capture('astar', a*) >b",
       pexl_seq_f(C,
	  pexl_capture_f(C, "astar", pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
          pexl_lookahead_f(C, pexl_match_string(C, "b"))), 
       inputs,
       IntList( false, false, false, true, true, true, true ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       2);

  DOTEST("> (capture('astar', a*) >b)",
       pexl_lookahead_f(C, 
           pexl_seq_f(C,
         pexl_capture_f(C, "astar", pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
         pexl_lookahead_f(C, pexl_match_string(C, "b")))), 
       inputs,
       IntList( false, false, false, true, true, true, true ),
       IntList( 0, 0, 0, 1, 2, 7, 6 ),
       /*
	 Not checking number of nodes, which is 2 for all successful
	 matches except for input "b", where there are 0 captures.
       */
       -1);
  
  DOTEST("> capture('WILL NOT CAPTURE', a*) capture('astar', a+)",
       pexl_seq_f(C,
          pexl_lookahead_f(C, 
             pexl_capture_f(C, "WILL NOT CAPTURE", 
                pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0))),
          pexl_capture_f(C, "a_star", 
	     pexl_repeat_f(C, pexl_match_string(C, "a"), 1, 0))),
       
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       2);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing negative lookahead (NOT)");

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  DOTEST("!epsilon", pexl_neg_lookahead_f(C, pexl_match_epsilon(C)),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("!false", pexl_neg_lookahead_f(C, pexl_fail(C)),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("> !epsilon", pexl_lookahead_f(C, pexl_neg_lookahead_f(C, pexl_match_epsilon(C))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("> !false", pexl_lookahead_f(C, pexl_neg_lookahead_f(C, pexl_fail(C))),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("!a", pexl_neg_lookahead_f(C, pexl_match_string(C, "a")),
       inputs,
       IntList( true, false, false, true, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("!(a*)",
       pexl_neg_lookahead_f(C, pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("!(a+)", 
       pexl_neg_lookahead_f(C, pexl_repeat_f(C, pexl_match_string(C, "a"), 1, 0)),
       inputs,
       IntList( true, false, false, true, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("a*!b", pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_neg_lookahead_f(C, pexl_match_string(C, "b"))),
       inputs,
       IntList( true, true, true, false, false, false, false ),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       0);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing negative lookahead WITH captures in target pattern");

  DOTEST("! capture('foo', epsilon)",
       pexl_neg_lookahead_f(C, pexl_capture_f(C, "foo", pexl_match_epsilon(C))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("! capture('foo', false)",
       pexl_neg_lookahead_f(C, pexl_capture_f(C, "foo", pexl_fail(C))),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("! capture('bplus', b+)",
       pexl_neg_lookahead_f(C, 
          pexl_capture_f(C, "bplus", 
	     pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0))),
       inputs,
       IntList( true, true, true, false, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("! capture('astar', a*)",
       pexl_neg_lookahead_f(C, 
          pexl_capture_f(C, "astar", 
             pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0))), 
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("capture('astar', a*) !b",
     pexl_seq_f(C,
        pexl_capture_f(C, "astar", 
	   pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
        pexl_neg_lookahead_f(C, pexl_match_string(C, "b"))), 
    inputs,
    IntList( true, true, true, false, false, false, false ),
    IntList( 0, 0, 0, 1, 1, 1, 3 ),
    2);

  DOTEST("! (capture('astar', a*) >b)",
     pexl_neg_lookahead_f(C, 
        pexl_seq_f(C,
           pexl_capture_f(C, "astar", 
              pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
           pexl_lookahead_f(C, pexl_match_string(C, "b")))), 
       inputs,
       IntList( true, true, true, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);
  

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing lookbehind");

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  DOTEST("'a' < 'a'", pexl_seq_f(C,
            pexl_match_string(C, "a"),
            pexl_lookbehind_f(C, pexl_match_string(C, "a"))),
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       0);

  DOTEST("'a' < 'a'", pexl_seq_f(C,
            pexl_match_string(C, "a"),
            pexl_lookbehind_f(C, pexl_match_string(C, "a"))),
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       0);

  DOTEST("'a' < capture('foo', 'a')",
       pexl_seq_f(C,
     pexl_match_string(C, "a"),
     pexl_lookbehind_f(C, pexl_capture_f(C, "foo", pexl_match_string(C, "a")))),
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       0);

  DOTEST("'a' capture('foo', <'a')",
       pexl_seq_f(C,
     pexl_match_string(C, "a"),
     pexl_capture_f(C, "foo", pexl_lookbehind_f(C, pexl_match_string(C, "a")))),
       inputs,
       IntList( false, true, true, false, true, true, true ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       2);

  DOTEST("'a' capture('foo', <'a' 'a')",
       pexl_seq_f(C,
     pexl_match_string(C, "a"),
     pexl_capture_f(C, "foo",
             pexl_seq_f(C, 
           pexl_lookbehind_f(C, pexl_match_string(C, "a")),
           pexl_match_string(C, "a")))),
       inputs,
       IntList( false, false, true, false, false, true, true ),
       IntList( 0, 0, 4, 1, 1, 5, 4 ),
       2);

  DOTEST("'a' < 'b'", pexl_seq_f(C,
            pexl_match_string(C, "a"),
            pexl_lookbehind_f(C, pexl_match_string(C, "b"))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       0);

  DOTEST("'a' < 'ab'", pexl_seq_f(C,
             pexl_match_string(C, "a"),
             pexl_lookbehind_f(C, pexl_match_string(C, "ab"))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 0, 5, 1, 1, 6, 5 ),
       0);

  DOTEST("'ab' < 'ab'", pexl_seq_f(C,
        pexl_match_string(C, "ab"),
        pexl_lookbehind_f(C, pexl_match_string(C, "ab"))),
       inputs,
       IntList( false, false, false, false, true, false, false ),
       IntList( 0, 0, 5, 1, 0, 6, 5 ),
       0);


  DOTEST("'a*b' (< 'ab') capture('cap_B', 'b')",
       pexl_seq_f(C,
     pexl_seq_f(C,
         pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
         pexl_match_string(C, "b")),
     pexl_capture_f(C, "cap_B", pexl_match_string(C, "b"))),
       inputs,
       IntList( false, false, false, false, false, false, true ),
       IntList( 0, 0, 5, 1, 0, 6, 1 ),
       2);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing variable length lookbehind targets");

  /* Min length of target succeeds, i.e. we match 'b' */
  DOTEST("'a*b' < ('ab'/'b')", 
       pexl_seq_f(C,
         pexl_seq_f(C,
             pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
             pexl_match_string(C, "b")),
         pexl_lookbehind_f(C,
              pexl_choice_f(C,
                 pexl_match_string(C, "ab"),
                 pexl_match_string(C, "b")))),
       inputs,
       //        ("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, true, true, true, true ),
       IntList( 0,     0,     5,     0,    0,    0,    2 ),
       0);

  DOTEST("'a*b' < ('aX'/'b')", 
       pexl_seq_f(C,
         pexl_seq_f(C,
           pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
           pexl_match_string(C, "b")),
         pexl_lookbehind_f(C,
            pexl_choice_f(C,
               pexl_match_string(C, "aX"),
               pexl_match_string(C, "b")))),
       inputs,
       //       ("",    "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, true, true, true, true ),
       IntList( 0,     0,     5,     0,    0,    0,    2 ),
       0);


  DOTEST("'a*b' < ('aab'/'b')",
    pexl_seq_f(C,
      pexl_seq_f(C,
         pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
         pexl_match_string(C, "b")),
      pexl_lookbehind_f(C,
         pexl_choice_f(C,
           pexl_match_string(C, "aab"),
           pexl_match_string(C, "b")))),
       inputs,
       //       ("",  "a",   "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false,   true, true, true, true ),
       IntList( 0,     0,     5,       0,    0,    0,    2 ),
       0);

  /* Min is 2, max is 5 */
  DOTEST("'a*b' < ('12345'/'aX'/'aaX'/'aaaX'/'aaab')",
    pexl_seq_f(C,
        pexl_seq_f(C,
           pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
           pexl_match_string(C, "b")),
        pexl_lookbehind_f(C,
           pexl_choice_f(C,
              pexl_match_string(C, "12345"),
              pexl_choice_f(C, 
                 pexl_match_string(C, "aX"),
                 pexl_choice_f(C, 
                    pexl_match_string(C, "aaX"),
                    pexl_choice_f(C, 
                             pexl_match_string(C, "aaaX"),
                             pexl_match_string(C, "aaab"))))))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, false, false, true, true ),
       IntList( 0,     0,     5,     1,     2,     0,    2 ),
       0);

  /* Lookbehind that tries to match beyond the position where the lookbehind was invoked */
  /* Min is 3, max is 5 */
  DOTEST("'a*b' < ('aaX'/'aaabb'$/'aaabb')",
    pexl_seq_f(C,
    pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
    pexl_lookbehind_f(C,
           pexl_choice_f(C,
              pexl_match_string(C, "aaX"),
              pexl_choice_f(C,
                 pexl_seq_f(C,
                       pexl_match_string(C, "aaabb"),
                       pexl_neg_lookahead_f(C, pexl_match_any(C, 1))),
                 pexl_match_string(C, "aaabb"))))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, false, false, false, false ),
       IntList( 0,     0,     5,     1,     2,     7,     3 ),
       0);

  /* Lookbehind with no maximum length */
  /* Min is 1, max is unbounded */
  DOTEST("'a*b' < ('a' 'b'*)",
    pexl_seq_f(C,
    pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
    pexl_lookbehind_f(C,
           pexl_seq_f(C,
                 pexl_match_string(C, "a"),
                 pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0)))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, false, true, true, true ),
       IntList( 0,     0,     5,     1,     0,    0,    2 ),
       0);

  /* Min is 2, max is unbounded */
  DOTEST("'a'* 'b' < ('a' 'b'+) 'bbc'",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_match_string(C, "b"),
             pexl_seq_f(C, 
             pexl_lookbehind_f(C,
                    pexl_seq_f(C,
                    pexl_match_string(C, "a"),
                    pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0))),
             pexl_match_string(C, "bbc")))),
       StrList("",   "aaaaaa", "b",   "ab", "abbbc", "aaabbbcABC"),
       IntList(false, false,   false, false, true,   true ),
       IntList(0,     5,       1,     2,     0,      3 ),
       0);

  /* Target pattern is nullable and nofail ==> compiles to a no-op */
  DOTEST("'a'* 'b' < ('a'*)",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_match_string(C, "b"),
             pexl_lookbehind_f(C, pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)))),

       StrList("",   "aaaaaa", "b",  "ab", "abbb", "aaab12345678"),
       IntList(false, false,   true, true, true,   true ),
       IntList(0,     5,       0,    0,    2,      8 ),
       0);

  confess("lookaroundtest",
    "Not testing that lookbehind of nullable/nofail compiles to a no-op");

  /* Target pattern is nullable but NOT nofail: See below, where
     target pattern is another predicate. 
  */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing lookbehind targets that have other predicates in them");

  /* Lookbehind where the target expression has another lookbehind in it */
  DOTEST("'a'* 'b' < (< 'b')",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_match_string(C, "b"),
             pexl_lookbehind_f(C,
              pexl_lookbehind_f(C, pexl_match_string(C, "b"))))),
       StrList("",   "aaaaaa", "b",  "ab", "abbb", "aaab12345678"),
       IntList(false, false,   true, true, true,   true ),
       IntList(0,     5,       0,    0,    2,      8 ),
       0);

  /* Lookbehind where the target expression has another lookbehind in it */
  DOTEST("'a'* 'b' < ('a' 'b' < 'b')",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_match_string(C, "b"),
             pexl_lookbehind_f(C,
              pexl_seq_f(C,
                    pexl_match_string(C, "ab"),
                    pexl_lookbehind_f(C, pexl_match_string(C, "b")))))),
       StrList("",   "aaaaaa", "b",  "ab", "abbb", "aaab12345678"),
       IntList(false, false,   false, true, true,   true ),
       IntList(0,     5,       1,     0,    2,      8 ),
       0);

  /* Lookbehind where the target expression has another lookbehind in it */
  DOTEST("'a'* 'b'* < ('a' 'b'* < 'b')",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0),
             pexl_lookbehind_f(C,
              pexl_seq_f(C,
                    pexl_match_string(C, "a"),
                    pexl_seq_f(C,
                    pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0),
                    pexl_lookbehind_f(C, pexl_match_string(C, "b"))))))),
       StrList("",   "aaaaaa", "b",  "ab", "abbb", "aaab12345678"),
       IntList(false, false,   false, true, true,   true ),
       IntList(0,     5,       1,     0,    0,      8 ),
       0);

  /* Lookbehind where the target expression has another lookbehind in it */
  DOTEST("'a'* 'b'* < ('a' 'b'* < 'a')",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0),
             pexl_lookbehind_f(C,
              pexl_seq_f(C,
                    pexl_match_string(C, "a"),
                    pexl_seq_f(C,
                    pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0),
                    pexl_lookbehind_f(C, pexl_match_string(C, "a"))))))),
       StrList("",   "aaaaaa", "b",   "ab",  "a", "aaab12345678"),
       IntList(false, true,    false, false, true, false ),
       IntList(0,     0,       1,     0,     0,    8 ),
       0);

  /* Lookbehind where the target expression has a lookahead in it */
  DOTEST("'a'* < ('a' >'b')",
       pexl_seq_f(C,
                   pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
                   pexl_lookbehind_f(C,
                                      pexl_seq_f(C,
                                                  pexl_match_string(C, "a"),
                                                  pexl_lookahead_f(C, pexl_match_string(C, "b"))))),
       StrList("",    "a",   "aa",  "b",   "ab",  "aab", "c",   "ac",  "aac"),
       IntList(false, false, false, false, true,  true,  false, false, false),
       IntList(0,     0,     0,     1,     1,     1,     0,     1,     1),
       0);

  /* Lookbehind where the target expression has a negative lookahead in it */
  DOTEST("'a'* < ('a' !'b')",
       pexl_seq_f(C,
                   pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
                   pexl_lookbehind_f(C,
                                      pexl_seq_f(C,
                                                  pexl_match_string(C, "a"),
                                                  pexl_neg_lookahead_f(C, pexl_match_string(C, "b"))))),
       StrList("",    "a",   "aa",  "b",   "ab",  "aab", "c",   "ac",  "aac"),
       IntList(false, true,  true,  false, false, false, false, true,  true),
       IntList(0,     0,     0,     1,     1,     1,     1,     1,     1),
       0);

  /* Check that the right fence (EOI) is set, and lookahead past EOI */
  DOTEST("'a' < ('a'* >'a')",
       pexl_seq_f(C,
          pexl_match_string(C, "a"),
          pexl_lookbehind_f(C,
             pexl_seq_f(C,
                pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
                pexl_lookahead_f(C, pexl_match_string(C, "a"))))),
       StrList("a",   "aa",  "ab",  "aab"),
       IntList(false, true,  false, true),
       IntList(0,     1,     1,     2),
       0);

  /* Check that the right fence (EOI) is set, and lookahead past EOI (again) */
  DOTEST("'a' < ('a'* >'b')",
       pexl_seq_f(C,
           pexl_match_string(C, "a"),
           pexl_lookbehind_f(C,
              pexl_seq_f(C,
                 pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
                 pexl_lookahead_f(C, pexl_match_string(C, "b"))))),
       StrList("a",   "aa",  "ab",  "aab"),
       IntList(false, false, true,  false),
       IntList(0,     1,     1,     2),
       0);

  /* Check that the right fence (EOI) is set, and negative lookahead past EOI */
  DOTEST("'a' < ('a'* !'b')",
       pexl_seq_f(C,
          pexl_match_string(C, "a"),
          pexl_lookbehind_f(C,
             pexl_seq_f(C,
                pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
                pexl_neg_lookahead_f(C, pexl_match_string(C, "b"))))),
       StrList("a",   "aa",  "ab",  "aab"),
       IntList(true,  true,  false, true),
       IntList(0,     1,     1,     2),
       0);

  /* Check that the right fence (EOI) is restored */
  DOTEST("'a' < (!'b' 'a'*) 'c'",
       pexl_seq_f(C,
           pexl_seq_f(C,
              pexl_match_string(C, "a"),
              pexl_lookbehind_f(C,
                 pexl_seq_f(C,
                    pexl_neg_lookahead_f(C, pexl_match_string(C, "b")),
                    pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)))),
           pexl_match_string(C, "c")),
       StrList("ac",  "aac"),
       IntList(true,  false),
       IntList(0,     2),
       0);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing specific lookbehind search situations");

  /* 
     The second search position matches "b", but this match does not
     extend all the way to the position where the lookbehind was
     invoked.  

     Pattern '"abc" < ("b"/"12")' on input "abc".
  */
  DOTEST("'abc' < ('b'/'12') on input 'abc'",
       pexl_seq_f(C,
       pexl_match_string(C, "abc"),
       pexl_lookbehind_f(C,
              pexl_choice_f(C,
                 pexl_match_string(C, "b"),
                 pexl_match_string(C, "12")))),
       StrList("abc"),
       IntList(false),
       IntList(-1),
       0);

    DOTEST("'abc' < ('b'/'ab') on input 'abc'",
       pexl_seq_f(C,
       pexl_match_string(C, "abc"),
       pexl_lookbehind_f(C,
              pexl_choice_f(C,
                 pexl_match_string(C, "b"),
                 pexl_match_string(C, "ab")))),
       StrList("abc"),
       IntList(false),
       IntList(-1),
       0);

  /* 
     This pattern, below, SHOULD match.  The match of "b" should fail
     (in the lookbehind) because the match does not extend to the
     start position of the lookbehing.  But the search should
     continue, and the third search position (starting at "a")
     succeeds.
  */
  DOTEST("'abc' < ('b'/'abc') on input 'abc'",
       pexl_seq_f(C,
       pexl_match_string(C, "abc"),
       pexl_lookbehind_f(C,
              pexl_choice_f(C,
                 pexl_match_string(C, "b"),
                 pexl_match_string(C, "abc")))),
       StrList("abc"),
       IntList(true),
       IntList(0),
       0);
     
  /* 
     Lookbehind where the target expression has another lookbehind in
     it, and that lookbehind needs to look beyond the left fence
     established by the first lookbehind.
  */
  DOTEST("'a'* 'b' < ('b' < 'ab')",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_match_string(C, "b"),
             pexl_lookbehind_f(C,
              pexl_seq_f(C,
                    pexl_match_string(C, "b"),
                    pexl_lookbehind_f(C, pexl_match_string(C, "ab")))))),
       StrList("aaaaaa", "b",  "ab", "abbb", "aaab12345678"),
       IntList(false,    false, true, true,   true ),
       IntList(6,        1,     0,    2,      8 ),
       0);

  /* 
     Lookbehind inside a lookahead, where the lookbehind needs to look
     beyond the position where the lookahead started.
  */
  DOTEST("'abc' > ('d' < 'abcd')",
       pexl_seq_f(C,
       pexl_match_string(C, "abc"),
       pexl_lookahead_f(C, 
             pexl_seq_f(C,
             pexl_match_string(C, "d"),
             pexl_lookbehind_f(C,
                    pexl_match_string(C, "abcd"))))),
       StrList("abcd", "abc",  "abcde"),
       IntList(true,   false,  true),
       IntList(1,      -1,     2),
       0);
  /* 
     Negative lookbehind test, where zero or more a's followed by zero or more
     b's are parsed, then lookbehind to ensure the string doesn't end in any 
     b's.
  */
  DOTEST("'a'* 'b'* !< 'b'", 
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
        pexl_seq_f(C, 
          pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0),
            pexl_neg_lookahead_f(C, 
              pexl_lookbehind_f(C,
                  pexl_match_string(C, "b"))))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList(  true, true,  true,  false, false,  false,     false),
       IntList( 0,        0,      0,    0,      0,    0,        0 ),
        0);
     
     
  /* 
     Lookbehind choice test, where a followed by one or more b's then a single
     c is matched. A look behind is used to test if a single b or 5 b's are 
     matched.
  */
  DOTEST("'a' 'b'+ 'c'' < ('ab{5}c'/'abc')",
     pexl_seq_f(C,
        pexl_match_string(C, "a"),
          pexl_seq_f(C,
            pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0),
            pexl_seq_f(C,
              pexl_match_string(C, "c"),
                pexl_lookbehind_f(C,
                  pexl_choice_f(C,
                    pexl_match_string(C, "abbbbbc"),
                    pexl_match_string(C, "abc")))))),
       StrList("abc", "abbc", "abbbbbc"),
       IntList(true,   false,  true),
       IntList(0,       0,      0),
       0);
       
  /* 
    Lookbehind choice test, where a followed by one or more b's then a 
    single c is matched. A look behind is used to test if 15 b's are matched 
    within the expression.
  */
  DOTEST("'a' 'b'+ 'c'' < 'ab{15}c'",
     pexl_seq_f(C,
        pexl_match_string(C, "a"),
          pexl_seq_f(C,
            pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0),
            pexl_seq_f(C,
              pexl_match_string(C, "c"),
                pexl_lookbehind_f(C,
                  pexl_match_string(C, "abbbbbbbbbbbbbbbc"))))),
       StrList("abc", "abbbbbbbbbc", "abbbbbbbbbbbbbbbc"),
       IntList(false,   false,  true),
       IntList(0,       0,      0),
       0);
     
     
  /* 
    Lookbehind choice test, where a followed by one or more b's then a 
    single c is matched. A look behind is used to test if 30 b's are matched 
    within the expression.
  */
  DOTEST("'a' 'b'+ 'c'' < 'ab{30}c'",
     pexl_seq_f(C,
        pexl_match_string(C, "a"),
          pexl_seq_f(C,
            pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0),
            pexl_seq_f(C,
              pexl_match_string(C, "c"),
                pexl_lookbehind_f(C,
                  pexl_match_string(C, "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"))))),
       StrList("abc", "abbc", "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"),
       IntList(false,   false,  true),
       IntList(0,       0,      0),
       0);
       
  /* 
    Lookbehind choice test, where a followed by one or more b's then a 
    single c is matched. A look behind is used to test if 50 b's are matched 
    within the expression.
  */
  DOTEST("'a' 'b'+ 'c'' < 'ab{50}c'",
     pexl_seq_f(C,
        pexl_match_string(C, "a"),
          pexl_seq_f(C,
            pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0),
            pexl_seq_f(C,
              pexl_match_string(C, "c"),
                pexl_lookbehind_f(C,
                  pexl_match_string(C, "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"))))),
       StrList("abc", "abbc", "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"),
       IntList(false,   false,  true),
       IntList(0,       0,      0),
       0);
       
  
  /* 
    Lookbehind choice test, where a followed by one or more b's then a 
    single c is matched. A look behind is used to test if 100 b's are matched 
    within the expression.
  */
  DOTEST("'a' 'b'+ 'c'' < 'ab{100}c'",
     pexl_seq_f(C,
        pexl_match_string(C, "a"),
          pexl_seq_f(C,
            pexl_repeat_f(C, pexl_match_string(C, "b"), 1, 0),
            pexl_seq_f(C,
              pexl_match_string(C, "c"),
                pexl_lookbehind_f(C,
                  pexl_match_string(C, "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"))))),
       StrList("abc", "abbc", "abbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbc"),
       IntList(false,   false,  true),
       IntList(0,       0,      0),
       0);
       
      
       
/* ----------------------------------------------------------------------------- */
  TEST_SECTION("Testing negative lookbehind");

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  DOTEST("a !< epsilon", pexl_seq_f(C,
                          pexl_match_string(C, "a"),
                          pexl_neg_lookbehind_f(C, pexl_match_epsilon(C))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  //  DOTEST("!< epsilon", pexl_lookbehind_f(C, pexl_neg_lookahead_f(C, pexl_match_epsilon(C))),
  DOTEST("!< epsilon", pexl_neg_lookbehind_f(C, pexl_match_epsilon(C)),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  //  DOTEST("!< false", pexl_lookbehind_f(C, pexl_neg_lookahead_f(C, pexl_fail(C))),
  DOTEST("!< false", pexl_neg_lookbehind_f(C, pexl_fail(C)),
       inputs,
       IntList( true, true, true, true, true, true, true ),
       IntList( 0, 1, 6, 1, 2, 7, 6 ),
       0);

  DOTEST("a !< a", pexl_seq_f(C,
			      pexl_match_string(C, "a"),
			      pexl_neg_lookbehind_f(C,
						pexl_match_string(C, "a"))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 0, 0, 0, 0, 0, 0 ),
       0);

  DOTEST("a !< (a*)",
	 pexl_seq_f(C,
	    pexl_match_string(C, "a"),
	    pexl_neg_lookbehind_f(C,
	       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 0, 0, 0, 0, 0, 0 ),
       0);

  //inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");

  DOTEST("a !< (a+)",
	 pexl_seq_f( C,
            pexl_match_string(C, "a"),
            pexl_neg_lookbehind_f(C, 
               pexl_repeat_f(C, pexl_match_string(C, "a"), 1, 0))),
       inputs,
       IntList( false, false, false, false, false, false, false ),
       IntList( 0, 0, 0, 0, 0, 0, 0 ),
       0);

  DOTEST("a* !< b", pexl_seq_f(C,
                    pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
                      pexl_neg_lookbehind_f(C, 
                        pexl_match_string(C, "b"))),
       inputs,
       IntList( true, true, true, true, true, true, true),
       IntList( 0, 0, 0, 1, 1, 1, 3 ),
       0);
  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Testing negative lookbehind (NOT)");

  confess("lookaroundtest", "Need MORE tests of variable length negative lookbehind targets");

  context_free(C);

  TEST_END();
}
