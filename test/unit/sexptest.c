/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexptest.c  Test the S-expression API, the reader, and writer            */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdlib.h>
#include <time.h>
#include "sexp-reader.h"
#include "sexp-writer.h"

#include "../test.h"

// This controls much of the "ordinary" output
#define PRINTING false
// This controls output within loops that iterate many, many times
#define PRINT_ALL false
// This controls whether or not we do a lot of memory- and
// time-consuming tests (we always do a few)
#define FUZZING false

#define newline() do { puts(""); } while(0)

// Input buffer large enough for all of our tests
#define MAX_BUF 5000
#if (MAX_BUF < (4 * MAX_BYTES_LEN))
#error Buffer size in test program must be many times larger than max bytestring length
#endif

static void set(char *dest, const char *src) {
  TEST_ASSERT(strlen(src) <= MAX_BUF);
  strncpy(dest, src, MAX_BUF-1);
  dest[MAX_BUF-1] = '\0';       // strncpy does not always null-terminate dest
}

__attribute__((unused))
static void fill(char *dest, const char c, int n) {
  TEST_ASSERT((n > 0) && (n < MAX_BUF));
  memset(dest, (int) c, n);
  dest[n] = '\0';
}

#define SET(str) do {                           \
    set(in, (str));                             \
    end = in;                                   \
  } while (0);

#define FILL(chr, n) do {                          \
    fill(in, (chr), (n));                          \
    end = in;                                      \
  } while (0);

typedef enum FuzzMode {
  GOOD_BYTES,
  WITH_BAD_CHAR
} FuzzMode;

// In range 0..max-1
static uint32_t random_in(uint32_t max) {
  return random() % max;
}

static bool alpha(char c) {
  return ((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z'));
}

static bool digit(char c) {
  return (c >= '0') && (c <= '9');
}

static bool whitespace(char c) {
  return (c == 9) || (c == 10) || (c == 13) || (c == 32);
}

static unsigned char random_alpha(void) {
  unsigned char c = random_in(52);
  if (c < 26) return c + 'A';
  return c - 26 + 'a';
}

static unsigned char random_digit(void) {
  unsigned char c = random_in(10);
  return c + '0';
}

// See sexp.c
static const char *id_special_start_chars = ".@^_~%&!*/:<=>?~$";
static const char *id_special_more_chars = "+-";

static unsigned char random_id_start(void) {
  unsigned char c = random_in(52 + strlen(id_special_start_chars));
  if (c < 26) return c + 'A';
  if (c < 52) return c - 26 + 'a';
  return id_special_start_chars[c - 52];
}

static unsigned char random_id_char(void) {
  const int n_starts = strlen(id_special_start_chars);
  const int n_more = strlen(id_special_more_chars);
  unsigned char c = random_in(52 + 10 + n_starts + n_more);
  if (c < 26) return c + 'A';
  if (c < 52) return c - 26 + 'a';
  if (c < (52 + n_starts)) return id_special_start_chars[c - 52];
  if (c < (52 + n_starts + n_more)) return id_special_more_chars[c - 52 - n_starts];
  if (c < (52 + n_starts + n_more + 10)) return '0' + (c - 52 - n_starts - n_more);
  printf("Error! random_id_char is returning '%c'\n", c);
  assert(false);
}

static bool printable(char c) {
  return ((c > 32) && (c < 127));
}

// Does not include whitespace
static unsigned char random_printable(void) {
  // c is printable if (c > 32) && (c < 127)
  unsigned char c = random_in(127 - 32 - 1) + 33;
  assert(printable(c));
  return c;
}

static unsigned char random_printable_or_ws(void) {
  // c is printable if (c > 32) && (c < 127)
  // ws is 9 (tab), 10 (newline), 13 (return), and 32 (space)
  const unsigned char ws[4] = "\t\n\r ";
  int n = random_in(127 - 32 - 1) + 33 + 4;
  if (n < 4) return ws[n];
  unsigned char c = n - 4;
  assert(printable(c));
  return c;
}

// Will generate neither whitespace nor NULs
static unsigned char random_unprintable(void) {
  // c is printable iff (c > 32) && (c < 127)
  // ws is 9 (tab), 10 (newline), 13 (return), and 32 (space)
  unsigned char c;
  const int unprintable_range_size = 256 - (127 - 32 + 1) - 4;
  assert(unprintable_range_size == 156);
  int n = random_in(unprintable_range_size) + 1;
  if (n < 9) c = n;             // 1..8 ==> 1..8
  else if (n < 11) c = n + 2;   // 9..10 ==> 11..12
  else if (n < 29) c = n + 3;   // 11..28 ==> 14..31
  else c = n + (255 - unprintable_range_size); // 29..157 ==> 127..255
  assert((c != 0) && !whitespace(c));
  assert(!printable(c));
  return c;
}

static void generate_random_sexp(char *dest) {
  uint32_t len = random_in(MAX_BUF-1);
  if (PRINT_ALL) printf("Generating random sexp with %u bytes\n", len);
  for (uint32_t i = 0; i < len; i++) {
    int roll = random_in(100);
    if (roll < 16) dest[i] = '(';
    //else if (roll < 17) dest[i] = ')';
    else if (roll < 30) dest[i] = " \t\n"[random_in(3)];
    else if (roll < 38) dest[i] = random_digit();
    else if (roll < 96) dest[i] = random_alpha();
    else if (roll < 98) dest[i] = random_printable();
    else {
      if (random_in(50) > 48)
        dest[i] = random_unprintable();
      else
        dest[i] = random_printable();
    }
  }
  dest[len] = '\0';
}

static void generate_bytestring(char *dest, enum FuzzMode mode) {
  assert(MAX_BUF > MAX_BYTES_LEN);
  uint32_t len = random_in(MAX_BYTES_LEN-2) + 1;
  if (PRINT_ALL) printf("Generating random bytestring with %u bytes\n", len);
  *dest = '"';

  for (uint32_t i = 0; i < len; i++) dest[i+1] = random_printable_or_ws();
  dest[len+1] = '"';
  dest[len+2] = '\0';

  for (uint32_t i = 1; i < len+1; i++) {
    if (dest[i] == '\\') dest[i] = '/';
    if (dest[i] == '"') dest[i] = '\'';
  }

  if (mode == GOOD_BYTES) return;

  uint32_t pos = random_in(len) + 1;
  assert((pos > 0) && (pos <= len));
  dest[pos] = random_unprintable();
}

// Determine whether c is in set, provided c is not '\0'
static bool containsp(const char *set, char c) {
  const char *ptr = strchr(set, c);
  return ptr && (*ptr != '\0');
}

static bool id_allowed(char c) {
  return alpha(c) || digit(c) || 
    ((c != '\0') &&
     (containsp(id_special_start_chars, c) ||
      containsp(id_special_more_chars, c)));
}

static unsigned char random_id_verboten(void) {
  // Return a random char that is
  // (1) Printable:
  //     c is printable if (c > 32) && (c < 127), or if ws.
  // (2) But not an allowable id character:
  //     id_special_start_chars, id_special_more_chars
  // (3) EXCEPT that we don't want to generate parens or
  //     semicolons, which are delimiters that, like
  //     whitespace, will stop the parsing of an identifier
  static char id_verboten[256] = {'\0'};
  static int len = 0;
  if (id_verboten[0] == '\0') {
    // Initialize
    for (int i = 0; i < 256; i++)
      if (printable(i) && !id_allowed(i) && !containsp("();", i))
	id_verboten[len++] = i;
    printf("Verboten chars (len = %d): %s\n", len, id_verboten);
  }
  return id_verboten[random_in(len)];
}

static void generate_identifier(char *dest, enum FuzzMode mode) {
  assert(MAX_BUF > MAX_IDLEN);
  uint32_t len = random_in(MAX_IDLEN - 1) + 1;
  if (PRINT_ALL) printf("Generating random idenfitier with %u bytes\n", len);

  dest[0] = random_id_start();
  for (uint32_t i = 1; i < len; i++) dest[i] = random_id_char();
  dest[len] = '\0';

  if (mode == GOOD_BYTES) return;

  // NOTE: No point inserting whitespace or parens into the
  // identifier.  These are delimiters and will stop the id parser.
  //
  // We pick a random position at which to insert a character that is
  // not valid for an identifier.  Don't want to alter the first char,
  // because we want the parser to start parsing an identifier.

  int roll = random_in(100);
  uint32_t pos = random_in(len) + 1;
  assert((pos > 0) && (pos <= len));
  // id cannot contain unprintable chars
  if (roll < 50) {
    dest[pos] = random_unprintable();
  } else {
    // id cannot contain certain printable chars
    dest[pos] = random_id_verboten();
  }
}

int main(int argc, char **argv) {

  char in[MAX_BUF];
  size_t len;
  Sexp *s;
  const char *str, *end;
  char *tmp;
  int64_t n;
  String *tmpString;
  const string *err;
  int strdiff;
  
  TEST_START(argc, argv);
  printf("Size of Token is %zu bytes\n", sizeof(Token));
  printf("Size of Sexp is %zu bytes\n", sizeof(Sexp));
  srandom(time(NULL));

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Error checking");

  s = read_sexp(NULL);
  TEST_ASSERT(s);
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_PANIC);
  tmpString = NULL;
  sexp_error_info(s, tmpString);
  TEST_ASSERT(String_len(tmpString) == 0);
  free_sexp(s);
  String_free(tmpString);

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Basic token recognition");

  SET("");
  s = read_sexp(&end);
  TEST_ASSERT_NULL(s);

#define EXPECT_ID(input, name) do {			\
    SET(input);						\
    s = read_sexp(&end);				\
    TEST_ASSERT(s && sexp_identifierp(s));		\
    TEST_ASSERT(strncmp(String_ptr(s->str),	\
			name,				\
			String_len(s->str))	\
		== 0);					\
    free_sexp(s);					\
  } while (0);  

#define EXPECT_INT(input, number) do {		\
    SET(input);					\
    s = read_sexp(&end);			\
    TEST_ASSERT(s && sexp_intp(s));		\
    TEST_ASSERT(s->n == (number));	\
    free_sexp(s);				\
  } while (0);  

#define EXPECT_ERROR(input) do {		\
    SET(input);					\
    s = read_sexp(&end);			\
    TEST_ASSERT(s && sexp_errorp(s));		\
    free_sexp(s);				\
  } while (0);  

  // Identifiers: can contain many different characters, including +
  // and -, but cannot start with + or -
  EXPECT_ID("a", "a");
  EXPECT_ID("  a  ", "a");
  EXPECT_ID("    \n\n\t\t  \nX ", "X");
  EXPECT_ID("abc", "abc");
  EXPECT_ID("~", "~");
  EXPECT_ID("az!$&%*/:<=>?@^_~AZ", "az!$&%*/:<=>?@^_~AZ");

  // Numbers
  EXPECT_INT("0", 0);
  EXPECT_INT(" 0", 0);
  EXPECT_INT("0\n\n", 0);
  EXPECT_INT(" \n  0 \t ", 0);

  EXPECT_INT("9876543210", 9876543210);
  EXPECT_INT("-1", -1);
  EXPECT_INT(" -1", -1);
  EXPECT_INT(" -1  ", -1);
  EXPECT_INT("+1", 1);
  EXPECT_INT(" +1", 1);
  EXPECT_INT(" +1  ", 1);
  EXPECT_INT("1", 1);
  EXPECT_INT(" 1", 1);
  EXPECT_INT(" 1  ", 1);


  // Number parsing must check for a delimiter
  EXPECT_ERROR("1k");
  EXPECT_ERROR("01.");

  // Check that only the number is read, because a paren delimits the number
  EXPECT_INT("101(abc", 101);
  TEST_ASSERT(end == in + 3);
  EXPECT_INT("202)abc", 202);
  TEST_ASSERT(end == in + 3);

  // Check that only the number is read, because a space delimits the number
  EXPECT_INT("-1 abc", -1);
  TEST_ASSERT(end == in + 2);

  // Codepoints, bytes, and named characters
#define EXPECT_CHAR(test, input, number) do {			\
    SET(input);							\
    s = read_sexp(&end);					\
    TEST_ASSERT(s && test(s));					\
    printf("Value is %d\n", s->codepoint);		\
    print_sexp_repr(s); puts("");				\
    TEST_ASSERT(s->codepoint == (uint32_t) (number));	\
    free_sexp(s);						\
  } while (0)

  EXPECT_CHAR(sexp_charp, "#\\x00", 0);
  EXPECT_CHAR(sexp_charp, "#\\xff", 255);
  EXPECT_CHAR(sexp_charp, "#\\xAA", 0xaa);

  EXPECT_CHAR(sexp_charp, "#\\u", 'u');
  EXPECT_CHAR(sexp_charp, "#\\v", 'v');
  EXPECT_CHAR(sexp_charp, "#\\w", 'w');
  EXPECT_CHAR(sexp_charp, "#\\+", '+');
  
  EXPECT_CHAR(sexp_charp, "#\\space", 0x20);
  EXPECT_CHAR(sexp_charp, "#\\tab", 0x9);
  EXPECT_CHAR(sexp_charp, "#\\newline", 0xA);
  EXPECT_CHAR(sexp_charp, "#\\return", 0xD);
  EXPECT_CHAR(sexp_charp, "#\\null", 0x00);
  EXPECT_CHAR(sexp_charp, "#\\nul", 0x00);

  // Invalid named chars
  EXPECT_ERROR("#\\Null");
  EXPECT_ERROR("#\\TAB");
  EXPECT_ERROR("#\\TAB");
  EXPECT_ERROR("#\\nnnnn");
  EXPECT_ERROR("#\\uvwxy");
  EXPECT_ERROR("#\\u000x");
  EXPECT_ERROR("#\\U1234567x");
  EXPECT_ERROR("#\\U12345678x");

  EXPECT_CHAR(sexp_charp, "#\\u0000", 0);
  EXPECT_CHAR(sexp_charp, "#\\uFffF", 0xffff);
  EXPECT_CHAR(sexp_charp, "#\\U00000000", 0);
  EXPECT_CHAR(sexp_charp, "#\\UffffFFFF", 0xffffffff);

  // There must be a token terminator (paren or whitespace) after the
  // hex digits in a codepoint or byte literal
  EXPECT_CHAR(sexp_charp, "#\\uFffF(", 0xffff);
  EXPECT_CHAR(sexp_charp, "#\\uFffF)", 0xffff);
  EXPECT_CHAR(sexp_charp, "#\\uFffF ", 0xffff);
  EXPECT_CHAR(sexp_charp, "#\\U00000000(", 0);
  EXPECT_CHAR(sexp_charp, "#\\U00000000)", 0);
  EXPECT_CHAR(sexp_charp, "#\\U00000000 ", 0);
  EXPECT_CHAR(sexp_charp, "#\\space ", 0x20);
  EXPECT_CHAR(sexp_charp, "#\\space(", 0x20);
  EXPECT_CHAR(sexp_charp, "#\\space)", 0x20);
  EXPECT_CHAR(sexp_charp, "#\\tab ", 0x9);
  EXPECT_CHAR(sexp_charp, "#\\tab(", 0x9);
  EXPECT_CHAR(sexp_charp, "#\\tab)", 0x9);
  EXPECT_CHAR(sexp_charp, "#\\newline ", 0xA);
  EXPECT_CHAR(sexp_charp, "#\\newline(", 0xA);
  EXPECT_CHAR(sexp_charp, "#\\newline)", 0xA);
  EXPECT_CHAR(sexp_charp, "#\\return ", 0xD);
  EXPECT_CHAR(sexp_charp, "#\\return(", 0xD);
  EXPECT_CHAR(sexp_charp, "#\\return)", 0xD);
  EXPECT_CHAR(sexp_charp, "#\\null ", 0x00);
  EXPECT_CHAR(sexp_charp, "#\\null(", 0x00);
  EXPECT_CHAR(sexp_charp, "#\\null)", 0x00);
  EXPECT_CHAR(sexp_charp, "#\\nul ", 0x00);
  EXPECT_CHAR(sexp_charp, "#\\nul(", 0x00);
  EXPECT_CHAR(sexp_charp, "#\\nul)", 0x00);
  
  // Each codepoint or byte sigil indicates exactly how many hex
  // digits are expected: \x => 2, \u => 4, \U => 8.
  //
  // There must be a delimiter after the codepoint/byte hex digits.

  EXPECT_CHAR(sexp_charp, "#\\xAA bcde", 0xaa);
  TEST_ASSERT(end == in+5);	// sharp, backslash, x, 2 digits
  
  EXPECT_CHAR(sexp_charp, "#\\uFffF)123", 0xffff);
  TEST_ASSERT(end == in+7);	// sharp, backslash, u, 4 digits

  EXPECT_CHAR(sexp_charp, "#\\U0000ffff(Q", 0xffff);
  TEST_ASSERT(end == in+strlen(in)-2); // the '(Q' is not consumed

  // No delimiter after the codepoint/byte syntax:
  EXPECT_ERROR("#\\xAAbcde");
  EXPECT_ERROR("#\\uFffF123");
  EXPECT_ERROR("#\\u123412345");
  EXPECT_ERROR("#\\U0000ffffQ");
  EXPECT_ERROR("#\\U0000ffff2");
  
#define EXPECT_STR(input, expectation) do {		\
    SET(input);						\
    s = read_sexp(&end);				\
    if (s) {						\
      printf("s-expression type: %d\n", s->type);	\
      fflush(NULL);					\
    }							\
    TEST_ASSERT(s && sexp_bytestringp(s));		\
    printf("String read: '%.*s'\n", (int) String_len(s->str), String_ptr(s->str));\
    fflush(NULL);							\
    TEST_ASSERT(strncmp(String_ptr(s->str),	\
			expectation,			\
			String_len(s->str))	\
		== 0);					\
    free_sexp(s);					\
  } while (0);  

  EXPECT_STR("\"\"", "");	// empty string
  printf("end = %zu, in+strlen = %zu\n", end-in, strlen(in));
  TEST_ASSERT(end == in + strlen(in));

  EXPECT_STR("\n\n\n\"a\"", "a"); // whitespace before the string
  TEST_ASSERT(end == in + strlen(in));

  EXPECT_STR(";; Comment\n\"az!$&%*/:<=>?@^_~AZ\"", "az!$&%*/:<=>?@^_~AZ");
  TEST_ASSERT(end == in+strlen(in));

  EXPECT_ERROR("\"\\\"");	// "\"" ==> "

  EXPECT_STR("\"\\\\\"", "\\");	// "\\" ==> "\"  a single backslash inside quotes

  EXPECT_STR("\"\\r\"", "\r");	// "\r" ==> return

  EXPECT_STR("\"\\r\\n\\t\"", "\r\n\t"); // "\r\n\t" ==> return newline tab
  TEST_ASSERT(end == in+strlen(in));
  
  /* ----------------------------------------------------------------------------- */


  TEST_SECTION("Comments");

#define EXPECT_EOF(input) do {				\
    SET(input);						\
    s = read_sexp(&end);				\
    TEST_ASSERT_NULL(s);				\
  } while (0);  

  EXPECT_EOF(";");
  EXPECT_EOF(";       ");
  EXPECT_EOF(";    Hello, world!   ");
  EXPECT_EOF(";\n       ");
  
  // Comment stops at newline
  EXPECT_ID(";; Hello, world!\nNext line not part of comment.", "Next");
  TEST_ASSERT(end == in + 21);
  

  // -----------------------------------------------------------------------------
  TEST_SECTION("Token length and format errors");

#define EXPECT_THIS_ERROR(kind) do {			\
    s = read_sexp(&end);				\
    TEST_ASSERT(s && sexp_errorp(s));			\
    TEST_ASSERT(sexp_error_info(s, NULL) == (kind));	\
    free_sexp(s);					\
  } while (0);  

  // ID
  
  // An identifier can be as long as MAX_IDLEN bytes
  FILL('A', MAX_IDLEN);
  TEST_ASSERT(strnlen(in, MAX_BUF) == MAX_IDLEN);
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_identifierp(s));
  TEST_ASSERT(strncmp(String_ptr(s->str),
		      in,
		      String_len(s->str))
	      == 0);
  free_sexp(s);					
  TEST_ASSERT(end == in+strlen(in));     

  // One more, and it's an error
  FILL('a', MAX_IDLEN + 1);
  TEST_ASSERT(strnlen(in, MAX_BUF) > MAX_IDLEN);
  EXPECT_THIS_ERROR(TOKEN_BAD_IDENTIFIER);

  SET("+a");
  EXPECT_THIS_ERROR(TOKEN_BAD_INT);
  SET("a\7b");
  EXPECT_THIS_ERROR(TOKEN_BAD_CHAR);

  // INT

  FILL('0', MAX_INT_LEN);
  TEST_ASSERT(strnlen(in, MAX_BUF) == MAX_INT_LEN);
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_intp(s));
  TEST_ASSERT(end == in+strlen(in));     
  TEST_ASSERT(s->n == 0);
  free_sexp(s);

  FILL('0', MAX_INT_LEN);
  in[0] = '-'; in[1] = '1';
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_intp(s));
  n = -1;
  for (int i = 0; i < (MAX_INT_LEN-2); i++) n *= 10;
  TEST_ASSERT(s->n == n);
  free_sexp(s);

  // This number is outside the range of an int64_t
  FILL('1', MAX_INT_LEN);
  fprintf(stderr, "Expect a warning here:\n");
  s = read_sexp(&end);
  TEST_ASSERT_NULL(s);
  
  // This number is outside the range of an int64_t
  FILL('9', MAX_INT_LEN);
  in[0] = '-';
  fprintf(stderr, "Expect a warning here:\n");
  s = read_sexp(&end);
  TEST_ASSERT_NULL(s);

  SET("1a");
  EXPECT_THIS_ERROR(TOKEN_BAD_INT);

  // BYTES

  FILL('a', MAX_BYTES_LEN);
  in[0] = '"';
  in[MAX_BYTES_LEN-1] = '"';
  TEST_ASSERT(strnlen(in, MAX_BUF) == MAX_BYTES_LEN);
  s = read_sexp(&end);
  TEST_ASSERT(s);
  TEST_ASSERT(sexp_bytestringp(s));
  TEST_ASSERT(end == in+strlen(in));     
  free_sexp(s);

  FILL('b', MAX_BYTES_LEN + 3);
  in[0] = '"';
  in[MAX_BYTES_LEN + 2] = '"';
  TEST_ASSERT(strnlen(in, MAX_BUF) == MAX_BYTES_LEN + 3);
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_LEN);
  free_sexp(s);

  SET("\"");
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_BYTESTRING);
  free_sexp(s);

  SET("\"\\x\"");
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_ESC);
  free_sexp(s);
  
  SET("\"\\xf\"");
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_ESC);
  free_sexp(s);

  SET("\"\n\"");
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_bytestringp(s));
  TEST_ASSERT(String_len(s->str) == 1);
  TEST_ASSERT(*String_ptr(s->str) == '\n');
  free_sexp(s);

  // \a is not a valid escape
  SET("\"\\n\\a\\t\"");
  // Verify that the input is right before we do the real test
  TEST_ASSERT((in[3] == '\\') && (in[4] == 'a')); 
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_ESC);
  // Check that cursor is positioned at start of bad escape sequence '\a'
  TEST_ASSERT(end == in+3);
  free_sexp(s);

  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Printing bytestring tokens");

  SET("\"Hello!\\n\\t\\x40\""); // Hello!\n\t@
  s = read_sexp(&end);
  TEST_ASSERT(s && sexp_bytestringp(s));
  TEST_ASSERT(end == in+strlen(in));
  if (1) {
    printf("1. Input: %s\n", in);
    printf("2. S-expression: "); print_sexp_repr(s); newline();
  }
  tmpString = String_unescape(s->str, &err);
  TEST_ASSERT_NULL(err);
  TEST_ASSERT(tmpString);
  const char *actual_string = "Hello!\n\t@";
  TEST_ASSERT(String_string_cmp(tmpString, actual_string, strlen(actual_string)) == 0);
  String_free(tmpString);
  free_sexp(s);
  
  /* ----------------------------------------------------------------------------- */

  TEST_SECTION("Low-level token reader API");

  Token tok;
  
  SET(";; Hello, world!\nNext line");
  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_COMMENT);
  TEST_ASSERT(end == in+16);	// Comment stops at newline
  
  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_WS);
  TEST_ASSERT(end == in+16+1);     // Newline after comment

  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_IDENTIFIER);
  TEST_ASSERT(end == in+16+1+4);   // "Next" on next line
  TEST_ASSERT(strncmp(tok.start, "Next", tok.len) == 0);

  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_WS);
  TEST_ASSERT(end == in+16+1+4+1); // Space after "Next"

  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_IDENTIFIER);
  TEST_ASSERT(end == in+16+1+4+1+4); // "line"
  TEST_ASSERT(strncmp(tok.start, "line", tok.len) == 0);
  
  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_EOF);
  TEST_ASSERT(end == in+strlen(in));

  // #\\u26C4 is a snowperson emoji ⛄ 
  SET("(a b c) #\\A #\\n #\\xFF \"Hi!\"  (1000)\t\t#\\u26C4;;;comment\n-12345");
  end = in;
  if (PRINTING) {
    printf("About to read this input: \n-----\n%s\n-----\n", end);
    printf("The tokens read are:\n");
  }
  int count = 25;
  do {
    tok = sexp_read_token(&end);
    if (PRINTING) {
      tmp = write_token_repr(tok);
      printf("%s\n", tmp);
      free(tmp);
    }
    count--;
  } while (tok.type != TOKEN_EOF);
  if (PRINTING) printf("-----\n");
  TEST_ASSERT(count == 0);
  
  SET("))");
  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_CLOSE);
  TEST_ASSERT(end == in+1);
  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_CLOSE);
  TEST_ASSERT(end == in+2);
  tok = sexp_read_token(&end);
  TEST_ASSERT(tok.type == TOKEN_EOF);
  TEST_ASSERT(end == in+2);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Lists");

  s = sexp_null();
  TEST_ASSERT(s);
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  str = write_sexp(s);
  TEST_ASSERT(str);
  TEST_ASSERT(strcmp(str, "()") == 0);
  TEST_ASSERT(sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(!sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(sexp_proper_listp(s));
  free_sexp(s);
  string_const_free(str);

  s = sexp_cons(sexp_null(), sexp_null());
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(s);
  str = write_sexp(s);
  TEST_ASSERT(str);
  TEST_ASSERT(strcmp(str, "(())") == 0);
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(sexp_proper_listp(s));
  free_sexp(s);
  string_const_free(str);
  
  // -----------------------------------------------------------------------------
  TEST_SECTION("Low-level sexp API to construct s-expressions without parsing\n");

  set(in, "Identifier-9");
  s = sexp_identifier(in, strlen(in));
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(!sexp_listp(s));
  TEST_ASSERT(!sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  free_sexp(s);

  // Bytestrings can contain null characters, written \x00
  SET("A\\x00\\nB");
  s = sexp_bytestring(in, strlen(in));
  TEST_ASSERT(s);
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(!sexp_listp(s));
  TEST_ASSERT(!sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  // Check for correct string AND that we added an extra NUL at the end
  TEST_ASSERT(String_string_cmp(s->str, "A\0\nB", 4) == 0);
  free_sexp(s);
  
  SET("u2603");                // SNOWMAN: ☃
  s = sexp_codepoint(in, strlen(in));
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(!sexp_listp(s));
  TEST_ASSERT(!sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  free_sexp(s);

  Sexp *null = sexp_null();
  Sexp *snowman = sexp_codepoint(in, strlen(in));
  set(in, "A");
  Sexp *id_A = sexp_identifier(in, strlen(in));
  set(in, "B");
  Sexp *id_B = sexp_identifier(in, strlen(in));
  s = sexp_cons(snowman, null);
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));

  s = sexp_cons(id_B, s);
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(sexp_proper_listp(s));

  s = sexp_cons(id_A, s);
  if (PRINTING) {
    print_sexp_repr(s); newline();
    print_sexp(s); newline();
  }
  TEST_ASSERT(sexp_proper_listp(s));

  s = sexp_nreverse(s);
  if (PRINTING) {
    print_sexp(s); newline();
  }
  TEST_ASSERT(sexp_proper_listp(s));
  free_sexp(s);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Parsing strings");

  // TODO: Implement sexp_equal() and then replace the strcmp in
  // READ_WRITE_TEST with an s-expression comparison.

#define READ_WRITE_TEST(s) do {				\
    char *rwtest_str = write_sexp(s);			\
    const char *cursor = rwtest_str;			\
    Sexp *rwtest_exp = read_sexp(&cursor);		\
    char *rwtest_str2 = write_sexp(rwtest_exp);		\
    if (strcmp(rwtest_str, rwtest_str2) != 0) 		\
      printf("str: %s\nstr2: %s\n", rwtest_str, rwtest_str2);	\
    TEST_ASSERT(strcmp(rwtest_str, rwtest_str2) == 0);	\
    free(rwtest_str);					\
    free(rwtest_str2);					\
    free_sexp(rwtest_exp);				\
  } while (0);

  SET("foo");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_IDENTIFIER);
  READ_WRITE_TEST(s);
  free_sexp(s);

  SET("-201");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_INT);
  READ_WRITE_TEST(s);
  free_sexp(s);

#define codepoint_or_byte_test(input, val, kind) do {			\
    SET(input);								\
    s = read_sexp(&end);						\
    if (PRINTING) {print_sexp(s); newline();}				\
    TEST_ASSERT(s->type == (kind));					\
    if (PRINTING)							\
      printf("Codepoint/byte value = %d\n", s->codepoint);	\
    TEST_ASSERT(s->codepoint == (val));				\
    READ_WRITE_TEST(s);							\
    free_sexp(s);							\
  } while (0);

#define codepointtest(input, val) do {				\
    codepoint_or_byte_test((input), (val), SEXP_CHAR);		\
  } while(0);

  codepointtest("#\\x00", 0);
  codepointtest("#\\u0000", 0);
  codepointtest("#\\U00000000", 0);
  codepointtest("#\\x41", 0x41);
  codepointtest("#\\u0041", 0x41);
  codepointtest("#\\U00000041", 0x0041);
  codepointtest("#\\uF041", 0xf041);
  codepointtest("#\\U0000F041", 0xf041);
  codepointtest("#\\U1000F041", 0x1000f041);
  codepointtest("#\\1", '1');	// the character '1'
  codepointtest("#\\z", 'z');	// the character 'z'
  codepointtest("#\\+", '+');	// the character '+'
  codepointtest("#\\x", 'x');	// the character 'x'
  codepointtest("#\\\\", '\\');	// backslash character

  SET("()");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_NULL);
  TEST_ASSERT(sexp_nullp(s));
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(sexp_length(s) == 0);
  READ_WRITE_TEST(s);
  free_sexp(s);
  
  SET("(a)");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CONS);
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(sexp_length(s) == 1);
  TEST_ASSERT(sexp_car(s)->type == SEXP_IDENTIFIER);
  TEST_ASSERT(sexp_car(s)->str);
  TEST_ASSERT(*String_ptr(sexp_car(s)->str) == 'a');
  READ_WRITE_TEST(s);
  free_sexp(s);
  
  SET("(())");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CONS);
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(sexp_length(s) == 1);
  READ_WRITE_TEST(s);
  free_sexp(s);

  SET("(() ())");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CONS);
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(sexp_length(s) == 2);
  READ_WRITE_TEST(s);
  free_sexp(s);

  SET("(a 0 -1 #\\x7F)");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CONS);
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(sexp_length(s) == 4);
  READ_WRITE_TEST(s);
  free_sexp(s);

  SET("(0 (1 (2 (3))))");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CONS);
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(sexp_length(s) == 2);
  TEST_ASSERT(sexp_car(s)->type == SEXP_INT);
  TEST_ASSERT(sexp_car(s)->n == 0);
  TEST_ASSERT(sexp_consp(sexp_cdr(s)));
  TEST_ASSERT(sexp_car(sexp_car(sexp_cdr(s)))->type == SEXP_INT);
  TEST_ASSERT(sexp_car(sexp_car(sexp_cdr(s)))->n == 1);
  READ_WRITE_TEST(s);
  free_sexp(s);
  
  // With whitespace and comments thrown in
  SET("(0 (1 (2\t(\n3   ) ) ;Hi!\n))");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CONS);
  TEST_ASSERT(sexp_proper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(sexp_length(s) == 2);
  TEST_ASSERT(sexp_car(s)->type == SEXP_INT);
  TEST_ASSERT(sexp_car(s)->n == 0);
  TEST_ASSERT(sexp_consp(sexp_cdr(s)));
  TEST_ASSERT(sexp_car(sexp_car(sexp_cdr(s)))->type == SEXP_INT);
  TEST_ASSERT(sexp_car(sexp_car(sexp_cdr(s)))->n == 1);
  READ_WRITE_TEST(s);
  free_sexp(s);

  // With whitespace and comments thrown in
  SET("  ;Comment followed by identifier\nFOO");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_IDENTIFIER);
  TEST_ASSERT(memcmp(String_ptr(s->str), "FOO", 3) == 0);
  READ_WRITE_TEST(s);
  free_sexp(s);
  
  // With whitespace and comments thrown in
  SET("  ;Comment followed by int\n-3");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_INT);
  TEST_ASSERT(s->n == -3);
  READ_WRITE_TEST(s);
  free_sexp(s);

  // With whitespace and comments thrown in
  SET("\n\n;Comment followed by int\n123\n\n");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_INT);
  TEST_ASSERT(s->n == 123);
  READ_WRITE_TEST(s);
  free_sexp(s);
  
  // With whitespace and comments thrown in
  SET("\n\n;Comment followed by string\n\"abc\"");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_BYTESTRING);
  TEST_ASSERT(memcmp(String_ptr(s->str), "abc", 3) == 0);
  READ_WRITE_TEST(s);
  free_sexp(s);

  // With whitespace and comments thrown in
  SET("\n\n;Comment followed by string\n\"abc\"; Another comment");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_BYTESTRING);
  TEST_ASSERT(memcmp(String_ptr(s->str), "abc", 3) == 0);
  READ_WRITE_TEST(s);
  free_sexp(s);
  
  // With whitespace and comments thrown in
  SET(";Comment followed by codepoint\n#\\u0041");
  s = read_sexp(&end);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(s->type == SEXP_CHAR);
  TEST_ASSERT(s->codepoint == 0x41);
  READ_WRITE_TEST(s);
  free_sexp(s);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Dotted pairs (improper lists");

  // Make sure we can construct, examine, and render a dotted pair,
  // even though we do not intend to use them.

  // First a simple test using a single cons cell: (a . b)
  SET("a");
  Sexp *a = read_sexp(&end);
  TEST_ASSERT(a && sexp_identifierp(a));
  SET("b");
  Sexp *b = read_sexp(&end);
  TEST_ASSERT(b && sexp_identifierp(b));
  s = sexp_cons(a, b);
  TEST_ASSERT(s);
  TEST_ASSERT(!sexp_errorp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(!sexp_extraclosep(s));
  print_sexp_repr(s); newline();
  str = write_sexp(s);
  TEST_ASSERT(str);
  TEST_ASSERT(strcmp(str, "(a . b)") == 0);
  string_const_free(str);

  Sexp *r = sexp_nreverse(s);
  TEST_ASSERT(r);
  TEST_ASSERT(!sexp_errorp(r));
  TEST_ASSERT(sexp_consp(r));
  TEST_ASSERT(sexp_listp(r));
  TEST_ASSERT(!sexp_incompletep(r));
  TEST_ASSERT(!sexp_extraclosep(r));
  print_sexp_repr(r); newline();
  str = write_sexp(r);
  TEST_ASSERT(str);
  puts(str);
  TEST_ASSERT(strcmp(str, "(b . a)") == 0);
  string_const_free(str);

  Sexp *copy = sexp_deepcopy(r);
  TEST_ASSERT(copy);
  str = write_sexp(copy);
  TEST_ASSERT(str);
  TEST_ASSERT(strcmp(str, "(b . a)") == 0);
  string_const_free(str);
  free_sexp(copy);

  // r is s, so we free only r.
  // a and b are part of r, so freeing r will free them.
  free_sexp(r);

  // Next a longer improper list: (a (b . c))
  SET("a");
  a = read_sexp(&end);
  TEST_ASSERT(a && sexp_identifierp(a));
  SET("b");
  b = read_sexp(&end);
  TEST_ASSERT(b && sexp_identifierp(b));
  SET("c");
  Sexp *c = read_sexp(&end);
  TEST_ASSERT(c && sexp_identifierp(c));
  s = sexp_cons(b, c);
  TEST_ASSERT(s);
  s = sexp_cons(s, sexp_null());
  TEST_ASSERT(s);
  s = sexp_cons(a, s);
  TEST_ASSERT(!sexp_errorp(s));
  TEST_ASSERT(sexp_consp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_ASSERT(!sexp_extraclosep(s));
  print_sexp_repr(s); newline();
  str = write_sexp(s);
  TEST_ASSERT(str);
  TEST_ASSERT(strcmp(str, "(a (b . c))") == 0);
  string_const_free(str);

  r = sexp_nreverse(s);
  TEST_ASSERT(r);
  TEST_ASSERT(!sexp_errorp(r));
  TEST_ASSERT(sexp_consp(r));
  TEST_ASSERT(sexp_listp(r));
  TEST_ASSERT(!sexp_incompletep(r));
  TEST_ASSERT(!sexp_extraclosep(r));
  print_sexp_repr(r); newline();
  str = write_sexp(r);
  TEST_ASSERT(str);
  puts(str);
  // reversing an improper list is ill-defined, so we get to choose
  // what is the correct output, which is:
  TEST_ASSERT(strcmp(str, "((b . c) a)") == 0);
  string_const_free(str);

  copy = sexp_deepcopy(r);
  TEST_ASSERT(copy);
  str = write_sexp(copy);
  TEST_ASSERT(str);
  TEST_ASSERT(strcmp(str, "((b . c) a)") == 0);
  string_const_free(str);
  free_sexp(copy);

  // r is s, so we free only r.
  // a and b are part of r, so freeing r will free them.
  free_sexp(r);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Parsing errors");

  SET("");
  s = read_sexp(&end);
  TEST_ASSERT(s == NULL);

  s = read_sexp(&end);
  TEST_ASSERT(s == NULL);

  s = read_sexp(NULL);
  TEST_ASSERT(s);
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_PANIC);
  free_sexp(s);
  
  // Bad integer
  SET("+");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_INT);
  if (PRINTING) {print_sexp_repr(s); newline();}
  free_sexp(s);

  // Bad character: `
  SET("`");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);
  if (PRINTING) {print_sexp_repr(s); newline();}
  free_sexp(s);

  // Bad character in list
  SET("(1 2 3 ` 4)");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);
  TEST_ASSERT(strncmp(String_ptr(s->error.data), "`", String_len(s->error.data)) == 0);
  TEST_ASSERT(!sexp_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(!sexp_listp(s));
  TEST_ASSERT(!sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  TEST_EXPECT_WARNING;
  TEST_ASSERT(!sexp_proper_listp(s));
  TEST_EXPECT_WARNING;
  TEST_ASSERT(!sexp_improper_listp(s));
  TEST_EXPECT_WARNING;
  TEST_ASSERT(!sexp_incomplete_listp(s));
  free_sexp(s);

  // Bad character in a nested list
  SET("(1 (2 (3 ` 4)))");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);
  TEST_ASSERT(strncmp(String_ptr(s->error.data), "`", String_len(s->error.data)) == 0);
  TEST_EXPECT_WARNING;
  TEST_ASSERT(!sexp_proper_listp(s));
  TEST_EXPECT_WARNING;
  TEST_ASSERT(!sexp_improper_listp(s));
  TEST_EXPECT_WARNING;
  TEST_ASSERT(!sexp_incomplete_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(!sexp_listp(s));
  TEST_ASSERT(!sexp_consp(s));
  TEST_ASSERT(!sexp_incompletep(s));
  free_sexp(s);

#define byteerrortest(input) do {		\
    SET(input);					\
    s = read_sexp(&end);			\
    if (PRINTING) {print_sexp(s); newline();}	\
    TEST_ASSERT(s->type == SEXP_ERROR);		\
    TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);	\
    free_sexp(s);				\
  } while (0);

  byteerrortest("#\\xf");
  byteerrortest("#\\xfff");
  byteerrortest("#\\xfg");

  // Only printable chars allowed after backslash
  SET("#\\ ");	
  s = read_sexp(&end);
  TEST_ASSERT(s->type == SEXP_ERROR);
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_SHARP);
  free_sexp(s);
  SET("#\\\n");	
  s = read_sexp(&end);
  TEST_ASSERT(s->type == SEXP_ERROR);
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_SHARP);
  free_sexp(s);

  // Unfinished list
  SET("(");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_incomplete_listp(s));
  free_sexp(s);

  // Unfinished list
  SET("(1");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_incomplete_listp(s));
  free_sexp(s);

  // Unfinished list inside a list
  SET("((((1");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_incomplete_listp(s));
  free_sexp(s);
  
  // Unfinished list with more than one element
  SET("(1 2 3 ");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_incomplete_listp(s));
  TEST_ASSERT(!sexp_proper_listp(s));
  TEST_ASSERT(!sexp_improper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  free_sexp(s);

  // Unfinished list inside another list with more than one element
  SET("(1 2 (3 ");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_incomplete_listp(s));
  TEST_ASSERT(!sexp_proper_listp(s));
  TEST_ASSERT(!sexp_improper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  free_sexp(s);
  
  // Unfinished list containing other lists
  SET("((1) (2) (3) ");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp_repr(s); newline();}
  TEST_ASSERT(sexp_incomplete_listp(s));
  TEST_ASSERT(!sexp_proper_listp(s));
  TEST_ASSERT(!sexp_improper_listp(s));
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  TEST_ASSERT(sexp_consp(s));
  free_sexp(s);

  // Extra close
  SET(")");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp_repr(s); newline();}
  TEST_ASSERT(sexp_extraclosep(s));
  free_sexp(s);
  
  // Extra close after list
  SET("(#\\x00 #\\x01))");
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(!sexp_nullp(s));
  TEST_ASSERT(sexp_listp(s));
  free_sexp(s);
  s = read_sexp(&end);
  TEST_ASSERT(s);
  TEST_ASSERT(sexp_extraclosep(s));
  free_sexp(s);

  SET("km!<((X( )) h");
  in[6] = (signed char) 246;    // Replace the 'X' with an unprintable
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_identifierp(s));
  free_sexp(s);
  s = read_sexp(&end);
  TEST_ASSERT(s);
  if (PRINTING) {print_sexp(s); newline();}
  TEST_ASSERT(sexp_errorp(s));
  TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);
  free_sexp(s);

  // -----------------------------------------------------------------------------
  TEST_SECTION("List operations on S-expressions");

  Sexp *expected_ls;
  char *r_str, *expected_str;

#define nreversetest(input, expected) do {			\
    SET(input);							\
    s = read_sexp(&end);					\
    TEST_ASSERT(s && !sexp_errorp(s));				\
    if (PRINTING) {						\
      printf("s-expression: ");					\
      print_sexp(s); newline();					\
    }								\
    r = sexp_nreverse(s);					\
    TEST_ASSERT(r && !sexp_errorp(r));				\
    if (PRINTING) {						\
      printf("     reverse: ");					\
      print_sexp(r); newline();					\
    }								\
    r_str = write_sexp(r);					\
    TEST_ASSERT(r_str);						\
    SET(expected);						\
    expected_ls = read_sexp(&end);				\
    TEST_ASSERT(expected_ls && !sexp_errorp(expected_ls));	\
    expected_str = write_sexp(expected_ls);			\
    TEST_ASSERT(expected_str);					\
    TEST_ASSERT(strcmp(r_str, expected_str) == 0);		\
    free(r_str);						\
    free(expected_str);						\
    free_sexp(expected_ls);					\
    free_sexp(r); /* nreverse is destructive, so this is s */	\
  } while (0);

  nreversetest("()", "()");
  nreversetest("(a)", "(a)");
  nreversetest("(a 1)", "(1 a)");
  nreversetest("(a (1))", "((1) a)");
  nreversetest("((a) 1)", "(1 (a))");
  nreversetest("(((((a)))) b)", "(b ((((a)))))");
  nreversetest("((a) (b))", "((b) (a))");
  nreversetest("(1 (2 (3)))", "((2 (3)) 1)");

#define DEEPCOPYTEST(input) do {				\
    if (1) printf("input: '%s'\n", input);			\
    SET(input);							\
    s = read_sexp(&end);					\
    TEST_ASSERT(s && !sexp_errorp(s));				\
    if (1) {						\
      printf("s-expression %s: ",				\
	     sexp_type_name(s->type));				\
      print_sexp(s); newline();					\
    }								\
    r = sexp_deepcopy(s);					\
    TEST_ASSERT(r && !sexp_errorp(r));				\
    if (1) {						\
      printf("   deep copy: ");					\
      print_sexp(r); newline();					\
    }								\
    r_str = write_sexp(r);					\
    TEST_ASSERT(r_str);						\
    strdiff = strcmp(r_str, input);				\
    if (strdiff)						\
      printf("input = '%s', r_str = '%s'\n", input, r_str);	\
    TEST_ASSERT(strdiff == 0);					\
    free(r_str);						\
    free_sexp(r);						\
    free_sexp(s);						\
  } while (0);

  DEEPCOPYTEST("1234");
  DEEPCOPYTEST("-5432345");
  DEEPCOPYTEST("x");		// short identifier
  DEEPCOPYTEST("identifier");
  DEEPCOPYTEST("\"bytes\"");
  // newlines and tabs render as \n and \t, so we can't include them
  DEEPCOPYTEST("\"some bytes here\"");
  DEEPCOPYTEST("#\\x");
  DEEPCOPYTEST("#\\y");
  DEEPCOPYTEST("#\\+");
  DEEPCOPYTEST("#\\xFF");
  DEEPCOPYTEST("#\\uFFFF");
  DEEPCOPYTEST("#\\UFFFFFFFF");
  DEEPCOPYTEST("()");
  DEEPCOPYTEST("(a)");
  DEEPCOPYTEST("(a 1)");
  DEEPCOPYTEST("(a (1))");
  DEEPCOPYTEST("((a) 1)");
  DEEPCOPYTEST("(((((a)))) b)");
  DEEPCOPYTEST("((a) (b))");
  DEEPCOPYTEST("(1 (2 (3)))");
  DEEPCOPYTEST("(1 (2 (3) ((4)) (((5)))) 6 7 8)");

  // These inputs, after being converted to s-expressions, print out
  // differently than they looked as inputs.  So the DEEPCOPYTEST
  // defined above cannot check them without making some enhancements.
  //
  //   DEEPCOPYTEST("(");		// incomplete
  //   DEEPCOPYTEST("((1)");	        // incomplete
  //   DEEPCOPYTEST(")");		// extra close

  // -----------------------------------------------------------------------------
  TEST_SECTION("Reading an S-expression made of bytestrings");

  // Construct a single sexp of the form: (b_1 b_2 b_3 ... b_n)
  // where each b_i is a randomly generated bytestring.
  const size_t limit = FUZZING ? (2 * STRING_APPEND_LIMIT) : 5000;
  char *buf = malloc(2 * limit);
  len = 0;
  buf[len++] = '(';
  assert(MAX_BYTES_LEN < STRING_APPEND_LIMIT);
  while (len <= limit) {
    size_t tmplen;
    generate_bytestring(in, GOOD_BYTES);
    tmplen = strlen(in);
    memcpy(buf+len, in, tmplen);
    len += tmplen;
    buf[len++] = ' ';
  }
  buf[len++] = ')';
  buf[len++] = '\0';
  if (PRINTING)
    printf("Input len = %zu; (%ld larger than limit of %zu)\n",
	   len, (ssize_t) ((size_t) len - limit), limit);
  str = buf;
  end = str;
  s = read_sexp(&end);
  TEST_ASSERT(s);
  printf("The sexp_length of s is %d\n", sexp_length(s));
  free(buf);
  free_sexp(s);		   // This failed when its impl was recursive!

  fflush(NULL);
  
  // -----------------------------------------------------------------------------
  TEST_SECTION("Reading a deeply nested s-expression");

  // Construct a single sexp of the form: (1 (0 (1 (0 (1 ...)))))
  //
  // Using a recursive implementation of read_sexp(), a depth of
  // around 8000 causes a stack overflow (segfault, with no way to
  // catch it).  Our defunctionalized continuation-passing version is
  // limited only by available memory.
  const int maxdepth = FUZZING ? (1000 * 1000) : (1 * 1000);
  int depth = 0;
  size_t calculated_strlen = maxdepth * 4;
  buf = malloc(calculated_strlen + 1);
  len = 0;
  for (depth = 0; depth < maxdepth; depth++) {
    buf[len++] = '(';
    buf[len] = (len & 1) + '0';
    len++;
    buf[len++] = ' ';
  }
  len--;  // remove the trailing space
  for (depth = 0; depth < maxdepth; depth++) {
    buf[len++] = ')';
  }
  buf[len++] = '\0';
  TEST_ASSERT(len == calculated_strlen);
  str = buf;
  end = str;
  s = read_sexp(&end);
  TEST_ASSERT(s);

  TEST_ASSERT(sexp_length(s) == 2);
  TEST_ASSERT(sexp_car(s)->type == SEXP_INT);
  TEST_ASSERT(sexp_car(s)->n == 1);
  TEST_ASSERT(sexp_consp(sexp_cdr(s)));
  TEST_ASSERT(sexp_car(sexp_car(sexp_cdr(s)))->type == SEXP_INT);
  TEST_ASSERT(sexp_car(sexp_car(sexp_cdr(s)))->n == 0);

  Sexp *next = s;
  while (sexp_consp(sexp_cdr(next))) next = sexp_car(sexp_cdr(next));
  printf("Deepest/last element of s: ");
  print_sexp(next); newline();
  TEST_ASSERT(sexp_consp(next));
  TEST_ASSERT(sexp_intp(sexp_car(next)));
  TEST_ASSERT(sexp_car(next)->n == (maxdepth & 1));
  // Don't free 'next' because it is part of 's'.

  // Now we write this long s-expression (producing a string)
  char *long_output = write_sexp(s);

  if (PRINTING) {
    printf("Length of input string = %zu\n", strlen(str));
    printf("First chars of input string are: %.*s\n", 40, str);
    printf("Length of printed sexp = %zu\n", strlen(long_output));
    printf("First chars of printed sexp are: %.*s\n", 40, long_output);
  }

  // It must match the input string
  TEST_ASSERT(strlen(long_output) == strlen(str));
  TEST_ASSERT(strcmp(str, long_output) == 0);

  // Check for the right number of close parens at the end
  char *firstclose = strchr(long_output, ')');
  TEST_ASSERT(firstclose);
  char *last = firstclose;
  while (*last == ')') last++;
  TEST_ASSERT(*last == '\0');
  TEST_ASSERT((last - firstclose) == maxdepth);

  if (FUZZING) {
    r = sexp_deepcopy(s); 
    TEST_ASSERT(r && !sexp_errorp(r)); 
    if (PRINT_ALL) { 
      printf("   deep copy: "); 
      print_sexp(r); newline(); 
    } 
    str = write_sexp(r); 
    TEST_ASSERT(str); 
    TEST_ASSERT(strcmp(str, long_output) == 0); 
    string_const_free(str); 
  }
  free(long_output);
  free(buf);

  if (FUZZING) READ_WRITE_TEST(s);

  Sexp *rev = sexp_nreverse(s);
  TEST_ASSERT(rev);
  TEST_ASSERT(sexp_consp(rev));
  TEST_ASSERT(sexp_consp(sexp_car(rev)));
  TEST_ASSERT(sexp_consp(sexp_cdr(rev)));
  TEST_ASSERT(sexp_intp(sexp_car(sexp_cdr(rev))));
  free_sexp(rev);
  // Cannot free s because sexp_nreverse is destructive, so rev IS s
  
  // -----------------------------------------------------------------------------
  TEST_SECTION("Rudimentary fuzzing for bytestrings");

  const int fuzziters = FUZZING ? 50000 : 1000;

  for (int i = 0; i < fuzziters; i++) {
    generate_bytestring(in, GOOD_BYTES);
    end = in;
    s = read_sexp(&end);
    if (!s) break;		// EOF
    if (sexp_errorp(s)) {
      puts("Error in test, expected a bytestring s-expression from this input:");
      printf("Input buf: '%s'\n", in);
      TEST_FAIL("Test implementation error");
    }
    TEST_ASSERT(sexp_bytestringp(s));
    READ_WRITE_TEST(s);
    free_sexp(s);
  }

  for (int i = 0; i < fuzziters; i++) {
    generate_bytestring(in, WITH_BAD_CHAR);
    end = in;
    s = read_sexp(&end);
    if (!s) break;		// EOF
    if (sexp_errorp(s)) {
      TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);
    } else {
      puts("Error in test, did not expect this s-expression:\n");
      print_sexp_repr(s);
      puts("\nto be generated from this input:\n");
      printf("Input buf: '%s'\n", in);
      TEST_FAIL("Test implementation error");
    }
    free_sexp(s);
  }
  
  // -----------------------------------------------------------------------------
  TEST_SECTION("Rudimentary fuzzing for identifiers");

  for (int i = 0; i < fuzziters; i++) {
    generate_identifier(in, GOOD_BYTES);
    end = in;
    s = read_sexp(&end);
    if (!s) break;		// EOF
    if (sexp_errorp(s)) {
      puts("Error in test, expected an identifier s-expression from this input:");
      printf("Input buf: '%s'\n", in);
      print_sexp_repr(s); newline();
      TEST_FAIL("Test implementation error");
    }
    TEST_ASSERT(sexp_identifierp(s));
    READ_WRITE_TEST(s);
    free_sexp(s);
  }

  for (int i = 0; i < fuzziters; i++) {
    generate_identifier(in, WITH_BAD_CHAR);
    end = in;
    s = read_sexp(&end);
    if (!s) break;		// EOF
    if (sexp_errorp(s)) {
      TEST_ASSERT(sexp_error_info(s, NULL) == TOKEN_BAD_CHAR);
    } else {
      puts("Error in test, did not expect this s-expression:");
      print_sexp_repr(s);
      puts("to be generated from this input:");
      printf("Input buf: '%s'\n", in);
      TEST_FAIL("Test implementation error");
    }
    free_sexp(s);
  }

  // -----------------------------------------------------------------------------
  TEST_SECTION("Rudimentary fuzzing of read_sexp() to look for segfaults");

  // This input string is a regression test
  SET("\"3(B2(ed(\nIeA3((l(z\tc(F6m((wj\n(C2XUnwE(O fqG5ug\nk\n((LHKAqc5Gg e^vY\twp(A(A DX( DU(FxsoHUG(i V((c(\t6C\t(\t((Gag9demF4W(\nwX Uc\tam(W(nkP1(p(ppe QaiUpi IA\tT\tg\n(iWOli(6DDuq7fuTbi(D(RuIE(ITZT(M qgO(qPiFzvEsG9 Z\tF(b(S0\n((3M6qEbyo((n(Z| \n\t(rtzE((JV(res(\nAGXEKqzUcGZ61(o (ZA>OGU1K(s\ttll(6\tRsqBn((l5(x\t(G\nh(\n(Z,\nUW fy (a(T(fMpNq(\nqNd(\taVNVvWGSn\tw(\nH(NV\nr(pC((6G(r1KI\n(EhtnGVy\tKw R~PX(KRk\thC Rz((JbECedKcdAx6sy(Sa4pfiJ3d(A\nh(Z(Gp\t9\nE(E\n\t P(o\n\t(rw\n7j(s\nGZ0(((((tB(1tEUnpDVj1iSV\nog\ns\tX9?p8vLPBsYX9i ((l(NyI((6\n8MJvjs(N(iQ\n(sFO4mkxR4Hz(hri3 jE=WPYX4(9S7n\tYK5\tBM((cpf(\n (t(KPjiXel58(4pD( \nbK,(ydU05FCDfLi\t(Ne(L( nyA(84aq\t(B(4tJ\tU(\tRu(vLwL6XG2\nfL(\t6fViM(l\nmf(d5a((XFb(woZPP8Zy\ny\tc\tH(\t5 FEae(TuA\t(vaj(CQ0q2((YmM\n(IOg7BU\nW(I(xMq (L((zA t(| (\t\nSTEkk(T(86(V(\nUl(TGqm((M(Ie\n(E(Y\n(-nDB6vSnKY(;uX V4(Ksq(6WR\natjytPw\n\teBR9Qw1\nzMIOxx((jW(n(czLB(\n(\tQ\nYtD(LF\t6d((\t(h(uph9hS q2n3Jh\n(\nY\tFqAaCbpnLEU\t \t9iheRCT o BM(\nEP\t0253S8f\nD({(T6UbguLmE5mkm(QqLgP\nE8HFHS(ByPf\t\nJh1O0J(AaSv(\"");
  s = read_sexp(&end);
  TEST_ASSERT(s && !sexp_errorp(s));
  if (PRINTING) {print_sexp_repr(s); newline();}
  READ_WRITE_TEST(s);
  free_sexp(s);

  int total_sexps = 0;
  int total_sexp_valid = 0;
  int total_sexp_errors = 0;
  int total_sexp_incomplete = 0;
  int total_sexp_extraclose = 0;
  int total_bad_chars = 0;
  int total_bad_integers = 0;
  int total_bad_sharps = 0;
  int total_long_strings = 0;
  int total_other_errors = 0;
  int total_chars = 0;

  int iterations = fuzziters/5;

  for (int i = 0; i < iterations; i++) {
    int sexp_count = 0;		// total = valid + error
    int sexp_valid_count = 0;
    int sexp_error_count = 0;
    int incomplete_count = 0;
    int extraclose_count = 0;
    int bad_char_count = 0;
    int bad_integer_count = 0;
    int bad_sharp_count = 0;
    int long_string_count = 0;
    int other_error_count = 0;
    generate_random_sexp(in);
    end = in;
    total_chars += strlen(in);
    if (PRINT_ALL) printf("input: '%s'\n", in);
    do {
      s = read_sexp(&end);
      if (!s) break;		// EOF
      sexp_count++;
      if (!sexp_errorp(s)) {
	sexp_valid_count++;
        if (sexp_listp(s) && sexp_incomplete_listp(s)) incomplete_count++;
        if (sexp_extraclosep(s)) extraclose_count++;
        if (PRINT_ALL) {print_sexp_repr(s); newline();}
	READ_WRITE_TEST(s);
      } else {
        sexp_error_count++;
        if (PRINT_ALL) {print_sexp_repr(s); newline();}
        switch (sexp_error_info(s, NULL)) {
	  case TOKEN_BAD_CHAR: bad_char_count++; break;
	  case TOKEN_BAD_INT: bad_integer_count++; break;
	  case TOKEN_BAD_SHARP: bad_sharp_count++; break;
	  case TOKEN_BAD_LEN: long_string_count++; break;
	  default: other_error_count++;
	}
	// Other errors include e.g. bad identifier, unterminated string
      }
      free_sexp(s);
    } while (true);

    if (PRINT_ALL)
      printf("From one random input of length %zu: %d sexps (%d incomplete, %d errors)\n",
             strlen(in), sexp_count, incomplete_count, sexp_error_count);

    total_sexps += sexp_count;
    total_sexp_valid += sexp_valid_count;
    total_sexp_incomplete += incomplete_count;
    total_sexp_errors += sexp_error_count;
    total_sexp_extraclose += extraclose_count;
    total_bad_chars += bad_char_count;
    total_bad_integers += bad_integer_count;
    total_bad_sharps += bad_sharp_count;
    total_long_strings += long_string_count;
    total_other_errors += other_error_count;
  }
  printf("\nGenerated and parsed %d random-ish strings:\n", iterations);
  printf("  Total of %d characters\n", total_chars);
  printf("  Read %8d sexps\n", total_sexps);
  printf("         %8d of those (%4.2f%%) were errors:\n", total_sexp_errors,
	 (100 * (float) total_sexp_errors / (float) total_sexps));
  printf("           %8d (%4.2f%%) bad chars\n", total_bad_chars,
	 (100 * (float) total_bad_chars / (float) total_sexp_errors));
  printf("           %8d (%4.2f%%) bad integers\n", total_bad_integers,
	 (100 * (float) total_bad_integers / (float) total_sexp_errors));
  printf("           %8d (%4.2f%%) bad sharps\n", total_bad_sharps,
	 (100 * (float) total_bad_sharps / (float) total_sexp_errors));
  printf("           %8d (%4.2f%%) strings too long\n", total_long_strings,
	 (100 * (float) total_long_strings / (float) total_sexp_errors));
  printf("           %8d (%4.2f%%) other errors\n", total_other_errors,
	 (100 * (float) total_other_errors / (float) total_sexp_errors));

  printf("         %8d of those (%4.2f%%) were valid:\n", total_sexp_valid,
	 (100 * (float) total_sexp_valid / (float) total_sexps));
  printf("           %8d (%4.2f%%) were incomplete\n", total_sexp_incomplete,
	 (100 * (float) total_sexp_incomplete / (float) total_sexp_valid));
  printf("           %8d (%4.2f%%) were an extra close paren\n", total_sexp_extraclose,
	 (100 * (float) total_sexp_extraclose / (float) total_sexp_valid));
  int no_issues = (total_sexp_valid - total_sexp_incomplete - total_sexp_extraclose);
  printf("           %8d (%4.2f%%) had no issues\n", no_issues,
	 (100 * (float) no_issues / (float) total_sexp_valid));

  TEST_ASSERT(total_sexps == total_sexp_valid + total_sexp_errors);

  TEST_END();
}
