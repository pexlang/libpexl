/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyzetest.c  TESTING analyze.c                                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include "print.h"
#include "charset.h"
#include "symboltable.h"
#include "old-analyze.h"

#include "../test.h"

int main(int argc, char **argv) {

  int nullable;
  pexl_Context *C = NULL;
  pexl_Index id;

  int stat;
  Charset cs;
  Node *node;
  unsigned char c;
  uint32_t min, max;

  pexl_Expr *tstr, *calltree;
  pexl_Expr *consttree; 
  pexl_Expr *t1, *t2, *t3, *t4; 
  pexl_Expr *A, *S;

  pexl_PackageTable *m; 
  pexl_Binary *pkg;
  pexl_Env toplevel, env, g;
  SymbolTable *st;
  pexl_Ref ref, refA, refS;

/*   size_t bufsize = 16;	/\* 15 chars and a NULL byte *\/ */
/*   char *buf = (char *)alloca(bufsize); */


  TEST_START(argc, argv);
  TEST_SECTION("Computing expression properties");

  /* pexl_Error checking */

  stat = exp_hascaptures(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_itself_hascaptures(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  
  stat = exp_patlen(NULL, &min, &max);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_nofail(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_nullable(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_headfail(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_needfollow(NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  stat = exp_getfirst(NULL, NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = exp_getfirst(NULL, NULL, &cs);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = exp_getfirst(NULL, fullset, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = exp_getfirst(NULL, fullset, &cs);
  TEST_ASSERT(stat == PEXL_ERR_NULL);

  C = context_new();
  TEST_ASSERT(C);
  TEST_ASSERT(C->bt);
  toplevel = 0;			// root env has id 0

  tstr = pexl_match_string(C, "hello");
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  stat = exp_getfirst(node, NULL, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = exp_getfirst(node, NULL, &cs);
  TEST_ASSERT(stat == PEXL_ERR_NULL);
  stat = exp_getfirst(node, fullset, NULL);
  TEST_ASSERT(stat == PEXL_ERR_NULL);


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Analyzing primitive patterns");

  printf("Checking literal string 'hello'\n");
  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 5) || (max != 5));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(!nullable);	/* literal non-empty string not nullable */
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(!exp_nofail(node)); /* literal non-empty string can fail */
  TEST_ASSERT(exp_headfail(node) == 0); /* literal multi-char string is NOT headfail */
  TEST_ASSERT(!exp_needfollow(node));	/* literal non-empty string does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);   /* firstset can be used as test, does not accept epsilon */
  print_charset(cs.cs);
  /* Now check the contents of cs for 'h' */
  for (c = 0; c < 255; c++)
    if (c == 'h')
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only 'h' */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only 'h' */

  printf("Checking TTrue created via empty literal string\n");
  pexl_free_Expr(tstr);
  tstr = pexl_match_string(C, "");
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  TEST_ASSERT(tstr->node->tag == TTrue); /* the 1 node is TTrue, which matches epsilon */
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable);	/* TTrue is nullable */
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node)); /* TTrue is nofail */
  TEST_ASSERT(exp_headfail(node) == 0); /* TTrue is not headfail */
  TEST_ASSERT(!exp_needfollow(node));	/* TTrue does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 1);	/* firstset cannot be used as test, exp accepts epsilon */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain every char */

  printf("Checking TAny\n");
  pexl_free_Expr(tstr);
  tstr = pexl_match_any(C, 1);
  TEST_ASSERT(tstr->len == 1);	/* should produce 1-node tree */
  TEST_ASSERT(tstr->node->tag == TAny); /* the node is TAny, which matches 1 byte */
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 1) && (max == 1));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(!nullable);	/* TAny is not nullable */
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(!exp_nofail(node)); /* TAny is not nofail */
  TEST_ASSERT(exp_headfail(node) == 1); /* TAny is headfail */
  TEST_ASSERT(!exp_needfollow(node));	/* TAny does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain every char */

  printf("Checking TSeq of TAny\n");
  pexl_free_Expr(tstr);
  tstr = pexl_match_any(C, 10000);
  TEST_ASSERT(tstr->len = 19999); /* should produce this size tree */
  TEST_ASSERT(tstr->node->tag == TSeq); /* the root node is Tseq */
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 10000) || (max != 10000));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(!nullable);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(!exp_nofail(node));
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(!exp_needfollow(node));	 /* exp does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain every char */

  printf("Checking TNot of TAny\n");
  pexl_free_Expr(tstr);
  tstr = pexl_match_any(C, -1);
  TEST_ASSERT(tstr->len == 2);	/* should produce this size tree */
  TEST_ASSERT(tstr->node->tag == TNot); /* the root node is TNot */
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(!exp_needfollow(node));	/* exp does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 1);	/* firstset cannot be used as test, exp accepts epsilon */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain no chars */

  printf("Checking TFalse\n");
  pexl_free_Expr(tstr);
  tstr = pexl_fail(C);
  TEST_ASSERT(tstr->len == 1);	/* should produce this size tree */
  TEST_ASSERT(tstr->node->tag == TFalse); /* the root node is TFalse */
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));
  TEST_ASSERT(!exp_nullable(node));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 1);
  TEST_ASSERT(!exp_needfollow(node));	/* exp does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain no chars */

  printf("Checking TSet\n");
  t3 = pexl_match_set(C, "abcdef", 6);
  TEST_ASSERT(t3->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t3->node->tag == TSet); /* tag should be TSet */
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 1) && (max == 1));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 1);
  TEST_ASSERT(!exp_needfollow(node));	/* exp does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    if (( c >= 'a') && (c <= 'f'))
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only a-f */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only a-f */
	
  t4 = pexl_match_set(C, "", 0);
  TEST_ASSERT(t4->len == 3);	/* a single charset occupies size of 3 nodes */
  TEST_ASSERT(t4->node->tag == TSet); /* tag should be TSet */
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 1) && (max == 1));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 1);
  TEST_ASSERT(!exp_needfollow(node));	/* exp does not need follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain no chars */


  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'seq'");

  t1 = pexl_match_string(C, "a");		     /* headfail */
  t2 = pexl_repeat_f(C, pexl_match_any(C, 1), 0, 0); /* .* is nofail */
  pexl_free_Expr(tstr);
  tstr = pexl_seq(C, t1, t2);		  /* a" .* */
  TEST_ASSERT(tstr->len == 4);
  node = tstr->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED); /* expected exp_patlen to return UNBOUNDED */
  TEST_ASSERT((min == 1));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 1);
  TEST_ASSERT(exp_needfollow(node));	/* exp would benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    if (c == 'a')
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only a */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only a */

  pexl_free_Expr(tstr);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'choice'");

  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3); pexl_free_Expr(t4);

  t1 = pexl_match_string(C, "ab");
  t2 = pexl_match_string(C, "c");
  t3 = pexl_choice(C, t1, t2);
  TEST_ASSERT(t3->len == 3);
  TEST_ASSERT(t3->node->tag == TChoice); /* should get TChoice */
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 1) && (max == 2));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(exp_needfollow(node));	/* exp would benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    if (c == 'a' || c == 'c')
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only a, c */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only a, c */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'not'");

  pexl_free_Expr(t1); pexl_free_Expr(t2);
  t1 = pexl_match_string(C, "A");
  t2 = pexl_neg_lookahead(C, t1);
  TEST_ASSERT(t2->len == 1 + t1->len);
  TEST_ASSERT(t2->node->tag == TNot);  /* Tnot */
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(!exp_needfollow(node));	/* exp would NOT benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 1);	/* exp accepts epsilon */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain all but A */
    else
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all but A */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookahead'");

  pexl_free_Expr(t1); pexl_free_Expr(t2);
  t1 = pexl_match_string(C, "A");
  t2 = pexl_lookahead(C, t1);
  TEST_ASSERT(t2->len == 1 + t1->len);
  TEST_ASSERT(t2->node->tag == TAhead); /* TAhead */
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 1);
  TEST_ASSERT(!exp_needfollow(node));	/* exp would NOT benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only A */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only A */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'rep'");

  pexl_free_Expr(t1); pexl_free_Expr(t2);
  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 0);
  TEST_ASSERT(t2->len == 1 + t1->len);
  TEST_ASSERT(t2->node->tag == TRep);  /* TRep */
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED); /* expected exp_patlen to return UNBOUNDED */
  TEST_ASSERT((min == 0));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 1);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(exp_needfollow(node));	/* exp would benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 1);	/* firstset cannot be used as test, exp accepts epsilon */
  /* Now check the contents of cs, which should be a full set */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all chars */

  pexl_free_Expr(t3);
  t3 = pexl_repeat(C, t1, 3, 0);	/* "A"{3,} */
  TEST_ASSERT(t3->len == 7 + t1->len);
  TEST_ASSERT(t3->node->tag == TSeq);  /* TSeq */
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_UNBOUNDED); /* expected exp_patlen to return UNBOUNDED */
  TEST_ASSERT((min == 3));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(exp_needfollow(node));	/* exp would benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 0);	/* firstset can be used as test, does not accept epsilon */
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain only A */
    else
      TEST_ASSERT(!testchar(cs.cs, c)); /* cs should contain only A */

  pexl_free_Expr(t1); pexl_free_Expr(t2);
  t1 = pexl_match_string(C, "ABC");
  t2 = pexl_repeat(C, t1, 0, 1);         /* "ABC"? ==> "ABC" / TTrue */
  TEST_ASSERT(t2->len == 2 + t1->len);
  TEST_ASSERT(t2->node->tag == TChoice);
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 3));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 1);
  TEST_ASSERT(exp_headfail(node) == 0);
  TEST_ASSERT(exp_needfollow(node));	/* exp would benefit from follow set */
  stat = exp_getfirst(node, fullset, &cs);
  TEST_ASSERT(stat == 1);	/* firstset cannot be used as test, exp accepts epsilon */
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all chars */

  pexl_free_Expr(t3);
  t3 = pexl_repeat(C, t1, 0, 3);	 /* "ABC"{0,3} */
  TEST_ASSERT(t3->len == 11);
  TEST_ASSERT(t3->node->tag == TChoice);
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 9));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 1);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would benefit from follow set */
  TEST_ASSERT(exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c)); /* cs should contain all chars */

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Using combinator 'lookbehind'");

  printf("Look back at string literal\n");
  pexl_free_Expr(t1); pexl_free_Expr(t2);
  t1 = pexl_match_string(C, "A");
  t2 = pexl_lookbehind(C, t1);
  TEST_ASSERT(t2->len == 2);
  TEST_ASSERT(t2->node->tag == TBehind);
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c));

  printf("Look back at \"A\"? which has variable length\n");
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);
  t1 = pexl_match_string(C, "A");
  t2 = pexl_repeat(C, t1, 0, 1);
  t3 = pexl_lookbehind(C, t2);
  TEST_ASSERT(t3);		/* the check for fixedlen happens later */
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  printf("This exp will fail to compile, because the target of the lookbehind has variable length.\n");

  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED); /* expected exp_patlen to return bounded */
  TEST_ASSERT((min == 0) && (max == 0));

  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c));

  printf("Look back at \"AB\"/\"CD\" which has length 2\n");
  pexl_free_Expr(t1); pexl_free_Expr(t2); pexl_free_Expr(t3);
  t1 = pexl_match_string(C, "AB");
  t2 = pexl_match_string(C, "CD");
  t3 = pexl_choice(C, t1, t2);
  t4 = pexl_lookbehind(C, t3);
  /* Acceptability of a lookbehind target is not checked until compile time */
  TEST_ASSERT(t4);
  TEST_ASSERT(t4->node->tag == TBehind);
  node = t4->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));

  stat = exp_patlen(child1(node), &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 2));

  printf("This exp will compile, because the target of the lookbehind has fixed length.\n");

  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 0) && (max == 0));

  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 1);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);
  /* Now check the contents of cs, which should contain all chars */
  for (c = 0; c < 255; c++)
    TEST_ASSERT(testchar(cs.cs, c));

  pexl_free_Expr(t4);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexlCapture");

  pexl_free_Expr(t1); pexl_free_Expr(t2);
  t1 = pexl_match_string(C, "AB");
  t2 = pexl_capture(C, "hi", t1);
  node = t2->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(node));
  TEST_ASSERT(exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 2) && (max == 2));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset can be used as test, does not accept epsilon */
  TEST_ASSERT(stat == 0);
  /* Now check the contents of cs, which should contain only A */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c));
    else
      TEST_ASSERT(!testchar(cs.cs, c));

  pexl_free_Expr(t3);
  t3 = pexl_seq(C, t2, t2);
  node = t3->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(exp_hascaptures(node));
  TEST_ASSERT(exp_itself_hascaptures(node));
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == EXP_BOUNDED);
  TEST_ASSERT((min == 4) && (max == 4));
  nullable = exp_nullable(node) || (node->tag == TFalse);
  TEST_ASSERT(nullable == 0);
  TEST_ASSERT(!(nullable && (min != 0)) && !(!nullable && (min == 0))); /* nullable disagrees with patlen */
  TEST_ASSERT(exp_nofail(node) == 0);
  TEST_ASSERT(exp_headfail(node) == 0);
  /* This exp would NOT benefit from follow set */
  TEST_ASSERT(!exp_needfollow(node));
  stat = exp_getfirst(node, fullset, &cs);
  /* firstset can be used as test, does not accept epsilon */
  TEST_ASSERT(stat == 0);
  /* Now check the contents of cs, which should contain only A */
  for (c = 0; c < 255; c++)
    if (c == 'A')
      TEST_ASSERT(testchar(cs.cs, c));
    else
      TEST_ASSERT(!testchar(cs.cs, c));

  pexl_free_Expr(t1);  pexl_free_Expr(t2);  pexl_free_Expr(t3);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Constant captures");

  fprintf(stderr, "Expect warning here:\n");
  consttree = pexl_insert(C, "", "");
  TEST_ASSERT(!consttree);	// name cannot be empty string

  consttree = pexl_insert(C, "x", "");
  TEST_ASSERT(consttree);
  pexl_free_Expr(consttree);

  consttree = pexl_insert(C, "foobar", "");
  TEST_ASSERT(consttree);

  printf("Printing with context:\n");
  pexl_print_Expr(consttree, 0, C);
  printf("Printing without context:\n");
  pexl_print_Expr(consttree, 0, NULL);
  pexl_free_Expr(consttree);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("pexl_References");

  m = packagetable_new(1);
  TEST_ASSERT(m);

  context_free(C);
  C = context_new();
  TEST_ASSERT(C);
  env = 0;

  st = symboltable_new(1, 1);
  TEST_ASSERT(st);
  /* Add empty string to symbol table using NULL instead of "" */
  stat = symboltable_add(st, NULL, 1, 0, 1, 0, 0, NULL);
  TEST_ASSERT(stat == 0);
  stat = symboltable_add(st, "foo", 1, 0, 1, 0, 0, NULL);
  TEST_ASSERT(stat == 1);

  pkg = binary_new();
  TEST_ASSERT(pkg);
  binary_free(pkg);

  /* Anonymous binding has string ID 0 */
  ref = env_bind(C->bt, env, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  id = context_intern(C, "foobar", 6);
  TEST_ASSERT(id >= 0);		// non-negative means success
  ref = env_bind(C->bt, env, id, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  TEST_ASSERT(ref == 1);	// second binding in bt is at index 1

  calltree = pexl_call(C, ref);
  TEST_ASSERT(calltree);
  TEST_ASSERT(calltree->node->a.bt == C->bt);
  TEST_ASSERT(calltree->node->b.index == ref);
  pexl_free_Expr(calltree);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Non-recursive block using anonymous exps");

  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  refS = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refS));

  A = pexl_call(C, refS);	/* A -> S */
  S = pexl_match_epsilon(C);	/* S -> true */
  node = A->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_patlen(node, &min, &max);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nullable(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nofail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  TEST_ASSERT(exp_needfollow(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_getfirst(node, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);

  pexl_free_Expr(A); pexl_free_Expr(S);
  printf("-----\n");

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Recursive block using anonymous exps");

  printf("Trying a single rule that is left recursive due to a self loop\n"); 
  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  refA = env_bind(C->bt, g, 0, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));
  A = pexl_call(C, ref);	/* A -> A */

  print_BindingTable(C); 
  printf("pexl_Expr *at refA: ");
  
  pexl_free_Expr(A);
  printf("-----\n");
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with realistic recursion");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  id = context_intern(C, "A", 1);
  TEST_ASSERT(id >= 0);
  ref = env_bind(C->bt, g, id, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(ref));

  A = pexl_choice_f(C, 		/* A -> "a" A "b" / epsilon */
		 pexl_seq_f(C, 
			 pexl_match_string(C, "a"),
			 pexl_seq_f(C, 
				 pexl_call(C, ref),
				 pexl_match_string(C, "b"))),
		 pexl_match_epsilon(C));

  TEST_ASSERT(A);		/* A will contain a TOpenCall */

  node = A->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  /* This exp is nullable, and we never get to the OPEN CALL */
  TEST_ASSERT(exp_nullable(node) == 1);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nofail(node) == 1);
  /* This exp has an OPEN CALL */
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  /* This exp benefits from needfollow, and we never get to the OPEN CALL */
  TEST_ASSERT(exp_needfollow(node) == 1);
  stat = exp_getfirst(node, fullset, &cs); 
  /* firstset cannot be used as test, exp accepts epsilon */
  TEST_ASSERT(stat == 1);

  pexl_free_Expr(A);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Grammar with one binding to unspecified");

  /* New env for grammar */
  pexl_free_Context(C);
  C = pexl_new_Context();
  g = toplevel;

  id = context_intern(C, "A", 1);
  TEST_ASSERT(id >= 0);
  ref = env_bind(C->bt, g, id, env_new_value(Eunspecified_t, 0, NULL));
  TEST_ASSERT(!pexl_Ref_invalid(refA));

  id = context_intern(C, "A", 1);
  TEST_ASSERT(id >= 0);
  S = pexl_call(C, ref);	/* S -> A */
  TEST_ASSERT(S);

  node = S->node;
  TEST_ASSERT(node);		/* precondition for next tests */

  TEST_ASSERT(!exp_hascaptures(node));
  TEST_ASSERT(!exp_itself_hascaptures(node));
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_patlen(node, &min, &max);
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nullable(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_nofail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_headfail(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  TEST_ASSERT(exp_needfollow(node) == PEXL_EXP__ERR_OPENFAIL);
  fprintf(stderr, "Expect a warning about encountering an open call:\n");
  stat = exp_getfirst(node, fullset, &cs); 
  TEST_ASSERT(stat == PEXL_EXP__ERR_OPENFAIL);
  
  pexl_free_Expr(S);

  symboltable_free(st);
  packagetable_free(m);
  context_free(C);

  TEST_END();

  return 0;
}

