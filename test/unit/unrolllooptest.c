/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  unrolllooptest.c  Testing loop unrolling functionality.                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>    /* exit() */
#include "print.h"
#include "compile.h"
#include "old-analyze.h"

#include "../test.h"

#define PRINTING NO

#define TOPLEVEL (0)		// root env has id 0

static void check_match (pexl_Match *m,
			 int expectation,
			 int leftover,
			 const char *input) {

  ssize_t expected_end = strlen(input) - leftover;
  
  TEST_ASSERT_MSG((m->end == -1) == (expectation == 0),
		  "Match error on input '%s': expected %s\n",
		  input,
		  expectation ? "a match" : "no match");

  if (m->end != -1)
    TEST_ASSERT_MSG(m->end == expected_end,
		    "Match error on input \"%s\": "
		    "expected end to be %lu, but found %lu\n",
		    input, expected_end, m->end);
}

static
pexl_Binary *compile_expression (pexl_Context *C,
				 pexl_Optims *optims,
				 pexl_Expr *exp,
				 pexl_Env env,
				 pexl_Error *err) {

  pexl_Ref ref = pexl_bind_in(C, env, exp, NULL); // NULL means anonymous
  TEST_ASSERT(!pexl_Ref_invalid(ref));
  pexl_Binary *pkg = pexl_compile(C, ref, optims, NULL, PEXL_CHAR_ASCII, err);
  return pkg;
}

static void compile_and_run (pexl_Context *C,
			     pexl_Expr *exp,
			     pexl_Optims *optims,
			     const char *inputs[],
			     int matches[],
			     int leftovers[]) {

  int i, stat;
  pexl_Error err;
  size_t inputlen;
  pexl_Binary *pkg = NULL;
  const char *input = NULL;
  pexl_Match *match = NULL;

  pkg = compile_expression(C, optims, exp, TOPLEVEL, &err);
  TEST_ASSERT(pkg && (pexl_Error_value(err) == OK));

  if (PRINTING) pexl_print_Binary(pkg); 

  match = pexl_new_Match(PEXL_TREE_ENCODER);
  TEST_ASSERT(match);

  i = 0;
  while ((input = inputs[i])) {
    printf("\t input is \"%s\"\n", input);
    inputlen = strlen(input);


//     if (strncmp(input, "aaabaaa", 8) == 0)
//       pexl_print_Binary(pkg);
    
    stat = vm_start(pkg, NULL, main_entrypoint(pkg), 
		    input, inputlen,
		    0, inputlen, 0,
		    NULL, match);
    TEST_ASSERT_MSG(stat == 0, "vm error: %d", stat);

    check_match(match,
		matches[i],
		leftovers[i],
		input);
    i++;
  } /* while */

  binary_free(pkg);
  pexl_free_Match(match);
}

// Compiles with and without loop unrolling enabled, runs vm, checks
// results against those passed in.
static void test_expression (pexl_Context *C,
			     const char *desc,
			     pexl_Expr *exp,
			     const char *inputs[],
			     int matches[],
			     int leftovers[]) {

  printf("\n ------------------------------------------------------------------\n");
  printf("| pexl_Expr: %s\n", desc);
  printf(" ------------------------------------------------------------------\n");
  fflush(NULL);

  pexl_Optims *optims = NULL;
  optims = pexl_addopt_simd(optims, 1);
  optims = pexl_addopt_tro(optims);
  optims = pexl_addopt_inline(optims);
  optims = pexl_addopt_peephole(optims);
  
  printf("Without unrolling (with simd, tro, inline, and peephole)\n");
  compile_and_run(C, exp, optims, inputs, matches, leftovers);
  pexl_free_Optims(optims);

  optims = pexl_addopt_simd(NULL, 1);
  optims = pexl_addopt_tro(optims);
  optims = pexl_addopt_inline(optims);
  optims = pexl_addopt_unroll(optims, 5);
  optims = pexl_addopt_peephole(optims);

  printf("With unrolling (simd, tro, inline, unroll, peephole)\n");
  compile_and_run(C, exp, optims, inputs, matches, leftovers);
  pexl_free_Optims(optims);

  pexl_free_Expr(exp);
  return;
}

#define IntList(...) (int[]){__VA_ARGS__}
#define StrList(...) (const char *[]){__VA_ARGS__, NULL}

#define TEST(expname, exp, inputs, exp_matches, exp_ends) do {	\
    test_expression(C, (expname), (exp),			\
		    (inputs), (exp_matches),			\
		    (exp_ends));				\
  } while (0)
  
int main(int argc, char **argv) {

  pexl_Context *C;
  const char **inputs;  

  TEST_START(argc, argv);
  
  /* Check the macros that help with signed aux fields */
  {
    int64_t i;
    for (i = - (1<<23); i < (1<<23); i++)
      if (!checksignedaux(i)) {
	TEST_FAIL("checksignedaux(%" PRId64 ") failed", i);
      }
    i = - ((1<<23) + 1);
    if (checksignedaux(i)) {
      TEST_FAIL("checksignedaux(%" PRId64 ") passed but should have failed"
		" on negative end of range",
		i);
    }
    i = (1<<23);
    if (checksignedaux(i)) {
      TEST_FAIL("checksignedaux(%" PRId64 ") passsed but should have failed"
		" on positive end of range",
		i); 
    }
  }

  C = context_new();
  TEST_ASSERT(C);
  
  /* ----------------------------------------------------------------------------- */

  inputs = StrList("", "a", "aaaaaa", "b", "ab", "aaaaaab", "aaabbb");
  
  // Basic test.  Loop will iterate from 1 to 3 positions behind
  // current input position, i.e. lengths of 'a' and 'aab'.
  TEST("'a*ba*' < ('aab'/'a')",
    pexl_seq_f(C,
      pexl_seq_f(C,
         pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
         pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
    pexl_lookbehind_f(C,
           pexl_choice_f(C,
              pexl_match_string(C, "aab"),
              pexl_match_string(C, "a")))),
       StrList("",   "aaaaaa", "aaabaaa", "b",  "aab", "abb", "aaab12345678"),
       IntList( false, false, true,   false, true, false, true ),
       IntList( 0,     0,     0,       0,    0,    0,    8 )
       );
       
  // Ensure that there can be instructions after the lookbehind loop,
  // in this case to match 'XYZ'.
  TEST("('a*ba*' < ('aab'/'a')) 'XYZ'",
    pexl_seq_f(C, 
      pexl_seq_f(C,
        pexl_seq_f(C,
           pexl_seq_f(C,
             pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
             pexl_match_string(C, "b")),
           pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
        pexl_lookbehind_f(C,
          pexl_choice_f(C,
            pexl_match_string(C, "aab"),
	    pexl_match_string(C, "a")))),
      pexl_match_string(C, "XYZ")),
      StrList("bXYZ", "bXYZ", "abXYZ", "aabXYZ", "aabaXYZ", "abb", "aaabaXYZ12345678"),
      IntList( false, false,  false,   true,     true,      false, true ),
      IntList( 0,     0,      0,       0,        0,         0,     8 )
       );
       
  // Ensure that there can two lookbehind loops in sequence, and we
  // find them both'.
  TEST("('a*ba*' < ('aab'/'a')) ('c*dc*' < ('cccc'/'cd'))'",
    pexl_seq_f(C, 
      pexl_seq_f(C,
        pexl_seq_f(C,
           pexl_seq_f(C,
             pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
             pexl_match_string(C, "b")),
           pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0)),
        pexl_lookbehind_f(C,
          pexl_choice_f(C,
            pexl_match_string(C, "aab"),
	    pexl_match_string(C, "a")))),
      pexl_seq_f(C,
        pexl_seq_f(C,
           pexl_seq_f(C,
             pexl_repeat_f(C, pexl_match_string(C, "c"), 0, 0),
             pexl_match_string(C, "d")),
           pexl_repeat_f(C, pexl_match_string(C, "c"), 0, 0)),
        pexl_lookbehind_f(C,
          pexl_choice_f(C,
            pexl_match_string(C, "cccc"),
	    pexl_match_string(C, "cd"))))),
      StrList("ab",   "aba", "abacd", "abacdd", "aabcd", "aabcdccc", "aabcdccccHH"),
      IntList( false, false,  true,   true,     true,    false,      true ),
      IntList( 0,     0,      0,      1,        0,       0,          2 )
       );
       
  // Here we craft an expression that compiles to code containing a
  // jump instruction AFTER the loop, with a target address BEFORE/AT
  // the loop.  That jump amount (jumps are relative) needs to be
  // adjusted to account for the extra code added in unrolling.
  // Basic test.  Loop will iterate from 1 to 3 positions behind
  // current input position, i.e. lengths of 'a' and 'aab'.
  TEST("('a*b' < ('aab'/'b'))*",
    pexl_repeat_f(C,
      pexl_seq_f(C,
        pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
        pexl_lookbehind_f(C,
	  pexl_choice_f(C,
	    pexl_match_string(C, "aab"),
	    pexl_match_string(C, "b")))),
       0, 0),
       StrList( "",  "b",  "bb", "abc", "xaab", "aabb", "aabaab12345678"),
       IntList(true, true, true,  true, true,  true,   true ),
       IntList( 0,    0,    0,     1,    4,      0,      8 )
       );

  /* Lookbehind where the target expression has another lookbehind in it 
     Non-looping lookbehind, so no unrolling should occur.*/
  TEST("'a'* 'b' < ('a' 'b' < 'b')",
       pexl_seq_f(C,
       pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
       pexl_seq_f(C, 
             pexl_match_string(C, "b"),
             pexl_lookbehind_f(C,
              pexl_seq_f(C,
                    pexl_match_string(C, "ab"),
                    pexl_lookbehind_f(C, pexl_match_string(C, "b")))))),
       StrList("",   "aaaaaa", "b",  "ab", "abbb", "aaab12345678"),
       IntList(false, false,   false, true, true,   true ),
       IntList(0,     5,       1,     0,    2,      8 )
       );
       
       /* Min is 2, max is 5 a{EXP}/aa{EXP} = aX | aaX if EXP = a*b*/
  TEST("'a*b' < ('12345'/'aX'/'aaX'/'aaaX'/'aaab')",
    pexl_seq_f(C,
    pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
    pexl_lookbehind_f(C,
           pexl_choice_f(C,
              pexl_match_string(C, "12345"),
              pexl_choice_f(C, 
                 pexl_match_string(C, "aX"),
                 pexl_choice_f(C, 
                    pexl_match_string(C, "aaX"),
                    pexl_choice_f(C, 
                             pexl_match_string(C, "aaaX"),
                             pexl_match_string(C, "aaab"))))))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, false, false, true, true ),
       IntList( 0,     0,     5,     1,     2,     0,    2 )
       );
       
  /* Lookbehind that tries to match beyond the position where the lookbehind was invoked */
  /* Min is 3, max is 5 */
  TEST("'a*b' < ('aaX'/'aaabb'$/'aaabb')",
    pexl_seq_f(C,
      pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
          pexl_lookbehind_f(C,
           pexl_choice_f(C,
              pexl_match_string(C, "aaX"),
              pexl_choice_f(C,
                 pexl_seq_f(C,
                       pexl_match_string(C, "aaabb"),
                       pexl_neg_lookahead_f(C, pexl_match_any(C, 1))),
                 pexl_match_string(C, "aaabb"))))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, false, false, false, false ),
       IntList( 0,     0,     5,     1,     2,     7,     3 )
       );
       
  /* Lookbehind with no maximum length */
  /* Min is 1, max is unbounded */
  TEST("'a*b' < ('a' 'b'*)",
    pexl_seq_f(C,
    pexl_seq_f(C,
          pexl_repeat_f(C, pexl_match_string(C, "a"), 0, 0),
          pexl_match_string(C, "b")),
    pexl_lookbehind_f(C,
           pexl_seq_f(C,
                 pexl_match_string(C, "a"),
                 pexl_repeat_f(C, pexl_match_string(C, "b"), 0, 0)))),
       inputs,
       //       ("",   "a", "aaaaaa", "b",  "ab", "aaaaaab", "aaabbb");
       IntList( false, false, false, false, true, true, true ),
       IntList( 0,     0,     5,     1,     0,    0,    2 )
       );

  context_free(C);
  TEST_END();
}
