#!/bin/bash
#
# Print the code coverage report, in sorted order, with summary at the
# end, after the individual files
#

coverage_report_file=$1

printf "Coverage report\n"
printf -- "--------------------------------------------------------\n"
printf "FILE                        LINES           BRANCHES\n"
printf -- "--------------------------------------------------------\n"
cat "$coverage_report_file" | sed "\$d" | sort
printf -- "--------------------------------------------------------\n"
tail -n 1 "$coverage_report_file"

