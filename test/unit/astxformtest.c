/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  astxformtest.c  Testing the AST transformation functions                 */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jack Deucher                                                    */

#include "preamble.h"

#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
#include "ast-xform.h"
#include "ast-print.h"

#include "../test.h"

#define PRINTING YES

#define newline() do { puts(""); } while(0)

int main(int argc, char **argv) {

  // TODO: Most tests are visual, tests converting from ast nodes to a
  // different order and type of ast nodes.  Need to ensure we have
  // programmatic tests that validate the output in each case.

#define MAX_BUF 100
#define setbuf(buf, s) do {                                        \
    char *last = stpncpy((buf), s, MAX_BUF - 1);                   \
    if ((last - (buf)) >= MAX_BUF)                                 \
      TEST_FAIL("MAX_BUF exceeded at line %d\n", __LINE__);        \
    else                                                           \
      *last = '\0';                                                \
  } while (0);
  
  int status;
  char buf[MAX_BUF] = {'\0'};
  //  char buf2[MAX_BUF] = {'\0'};

  ast_expression *id, *exp, *exp2, *exp3, *exp4, *exp5, *result_exp;

  TEST_START(argc, argv);

  // ------------------------------------------------------------------
  TEST_SECTION("Basic Tests");

  if (PRINTING) printf("Test with one expression\n");
  exp = make_char(0x0057);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_CHAR);
  TEST_ASSERT(exp->codepoint == 0x57);
  if (PRINTING) ast_print_tree(exp);

  exp2 = transform_AST(exp, donotmuch, NULL);
  TEST_ASSERT(exp == exp2);     // transform made no changes
  if (PRINTING) ast_print_tree(exp2);
  result_exp = compareAST(exp, exp2);
  TEST_ASSERT_NULL(result_exp); // NULL means "no differences"
  // Double-checking the result of compareAST
  TEST_ASSERT(exp2->type == AST_CHAR);
  TEST_ASSERT(exp2->codepoint == 0x57);

  if (PRINTING) {
    printf("Original exp is an expression with one char:\n");
    ast_print_tree(exp);
    printf("Making a copy\n");
  }
  exp3 = exp;                   // save the pointer for testing
  exp4 = copyAST(exp);
  TEST_ASSERT(exp4 != exp);     // copy is distinct from original

  if (PRINTING) ast_print_tree(exp);

  // Check that original tree is unchanged:
  TEST_ASSERT(exp == exp3);     // original pointer unchanged
  if (PRINTING) {
    printf("exp->type = %d %s\n", exp->type, ast_type_name(exp->type));
  }
  TEST_ASSERT(exp->type == AST_CHAR);
  TEST_ASSERT(exp->codepoint == 0x57);

  // Compare the copy to the original:
  result_exp = compareAST(exp, exp4);
  TEST_ASSERT_NULL(result_exp); // NULL means "no differences"
  // Double-checking the result of compareAST
  TEST_ASSERT(exp4->type == AST_CHAR);
  TEST_ASSERT(exp4->codepoint == 0x57);
  free_expression(exp);
  free_expression(exp4);

  if (PRINTING) {
    printf("Original exp is a sequence of 10 char A expressions:\n");
  }
  exp = NULL;
  for(int i = 0; i < 10; i++) {
    exp = build_seq(exp, make_byte(65));
    TEST_ASSERT(exp);
  }
  if (PRINTING) {
    ast_print_tree(exp);
    printf("Making a copy\n");
  }
  exp3 = exp;                   // save the original pointer
  exp4 = copyAST(exp);
  TEST_ASSERT(exp4 != exp);     // copy is distinct from original
  TEST_ASSERT(exp == exp3);     // original pointer unchanged

  if (PRINTING) ast_print_tree(exp);

  result_exp = compareAST(exp, exp4);
  if (result_exp) {
    puts("Unexpected mismatch:");
    ast_print_tree(result_exp);
    puts("\n");
  }
  TEST_ASSERT_NULL(result_exp); // NULL means "no differences"
  // Double-checking the result of compareAST
  free_expression(exp);
  free_expression(exp4);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Conversion to UTF8: Individual codepoints");
  
#define TEST_CODEPOINT_XFORM(val) do {			\
    exp = make_char(val);				\
    exp2 = transform_codepoints(exp);			\
    TEST_ASSERT_NOT_NULL(exp2);                         \
    if (PRINTING) printf("Converting %u:\n", val);	\
    if (PRINTING) {					\
      printf("Before:\n");				\
      ast_print_tree(exp); newline();			\
      printf("After:\n");				\
      ast_print_tree(exp); newline();			\
    }							\
    free_expression(exp);                               \
    free_expression(exp2);				\
  } while (0)

  // One byte sequences (all of them)
  for (int i=0; i < 0x7F; i++) TEST_CODEPOINT_XFORM(i);
  // Two byte sequences (some of them)
  for (int i=0x80; i < 0x90; i++) TEST_CODEPOINT_XFORM(i);
  // Three byte sequences (some of them)
  for (int i=0x800; i < 0x810; i++) TEST_CODEPOINT_XFORM(i);
  // Four byte sequences (some of them)
  for (int i=0x10000; i < 0x10010; i++) TEST_CODEPOINT_XFORM(i);


  // -----------------------------------------------------------------------------
  TEST_SECTION("Conversion to UTF8: Codepoint ranges");

  confess("TODO", "Programmatically check the range results");

  //------------------------CODE POINT RANGE--------------------------
  exp = make_range(0x00, 0x04);
  exp2 = transform_codepoints(exp);
  TEST_ASSERT_NOT_NULL(exp2);
  if (PRINTING) {
    printf("Before:\n");
    ast_print_tree(exp); newline();
    printf("After:\n");
    ast_print_tree(exp); newline();
  }
  free_expression(exp);
  free_expression(exp2);

  exp = make_range(0x0080, 0x0085);
  exp2 = transform_codepoints(exp);
  TEST_ASSERT_NOT_NULL(exp2);
  if (PRINTING) {
    ast_print_tree(exp2); newline();
  }
  free_expression(exp);
  free_expression(exp2);

  // -----------------------------------------------------------------------------
  TEST_SECTION("Conversion to UTF8: Codepoint sets");

  exp = make_codepointset(0);
  // Codepoint 0x26C4 is called SNOWMAN WITHOUT SNOW: ⛄
  // In UTF-8 it is a 3-byte sequence: 0xE2 0x9B 0x84 (226 155 132)
  codepointset_add(exp->codepointset, (pexl_Codepoint) 0x26C4);
  if (PRINTING) {
    printf("Before:\n");
    ast_print_tree(exp); newline();
  }
  exp2 = transform_codepoints(exp);
  TEST_ASSERT_NOT_NULL(exp2);
  if (PRINTING) {
    printf("After:\n");
    ast_print_tree(exp2); newline();
  }

  TEST_ASSERT(exp2->type == AST_SEQ);
  TEST_ASSERT(exp2->child);
  TEST_ASSERT(exp2->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child->codepoint == 226);
  TEST_ASSERT(exp2->child2);
  TEST_ASSERT(exp2->child2->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child);
  TEST_ASSERT(exp2->child2->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child->codepoint == 155);
  TEST_ASSERT(exp2->child2->child2);
  TEST_ASSERT(exp2->child2->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->codepoint == 132);
  free_expression(exp);
  free_expression(exp2);

  exp = make_codepointset(0);
  codepointset_add(exp->codepointset, (pexl_Codepoint) 9731); // ☃ -> 226 152 131
  codepointset_add(exp->codepointset, (pexl_Codepoint) 'A');  // A -> 65
  codepointset_add(exp->codepointset, (pexl_Codepoint) 225);  // á -> 195 161

  exp2 = transform_codepoints(exp);
  TEST_ASSERT_NOT_NULL(exp2);
  if (PRINTING) {
    printf("After:\n");
    ast_print_tree(exp2); newline();
  }
//     Choice 
//     ├── Choice 
//     │   ├── Byte 65
//     │   └── Sequence 
//     │       ├── Byte 195
//     │       └── Byte 161
//     └── Sequence 
// 	├── Byte 226
// 	└── Sequence 
// 	    ├── Byte 152
// 	    └── Byte 131

  // Codepoint sets are sorted from smallest codepoint value to largest
  TEST_ASSERT(exp2->type == AST_CHOICE);
  TEST_ASSERT(exp2->child);
  TEST_ASSERT(exp2->child->type == AST_CHOICE);
  // A -> 65
  TEST_ASSERT(exp2->child->child);
  TEST_ASSERT(exp2->child->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child->child->codepoint == 65);
  // á -> 195 161
  TEST_ASSERT(exp2->child->child2);
  TEST_ASSERT(exp2->child->child2->type == AST_SEQ);
  TEST_ASSERT(exp2->child->child2->child);
  TEST_ASSERT(exp2->child->child2->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child->child2->child->codepoint == 195);
  TEST_ASSERT(exp2->child->child2->child2);
  TEST_ASSERT(exp2->child->child2->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child->child2->child2->codepoint == 161);
  // ☃ -> 226 152 131
  TEST_ASSERT(exp2->child2);
  TEST_ASSERT(exp2->child2->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child);
  TEST_ASSERT(exp2->child2->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child->codepoint == 226);
  TEST_ASSERT(exp2->child2->child2);
  TEST_ASSERT(exp2->child2->child2->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child2->child);
  TEST_ASSERT(exp2->child2->child2->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->child->codepoint == 152);
  TEST_ASSERT(exp2->child2->child2->child2);
  TEST_ASSERT(exp2->child2->child2->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->child2->codepoint == 131);
  
  free_expression(exp);
  free_expression(exp2);

  //-----------------------MORE COMPLEX TREES-------------------------

  exp = make_seq(make_char(0x101),
		 make_choice(make_char(0x202),
			     make_seq(make_char(0x303),
				      make_char(0x404))));
  ast_print_tree(exp); newline();
  exp2 = transform_codepoints(exp);
  ast_print_tree(exp2); newline();

//     Sequence 
//     ├── Sequence 
//     │   ├── Byte 196
//     │   └── Byte 129
//     └── Choice 
// 	├── Sequence 
// 	│   ├── Byte 200
// 	│   └── Byte 130
// 	└── Sequence 
// 	    ├── Sequence 
// 	    │   ├── Byte 204
// 	    │   └── Byte 131
// 	    └── Sequence 
// 		├── Byte 208
// 		└── Byte 132

  TEST_ASSERT(exp2->type == AST_SEQ);
  // codepoint 0x101 -> 196 129
  TEST_ASSERT(exp2->child);
  TEST_ASSERT(exp2->child->type == AST_SEQ);
  TEST_ASSERT(exp2->child->child);
  TEST_ASSERT(exp2->child->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child->child->codepoint == 196);
  TEST_ASSERT(exp2->child->child2);
  TEST_ASSERT(exp2->child->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child->child2->codepoint == 129);
  // codepoint 0x202 -> 200 130
  TEST_ASSERT(exp2->child2);
  TEST_ASSERT(exp2->child2->type == AST_CHOICE);
  TEST_ASSERT(exp2->child2->child);
  TEST_ASSERT(exp2->child2->child->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child->child);
  TEST_ASSERT(exp2->child2->child->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child->child->codepoint == 200);
  TEST_ASSERT(exp2->child2->child->child2);
  TEST_ASSERT(exp2->child2->child->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child->child2->codepoint == 130);
  // codepoint 0x303 -> 204 131
  TEST_ASSERT(exp2->child2->child2);
  TEST_ASSERT(exp2->child2->child2->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child2->child);
  TEST_ASSERT(exp2->child2->child2->child->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child2->child->child);
  TEST_ASSERT(exp2->child2->child2->child->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->child->child->codepoint == 204);
  TEST_ASSERT(exp2->child2->child2->child->child2);
  TEST_ASSERT(exp2->child2->child2->child->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->child->child2->codepoint == 131);
  // codepoint 0x404 -> 208 132
  TEST_ASSERT(exp2->child2->child2->child2);
  TEST_ASSERT(exp2->child2->child2->child2->type == AST_SEQ);
  TEST_ASSERT(exp2->child2->child2->child2->child);
  TEST_ASSERT(exp2->child2->child2->child2->child->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->child2->child->codepoint == 208);
  TEST_ASSERT(exp2->child2->child2->child2->child2);
  TEST_ASSERT(exp2->child2->child2->child2->child2->type == AST_BYTE);
  TEST_ASSERT(exp2->child2->child2->child2->child2->codepoint == 132);

  free_expression(exp);
  free_expression(exp2);


  //-----------------------AST FIND TESTS-------------------------
  TEST_SECTION("replaceFind");

  setbuf(buf, "Hello");
  exp = make_identifier_from(buf);
  if (PRINTING) {
    ast_print_tree(exp); newline();
  }

  exp2 = make_anybyte();
  exp = make_seq(exp2, exp);
  exp = make_choice(make_byte(200), exp);
  if (PRINTING) {
    ast_print_tree(exp); newline();
  }
  id = make_identifier_from(buf);
  exp3 = NULL;
  status = map_AST(exp, replaceFind, &exp3, id);
  TEST_ASSERT(status == true);
  TEST_ASSERT_NOT_NULL(exp3);
  TEST_ASSERT(exp3->type == AST_FIND);
  if (PRINTING) {
    ast_print_tree(exp3); newline();
  }
  free_expression(exp);
  free_expression(exp3);
  free_expression(id);

  exp = make_identifier_from(buf);
  exp2 = make_anybyte();
  exp = make_seq(exp2, exp);
  exp = make_choice(make_byte(200), exp);
  setbuf(buf, "Bye");
  id = make_identifier_from(buf);
  if (PRINTING) {
    ast_print_tree(exp); newline();
  }
  exp3 = NULL;
  status = map_AST(exp, replaceFind, &exp3, id);
  TEST_ASSERT(status == false);
  TEST_ASSERT_NULL(exp3);
  TEST_ASSERT(exp->type == AST_CHOICE);
  free_expression(exp);
  free_expression(id);

  setbuf(buf, "Hello");
  exp = make_identifier_from(buf);
  exp2 = make_anybyte();
  exp = make_seq(exp2, exp);
  exp = make_choice(make_seq(make_choice(make_byte(2),
					 make_byte(1)),
			     make_byte(129)),
		    exp);
  if (PRINTING) {
    ast_print_tree(exp); newline();
  }
  setbuf(buf, "Bye");
  id = make_identifier_from(buf);
  if (PRINTING) {
    ast_print_tree(id); newline();
  }
  exp3 = NULL;
  status = map_AST(exp, replaceFind, &exp3, id);
  TEST_ASSERT(status == false);
  TEST_ASSERT_NULL(exp3);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_CHOICE);
  free_expression(exp);
  free_expression(id);

  setbuf(buf, "Hello");
  exp = make_identifier_from(buf);
  exp2 = make_anybyte();
  exp = make_seq(exp2, exp);
  exp = make_choice(make_seq(make_choice(make_byte(2),
					 make_byte(1)),
			     make_byte(129)),
		    exp);
  setbuf(buf, "Hello");
  id = make_identifier_from(buf);
  if (PRINTING) {
    ast_print_tree(exp); newline();
  }
  exp3 = NULL;
  status = map_AST(exp, replaceFind, &exp3, id);
  TEST_ASSERT(status == true);
  TEST_ASSERT_NOT_NULL(exp3);
  TEST_ASSERT(exp3->type == AST_FIND);
  if (PRINTING) {
    ast_print_tree(exp3); newline();
  }
  free_expression(exp3);
  free_expression(exp);
  free_expression(id);
  
  setbuf(buf, "Hello");
  exp = make_identifier_from(buf);
  exp2 = make_anybyte();
  exp3 = make_anybyte();
  exp4 = make_byte(200);
  exp5 = make_seq(exp2, exp);
  exp = make_seq(make_choice(exp4, exp5), exp3);
  if (PRINTING) {
    ast_print_tree(exp); newline();
  }
  id = make_identifier_from(buf);
  exp3 = NULL;
  status = map_AST(exp, replaceFind, &exp3, id);
  TEST_ASSERT(status == false);
  TEST_ASSERT_NULL(exp3);
  TEST_ASSERT_NOT_NULL(exp);
  TEST_ASSERT(exp->type == AST_SEQ);
  free_expression(exp);
  free_expression(id);
  
//   TEST_SECTION("replaceFind: qualified identifiers (with module name)");

//   setbuf(buf, "Hello");         // pattern name
//   setbuf(buf2, "");             // module name
//   exp = make_identifier(buf2, buf);
//   //ast_print_tree(exp); newline();
//   exp2 = make_anybyte();
//   exp = make_sequence(make_explist(exp2, make_explist(exp, NULL)));
//   exp = make_choice(make_explist(make_byte(200), make_explist(exp, NULL)));
//   ast_print_tree(exp); newline();
//   id = make_identifier(buf2, buf);
//   exp3 = NULL;
//   status = map_AST(exp, replaceFind, &exp3, id);
//   TEST_ASSERT(status == true);
//   TEST_ASSERT_NOT_NULL(exp3);
//   TEST_ASSERT(exp != exp3);
//   TEST_ASSERT(exp3->type == AST_FIND);
//   ast_print_tree(exp3); newline();
//   free_expression(exp);
//   free_expression(exp3);
//   free_expression(id);

//   setbuf(buf, "Hello");         // pattern name
//   setbuf(buf2, "");             // module name
//   exp = make_identifier(buf2, buf);
//   exp2 = make_anybyte();
//   exp = make_sequence(make_explist(exp2, make_explist(exp, NULL)));
//   exp = make_choice(make_explist(make_byte(200), make_explist(exp, NULL)));
//   setbuf(buf, "Bye");           // call is NOT to this pattern
//   id = make_identifier(buf2, buf);
//   ast_print_tree(exp); newline();
//   exp3 = NULL;
//   status = map_AST(exp, replaceFind, &exp3, id);
//   TEST_ASSERT(status == false);
//   TEST_ASSERT_NULL(exp3);
//   TEST_ASSERT_NOT_NULL(exp);
//   TEST_ASSERT(exp->type == AST_CHOICE);
//   free_expression(exp);
//   free_expression(id);

//   setbuf(buf, "Hello");         // pattern name
//   setbuf(buf2, "");             // module name
//   exp = make_identifier(buf2, buf);
//   exp2 = make_anybyte();
//   exp = make_sequence(make_explist(exp2, make_explist(exp, NULL)));
//   exp = make_choice(make_explist(make_byte(200), make_explist(exp, NULL)));
//   id = make_identifier(NULL, buf); // empty string not same as NULL
//   ast_print_tree(exp); newline();
//   exp3 = NULL;
//   status = map_AST(exp, replaceFind, &exp3, id);
//   TEST_ASSERT(status == false);
//   TEST_ASSERT_NULL(exp3);
//   TEST_ASSERT_NOT_NULL(exp);
//   TEST_ASSERT(exp->type == AST_CHOICE);
//   free_expression(exp);
//   free_expression(id);

//   setbuf(buf, "Hello");         // pattern name
//   setbuf(buf2, "foo");          // module name
//   exp = make_identifier(buf2, buf);
//   exp2 = make_anybyte();
//   exp = make_sequence(make_explist(exp2, make_explist(exp, NULL)));
//   exp = make_choice(make_explist(make_byte(200), make_explist(exp, NULL)));
//   setbuf(buf2, "bar");          // module name
//   id = make_identifier(buf2, buf);
//   ast_print_tree(exp); newline();
//   exp3 = NULL;
//   status = map_AST(exp, replaceFind, &exp3, id);
//   TEST_ASSERT(status == false);
//   TEST_ASSERT_NULL(exp3);
//   TEST_ASSERT_NOT_NULL(exp);
//   TEST_ASSERT(exp->type == AST_CHOICE); // same pattern, different module names
//   free_expression(exp);
//   free_expression(id);

//   setbuf(buf, "Hello");         // pattern name
//   setbuf(buf2, "foo");          // module name
//   exp = make_identifier(buf2, buf);
//   exp2 = make_anybyte();
//   exp = make_sequence(make_explist(exp2, make_explist(exp, NULL)));
//   exp = make_choice(make_explist(make_byte(200), make_explist(exp, NULL)));
//   setbuf(buf, "Bye");          // pattern name
//   id = make_identifier(buf2, buf);
//   ast_print_tree(exp); newline();
//   exp3 = NULL;
//   status = map_AST(exp, replaceFind, &exp3, id);
//   TEST_ASSERT(status == false);
//   TEST_ASSERT_NULL(exp3);
//   TEST_ASSERT_NOT_NULL(exp);
//   TEST_ASSERT(exp->type == AST_CHOICE); // same module, different pattern names
//   free_expression(exp);
//   free_expression(id);

//   setbuf(buf, "Hello");
//   setbuf(buf2, "foo");          // module name
//   exp = make_identifier(buf2, buf);
//   exp2 = make_anybyte();
//   exp = make_sequence(make_explist(exp2, make_explist(exp, NULL)));
//   exp = make_choice(make_explist(make_sequence(make_explist(make_choice(make_explist(make_byte(2),
// 										     make_explist(make_byte(1), NULL))),
// 							    make_explist(make_byte(129), NULL))),
// 				 make_explist(exp, NULL)));
//   id = make_identifier(buf2, buf);
//   ast_print_tree(exp); newline();
//   exp3 = NULL;
//   status = map_AST(exp, replaceFind, &exp3, id);
//   TEST_ASSERT(status == true);
//   TEST_ASSERT_NOT_NULL(exp3);
//   TEST_ASSERT_NOT_NULL(exp);
//   TEST_ASSERT(exp3->type == AST_FIND); // same module AND pattern names
//   ast_print_tree(exp3); newline();
//   free_expression(exp3);
//   free_expression(exp);
//   free_expression(id);
  
  TEST_END();
}

