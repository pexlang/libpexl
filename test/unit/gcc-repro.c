//  -*- Mode: C; -*-                                                       
// 
//  gcc-repro.c
// 
//  AUTHORS: Jamie A. Jennings

#include <inttypes.h>
#include <stdio.h>

//
// Does a 256-bit vec have a 1 or 0 stored at position c, where c is const char?
//

// Original: no cast of the 'c' parameter (fails on x86, works on ARM)
#define testchar1(set, c) ((set)[(((c)) >> 5)] & (((uint32_t) 1) << ((c) & 31)))

// Fixed: cast of the 'c' parameter (succeeds on x86 and on ARM)
#define testchar2(set, c) ((set)[(((uint8_t)(c)) >> 5)] & (((uint32_t) 1) << ((uint8_t)(c) & 31)))

int main(void) {
  
  uint32_t set[8] = {0};

  char c = -1;
  int t1 = testchar1(set, c);
  int t2 = testchar2(set, c);

    

  printf("On Fedora 38, x86, with gcc 13.2.1, program fails at marked line above.\n"
	 "On macOS 14.4.1 (23E224), M1 ARM, with gcc version 14.1.0, it succeeds.\n"
	 "And this message is printed.\n"
// 	 "t1a = %d, t1b = %d, t2a = %d, t2b = %d\n", t1a, t1b, t2a, t2b);
 	 "t1 = %d, t2 = %d\n", t1, t2);
  return 0;
}
