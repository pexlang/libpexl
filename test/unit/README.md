# Unit tests

## Running the tests

`make clean && make build && make test`

## Interpreting the output

The tests run in a sequence that is, roughly speaking, from low-level to
high-level.  Each test program prints a banner that looks like

```
$$$ START <name>
```

...where `<name>` is the program name.  It should be the case that none of the
output needs to be read, unless something goes wrong.  If any test fails, it
will throw an assertion and exit the test program with a non-zero exit status.

The Makefile runs the tests in order, and `make` will exit with a non-zero
status as soon as any test program fails.

The start of each major section of tests is announced, as well as the end of
each test program.  Look for `$$$ SECTION <description>` and `$$$ END <name>`
in the output.

## Do not believe any performance results

Some of the unit test files implement simple benchmarks with optimizers enabled
and disabled, so that we can check for consistency in results.  Occasionally,
these programs measure an operation repeatedly and print a "performance ratio"
indicating how much impact an optimizer had.  But the unit test programs are
linked with specially-built object files from libpexl, ones with compiled with C
language optimizations disabled, and many debugging features enabled.

The same program, when linked to the production library `libpexl.a` or
`libpexl.so` from the `src` directory, should yield useful benchmark numbers.

See the `test/bench` directory for some early, rudimentary benchmark programs,
and note that a full benchmark suite is in development separately from this
repository. 


