/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ktest.c                                                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/*
  This is more like a demonstration than a unit test, but it does test
  the functions that support using "defunctionalized continuations".
*/

#include "preamble.h"

#include <stdlib.h>
#include <stdbool.h>
#include <time.h>		// For a PRNG seed
#include "../test.h"

#include "k.h"

#define PRINTING false
#define FUZZING false

typedef struct binarytree {
  int value;
  struct binarytree *left;
  struct binarytree *right;
} binarytree;

static binarytree *make_binarytree(int val, binarytree *l, binarytree *r) {
  binarytree *new = malloc(sizeof(binarytree));
  if (!new) TEST_FAIL("out of memory");
  new->value = val;
  new->left = l;
  new->right = r;
  return new;
}

// -----------------------------------------------------------------------------
// Recursively add up the number of nodes in a binarytree
// -----------------------------------------------------------------------------

static size_t size(binarytree *tree) {
  if (!tree) return 0;
  return size(tree->left) + size(tree->right) + 1;
}

/*
  (1) Convert 'size' function to CPS, still written in C but assuming
  we can write 'make_k' to take a variable number of arguments:

    static size_t size_k(binarytree *tree, continuation k) {
      if (!tree) k(0);
      else size_k(tree->left, 
	          make_k(K_RIGHT, 
		         tree->right, 
                         make_k(K_ADD1, k)));
    }

  Any article on CPS will explain how to produce the code above, which
  linearizes the computation, putting the recursive call to 'size_t'
  into tail position.

  Recall that a continuation is a function of one argument, so
  'make_k' is meant to return a function.  The argument is the result
  of a prior step in the computation.  Here, that argument has type
  size_t.

  Notice how each continuation made with 'make_k' includes a
  continuation of its own (as an argument).

  I have tagged each continuation with its type:

    K_RIGHT: Calculate size(tree->right), add it to incoming argument.
             Incoming arg when applying K_RIGHT is size(tree->left).

    K_ADD1: Add 1 to incoming argument.
            Incoming arg is size(tree->left) + size(tree->right).

  In a language with higher order functions, 'make_k' can return a new
  function.  C lacks this capability, so our continuations will be
  data structures instead.

  Our 'make_k' already looks like a typical C constructor, and
  'continuation' could be a struct type.  


  (2) To defunctionalize the continuation, we turn it into a data
  structure.  We will need a function, which we'll call 'apply_k',
  that applies its continuation argument to its value argument:

    size_t apply_k(continuation k, size_t val);

  The function 'apply_k' performs the computation that would have been
  done had the continuation been a function all along.

  Now our CPS function 'size_k' must call 'apply_k':

    static size_t size_k(binarytree *tree, continuation k) {
      if (!tree) apply_k(k, 0);
      else size_k(tree->left, 
	          make_k(K_RIGHT, 
                         tree->right, 
                         make_k(K_ADD1, k)));
    }

  (3) Because a C compiler may or may not eliminate tail recursion,
  the recursive call to 'size_k' can cause the C stack to grow.  We
  can prevent this by introducing an explicit loop:

    static size_t size_k(binarytree *tree, continuation k) {
      size_t value = 0;
      while (tree) {
	k = make_k(K_RIGHT, tree->right, make_k(K_ADD1, k));
	tree = tree->left;
	value++;
      }
      apply_k(k, 0);
    }


  (4) The action of 'apply_k' depends on its continuation argument.

  If k is K_RIGHT, the value argument passed to 'apply_k' is
  size(tree->left).  The next step is to calculate the size of the
  subtree stored in the K_RIGHT structure, and add it to the size of
  the left subtree.

  The K_ADD1 continuation incremements its argument and passes that
  result to k->k, the next continuation:

    static size_t apply_k(continuation k, size_t value) {
      if (!k) return value;
      switch (k->type) {
        case K_RIGHT: return size_k(k->tree, k->k);
        case K_ADD1: return apply_k(k->k, value + 1);
	default: abort();
      }
    }


  (5) Unfortunately, 'size_k' and 'apply_k' are mutually recursive.
  As they execute, the C stack will grow in proportion to the height
  of the binary tree, which is exactly what we want to avoid.

  We can fix this by modifying 'size_k' to remove the call to
  'apply_k'.  We'll make 'size_k' return a continuation instead of an
  answer.  Then we'll arrange to call 'size_k' from 'apply_k'.

    static continuation size_k(binarytree *tree, continuation k) {
      size_t value = 0;
      while (tree) {
	k = make_k(K_RIGHT, tree->right, make_k(K_ADD1, k));
	tree = tree->left;
	value++;
      }
      return make_k(K_ANSWER, 0, k);
    }

  In order for 'size_k' to return a continuation, we had to introduce
  a new type, K_ANSWER, which exists to convey the answer to the base
  case, 0, along with the continuation to apply to that value.

  Adjusting 'apply_k' accordingly, we get:

    static size_t apply_k(continuation k, size_t value) {
      if (!k) return value;
      switch (k->type) {
        case K_ANSWER:
	  return apply_k(k->k, value);
        case K_RIGHT:
	  k = size_k(k->tree, make_k(K_ADD1, k->k)); break;
        case K_ADD1:
	  return apply_k(k->k, value + 1);
	default: abort();
      }
    }
    

  (5) All of the calls to 'apply_k' are in tail position, so we can
  convert the entire prodedure into a loop:

    static size_t apply_k(continuation k, size_t value) {
      binarytree *tree, *rtree;
      size_t value;
      while (k) {
	switch (k->type) 
	  case K_ANSWER: 
	    value = k->value; k = k->k; break;
	  case K_RIGHT:
	    k = size_k(k->tree, make_k(K_ADD1, k->k)); break;
	  case K_ADD1:
	    value += 1 + k->value; k = k->k; break;
	  default:
	    abort();
	}
      }
      return value;
    }

  This version of 'apply_k' is a trampoline.  Each call to 'size_k' is
  a single bounce.  There are no recursive calls to 'size_k' anywhere,
  nor any mutually recursive calls.  The loop in 'apply_k' keeps the
  bouncing into 'size_k' until there is no more work to be done,
  i.e. we have run out of continuations.

  To compute 'size(tree)', call 'apply_k(size_k(tree, NULL), 0)',
  because we have chosen NULL to represent the lack of a subsequent
  continuation.
  
  This code uses a small fixed amount of the C stack to compute
  the size of a tree of arbitrary height.

  Recall that the C stack has limited size, and a program will
  segfault if it exceeds that limit.  This happens if we call
  'size(tree)' with a deep enough tree.  But 'size_k(tree)' can handle
  a tree of arbitrary height, limited only by the heap memory needed
  to allocate the linked list of continuations.

  Furthermore, if we do run out of memory, malloc() will, at least in
  theory, return NULL and our program can report the error and exit
  gracefully.


  (6) While we have achieved a defunctionalized CPS version of
  'size()', we will make one more change, one that is partly an
  optimization and partly ergonomic.

  Recall that the CPS transformation effectively replaces the C call
  stack with a linked list of continuations.  

  Unsurprisingly, perhaps, the continuations created via a CPS
  transformation follow a stack discipline.  In a language like C,
  with manual memory management, it is much easier to work with a
  stack than a linked list.

  We'll create a continuation stack, and making a new continuation
  will become pushing a frame.  Applying a continuation will become
  popping a frame.  The top frame will always be the current
  continuation, so that we don't have to pass it as a separate
  argument.

  The "next continuation" field from our earlier linked-list
  continuation model is no longer needed.  The stack is the list, and
  the next continuation is always right under the current one.

    static void size_k(binarytree *tree, Kstack *ks) {
      size_t value = 0;
      while (tree) {
	kpush(ks, K_RIGHT, (kdata){tree->right, value});
	tree = tree->left; 
	value++;
      }
      kpush(ks, K_ANSWER, (kdata){NULL, 0});
    }

  Note: We could have made the continuation stack a global variable,
  as is done in many tutorials, but this is (an unoptimized version
  of) working code.  Therefore, we want to (1) avoid globals and (2)
  support multiple continuation stacks.

  Below, the function 'defcon_size' is the defunctionalized CPS
  replacement for the 'size()' function on binary trees.  It combines
  the entrypoint and the loop of 'apply_k' into a single function.  It
  works with 'size_k', a defunctionalized CPS version of the original
  recursive 'size()' function.

  Note: There is a practical reason to inline the loop of 'apply_k'
  into the entrypoint function 'defcon_size'.  When we "defcon"
  another function, say 'height()', we'll need a different version of
  'apply_k' -- one that calculates tree height.  By combining the
  entrypoint and the loop, we avoid having multiple unnecessary
  versions of 'apply_k'.

*/

// Declare type of continuation data, name for a type-specific push
// function, and names for a data getter & setter
typedef struct kdata {
  binarytree *tree;
  size_t value;
} kdata;

KDATATYPE(kdata, newstack, push, data, setdata);

// Continuation types
KTYPES(K_ANSWER,	      // Have an answer (base case)
       K_RIGHT,		      // Working on right subtree
       K_ADD1,		      // Final calculation for size()
       K_MAX1,		      // Final calculation for height()
       K_FREE);		      // Used in defcon_free_binarytree()

// For debugging
__attribute__((unused))
static void print_kdata(void *ptr) {
  kdata data = *(kdata *)ptr;
  printf("tree = %p; value = %zu", data.tree, data.value);
}

// -----------------------------------------------------------------------------
// Recursively count the nodes in a binarytree
// -----------------------------------------------------------------------------

// Note: 'size_k' and 'defcon_size' are a direct result of the defcon
// conversion process.  They could be made more efficient.  For
// example, we could exploit the fact that the value field in K_ANSWER
// is always zero.

static void size_k(binarytree *tree, Kstack *ks) {
  size_t value = 0;
  while (tree) {
    push(ks, K_RIGHT, (kdata){tree->right, value});
    tree = tree->left; 
    value++;
  }
  push(ks, K_ANSWER, (kdata){NULL, 0});
}

static size_t defcon_size(binarytree *tree) {
  Kontinuation *k;
  size_t value = 0;
  Kstack *ks = newstack(NULL);
  size_k(tree, ks);		// Get things started
  while ((k = K_top(ks))) {
    switch (k->type) {
      case K_ANSWER: {
	value = data(k).value;
	K_pop(ks);
	break;
      }
      case K_RIGHT: {
	tree = data(k).tree; 
	k = K_pop(ks);
	push(ks, K_ADD1, (kdata){tree, value});
	size_k(tree, ks);
	break;
      }
      case K_ADD1: {
	value += 1 + data(k).value;
	K_pop(ks);
	break;
      }
      default: {
	confess("defcon_size", "unknown continuation type: %d", k->type);
	abort();
      }
    }
  }
  Kstack_free(ks);
  return value;
}

// -----------------------------------------------------------------------------
// Recursively free the nodes of a binarytree
// -----------------------------------------------------------------------------

static void free_binarytree(binarytree *tree) {
  if (tree) {
    free_binarytree(tree->left);
    free_binarytree(tree->right);
    free(tree);
  }
}

static void defcon_free_k(binarytree *tree, Kstack *ks) {
  if (!tree) return;
  if (tree->left) push(ks, K_FREE, (kdata){tree->left, 0});
  if (tree->right) push(ks, K_FREE, (kdata){tree->right, 0});
  free(tree);
}

// The 'noisy' parameter exists so we can test the continuation stack
// print function
static void defcon_free_binarytree_AUX(binarytree *tree, bool noisy) {
  if (!tree) return;
  Kstack *ks = newstack(NULL);
  Kontinuation *k;
  defcon_free_k(tree, ks);
  while ((k = K_top(ks))) {
    assert(k->type == K_FREE);
    if (noisy) Kstack_print(ks, NULL);
    tree = data(k).tree;
    K_pop(ks);
    defcon_free_k(tree, ks);
  }
  Kstack_free(ks);
}

static void defcon_free_binarytree(binarytree *tree) {
  defcon_free_binarytree_AUX(tree, false);
}

// -----------------------------------------------------------------------------
// Recursively compute the height of a binarytree
//
// We are defining height as the highest number of nodes along any
// path from a leaf to the root, which is one more than the count of edges.
// -----------------------------------------------------------------------------

static size_t height(binarytree *tree) {
  if (!tree) return 0;
  size_t left = height(tree->left);
  size_t right = height(tree->right);
  return ((left > right) ? left : right) + 1;
}

static void height_k(binarytree *tree, Kstack *ks) {
  size_t value = 0;
  while (tree) {
    push(ks, K_RIGHT, (kdata){tree->right, value});
    tree = tree->left;
    value++;
  }
  push(ks, K_ANSWER, (kdata){NULL, 0});
}

static size_t defcon_height(binarytree *tree) {
  Kontinuation *k;
  size_t value = 0;
  Kstack *ks = newstack(NULL);
  // Get things started:
  height_k(tree, ks);
  while ((k = K_top(ks))) {
    switch (k->type) {
      case K_ANSWER: {
	value = data(k).value;
	K_pop(ks);
	break;
      }
      case K_RIGHT: {
	tree = data(k).tree;
	k = K_pop(ks);
	push(ks, K_MAX1, (kdata){NULL, value});
	height_k(tree, ks);
	break;
      }
      case K_MAX1: {
	value = 1 + ((value > data(k).value) ? value : data(k).value);
	K_pop(ks);
	break;
      }
      default:
	abort();
    }
  }
  Kstack_free(ks);
  return value;
}

// -----------------------------------------------------------------------------
// Recursively print a binarytree
// -----------------------------------------------------------------------------

/*
  A tree up to MAX_PRINT_DEPTH will print correctly, i.e. with the
  indended visual style.  Deeper trees will print, but the deepest
  nodes will not be indented correctly.
*/
#define MAX_PRINT_DEPTH 16

static void indent (int depth, uint8_t *parents) {
  int start = 1;
  if (depth > MAX_PRINT_DEPTH) {
    depth = MAX_PRINT_DEPTH;
    printf("TOO DEEP ==>  ");
    start = 3;
  }
  for (int i=start; (i < depth); i++) {
    printf("   %s", (parents[i] ? "│   " : "    "));
  }
}

static void really_print_tree (binarytree *tree,
			       bool has_sibling,
			       int depth,
			       uint8_t *parents) {
  if (depth < MAX_PRINT_DEPTH) parents[depth] = has_sibling;
  indent(depth, parents);
  if (depth > 0) printf("   %s", (has_sibling ? "├── " : "└── "));
  if (!tree) { printf("NULL\n"); return; }
  printf("%4d\n", tree->value);
  really_print_tree(tree->left, true, depth+1, parents);
  really_print_tree(tree->right, false, depth+1, parents);
}

static void print_tree (binarytree *tree) {
  printf("Tree:\n");
  if (!tree) {
    printf("NULL tree\n");
    return;
  }
  uint8_t *parents = malloc(MAX_PRINT_DEPTH);
  if (!parents) {
    warn("print", "out of memory");
    return;
  }
  memset(parents, 0, MAX_PRINT_DEPTH);
  really_print_tree(tree, true, 0, parents);
  free(parents);
}

// In range 0..max-1
static uint32_t random_in(uint32_t max) {
  return random() % max;
}

#define N 1000

// Must do this iteratively so that we can use it to build large trees
// for testing the defunctionalized continuation approach.  Otherwise,
// we'd build this twice and have to ensure they do the same thing,
// which is fraught with a nondeterministic algorithm.
static binarytree *build_random_tree(int n) {
  if (n <= 0) return NULL;
  binarytree *root = make_binarytree(random_in(N), NULL, NULL);
  binarytree *node = root;
  while (--n > 0) {
    int where = random_in(4);
    // 0 = 00 left only
    // 1 = 01 right only
    // 2 = 10 both, extend on left next
    // 3 = 11 both, extend on right next
    if ((n == 1) && (where & 2)) where -= 2;
    if (where != 1)
      node->left = make_binarytree(random_in(N), NULL, NULL);
    if (where != 0)
      node->right = make_binarytree(random_in(N), NULL, NULL);
    if (where & 2) n--;
    node = (where & 1) ? node->right : node->left;
  }
  return root;
}

// Compute the height of a full binary tree of size n, or the next
// full tree size larger than n.  Cheap hack to avoid needing log2()
// and floating point.
static size_t full_tree_ht(size_t n) {
  if (n < 2) return 1;
  size_t i = 0;
  while ((n = n >> 1)) i++;
  return i + 1;
}

int main(int argc, char **argv) {

  printf("Usage: %s [allow_segfault]\n"
	 "  where the optional argument is any value.  When present, it\n"
	 "  allows attempted recursive procedure calls on large trees,\n"
	 "  which we fully expect will produce a segfault.\n\n",
	 argv[0]);

  TEST_START(argc, argv);
  srandom(time(NULL));

  int allow_segfault = (argc > 1);
  if (allow_segfault)
    printf("Direct-style recursive tree functions expected to segfault.\n");
  else
    printf("Running only defcon tree functions, which should NOT segfault.\n");
    

#define CHECK_SIZE(tree, expected) do {				\
    if (allow_segfault) {					\
      sz = size(tree);						\
      printf("Direct-style tree size is %zu nodes\n", sz);	\
      TEST_ASSERT(sz == (expected));				\
    }								\
    sz_d = defcon_size(tree);				 \
    printf("Defcon tree size = %zu nodes\n", sz_d);	 \
    TEST_ASSERT(sz_d == (expected));			 \
  } while(0)

#define CHECK_HEIGHT(tree, min, max) do {		 \
    if (allow_segfault) {				 \
      ht = height(tree);				 \
      printf("Direct-style tree height is %zu\n", ht);	 \
      TEST_ASSERT(ht <= (max));				 \
      /* Silence compiler warning by adding one */	 \
      TEST_ASSERT((ht + 1) >= ((min) + 1));		 \
    }							 \
    ht_d = defcon_height(tree);				 \
    printf("Defcon tree height is %zu\n", ht_d);	 \
    if (allow_segfault) {				 \
      TEST_ASSERT(ht_d == ht);				 \
    } else {						 \
	TEST_ASSERT(ht_d <= (max));			 \
      /* Silence compiler warning by adding one */	 \
	TEST_ASSERT((ht_d + 1) >= ((min) + 1));		 \
    }							 \
  } while(0)

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Building small trees");
  /* ----------------------------------------------------------------------------- */

  size_t sz, ht, sz_d, ht_d;

  binarytree *tree = NULL;
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 0);
  CHECK_HEIGHT(tree, 0, 0);
  free_binarytree(tree);

  // One-node tree
  tree = make_binarytree(44, NULL, NULL);
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 1);
  CHECK_HEIGHT(tree, 1, 1);

  // Extend into a 2-node tree on the right
  tree->right = make_binarytree(55, NULL, NULL);
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 2);
  CHECK_HEIGHT(tree, 2, 2);

  // Extend into a 3-node full tree
  tree->left = make_binarytree(33, NULL, NULL);
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 3);
  CHECK_HEIGHT(tree, 2, 2);
  free_binarytree(tree);  

  // Build a small random tree
  tree = build_random_tree(10);
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 10);
  CHECK_HEIGHT(tree, full_tree_ht(10), 10);
  defcon_free_binarytree_AUX(tree, true);

  // Build a slightly larger random tree that is printable
  tree = build_random_tree(16);
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 16);
  CHECK_HEIGHT(tree, full_tree_ht(16), 16);
  defcon_free_binarytree(tree);

  // Build another somewhat larger random tree, still printable
  tree = build_random_tree(30);
  if (PRINTING) print_tree(tree);
  CHECK_SIZE(tree, 30);
  CHECK_HEIGHT(tree, full_tree_ht(30), 30);
  defcon_free_binarytree(tree);
  
  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Building large trees");
  /* ----------------------------------------------------------------------------- */

#define RANDOM_TREE_TEST(n) do {		\
    tree = build_random_tree(n);		\
    CHECK_SIZE(tree, (n));			\
    CHECK_HEIGHT(tree, full_tree_ht(n), (n));	\
    defcon_free_binarytree(tree);		\
  } while (0)

  RANDOM_TREE_TEST(100 * 1000);
  RANDOM_TREE_TEST(200 * 1000);

  /* ----------------------------------------------------------------------------- */
  TEST_SECTION("Building trees so large that recursive traversals will segfault");
  /* ----------------------------------------------------------------------------- */

  // This size reliably segfaults on macOS with clang, but not gcc-13
  RANDOM_TREE_TEST(250 * 1000);

  // This size reliably segfaults on macOS with clang and gcc-13
  RANDOM_TREE_TEST(400 * 1000);

  if (FUZZING) {

    // At the sizes below, we cannot use free_binarytree() because it
    // will segfault.  Consequently, all tests have been converted to
    // use defcon_free_binarytree().

    RANDOM_TREE_TEST(800 * 1000);

    RANDOM_TREE_TEST(1000 * 1000);
    RANDOM_TREE_TEST(10 * 1000 * 1000);
  }

  TEST_END();
}
