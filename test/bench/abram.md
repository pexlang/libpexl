| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `testrun local /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl abram 1000 /Users/jennings/Projects/libpexl/src` | 670.8 ± 8.6 | 664.1 | 688.2 | 1.00 |
| `testrun system /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl abram 1000 /Users/jennings/Projects/libpexl/src` | 671.3 ± 6.1 | 664.5 | 687.2 | 1.00 ± 0.02 |

Pattern: abram

Repetitions: 1000

Date: Sat Oct 16 11:19:26 EDT 2021

System: Darwin Jamies-Compabler.local 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64 x86_64

Commands:
* testrun local /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl abram 1000 /Users/jennings/Projects/libpexl/src
* testrun system /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl abram 1000 /Users/jennings/Projects/libpexl/src
