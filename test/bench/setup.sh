#!/bin/bash

# Bash functions are so broken.
testrun() {
    local program="$2"
    local pat="$3"
    local reps="$4"
    local lib="$5"
    if [[ "$1" == "local" ]]; then
	LD_LIBRARY_PATH="$5" $program run capture $pat $reps
    elif [[ "$1" == "system" ]]; then
	$program run capture $pat $reps
    else
	echo "ERROR: bad parameter ($1) to testrun"
	exit -1
    fi
}

export -f testrun
