/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  findbench.c  Benchmarking code in find.c                                 */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include "find.h"
#include "time.h"

#define YES 1
#define NO 0
#define TRUE 1
#define FALSE 0

/* Caller must free the return value */
static const char *readfile (const char *filename, size_t *filesize) {
  FILE *f = fopen(filename, "r");
  if (!f) return NULL;
  int stat = fseek(f, 0, SEEK_END);
  if (stat) {
    fprintf(stderr, "Failed fseek\n");
    fclose(f);
    return NULL;
  }
  long filelen = ftell(f);
  if (filelen < 0) {
    fprintf(stderr, "Invalid file size %ld\n", filelen);
    fclose(f);
    return NULL;
  }
  *filesize = (size_t) filelen;
  stat = fseek(f, 0, 0);
  char *contents = (char *) malloc(*filesize+1); 
  if (!contents) {
    fprintf(stderr, "malloc failed\n");
    return NULL;
  }
  size_t n = fread(contents, (size_t) *filesize, 1, f);
  if (n != 1) {			/* one item */
    fprintf(stderr, "fread failed\n");
    free(contents);
    contents = NULL;
  }
  contents[*filesize] = '\0';
  fclose(f);
  return contents;
}

static void print_charset(const uint32_t *st)
{
  int i, first;
  printf("[");
  for (i = 0; i <= UCHAR_MAX; i++)
  {
    first = i;
    while (i <= UCHAR_MAX && testchar(st, i))
      i++;
    if (i - 1 == first) /* unary range? */
      printf("(%02x)", first);
    else if (i - 1 > first) /* non-empty range? */
      printf("(%02x-%02x)", first, i - 1);
  }
  printf("]");
}

static long double
best_of_3 (long double d1, long double d2, long double d3) {
  long double best = d1;
  if (best > d2) {
    if (d2 > d3) {
      best = d3;
    } else {
      best = d2;
    }
  } else {
    if (best > d3) {
      if (d3 > d2) {
	best = d2;
      }	else {
	best = d3;
      }
    }
  }
#if 0
  printf("   (1) %0.3Lf\n", d1);
  printf("   (2) %0.3Lf\n", d1);
  printf("   (3) %0.3Lf\n", d1);
  printf("(best) %0.3Lf\n", d1);
#endif
  return best;
}

static int
prep_simd_charset (Charset *cs, Charset *simd_cs, uint8_t *simd_char1) {
  SetMask_t simd_set;
  int simd_size = charset_to_simd_mask(cs, &simd_set, simd_char1); 
  if (simd_size < 0)
    fprintf(stderr, "error %d from charset_to_simd_mask\n", simd_size);
  else if (simd_size == 0)
    fprintf(stderr, "error: empty charset");
  memcpy(&(simd_cs->cs), &simd_set, 32);
  return simd_size;
}

static long double
AB_duration_regular (const char *input, size_t len,
		     CharSetType cs_type, Charset *cs, const char char1,
		     int reps, int *return_count) {
  int count;
  const char *ptr;
  clock_t t0 = clock();
  for (int i = 0; i < reps; i++) {
    ptr = input;
    count = 0;
    switch (cs_type) {
    case CharSet1:
      while (1) {
	ptr = find_char(char1, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++;
      }
      break;
    case CharSet8: case CharSet:
      while (1) {
	ptr = find_charset(cs, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++;
      }
      break;
    default:
      assert(0);
    } /* switch */
  } /* for reps */
  clock_t t1 = clock();
  *return_count = count;
  return (long double) (t1 - t0) / CLOCKS_PER_SEC * 1000;
}

static long double
AB_duration_simd (const char *input, size_t len,
		  Charset *simd_cs, const char simd_char1, Charset *cs,
		  int reps, int *return_count) {
  int count = 0;
  const char *ptr;
  clock_t t0 = clock();
  char dummy;
  CharSetType cs_type = charsettype(cs->cs, &dummy);
  for (int i = 0; i < reps; i++) {
    ptr = input;
    count = 0;
    switch (cs_type) {
    case CharSet1:
      while (1) { 
	ptr = find_simd1(simd_char1, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++; 
      }
      break;
    case CharSet8:
      while (1) {
	ptr = find_simd8(simd_cs, cs, ptr, input + len);
	if (ptr == input+len) break;
	count++;
	ptr++; 
      }
      break;
    case CharSet:
      while (1) {
	ptr = find_simd(simd_cs, cs, ptr, input + len); 
	if (ptr == input+len) break;
	count++;
	ptr++; 
      }
      break;
    default:
      assert(0);
    } /* switch */
  } /* for reps */
  clock_t t1 = clock();
  *return_count = count;
  return (long double) (t1 - t0) / CLOCKS_PER_SEC * 1000;
}

static void print_testname (const char *testname, const char *type, Charset *cs) {
  printf("\"%s\", \"%s\", ", testname, type);
  printf("\""); print_charset(cs->cs); printf("\", ");
}
/*
  Compare searching the input using find_char() or find_charset(),
  which are the NON SIMD ROUTINES, versus find_simd().
*/
static void AB_test_find (const char *testname,
			  const char *inputname,
			  const char *input, size_t len,
			  Charset *cs,
			  int reps) {
  if (!input) {
    if (inputname) fprintf(stderr, "ERROR: '%s' not found ... skipping\n", inputname);
    return;
  }
  print_testname(testname, "regular", cs);
  char char1;
  CharSetType cs_type = charsettype(cs->cs, &char1);

  int count1, count2, count3;
  long double d1 = AB_duration_regular(input, len, cs_type, cs, char1, reps, &count1);
  long double d2 = AB_duration_regular(input, len, cs_type, cs, char1, reps, &count2);
  long double d3 = AB_duration_regular(input, len, cs_type, cs, char1, reps, &count3);

  if ((count1 != count2) || (count1 != count3) || (count2 != count3)) {
    fprintf(stderr, "ERROR: inconsistent counts %d, %d, %d\n", count1, count2, count3);
    assert(0);
  }
  int regular_count = count1;

  long double best = best_of_3(d1, d2, d3);

  /* Inputfile, size in bytes */
  printf("\"%s\", %zu, ", inputname, len);
  /* Reps, best time total, best time per repetition */
  printf("%d, %0.1Lf, %0.1Lf, ", reps, best, best / (long double) reps);
  /* Match count, One match per N bytes on average */
  printf("%d, %0.1Lf\n", regular_count,
	 ((long double) len) / ((long double) regular_count));

  long double regular_best = best;
  
  if (pexl_simd_available()) {
    simd_cache_initialize();
    print_testname(testname, "simd", cs);
    Charset simd_cs;
    uint8_t simd_char1 = '\0';
    prep_simd_charset(cs, &simd_cs, &simd_char1);
    d1 = AB_duration_simd(input, len, &simd_cs, simd_char1, cs, reps, &count1);
    d2 = AB_duration_simd(input, len, &simd_cs, simd_char1, cs, reps, &count2);
    d3 = AB_duration_simd(input, len, &simd_cs, simd_char1, cs, reps, &count3);
    simd_cache_print_stats();

    if ((count1 != count2) || (count1 != count3) || (count2 != count3)) {
      fprintf(stderr, "ERROR: inconsistent simd counts %d, %d, %d\n", count1, count2, count3);
      assert(0);
    }
    int simd_count = count1;
    if (simd_count != regular_count) {
      fprintf(stderr, "ERROR: inconsistent regular/simd counts: %d vs %d\n", regular_count, simd_count);
      assert(0);
    }

    best = best_of_3(d1, d2, d3);

    /* Inputfile, size in bytes */
    printf("\"%s\", %zu, ", inputname, len);
    /* Reps, best time total, best time per repetition */
    printf("%d, %0.1Lf, %0.1Lf, ", reps, best, best / (long double) reps);
    /* Match count, One match per N bytes on average */
    printf("%d, %0.1Lf\n", simd_count,
	   ((long double) len) / ((long double) simd_count));

    fprintf(stderr, "** SIMD SPEEDUP: %0.1Lf / %0.1Lf = %0.2Lfx\n\n",
	    regular_best,
	    best,
	    regular_best / best);
  }
  return;
}

int main(int argc, char **argv) {

  int reps = 50;
  Charset cs;
  
  /* Setup */

  const char *wordsfile = "/usr/share/dict/words";
  size_t wordslen;
  const char *words = readfile(wordsfile, &wordslen);
  char *inputfile = NULL;
  size_t inputlen = 0;
  const char *input = NULL;
  if (argc > 1) {
    inputfile = argv[1];
    input = readfile(inputfile, &inputlen);
  }

  printf("Test name, Type, Charset, Filename, Filesize, Reps, "
	 "Total time, Time per rep, Matches, Bytes per match\n");

  /* ----------------------------------------------------------------------------- */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '\n');	     /* add char to set */

  AB_test_find("newline", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("newline", inputfile, input, inputlen, &cs, reps);
  
  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '@');	     /* add char to set */

  AB_test_find("at sign", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("at sign", inputfile, input, inputlen, &cs, reps);
  
  /* Frequency order for English: etaoinshrdlcumwfgypbvkjxqz */

  /* etaionsh are the 8 most freq chars in English */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'e');	     /* add char to set */
  setchar(cs.cs, 't');	     /* add char to set */
  setchar(cs.cs, 'a');	     /* add char to set */
  setchar(cs.cs, 'i');	     /* add char to set */
  setchar(cs.cs, 'o');	     /* add char to set */
  setchar(cs.cs, 'n');	     /* add char to set */
  setchar(cs.cs, 's');	     /* add char to set */
  setchar(cs.cs, 'h');	     /* add char to set */

  AB_test_find("etaionsh", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("etaionsh", inputfile, input, inputlen, &cs, reps);
      
  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'p');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */

  AB_test_find("pbvkjxqz", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("pbvkjxqz", inputfile, input, inputlen, &cs, reps);
  
  /* (+[^|{}] */
  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, '+');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, '^');	     /* add char to set */
  setchar(cs.cs, '|');	     /* add char to set */
  setchar(cs.cs, '{');	     /* add char to set */
  setchar(cs.cs, '}');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */

  AB_test_find("misc punct 8", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("misc punct 8", inputfile, input, inputlen, &cs, reps);

  /* etaoinshrdl are the 12 least frequent chars in English; */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'e');	     /* add char to set */
  setchar(cs.cs, 't');	     /* add char to set */
  setchar(cs.cs, 'a');	     /* add char to set */
  setchar(cs.cs, 'o');	     /* add char to set */
  setchar(cs.cs, 'i');	     /* add char to set */
  setchar(cs.cs, 'n');	     /* add char to set */
  setchar(cs.cs, 's');	     /* add char to set */
  setchar(cs.cs, 'h');	     /* add char to set */
  setchar(cs.cs, 'r');	     /* add char to set */
  setchar(cs.cs, 'd');	     /* add char to set */
  setchar(cs.cs, 'l');	     /* add char to set */

  AB_test_find("etaoinshrdl", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("etaoinshrdl", inputfile, input, inputlen, &cs, reps);

  /* wfgypbvkjxqz are the 12 least frequent chars in English; */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, 'w');	     /* add char to set */
  setchar(cs.cs, 'f');	     /* add char to set */
  setchar(cs.cs, 'g');	     /* add char to set */
  setchar(cs.cs, 'y');	     /* add char to set */
  setchar(cs.cs, 'p');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */

  AB_test_find("wfgypbvkjxqz", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("wfgypbvkjxqz", inputfile, input, inputlen, &cs, reps);

  /* ()gypbvkjxqz has the 10 least frequent chars in English; */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, 'g');	     /* add char to set */
  setchar(cs.cs, 'y');	     /* add char to set */
  setchar(cs.cs, 'p');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */

  AB_test_find("()gypbvkjxqz", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()gypbvkjxqz", inputfile, input, inputlen, &cs, reps);

  /* ()[]pbvkjxqz has the 8 least frequent chars in English; */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, 'p');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */

  AB_test_find("()[]pbvkjxqz", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()[]pbvkjxqz", inputfile, input, inputlen, &cs, reps);

  /* ()[]{}vkjxqz has the 6 least frequent chars in English; */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '{');	     /* add char to set */
  setchar(cs.cs, '}');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */

  AB_test_find("()[]{}vkjxqz", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()[]{}vkjxqz", inputfile, input, inputlen, &cs, reps);

  /* Avg 32.3 bytes/match in /usr/share/dict/words on MacOS */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '@');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, '#');	     /* add char to set */
  setchar(cs.cs, '^');	     /* add char to set */
  setchar(cs.cs, '$');	     /* add char to set */

  AB_test_find("()[]@bvkj#^$", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()[]@bvkj#^$", inputfile, input, inputlen, &cs, reps);

  /* Avg 33.5 bytes/match in /usr/share/dict/words on MacOS */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '@');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, 'k');	     /* add char to set */
  setchar(cs.cs, '+');	     /* add char to set */
  setchar(cs.cs, '#');	     /* add char to set */
  setchar(cs.cs, '^');	     /* add char to set */
  setchar(cs.cs, '$');	     /* add char to set */

  AB_test_find("()[]@bvk+#^$", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()[]@bvk+#^$", inputfile, input, inputlen, &cs, reps);

  /* Avg 40.5 bytes/match in /usr/share/dict/words on MacOS */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '@');	     /* add char to set */
  setchar(cs.cs, 'b');	     /* add char to set */
  setchar(cs.cs, 'v');	     /* add char to set */
  setchar(cs.cs, '-');	     /* add char to set */
  setchar(cs.cs, '+');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, '#');	     /* add char to set */
  setchar(cs.cs, '$');	     /* add char to set */

  AB_test_find("()[]@bv-+j#$", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()[]@bv-+j#$", inputfile, input, inputlen, &cs, reps);

  /* ()[]{}@#jxqz has the 4 least frequent chars in English; */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '{');	     /* add char to set */
  setchar(cs.cs, '}');	     /* add char to set */
  setchar(cs.cs, '@');	     /* add char to set */
  setchar(cs.cs, '#');	     /* add char to set */
  setchar(cs.cs, 'j');	     /* add char to set */
  setchar(cs.cs, 'x');	     /* add char to set */
  setchar(cs.cs, 'q');	     /* add char to set */
  setchar(cs.cs, 'z');	     /* add char to set */

  AB_test_find("()[]{}@#jxqz", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("()[]{}@#jxqz", inputfile, input, inputlen, &cs, reps);

  /* ()*+[\\]^_`{|}~ are 15 uncommon symbols */

  charset_fill_zero(cs.cs);  /* erase all chars */
  setchar(cs.cs, '(');	     /* add char to set */
  setchar(cs.cs, ')');	     /* add char to set */
  setchar(cs.cs, '*');	     /* add char to set */
  setchar(cs.cs, '+');	     /* add char to set */
  setchar(cs.cs, '[');	     /* add char to set */
  setchar(cs.cs, '\\');	     /* add char to set */
  setchar(cs.cs, ']');	     /* add char to set */
  setchar(cs.cs, '^');	     /* add char to set */
  setchar(cs.cs, '_');	     /* add char to set */
  setchar(cs.cs, '`');	     /* add char to set */
  setchar(cs.cs, '{');	     /* add char to set */
  setchar(cs.cs, '|');	     /* add char to set */
  setchar(cs.cs, '}');	     /* add char to set */
  setchar(cs.cs, '~');	     /* add char to set */

  AB_test_find("misc punct 15", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("misc punct 15", inputfile, input, inputlen, &cs, reps);

  charset_fill_zero(cs.cs);  /* erase all chars */
  int j;
  for (j = 65; j < 65 + 26; j++) setchar(cs.cs, j);
  for (j = 65+32; j < 65 + 32 + 26; j++) setchar(cs.cs, j);

  AB_test_find("a-zA-Z", wordsfile, words, wordslen, &cs, reps);
  AB_test_find("a-zA-Z", inputfile, input, inputlen, &cs, reps);

  /*
    TODO: Do the same for the span functions.  Can start with the
    tests in test/unit/findtest.c
  */

  return 0;
}

