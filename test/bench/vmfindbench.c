/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vmfindbench.c  Benchmarking pexle_find()                                 */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* exit() */
#include <assert.h>

#include "libpexl.h"

static
pexl_Position runtest (pexl_Context *C, const char *patname, pexl_Expr *exp, 
		       const char *input, pexl_Position start) {
  int status;
  pexl_Error err;
  size_t inputlen;
  pexl_Optims *optims = NULL;
  assert(C && patname && exp && input);
  
  // The `optims` struct configures compiler optimization passes
  optims = pexl_addopt_simd(optims, 1);
  assert(optims);

  pexl_Match *match = pexl_new_Match(PEXL_TREE_ENCODER);
  assert(match);

  assert(C);
  assert(exp);
  assert(input);
  assert(start >= 0);

  inputlen = strlen(input);

  assert(!pexl_Ref_invalid(pexl_bind(C, exp, patname)));
  pexl_Binary *pkg = pexl_compile(C, PEXL_NO_MAIN, optims, NULL, PEXL_CHAR_ASCII, &err);
  assert(pkg && pexl_Error_value(err) == PEXL_OK);

  pexl_print_Binary(pkg);

  // The vmflags bit 0 being 0 enables SIMD at runtime.
  status = pexl_run(pkg, NULL, patname,
		    input, inputlen, 0, 
		    0,		// enable SIMD
		    NULL,	// do not track statistics
		    match);
  assert(status == PEXL_OK);

  pexl_Position retval = pexl_Match_end(match);
  pexl_free_Match(match);
  pexl_free_Binary(pkg);
  return retval;
}

int main(void) {

  pexl_Context *C;
  pexl_Expr *exp;
  pexl_Position pos;
  
  C = pexl_new_Context();
  assert(C);

  printf("Find '[a-z]' repeatedly (should emit IFind instruction)");
  exp = pexl_repeat_f(C,
		      pexl_find_f(C, pexl_match_range(C, 'a', 'z')),
		      0, 0);

  const char *input = "    Hello, world!!!!!";
  pexl_Position inputlen = strlen(input);
  printf("Input of length %" pexl_Position_FMT " is: %s\n", inputlen, input);
  pos = runtest(C, "[a-z]", exp, input, 0);
  printf("pos = %" pexl_Position_FMT "\n", pos);
  /* Should end at position one past the last lowercase letter */
  assert(pos == 16);

  pexl_free_Context(C);

  return 0;
}

