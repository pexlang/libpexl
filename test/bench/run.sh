#!/bin/bash

libpath=$(cd ../../src && pwd)
. ./setup.sh

echo "Comparing (1) local libpexl.dylib from $libpath"
echo "with (2) system /usr/local/lib/libpexl.dylib"

testprogram=/Users/jennings/Projects/comparisons/libpexl/kjv-libpexl

pattern=$1
repetitions=$2

if [[ -z "$pattern" || -z "$repetitions" ]]; then
   echo Usage: $0 pattern reps
   echo Where pattern is one of: abram longword omega the tubalcain
   echo And reps is a number, like 1000
   exit -1
fi

declare -a commands=(
    "testrun local $testprogram $pattern $repetitions $libpath"
    "testrun system $testprogram $pattern $repetitions $libpath"
)

echo Commands:
for c in "${commands[@]}"; do
    echo $c
done

output_mode="-s basic"
#output_mode="--show-output"

hyperfine $output_mode --export-markdown "$pattern.md" --warmup 5 "${commands[@]}"

printf "\nPattern: %s\n" "$pattern" >> "$pattern.md"
printf "\nRepetitions: %s\n" "$repetitions" >> "$pattern.md"
printf "\nDate: %s\n" "$(date)" >> "$pattern.md"
printf "\nSystem: %s\n" "$(uname -a)" >> "$pattern.md"
printf "\nCommands:\n" >> "$pattern.md"
for c in "${commands[@]}"; do
    printf "* %s\n" "$c" >> "$pattern.md"
done

echo "Wrote $pattern.md"
