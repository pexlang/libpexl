| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `testrun local /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl longword 500 /Users/jennings/Projects/libpexl/src` | 820.9 ± 10.2 | 807.4 | 829.9 | 1.00 |
| `testrun system /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl longword 500 /Users/jennings/Projects/libpexl/src` | 996.1 ± 8.4 | 990.9 | 1019.7 | 1.21 ± 0.02 |

Pattern: longword

Repetitions: 500

Date: Sun Oct 24 09:01:41 EDT 2021

System: Darwin Jamies-Compabler.local 20.6.0 Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64 x86_64

Commands:
* testrun local /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl longword 500 /Users/jennings/Projects/libpexl/src
* testrun system /Users/jennings/Projects/comparisons/libpexl/kjv-libpexl longword 500 /Users/jennings/Projects/libpexl/src
