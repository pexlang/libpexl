#!/bin/bash

# Usage example: ./runall.sh clang gcc-14

default_local_compilers=(gcc-14 clang)

# About macos:
#
# With macos 14.4, Apple broke GCC on mac.  See
#   https://gcc.gnu.org/bugzilla/show_bug.cgi?id=114007
# GCC 14 fixes the issue, but as of mid-2024 this fix has not been
# back-ported to earlier GCC versions, and we don't know whether it
# will be.  So on new macos versions, use GCC 14 or newer.
#
# About Linux:
#
# - centos:8 fails to link the objects together into one (all.o) due
#   to some difference (linker script?) in how 'gcc -r -o ...' works
#   on this distro.  Works fine on ubuntu and MacOS.  Used to work
#   when we invoked 'ld' directly to combine objects, but that is not
#   a best practice and will not work everywhere like 'gcc -r'
#   SHOULD. Error is:
#     /usr/bin/ld: cannot find -lgcc_s
#     /usr/bin/ld: cannot find -lc


# (-e) exit if a command gives an error, (-o pipefail) even if in a
# pipe, (-u) exit if using a variable that is undefined
set -euo pipefail

printf "====================================================\n"
printf "These tests will stop on the first failure.  If they\n"
printf "run to completion, then all tests have passed.\n"
printf "====================================================\n"

# Compilers to use on the LOCAL machine
declare -a compilers
# Containers to launch (via docker/podman scripts)
declare -a containers=(ubuntu debian rocky fedora manjaro)

# Get compilers from the command line, if present.
if [ "$#" -eq 0 ]; then
    # default is to use only cc
    compilers=$default_local_compilers
    printf "Using default compiler list for local machine tests: %s\n" "${compilers[*]}"
    printf "To skip the local machine tests, supply a single dash argument.\n"
elif [ "$1" = "-" ]; then
    printf "Skipping local machine tests.\n"
    compilers=()
else
    compilers=( "$@" )
    printf "Compilers to test locally:\n "
    for c in "${compilers[@]}"; do
	printf " %s" "$c"
    done
    printf "\nContainers to build:\n "
    for c in "${containers[@]}"; do
	printf " %s" "$c"
    done
    printf "\n"
fi    

printf "\n"

if [ "${#compilers[@]}" -ne 0 ]; then
    for c in "${compilers[@]}"; do
	printf "\$\$\$ Testing on local machine using %s\n" "${c}"
	export CC="${c}"
	make clean && make "PEXL_NO_SIMD=1" build && make test
	make clean && make test
	cd ../bench
	make clean && make test
	cd ../system
	make clean && make test
    done
fi

# Container-based tests follow.  Each should do the same tests as
# above, i.e. unit tests with and without PEXL_NO_SIMD set, the system
# tests, and the benchmark tests.
cd ../../containers
for c in "${containers[@]}"; do
    printf "\$\$\$ CONTAINER %s\n" "${c}"
    ./podrun.sh "${c}"
done

printf "====================================================\n"
printf "               All tests completed.\n"
printf "====================================================\n"
