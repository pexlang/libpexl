# Benchmarking

## Versus lpeg

We began with tests culled from Roberto's 2008 lpeg paper [1].  We created 4
variants for each test:

- Search: find the first occurrence in the input, returning its end position
- Capture: find the first occurrence in the input, returning start/end positions
- Search All: find all occurrences, returning the end position for each
- Capture All: find all occurrences, returning start/end positions for each  

A cursory analysis in spring 2022 suggested the first new compilation strategy
we should implement, which is to have a vm opcode that searches ahead in the
input for a given character.  We expect that using this opcode in code emitted
for "search" patterns will improve performance.  Currently, the libpexl code
generator emits the following code to search for the first character in a
pattern:

    *  0  testchar 'a' - on failure JMP to 22               <==== SEARCH STR len=4
       2  choice - on failure JMP to 22
       4  opencapture Common #0 'capture_exp'
       6  any 
       7  char 'd'
       8  char 'a'
       9  char 'm'
      10  span  [(41-5a)(61-7a)]                            <==== SPAN SET
      19  closecapture 
      20  commit and JMP to 25
    * 22  any 
    * 23  jmp to 0
      [SNIP]

The instructions marked with `*` form a loop; these three instructions are
executed for every byte of the input, until the letter 'a' is found or the end
of the input is reached.  This is slow, and can be replaced by a single (new)
"find" instruction.

Performance is even worse when instead of the `testchar` opcode at address 0
above, we have a `testset` instruction.  Our plan is to first replace the
`testchar` loop with a `find` instruction, and then generalize the capability of
`find` to apply to arbitrary character sets.  (Except that empty character sets
will always generate a `fail` instruction, and fully complete character sets
will generate an `any` instruction.)

### Where lpeg will always be best

Because lpeg is tightly integrated with Lua, it has wonderful Lua integration.
The lpeg library API uses Lua data structures, including strings.  Lua's string
representation is interesting in that a string can contain null characters, but
is also terminated with a null.  That allows lpeg to rely on an input string
having one extra character beyond its logical end, and lpeg puts this to good
use.  The lpeg vm implements `char` and `testchar` opcodes by first testing the
current character, and only in the case of a match testing whether the current
character is beyond the end of the input.  The saved comparison in the common
case in which the cursor is not at the end of input gives a measurable
performance boost.

For a Lua user, lpeg's support for call-backs into Lua during pattern matching
provide an escape hatch for all manner of extensions to the pattern language
capability. 

### Where lpeg can be improved

- inlining everything is not efficient; and memory usage in general
- external dsl and Lua independence, though these are alternate design goals 
- look-behind was added to the PEG formalism with only limited implementation
  support (fixed-length patterns only)
- ascii-only (no unicode support)
- persistent patterns and pattern libraries, though these are alternate design
  goals 


## Against Rosie

Comparisons to Rosie are likely to make libpexl shine, because we are building
PEXL in part to overcome these Rosie implementation limitations:

- Rosie parses the definition for a "core RPL" using a hand-coded lpeg parser,
  and then uses "core" to parse the current RPL definition.  It does this at
  every start-up.
- Rosie is worse than lpeg at consuming memory.  Rosie keeps all kinds of
  debugging information around, even when it is not needed.  
- Rosie captures everything by default, so it often produces more data during
  matching than may be needed afterwards by the user.
- The Rosie vm has not been optimized at all, so we suspect we are leaving some
  performance on the table from that as well.


## Against regex libraries

It's not trivial to generate an illuminating set of benchmark patterns and
inputs.  First, PEG parsers can recognize languages that most regex
implementations cannot.  Similarly, regex dialects like Perl and PCRE may
recognize languages that a PEG cannot, due to their various extensions.  Second,
regex implementations do not produce parse trees, but rather flat lists.  If we
write a pattern to find all URLs in the input, a regex will return in its
captures the last one only.  Rosie and libpexl will return a nested structure
containing a list of all the matched items, each of which may have sub-matches.
Such a pattern would make an interesting comparison, as long as we observe that
the PEG implementation being tested is providing a greater service.

Lastly, some (most?) commonly used regex patterns are simply wrong.  E.g. this
pattern to match an email address is from a regex benchmark repository [3]:

    [\w\.+-]+@[\w\.-]+\.[\w\.-]+

It fails to match valid email addresses containing slashes and double quotes,
for example.

Should we transliterate the pattern above to our PEG language and compare it
against a regex implementation?  How faithful must we be to the original
pattern?  For example, can we use the possessive PEG repetition operator, or
must we emulate the backtracking non-possessive regex `+`?

We might instead choose to match Rosie's `net.email` pattern for Rosie or
libpexl against the pattern above in a regex implementation.  Since they match
different sets of strings, and capture different things.  (The pattern above has
no captures, whereas `net.email` captures the entire address, the `name` part,
and the host name part.)

### Example

Using the input file and Python3 program from the "optimized" regex benchmark
repository [4], versus Rosie 1.3.0, we get, roughly:

| Program      | Email pattern | IPv4 pattern | URL pattern | 
| ------------ | ------------- | ------------ | ----------- |
| benchmark.py | 407ms         | 319ms        | 483ms       |
| Rosie v1.3.0 | 436ms         | 270ms        | 352ms       | 
| ------------ | ------------- | ------------ | ----------- |
| Rosie/python | 1.07          | 0.85         | 0.73        |

Notes:
- Rosie was timed by the bash shell, so its time includes startup and file i/o
  for the 6.5MB input file.  The `benchmark.py` program reports its own internal
  timings and does not include startup or file i/o.
- Rosie command was `rosie match -w -o status findall:net.email
  /tmp/input-text.txt`.
- All times are best of 3 after 3 warm up runs.  The Python benchmark times were
  the individual (per pattern) best of 3.  
- The Rosie `net.email` pattern matched 270 possible email addresses, whereas
  the Python benchmark pattern matched only 92.  Probably this is because the
  Python pattern requires two levels of domain names, e.g. "foo@bar.com".  The email
  address standard, which Rosie follows, matches "foo@bar" because the domain
  name does not need to be fully qualified.
- This optimized Python3 benchmark is number 22 of 25 on the posted benchmark
  results.  The fastest entry, Rust, requires merely 3.5% of the time needed by
  the Python3 program.

## Other regex benchmark suites

The benchmark at [5] is from 2010, and is almost as naively constructed as the
one we tested above [4].  At least this older benchmark includes one pattern
with alternation (URI or Email), and several patterns have captures.

The Computer Languages Benchmarks Game [6] has a section called "Regex Redux",
where the best performer is a C program using PCRE2.  That C program requires
only about 8.3% of the time needed by the Python3 entry, and both use parallel
threads running on different cores.  The PCRE2 program additionally uses a regex
JIT compiler.

Is it important at all that these benchmark entries are not trivial programs?
The C/PCRE2 program is over 150 lines.  Contrast the program complexity with the
simplicity of the regex patterns being tested.  They are all so simple that even
RE2 can run them.

The Regex Redux benchmark has two phases: one in which a series of patterns are
matched, one at a time, and the number of matching occurrences is tallied; and a
second phase in which different patterns are searched and their matches replaced
in the output with another string.

We do not support the match/replace operation in Rosie or libpexl, else we could
run this benchmark today.  As results for each phase are not broken out in the
benchmark results, we shall wait until we have a match/replace operation before
reconsidering this benchmark.  We note, though, that this benchmark has
excessively simple patterns -- of the 14 patterns, only two have a star, and
none have any captures.

The fastest match-or-not results on very simple patterns are surely obtained
with tools more specialized than regex, which tend to be used for a wide variety
of more complicated matching.  In particular, we prioritize use cases that
include captures, which we believe to be very common.  Even when doing input
validation, we endorse the practice of "parse, don't validate" [7].  In other
words, it is best to perform a successful parse and store the results (as a
record, struct, or object) than to simply validate.  Validation on its own
results in no new data.  As we pass a string around in our program, we have lost
in its representation whether or not it has ever been validated.



## Resources

1. "A Text Pattern-Matching Tool based on Parsing Expression Grammars", by 
   Roberto Ierusalimschy (SPE 2008)
2. https://www.regexbuddy.com/manual.html#benchmark
3. https://github.com/mariomka/regex-benchmark
4. https://github.com/mariomka/regex-benchmark/tree/optimized
5. http://lh3lh3.users.sourceforge.net/reb.shtml
6. https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/regexredux.html
7. https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/


