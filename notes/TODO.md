# PEXL

## Language design, user level

[ ] Implement Unicode Compatibility matching (like case insensitivity)

[ ] Design concrete syntax for Rosie 2.0

[ ] `x where y` generalizes look-behind, and is `POSTPARSE(x, /, y)` where `/`
	is the null path, i.e. use the entire span of the match `x` to match `y`; a
	path into the tree matched by `x` could be specified instead; in any case,
	it is the match of `x` that is included in the output; matching `y` is a
	predicate -- if it fails, the match fails
	
[ ] `PREPARSE(x, /, y)` generalizes look-ahead by first matching `x` and then
	using `/` or a path into the tree matched by `x` to indicate a span of the
	input; the pattern `y` must match this span, and it is the match of `y` that
	is included in the output; matching `x` is a way to bound the span against
	which `y` is matched -- if `x` fails, the match fails

## Performance and low-level language design

- [ ] Unicode character alternatives are expected to be expensive because they
      require use of one or more IChoice instructions.

	  - A simple example is ASCII (a subset of Unicode) case insensitive
        matching.  The RPL expression `ci:"IBM"` macro-expands into `{{"I" /
        "i"} {"B" / "b"} {"M" / "m"}}`.  Each of the choice operators results in
        an IChoice instruction which pushes a frame onto the backtrack stack,
        only to pop it after a single character test.  E.g. if the next input
        char matches "I", the backtrack stack is popped and the alternative
        discarded; if it does not match "I", the backtrack stack is popped and
        the information in the popped frame is used to reset the input pointer
        (and the capture stack) and jump to the instruction that tries "i"
        instead.
		
	  - Unicode case insensitive matching in general is more complex.  First,
        there are two "case folding" algorithms, called "simple" and "full".  In
        both cases, and in two other situations, we have a sequence of
        alternatives (like in the ASCII example) but each choice is between a
        sequence of bytes.  The two other situations are: Matching characters
        with their "look-alikes", and matching multiple representations of the
        same character.  E.g. the character á can be a single Unicode codepoint,
        (U+E01), or two codepoints (U+61, U+301).  The single codepoint, encoded
        with UTF-8, is two bytes: 0xC3, 0xA1.  The two-codepoint sequence in
        UTF-8 is 0x61, 0xCC, 0x81 -- a 3-byte sequence.
		
	  - We expect to see user-written patterns frequently contain expressions
        like these, i.e. a small number of alternatives, each of which is a
        sequence of small number of bytes. CAN WE EMIT CODE THAT RUNS FASTER
        THAN THE CURRENT APPROACH THAT USES IChoice?
		
	  - One approach is to first limit the number of alternatives and the number
        of bytes in each.  (Later, we can relax these limits if needed.)  Let's
        consider 3 alternatives to start, because their are 3 Unicode cases
        (upper, lower, and title).  And we'll consider sequences of at most 4
        bytes for each alternative, because both UTF-8 and UTF-16 use at most 4
        bytes to encode a single codepoint.
		
	  - With 3 alternatives of 4 bytes each, we have 12 bytes of data that
        parameterizes the match we want to perform.  Suppose we exclude the NUL
        character from eligibility, so we can use it as a sentinel.  (Our
        compiler can generate different code when we need to match alternatives
        that include NULs.)  Then, the example from above of matching either
        form of á would have this data, assuming UTF-8:
		
		`C3 A1 00 00`   // 2-byte variant
		`61 CC 81 00`   // 3-byte variant
		`00 00 00 00`   // No third variant
		
		For each alternative:
		1. Ensure there are 4 or more bytes left in the input.
		2. Interpret the 4 bytes as an unsigned 32-bit integer, P.
		3. Interpret the next 4 bytes of input as an unsigned 32-bit integer, I. 
		4. Calculate `M = P^I`, a bitmask indicating which bits of input failed
		   to match the pattern bits, where `^` is bitwise XOR.  Consider the
		   first byte of `M`.  It will be zero iff the first byte of `P` matched
		   the first byte of `I`.  But what about the "don't care" bytes of `P`,
		   which are zero?  In `M`, those bytes may be zero (if there are NULs
		   in the input), or more likely they will have some 1 bits.  If we can
		   mask out the bytes of `M` that align with the zero bytes in `P`, then
		   we have a match when `M==0`.
	    5. Calculate `Z` where each byte `Z[i] = (P[i]!=0)-1`.  With unsigned
           arithmetic, `Z[i]` will be -1 (0xFF) for each non-zero byte of the
           pattern, `P`, and 0x00 for each zero byte of `P`.
	    6. calculate `Result = M & Z`.  The zero bytes in the mask, `M`, will
           zero out the "don't care" bytes of `M`.  The 0xFF bytes of `M`
           preserve the contents of corresponding bytes of `M`.  If `Result` is
           zero, then we have a match, no matter how many bytes of `P` we were
           trying to match.
		   
	  - Note that the zeroing mask, `Z` could be computed at compile time, since
        it depends only on the pattern.  And if we did this, we would not need
        NUL bytes as sentinels.  However, the data controlling the match
        instruction would double in size because each byte sequence (4 bytes, in
        our current formulation) would be accompanied by a mask of the same
        length.  Since cache memory is relatively small, and arithmetic
        operations are fast, it's possible that computing `Z` at runtime will be
        faster due to the cost of cache misses.  Experimentation is needed.
		
	  - Also note that we should compute `Z` all at once, if possible, instead
        of one byte at a time.  I don't see an obvious way to do that, though
        there may be an approach that adds a little extra pre-computation (and
        storage) to make `Z` easy to compute.  For example, consider a 32-bit
        value with only the high bit set and the rest are 0.  This is
        `INT32_MIN`.  Right shifts are "sign-extended", so `INT32_MIN >> 7`
        gives 0xFF000000.  We could compute mask `Z = INT32_MIN >> k` where k=7
        for a one-byte `P`, and k = 15 or 23 or 31 for 2, 3, or 4 byte patterns,
        `P`.  We could store one more byte, containing k, with each of the
        alternative patterns, then do the shift at run-time.
		
	  - Finally, we might compare the above approaches to computing `Z`
        directly from `P` at runtime using the signed right shift method.		

	  - Returning to the matching algorithm for "alternatives of short byte
        sequences", the above steps calculate whether we have a match for one
        pattern `P`, but we hypothesized a set of, say, 3, such patterns.  If
        any of them match, then we have a match and our program continues, else
        the match fails.  We could use a sentinel like 0 to indicate we should
        stop matching.  I suspect it would be faster to have a fixed number of
        alternatives, and try all of them.  There will be more calculations than
        if we stop early (at the first `P` that is zero, e.g.) but we will not
        do any branches until the very end, when we branch on "was there any
        match" or not.  Experimentation is needed.

- [ ] We have 3 data structures that store strings: hashtable, stringtable
      (which wraps hashtable), and the symboltable.  We should consolidate as
      follows: 
	  - [ ] Change the hashtable to use the new table.c code.
	  - [ ] Allow the payload (data) size in a hashtable entry to be of
            customizable size. 
	  - [ ] Merge hashtable and stringtable, keeping hashtable.
	  - [ ] Change symboltable to use hashtable to intern strings.

- [ ] Change uses of `NOT_EOI` to not branch, when possible. E.g. the current
      ITestAny code is:

      ```
      if AT_EOI JUMPBY(X);
      DISPATCH;
      ```

      Where `DISPATCH` is `goto *vm_labels[(pc)->i.code & 0xFF];`.

      Because `pc` already points at the next instruction, the ITestAny code
      could be something that uses a CMOV (conditional move) instead of a
      branch.  Maybe split calculation of the destination address from the
      evaluation of the branch condition?

      ```
      branch_dest = PC + X;           // calculate
      if AT_EOI PC = branch_dest;     // possibly CMOV
      DISPATCH;                       // goto
      ```

- [ ] Consider having a computed goto target for 'fail'.  Then we can select it
      in a calculation involving `NOT_EOI` or `EOI`.  E.g. IChar code is
      currently:

	  ```
      if (NOT_EOI && ((uint8_t)*c == (A & 0xFF))) {
        ADVANCE;
        DISPATCH;
      }
      goto fail;
      ```

      But it could be:

      ```
      Bool pass = NOT_EOI && ((uint8_t)*c == (A & 0xFF));
      c += pass;
      DISPATCH_IF(pass);
      ```

      `DISPATCH_IF` is `goto *vm_labels[(((bool)(cond)) * (pc)->i.code) & 0xFF];`.
      This definition relies on `IFail` being the 0th entry in `vm_labels`.


- [ ] Define environment vars that test programs can check to determine:
      - [ ] If they should print output
	  - [ ] If they should conduct exhaustive tests and fuzzing
	  - [ ] Then implement `make fuzzing` and `make printing` for convenience 


- [ ] Implement s-expressions.
      - [X] Write an s-expression parser.
	  - [X] Implement an s-expression writer such that `read_sexp(str)` produces
            an identical structure to `read_sexp(write_sexp(read_sexp(str)))`,
            provided there are no errors in the expression.
	  - [X] Avoid using the C stack to parse nested s-expressions, so that we
            can parse arbitrarily deeply nested ones.  To do this, convert to
            CPS, eliminate tail calls, and defunctionalize the continuation.
	  - [X] Use an arena allocator for the continuations.
	  - [ ] Use an arena allocator for the s-expressions created while parsing.
	  - [ ] Use the s-expression parser to write ASTs in s-expression format.

- [ ] Build in an s-expression encoder for match tree output.  It should be
      fast, because the capture stack is essentially in the right form already.
      It could be a useful alternative to the tree encoder.
	  
- [X] Unify all the string implementations, e.g. String in reader.c and
      buf.c/buf.h.  There may be others!  (Created `pstring`.)

- [ ] Build in a JSON encoder.  Not with a pretty-printer.  Can add that later,
      of course.

- [ ] Natalie suggested that if we needed to reorder a test for EOI so that it
      occurs AFTER we have examined a character, we could try something like
      this: Access A[i] where i = ++c - (c/len).  With integer division, this
      increments c, then looks at A[c], i.e. one past the old value of c.  When
      the new value of c==len, we have exhausted the input.  But i = c, so the
      A[i] access is reading valid memory.  As long as we check for eoi if we
      get a match after, like lpeg does, this should work.  We need to
      experiment or investigate the speed of integer division, of course.  And
      we should code it without a pre-increment and a regular reference to c in
      the same expression.
      ALTERNATIVE: An offset (integer) comparison is probably faster than
      integer division, so we could do this instead: `c++; i = c - (c == len);`
      Now we can examine A[i] knowing that i < len, provided c < len before the
      calculation.  If A[i] is a match, then check for EOI, maybe like this:
      `if ((A[i]==chr) && (c < len)) ... match ...`
	  RELATED: Sometimes we can advance the input pointer, c, beyond the end of
      the input, as long as we don't read the byte there.  We do this now in
      `ISet` and `IAny`.  C does not guarantee us read access to that byte, but
      apparently there is plenty of vectorized code out there (including in
      glibc) that allows _loading_ bytes beyond the end of an allocated block,
      provided at least one byte is within the block.  It is an error to use the
      bytes beyond the end of the block, but not to load them.  However, loading
      them must be done carefully to avoid a page fault.  Since we are dealing
      with bytes, and with byte arrays of arbitrary length and alignment, it
      does not seem possible to use these techniques.  See, e.g.
      https://stackoverflow.com/questions/37800739/is-it-safe-to-read-past-the-end-of-a-buffer-within-the-same-page-on-x86-and-x64

- [ ] Rework the expression interface (simplify!).  Idea:
	  - [X] `pexl_match_bytes(C, bytes, len)`
	  - [X] `pexl_match_any(C, n)`
	  - [X] `pexl_match_set(C, bytes, len)`, `pexl_match_range(C, from, to)`
	  - [X] `pexl_match_fail(C)`
	  - [X] `pexl_lookahead(C, exp)`, `pexl_neg_lookahead(C, exp)`
	  - [X] `pexl_lookbehind(C, exp)`
	  - [X] `pexl_choice(C, either, or)`
	  - [X] `pexl_seq(C, first, next)`
	  - [X] `pexl_find(C, exp)`
	  - [X] `pexl_repeat(C, exp, min, max)` (max can be infinite)
	  - [X] `pexl_call(C, exp)`
	  - [X] `pexl_xcall(C, pkg, exp)`
	  - [X] `pexl_capture(C, name, len, exp)`
	  - [X] `pexl_insert(C, name, len, value, len)` ("constant capture")
	  - [X] `pexl_scope_push(C)` to enter a new lexical scope; creates a new
			environment (child of current) and updates the current environment as
			stored in the context
	  - [X] `pexl_scope_pop(C)` to leave the current scope for its parent
      - [X] All the `pexl_<exp>_f` functions are convenience functions.
	  - [X] Convenience function: `pexl_match_epsilon(C)` calls `pexl_match_bytes`
	  - [X] Convenience function: `pexl_match_string(C)` calls `pexl_match_bytes`
	  - [X] Convenience function: `pexl_neg_lookbehind(C, exp)` calls
            `pexl_neg_lookahead(C, pexl_lookbehind(C, exp))`
	  - [X] `pexl_backref(C, name)` and `name` refers to a capture name
	  - [ ] `pexl_where(C, exp1, path, exp2)` and `exp1` is matched first, then
            the input span at `path` (rooted at `exp1`) is matched against
            `exp2` 

- [ ] Work out a full specification for back references.  The definition
	  pioneered in Rosie 1.x is "the most recent capture with the same name
	  matches, but not within a finished recursive call to the same current open
	  rule".  This allows `extra/examples/html.rpl` to work properly (from the
	  Rosie repository).
	  - [ ] We need an example (with a parse tree diagram) of `backref` working
            without any recursion.  E.g. match the same url or ip address twice.
	  - [ ] Now add recursion, as in `html.rpl` or the minimalist `xml-lite.rpl`
            here in the pexl repository.  Show how it is supposed to work.
	  - [ ] Finally, what happens when the recursive pattern does not have a
            capture?  The `backref` does not do what we want.  Is there any way
            it can, when it appears that some necessary information (in the form
            of parse tree structure) is missing?
	  - [ ] Decide how to handle that case in the implementation.  Suppose that
            using `backref` within a recursive pattern does not work as expected
            unless the recursive pattern is captured.  Can we detect this at
            compile time?  Or do we need to detect it at run-time?  (If the
            latter, does it cause an error, fail to match, or try match an
            unintended target capture?)
	  - [ ] Or is there some way to make it work as desired even in the absence
            of the recursive capture?
	  - [X] Finding the referent of a back reference is just like looking up a
            binding in a language with dynamic scope: we find the most recently
            created binding.

- [ ] Define the _reversible_ subset of PEG expressions.
      - Epsilon is reversible.
	  - A single symbol is reversible, as is a fixed sequence of them.
	  - A sequence of reversible expressions is SOMETIMES reversible.  Kristian
	    has a sufficient condition that ensures we get the same match backwards
	    as forwards.
	  - A choice between two reversible expressions is NOT necessarily
        reversible: 
		OK: `(a/ab)b` matches "ab", and `b(a/ba)` matches "ba"
		NOT: _I thought of this example earlier but it was too long to fit in
        the margin..._
	  - Is the star of a reversible expression reversible?  Can expand a star
        using choice (with epsilon) and sequence (with itself), so probably
        depends on the conditions for sequences and choices.
	  - What about fixed-repetitions, like twice or from 3 to 5 times?
	  - Harder cases, if we want to go there: look-arounds, anchors,
        back-references, `where` expressions.
	  - Krisjian has formulated what might be a set of sufficient conditions for
        an expression (using a subset of the full PEG operator set) to be
        reversible.  We need to validate these, and then look at whether we can
        efficiently evaluate these conditions (or conservatively approximate
        them).  Assuming we will end up with an approximation, how often does it
        yield a useful result?  Need a collection of expressions that are
        arguably "typical searches containing multi-character literals".

- [ ] Implement a `reversible` test in the analyzer that detects,
      conservatively, whether we can match a pattern backwards.  When we can, we
      can use tricks like _first locate "Abram", then work backwards from there
      to match zero or more spaces followed by [A-Za-z]+_.  (This is the pattern
      from our benchmarks called `abram`, defined in the lpeg paper.)

- [ ] Implement in PEXL the worst-case asymptotic run-time classifier we
      designed and prototyped with Jacob some years ago in Rosie.  Outputs:
      Constant, Linear, Super-linear.
	  
- [ ] Opportunity to fuse open/close capture with ICommit.  Consider the `abram`
      pattern, captured and with the search loop wrapped around it:
	  ```
	   0  find (cs #0) (9-255 chars) [A-Z, a-z]
	   1  choice - on failure JMP to 16
	   3  opencapture Common #0 'find_capture'
	   5  any 
	   6  span (cs #1) [A-Z, a-z]
	   7  span (cs #2) [0x20]
	   8  char A
	   9  char b
	  10  char r
	  11  char a
	  12  char m
	  13  closecapture 
	  14  commit and JMP to 19
	  16  any 
	  17  jmp to 0
	  19  ret 
	  ```

	  The entire choice is captured with pattern name #0 in this example.  This
      is a common situation: a choice between a captured pattern and another
      one.  We should consider designing a new instruction that fuses `IChoice`
      with `IOpenCapture`.  Assume a regular (common) capture type, so that the
      aux field can hold the string table entry and the operand field can hold
      the jump offset.
	  
	  We pair this with a fused `ICommitClose` that has an offset to jump to.
      We'd still have the same amount of stack fiddling, though: one BT stack
      entry (`IChoice`) and one Capture stack entry (`IOpenCapture`).
	  
	  Maybe there's some way to reuse those entries?  The BT stack choice frame
      points to the Capture stack frame, and surely it would be cheaper to
      merely update the input position in the Capture frame than it would to pop
      both stacks, create new frames, and push them.  **THIS BEARS FURTHER
      EXAMINATION.** 

	  To reduce the number of instructions, perhaps more is to be gained by
      replacing the 5 IChar instructions with, e.g. an `IChar3` and an `IChar2`.
      Yet, the execution spends little time there.  The `IChar A` fails almost
      27x as often as it succeeds (2777/74652), i.e. it succeeds less than 4% of
      the time.  And there are only 59 occurrences of `Abram`.  If the
      implementation searched for `Abram` and then worked backwards, it would
      only evaluate 59 candidate matches, of which 55 will end up succeeding.
      (See the "ground truth" match data in the benchmark project, e.g. in
      `comparisons/libpexl/kjv-libpexl.c`.)

- [ ] Consolidate all string functions into one module:
      - [-] For convenience, maintain a NUL terminator that is not included in
        the length of the string (makes printing easy when user knows that there
        are no NUL bytes)
      - [X] Create and free arrays of chars (which we ensure are 8 bit units)
      - [X] Append strings to the end of a string, which copies the appendage(s)
      - [X] Concatenate any number of strings, producing a new string
      - [X] Escape a string, producing a new one containing only printable ASCII
      - [X] Un-escape a string of printable ASCII, producing a new byte string
      - Eventually:
        - [X] Subsume the buf package by implementing the remaining functions
              defined there
        - [ ] Validate that a byte string is UTF-8
        - [ ] Advance to next UTF-8 codepoint
        - [ ] Convert a (uint32_t) codepoint into a UTF-8 byte string
        - [ ] Calculate codepoint (uint32_t) from UTF-8, returning also whether
              the next codepoint is a combining one

- [X] The implementation of IBytes should start out simple (strstr, assuming we
      ensure there are no NUL chars in the subject string) and become more
      sophisticated (SIMD? Boyer-Moore? KMP?) only as needed.

- [ ] Consider having two IPartialCommit instructions, in order to avoid the
      runtime test of the flag stored in aux(pc).

- [ ] Why does ILookBehindCommit have an operand (offset) field that it doesn't use?

- [ ] Looks like ICloseConstCapture could use the aux field and be a 1-word instruction.

- [ ] Peephole should look for jumps to the very next instruction?

- [ ] This type checking macro could be useful:
      `#define CHECKED_TYPE(original_type, p) ((conversion_type*) (1 ? p : (original_type*) 0))`
	  From https://www.trust-in-soft.com/blog/2015/07/06/compile-time-consistency-checks-for-types-in-c/

- [X] Put optimizers back into `lookaroundtest` and find the optimizer that is
      not updated the pattern entrypoint.  And `unrolllooptest`.  SOLUTION:  The
      opt-unroll code was missing some post-unroll steps, like updating the
      symbol table and also jump/call targets.

- [X] The default entrypoint for a package needs attention.  In unit tests like
      `vmtest`, we set it manually to the last thing compiled, because it is not
      set by any `libpexl` function anymore.

- [X] Compilation and linking needs a `CompState`, string table, package table,
      binding table.  Seems the `CompState` could store a link to those other
      tables, to simplify the compiler interface.  SOLUTION: Used Context.

- [ ] Do we need C->err?  Should it be an Error struct instead?  See compile()
      in compile.c and pexl_compile() in libpexl.c for examples of where cleanup
      is needed.

- [ ] Add a `pexl_import(C, importpath)` high-level API, supported by internal
      APIs something like these: `add_import(C, pkg)` and `Package *load_pkg(C,
      importpath)`.

- [X] Should `pexl_bind()` continue to take ownership of its expression argument,
      or should it make a copy?  SOLUTION: It makes a copy, as it should.

- [X] Separate user package from the package table.  Did NOT rename `PackageTable` to
      `ImportTable` because each package has a field which its import table.

- [ ] At runtime, user must supply a Package and the **same** package table used
      during compilation to the vm.  How best to ensure this or detect that it
      is not the case so we can throw an error?

- [X] Recent changes have broken the XCall test in interfacetest.c.  The call to
      `reset_compilation_state` appears to chase a wild pointer.

- [X] Move `optims` out of the Context.  Shorten `Optimizer_config`.

- [ ] Implement a `table` package for dynamically expanding tables.  We use
      these all over the place.

- [ ] MAYBE coalesce Optimizer Configurations into a tagged union, and allocate
      a single array of them to simplify their memory management.

- [ ] MAYBE convert the `Optims` argument to `pexl_compile` into a more general
      struct that contains optimization info but also other flags that configure
      compilation, such as whether to retain debugging info in the compiled
      package. 

- [X] In compile.c, rename compiling and linking and finishing up.

- [X] DECIDED AGAINST THIS.  Maybe change `Binding` to be something like
      `BindingRecord`, so that we can rename `Ref` to become `Binding`.

- [X] Maybe change `Env` model to be a single table.  Add a field like `env_id`
      (int), where 0 is the root.  The result, `e` of `new_env(top)` is an
      integer `env_id` for a new env that is a child of `top`, and we keep a
      table of parent-child relationships as part of the `Env` structure.
	  - Now, a Binding is just an index into the single `Env` table.
	  - Multiple environments (to implement local scopes) will incur fewer
		allocations, as there's no need for a new `Env` structure, just an entry
		into an existing table encoding the environment tree.
	  - HOWEVER, iterating through an environment takes longer, because we now
		have to examine every binding (across all environments) to find ones whose
		`env_id` matches the one we want.  Maybe AVOID OPTIMIZING THIS NOW,
		because if it becomes an issue, we can work on it later.
      - Sunday, July 30, 2023: This change probably took 20 hours to accomplish,
        not because the Environment representation is exposed at the PEXL_API
        level, but because so many of our unit test programs use internal
        functions that manipulate environments in one way or another.

- [X] The `pexl_Error` and `pexl_XRef` structures used by the PEXL_API should
      not have their full definitions exposed, i.e. they should be
      encapsulated.  This is the case now in `libpexl.h` but while it works for
      unit tests (which see the structure details), it does not work for "client
      code" that calls the PEXL API.  Either we make these into pointers, or we
      expose their full structure, or we find another way.
	  - [X] `pexl_Error` done.  Now it's two 32-bit values packed into a single
            64-bit int.
	  - [X] `pexl_XRef` done.	

- [ ] Use case: We need only the start/end positions of a match to `P`, i.e. we
      are _searching_ for `P`.  Today we would match `find:P'`, where `P'` is
      `P` wrapped in a capture.  This does the job, but it also does extra work:
      it builds the parse tree for `P` even though we don't need it.  Worse, the
      root of the parse tree does NOT contain the start position for `P`; it
      holds the position at which we started searching.  The one child of the
      root holds the start position.  How can we optimize for this use case,
      which is surely a common one?

- [X] Implement a better loop for "Find". The expression 'find:e' compiles into:
	  ```
	  L0: testset <firstset(e)> - on failure JMP to L1
          choice - on failure JMP to L1
		  <e>
		  commit and JMP to L2
	  L1: any 
		  jmp to L0
	  L2:
	  ```

	  When there are many "false positives", i.e. many places in the input in
	  firstset(e), after which 'e' fails, a significant amount of effort is
	  spent recovering from the failure: The backtrack stack is unwound down to
	  the BTChoice frame, restoring the input position and jumping to L1, which
	  ultimately takes the vm back to L0.  At the next match to firstset(e), a
	  new BTChoice frame is constructed, and it looks exactly like the previous
	  one except for the new input position.  The jump address (on failure) is
	  clearly the same; less obvious, perhaps, is that the top of the capture
	  stack must also be the same.

	  There is an opportunity here to reduce the cycles expended by 'find:e' if
	  we introduce an unconditional loop construct that reuses its stack frame
	  on failure, like BTBackLoop does, instead of popping it and processing its
	  contents, like BTChoice does.
	  ```
			findloop L1           // push a BTFindLoop frame
		L1: find firstset(e)      // saves position in BTFindLoop frame
			<e>
			commit and jmp to L2  // pop the BTFindLoop frame
		L2:
		```

	  The BTFindLoop frame is has the same structure as BTChoice.  A new
	  ILoopUpdate expects BTFindLoop at the top of the stack, and updates the
	  position field with the current position.

	  On failure, the vm unwinds the stack until it encounters a "stopping"
	  frame, like BTChoice.  BTFindLoop is a new stopping frame.  On seeing this
	  frame, the vm restores the capture stack top, restores the input position
	  while advancing it by one and checking for EOI, and jumps to the stored
	  address (top of the loop).

- [ ] Change the way `Xref` data is stored in a tree node.  We currently put the
      Binary package there, along with a symbol table index.  But during code
      generation, we need the index of the package in the package table, which
      we (could have) had at expression-building time!
	  
- [X] Expose functions to manipulate MatchTree.  Use them in
      test/system/kjv-test.c so that it will compile with the new `libpexl.h`.  

- [X] Fix `pexl_BinaryNumber` which was created in all the automated renaming.

- [X] Maybe change `packagetable->size`  to `packagetable->next` to follow the
      convention used elsewhere the code.  Or go the other way.

- [X] Design compiler interface to include character encoding as argument
- [X] Redesign compiler interface overall.  Idea:
	  + The primary interface should be `pexl_compile(C, optims, encoding)`.
	    The `encoding` parameter is for future use, when we emit different code
	    for UTF-8, UTF-16 BE/LE, and UTF-32. 
		+ We have a separate TODO item to enhance this to include other flags,
          ones that **no effect** on semantics such as whether to retain or
          strip debugging information.
	    + Also a separate item to add `encoding`.
	  + Have a default set of optimization passes configured.
	  + The compiler should return an executable.
	  X Rename `Package` to something like `pexl_binary`, and ensure it contains
	    everything that the vm needs. **Keeping the name `package`.**
	  + This approach requires all expressions to be bound.  We should have a way
		to bind the "main" expression, for when we are not compiling a library.
		In this case, the symbol table index of the main expression is stored in
		the executable package.  _Separate TODO item for this._
	  + The current `pexlr_match` should become `pexl_run` and take an
		executable, an input, and an optional pattern name.  No pattern name means
		"execute the main pattern".
	  + We will need a way to alter run-time configuration, e.g. to disable SIMD
        or enable some amount of tracing/debugging on a per-invocation basis.

- [X] Make it easy to compile and run a single expression, even anonymously, by
      keeping the idea of a default entrypoint (maybe change to a default symbol
      table entry), and allowing the pattern name argument of `pexl_run` to be
      NULL. 
- [X] Add an encoding argument to `pexl_compile`.
- [X] Add a `PackageAttributes` argument to `pexl_compile`, which can be NULL. 

- [X] Implement optimization validation to detect policy violations, e.g. when
      peephole appears before an optimizer like inlining that depends on
      function integrity.  (Is there a name for the property that the code of a
      function contains branches whose targets like within it?  It's something
      larger than a 'basic block'.)
      - [ ] Test optimizer validation.

- [X] gcc-12 and gcc-13 on macos fail with an LTO error.  Temporarily disabled
	  LTO for gcc.

- [X] Extend the test programs that Vivek wrote to compile with different
      optimization configurations, and ensure that the complete match structures
      are identical across all of them, using "no optims" as the ground truth.

- [X] Symbol table: Adding strings, import paths, or capture names should return
      the index of the existing item if it is already in the table, because
      these can be reused.

- [ ] The `analyzetest` programs, and others written around the same time, are
      quite low level.  They were written to manipulate individual structures
      that are now aggregated into larger ones like `Context` and `CompState`.
      In order to keep these test programs working (and without memory leaks),
      we have had to patch them repeatedly as we create and reorganize the
      larger structures.
	  - Consider whether we need to retain these unit tests.
	  - If so, then consider recoding them to use the new high level API, which
		uses the new large structures.  This should simplify those tests
		considerably. 
		[ ] `analyzetest`
		[ ] `analyzetest2`
		[ ] `analyzetest3`
		[ ] `analyzetest4`

- [ ] The pattern epsilon emits no instructions, except for the IRet that ends
      it.  When calling such a pattern, the call should be replaced by INoop.

- [X] We should regularize the instructions and the DISPATCH macro should fill
	  local variables with the aux and addr args.  Wrote DECODE0, DECODE1, and
	  DECODE2 to decode only as much as needed, and to do so before the decoded
	  operands will be needed.

- [X] Consider storing `ADDR - 1` as the jump target, so we don't have to do the
      subtraction at runtime.  When printing instructions, will need to
      increment the addr field so that it reads properly.

- [ ] Memoization/caching.  Packrat parsers memoize everything, so their worst-case
      space consumption is O(NM) for input size N and number of grammar elements M.
	  - Selective memoization could be used to eliminate worst-case exponential
		run-times of pure backtracking (non-memoizing) PEG parsers like Rosie.
		Selecting certain patterns -- those involved in recursion -- might be a win.
	  - Another approach is to stochastically memoize.  Taking inspiration from skip
		lists, we might flip a coin on whether to memoize the result of pattern P at
		position n.  Doing the same for sub-patterns of P is analogous to the next
		higher layer in a skip list.
	  + Our implementation of SIMD `find` (as of Friday, June 23, 2023) shows that
		testing 16 input bytes at once is a net performance gain except when the
		number of matches per 16 bytes is around 8 or more.  In that range, we lose
		performance because we currently throw away results after the first byte
		matched.  We should cache those results -- see separate item below.  In the
		worst case, currently, we match on all 16 bytes, `find` stops at the first
		byte, and we discard the fact that the next 15 bytes also match.  So if we
		advance by 1 byte and run the same `find` again, we're guaranteed to get a
		match (and 14 more after that!) because we've processed this part of the
		input already.
		- Caching the result of SIMD `find` raises some of the same issues as
		  memoization in general.  So, let's explore ways of memoizing in general, to
		  see if that yields design ideas for caching the results of SIMD `find` (and
		  `span`).
		- Idea 1: Add an explicit "check the cache" instruction, which would be used
		  as a guard before a possibly-unneeded instruction like `call P` or `find
		  <charset>`.  "Guard" is a traditional PL term, and is appropriate, but it
		  has the wrong connotation.  Here, we are skipping an instruction because
		  we already have the result.  Maybe "expedite" is a better word.  In any
		  case, such an instruction would implicitly refer to the following
		  instruction.  If we consider an expediter for any instruction, the
		  execution of such an instruction would:
		  1. Use the PC, which now points to the _expedited instruction_ and the
			 current input position as the index into the cache.
		  2. If there's a hit, add the capture stack fragment stored there to the
			 global capture stack; set the new input position and new cap stack top
			 (and other vm state) using the cached data; skip the next instruction.
		  3. To deal with inlined calls, the expediter instruction could include the
			 address to jump to if there's a cache hit.
		  4. What if there's no cache hit?  Then the expedite instruction stores the
			 address of the expedite instruction and the current vm state.  The vm
			 state includes the input position, top of capture stack, and any global
			 flags (like capturing/not).
		  5. If there's no cache hit, then how do we fill out the remainder of the
			 cache entry?  We need the _results_ of executing the instruction that
			 we could not expedite.  Let's assume the expedited instruction cannot
			 jump.  We could emit an explicit "cache current results" instruction
			 after the expedited instruction.  In this case, the expedite
			 instruction explained above would skip the next TWO instructions (the
			 expedited one and the explicit cache instruction).
		  6. The explicit cache instruction would first need to find the cache entry
			 that it needs to fill out.  So it must know the address of the
			 expedited instruction (which immediately precedes it) and the position
			 in the input _before_ the expedited instruction.  **How to know
			 the previous input position?**
			 - Could add another piece of vm state to hold previous input position.
			   But more vm state is bad; it must be saved and restored.
			 - The cache could store a stack of indices to the open entries.  Each
			   expedite instruction pushes onto that stack the index of the new
			   entry it creates.  Each cache instruction pops that stack and uses
			   the removed value to know where is the entry it must fill out.
			 - Could leave space _within_ the expedite instruction, so the expedite
			   instruction could store the input position there within the byte
			   code itself.
		  7. The cache entry is guaranteed to exist, because it was created by the
			 expedite instruction.  What the explicit cache instruction must save
			 there is: the current input position and other vm state, and a copy of
			 the capture stack segment from the earlier (saved) top frame index to
			 the current top index.
	   -  Idea 2: Take the actions of the expedite and cache instructions described
		  above in Idea 1, and fuse them with the instructions that would benefit
		  most from caching.  E.g. we could have both "call" and "cached call" (and
		  maybe "cached inline call", too).
		- We have made the assumption that _an expedited instruction cannot branch_.
		  But `ITestChar` and `ITestSet` branch.  Extending the current design to
		  accommodate such instructions should be possible, but things may get
		  complicated.  E.g. we may need variants of the `Test` instructions that
		  set a vm flag instead of branching.  Then, the explicit cache instruction
		  can check that flag to know the instructions results, cache those results
		  (including the jump address) and then take the jump.
		- However, almost any instruction can branch by invoking the exception
		  hander on failure.  What happens if the expedited instruction fails?  If
		  it had been preceded by an expedite instruction, we want to record the
		  failure in the cache as part of failing.  How can the `fail` routine know
		  that one (or more!) expedite instructions was in progress?  This sounds
		  like a stack issue, i.e. it seems that we need `expedite` to push a frame
		  on the backtrack stack if there's no existing cache entry, and `cache` to
		  pop that frame once the cache entry is complete.  Then, `fail` can process
		  any "incomplete" cache entries with "the expedited instruction failed" as
		  it unwinds the stack.

- [X] Resolve the matter of simd charsets not storing a "char not in this set"
      for use as sentinel non-matching values when processing < 16 bytes.

- [X] Write tests for all the "span" functions defined in find.c.
- [ ] Write MORE tests for all the "span" functions in findtest.c.  Ensure a
      variety of start/end positions for the target span within the input.

- [X] Cache SIMD search results.  As of Friday, June 23, 2023, it looks like
	  that testing 16 input bytes at once is a net performance gain except when
	  the number of matches per 16 bytes is around 8 or more.  In that range, we
	  lose performance because we currently throw away results after the first
	  byte matched.  We should cache those results.
	  + Cache implemented, but not being used.  Monday, July 3, 2023
	  + Checking the cache.  Wednesday, July 5, 2023
	  + Worked on the `find_simd` API.  Thursday, July 6, 2023
	  + Benchmarked `find_simd()` with the new `findbench`.  Thursday, July 6, 2023
	  + Reinstate checking the cache, and use the stored results. 
	  + Measure performance with `findbench`. Sunday, July 9, 2023 
		SEE `find-results-diff.txt`.
		**VERDICT**: Cached SIMD wins over regular byte-at-a-time searching in almost
		all cases.  For single characters, gains are modest and there are some
		losses, so it may be best to stick with memchr.  For full character sets
		and inputs where virtually every byte matches, the byte-at-a-time method
		works better.

- [ ] MAYBE Make the simd cache size configurable at `pexl_compile()` time?

- [X] Run the `findbench` program on x86.  All tests as of Sunday, July 9, 2023
      were done on a Mac M1 (arm64). **NOT USING `findbench` ANYMORE.  SEE THE
      COMPARISONS REPO FOR A FULL SET OF BENCHMARKS.**

- [ ] After analyzing `findbench` results on x86, decide whether to replace cached
      `find_simd1()` with the simpler `memchr` version.

- [ ] Add C block comments, which are not allowed to nest, to the benchmark suite:
		```
		Comment <- ’/*’ Close 
		Close <- ’*/’ / . Close
		```
		Or the shorter version `Comment <- ’/*’ (!’*/’ .)* ’*/’`, though the
		longer one, above, is easier to read and shows off our tail recursion
		elimination. 
		But then we need to use C code as input.  Maybe we can find an analogous
		construct for the input file we are already using, kjv10.txt.

- [ ] Consider STARTING the simd searches by manually stepping through each byte
	  until the input pointer is at a 16-byte boundary.  At that point, stepping
	  through 16 bytes at a time is faster due to the aligned loads (supposedly).
	  Would need to do an A/B test to see.  Some helpful snippets:
      - Test that a value is a power of two
       ``` 
       bool power_of_two(uintptr_t x) {
           return (x & (x-1)) == 0;
       }
      ```
      - Take a pointer and advance it to a given alignment
      ``` 
      uintptr_t advance_align(uintptr_t p, size_t align) {
        uintptr_t a, modulus;
        assert(power_of_two(align));
        a = (uintptr_t) align;
        modulus = p & (a-1);  // (p % a) where 'a' is a power of 2
        if (modulus != 0) {
            // 'p' is not aligned, so find next higher aligned value
            p += a - modulus;
        }
        return p;
      }
      ```

- [ ] GCC (and, reportedly, clang) support some pretty advanced function
      attributes that can open the gate to more C optimizations.
      https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-access-function-attribute
      https://gcc.gnu.org/onlinedocs/gcc/Common-Function-Attributes.html#index-null_005fterminated_005fstring_005farg-function-attribute

- [ ] Fill the simd cache BACKWARDS, i.e. the simd next value of cache->next
	  should be one less (modulo CACHE_SIZE).  That way we can continue to look
	  through the cache forwards (better for CPU cache) in `cache_query` and we
	  are looking back in time from the most recently used entry.

- [ ] Replace `charsettable_add()` with e.g. `charsettable_add_charset()` which
	  will take a plain charset and return the index of the charset if it
	  exists, else a new index.  Then a separate function can be used to add a
	  SIMD charset to an existing charset table entry.

- [ ] Look in capture.c for the suggestions to memoize, which may improve
      performance (or worsen it, for patterns that have few captures).

- [X] Now that the bug in the inliner has been fixed, need to implement tail
	  recursion optimization.  See notes in codegen, inlineoptimizertest, and
	  kjv-libpexl from Wednesday, July 12, 2023.

- [ ] Gallant's observations about the benefits of streaming (versus reading
      line by line, or using `mmap`) align well with our thinking about
      designing the PEXL engine to process an input stream, dynamically finding
      "record boundaries" (which could be lines, or something else) during
      matching.
  
- [ ] Should we support a TRAINING mode where we let the user give us sample
      inputs, and we compute occurrence frequencies for all chars that are the
      targets of searches?  This would also let us set expectations of success
      or failure at key points, and we could compile patterns using this
      information.  
      - [ ] Consider the trade-offs that are made based on the expectation that
		    a match is likely versus unlikely
		    (e.g. https://blog.burntsushi.net/ripgrep/).
  
- [ ] Can we compile search patterns such that they to use 'find' only where we
      predict it will be faster?  Document the assumptions we would need to make
      about frequencies of occurrence (of BYTES not chars!), which depends on
      the input text.  See run-time idea (saturating counter) also.

- [X] MAYBE Use a saturating counter (2 bits?) in the vm to determine when to
      swap between SIMD and plain searches. [Designed with Natalie Grogan.]
      This may work better than caching results of SIMD charset searches.
	  - Requires a saturating counter.  At max value, switch to plain search.
        At min value, switch to SIMD.
	  - When doing SIMD searches, run popcnt on the 16-bit result.  If popcnt is
        above a threshold, nu (e.g. 37.5% or 50%), decrement the counter.
	  - When doing plain searches, keep track of how many bytes pass by without
        a hit.  If that exceeds another threshold, probably 1-nu, then increment
        the counter.
	  - The counter represents frequency of occurrence of this charset in the
        input. 
	  - Krisjian's experiments suggest that the measurement overhead to maintain
        the counter is significant.  Very good results are obtained by always
        starting a search byte-by-byte, then switching to SIMD if the target is
        not found within n bytes.  We are using n=16 currently (Saturday,
        January 27, 2024). 

- [X] Redesign loop (repetition) interface.  Maybe something like 
	  `pexl_repeat(C, exp, min, max)` where min and max are non-negative, and a
      max of 0 means "no maximum". See loop-notes.txt for implementation
	  thoughts. 

- [ ] Lower repetition nodes into a sequence of (1) exactly `min` repetitions
      (maybe called `DOTIMES`, after Lisp?) followed by (2) either `star` or
      `atmost max` repetitions.

- [ ] Implement a de-sugar / macro transformer
	  - Example: `{>p / .}*` ==> `find:p`
	  - Example: `{p / .}*` ==> `find:p p` which should **not** match `p` twice

- [ ] Consider starting the find/span operations on a 32-byte boundary, like the
      code below does for a 64-byte boundary for AVX2:

	```
    #include <stddef.h>
    int run_switches(const char *s, size_t n) {
      int res = 0;
      char tmp = 0;
      for (size_t i = n & 63; i--; ++s)
        tmp += (*s == 's') - (*s == 'p');
      res += tmp;
      for (n >>= 6; n--;) {
        tmp = 0;
        for (size_t i = 64; i--; ++s)
          tmp += (*s == 's') - (*s == 'p');
        res += tmp;
      }
      return res;
    }
	```	

- [ ] Integrate Gabriella's assembler (but change to sexp syntax?)
- [ ] Integrate Jack's AST code

- [X] Migrate Kiran's benchmarks for inlining (in the unit tests) into the
      benchmark suite -- Meles is writing a similar set of benchmark patterns
- [ ] Implement policies for Tristan's loop unrolling.  Ideas from Dec 2022:
      - Implement limit on size of loop body that may be unrolled
      - Ensure that loops are always unrolled when number of iterations is 1
	  - Implement iterative loop unrolling for unbounded loops (with limit)
	  - Implement recursion unrolling (with limit)

- [ ] Implement policies for inlining

- [ ] Maybe de-functionalize the tree-walking parts of the compiler? AFTER
	  finalizing the AST representation, though.

----------------------------------------------------------------------------------------

Tuesday, June 27, 2023

- [X] Make an optimization flag for SIMD.  Monday, July 10, 2023
- [ ] Add tests to ensure the other 2 combinations work:
      - VM is told NOT to use SIMD, and CPU does not support it (or not compiled in)
      - VM is told NOT to use SIMD, and CPU does support it
- [X] Store charsets as both plain text and SIMD masks in a table.
- [X] Split IFind into IFind/IFind1/IFind8
- [X] Put the charset table index into the IFind
- [X] Put the charset table index into ISet, ITestSet instructions.
- [X] Write simd_available() that returns whether or not SIMD should be used.
- [ ] Change simd_available() into a general "get configuration" function that
      returns:
	  - CPU architecture at libpexl compile time (based on preprocessor macros)
	  - OS at libpexl compile time (based on preprocessor macros)
	  - simd available in CPU (based on preprocessor macros)
	  - simd compiled into libpexl
	  - libpexl version indicator
	  
- [x] At compile time, store a "have SIMD" flag in CharsetTable.  Did this by
      setting simd_charsets to NULL if SIMD was not available when compiling.
- [X] At runtime, the vm should take a uint32_t arg containing flags.  One flag
      bit is "use SIMD if possible".  SIMD is possible if (1) the vm was
      compiled with SIMD, and (2) the byte code contains SIMD charsets.
- [ ] Decide on max input size, which determines size needed for an index into
      the input.  48 bits would be convenient, because then an index and a
      16-bit SIMD result would fit nicely together in 2 32-bit slots.  If we end
      up using 32-bit SIMD results in future, we could expand to 64-bit indices.

- [X] Decide how the simd impls of IFind should handle the last 16 bytes of the
      input.  Either we supply the missing char from the set that is needed for
      the current approach, or we do something else.
	  - An option to consider is to simply revert to a manual test where we
        build up a 16-bit result, bit by bit.
	  - While we're looking at performance in this area of the code, we should
        consider STARTING the simd searches by manually stepping through each
        byte until the input pointer is at a 16-byte boundary.  At that point,
        stepping through 16 bytes at a time is faster due to the aligned loads
        (supposedly).  Would need to do an A/B test to see.  [Done.  Saw no
        material difference between prior inelegant technique and simply calling
        the non-simd version.]

- [X] Create ISpan1, ISpan8, and ISpan out of the current ISpan
- [ ] Test ISpan1, ISpan8, and ISpan

- [ ] (from performance.txt) Create a VmState (struct) type containing a
  trampoline, backtrack stack, capture stack, and maybe a few other things.
  Pass this into the vm, where it will set the entrypoint in the trampoline and
  "reset" the other structs.  This allows the caller to reuse the VmState if
  they wish, saving several calls to malloc() and (for what it's worth) remove
  the thread-local trampoline altogether. Protocol:
  1. make a VmState and a Match
  2. pass them to one or more calls to the vm
  3. free the VmState and Match

- What to do in future when packages are AOT compiled, and some in the package
  table have SIMD and some do not?  To avoid frequent runtime checks, we could
  inspect all packages before starting the vm.  If the user wants SIMD used (via
  flag) AND all packages support it, then we invoke the vm with SIMD enabled.

- [ ] Should have two jump tables (computed GOTO arrays) for the vm.  One for
      SIMD and one for no SIMD.  Instructions that will differ between the two:
	  IFind1, IFind2, IFind8, IFind,
	  ISpan1, ISpan2, ISpan8, ISpan

- [ ] Implement IChar1, IChar2, IChar3
- [ ] Plan the UTF-16 support, specifically wrt IChar.  Do we use only IChar2,
      which matches two bytes, because this is one 16-bit code unit?


Tuesday, May 30, 2023

- [X] Re-enable LTO on MacOS for clang and gcc
- [X] Figure out how to get LTO working in Linux/x86
- [X] Figure out how to get LTO working in Linux/arm64
- [ ] Add -Wodr (One Definition Rule) if supported by GCC/clang versions we use


Sunday, December 25, 2022
Thursday, December 29, 2022

- Implement limitation on iterative loop unrolling
- Implement iterative loop unrolling for unbounded loops (with limit)
- Implement recursion unrolling (with limit)

- Compiler configuration struct:
  + Peephole optimizer (on/off)
  + Use SIMD when available (on/off)
  + Loop unrolling:
    + Iteration unroll limit, unsigned, 0 ==> do not unroll at all
    - Recursive unroll limit (same)
  - Inlining control parameters:
    - What size pattern is too large to inline?
    - Other considerations?
  - Input text encoding (ASCII, UTF-8, UTF-16LE, UTF-16BE, UTF-32, others?)

- Early results from 'findtest' across {arm64,x86} x {gcc,clang} suggest that
  memchr() is hard to beat when searching for a single character.  Here are some
  variations we might implement in 'find.c' and benchmark in 'findtest.c':
  - How far into the input must the char appear before SIMD beats memchr()?
  - When searching for a charset containing 2-8 chars, would it be faster to run
    memchr() for each char in the set, and take the nearest answer?  We'd want
    to set a bound on how far to search to make this work, and the best bound
    would seem to be related how many bytes fit into a cache line (or 2 or 3?).
  - Mark Stoodley's jit experiments with Rosie (from years ago) suggested that
    range queries perform better than charsets, which isn't surprising if we
    have just one range.  We should implement a CharRange instruction and
    benchmark it. 
    And we should try a CharRange2 instruction that tests for membership in
    either of two ranges as well.  
    And maybe CharRange3?
  - In English text encoded in ASCII, there may be a fast way to "screen" for
    common chars by first ORing the bits of the chars in the set.  If the result
    has few enough bits, then we use this as a mask while searching.  If any
    input char has bits outside the mask, it cannot be in the set.

- Implement loops for "at least n" and "at most n" repetitions
  See loop-notes.txt

- Implement SIMD for ISpan in codegen and vm

- Consider runtime adaptation for search and span operations.
  - Since SIMD searching performs WORSE than brute force when the search target
    is close to the current position, we could e.g. switch between SIMD and
    brute force depending on recent results.

- [X] libpexl.h needs to be cleaned up: (done Thursday, July 27, 2023)
      - [X] Ensure proper prefix for all functions and structure names
	  - [X] Remove convenience macros entirely or convert to functions
    

Wednesday, December 21, 2022

- The M1-build branch simplifies the build in an attempt to achieve the
  following on an M1 Mac:
  + Build with clang for x86 target
  + Build with gcc for x86 target (where gcc is x86, running in Rosetta)
  + Build with clang for native arch
  + Build with gcc for native arch (where gcc is for M1)
  + The above all work with x86 SIMD
  - The above all work but without LTO  <============== TODO!


Tuesday, November 8, 2022

- Test the use of the "special" IPartialCommit in coderep().  It is always
  generated, but when 'opt' is true, coderep() also generates a regular
  IPartialCommit.  Should that one also be "special"?  Need to ensure we test
  this path.  Related: We are considering using two different opcodes for
  regular versus special IPartialCommit instructions in PEXL.

- ALSO: See the not-yet-done items below!


Saturday, October 22, 2022

+ Finish integrating SIMD code for charset SEARCHES.

- Enhance compiler to use SIMD code for charset SPANS.

- Integrate Gabriela's ASM code, perhaps simplifying the syntax even further.

- Defunctionalize the recursive parts of the compiler to get around the current
  artificial limit on expression depth, which is set based on "typical" stack
  sizes (because we are using the C stack to recur).


Tuesday, June 14, 2022

+ Change hashtable impl to use buf.c as its block storage for keys, and store
  them in length-prefixed form.  THIS SHOULD FIX THE VMTEST FAILURE, RIGHT?  Yes.

+ Remove non-recursive environments.  All environments now allow recursive
  bindings. 

+ Write a tree walker for expressions.  Test it on print_exp.  Use it to rework
  analyzer functions like headfail, etc. ???

+ Experiment with SIMD intrinsics for find and span operations.

+ Add link-time optimization so that the "find" functions in find.o can be
  inlined into the vm.

+ Use "X macros" to instantiate instruction tables in instruction.h.

+ Write a pretty printer for the MatchTree structure.

+ Symbol table collisions can occur, e.g. in testframework.h if we name a
  pattern "ipv4" and also have a capture name of "ipv4".  When we look up "ipv4"
  in the symbol table, we find the capture name and NOT the entrypoint!

- Should not dispatch within find_simd() and span_simd() based on charset size.
  Instead, could have a proper initializer for a charset object that contains a
  pointer to the right find and the right span functions for it.  As we add
  characters to the set (and clear them!), we change these pointers as needed.
  Then at runtime, we just call the pointed-to function.  This is a case where
  "objects are useful" (though not classes or inheritance, lol).

- Unify pattern and expression structs?  Can't do it.  Issues preventing
  calculation of metadata incrementally for an expression: 
  - Calls are "open", so we have no metadata for call nodes.
  - Character encoding (UTF8, etc.) is not known until compile time,
    so we don't know the first byte of anything.

+ Create a new IBytes instruction to match a byte sequence.  The code generator
  should produce this when it would be faster (at run-time) than the equivalent
  sequence of IChar instructions.  

- Try a simple inlining policy like "inline any pattern shorter than N
  instructions".  Note that instructions with charset args will interfere with
  the goals of this policy, at least until we move charsets to their own indexed
  table. 

- Move charsets to a table of their own, taking them out of the instruction
  vector.  Make sure we have a fast way to call the right find/span functions
  for each charset, based on its size.

- Implement a "char range" instruction, which should be faster than using a
  charset when we only need to test that a byte is within a range.  This will be
  used frequently as they occur in the UTF-8 "any character", and probably in
  some other patterns as well.

- Implement a SIMD version of the "char range" instruction.

- FUTURE: Allow the ANY instruction to take an argument (reps)?


Monday, May 30, 2022

- Give the vm its own struct, containing:
  - Trampoline, with empty import table
  - Match, with a data buffer
- make_vm() and free_vm()


Thursday, November 11, 2021

See language.txt for latest language thoughts, which suggest the following next
steps:

- Remove from AST the let/letrec distinction.  Every scope is letrec.

- Rename `let` in the AST to `block`.  Concrete syntax is curly braces.

- Rename PRAGMA to DIRECTIVE.

- An expression is now a valid statement.  A block is now a valid expression.  A
  block (and a file is an implicit block) contains a sequence of items.  An item
  is a STATEMENT or a DIRECTIVE.  A statement is a BINDING, MODULE, or
  EXPRESSION.  Need to change the AST accordingly.

- TO DO LATER: With the simplifying decision that all scopes allow recursion, we
  must help the user avoid accidental recursion.  We already detect recursion,
  as part of static analysis, so we should EXPOSE STATIC ANALYSIS RESULTS.
  Let's implement an API that could be used to generate a report, for a single
  expression, indicating recursion (if mutual, then what other bindings are
  involved), min/max length, nullable/no-fail properties, first set, and
  provable runtime bounds.


Thursday, October 28, 2021

PEXL was designed to be the core of Rosie 2.0.  That suggests porting the Rosie
1.x Lua code to use libpexl (instead of the current `rpeg` code).  But we should
consider our requirements, and whether another option would be better.

First, some observations:

- A lot of the Rosie 1.x code will have to be changed.  While the PEXL API is
  not yet stable, it will likely end up at a different level of abstraction than
  lpeg.

  For example, lpeg was designed to be integrated into Lua and can therefore
  leverage Lua tables to implement pattern environments.  But lpeg has pattern
  environments only for (possibly recursive) grammars.  PEXL environments are
  implemented in C and used widely.  That is, patterns can call other patterns
  at run-time, whereas in lpeg all non-recursive calls are inlined.

  Another example of the prominent role of PEXL environments is that we have
  designed in PEXL a first-class module system and a mechanism for a proper
  prelude.

  In Rosie 1.x, the RPL compiler maintains environments in Lua.  These get
  compiled out when patterns are reified as lpeg structures (except for
  recursive patterns).

  Heavy users of lpeg have voiced concerns over the large memory required by
  lpeg patterns, and complete inlining appears to be the cause, because those
  users frequently build patterns out of other patterns.  We see this in RPL,
  where the pattern `all.things` compiles to 825,857 vm instructions, occupying
  6,606,856 bytes.  PEXL would compile `all.things` into a pattern containing
  calls to other patterns in most cases; definitions of called patterns would be
  inlined according to a policy aimed at balancing run-time performance with
  code size.

  Support for proper pattern calls and for modules argues for maintaining
  environments in the PEXL code.  Porting Rosie 1.x to libpexl, then, will
  require removing the environment structure, in favor of calling into libpexl
  for that functionality.

- The RPL macro expansion code in Rosie 1.x is fragile and overly complicated.
  We need to rewrite this anyway.

- The CLI and REPL code in Rosie 1.x could be simplified if we changed how
  literal strings were stored.  Also, having `find:` as a built-in function
  would simplify the interface between those (CLI, REPL) and the C library (rpeg
  today, libpexl in future).

- In Rosie 1.x, the line between the compiler and the runtime is not crisp.  To
  achieve the PEXL goals, we will have to restructure the Rosie 1.x Lua code
  such that these are cleanly separate: compiler, runtime, CLI/REPL.

- Rosie 1.x takes a long time to start up, as it reads a set of compiled lua
  files.  To better embed Lua, we should put this code into an object file,
  i.e. arrange for it to be a memory image.

- Today, Rosie embeds the Lua interpreter.  Should we instead require Lua or
  `luajit` as a prerequisite?  If we do this, then installing becomes more
  complicated.  And we would need to add another dimension to our system test
  matrix: the Lua version.

- What do we get if we continue to implement the RPL compiler front-end in Lua?

  - We get to keep a non-trivial amount of code from Rosie 1.x, despite that a
    significant number of changes must be made.

  - Lua itself is stable and very well implemented.  Keeping up with bug fixes
    is almost no work at all.  On the other hand, migrating to a new Lua version
    is non-trivial because the language itself does change, and backwards
    compatibility is not the highest priority for the Lua team.

  - Lua is small and fast.

  - Lua is implemented in portable C, which means that our project can be built
    in its entirety using only a C compiler.  (However, the build has two
    phases, where Lua and the Lua compiler is built in the first phase, and then
    the Lua compiler is used to byte-compiled Lua source.)

  - We had to implement modules, lists, records, and exceptions, because these
    are either missing from Lua or are there only weakly.  If we change
    languages, we should look for these features in any replacement.

  - The Lua modules we didn't write ourselves are:
      * cjson - implemented in C, with a Lua interface
      * argparse - implemented in Lua
      * readline - a Lua wrapper to system readline for the REPL
    These are easily replaced if we change languages.
    
- What do we get if we write a new Rosie 2.0 front-end in another language?

  - Assuming it's a compiled language, we won't have to embed an interpreter
    like Lua or some alternative.

  - Building Rosie 2.0 will produce a library (libpexl) and an executable that
    uses that library.  This is a win, because in Rosie 1.x, the CLI and REPL do
    not use librosie.  Interface libraries for other languages, then, must deal
    with issues that we didn't anticipate (ergonomics) or odd/incorrect behavior
    that we didn't intend (bugs).

  - It would be pleasant to work in a different language.  Lua is great, but it
    lacks many felicities of Scheme (e.g. macros, lists, symbols, modules,
    records, exceptions); nor does it have the benefits of Go or Rust (static
    typing, rich data structures, a robust module system, compiles to binary).

  - What should be the boundary between front-end and back-end, anyway?  We
    should decide this before choosing an implementation language!

    - In Rosie 1.x, the line is not well placed, because we have structures in
      Lua that mirror (different) structures in C.  This is infelicitous
      generally, but it specifically prevents us from implementing tracing well.

    - Our current approach in PEXL is to divide into: CLI/REPL, compiler,
      runtime.  The runtime is "done", but we have an incomplete compiler.  That
      is, the compiler interface is very low-level.  We don't do (many)
      tree-based transformations in C, nor any macro expansion.  But we could.
      That would leave the CLI/REPL to be written in any language.

    - It seems that we should either raise or lower the abstraction level of the
      PEXL expression building APIs.  Let's look at those options.

      - REALLY RAISE: Implement the entire compiler in C, including macro
        definition and expansion, so that the user of the compiler library can
        build an AST through direct calls, and easily compile it.

      - RAISE: Build expressions without instantiating environments.  The AST
        should include node types for `let` and `letrec` (and `package`?) that
        will later cause environments to be created.  We will need a node type
        for `ref`, and also one for `application`.

	- Advantages: Users of the compiler library (libpexlc?) can create a
          PEXL expression tree directly, without needing to represent it
          themselves.  In the compiler, we can better implement tree-level
          optimizations with such an AST.

	- Disadvantages: Where to implement macro definition and expansion?  We
          could support only a lowered AST as input to the compiler, but then
          the user must implement macros.  At some level, this situation looks
          ok, because *WE* are the primary user of the compiler library.  Which
          means that we'll wrap it with a macro expander.  But we know that
          macros need a syntactic environment, and will we be able to maintain a
          clean separation between that and the environments that the compiler
          library will reify?  This situation negates the advantage of the user
          not needing to maintain an AST -- they must, in order to do macro
          expansion, unless the compiler also provides a tree manipulation
          interface. 

      - LOWER: Simplify the compiler we have today in C.  Keep the explicit
        environment creation and reference approach.  Simplify the expression
        builder by supporting a "lowered" expression language, and redesign the
        tree representation to use a linked structure instead of a "tree stored
        in a vector".  

        - Advantages: This will shrink the API (no foo_f functions to free the
          args to the foo builder).  Also, we can simplify the compile/link code
          if we simply declare that, at this low level, there is no "recompile"
          action -- there is only "compile", i.e. make the library user build a
          new expression (starting with fresh environments).  The user will have
          an explicit AST anyway.

	- Disadvantages: The REPL will be built on top of a higher level wrapper
          around the lower level compiler.  Unless we provide some hooks, which
          may be tricky to implement, the REPL may end up recompiling more than
          it needs to when the user updates a definition.  We can live with
          this, particularly since typical REPL use for PEXL (Rosie 2.0) likely
          involves only a few definitions in the top level (user) package.  And
          a REPL session likely does not last long, the way a Scheme or Lisp one
          might. 

      - REALLY LOWER: Eliminate the compiler we have today in C.  Reimplement in
        a higher level language, producing vm bytecode for execution by the
        runtime. 

  - Conclusion: Implement the LOWER option, which will simplify some code and is
    a good thing no matter how we proceed with the rest of the compiler.  Even
    if we implement the rest of the compiler in C, this is the right approach.

  - Requirements for implementation language:
    - Stable
    - Code coverage tools
    - Profiling tools
    - Cross-platform: Linux, MacOS, and even Windows
    - Modules; data structures like lists, trees, graphs; macros

  - DECISION (tentative) as of October 31, 2021: Let's implement an AST in C,
    for two reasons.  First, we'll get an idea of just how awkward (or not) it
    is to do this in C.  Maybe it won't be too bad, and we could end up with a
    pure C project.  Second, we need to start experimenting with designs for the
    AST in order to drive changes to the existing libpexl API.


Sunday, October 24, 2021

We are now equal or better in performance to lpeg 1.0.1 (after we implemented
computed goto in the libpexl vm).  In the 'comparisons' repository, see commit
34f2367f4fb5c6a2c6c2558002b9003ad1bcbaa4 for test results.

Now the test in which lpeg is faster is "the" which searches for the string
"@the", which does not exist in the input.  Since "@" does not appear in the
input, the test spends all of its time cycling between testchar/any/jmp:

    Compiled pattern 'the' for action 'search'
    Package 0x7fa250c06170: size: 14/14 instructions
    Default entrypoint: 0
    Code vector with 14 instructions:
       0  testchar '@' - on failure JMP to 10
       2  choice - on failure JMP to 10
       4  any 
       5  char 't'
       6  char 'h'
       7  char 'e'
       8  commit and JMP to 13
      10  any 
      11  jmp to 0
      13  ret 

In the lpeg vm, ITestChar (and other opcodes) compare the current input
character to the target character BEFORE checking for end of input.  This
short-circuiting test surely saves time, since all but one comparison will not
occur at the end of the input!

But, how is it ok to dereference a pointer to a byte that is past the end of the
input?  In lpeg it is ok because the input string is obtained using
luaL_checklstring(), which uses lua_tolstring(), which returns a NULL terminated
string.  Lua strings can contain NULL bytes, but they are always stored with a
NULL terminator.  When the lpeg vm runs, it is passed a pointer that is one
beyond the last byte of the input string, i.e. it can safely dereference the end
pointer (and it will see the NULL byte if it does).

In PEXL, we do not constrain the input to be NULL terminated.  We are given a
pointer to the start of the input, and a length.  We do not dereference beyond
that length.

We might consider making a copy of the input and padding with NULLs, in order to
speed up the libpexl vm.  However, the "bad" performance (relative to lpeg)
occurs in loops like testchar/any/jmp, and our code generator could do better
than to emit such loops.  We have, on (literal) paper, a design for implementing
'find:X' (X is any pattern) such that the vm can use functions like
memchr/strchr, strspn, strcspn, and strpbrk.  NOTE: We want to pad the input so
that the SIMD instructions do not need to drop back to one char at a time.

Yes, this adds complexity to code generation and to the vm.  But we hope it will
increase performance in most use cases, by leveraging optimized (?) library
routines for searching, which are surely much faster than using 3 vm
instructions.

We expect that those string library routines will not help, and may be
detrimental to performance, in cases where the next character matches, and thus
a search is unnecessary.  When the next character would pass ITestChar, it will
likely be much faster to use ITestChar than to call, say, memchr.

It may be best to use both techniques.  First test the next character.  If it
does not match, then call a string library routine.

Perhaps, one day, we can take a cue from tracing JIT compilers (or more simply,
branch prediction) and keep statistics on how often the testchar/any/jmp loop
executes in a given set of vm invocations.  If a particular loop executes often
(as opposed to terminating in the first testchar), then we can dynamically
replace it with a library call.  But the time for this is not now.  And maybe,
never.  We may implement an actual (machine code) JIT before then!


Friday, October 22, 2021

- Can incrementally move to new backtrack stack design (see below).
  - Change BTEntry contents to store 64-bit (signed) positions
  - Change BTEntry contents to store 32-bit (unsigned) code addresses
  - Change BTEntry package index to 16 bits (unsigned)
  - Move to TAG-based system (see below)

- FUTURE: Consider prefetch intrinsics.
- FUTURE: Look at valgrind --tool=cachegrind, and --tool=massif.
- FUTURE: Look into __attribute__((hot)) for the vm.

+ GCC 10 warns about insufficient number of arguments to variadic macro, due to
  how TEST macros are written in test/unit/test.h.  Suppressing this warning
  does not appear to be straightforward.  Can we rewrite the variadic TEST_FAIL
  and TEST_FAIL_AT_LINE?


Tuesday, October 19, 2021

+ Change vm from "switch" to "computed goto" (Sunday, October 24, 2021)

- Add a test for BADOPCODE
- Add tests for corrupt code in general (try random garbage repeatedly?)

- Tuning the computed goto
  - Consider generating code where jumps to relative addresses are off by one,
    because we pre-increment the pc in DISPATCH and would like to avoid having
    to compute PC+jump_offset-1 when we could just compute PC+jump_offset.
  - When we change the vm from a switch to a computed goto, we should make sure
    to get right (wrt performance) the places where we restore the PC from the
    backtrack stack.  We should restore PC before restoring current the input
    position or the capturing status, because we want to load the PC some cycles
    before needing its value.  (We need its value when we decode the next
    instruction and compute the goto target.)


Sunday, October 17, 2021

+ What are the desired semantics of lookbehind within lookbehind?
  + Should the inner lookbehind be able to see past the leftfence of the outer
    one?  Yes.  How to make this possible, while simplifying BTStack?
  + Also, need to write a test for this BEFORE we make changes!

+ What are the desired semantics of lookbehind within lookahead?
  + Should the lookbehind be able to see past where lookahead started? YES.
  + Add test for this.

- What are the desired semantics of lookahead within lookbehind?
  - Should the lookahead be able to see past where lookbehind started? YES.
  - Add test for this.

- The length of a lookaround pattern is 0, but the target of a lookaround has a
  length, and we need to check to see if we are computing that length correctly.
  - '> "a+"' may examine an unbounded number of characters, but length is 0.
  - '> (> "a+")' same.  
  - '< (> "a+")' Does this work in the current vm on input "a"?
  - '"a" > (< "ab+")' Does this work in the current vm on input "ab"?

+ The current implementation of lookbehind checks that a match of the target
  pattern ends at the start position (where lookbehind was invoked) and FAILS if
  not.  We should make sure that the search continues in this case.
  + IMPLEMENT THIS TEST, where lookbehind at "b" fails:
    '"abc" < ("b"/"12")' on input "abc".
  + IMPLEMENT THIS TEST, where a longer lookbehind match succeeds:
    '"abc" < ("b"/"abc")' on input "abc".


Saturday, October 16, 2021

- Rework libpexl.h
  - It should contain only what is needed for external programs like the
    benchmark example, kjv-libpexl.c.
  - The function declarations should be usable for static linking, dynamic
    linking, and dynamic LOADING (where we need function pointers).

- Strategy for reusable storage for capture stack, backtrack stack, and
  trampoline: These will be aggregated into a vm "working memory" structure that
  will be passed to the vm.  The user can simply not supply this object, in
  which case the vm will allocate those objects and free them.  If the user
  supplies this "working memory" object, the vm will reset and reused each of
  its constituent items.  We will provide an API to free this structure.

- Write a vmtest that causes the backtrack stack to expand.

- How much can we shrink the size of a backtrack stack frame?  Currently it
  (look like it) is 32 bytes because the enum, while only 8 bits, surely
  occupies 8 bytes so that a frame can be 8-aligned.  (Remember that the frame
  contents always contain at least one pointer.)

  - The BTBackLoop contents has 3 pointers.  We could remove the leftfence field
    by moving it to another frame, i.e. have ILookBehind push two frames.  One
    frame would be a BTLookAround frame containing boi/eoi.  We would use boi as
    our leftfence.  The other frame would contain the next search position and
    the address of the loop.

  - The BTLookAround contents could be reduced to 16 bytes if we encode the
    capturing flag in the low bit of one of the pointers.

  - BTChoice contents is a problem.  With 8-alignment, it consumes 24 bytes
    currently, and all fields are needed.  We can get this down to 16 bytes if
    we replace a pointer with a 32-bit index, e.g. the PC.

  - BTCall contents, with PC changed to an index, could be 8 bytes, because
    Package could be an index into the package table instead of a pointer.  The
    package index could also encode that this is a BTCall frame, so that we can
    eliminate the frametype enum.

  - Related, a capture stack entry is currently 16 bytes and is 8-aligned
    because it starts with a pointer into the input.  For consistency with the
    BT stack, we could store the capture position as an index.  But as the entry
    size would remain 16 bytes, it's not clear we gain anything from that
    change.  It's possible, though, that avoiding storing pointers is a good
    thing.  Maybe the C compiler can generate more efficient code in some places
    because it won't need to worry about pointer aliasing?  Maybe in future we
    will support a use case where the vm can, engine-like, return a continuation
    that can be invoked with additional input (in a new string buffer)?  In that
    case, and if we want to support streaming, we will be happier if we store
    offsets, because offsets can accumulate across re-entries into the vm.

  OPEN QUESTION:  Will using indices instead of pointers slow down the vm enough
  that any benefits of halving the BT frame size will be lost?

  One possible design
  -------------------
  8-byte frames

  Sizes: Addr = 4 bytes        // index into code vector
         Captop = 3 bytes      // index into capture stack
         Accum = 2 bytes       // loop counter
         Pkg = 2 bytes         // index into package table
         Position = 8 bytes    // index into input
	 Capturing = 1 BIT

  Type  #Frames Contents
  ------ ------ -----------------------------------------------------
  Choice     2: TAG (1), Captop (3), Addr (4), Position (8)
  Call       1: TAG (1), PAD (3), Addr (4)
  XCall      1: TAG (1), PAD (1), Pkg (2), Addr (4)
  LookAhead  3: TAG (1), PAD + capturing (7), BOI (8), EOI (8)
  LookBehind 3: TAG (1), PAD + capturing (7), BOI (8), EOI (8)
  BackLoop   2: TAG (1), PAD (1), Counter (2), Addr (4), Position (8)
  DoTimes    2: TAG (1), Captop (3), Accum (4), Position (8)
  Find       2: TAG (1), Captop (3), Addr (4), Position (8)

  Examine high byte of first 8-byte value:
    0000_0000 => Choice
    0000_0001 => BackLoop
    0000_0010 => Find (not yet implemented)
    0000_0011 => DoTimes (not yet implemented)
    1000_0000 => LookAhead
    1000_0001 => LookBehind
    1000_0010 => Call
    1000_0011 => XCall

  UNWIND_STOP == high bit of TAG is 0
  LOOKAROUND == TAG is 1000_000x
  IRet (Call/XCall) == TAG is 1000_001x

Monday, October 11, 2021

+ Currently, frequently used vm variables are kept in the vmstate struct on the
  stack.  We should see if moving those into "register" variables improves the
  speed of patterns like "omega" that are mostly loops of ITestSet / IAny /
  IJmp, i.e. the vast majority of executed instructions touch neither the
  backtrack nor capture stacks.

+ Change leftover to "last matched input position + 1" to be more like other
  libraries, where that index is zero-based and will be equal to the input
  string length when matching consumes the entire input.

+ Remove matched flag, because we can use match->end = -1 for that.
  + Change benchmark accordingly

+ Do not generate captures if there are no captures
  + Change vm.c to not call the capture encoder when zero captures
  + Change test programs to look only for last match position, not captures
  + Change benchmark program kjv-libpexl to use last match index when not capturing


Saturday, October 9, 2021

+ Streamline the use of the backtrack stack
  + Stack operations currently require two copies of the data that goes into a
    frame: We first copy into a BTEntry by assigning each struct member; then
    the struct is copied onto the stack.
    + The latter step looks like it compiles into memmove (which shows up in the
      flame graph).  This itself is slower than memcpy, which the C compiler
      decided it could not use in case the source and destinations overlap, even
      though they won't. 
    + A better approach would be to advance the stack pointer first (expanding
      the stack if needed) and then copy values directly into the newly
      allocated frame -- for a total of one copy.
  + Ensure we are only pushing what we need to
    + Do Call and Choice need to save Package?  ONLY CALL DOES.
    + Check the size of a BTEntry, rearrange as needed.  24 bytes, all necessary.
  X Maybe use a bit in cap_idx to indicate "not capturing"
    - Would avoid an assignment on each of pop/push
    - Cost is ensure bit is clear when capturing
  X Should we move to a variable-sized frame?
  + Change the BT frametype enum so that we can check a bit to unwind
  
- Now that we are profiling, we should add dynamic instrumentation to the vm
  - Count each instruction type
  - Keep high water mark for the two stacks
  - If enabled, maybe just print them when the vm exits, along with some
    stats about the returned capture?  This avoids returning all that data. 
    
- See iCloud notes on implementing 'find:P'.


Thursday, October 7, 2021

- Decide on the precise role of the expression structures.

  * They are currently standing in for an AST, because we have no higher level
    abstractions yet.  But there is no "new scope" node.  Instead, we require
    expression nodes to be constructed in the context of an environment.
    I.e. an environment must be instantiated first, in order to generate
    references that can be used to build more expressions.

  * They are somewhat low-level, concerned with efficiency as demonstrated by
    the way expressions are combined -- by copying into a singly allocated
    result structure, instead of simply linking nodes with pointers (as would be
    typical for an AST).

  * The interface is somewhat high-level, however, as can be seen in the repeat
    API.  First, it is a single API for creating both "at most" and "at least"
    repetitions.  Second, the implementation does not generate a repeat node,
    but instead a sequence of n copies of the target expression (for "at least
    n") or a sequence of n choices using the target expression (for "at most").
    

Sunday, September 26, 2021

- Write a tree printer for tree.c, for human reading of tree output
- Write tree_to_sexp for tree.c, which should help with testing
- Re-enable lookbehind tests in various test programs

- Check external symbols to ensure all have a prefix of 'pexl[erc]_'


Saturday, March 6, 2021

- Idea: Have make_context create a top-level environment, and have that be the
  only way to create a top-level environment.  Allow setting the recursive flag
  to either value, for top-level and other environments.
  - This eliminates the need to create an environment BEFORE calling
    make_context.
  - And it eliminates the need for free_env, because free_context can be made to
    free all the envs associated with that context.  (We will need to keep a
    list.)

- Decide what to do about the print routines.
  - Keep in lib and use a pexlp prefix, perhaps?
  - Split out the print functions that are only for compilation structures
    so that they will not be included in the runtime lib

+ Finish implementing lookbehind

+ Finish the capture interface

+ Can we limit instruction vector size to 2^24 so that we can use 24 bits aux
  field for addresses instead of an additional 32 bit word?  YES -- already
  doing this (see MAX_INSTRUCTIONS in instruction.h).

- Implement memcmp instruction to compare input against a null-terminated string
  stored in the string table

- The patlen types are uint32_t currently.  Consider limiting these values to 24
  bits and using int32_t types to pass them around.

- Add APIs for:
  - Analyze a pattern, returning constant, linear, super-linear
  - Analyze and return the usual things (length, captures, nofail, nullable, ...)
  - Look-ahead or look-behind that are guaranteed constant-time

+ Decide on a "logging level" design; convert existing logging to it

Sunday, February 7, 2021

+ Change to BSD 3-clause license: https://opensource.org/licenses/BSD-3-Clause
+ Rename the _t type names to not use _t
+ Change to C99?
+ Create an ARCHITECTURE.md, inspired by https://matklad.github.io//2021/02/06/ARCHITECTURE.md.html
+ Make the repository public

+ Finalize the libpexl API, as much as is possible at this stage of development
  + Expression constructors
  + Compile and link, in memory
  - File i/o for compiled/linked patterns
  - Callbacks to support compiler front-ends and module loading


Tuesday, December 22, 2020

- API ideas inspired by Rust:

  X Combinators like `seq` and `not` can be provided in two forms.  The first
    takes ownership of its args.  E.g. `not(e)` would "consume" `e` so the
    caller does not have to free `e` -- but also the caller cannot re-use `e`.
    The second form explicitly copies `e`, so that the caller can re-use `e`.
    Perhaps that form would be denoted `not_(e)` or `not_borrow(e)`.

  + Similar to above, but the usual operators copy their args, so that the
    caller must free them, e.g. the caller must free `e` after `not(e)`.
    But a variant, `not_f(e)` takes ownership of e.

  - The `match` interface can be provided in two forms.  In both forms, the
    caller retains ownership of the input buffer.  In one form, `match` borrows
    the buffer as immutable and pledges not to change it.  In the other form,
    `match` borrows the buffer as mutable, and can e.g. replace the last byte
    with \0 in order to avoid end-of-buffer checks except at \0 bytes.  (This is
    reportedly a performance win, but we have not tested it.)  We assume that
    `match` will restore the buffer before returning, because the cost to do so
    is negligible.

- TODO: Tuesday, December 22, 2020
  (1) Add optimization back into lookahead
  (2) Examine TNot again
  (3) Add optimizations to lookbehind
  (4) Consider a negative lookbehind API
  (5) Differentiate bounded vs unbounded in the API

X Determine whether we need to COPY sub-expressions passed to combinators like
  exp_seq()... We need to, and we do.  (Do we test this?)

X Running fix_open_calls() should be done on a COPY of the expression.

+ The result of fix_open_calls() is a Pattern structure containing actual C
  pointers to other Pattern structures.  Deleting a binding or rebinding will
  NOT trigger free() on the Pattern value.  When (and how) can we free a
  Pattern?

  + (1) Suppose we did NOT actually fix the open call, but simply "validated" it
    for proper scope and type (including metadata)?  Can also correct
    associativity and such, producing a COPY of the original expression tree.
    At link time, we fix the calls by following the references, and assume no
    bindings have changed?

    ** What we actually did was to COPY the expression tree and run
       fix_open_calls on the copy.  By doing this, (1) we retain the original
       expression tree in case we need to re-compile later, after the user
       changes some things in the environment.  And (2) we can leave codegen
       mostly unchanged, in that each call target remains a Pattern *.

  + (2) Remember that we have the complete control flow graph with its SCCs!  We
    can use it to plan where the entrypoint addresses will be, except in cases
    of mutual recursion.  That will let us generate code directly into the
    output (code vector) IN THE RIGHT PLACE almost all of the time.

  + (3) codegen:
	case call(ref):
	  follow ref;
	  if value at ref has a symtab-idx set, then
	    emit CALL to symtab[symtab-idx].value 
	  else
	     add binding to symbol table by name or anon, giving idx
	     cache idx in the symtab-idx field of the binding's value
	     generate an indirect instruction 'CALLSYM idx'
	     queue the location of this instruction for later fixup
	case xcall(xref):
	  IF static linking:
	       emit XCALL to (package-number, symtab-idx)
	       mark target package for inclusion in the output, in case it's needed
	  IF dynamic linking:
	       emit XCALL to (package-number, symtab-idx)
	       put importpath and prefix into symbol table
	       generate package table entry for inclusion in the output

    ** What we actually did was to leave alone the Pattern pointer in the call
       node.  So the above is actually `case call(pat)`.

    ** We have not implemented xcall yet.

  + (4)fixup: process the queue of fixup locations in the output (code vector)


X Code generation should be done at what we've been calling link time? YES
  The algorithm above uses the environment to cache the addresses of bindings in
  the code vector.  Thus, no bindings can change while code generation and
  linking are occurring.  Also, compilation relies on the pattern metadata which
  is also cached in bindings.  Any binding change could potentially invalidate
  compilation results.  But, this cannot happen:
    Our current approach compiles everything needed to use a pattern or an
    entire collection of patterns.  Suppose we compile patterns A and B,
    producing a code vector that we use for matching tasks.  Then we modify B,
    and compile B into a new code vector.  If A depended on B, then A's cached
    metadata is now invalid, because B changed after that metadata was
    calculated.  The old code vector contains the old A (with the old B) --
    which is correct!  And if we want a new code vector with A, we will compile
    BOTH A and its dependency B, and get a new version of A -- also correct!

+ Ensure that recursion is allowed only in recursive environments.  And:
  env_new(parent, recursive_flag)    /* parent can be NULL */

+ Change Package to remove environment(s)

+ The two linking functions needed are:
    pkg = link-environment(top-level)
    pkg = link-expression(val)

+ Need string block storage to intern strings.  Can use hashtable for this.

+ HOW TO REFER TO A NAME in an xcall node?  NEED block storage for strings,
  i.e. a place to store strings that can later be an input to the linker.  WE
  DON'T WANT A char pointer IN A NODE FIELD, because we'd have to find it and
  free it just to free an exp, whereas today it is simple to free an exp.
  + Expressions now have a hash table, so we can quickly look up strings during
    the analysis phases as well as later during code generation, when we can
    add each string that is actually used into the package's symbol table. 

X Create an Environment type which is a discriminated union between a top-level
  env and a child (ordinary) env: Add a flag to Env to say if it is top-level or
  not.  If YES then the parent field points to the prelude (if any).
  + Delay the implementation of prelude, which should be a PACKAGE not env
  + Top-level env needs a hash table to hold strings
  
+ Create an 'xcall' node that refers to a name and a package.

+ How to refer to a capture name string in an instruction?
  + Index into string storage block for that package's symbol table?
  X Index into that package's symbol table?
  
- Devise a way for walk_captures to access the string storage for each package.
  X Could coalesce all strings in package table into a single string block.
    This can be done at link time.
    But it means changing how capture names are stored in instructions, right?
  X Could have a capture table entry that means "change current package to XYZ"
    What about when there are no captures while we are executing code from XYZ?
  + More straightforward is to store the current package number in each capture,
    and then also pass the package table to the output encoders.
  + The byte-coded output encoders MUST TAKE PREFIX INTO ACCOUNT!
  - The output encoders need access to the prefix assigned by the CALLER
    - The calling package must have a map taking each package it calls to the
      string used as a prefix, if any.  Let's use PRE(P, Q) to denote the prefix
      that package P uses when calling a pattern in package Q.
    - Because the calling package is a dynamic quantity with respect to the
      generated code for the package we are calling, we can set a "prefix
      register" in the vm that contains the prefix to use for captures.  Better,
      we can find the calling package on the top of the backtrack stack!
    - In package P, the instruction XCall(Q, entrypoint) will set the prefix
      register to PRE(P, Q). The Return instruction will restore the prefix
      register to its prior value.
    - Each OpenCapture frame needs to hold the prefix (an offset in the string
      storage for the current package) as well as its current contents (input
      position, current package and offset, capture type).
    - CONSIDER using a struct for the saved/restored registers, and memcpy to do
      the saving and restoring.
    - To SUM UP this approach:
      - Each package needs a list of its imports anyway, so implement this now
        by extending the package table to include the prefix used by the user
	package. 
      - When generating code for package P, the package numbers for its imports
        are determined by its own package table:
	  (importpath, prefix, code, pkg table, pkg table num, <other meta data>)
      - How to load a package P:
          - If importpath of P is in the machine's package table, we have it.
	  - Else:
	  - Find it by importpath.
	  - Read its package table and recursively determine if its dependencies
            can be found.
	  - If they can, then we have them in topologically sorted order based
	    on the way we collected them, so process them from last to first so
  	    that we start with the ones with no new dependencies.
	  - Add each package P_i to the machine's package table by:
	  -   Allocating the next slot to P_i.
	  -   Iterating through the entries Q_j in the package table of P_i:
	  -     Find Q_j in the machine's package table in some slot k.
	  -     In the package table for P_i, set the pkg table num for Q_j to k.
	  - Now package P_i is in the following state:  Its code has references
            to its own pkg table, e.g. to entry j in order to call into Q_j.  If
      	    we were implementing dynamic linking, then we would implement XCall
  	    by jumping indirectly through the local package table (optimized
  	    with memoization).  But for static linking, we can go ahead and
            alter each XCall(j, m) in P_i to refer instead to the global package
            table by using the map we stored when we loaded P_i.
	    - To sum that up:
	    - For STATIC LINKING, loading the package creates a map from the
  	      local package table used during code generation to the current
  	      global one.  Traverse the code vector, updating the code according
 	      to this map.
	      OR we could jump indirectly through a local package table, using
  	      it like a PLT:
	      XCall(pkg_number, entrypoint) would be implemented using
	        pkg = local_pkg_table[pkg_number].address
	      Perhaps we should start with this and measure its cost?
	    - If we ever implement DYNAMIC LINKING, then the XCall instruction
	      for a dynamically linked package behaves differently than an XCall
  	      in statically linked code.  It indexes in the local package table
  	      using the package number in the XCall instruction.  It finds there
  	      the global package number, then proceeds as usual.  If the code
	      vector is mutable, we can update the instruction in place as long
  	      as we can tag it as updated.  If the code is immutable (e.g. in a
	      shared read-only mmap region) we can continue to use the indirect
	      jump (similar to how PLT is used in dynamic linking of traditional
	      programs). 
	    - IF WE DECIDE that XCall takes the symbol table entry and not the
	      entrypoint address as its second argument.  Caching the ultimate
	      destination address may be advantageous in both cases (static and
	      dynamic)   We'd need to map a pair of small integers (pkg number,
	      symbol number) to a package pointer and an entrypoint (pointer
	      into the code vector).

     To match using the symbol P.foo:
     - Recursively import package P
       - If it's importpath appears in the global package table, then done.
       - If a compiled package exists:
         - Check that it matches the corresponding source
	 - If it is out of date, compile it
       	 - Load it into the global package table
     - Compile the code for P
     - Save the package P (anonymous or named)       
     - Use the in-memory P for matching

- ALTERNATIVE approach to solving the previous bullet, the requirement that
  while walking the captures, we have access to the prefix used in package P for
  xcalls to package Q:
  - This also addresses dynamic linking of shared pattern libraries, wherein we
    need to check somehow that the package Q' we are loading is the same as OR
    COMPATIBLE WITH the package Q that we compiled against.
  - First, we keep (conceptually) two symbol tables: one for the symbols we
    provide (both for EXPORT and for local use) and one for the symbols we
    IMPORT from other packages.  The first symbol table looks like the one we
    already have.  The new one, for imports, is organized into stanzas, each
    starting with a symbol of type IMPORT:
      <importpath>  type=IMPORT   <prefix>
      <symbol>      type=PATTERN  <fully_qualified_name> <metadata>
      <symbol>      type=STRING   <fully_qualified_name> <metadata>
      <symbol>      type=INT32    <fully_qualified_name> <metadata>
      <symbol>      type=FUNCTION <fully_qualified_name> <metadata>
      <symbol>      type=MACRO    <fully_qualified_name> <metadata>
  - The set of symbol entries in the stanza will be in SORTED order.
  - The capture for an XCall should appear in the caller's code, where the
    Capture instruction can refer to the fully qualified name of the pattern
    that we are about to call, e.g. `net.email`.  Note that the symbol name
    field in the table can point to the `email` suffix of this string.
  - When compiling a package Q, we create a symbol table containing Q's exports
    (and local names) along with pattern metadata as we do now.
  - Later, when some package P performs an import of Q, we start creating a
    stanza following the template above: we note the importpath and prefix.
  - While compiling P, each reference to Q (today, only to patterns via XCall)
    leverages pattern metadata from Q's "export symbol table".  When we look up
    a symbol in Q's export table, we create (if not already there) a
    corresponding entry in P's import table, in the stanza for Q.
  - Later, we can execute the code in P by dynamically loading a compiled
    version of Q.  For each symbol that P imports from Q, we compare the type
    and metadata stored in P's import table with that in Q's export table (easy
    if both tables are sorted and we use memcmp on the metadata structures).
  - If the types and metadata match, we know that the package we imported, while
    it may not be the original Q, is at least some Q' that is COMPATIBLE with
    Q.  This is rather like ABI compatibility in shared libraries at the OS
    level.
  - We can also implement a "quick check" to know that the Q we imported is
    actually the one we compiled against.  If the quick check fails, we can
    check for compatibility.  E.g. we can store a signature of some kind in each
    compiled package Q, and keep a copy in each package P that is compiled
    against Q.  Simply recompiling a package and all its dependencies will
    ensure that the "quick check" will be used.  This would be the process to
    follow before archiving a set of compiled pattern files for deployment.
  - During the compatibility check, we can prepare a run-time structure for use
    in execution (matching):
      Currently, each XCall(pkgnum, symidx) in the code of P has:
      - pkgnum, which refers to the index of, say Q, in the MACHINE package
        table, whereas it should refer to a structure relative to P.
      - symidx, which is an index into Q's symbol table AT THE TIME P IS
        COMPILED, whereas we would like to support dynamic linking against a
	version of Q with a different (but "compatible") symbol table.
      So we must change how XCall is compiled.
      Can we parameterize each XCall with a single number, the index into the
      Imported Symbols table that we will soon have?
      Yes, but we intend to sort the symbols before generating the final symbol
      table, so we'll have to produce a map while sorting and then update all
      the XCall instructions in the code.

      At runtime, we encounter XCall(j) and we want it to be easy to determine
      the new package and entrypoint from j.  A lookup table seems appropriate:
         LINKAGE TABLE: entry j --> (Package, Entrypoint)
      While we are loading Package and checking its compatibility with our code,
      we can fill in such a table as follows:
      - The imported symbols table for P is in sorted order, as is the exported
        symbols table of Q (the package we are loading and checking).
      - We iterate simultaneously through both tables, and for each symbol that
        is in both, we do this:
        - j is the iterator index in P's import table; let k be the index in Q
	- fill in LINKAGE[j] = (address of Q, entrypoint from export table at k)

     When we encounter XCall(j) at runtime, we do this:
       current package <-- LINKAGE(P, j)
       jump to LINKAGE(P, j)
     Here we emphasize that each package has its own run-time linkage table.
       
     NOTE: It looks simple enough to implement this dynamic linking/loading
     approach first, and add static linking later, only if needed.  A statically
     linked package could be made from the run-time image of a dynamically
     linked set of packages.  We can "simply" persist the packages and the
     linkage tables, and provide a file header describing the file contents.
     Or, we could go further and consolidate all the code into one instruction
     vector (omitting unused patterns), scrap the imported symbol table (not
     needed), and retain the exported symbol table for only the "top level" or
     "user" package.

     DURING CODE GENERATION: The TXCall tree node has a pointer to package Q and
     an index into Q's symbol table.  We will need to:
     (1) look up Q in P's imported packages table, and record its entry index
     (2) create an entry in P's imported symbols table; in the entry, store the
         entry index of Q in P's imported packages table
     (3) in the generated code, insert the entry index into P's imported symbol
         table

     AFTER CODE GENERATION: Construct the actual (final?) imported symbol table
     for P by first sorting the compile-time imported symbol table, first by
     package number and then by symbol name, WHILE building a map from each
     symbol's old position to its new position.  (This is easy to do: simply
     sort a vector of position numbers [0, .. n-1] by the information in the
     symbol table.  Call the resulting permutation of indices the 'mapping'.)

     Using both the imported packages table (already in package number order,
     because it defines that order) and the sorted index into the imported
     symbol table, do two things: (1) generate the final imported symbol table,
     and (2) update every XCall(j) instruction to be XCall(mapping[j]).

     Finally, we must sort the exported symbols of P.  Again we create a mapping
     from old position to new position.  Again we fix up all instructions that
     refer to a symbol table entry (captures).
     
     To facilitate captures that go with XCalls, we can do two things:

     (1) Have a capture instruction specifically for XCalls.  XCapture(i) will
         get its capture name from the exported symbol table, at index i.
     (2) We can implement a peephole optimizer that looks for sequences like:
           XCapture(i); XCall(i); CloseCapture
	     -or-
           Capture(i); Call(j); CloseCapture
         and convert the first into a single call to a new instruction XCCall(i).
         and the second into a single call to a new instruction CCall(i, j).

      The reason to do this in an optimizer (as opposed to during code
      generation) is to allow the XCall'd pattern to be inlined during an
      earlier optimization stage.  Any XCall that was not inlined would then be
      handled by the peephole step.  Generally, we cannot inline across package
      boundaries if we want to support the notion of "compatible packages" as
      described earlier.  However, we may drop the notion of compatibility in
      favor of ensuring that at run-time we load exactly the package we compiled
      against.  Or, we may find opportunities to dynamically optimize across
      package boundaries at run-time.  E.g. if XCall(j) refers to a
      single-instruction pattern (plus a return), then we can replace the
      XCall(j) instruction with its implementation.  That is, as long as we have
      a writable code vector, which is not a given (because we may want to share
      patterns in memory with multiple processes via read-only mmap).


  - To implement this approach, we need the following while compiling P:
      - A dictionary (hashtable) of packages we are importing
        Key: importpath, data: prefix, table of imported symbols
      - Tables of imported symbols (another dictionary; key: name, data: maybe
        can reuse the symbol table entry structure)
      - String sort (UTF-8?)
      - Generate sorted symbol table for P's exports after compilation; fixup
        any references to symbol table indices... see above.

- DYNAMIC LINKING: During linking/loading, each package's code symbol table and
  code vector ends up at some place in memory.  Our vm's "package table" is like
  the Global Offset Table in an ELF binary, in that we use it to hold the
  address for each package.

  Every XCall in a code vector is currently parameterized by: package number
  (from compilation time) and symbol table entry index (from compilation time).
  These were stop-gap (temporary) choices, and now they must change.

  *** NEW APPROACH:

  * Change XCall into a 1-word instruction in which the 24-bit offset is an
    index into a symbol table holding external symbols.

    An entry in this table holds: importpath, symbolname, pattern metadata.  For
    efficient insertion, should make this a hash table by hashing the index pair
    (importpath, symbolname).

  * Build the symbol table of external symbols during code generation.
  
  * To link/load, we iterate through the external symbol table, ensuring that
    (1) the referenced package is loaded; (2) the symbol name is found; (3) the
    symbol's type is compatible.

    Keep a topologically sorted list of imports?  But it can be out of date.
    E.g. package P will do its import in order Z, Y, X, because X requires Y and
    Y requires Z.  What happens if we load P and then find out when loading Z
    that it needs Y (a reversal of what was the case when we compiled P)?  We
    would load Y, finish loading Z, then continue on the list.  Next on the list
    is Y, which is already loaded, and then X which imports Y, which again is
    already loaded.  Since we cannot count on the topological ordering being
    correct, we will need to code for out of order dependencies AND for
    circularity detection.
    -OR-
    Read the dependencies from all of the packages until either done or a
    circularity is detected.  The latter is in an error.  The former can be
    topologically sorted and then the packages loaded in that order.

  * Once all externals are resolved, we no longer need the external symbol
    table.  So, while we are resolving externals, we can fill in a new table
    that uses the same index.

    The new table, indexed by the immediate parameter in an XCall instruction,
    contains a package pointer and a void pointer.

  * The void pointer is either to the code (for a pattern symbol) or to data
    storage (for, e.g. a string or integer value).  In all cases, the pointer
    is to read-only memory.

  * At run-time, the vm will process IXCall by using its immediate operand (the
    24-bit "offset" field in the instruction) to index into the external symbols
    array, where it finds the new PACKAGE (a pointer to a Package struct) and
    the new PC value (a pointer into a code vector).



      
    
- CONCRETE SYNTAX CONSIDERATIONS for RPL 2.0
  - allow lispy identifiers (e.g. containing dashes and other chars) but not
    using star or plus or question mark 
  - provide distinct operators for UNBOUNDED look-around, perhaps:
       >>x   unbounded lookahead
       >>!x  unbounded negative lookahead
       <<x   unbounded lookbehind
       <<!x  unbounded negative lookbehind
  - eliminate the automatic tokenization in favor of using ~ and tokenize:()
  - single quoted strings for non-interpolated strings, e.g. '\n' has 2 chars
  - implement the in:(p1, p2) idea where match of p1 is the input to p2
  - make search the default unless ^..$ are used?
  - can support Kleene star if we want to, with several possible syntaxes:
       x*K could mean x* where * is the non-possessive Kleene star
       x** is another possible syntax, or x*' or x{*} or something, e.g.
         a** a, where a="a  ==  /a*a/  ==  A, where A = "a" A / "a" in RPL 1
         the double star mirrors << and >> and suggests ++ and ?? as well.
	 all are expensive.
       K*:() or similar could leverage the application syntax
         and function syntax has a nice benefit if it is a binary function where
	 the second arg is the continuation -- nesting becomes obvious, e.g.
	 K*:(., a K*:(.)) == /.*a.*/
  - MAYBE have a regex-like embedded syntax, if we can solve these issues:
       /ab|c+d*/              but would this be atomic+possessive?
       net.ip /:\d{1,4}/      mixing could be problematic due to / overloading
       net.ip //:\d{1,4}//    double slashes are an echo of <<, >>, ** etc.
       /{net.ip}\:\d{1,4}/    should reverse embedding be allowed?
  - MAYBE expand identifier chars to include \ -
  - MAYBE allow new unary and binary operators to be defined
  - we could support "run-time captures" of some sort (femtolisp?)
  - we could support unordered choice by implementing call/cc and using the
    continuation-based conversion of unordered choice to PEG, using our
    capture-preserving extension to that approach.
  
- What should be a good LIMIT on the size of a string literal that is part of a
  pattern?  Use cases that require long strings and nothing else would be best
  implemented by a technique like Aho-Corasick, or even KMP/BM if there's just
  one string.  What is a use case that needs both long strings AND pattern
  operations beyond just choice?  Lacking any, we might set a limit on a single
  long string at something small, like 1K or 64K.

- OTHER CONSIDERATIONS for RPL 2.0
  - Discard the PEG convention of matching a prefix of the input
    - Support match and search
    - Already have ^ and $ for use as needed

    
- SOLVE this use case: We want to extract source lines of code from a C program
  by skipping comments and white space.  Matching comments and blank lines is
  easy, e.g. 

    rosie match -w -o subs 'findall:{{"/*" find:"*/"}/"\n"[ \t]*>"\n"}' analyze.h

  But how to invert this match?  It's not obvious how to rewrite this pattern,
  and it's also not obvious that a command line option to invert the match (like
  grep's "-v") has an obvious meaning for each output encoder, like "-o subs"
  used in this example.

  (Note that a different pattern could be written to match only the source
  lines, but it seems to be a valid development approach to first write a
  pattern to match the easier part of the problem -- i.e. to match the lines we
  want to ignore -- and then invert the solution.)

- Convert code.h, expression.h to use X macros.

- Move print.c to compiler dir.  Have another print.c each in runtime and
  common.  Same for errors.h.

- Optimization for vm_start(): If the output encoder does not need captures,
  then we should not bother to keep them!

X A capture optimization: Can we generate the byte-encoded representation DURING
  MATCHING in the vm?  Then, when this is the output encoding we want,
  walk_captures() is a no-op.
    We did not implement this because encoding positions into strings is work
    that we want to do only for the captures that are still there at the end of
    the matching process.  We know that the capture list can grow and shrink a
    lot during matching due to backtracking, so why do the extra encoding work
    for captures we will throw away?

+ A capture frame (i.e. a single Open or Close entry) should contain an index
  into the package table and the offset of the string in that package's symbol
  table's block storage.

+ Simplify the expression types down to Epattern_t and Eunspecified_t for now

X From the Node type:
    /* TODO: Remove pat, because we will not need it */
    struct Pattern *pat;	/* for TCall */

+ From the Node type:
    /* TODO: Change capname into an INDEX (hashtable? string block?) */
    const char     *capname;	/* for TCapture */
