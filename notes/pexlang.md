# PEXLang, a Parsing Expression Language

<br>Monday, November 7, 2022
<br>Jamie A. Jennings
<br>

## Highest priority goals

### Asymptotic performance

* Linear time and constant space (excluding captures) for as many (classes of)
  patterns as possible.
  
* Linear time and linear space, when needed, for as many other (classes of)
  patterns as possible, such as all recursive patterns.
  
* Option to avoid linear space in favor of polytime backtracking.

### Actual performance

* Faster than lpeg and Rosie
* Consume less memory than lpeg and Rosie.
* Competitive with commonly used regex implementations (excluding a few that are
  trade off functionality for high performance).

### Usability

* Composability is paramount.

* Ease of sharing collections of patterns is extremely important to get right.
  (OCaml-style parameterized modules?  Rosie-style simplicity?  Dynamic pattern
  variables for when the default static scope is not the right choice?)

* Embrace the speculation that few declarative languages have been successful,
  and design a pattern language with an imperative/functional flavor,
  i.e. mostly functional but with sequencing to reflect the way humans probably
  think about textual pattern matching ("match this, then that").
  
### Broad applicability

* Use "in the large", i.e. suited to teams of developers, large pattern
  collections, projects that are maintained and enhanced over time, pattern
  libraries that are maintained by third parties.
  
* Fully embrace Unicode, particularly UTF-8 and UTF-16.


## Language design points

### Assume a mix of valid Unicode and random bytes

	<char> is a Unicode codepoint, suitably encoded
    <byte> is a single byte
	<bytes> := (<char> | <byte>)*

    <exp> := ...

### We'll have "possessive regex" operators

	(quote <bytes>)         // literal match of <bytes>
	(begin <exp>*)          // sequence
	(choice <exp>*)         // "atomic" alternation
	(star <exp>*)           // "atomic" Kleene star (implicit begin)
	(n-of n <exp>*)         // exactly n repetitions (implicit begin)
	(at-most n <exp>*)      // limited repetitions (implicit begin)

### And supplement them with non-traditional operators

We need to take the match/search distinction and bring it into the language
(unlike regex), as we did with RPL.  At run-time, we can do a little bit of
compiling when needed, for example:

* User has a `find` expression and wants to match it starting at the beginning of
  the input.  On the fly, we replace `find` with `begin`.

* User has an expression that does not start with `find`, but they want to
  search for this pattern.  On the fly, we wrap the pattern in `find`.  If every
  expression starts with `begin`, this is the merely the reverse of the other
  swap.

<br>

    (find <exp>*)           // skip ahead until <exp>* succeed (implicit begin)

Addressing captures well is daunting.  As I've written elsewhere, a useful
mental model is that every named pattern is captured, although the user can
specify excisions from the match tree, in order to simplify (shrink) the
output. 

In RPL, we have `alias p = ...` which tells the compiler not to emit a capture
instruction for the target pattern, `p`.  The sub-matches of `p` will, however,
appear in the output.  There is no mechanism to suppress those, without altering
the definitions of all constituent patterns of `p` to also be aliases.

Another issue, though minor, with the RPL `alias` is that an aliased name cannot
be the target of a back-reference, because the name does not appear in the
capture list (the match tree).

I propose that we try to implement the mental model in which every named pattern
is captured during the matching process.  In this model, after matching is
complete, the tree is altered.  Pruning is the easiest alteration to imagine,
but excising interior nodes can be useful (and this is what the RPL `alias`
achieves, except it does this eagerly).

Once again, we have a model that appears to require "useless effort" to be
expended, because during matching we may create a large data structure for a
match, only to trim it down later.  However, static analysis of the pattern
being compiled may illuminate optimizations in which sub-trees destined to be
pruned are never created in the first place (provided they are not needed for
some back-reference or look-behind kind of operation, in which case they really
will need to be removed post-match).

Let's describe, in this "low level" language, an operator to capture a match by
associating with it an identifier.  We'll name it with a `!` to indicate some
inchoate (for now) distinction from other forms.  The distinction involves the
notion that while the code may say to capture, the at run-time the capture may
be a no-op -- assuming we have proven it will be pruned from the output later
and is not needed during matching.  And on the other hand, the code may say to
suppress capturing a pattern, but at run-time certain captures inside the
`<exp>*` (arguments to `suppress!`) may in fact be captured, so they can be
referred to during matching -- though they'll be excised later from the output.  

	(capture! <id> <exp>*)  // capture <exp>* (implicit begin)
	(suppress! <exp>*)      // suppress captures (implicit begin)


### Look-arounds are a mess in regex and not mellifluous in PEGs

Can we replace the look-around operators, below, with a different abstraction,
perhaps one that uses delimited input spans?

	(ahead <exp>)
	(behind <exp>)
	(!ahead <exp>)
	(!behind <exp>)
	
#### Look-ahead replacement

The concept of look-ahead could be augmented with a possibly-delimited input
span.  A "match twice" operator would match its first argument, and use the span
of that match as the input to the second argument.  If the first argument fails
to match, the overall expression fails.  If the second argument fails, the
overall expression fails.  If both succeed, the current input position will be
wherever the second expression finishes matching, and the output (tree) will
contain any captures produced by the second expression.

    (with <exp> <exp>)       // possibly-delimited look-ahead
    (with <exp>)             // PEG positive look-ahead (no captures)
    (without <exp>)          // PEG negative look-ahead (no captures)

#### Look-behind replacement (and back-references, too!)

The look-behind operator in regex seems quite reasonable at first glance.  It is
the mirror to look-ahead.  But it's the wrong idea.  First, going over the same
section of input twice seems like a bad idea, unless we can achieve something
additional for our efforts (as is the case for `with`).  Second, we have an
opportunity to do all kinds of things with input that's already been processed.
The input behind the current position is already represented in a parse tree,
even if that tree is a single node (a single span).

It seems potentially useful to allow predicates on the captures that exist at
any point during the matching process (noting, of course, that those captures
may not all exist when the process ends, even if the match succeeds).

There's a striking similarity to the way I designed back-references in Rosie, in
which a back-reference yields a literal (a previously matched input span) that
must be matched next.  There, the referenced expression is given by name, and
the capture tree is traversed to find it.

Suppose we generalize what happens next.  In a Rosie back-reference, the entire
span matched by the reference acts as a string literal to be matched next.  We
could provide predicates on those spans in order to achieve a more general form
of look-behind.

Let's assume `(root)` refers to the root of the overall match (tree).

    (was (root) (find <bytes> $)   // usual regex-style lookbehind
    (was (root) (find <exp> $))    // PEXL-style lookbehind (a search)
    (was <id> <bytes>)             // equality predicate on already-matched <id>
    (was <id> <exp>)               // <exp> succeeds on already-matched <id>
	

Now, if `<id>` can be a path through a capture tree, we have what looks like a
very expressive primitive, `was`, that generalizes both back-references and
look-behind. 

And it might be nice to replace `$` above, so that we can keep the language as
primitive as possible.  Maybe we need `ends-with` as a special form of `find`
which succeeds only if the target `<exp>` matches at the end of input.

### Modifying the match

If we implement string processing operations like "split" and "replace" (for
example) as well as matching, then we may have created a replacement for a wide
array of string operations.

The obvious approach is to create the match tree as we do today in Rosie and
PEXL, and then perform a transformation on the tree afterwards.  For example, to
"split" an input string by `<exp>`, we find all occurrences of `<exp>` and then
"complement" the tree, yielding a rope-like data structure that is really just a
list (the children of the root node) of the spans that did not match `<exp>`.

We probably do not want to define a language whose semantics are different from
this.  For example, we could say that our language allows the user to specify
match tree manipulations as the match is being created.  This leads to several
problematic issues: 

1. Mutation of the match may interfere with operators that perform operations
   akin to look-behind and back-reference.  We don't want the user to be
   concerned with the order of matching and mutating.

2. Captures are often discarded, because they can be created while processing a
   choice "arm" (alternative) that may not ultimately succeed.  Why do the extra
   work? 
   
3. It may seem like we are locked into doing extra work by creating a match tree
   and then post-processing it.  But it is conceivable that the compiler can
   determine when it is safe to mutate the tree during matching.  As with all
   potential optimizations, doing this may not perform better than the pure
   post-processing approach.
   
How do we specify tree transformations?  The literature of how to specify tree
manipulations includes such well-trod territory as macros in Lisp and Scheme.
We have these and other sources of inspiration to draw upon.


