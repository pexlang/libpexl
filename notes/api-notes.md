# String processing API notes

Author: Jamie A. Jennings
Last major edit: Thursday, March 16, 2023

## The string encoding type must be a compilation argument

We should compile down to a byte-oriented vm, so we must be told at compilation
time what the expected input text encoding is.  Generally, text does not contain
any indication of its encoding, and guessing gives poor results.

## NUL-terminated input is helpful

There are many advantages when the input string is null-terminated.  We should
have an implementation that assumes this, as well as one that does not.

For short inputs provided through the non-null-terminated API, we may want to
copy them and add the null terminator.

By default, we assume that there may be null chars in the input regardless of
whether there is a null terminator or not.  Is there any advantage to KNOWING
that there are no null bytes within the input?  If so, then the caller can tell
us this information as well (or we can detect it automatically if the
performance gains would significantly offset this test).

