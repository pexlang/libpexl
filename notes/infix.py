from functools import reduce
import timeit, subprocess
import rosie

# def reduce(fn, ls, initial):
#     accum = initial
#     for i, element in enumerate(ls):
#         accum = fn(accum, element)
#     return accum

e = rosie.engine()

# This, below, is the usual (and naive) result of encoding order of
# operations, including parentheses, in a grammar without left
# recursion.

e.load('''
grammar
   Term = (Factor "*" Factor) / Factor
   Factor = (Num "*" Factor) / Num / ("(" Expr ")")
   Num = [0-9]+
in
   Expr = (Term "+" Term) / Term
end
''')

expr = e.compile('Expr')

# This variant, below, avoids the exponential-time disaster of the
# naive approach.

e2 = rosie.engine()
e2.load('''
grammar
   Term = Factor ("*" Factor)?
   Factor = (Num ("*" Factor)?) / ("(" Expr ")")
   Num = [0-9]+
in
   Expr = Term ("+" Term)?
end
''')

expr2 = e2.compile('Expr')

# Python's recursion implementation is brain-dead.  This only works
# with limited-depth parse trees -- i.e. unsuitable for testing or for
# real use.
def eval_parse_tree___(expr_match):
    if expr_match['type']=='Expr.Num':
        return int(expr_match['data'])
    elif expr_match['type']=='Expr':
        return reduce(lambda accum, sub: accum+eval_parse_tree___(sub), expr_match['subs'], 0)
    elif expr_match['type']=='Expr.Term' or expr_match['type']=='Expr.Factor':
        return reduce(lambda accum, sub: accum*eval_parse_tree___(sub), expr_match['subs'], 1)
    else:
        print(expr_match)
        raise ValueError

# It is 2021, and we are forced to write iteration over trees.  Python
# is broken.
def eval_parse_tree_iter(match):
    acc = []
    while True:
        if match['type']=='Expr':
            current = {'accum': 0,
                       'fn': lambda accum, i: accum+i,
                       'subs': match['subs']
                       }
            acc.append(current)
        elif match['type']=='Expr.Term' or match['type']=='Expr.Factor':
            current = {'accum': 1,
                       'fn': lambda accum, i: accum*i,
                       'subs': match['subs']
                       }
            acc.append(current)
        elif match['type']=='Expr.Num':
            n = int(match['data'])
            current = acc[-1]
            current['subs'].pop()
            current['accum'] = current['fn'](current['accum'], n)
            while not current['subs']:
                accum = current['accum']
                acc.pop()
                if not acc:
                    return accum
                current = acc[-1]
                current['accum'] = current['fn'](current['accum'], accum)
                current['subs'].pop()
        match = current['subs'][-1]
        continue

def eval(string, exp):
    m = exp.match(string)
    if m is None:
        print("Syntax error in expression")
        return None
    rm = m.rosie_match
    assert(rm)
    return eval_parse_tree_iter(rm)

def run(string, expected_answer):
    answer = eval(string, expr)
    print("{} = {}".format(string, answer))
    assert(answer == expected_answer)
    answer = eval(string, expr2)
    assert(answer == expected_answer)

# Collect data showing exponential time to match ((((((7))))))
def timed_run(i, expr_name):
    run_command = 'eval("' + "("*i + "7" + ")"*i + '", ' + expr_name + ')'
    t = timeit.Timer(run_command, setup="from __main__ import eval, expr, expr2")
    return t.timeit(number=1)

def observe(expr_name, m, n, step=1, out=None):
    data = []
    print("Data for plotting:")
    for i in range(m, n, step):
        print("{:3} ".format(i), end='')
        time = timed_run(i, expr_name)
        print("{:10.6f}".format(time))
        data.append((i, time))
    if out:
        f = open(out, 'w')
        for datum in data:
            f.write("{} {}\n".format(datum[0], datum[1]))
        f.close()
        print("Wrote " + out)
    return data
        
def plot(name, datafile, eqn, eqn_vars, eqn_desc):
    graphfile = datafile + '.png'
    proc = subprocess.Popen(["/usr/local/bin/gnuplot"], stdin=subprocess.PIPE)
    def w(line):
        proc.stdin.write(bytes(line, encoding='UTF-8'))
        proc.stdin.write(b'\n')
    w('set terminal png')
    w('set title "' + name + '"') 
    w('set xlabel "input size (chars)"')
    w('set ylabel "time (seconds)"')
    w('f(x) = ' + eqn)
    w('set fit logfile "' + datafile + '.log"')
    w('fit f(x) "' + datafile + '" via ' + eqn_vars)
    w('set output "' + graphfile)
    w('plot "' + datafile + '" t "measured", f(x) t "' + eqn_desc + '"')
    proc.stdin.close()
    proc.wait()
    print("Graph written to file " + graphfile)

def plot_expr(datafile):
    plot('expr', datafile, 'a*(b**x)+c', 'a,b,c', 'exponential fit')

def plot_expr2(datafile):
    plot('expr2', datafile, 'a*x+b', 'a,b', 'linear fit')
    
# -----------------------------------------------------------------------------
# Examples
# -----------------------------------------------------------------------------
print("\nRunning examples...")

run("1 + 2 * (3 + 4)", 15)
run("2 * 3 + 4 * 5", 26)
run("2 * (3+4) * 5", 70)
run("2 * (3+4) + 5", 19)
run("(((2)) * ((((3+4)))) + 5)", 19)
run("("*6 + "1+2" + ")"*6, 3)

print("All examples passed.")

print("\nTo see exptime behavior, try observe('expr', 1, 12)")
print("To see linear behavior, try observe('expr2', 1000, 8001, 200)")
print("Give parameter out='filename' to observe() to save the data")

print()

print("To plot with exponential fit, try e.g. plot_expr('/tmp/expr.data')")
print("To plot with linear fit, try e.g. plot_expr2('/tmp/expr2.data')")
