# Language Design Ideas (2024)

See /ARCHITECTURE.md for generalizations of look-arounds (`where`, `with`, and
`if`).  The issue of the day is modules and generally, nested scopes.

I'll use the S-expression syntax for the internal intermediate language (IIL)
here, for convenience.

Simple definitions:

```
(def tic "tic")
(def ttt (seq tic "tac" "toe"))
(def a-or-b (choice a b))
```

Modules create a namespace, and are first-class:

```
(def m (mod
         (def foo "foo")
		 (def fbb (seq foo "bar" "baz"))))
(def x m.fbb)		 
```

Note that the definition of `fbb` can reference the definition of `foo`, which
appears in the same namespace.  (I'm thinking of `letrec*` as a possible model
for both top-level and module definitions.)

If we allow local (private) definitions, we do not need a Scheme-like `let`
because `mod` will suffice.  We could, following Rust and others, use `let` to
introduce local definitions.

```
(def m (mod
         (let foo "foo")
		 (def fbb (seq foo "bar" "baz"))))
(def x m.fbb)  // works
(def y m.foo)  // error because foo is private to module m
```

Finally, we need an easy way to import modules from files.  Maybe the following? 

```
(def m (load "some/file/path/m"))
```

In Rosie 1.x, we do not allow absolute paths.  To remove a security concern, we
want to give users a way to constrain the directories from which Rosie will load
patterns.  We should do the same with PEXL.  An engine should, on creation, be
given a default "load path", which is really a list of paths but the name "load
path" is both ancient and still in use.

The syntax above handles nicely the flexibility of `import X as Y` from Rosie
1.x.  But it stutters when the name we want, `m` in the example above, is also
the name of the file holding the module contents.

Here, syntactic sugar can help, e.g.

```
(import "some/file/path/m") ==> (def m (load "some/file/path/m"))
```

What, then, does `load` do?  It would be terribly retrograde if it acted like
`#include` from C.  Then again, in a low-level intermediate language, maybe it's
the right thing.  It's simple, and it wouldn't really work like C.  The file to
be loaded would be parsed and its AST would be included.  We don't have to
expose it in RPL 2.0.  But I bet people will ask for it.


# Language Design Ideas (2023)

See Language Issues to Resolve (2020-2022), below, for earlier notes on open
design issues.  Here in this section, we freshly sketch possible designs.

## Sequence

Pattern sequences should continue to be expressed by adjacency, using whitespace
as the operator.  Note that in RPL 1.3, whitespace is not needed to signal a
sequence: 
```
$ rosie match '"hello""world"' <<< 'hello world'
hello world
$ rosie match '"x"."z"' <<< 'x y z'
x y z
$ 
```

Requiring whitespace means more typing, but it likely reduces opportunities to
misinterpret pattern code.  In RPL, to avoid ambiguity, the dot cannot follow an
identifier without intervening whitespace:

```
$ rosie match 'word.any .' <<< 'hello x'
hello x
~/Projects/libpexl/src$ rosie match 'word.any.' <<< 'hello x'
Syntax error
	[parser]: syntax error while reading expression
	in user input :1:1: word.any.
	in user input 
$ 
```

## Choice

There are three reasonably popular kinds of choices in regex and PEG languages
today:
1. Unordered, non-possessive choice (regex default, e.g. `A|B`)
2. Unordered, possessive choice (supported in some regex dialects, e.g. `(?>E)`
   where `E` could be `A|B`); it is unordered only because it is internally
   non-possessive, i.e. `E` could be `(A|B)C` which will match `AB` or `AC`,
   perhaps choosing based on the longest match convention _(check this)_
3. Ordered, possessive choice (PEG default, e.g. `A/B`)

Since almost every regex implementation uses backtracking, unordered choices are
a common source of poor worst-case complexity in theory and denial of service
attacks in practice.  The common remedy is to wrap the choice in an atomic
(possessive) expression.  However, this changes the semantics of the regex.
Sometimes the change is innocuous or desirable, but not always.  As we know that
regex are severely under-tested, performance fixes that may alter semantics are
a fraught proposition.

The PEG approach resembles imperative code, and that may be why developers like
it.  The expression `(A/B)C` is easy to understand, as its operational semantics
do not involve any backtracking.  By contrast, understanding `(A|B)C` in regex
effectively requires the developer to imagine all paths through an NFA -- easy
for trivial regexes, but burdensome and error-prone for larger expressions.

There are two negatives around PEG possessive choice, in my opinion.  The first
is that it requires the developer to recognize when `B` can match a prefix of
`A`, so they can put `A` first in order to prefer the longer match.  The second
issue is that sometimes the developer wants the semantics of `AC/BC` and should
be able to get it without repeating `C`, which may be a long unnamed pattern.
Let's address these.

* The prefix problem is interesting, because it's possible the compiler can
  help.  It is decidable whether a regular language is prefix-free, but not so
  for context-free languages.  But we have PEGs, or something like them (by the
  time we are finished designing).  If our type of language, whatever it turns
  out to be, has a decidable prefix property, then we can decide whether any
  string in L(A) is a prefix of a string in L(B), and the compiler can issue a
  warning or error for the expression `A/B`.  Such a warning tells the developer
  that `A/B` may match `A` "prematurely", in the sense that a longer match would
  be obtained by `B`.  Some recent work on scanner-less parsing for LL(1)
  languages suggests that something like using "one character of lookahead"
  (versus the usual "one token" definition) might be a useful design compromise,
  giving the benefits of scanner-less parsing and also the linear-time guarantee
  of LL(1) languages.  I don't recall how widely their approach applies, but I
  suspect it's limited to unambiguous grammars for LL(1) languages.  **TODO:
  Find the paper and reread.**
   
* Our language could support an ordered non-possessive choice, e.g. `(A||B)C`.
  We could wrap up `C` into an anonymous pattern and generate the code `AC/BC`.
  Notice that this is like reifying the continuation of the expression `A||B`,
  in that `C` really does mean "everything that follows".  In our current
  backtracking implementation, we do this all the time by pushing a frame on the
  backtrack stack.  The performance implications are somewhat subtle but
  important, due to the unbounded nature of the backtracking.  We open ourselves
  to the classic regex `a?a?a?aaa` exponential runtime problem.  If instead we
  forced the developer to write `AC/BC`, at least the backtracking is explicit.
  **Open question: What is the non-possessive scope here?**  Suppose I write
  `PQ` where `P=(A||B)C`.  Is `PQ === ACQ/BCQ`?  That would be the case if we
  were reifying an undelimited continuation.  But if `PQ == (AC/BC)Q`, then we
  have a delimited continuation again.  Is there a use case for the undelimited
  continuation?  Yes, it arises in the transformation from regex to PEG.  Is
  there a good use case for supporting an equivalent of the regex choice, then?
  **TODO: When do regexes use the plain choice, and when do they wrap it in an
  atomic expression?  What is the use case for plain choice?  Note that regex
  solutions do not typically allow combining of patterns, i.e. the entire
  expression is written out and therefore the continuation part of the pattern
  is explicit.** 


## Repetition

Again we have a dilemma related to the possessive nature of PEG expressions.
The PEG variant of Kleene star is possessive, and `A*` is equivalent to `P`
where:

```
P = (A P) / epsilon
```

The expression `A* B`, then, is equivalent to `P B`.  Because the PEG choice is
possessive, each match to `A` will never be rolled back, even if `B` fails to
match.

Contrast this with the PEG equivalent of the regex (Kleene) star: the regex
`A*K` is equivalent to `P` where:

```
P = (A P) / K
```

Here, if `K` fails to match, there is a prior `K` on the backtrack stack,
sitting there as the alternative to the `(A P)` whose match is still pending.
With a backtracking implementation, the non-possessive star operator is costly.
Its use builds a backtrack stack linear in the size of the input (worst-case,
where all but a finite part of the input matches `A`).  The backtrack stack is
an explicit reminder of the behavior of the regex star, which is greedy but not
possessive: it matches all it can, and then backs off one match at a time if the
continuation fails.

I've written before about how this behavior seems to surprise developers (but
not students of automata theory).  And I do wonder if this behavior -- the
non-possessive star -- has practical utility.  I suspect that the use cases
driving its use could be better served by some alternative, one with less
surprising semantics and lower asymptotic complexity.

The reader should note, with little surprise, that the transformation of the PEG
star into a recursive PEG grammar is a _local transformation_, whereas the
conversion of the regex star to a recursive PEG grammar is a _global
transformation_ (requiring `K`, which represents the entire continuation of the
pattern after the starred expression).

In practical usage, does anyone need a pattern like `a?a?a?aaa` to match `aaa`?
The regex interpretation will, but the PEG interpretation will not.  Automata
theory students and professional software developers, anecdotally, all expect
the PEG semantics to hold here.  They expect each `a?` to match, and for the
entire expression to then fail.  Upon learning Thompson's Construction of an NFA
for this regex, automata theory students then understand the exponential number
of paths through the NFA that must be tested by a backtracking algorithm.

Possessive repetitions seem to be an appropriate design choice.  They appear to
be what is expected by developers; and, non-possessive repetitions can be
obtained by explicit use of recursive grammars and pattern continuations.

Following this line of reasoning, we should look at how difficult or easy it is
to write non-possessive repetitions.  As we saw above, it is easy if we have the
entire continuation `K` to plug into the conversion formula.  And as we noted,
this is a global program (pattern) transformation.

However, there is a middle ground, which is to use a local transformation,
corresponding to a delimited continuation (versus a global one).  Suppose we
used a double star to indicate non-possessive repetition.  Then `A** B` would be
equivalent to `P`, where:

```
P = (A P) / B
```

Here we have the non-possessive version of Kleene star, but only within the
expression `A** B`.  In the expression `P Q`, with `P` defined as above, there
would be no backtracking if `Q` fails.  The part of the pattern continuation
(after `A**`) that is within the backtracking scope is `B`.  The part outside of
that is `Q`.  

By contrast, the transformation of the regex `A*BQ` into a PEG has `K=BQ`,
giving `P` where:

```
P = (A P) / (B Q)
```

Note the difference in behavior.  If `Q` fails, the pattern `P` backtracks to
one fewer matches of `A`.

In a system designed to promote pattern composition, operators that induce
global pattern transformations are unwelcome.  They are hard to reason about
because they are non-local, and this limits both pattern compilers and human
developers in their quest to understand how patterns will behave.

## Grouping

## Look-around

## Back references



=============================================================================

# Language Issues to Resolve (2020-2022)

## Infix Expression parsing

- Parsing infix expressions is difficult to achieve with PEGs because the
  formalism does not support left recursion.  Because it is a use case that is
  common enough to motivate extending regex libraries, we should consider how
  our solution (a regex replacement effort) could support users who need to
  parse infix expressions, and do so efficiently.

  First, note that a naive PEG grammar with infix operators and grouping
  constructs can produce an exponential-time parser.  See
  `rpl/rosie/rpl_1_2.rpl` for an example; the subsequent `rpl_1_3.rpl` has a
  linearized grammar and Rosie uses Shunting Yard as a post-parsing step to
  produce the desired parse tree for RPL expressions.

  Left recursion, prefix/infix/postfix operators, operator precedence, and
  operator associativity are parser features that make it relatively easy to
  parse expressions.  Pratt parsing constructs the desired parse tree in one
  pass through the input, but requires looking ahead by one expression.

  Knuth's Shunting Yard algorithm is an offline algorithm that achieves the same
  result (producing the desired parse tree) by restructuring the parse tree
  obtained by using a linearized form of the expression grammar.  Such a grammar
  has a rule like 'exp => exp (infix_operator exp)*', where 'exp' has other
  rules for prefix operators, including grouping constructs.

  We could, in our project, allow the user to specify operators with precedence
  and associativity so that we can automatically run Shunting Yard after
  parsing.  This approach requires the user to write a linearized form of their
  expression grammar.  While this is not a difficult task, it is an additional
  skill/concept to learn.  Post-processing with Shunting Yard may be efficient
  in that no effort is expended rewriting the parse tree unless there is a
  successful parse.  Also, we know that this approach does not require left
  recursion (see `rpl_1_3.rpl`) because we can write, e.g.

    exp => term (infix_operator exp)*

  where 'term' consumes prefix operators and base terms.

  Finally, it's possible that user-specified syntax errors (in the linearized
  grammar) may be of higher quality than those that might be generated in Pratt
  parsing.

  Pratt parsing is the obvious alternative.  Our code generator could emit code
  for Pratt parsing if we give the user a way to designate operators with their
  precedence and associativity -- just as we would do if we used Shunting Yard.

  In either approach, must the user define 'term'?  Not necessarily.  A term
  could be a sequence of arbitrary white space, comments, and actual base terms.
  While this sequence is explicit in the grammar, that it constitutes a term may
  be implicit.  In other words, the definition of a term could be treated by the
  expression parser as whatever sequence appears between infix operators.

  Note also that postfix operators may be treated as a variant of infix.

  References:
  [1] Ugly Python code using "binding power" terminology:
  https://eli.thegreenplace.net/2010/01/02/top-down-operator-precedence-parsing/
  [2] Java code but a good high-level description, separating prefix from infix
  and using the "precedence" terminology:
  http://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy/
  [3] Javascript implementation of an overly complicated example:
  http://crockford.com/javascript/tdop/tdop.html
  [4] Rust code with a nice treatment of prefix, infix, postfix, and misfix:
  https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html
  [5] Pseudo-code contrasting Pratt, Shunting Yard, and precedence climbing:
  https://www.engr.mun.ca/~theo/Misc/exp_parsing.htm
  [6] Several good posts (as usual!) from Andy Chu:
  http://www.oilshell.org/blog/tags.html?tag=parsing#parsing


## Capture control

- How will we make it easy (syntactically) to:
  - Capture or not
  - Suppress all captures in an expression
  - Keep only the top-most capture(s) of an expression

  Current thinking (July 4, 2022) is to simplify pattern definitions by omitting
  any notion of capturing.  Everything with a name will be captured.  Separate
  "compiler directives" or "capture declarations" will configure what to
  capture, and users of a pattern can override those decisions.

  We started with an S-expression syntax, then added an infix '/' and postfix
  '?' and '*'.  Then we changed to a C-like (imperative) syntax where the
  pattern is pulled out in front of the parentheses, which now contain only the
  sub-patterns. 

  E.G.
      subqux = "nothing to see here"
      qux = "a" subqux "b"
      baz = "this will not be captured" qux
      bar = baz qux? qux
      foo = bar

  SCHEMA: foo( 
            bar(baz(qux(subqux))
                qux(subqux)? 
                qux(subqux)))

  @show foo               -- in output, show foo and all inside it
  @show foo()             -- show foo but nothing inside it
  @show foo(bar)          -- show foo, bar, and all inside bar
  @show foo(bar())        -- show foo and bar, but nothing inside bar
  @show foo(bar baz())    -- show foo, bar, baz (nothing in baz), everything after
  @show foo(bar() baz qux())  -- show foo, bar, baz (and inside), first qux (nothing inside)
  @show foo(_ _ qux())        -- show foo, and first qux if present (nothing inside it)
  @show foo(_ _ _ qux)        -- show foo, and 2nd qux (and all inside it)

  E.G. 
      baz = "this will not be captured" qux
      bar = baz qux?
      foo = bar / baz

  SCHEMA: foo ((bar(baz(qux) qux?)) / (baz(qux)))

  @show foo(bar() / baz())        -- show foo, bar, baz, nothing deeper
  @show foo(bar / baz())          -- show foo, bar (and inside it), baz (nothing inside it)
  @show foo(_ _(qux()) / _qux())  -- show foo and any qux items inside it

  E.G. 
      baz = "this will not be captured" qux
      bar = baz qux*
      foo = bar / baz

  SCHEMA: foo ((bar(baz(qux) qux*)) / (baz(qux)))

  @show foo(_ (_ (qux())) / _ (qux()))  -- show foo and any qux items inside it
  @show foo(_ (_ qux[0]()) / _ ())      -- show foo and 1st qux item of bar (if present)
  @show foo(_ (_ qux[5]()) / _ ())      -- show foo and 5th qux item of bar (if present)
  @show foo(_ (_ qux[5]()) / _ (qux())) -- as before but also show the qux inside baz

  We could start WITHOUT this list item capability: you get the whole list or
  none of it.  Then we can add support for the above syntax, for ONE of a list.
  Later, could add ranges or other more sophisticated features.


## Syntactic modules

- Would be nice if modules/packages could be declared anywhere.  The net of all
  the notes (way down below in this file) is this:

  (1) A let/in/end block introduces with a new lexical scope.  

      outer = "c"
	  foo = let
	    a = "a"
	    @import ext
	    b = a "b" outer ext.d
	        let inner = "e" in inner end  -- This finishes the defn of 'b'.
	  in
            b                                 -- Expression required.
	  end
      -- NOTE: 'outer' and 'foo' are visible here, but not
	  --       'a', 'ext', 'ext.d', 'b', or 'inner'.
	  -- NOTE: 'outer' matches "c" and 'foo' matches "abcde", assuming
	  --       that 'ext.d' matches "d".

  (2) A namespace (module with its own lexical scope) can be defined "inline" in
      a similar way, by declaring `module` and the name, followed by a block.

	  foo = module 
	     a = "a"
	     local b = "b"                    -- Unused definition
	     import ext                       -- Useless import
	  end
      -- NOTE: 'foo.a' is visible here, but not any of:
	  --       'a', 'b', 'foo.b' or 'ext'.
	  -- NOTE: 'foo' is a namespace, so it is (implicitly) local to the
	  --       scope in which 'foo' is defined.

  (3) An expression is a valid statement, although this is only useful when both
      the following are true: the expression is the last statement in a scope,
      and the scope appears in an expression context.  
 
          Bal = let S = a S b / epsilon in S end

  (5) To import a module from elsewhere:

          import a           -- new namespace 'a' populated by file 'a'
          import a/b         -- new namespace 'b' populated by file 'a/b'
          import a/b/c as d  -- new namespace 'd' populated by file 'a/b/c'
 
   In each case above, files must have the right extension (e.g. .rpl), and
   they are found by iterating over a `libpath`, which is a list of
   directories.  In future, we may want to allow https (and other) URL
   schemes instead of a (relative) file name.  Similarly, relative file names
   could start with "./" and absolute ones with "/":

          import ./a        -- new namespace 'a' populated by file './a'
          import ./a/b      -- new namespace 'b' populated by file './a/b'
 
   The `libpath` is meant to facilitate having "library directories" whose
   locations in the file system may vary from machine to machine.  

   If the import path contains whitespace, it must be enclosed in double
   quotes (interpolated) or single quotes (raw).  As with other string
   literals, a sequence of quoted strings will be concatenated into a single
   string.

          import "./a a/b"    -- relative file 'b' in directory 'a a'
          import './a ' 'a/b' -- same
          import './a"' "a/b" -- relative file 'b' in directory 'a"a'
          import './a' a/b    -- syntax error (bare strings not concatenated)

   NOTE: If it turns out to be useful, we could parameterize modules (perhaps
   like OCaml does) and use them in some of the cases where macros would be
   used today.

X     We could try a different approach (Scheme48 has a path translation
X     facility; environment variables might suffice; symlinks punts the problem
X     to the user and their OS).  What would the Scheme48 approach look like?
X     
X         @import $a/b         -- new namespace 'b'
X         @import $a/b/c as d  -- new namespace 'd'
X	  And a separate configuration item: translate $a /usr/local/lib/rpl
X     Downside: Need to maintain a SET of names and avoid collisions.
X     Upside: No worries about what comes first on the search path.
X
X     HOWEVER, we could use $ or something else to signal the desire to import
X     from `libpath`, and if not present, we simply pass the path to the OS.
X
X     The `file:` URL scheme could be the indicator, though we will initially
X     restrict the hostname to be omitted, e.g. 'file:/usr/lib/rpl/a.rpl', or
X     empty, e.g. 'file:///foo'.  But this is an absolute path?  Is it possible
X     to get a relative file url?  Cursory research suggests not, so this idea
X     is not workable.
X
X     We could make them write 'file', the absence of which means to import from
X     a place on the library path.  E.g.
X
X         @import file a/b         -- 'a/b' passed to the OS
X         @import a/b              -- 'a/b' searched on libpath
X
X     ALTERNATIVE: Always use libpath, but make it easy to extend the libpath
X     so that it starts with the directory of the current file.  Seems
X     equivalent to supporting library directories and relative file names, but
X     not absolute filenames, and that could be a win.  First, hard-coded
X     absolute file names are never the right thing.  Second, relative file
X     names (if rooted at directory of current file, like Rust module
X     references) make it easy to divide a project into a set of files without
X     having to modify the `libpath` to use it.


  EARLIER DRAFTS OF IDEAS FOLLOW
  ------------------------------

  Taking inspiration from Rust, we could do something like this:

	  outer = "o"
	  let foo {
	     local a = "a"
	     b = "b" outer
	  }

          -OR- 

	  The contents of foo.rpl are interpreted as the contents of package
	  'foo' in any file that declares `import foo`.  In that case, the only
	  symbols visible inside 'foo' are (1) those defined there; (2) those
	  imported; (3) those defined in the standard or specified prelude.

      In both cases (writing `let foo...` or `import foo`), the visible symbols
      in package foo are accessed like 'foo.x'.

      THEN... Omitting the name hides locals like 'a' but 'b' is visible outside
      the let body.

	  let {
	     local a = "a"
	     b = "b"
	  }
	  
      Note that `let` could be omitted in the use above.  Curly braces would
      suffice if we don't need recursion or a namespace.

      THEN... Writing letrec allows recursive definitions in the letrec body.

	  letrec foo {
	     local a = "a"
	     b = "b" b / ""
	  }

      THEN... Writing letrec and omitting the name hides locals like 'a' and
      'b', but 'Bal' is visible outside the letrec body.

	  letrec {
	     local a="a"; local b="b"
	     Bal = a Bal b / epsilon
	  }

      THEN... A module in an expression context evaluates to whatever it binds
      to _.  No name (after let or letrec) is allowed, and all definitions
      except _ must be local.  This is clunky syntax, but it exists to satisfy a
      rare need.

          Bal = letrec {
	    local a="a"; local b="b"
	    local Bal = a Bal b / epsilon   -- outer Bal not visible,
	     _ = Bal                        --   so local Bal not shadowing
	  }

      Note: We could add another keyword, like `in` to mark the expression
      above, instead of assigning it to _.

      OR, we could allow blocks to contain expressions (like Lisp and Rust), and
      the user would have to use a semi-colon to demarcate the expression, e.g.

          Bal = letrec {
	    local a="a" local b="b"
	    local Bal = a Bal b / epsilon;    -- semicolon required
	    Bal                               -- expression, using local Bal
	  }

      Note that adding a semicolon after the expression would, in Rust, produce
      an empty (void/unit) expression.

      And what would it look like if we wanted to write a let/letrec expression
      and then put a star or plus on it?

          E.g. Writing (a^n b^n)*
            letrec {_ = a _ b / epsilon}*      -- star looks wrong
            (letrec {_ = a _ b / epsilon})*    -- better, if clunky
            {letrec _ = a _ b / epsilon}*      -- maybe? no.

      With the semi-colon syntax instead of underscore:

          E.g. Writing (a^n b^n)*
            letrec {S = a S b / epsilon; S}*      -- star looks wrong
            (letrec {S = a S b / epsilon; S})*    -- better, if clunky
            {letrec S = a S b / epsilon; S}*      -- maybe? no.

      Hmmmm... Maybe the first symbol after an open brace SHOULD optionally
      declare a recursive scope?  If we did this, we could borrow (lol) Rust's
      `mod` usage as both a module defining and importing keyword.

      THEN... Imports are allowed, and their symbols have local visibility.

	  let {
	     import net
	     ip = net.ipv4
	  }
	  -- Here, ip is visible but not net or net.*.

  - If we eliminated letrec, and all scopes allowed recursion, what would we
    lose?  In RPL, we wanted to avoid accidental recursion.  But RPL lacked the
    (planned) PEXL capability of an `@linear` annotation, which would typically
    fail on a recursive grammar.

  - Rust allows nested modules.  We don't have to, since we could add them later
    if needed.  But there's no reason not to.  We don't have to allow access to
    symbols in nested modules, like Rust and other languages do.

  - Rust does not make you declare that a file of code is a module.  When
    another file contains `mod m`, Rust looks for file 'm.rs' and interprets its
    contents as the module named 'm'.

    - Note: When a module name is derived from a file name, it opens up string
      encoding issues, because operating system paths are typically not required
      to conform to any particular encoding, Unicode or otherwise.

    - Note: Rust will look for file 'm/mod.rs' if it doesn't find 'm.rs'.  That
      allows 'mod.rs' to include other modules in the directory 'm'.  Rust will
      expose those other modules (e.g. 'm/foo.rs') if they are declared with
      `pub` (e.g. `pub mod foo`) in mod.rs.  We don't need to allow nested
      modules, so we will force the user of 'm' to import 'm/foo' directly, if
      they want to also use that module.

  - Rust's use of `mod` to introduce a module definition ("inline") and also to
    import a module from somewhere else is uncommon.  Most languages do not have
    a way to write a module inline, and they have a verb ("import", "use") that
    signifies "load some code from elsewhere".  However, in PEXL we may want to
    try extra hard to keep the set of reserved words small, in order to allow
    users to name patterns as easily or naturally as possible.  If PEXL reserved
    words are needed rarely, then we could mark them with a sigil like '@'.  We
    expect that by far the most used will be 'import' and 'test'; is it really
    much more difficult to write '@import' and '@test' instead?

- A possible grammar

  MODULE := let IDENT? { STMT* }
  MODULE := letrec IDENT? { STMT* }

  STMT := DEFN / IMPORT / PRAGMA

  DEFN := local? IDENT = EXP
  IMPORT := import IMPORTSPEC
  IMPORT := import IMPORTSPEC as IDENT

  PRAGMA := #prelude IMPORTSPEC
  PRAGMA := #linear DEFN
  PRAGMA := #capture IDENT CAPSPEC

  CAPSPEC := to be determined

  An IDENT is an unqualified name, i.e. with no dots.
  A QUALIDENT is a possibly qualified name (e.g. X or X.Y, maybe more).

  An IMPORTSPEC could be an arbitrary string, which would allow us to easily
  extend the language with new interpretations of that string, e.g. to recognize
  OS paths (absolute, relative) or URLs.

  Relatedly, the AST node could require the localname, as suggested by the
  definition above.  Each kind of importspec would presumably be associated with
  a way to determine the default localname, in case the user does not provide
  it.

  The rest of the grammar:

  EXP := (EXP)                        -- grouping
      := MODULE                       -- locals, except for _
      := LITERAL
      := true
      := false
      := byte                         -- single byte
      := .                            -- codepoint? glyph?
      := SET
      := RANGE
      := EXP+                         -- sequence
      := EXP / EXP                    -- ordered, possessive choice
      := EXP*                         -- possessive Kleene star
      := EXP+                         -- == EXP EXP*
      := EXP?                         -- == EXP / ''
      := EXP{NUM, NUM}                -- note NUM can be inf
      := QUALIDENT                    -- call
      := PREDICATE
      := APPLICATION                  -- functions and macros

  LITERAL := INTERP_STRING
          := RAW_STRING
          := LITERAL+                 -- adjacency is concatenation

  INTERP_STRING := " CHARSPEC* "      -- interpolated
  RAW_STRING    := ' PRINTABLE* '

  CHARSPEC := PRINTABLE
           := \x HEX HEX              -- arbitrary byte
           := \u HEX HEX HEX HEX
           := \U HEX HEX HEX HEX HEX HEX HEX HEX
  
  PRINTABLE := to be determined via Unicode properties

  SET   := [ CHARSPEC* ]
        := [^ CHARSPEC* ]

  RANGE := [ CHARSPEC - CHARSPEC ]
        := [^ CHARSPEC - CHARSPEC ]

  PREDICATE := > EXP                  -- lookahead
            := < EXP                  -- lookbehind
	    := ! PREDICATE
	    := ^                      -- start of input
	    := $                      -- end of input

  APPLICATION := QUALIDENT : ARGLIST
  ARGLIST := GENEXP                   -- exactly one argument
          := (GENEXP, GENEXP, ...)    -- zero or more arguments

  GENEXP := EXP
         := NUM
         := #INTERP_STRING            -- hash says "string, not pattern"
         := #RAW_STRING               -- hash says "string, not pattern"

  NUM :=  [0-9]+                      -- usual unsigned integers
      := +[0-9]+                      -- explicit + is ok
      := -[0-9]+                      -- usual negative number
      :=  inf                         -- infinity
      := +inf                         -- infinity
      := -inf                         -- negative infinity

  Functions applied during matching can implement constant captures and back
  references, and other things.  Back references are achievable because we can
  give the run-time function access to the current capture information as well
  as the input.  We can generalize back references to more complex predicates on
  the current parse tree.

  An application returns a boolean indicating success/failure, a position in the
  input at which to resume matching, and optionally a capture tree to be
  inserted at the current tree position, i.e. to be pushed onto the capture
  stack. 

  constant:(IDENT, LITERAL)   -- return true and a single capture node
  backref:(QUALIDENT)         -- return true or false; advance the input position

  Predicates on capture trees (e.g. fqdn/tld == "org") could be implemented as
  run-time functions that are not allowed to return a capture and are not
  allowed to change the input position.  ANY FUNCTION could be used as a
  predicate if, after calling it for its true/false value, the vm ignores any
  returned capture and ignores the returned input position.

----------------------------------------------------------------------------------------

+ Remove automatic tokenization because it confuses people.  Even I forget about
  it sometimes, and then I get an unexpected result (usually a failure to
  match).


----------------------------------------------------------------------------------------

- Find a new way to control captures.  The 'alias' mechanism is smooth in many
  ways, but it forces the library author to make a choice that the library user
  sometimes wants to override.  Some ideas:

  - The library user could write 'advice'-like annotations to get back captures
    that were suppressed by the library author, and vice-versa.

      - Suppose everything with a name is captured by default, which is the
        Rosie approach.  Could suppress captures using an application syntax and
        an equivalent operator, e.g.
	  foo = nocapture:(bar baz qux)
	  foo = nocapture:(bar) nocapture:(baz) nocapture:(qux)
	  foo = -(bar baz qux)
	  foo = -bar -baz -qux

      - And what if we want to suppress the "bar" capture but KEEP the things
        that bar captures?  I.e. what if we want an RPL alias?  Could scrap the
        previous definition and use these instead:
          foo = nocapture*:(bar baz qux)  ===> Suppress all captures
          foo = nocapture:(bar baz qux)   ===> Suppress bar, baz, qux
	  foo = -*(bar baz qux)  ===> Suppress all captures
	  foo = -(bar baz qux)   ===> Suppress bar, baz, qux

      - Is there a reasonable looking general form, where we can specify "tree
        paths" to capture and to suppress?  Probably, since the syntax would
        surely be verbose, we should separate such "declarations" about the
        output from the pattern definitions themselves.

	  foo = bar baz qux
          ;; Let's try SEXP syntax for "capture declarations"
	  Capture(foo, (-bar))
	      ===> SEXP for: no bar, but keep whatever bar captures
	  Capture(foo, (-*bar +subbar2))
	      ===> SEXP for: nothing from bar, but keep bar.subbar2 and
	           whatever subbar2 captures
	  Capture(foo, (-*bar +*subbar2))
	      ===> SEXP for: nothing from bar, but keep bar.subbar2 and
	           everything in subbar2
	  Capture(foo, (-*bar +*subbar2 (subbar3 +osubsub))
	      ===> SEXP for: nothing from bar, but keep bar.subbar3 and
	           everything in subbar3 AND keep subbar3.subsub and
		   everything in subbar3.subsub

      - Would be simpler to not allow omission of nodes on paths.  So if you
        want subbar3.osubsub, then you get bar, bar.subbar3, and
        bar.subbar3.osubsub in the tree, though without the other sub-matches of
        bar like subbar2.  That would look like this:

          Capture(foo, (-*bar (subbar3 +osubsub))
              ===> SEXP for: nothing from bar, but keep bar.subbar3 because it
                   is on the path, and keep subbar3, subbar3.osubsub and whatever
                   osubsub captures 
        
          Capture(foo, (-*bar (subbar3 +osubsub))
              ===> SEXP for: nothing from bar in general, but keep bar because
                   it is on the path, bar.subbar3 because it is on the path, and
		   keep subbar3, subbar3.osubsub and whatever osubsub captures 

      - Things are complicated by the fact that there is a default set of
        collected items, i.e. what the original pattern author wrote.  Here we
        are trying to override her decisions in some cases, and accept them in
        other cases.  Suppose you either get what the author wrote, or you have
        to specify everything you want?

          Capture(foo, (bar subbar3 osubsub)
              ===> SEXP for: keep bar, bar.subbar3, bar.subbar3.osubsub
        
          Capture(foo, (bar subbar2 (subbar3 osubsub))
              ===> SEXP for: keep bar, bar.subbar2, bar.subbar3, and
	           bar.subbar3.osubsub

          Capture(foo, (bar subbar2 (subbar3 osubsub) (subbar4 ++))
              ===> SEXP for: keep bar, bar.subbar2, bar.subbar3, 
	           bar.subbar3.osubsub, bar.subbar4 and everything below it

  - IF we end up with a control-flow language, then perhaps that is a place for
    it?  There, a library user could specify all kinds of things about how to
    use patterns in processing the input (e.g. matching/searching, what to do
    with the output).  We may even be able to compile OUT unneeded captures,
    based on the control-flow and other information provided by the user.

  NOTE: We may want to prohibit slashes in capture names, so that we can use
  them in tree paths.  The examples above would be clearer if we did not use
  dots as separators, because those are for namespaces.  We might have written
  "foo/bar/subbar2" for clarity.

----------------------------------------------------------------------------------------

- Make it easy to write patterns that are guaranteed to run in linear time, and
  possible to describe languages in PEG that are not regular.

  - Look-around should be bounded by default.  A different operator could be
    required for unbounded look-around, similar to how we prevent accidental
    recursion today in RPL by requiring the use of grammar...end.

  - Are there ergonomic issues with using a different operator for unbounded
    look-around?  Requiring a different operator is a strong signal to the
    pattern developer that she is doing something that we might call
    "asymptotically unsafe" (in the sense that Rust uses the unsafe concept).
    Except, the "different operator" approach seems to sidestep an important
    issue, which is that the 'unsafe' property of a pattern should taint all
    patterns that use it... right?

    After all, we want to ENCOURAGE sharing.  So assume I use libraries A and B
    which were written by other people.  I build my own set of patterns, C, and
    I want to ensure that some/all of my patterns will need only linear time to
    run.  How can I do that if something in A or B uses an unbounded lookahead,
    for example?

    To be precise: Any unbounded pattern, when it is the object of a look-around
    or a Kleene star operator, can cause exponential backtracking.

    How do we let the developer know when this happens, and how does the
    developer let us know when they care and when they don't?

    Forcing the use of, e.g. ** for Kleene star, and << (>>) for unbounded
    lookbehind (lookahead) will make the developer aware of when and where the
    situation occurs, and has the benefit that this information is retained in
    the pattern source, and not discarded with compiler or linter output.

    A compiler directive could be used, if we introduce a pragma or annotation
    syntax of some kind.  E.g.

        @linear foo = A.bar B.baz

    Then the compiler can enforce linearity as a property of foo, which in turn
    requires that property of A.bar and B.baz.  Note that A.bar and B.baz can be
    unbounded AND linear, e.g. maybe A.bar = "a"*.

    In that case, this would fail to compile:

        @linear foo = >>A.bar B.baz

    And the failure is purely due to the linear requirement.  The use of >>, <<,
    or ** would signal where the non-linear behavior originates.

    Do we need to make visible the notion of unbounded versus bounded patterns?
    Perhaps using the same mechanism, e.g.

        @bounded baz = ".com" / ".org" / ".horribly_long_tld"

    If we had this, then the following would fail to compile:

        @bounded bar = "a"*

    Bounded-length patterns will need only constant time to match (or fail),
    although the constant will vary based on the properties of the pattern.

    We could call these "constant-time" patterns, to parallel the use of linear,
    giving something like @const (which is likely to be misinterpreted or simply
    confusing) or @constant.  So maybe we stay with @bounded (in input
    consumption) and @linear (time)?  We could probably rationalize @linear as
    linear input consumption, actually.

    @bounded ==> @linear

    @bounded @linear Pattern
    -------- ------- -------
     T        T      epsilon
     T        T      literal
     T        T      seq(@bounded, @bounded)
     T        T      choice(@bounded, @bounded)
     F        T      rep(@bounded)
     F        T      rep1(@bounded)
     T        T      optional(@bounded)
     T        T      lookahead(@bounded)
     T        T      lookbehind(@bounded)

     F        T?     kleene_star(@bounded)  *** backtracking impl ***
     F        T      kleene_star(@bounded)  *** FSM impl ***

     F        T      seq(@linear, @bounded); seq(@bounded, @linear)
     F        T      choice(@linear, @bounded); choice(@bounded, @linear)
     F        T      rep(@linear)
     F        T      rep1(@linear)
     F        T      optional(@linear)
     F        T      lookahead(@linear)
     F        F      lookbehind(@linear)

     F        F      kleene_star(@linear)  *** backtracking impl *** (quadratic, cubic, ...)
     F        T      kleene_star(@linear)  *** FSM impl ***
    
     F        F      seq(nonlinear, <anything>); seq(<anything>, nonlinear)
     F        F      choice(nonlinear, <anything>); choice(<anything>, nonlinear)
     F        F      rep(nonlinear)
     F        F      rep1(nonlinear)
     F        F      optional(nonlinear)
     F        F      lookahead(nonlinear)
     F        F      lookbehind(nonlinear)

     F        F      kleene_star(nonlinear)  *** backtracking impl ***
     F        T?     kleene_star(nonlinear)  *** FSM impl ***
    
     F        F      grammar using arbitrary recursion
     	      	     ** Can we recognize recurrence equations with an exponential solution? **

     Consider unordered non-possessive choice? 'a?a?a?aaa' gives exp time in
     number of a's against input "aaa".  We'd presumably use the
     continuation-based transformation algorithm, which would give something
     like this for a?{11}a{11}:
       rosie --verbose --rpl 'A = "a"; k1={A k2}/k2; k2={A k3}/k3; k3={A k4}/k4;
       k4={A k5}/k5; k5={A k6}/k6; k6={A k7}/k7; k7={A k8}/k8; k8={A k9}/k9; k9={A
       k10}/k10; k10={A "aaaaaaaaaa"} / "aaaaaaaaaa"' match -a --time '{{{A k1} /
       k1} $}' <<< aaaaaaaaaa 

----------------------------------------------------------------------------------------

- Pattern debugging requirements:

  - Debugging (tracing, breakpoints) a grammar should be possible.
  - Breakpoints should be supported on pattern names.
  - Tracing a set of patterns (instead of everything) should be possible.


----------------------------------------------------------------------------------------

+ lookbehind should be possible for non-fixed-length patterns.  In other words,
  remove the lpeg restriction that a lookbehind pattern must have a fixed
  length.  Among other reasons to do this, the fixed-length restriction prevents
  looking behind for a single character within a char set, because with UTF-8
  encoding, one char in the set could be a single byte and one multi-byte,
  e.g. [a☃].

  Another reason to CONSIDER removing the fixed-length restriction is that it is
  not mellifluous.  It is a sharp edge on a feature in a formalism (PEG) that is
  otherwise friendly to the hands.  Of course, lookbehind is NOT an operator in
  the PEG definition at all!  Regex users seem to want lookbehind, like they
  want back-references, for the convenience.

  Defining the semantics for a lookbehind operator in the PEG world is
  surprisingly fraught.  See the separate file lookbehind.txt for a discussion.

----------------------------------------------------------------------------------------
NOTES on regex greedy/lazy options.

E.g. problem is "capture the last ipv4 address in the input".

  Conceptually, the regex that comes to mind is /.*(IPV4)/ where IPV4 does the
  right thing.  It's likely that we don't think at all about greedy versus lazy,
  because we sort of know that regex finds the longest match by default.

  Of course, if the default were lazy, then /.*(IPV4)/ would capture the first
  instance, not the last one.

  Note that it is irrelevant whether our implementation uses a FSM
  implementation or does backtracking -- we still rely on the longest match
  concept when we write that regex.

  How would we write this in a PEG language like RPL?
  We could use the 'find' macro:
     find:{ipv4 !find:ipv4}
  where find:X expands to:
     grammar alias <s> = {!X .}* in find = {<s> X} end
     
  In Rosie 1.2.2 and earlier, this executes exactly as written, i.e. the pattern
  X is attempted at every byte position in a forward search for the pattern.
  Since the pattern, in this case, contains another search, we expect O(n^2)
  behavior in the length, n, of the input.

  Intriguingly, it is cheaper to capture all instances of ipv4 in the input, and
  then in post-processing retain only the last in the list.  E.g.

  $ rosie match -w -o json 'findall:net.ipv4' /etc/resolv.conf | jq '.subs|select(.type?="net.ipv4")|last'
      {
	"type": "net.ipv4",
	"s": 382,
	"e": 389,
	"data": "8.8.4.4"
      }
  $ 

  Modulo some idiosyncratic optimization, a backtracking regex engine will
  process /.*(IPV4)/ by zipping immediately to the end of the input and then
  trying to match IPV4 against each input position from just beyond the last one
  (in case IPV4 is nullable) backwards to the start.  I.e. "dot star matches as
  much as possible".

  OBSERVATIONS:
  (1) The RPL 'find' macro behaves like a lazy dot star, e.g. /.*?(IPV4)/
  (2) The RPL 'findall' macro returns all the matches, whereas multiple API
      calls are typically needed to do this with regex.
  (3) Nested uses of 'find' increase the exponent in the polynomial of the
      worst-case complexity.
  (4) We could support an efficient implementation of a 'findlast' operation in
      RPL.  Do we know anything about how often "find the last X" is a use case?
      I recall that the dot-star construct appears fairly often in regex in the
      wild, and so perhaps that is a good proxy measurement for "find the
      last X". 
  (5) If a programmer decided to write explicit string operations instead of
      using regex, they might think of the search direction and the stopping
      criterion independently.  It's possible to search from the end of the
      string backwards, stopping with the last match before the starting
      position (thereby returning the first match), or vice versa.  Regex
      always finds the first match, but allows the pattern writer to choose the
      search direction.

      UNDERSTANDING GREEDY VS LAZY

      Is it even a valid understanding of regex greedy/lazy to say that "/.*X/
      gives the last X and /.*?X/ gives the first match"?  This seems to
      describe their use in practice, but there must be nuances due to the fact
      that Kleene star is affected by everything that comes after it in the
      pattern, i.e. the entire continuation.

      Is there a use case for giving the programmer more control here, e.g. to
      separate the direction of search from the stopping criterion?  I can't
      think of a good one.  And yet, the greedy/lazy option for regex star is
      unsatisfying from a PL design perspective... I cannot yet identify why,
      but it may have to do with requiring the pattern writer to think about
      each repetition in her pattern interacting with all of the others in terms
      of longest and shortest matches.

      Maybe that's the issue that I am circling around.  The meaning of Kleene
      star depends on everything that comes after it in the pattern, which can
      include more repetitions.  Doesn't that mean that the pattern writer has
      to consider how they interact?  Or does this reduce to a local statement
      about the first (nearest) versus last (farthest) matches?

      DECLARATIVE VS IMPERATIVE/FUNCTIONAL

      The popularity of PEGs suggests to some that their resemblance to parser
      combinators is important, i.e. that the pattern writer is thinking in
      terms of control structures.  I've always suspected that regex should have
      always been regarded as a declarative language, but it's been so
      contaminated with other ideas (backtracking, subroutines, recursion,
      semantics-altering flags) that in practice it's a bit of both imperative
      and declarative.  In other words, a mess.

      I suspect (with no evidence at hand) that the best replacement for regex
      will either go all in as a declarative language or abandon that pretense
      and give the developer a way to say what string operations she wants to
      execute and how.  The latter approach still leaves room for compiler
      optimizations, and even run-time ones.  (I can imagine a tracing engine
      that adapts to the data being processed by changing the way certain parts
      of the pattern are matched on the fly.)

      Here's the rub: Go too far in that direction, and you build a programming
      language, maybe SNOBOL or awk or (shudder) Perl.

      That glimpse of "regex future" is to scare me back to considering a fully
      declarative approach.  The way the Rosie system separates pattern matching
      from output processing is relevant here.  The output processing is
      arbitrary code.  It walks a tree.  We provide no language for it -- you
      can extend Rosie in Lua, in C, or externally with tools like jq.

      THE RPL ALIAS SPANS BOTH PHASES

      One aspect of Rosie/RPL seems to straddle the two phases of pattern
      matching and output processing: the 'alias' capability.  An 'alias' is the
      simplest of substitution macros (at the expression level).  It prevents a
      name from appearing in the output, and is essentially a manual
      optimization.  After all, we could simply ignore parts of the output that
      we don't care about.  But by declaring aliases up front, we save the cost
      of collecting their data.  There are other ways to approach this
      optimization, though.

      We could let the pattern writer give us a schema that shows the fields
      they want us to capture, for example.

      Or, we could provide a language for output processing, and we could derive
      from an output encoder which pattern names to capture and which we can
      discard.  E.g. the boolean output encoder (which we have today!) would
      name no fields at all, and so none would be captured.  The result is a
      match or no match.  One imagines a 'span' output encoder that returns only
      the span of the match, with no other data and no sub-matches.

      Let's also reconsider the example problem of finding the last IPV4 in the
      input.  If the output encoder referenced only the last match, we could
      optimize the matcher to keep only the last one, i.e. to use constant space
      in the capture data structure.

      HOW TO NOT CARE

      To fully address the regex use cases that we infer from the existence of
      common features like greedy/lazy repetition, lookaround, back references,
      and named captures, we need a good way to express parts of the input that
      we don't care about.  Specifically, there are different ways not to care.

      In /.*(IPV4)/ we don't care if the .* contains any matches to IPV4.  In a
      recursive regex or a PEG in which we match balanced brackets, the parts of
      the input that we don't care about must not contain whatever brackets we
      are balancing.  In /.*?(IPV4)/ the .* must not contain an IPV4, but the
      implied .* at the end of the expression can.

      When I teach automata theory, at some point in the semester, I start
      allowing students to label an arrow with a * to mean "any symbol in Sigma
      that is not already shown on an arrow leaving the same state".  It is a
      shorthand that works well for writing, reading, and grading automata
      diagrams.  I think it also works well for thinking, especially when the *
      arrows lead to an error state and we naturally read them as "anything
      else".  Maybe there's an analogous notion in text-matching patterns, where
      we can devise a way to denote a span of input containing "things I don't
      care about"?
      
      NOT CARING IS AN ASPECT OF SEARCH

      In the previous section, we talked about what input it is ok to skip over,
      i.e. what we don't care about.  The context for skipping over anything is
      that we are searching for something.
      
      Let's dissect the idiom underlying the 'find' macro.  'find:X' expands,
      essentially, to:

          grammar alias <s> = {!X .}* in find = {<s> X} end

      I see two inefficiencies and one bit of awkwardness here.  The 'find'
      macro relieves us from specifying X twice, which is good, but there is an
      awkward reference to dot in the expansion.  Utilizing dot in the expansion
      is essential if we want to leverage whatever definition dot has in the
      prelude.  (It may be defined as ASCII-only, or as a single byte, or as a
      UTF-8 character, or something else.)  However, the reference to dot is
      awkward in how it defines the size of an iteration in the "search" loop
      called <s>.  The dot forces the search to proceed one character (or byte
      or whatever) at a time, even when it could go faster.

      This calls out for an abstraction for searching that we could use in place
      of macro definitions like the one for 'find' which specify small search
      steps.

      Perhaps with a better abstraction, the inefficiencies can be eliminated.
      First, it's easy to see that the search stops when X is matched, and
      immediately afterwards, X is matched again.  Surely we could arrange to
      match X just once!  Second, each time X fails to match, a backtracking PEG
      matcher will back up to where the attempt started, and then advance by
      whatever dot consumes.  In many cases, the matching engine should be able
      to advance more than just one character (byte, etc.) before trying again.
      A pre-computation like the one in Knuth-Morris-Pratt could be used to know
      how far is safe to skip ahead.  The point here is that the (current, in
      Rosie 1.2.2) expansion of the 'find' macro is forced to search one
      character at a time for lack of any alternative.

      DESIGNING SEARCH

      So, if a "search" primitive is warranted, what might it look like?

      Let's start with how it might be used.  Would it simply replace the 'find'
      macro?  I.e. 'search:X' would skip ahead in the input, using whatever
      technique the compiler can prove correct (and hope is also efficient).

      The 'find' macro also consumes the X that it finds.  The related macro
      'keepto' captures everything up to and including X.  One could imagine a
      (possibly) useful 'skipto' macro that would search for X but leave the
      cursor just at the start of the X.

      Which of these behaviors should 'search' have?  What other variants may be
      needed?

      (1) First let's look at variants that consume X.  Should X be captured?
      That can be determined by X itself, i.e. if X captures, then yes.  Should
      the input that we skip over while searching be captured, like 'keepto'
      does?  In RPL today, if we name an expression that contains a use of
      'find', we will capture everything up to and including the found pattern.
      If that expression is an alias, only the target pattern, X, is captured.
      Unless that alias is used within a named expression...  We can see how
      this gets tedious, because RPL almost conflates naming with capturing.
      (The 'alias' feature prevents them from being totally coupled.)

      For reference, compare the outputs of these commands, recalling all the
      while that the RPL match type '*' is the capture of the entire anonymous
      command-line expression:
      rosie match -w -o jsonpp 'find:net.ipv4' /etc/resolv.conf
      rosie --rpl 'foo = find:net.ipv4' match -w -o jsonpp foo /etc/resolv.conf
      rosie --rpl 'alias foo = find:net.ipv4' match -w -o jsonpp foo /etc/resolv.conf
      rosie --rpl 'alias foo = find:net.ipv4; bar = foo' match -w -o jsonpp bar /etc/resolv.conf

      Because I don't want to digress into an exploration of how captures might
      work in Rosie 2.0 (i.e. using the PEXL engine), I'll punt on the question
      of whether 'search:X' captures X.  Let's say that if X captures, then the
      result of 'search:X' will include a capture of X.

      Similarly, we can punt on the issue of whether the input leading up to X
      is captured, because we can imagine a more fine-grained control of
      captures in a PEXL-based implementation that we have today in Rosie
      1.2.2.  For example, 'capture(search:X, "foo")' would presumably capture
      everything up to and including X.  And if X was a capturing pattern, there
      would be a sub-match for X itself.

      (2) Second, let's look for use cases in which the search stops at the
      start of X (and therefore does not consume X).  Today, in Rosie 1.2.2, we
      can use 'find:>X' to achieve this.  The search implemented by 'find' stops
      "when looking at X".  A use case for this is when X marks the start of
      something, like a block of code or the beginning of a record structure.
      In such situations, we go on to parse the code/structure/whatever using a
      different pattern.  (E.g. in rpl_1_3.rpl we use 'binding_prefix' to detect
      the start of a new binding, so that we can tell where the previous
      definition ends.  We go on to parse a binding with a different pattern,
      though.)

      At present, I see no reason not to continue using "search for looking at
      X" as the idiom for a search that finishes with the current position at
      the start of the target pattern, X.

      Modulo look-behind (see other section, below), we can conclude that
      'seq(search:>X, X)' is equivalent to 'search:X', and probably the compiler
      can do this replacement (or a similar one) to ensure that X is fully
      matched just once.

      (3) Next, let's consider the use cases driving the "lazy dot star" in
      regex, as in our earlier example /.*?(IPV4)/.  This case should be
      addressed straightforwardly as 'search:IPV4', presuming IPV4 captures.

      (4) And what about the regex "greedy dot star"? E.g. /.*(IPV4)/ Should we
      think about the meaning of this expression as "find the last IPV4 and
      capture it"?  It's really more like "find the shortest sequence at the end
      of the input that can match IPV4" (because IPV4 may match variable length
      inputs). 

      There is an implicit reference to the end of the input (and, crucially, to
      the rest of the regex after IPV4, which in this simple example is empty).
      "The rest of the regex" after the .* is an undelimited continuation, which
      prompts the question, "what would an operation based on a delimited
      continuation be like, and would it be useful"?

      One obvious example is in parsing a stream of input symbols.  This case
      makes sense only when there is a (recursive) set of patterns that we are
      looking for.  In the most basic cases, we are looking for a finite number
      of "record types".  Maybe we are lucky and they are easily distinguished
      by a small prefix, making any parsing technology very efficient.  In such
      a case, we may need to express "match the last IPV4" where "last" means
      "before the end of the current record" or "before the start of a new
      record".

      THIS CASE aligns nicely with the concept I wrote about several years ago
      in which matching could be "scoped" to a segment of the input.  I had been
      thinking of a "static" determination of the scope, i.e. first match the
      segment of the input that will be the scope, then re-parse that segment
      using a different pattern.  

      Earlier, I wrote:

        It's also possible to think about a dynamic variant, in which we are in
        the middle of parsing something -- call it R for record -- and we now
        want to match the "last IPV4 in R".  The limit of our input segment (the
        end of R) may be found during the search for IPV4.  Or, we may match
        IPV4 before/at the end of R.

      But now I am less convinced that a dynamic variant makes sense, except
      perhaps as an optimization in which the region matched by R and the
      matching of X occur simultaneously.

      In the past, I've written expressions like 'in:(R, X)' to mean "in the
      span of the input that matches R, return what X matches".  This is the
      semantics that makes the most sense to me.  The pattern writer should not
      have to think about edge cases in which a certain input sequence can match
      both R and (part of) X.  The semantics should be simpler: A match for R
      establishes the region of the input in which the pattern X is matched.  By
      sequencing the two matching operations, the meaning of 'in:(R, X)' is
      (imo) easier to think about.

      Question: When X contains a lookahead, should it be able to look beyond
      the end of the region defined by R, like a regex lookahead within a
      lookbehind can look past the position at which the lookbehind began?

      The notion of an operation like 'in:(R, X)' bears further examination
      using the delimited continuation concept.  In the case in which R is the
      entire input, this devolves to the regex case.  This is no surprise,
      because in languages with undelimited continuations, like Scheme, in
      practice continuations are delimited by some top-level construct like the
      REPL or a debugger.
      
      See cont.txt for more.

----------------------------------------------------------------------------------------
NOTES on how regex process lookarounds.

Examples use Python version 3.9.0:
    '3.9.0 (default, Oct 27 2020, 14:15:17) \n[Clang 12.0.0 (clang-1200.0.32.21)]'

(1) Most appear to allow captures in lookahead and lookbehind

      >>> re.match('abc(?<=abc)', 'abcdef')
      <re.Match object; span=(0, 3), match='abc'>
      >>> re.match('abc(?<=abc)', 'abcdef').groups()
      ()
      >>> re.match('abc(?<=a(b)c)', 'abcdef').groups()
      ('b',)
      >>> 

(2) Most (according to regex info websites) do not attempt to run backwards, and
    instead allow only fixed-length lookbehind patterns.  (Reportedly, the
    exceptions are JGsoft and .NET.)  Fortunately for developers, the length
    appears to be measured in characters, not bytes:

      >>> re.match('a☃(?<=☃|o)', 'a☃')
      <re.Match object; span=(0, 2), match='a☃'>
      >>> 
    
    However, there is no canonicalization performed:
    
      >>> len('é')                # latin small e; combining acute
      2
      >>> len('é')                # latin small e with acute
      1
      >>> re.match('aé(?<=é|☃)', 'a☃')
      >>> re.match('aé(?<=é|☃)', 'a☃')
      <SNIP>
      re.error: look-behind requires fixed-width pattern
      >>>       

(3) Within a lookbehind, we can lookahead past the point where the lookbehind
    started, and vice-versa: 

      >>> re.match('abc(?<=ab(?=cdef)c)', 'abcdef')
      <re.Match object; span=(0, 3), match='abc'>
      >>> re.match('abc(?=d(?<=abcd))', 'abcdef')
      <re.Match object; span=(0, 3), match='abc'>
      >>> 

(4) The notion that lookbehind finds a place where its target pattern matches
    DOES SEEM REASONABLE, provided that the target pattern consumes up to the
    start of the lookbehind.  The important part is that consumes that far,
    since it may look ahead past the start point.

      IF we decided that this is the right semantics, then putting a fencepost
      at the start of the lookbehind is not the right implementation (nor mental
      model): it can't mark the end of the input if we're allowed to look beyond
      that point.

      Is the regex mental model a good one?  At least in the Python (package
      're') implementation, it exhibits a certain consistency of behavior.
      Though if it allowed non-fixed-length lookbehind targets, would the
      developer's mental model become overly complex?

      Important: The regex model, as exemplified in Python re, says that the
      lookbehind succeeds by finding a position before the current one at which
      the target pattern succeeds AND consumes every character up to (but not
      including) the current one.  The "consumes" is important, because a
      lookahead within the target pattern can look beyond the current position.

      For the purpose of optimization, we should ensure in PEXL that the
      lookbehind target does not consume beyond the current position at the time
      of entering the lookbehind.  Simultaneously, though, we must allow
      any lookahead within the target pattern to see beyond the current
      position. 

      Digression on an efficient implementation
      
        We could implement a "transparent fencepost", one that we can see
  	through but not move through.  But today, something we inherit from lpeg
  	is that we do not distinguish between looking and consuming -- there is
  	no "mode" indicating one or the other.  In the lookahead case, we don't
  	need a fencepost.  The actual end of the input is the appropriate
  	endpoint.  But in lookbehind, we can construct a transparent fencepost
  	where the lookbehind started.  If we try to "consume" past there (we are
  	really not consuming, we are looking behind), we will fail.  But we can
  	lookahead past there, which means that we need to know we are looking
  	ahead within a lookbehind context.

	This suggests:
	(1) Matching the target of lookbehind is a dynamic condition; i.e. we
	    must establish a lookbehind context on entering the target pattern,
	    and remove it when leaving (unwind).
	(2) That context should include the current position on entering the
	    target, so that we can be sure never to consume past there.  Let's
	    call that a "fencepost".
	(3) Lookahead must similarly have a context, so that while executing the
	    target of the lookahead, we do not pay attention to the fencepost.
	    And we remove this context when leaving the lookahead target (by
	    unwinding, as usual).  
      
      Fortunately, achieving an efficient implementation appears to be
      independent of the developer's mental model.  That is, the developer need
      only know the contract that lookbehind provides.  To test that hypothesis,
      let's look at how patterns containing lookarounds combine.

      We may write lookahead(X) where X consumes a 'c' and looks behind for
      'abc'.  So the pattern lookahead(X) can fail depending on the characters
      BEFORE the current position.  That's surprising on the surface, but not so
      much if our mental model is that "X matches a 'c' as long as 'ab' precedes
      it."  And that aligns nicely with the definition:

      	X = seq('c', lookbehind('abc'))
        match(seq('ab', X), "abc") ==> matches the entire input "abc"
        match(seq('ab', lookahead(X)), "abc") ==> matches "ab"

	Rosie v1.2.2 behaves this way today:
	 $ rosie --rpl 'X="c"<"abc"' match '{"ab" X}' <<<"abc"
	 abc
	 $ rosie --rpl 'X="c"<"abc"' match '{"ab" >X}' <<<"abc"
	 abc
	 $ 

      One consistency check we should do is to make sure this alternate
      definition gives the same match results as above:

      	X = seq(lookbehind('ab'), 'c')

      Note that seq(lookahead('d'), 'd') can FAIL even when the next character
      is 'd', if executed in a lookbehind context.  This is not only surprising,
      it could foil local optimizations.

      >>> re.match('abc(?<=(?=d)d)', 'abcdef')      # Lookbehind alters (?=d)d
      >>> re.match('abc(?<=(?=d)c)', 'abcdef')      # Surprise!
      >>> re.match('abc(?<=(?=d))', 'abcdef')       # Devolves to lookahead
      <re.Match object; span=(0, 3), match='abc'>
      >>> re.match('abc(?=d)d', 'abcdef')           # As expected
      <re.Match object; span=(0, 4), match='abcd'>
      >>> 	

      That seq(lookahead('d'), 'd') can FAIL (when the next character is 'd') in
      some contexts is bad for composability, no?  Admittedly, what is happening
      is that the meaning of "the next character is 'd'" has changed (next has
      become previous).

      Rosie v1.2.2 has the following behavior.

	$ rosie match '{"ab" {>"c" "c"}}' <<<"abcdef"
	abcdef
	$ rosie match '{"abc" <{>"c" "c"}}' <<<"abcdef"
	abcdef

      Clearly, BACKWARDS and FORWARDS are absolute directions in regex, in the
      sense that "look ahead" does NOT mean "look ahead in the direction of
      matching".  Otherwise, lookbehind would "reverse the direction", and this
      would succeed (match):

      >>> re.match('abc(?<=(?<=d))', 'abcdef')
      >>> 

(5) The regex mental model is certainly NOT one of running the engine backwards!
    These results would be opposite if we thought of lookbehind as moving the
    cursor backwards in the input:

      >>> re.match('abc(?<=c(?=cd))', 'abcdef')
      >>> re.match('abc(?<=c(?=d))', 'abcdef')
      <re.Match object; span=(0, 3), match='abc'>
      >>> 

      Clearly, from the results immediately above, the mental model of
      lookbehind must be "find a place before/at the start of the lookbehind
      where the target pattern matches in such a way that it would have consumed
      exactly up to that start point".

(6) According to https://www.regular-expressions.info/lookaround.html (Thu Dec
    24 14:17:23 EST 2020) Java has been removing restrictions on lookbehind
    target patterns since Java 4, but the results are unreliable:

    "Java 13 allows you to use the star and plus inside lookbehind, as well as
    curly braces without an upper limit. But Java 13 still uses the laborious
    method of matching lookbehind introduced with Java 6. Java 13 also does not
    correctly handle lookbehind with multiple quantifiers if one of them is
    unbounded. In some situations you may get an error. In other situations you
    may get incorrect matches. So for both correctness and performance, we
    recommend you only use quantifiers with a low upper bound in lookbehind with
    Java 6 through 13."


----------------------------------------------------------------------------------------

- Explore the idea of a control-flow language.  A minimal language would do
  everything we can do with Rosie 1.2.2 on the command line, such as choosing
  between 'match' and 'search' functions (the latter currently named 'grep'),
  setting the output encoder, and assigning colors to types.

  Syntax could be anything, but I'll use S-expressions here, and assume a
  Scheme-like semantics.

  IMPORTANT: Whatever the final language, it can be highly specialized for
  string handling, assuming that is possible without ending up with Perl.

  ;; Find each match to net.ip, printing it in color, until end of input
  (define run
    (repeat
      (println (search "net.ip") 'color)))
    
  ;; Print the .org URLs in plain text
  (define dotorg
    (repeat
      (let* ((addr (search "net.url"))
             (domain (extract addr "net.registered_name")))
        (if (eq (substring domain -1 -5) ".org")
	    (println addr 'text)))))


  ;; 

----------------------------------------------------------------------------------------

+ Repetition of a nullable pattern
    Not supported in lpeg, but what is the reason to outlaw it?
    Can we amend the current code generation strategy to allow for the argument
    pattern, p, to be nullable?

    Current code generation strategy for repetition:

    (0) The most special case: if exp can be converted to a charset, issue an
        ISpan instruction

    (1) Special case: if HEADFAIL(p) or
	(not NULLABLE(p) and DISJOINT(FIRST(p), FOLLOW(p))) 

      L1: testset FIRST(p), on failure -> L2;
	  <p>;
	  jmp L1;
      L2:

    (2) General case, no optimization

	  testset FIRST(p), on failure -> L2; // OMIT if NULLABLE(p) or RUNTIME(p)
	  choice L2;                   // on next failure, restore, -> L2
      L1: <p>; 
      	  ADD THIS: fail if we have not advanced since posn on top of BTStack
	  partialcommit L1;            // update restore point
      L2: 

    (3) General case, optimization due to choice on stack
        (Will INFINITE LOOP when p is nullable.)

	  partialcommit L1;    // accounts for 0 repetitions
      L1: <p>;                 // if p fails, then backtrack
      	  ADD THIS: fail if we have not advanced since posn on top of BTStack
	  partialcommit L1;    // commit to another repetition, try for more


- Captures of repetition of nullable patterns: How does our approach compare to
  what popular regex engines do?

  Does (a?)* matched against "" generate a zero-length capture?
  What about ((a?)*)?
  

----------------------------------------------------------------------------------------

- How to accommodate an API that supports both match() and search(), like Perl
  does, and also fullmatch(), findall(), and finditer() like Python's re does?

  Args: pattern, text, byte positions pos and endpos
  N.B. the slice text[pos:endpos] does not include text[endpos]

  match()     look for a match that starts at pos and is contained in [pos:endpos]
  search()    look for a match starting at pos or later, contained in [pos:endpos]
  fullmatch() entire text[pos:endpos] must match the pattern
  findall()   return list of non-overlapping SEARCHES for pattern in text[pos:endpos]
  finditer()  iterator/generator for findall()

  Below, 'pat' neither starts with ^ nor ends with $.      

  match(^pat) === match(pat)
  match(^pat$) === match(pat$) === fullmatch(pat)
  search(^pat) === match(pat)
  search(^pat$) === fullmatch(pat)
  findall(pat) === LOOP: cons(search(pat, pos=prev_match_end), results) until no match
  findall(^pat) === list(match(pat)) if a match, else empty list
  findall(pat$) === list(search(pat$)) if a match, else empty list
  findall(^pat$) === list(fullmatch(pat)) if a match, else empty list

  Note that findall(pat) is NOT the same as search(pat*).  The former returns a
  list of search results, and the latter searches ONCE for zero or more
  contiguous repetitions of pat.
  Also, findall(pat) is NOT the same as the RPL 'find:pat' because of the format
  of the return value.  They both return the same substrings, but findall(pat)
  returns a list and 'find:pat' returns a single result that contains a list of
  sub-matches.  The difference is small but (perhaps) important.

  Option (1): Implement a SEARCH instruction.

    Retain the current simple low-level API that implements match().

    (a) match() is implemented by the low-level API.
    
    (b) To build search(), construct the initial trampoline such that it uses a
    new SEARCH instruction whose operand is the firstset of pat.  We have that
    firstset stored in the symbol table in the metadata for pat.

    Strategy: SEARCH does a fast byte scan starting at the current position for
    any byte in the firstset of pat.  If SEARCH fails, the entire pattern fails.

    	        CHOICE on failure jump to repeat
		goto search
    	repeat: ADVANCE
	search:	SEARCH <index_of_pat>                // on failure, goto failtwice(*)
	        XCALL <user_pkg, index_of_pat>       // on failure, will jump to repeat
		COMMIT                               // pop choice off backtrack stack

    (*) This implementation of SEARCH requires a choice to be on the backtrack
    stack.  Will need to add a defensive check against popping an empty stack to
    avoid a crash in the case of bad bytecode in which a SEARCH (or an existing
    FAILTWICE instruction) is unguarded by a choice instruction.

    In future, compiler can emit a different search instruction when the pattern
    starts with a long enough string.  E.g.
    	        STRINGSEARCH <searchstring_index>
    Such an instruction could use Boyer-Moore or KMP or similar, with the
    precomputed data already stored at <searchstring_index>.

    UNLESS pat starts with the anchor ^, which would invalidate the above
    approach.  However, we could emit a new instruction for ^ that checks to see
    if we are at position pos.  This is the mirror of ITestAny.

    (c) findall() is a series of calls to search(), each starting where the last
    one left off, to produce a possibly-empty sequence of non-overlapping
    results. 

    BENEFITS:
    - Retain simple low-level API.
    - Easy to add special path that uses BM or KMP.
    - The RPL 'find:' macro can probably be implemented simply using SEARCH.
    - If pat is already written as a search, its own internal SEARCH instruction
      will be redundant, which will cost some cycles but cause no harm.  If this
      cost turns out to be important, we can probably optimize it away.

  Option (2): Compiler generates both match and search variants of pat.

    Retain the current simple low-level API that implements match().

    The high-level API then looks up pat in the symbol table and finds two
    entries, one tagged 'match' and one 'search'.  Presumably they share code in
    the instruction vector.

    This would allow the compiler to generate search code that is more tailored
    to the target pattern than the generic SEARCH concept sketched above.

    But, what instructions would be emitted in such a scenario, and how would
    they be implemented in the vm?  It seems that the concepts of SEARCH and
    SEARCHSTRING from option (1) above would enable reasonably good performance
    always.  What kind of pattern would benefit from a different approach?

    No clear benefits here, and the pollution of the symbol table with extra
    patterns is ugly.

  Option (3): Provide multiple low-level APIs to the vm.

    In this option, we build the concept of searching into the vm.  It's not
    clear if there is a better way to do this than by implementing SEARCH and
    SEARCHSTRING as outlined in option (1) above.

    If I recall correctly, one of the special cases used in some regex engines
    is to search the input text for ANY long fixed string that is present in the
    pattern, thereby quickly eliminating input texts that do not contain the
    string.  But even this technique might be better implemented using the
    proposed SEARCHSTRING instruction, but emitted in a different context from
    option (1).  E.g. if SEARCHSTRING succeeds, then backtrack to where we
    invoked SEARCHSTRING and do a repeated 'SEARCH; CALL' loop.  Or, if the
    pattern length before the fixed string is fixed, we could bring back the
    IBehind instruction.

    No clear benefits here, particularly because we want to support the notion
    of the RPL 'find:' macro at the language level, not only in the API.  To
    support it in the language (so it can be used within a pattern), we either
    retain the current implementation of {{!pat / .}* pat} which is already
    supported by the vm, or we need to use new instructions like SEARCH and
    SEARCHSTRING to provide a more efficient implementation.

  SOLUTION: Follow option (1), which requires the following steps.
    - SEARCH can start as a NOOP, which gives us the current 'find:'.
    - Better: Implement SEARCH for firstset.
    - Best: Also implement SEARCHSTRING.
    - Add a new instruction for ^ that is like ITestSet but looking backwards.
    - Add a guard against popping empty BT stack -- today it's only an assertion.

----------------------------------------------------------------------------------------

