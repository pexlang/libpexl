# Tuesday, June 13, 2023

Revised set of operations to implement:

* Exactly n repetitions
* From 0 to n repetitions, i.e. at most n
* Any number of repetitions, i.e. 0 or more

## Exactly n

    ;; ASSUME n > 1
    TESTSET first(P) Ldone  ; early fail
	LDLOOP n                ; push acc, then acc <- n
    CHOICE Lfail			; must be on top of BTStack for LOOPCOMMIT
    Lloop:
      <P>                   ; on error, branch to Ldone
	  LOOPCOMMIT Lloop      ; partialcommit with acc decr, jmp Lloop if acc > 0
    COMMIT Ldone            ; pop the BTChoice from top of stack
    Lfail:
	  FAIL                  ; choice already popped, so unwind to next choice
    Ldone:
	  POPLOOP               ; restore old acc value from top of stack
    
## At most n

    ;; ASSUME n >= 1
    TESTSET first(P) Ldone  ; early fail
	LDLOOP n                ; push acc, then acc <- n
    CHOICE Ldone			; must be on top of BTStack for LOOPCOMMIT
    Lloop:
      <P>                   ; on error, branch to Ldone
	  LOOPCOMMIT Lloop      ; partialcommit with acc decr, jmp Lloop if acc > 0
    COMMIT Ldone            ; pop the BTChoice from top of stack
    Ldone:
	  POPLOOP               ; restore old acc value from top of stack

## Any number (zero or more)

    TESTSET first(P) Ldone  ; early fail
    CHOICE Ldone			; must be on top of BTStack for LOOPCOMMIT
    Lloop:
      <P>                   ; on error, branch to Ldone
	  PARTIALCOMMIT Lloop   ; partialcommit with acc decr, jmp Lloop if acc > 0
    COMMIT Ldone            ; pop the BTChoice from top of stack
    Ldone:
	  POPLOOP               ; restore old acc value from top of stack



# Saturday, December 31, 2022

Notes on implementing loops for "at least n" and "at most n" repetitions

  - Perhaps using a new VM instruction for conditional branch, e.g. BRGZ, BRZ?
  - Dedicate a local var (vm register) for the loop counter
    - When to save the loop counter to the stack?  Certainly for a CALL/XCALL.
      But what to do when the repeated pattern, P, contains a loop also?
      Currently, we store such values on the stack (BTBackLoopContents).  That
      might be slower but it is certainly simpler.  And loop unrolling will
      reduce the number of times we actually need the stack-stored value.
  - Introduce a LOAD instruction to set the loop counter
  - Introduce a DECR instruction to decrement the loop counter
  - Could fuse the common sequences of these into superinstructions
    - E.g. "DECR; BRGZ label" ==> "LOOP label"
  - Possible templates:

    ;; "At least n copies of P" (n > 1):
    ;; OPTION 1: Code the "mandatory n copies" loop, then a "P*" loop
          LDLOOP n			; and push old value of LOOP register
    Lloop:
          <P>                           ; on error, fail
          LOOP Lloop			; decr LOOP, then branch if LOOP > 0
	  POPLOOP			; restore old LOOP value from top of stack
	  TESTSET first(P) Ldone	; early fail for P*
          CHOICE Ldone
    Lstar:
	  <P>
	  PARTIALCOMMIT Lstar
    Ldone:				; P failed, so BTChoice was popped from stack

    ;; "At most n copies of P" (n > 1):
    ;; OPTION 1: Code the "up to n copies" loop
	  TESTSET first(P) Ldone        ; early fail
          LDLOOP n			; and push old value of LOOP register
          CHOICE Ldone			; must be on top of BTStack for PARTIALCOMMIT
    Lloop:
          <P>                           ; on error, branch to Ldone
	  PARTIALCOMMIT Lcont           ; just a fallthrough, actually
    Lcont:
          LOOP Lloop			; decr LOOP, then branch if LOOP > 0
	  COMMIT			; pop the BTChoice from top of stack
    Ldone:				; P failed, so BTChoice was popped from stack
	  POPLOOP			; restore old LOOP value from top of stack

    ---------------------- ALTERNATIVE ----------------------

    ;; "At least n copies of P" (n > 1):
    ;; OPTION 2: Consume as many copies of P as we can, then check loop counter,
    ;;           using ONLY ONE copy of P.
	  TESTSET first(P) *fail*       ; special in-built label?
          LDLOOP n			; and push old value of LOOP register
          CHOICE Ldone			; must be on top of BTStack for PARTIALCOMMIT
    Lloop:
          <P>                           ; on error, jmp to Ldone
          LOOP Lloop			; saturating (stops at zero), and
	       				;  does NOT pop old LOOP value from stack
	  PARTIALCOMMIT Lloop
    Ldone:
	  FAIL if LOOP > 0		; conditional fail, a new instruction
	       	       	 		; if LOOP==0, restore LOOP register from stack.
					; else fail, which triggers restore via unwinding.

    ;; "At most n copies of P" (n > 1):
    ;; OPTION 2: Code the "up to n copies" loop using ONLY ONE copy of P.
	  TESTSET first(P) Ldone        ; early fail
          LDLOOP n			; and push old value of LOOP register
          CHOICE Ldone			; must be on top of BTStack for PARTIALCOMMIT
    Lloop:
          <P>                           ; on error, branch to Ldone
          LOOP Lcont			; saturating (stops at zero), and
    	       				;  does NOT pop old LOOP value from stack
    Lcont:
	  PARTIALCOMMIT Lloop
    Ldone:
	  ENDLOOP			; restore old LOOP value from stack
          COMMIT			; remove BTChoice frame


