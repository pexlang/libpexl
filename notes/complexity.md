# PEXL matching complexity analysis

## Worst-case run time

Notes: 
* N = length of input
* n(A) = maximum length of pattern A; written just n for short
* C(A) = worst-case cost of trying to match pattern A
* E[s] = environment value for identifier 's'
* The pattern A may succeed or fail.  A' denotes A succeeding, 
  and A" denotes A failing.  Therefore, C(A) == max(C(A'), C(A")).

Expression       | C()                      | Notes
---------------- | ------------------------ | --------------
Char, Any        | 1                        | 
Set (charset)    | 1 (SIMD) or k = \|set\|  | 
True, False      | 1                        | 
Literal (bytes)  | 1                        | 
Call(Id)         | C(E[Id])                 | 
Seq(A, B)        | max(C(A"), C(A') + C(B)) |
Choice(A, B)     | max(C(A'), C(A") + C(B)) |
Star(A)          | N/n * C(A') + C(A")      | 
LookAhead(A)     | C(A)                     | How to account for not consuming input?
LookBehind(A)    | n * C(A") + C(A)         | How to account for not consuming input?
Find(A)          | N/n * C(A") + C(A)       | 
Apply(Pr, A)     | depends on primitive Pr  | E.g. BackReference function
recursive A      | c^N worst case for some c| How to prove better for certain grammars?

Observations:

- The duality between Seq and Choice is interesting, if not surprising.  In a
  sequence, B is attempted when A succeeds, whereas in a choice, B is attempted
  when A fails.

- Is the duality between Star and Find interesting in any way, or not?

- Star(A) in the worst case will need the worst-case time of A, but then only
  consume 1 byte.  (If A fails or consumes zero bytes, Star(A) terminates.)
  When A is constant-time, Star(A) is linear.
  
- Find(A) behaves like Star(A), except in the worst case it needs the worst-case
  time of A but then consumes zero bytes (because A fails).  Again, when A is
  constant-time, Find(A) is linear.
  
- LookAhead(A) needs the worst-case time of A but always consumes zero bytes,
  whether A succeeds or fails.

- LookBehind(A) in the worst case will try to match A at N start positions (all
  the way back to the start of the input if A is unbounded), and will always
  consume zero bytes.

- That Find(A) and LookBehind(A) have the same worst-case complexity is
  reassuring, because both are, in the worst case, byte-at-a-time searches for a
  match for A.

- Note that max(C(A'), C(A")) = C(A').  That is, while it is possible to "fail
  early", it is not possible to "succeed early".  There is some smallest number
  of steps that pattern A requires to succeed; A may fail in fewer steps, but by
  definition it may not succeed in fewer steps.  An alternative argument is that
  in the worst case, success or failure will be determined by the last byte
  examined in the longest sequence of steps that pattern A can take.  Therefore,
  O(A) is O(A").
  
## Space analysis



  
  
--------------------------------------------------------------------------------
## Appendix

Exponential (in number of parens) expression parsing in RPL 1.2.

	.../dev/rpl/rosie$ time rosie -f rpl_1_2.rpl match rpl_1_2.form <<< "(((((((((((foo)))))))))))"
	(((((((((((foo)))))))))))

	real	0m4.362s
	user	0m4.324s
	sys	0m0.027s

	.../dev/rpl/rosie$ time rosie -f rpl_1_2.rpl match rpl_1_2.form <<< "((((((((((foo))))))))))"
	((((((((((foo))))))))))

	real	0m1.224s
	user	0m1.199s
	sys	0m0.022s

	.../dev/rpl/rosie$ time rosie -f rpl_1_2.rpl match rpl_1_2.form <<< "(((((((((foo)))))))))"
	real	0m0.449s
	user	0m0.423s
	sys	0m0.020s

	.../rpl/rosie$ time rosie -f rpl_1_2.rpl match rpl_1_2.form <<< "((((((((foo))))))))"
	((((((((foo))))))))

	real	0m0.257s
	user	0m0.233s
	sys	0m0.021s

	.../rpl/rosie$ time rosie -f rpl_1_2.rpl match rpl_1_2.form <<< "(((((((foo)))))))"
	(((((((foo)))))))

	real	0m0.195s
	user	0m0.176s
	sys	0m0.015s

	.../rpl/rosie$ time rosie -f rpl_1_2.rpl match rpl_1_2.form <<< "foo"
	foo

	real	0m0.180s
	user	0m0.160s
	sys	0m0.016s


