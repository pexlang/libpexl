# SPASM, a String Processing ASM

<br>Jamie A. Jennings
<br>
<br>A WORK-IN-PROGRESS DRAFT OF IDEAS
<br>
<br>This draft began: Monday, November 7, 2022
<br>Last major edit: Sunday, January 21, 2024

## Unicode support

Unicode support is built-in, and a pattern expression is compiled for a
particular character encoding, such as UTF-8 or UTF-16.  At this point, it is
unclear (1) whether UTF-32 will be supported, and (2) how the endian issue for
UTF-16 will be addressed.

## Instruction groups

### Matching expressions for literals

Below, `<byte>` is 8 bits and `<word>` is 16 bits.  The grammar symbol
`<string>` is yet to be defined, but is meant to be a sequence of bytes,
possibly with a tag indicating if it happens to represent a valid string in the
current encoding.

	(any n)                                // skip ahead n bytes, n > 0
	(anyutf8 n)                            // skip ahead n utf8 codepoints, n > 0
	(anyutf16 n)                           // skip ahead n utf16 codepoints, n > 0
	(byte1 <byte>)                         // like the lpeg/rpeg IChar
	(byte2 <byte> <byte>)                  // seq e.g. for 2-byte UTF-8
	(byte3 <byte> <byte> <byte>)           // seq e.g. for 3-byte UTF-8
	(utf8long <byte> <byte> <byte> <byte>) // match 4-byte UTF-8 codepoint
	(word1 <word>)                         // match word, endian-aware
	(surrogate <word> <word>)              // endian-aware UTF-16 pair match

	<setexp> := (set ...) | (range ...)    // as defined below
	(range <byte> <byte>)                  // inclusive range
	(set [<byte> | <setexp>]*)             // like ISet
	(span [<byte> | <setexp>]*)            // like ISpan

TODO: Define UTF-16 expressions wrt handling surrogate pairs.  E.g. can the
`wrange` instruction be used to match any 16-bit value, or do we exclude
surrogate pairs halves from being allowable arguments?

	<wsetexp> := (wset ...) | (wrange ...) // as defined below
	(wrange <word> <word>)                 // inclusive range, endian-aware
	(wset [<word> | <wsetexp>]*)           // like ISet but endian-aware
	(wspan [<word> | <wsetexp>]*)          // like ISpan but endian-aware

### Tests

	(testany n <label>)           // jump if fewer than n bytes left
	(testset <setexp> <label>)    // jump if <setexp> fails to match
	(wtestset <wsetexp> <label>)  // jump if <wsetexp> (endian-aware) fails to match
	
### Repetitions

	(atmost n <exp>)       // n = 0 is possessive Kleene star (nofail)
	(exactly n <exp>)
	(atleast n <exp>)      // desugars to (begin (exactly n <exp>) (atmost 0 <exp>))

### Capture

	(capture! <id> <exp>)   // like a Rosie capture
	(insert! <id> <string>) // like a Rosie or lpeg "constant capture"
	(suppress! <exp>)       // suppress captures in <exp>

### Control flow

Earlier draft:

	(call <label>)
	(xcall <pkgnum> <label>)
	(return)
	(jump <label>)	        // PC += signed 16-bit offset 
	(jumpfar <label>)	    // PC += signed 32-bit offset
	(fail)
	(noop)

    (choice <label>)        // on failure: restore state, jump to <label>
	(commit <label>)        // pop choice frame, jump to <label>
	(partialcommit <label>) // update choice frame, jump to <label>

	(label <label>)

Structured alternative:

    (choice
	  (<p1>)
	  (<p2>)
	  ...
	  (<pn>))
	  
Inside a choice, `(commit)` breaks out of the choice construct.


### Miscellaneous

  IEnd:                end of pattern (success)
  IGiveup:             used internally by the vm (fail)
X  INoop:               no operation

X  IAny:                consume a char, fail if at end of input
X  IRet:                return from a call or xcall
X  IFail:               pop backtrack stack, jump to saved offset
  IFailNot:            fail negative lookahead
  ILookAhead:          push lookahead stack frame
  ILookAheadCommit:    pop lookahead frame, restore position
X  ICloseCapture:       push close capture marker onto capture list
  IBehind:             walk back aux characters, fail if not possible
x  IChar:               if char == aux then advance else fail
x  ISet:                if char not in set, fail
X  ISpan:               read a span of chars from charset
X  ICall:               call 'offset' in current code vector
X  ICloseConstCapture:  push close const capture to capture list
  IBackref:            match same data as prior capture (key in 'offset')
X  ITestAny:            if no chars left, jump to 'offset'
X  IJmp:                jump to 'offset'
X  IChoice:             push choice frame incl. 'offset' (next fail jumps there)
X  ICommit:             pop choice frame and jump to 'offset'
  ILookBehindCommit:   check backloop end position, fail or commit
X  IPartialCommit:      update top choice frame to current position and jump
X  IOpenCapture:        push an open frame to capture list
X  ITestChar:           if char != aux, jump to 'offset'
  IXCall:              jump to entrypoint 'offset' in package number 'aux'
  ILookBehind:         push lookbehind frame to backtrack stack
X  ITestSet:            if char not in charset, jump to 'offset'
  
*/


## Instruction coding

Possibly this:

- 32-bit instruction words
  - In UTF-8 mode, opcode 11110x => match 4-byte UTF-8 sequence
	- The entire instruction is the UTF-8 codepoint
  - In UTF-16 mode, opcode 110110 => match 2-word surrogate pair
	- The entire instruction is the UTF-16 pair (W1 W2)
  - For other 6-bit opcodes:
  - 26-bit operand, available via union as one of:
	- signed 26 bit jump offset
	- 2-bit flag, 3 bytes
	- 2-bit flag, 1 byte, 1 word (16 bits)
	- 2-bit flag, 1 byte, 1 byte, 1 byte
    - 2-bit flag, unsigned 24-bit table index

* Offset and aux operands
  - IPartialCommit(flag, offset) -- Separate into two opcodes, so we don't need 'flag'
  - IOpenCapture(symkey, kind) -- Separate into 2+ opcodes, so we don't need 'kind'
  - ITestChar(byte, offset) -- CAN WE KEEP OFFSET TO E.G. 16 or 18 bits?
  - IXCall(pkgkey, offset) -- Change Offset to an index into symtab of other pkg
  - ILookBehind(min, max) -- Just keep min, max small so they fit in 26 bits total

* Offset and charset operands
  - ITestSet(setkey, offset) -- CAN SETKEY BE ~10bits AND OFFSET ~16 bits?


#define opcode(pc) ((pc)->i.code)
#define setopcode(pc, op) ((pc)->i.code) = (op)

#define addr(pc) ((pc + 1)->offset)
#define setaddr(pc, addr) (pc + 1)->offset = (addr)

/* Convenient ways of accessing the aux field: */
#define aux(pc) ((pc)->i.aux)
#define signedaux(pc) ((aux((pc)) & 0x800000) ? (int)(aux((pc)) | 0xFF000000) : aux((pc)))
#define setaux(pc, idx) (pc)->i.aux = ((idx)&0xFFFFFF)
#define ichar(pc) ((pc)->i.aux & 0xFF)
#define setichar(pc, c) (pc)->i.aux = ((pc)->i.aux & 0xFFFF00) | (c & 0xFF)
/* And helpers to prepare/test 32-bit and 64-bit signed values for storing in aux */
#define prepsignedaux(val) ((val)&0x00FFFFFF)
#define checksignedaux(val) \
  (((val) >= 0) ? (((val)&0x007FFFFF) == (val)) : ((((int64_t)(val)) & 0xFFFFFFFFFF800000) == 0xFFFFFFFFFF800000))

typedef struct CodeAux
{
  uint8_t code;          /* 8-bit opcode */
  unsigned int aux : 24; /* 24-bit aux field: [0 .. 16,777,215] env index, or other operands, like chars */
} CodeAux;

typedef union Instruction
{
  CodeAux i;       /* opcode and aux field packed into a 32-bit word */
  int32_t offset;  /* follows an opcode that needs an offset value */
  uint8_t buff[1]; /* char set following an opcode that needs one */
} Instruction;

/* ----------------------------------------------------------------------------- */
/* Instructions                                                                  */
/* ----------------------------------------------------------------------------- */

#define _InstructionList(X)                                                 \
  /* No operands */                                                         \
  X(IGiveup, "giveup", 1, &&LABEL_IGiveup)                                  \
  X(INoop, "noop", 1, &&LABEL_INoop)                                        \
  X(IAny, "any", 1, &&LABEL_IAny)                                           \
  X(IRet, "ret", 1, &&LABEL_IRet)                                           \
  X(IEnd, "end", 1, &&LABEL_IEnd)                                           \
  X(IFail, "fail", 1, &&LABEL_IFail)                                        \
  X(IFailNot, "failnot", 1, &&LABEL_IFailNot)                               \
  X(ILookAhead, "lookahead", 1, &&LABEL_ILookAhead)                         \
  X(ILookAheadCommit, "lookaheadcommit", 1, &&LABEL_ILookAheadCommit)       \
  X(ICloseCapture, "closecapture", 1, &&LABEL_ICloseCapture)                \
  /* Aux operand */                                                         \
  X(IBehind, "behind", 1, &&LABEL_IBehind)                                  \
  X(IChar, "char", 1, &&LABEL_IChar)                                        \
  X(IFind, "find", CHARSETINSTSIZE, &&LABEL_IFind)                          \
  /* Charset operand */                                                     \
  X(ISet, "set", CHARSETINSTSIZE, &&LABEL_ISet)                             \
  X(ISpan, "span", CHARSETINSTSIZE, &&LABEL_ISpan)                          \
  /* Offset (second word) operand */                                        \
  X(ICall, "call", 2, &&LABEL_ICall)                                        \
  X(ICloseConstCapture, "closeconstcapture", 2, &&LABEL_ICloseConstCapture) \
  X(IBackref, "backref", 2, &&LABEL_IBackref)                               \
  X(ITestAny, "testany", 2, &&LABEL_ITestAny)                               \
  X(IJmp, "jmp", 2, &&LABEL_IJmp)                                           \
  X(IChoice, "choice", 2, &&LABEL_IChoice)                                  \
  X(ICommit, "commit", 2, &&LABEL_ICommit)                                  \
  X(ILookBehindCommit, "lookbehindcommit", 2, &&LABEL_ILookBehindCommit)    \
  /* Offset and aux operands */                                             \
  X(IPartialCommit, "partialcommit", 2, &&LABEL_IPartialCommit)             \
  X(IOpenCapture, "opencapture", 2, &&LABEL_IOpenCapture)                   \
  X(ITestChar, "testchar", 2, &&LABEL_ITestChar)                            \
  X(IXCall, "xcall", 2, &&LABEL_IXCall)                                     \
  X(ILookBehind, "lookbehind", 2, &&LABEL_ILookBehind)                      \
  /* Offset and charset operands */                                         \
  X(ITestSet, "testset", 1 + CHARSETINSTSIZE, &&LABEL_ITestSet)             \
  /* Offset, aux and charset operands */                                    \
  /* none (so far) */                                                       \
  X(Iunused28, "illegal opcode", 1, &&BADOPCODE)                            \
  X(Iunused29, "illegal opcode", 1, &&BADOPCODE)                            \
  X(Iunused30, "illegal opcode", 1, &&BADOPCODE)

#define _FIRST(a, b, c, d) a,
typedef enum Opcode
{
  _InstructionList(_FIRST)
      PEXL_NUM_OPCODES /* sentinel */
} Opcode;
#undef _FIRST

#define _SECOND(a, b, c, d) b,
static const char *const OPCODE_NAME[] = {
    _InstructionList(_SECOND)};
#undef _SECOND

#define OPCODE_NAME(code) (OPCODE_NAME[(code)])

#define _THIRD(a, b, c, d) c,
static const int OPCODE_SIZE[] = {
  _InstructionList(_THIRD)
};
#undef _THIRD

#define _EXTRACT_VM_LABELS(a, b, c, d) d,
#define DECLARE_VM_LABELS			\
  static void *vm_labels[] = {			\
    _InstructionList(_EXTRACT_VM_LABELS)	\
  }

/* ----------------------------------------------------------------------------- */
/* Utilities for other modules                                                   */
/* ----------------------------------------------------------------------------- */

#define sizei(pc) (OPCODE_SIZE[opcode(pc)])

#endif
