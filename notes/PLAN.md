# Project plan

## Phase 0 (current work)

- [ ] Switch over to new AST representation
- [ ] Follow the compilation phases outlined in /ARCHITECTURE.md.
- [ ] Bounded look-arounds by default?
- [ ] Generate _search patterns_ automatically 
      - [ ] from `find:exp` 
	  - [ ] from `S = exp / . S`
- [ ] Decide how to handle tokenization feature, if at all
- [ ] Provide whitespace-consuming pattern like the existing `~`
- [ ] Maybe support word boundary anchors?
- [ ] Finish/polish the implementation of the current set of optimizers
- [ ] Broaden the benchmark suite to help us decide on default optimizer config
- [ ] Decide how optimizers can be configured by the end user


## Phase 1 milestones

- [ ] PEXL API finalized
- [ ] PEXL S-expression support complete
- [ ] Rosie 2.0 syntax finalized
      - [ ] Current operators: 
	        sequence `/` `*` `+` `?` `>` `!>` `<` `!<` grouping `backref`
      - [ ] Pattern definition and capture
      - [ ] Package, import
      - [ ] Future operators: e.g. `where`, unbounded look-around, local modules
      - [ ] Future fine-grained capture control
      - [ ] Future expression parser
- [ ] Unicode support
      - [ ] RPL can be ASCII only for this phase
	  - [ ] UTF-8 input text
	  - [ ] case-insensitive (ASCII), case-fold simple, case-fold full
- [ ] Rosie 2.0 CLI
      - [ ] config, version, help
      - [ ] match, search
      - [ ] compile
      - [ ] test
      - [ ] output formats: status, line, color, json, jsonpp, sexp
      - [ ] basic trace capability
- [ ] Release some benchmarks publicly
- [ ] FFI interface from Python
- [ ] Release Rosie 2.0 supporting CLI, C, Python


## Phase 2 milestones

- [ ] Release more benchmarks publicly
- [ ] Consider parsing benchmarks from https://www.cl.cam.ac.uk/~nk480/flap.pdf
- [ ] Declare `linear` and have compiler enforce it
- [ ] RPL conversion program from Rosie 1.4 to Rosie 2.0
- [ ] Regex to Rosie conversion
- [ ] Consider: Ohm to Rosie conversion
- [ ] Improved trace capability
- [ ] REPL
- [ ] Good "how to" guide for writing patterns


## Phase 3 milestones

- [ ] Unicode support
      - [ ] RPL can be UTF-8
	  - [ ] UTF-16LE and UTF-16BE input text
	  - [ ] pattern modifiers for matching:
	        - [ ] composed normal form (NFC) text
			- [ ] decomposed normal form (NFD) text
			- [ ] unknown form (matches any mix of NFC and NFD)
			- [ ] composed normal form compatibility (NFKC) (includes look-alikes)
			- [ ] decomposed normal form compatibility (NFKD) (includes look-alikes)
- [ ] Additional language features:
      - [ ] Unbounded look-around
	  - [ ] Local modules
	  - [ ] `where` expressions
- [ ] FFI interfaces
      - [ ] Rust
      - [ ] Julia
      - [ ] Java
      - [ ] Haskell

## Beyond

- [ ] Language features
	  - [ ] Left recursion support
	        See, e.g. https://github.com/lukehutch/pikaparser (https://arxiv.org/abs/2005.06444)
			See, e.g. https://tinlizzie.org/VPRIPapers/tr2007002_packrat.pdf
	  - [ ] Expression parser
		    See, e.g. https://github.com/lukehutch/pikaparser
- [ ] Optimization tuning via sampling typical input
- [ ] Profiling for end users
