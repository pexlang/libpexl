Wednesday, June 15, 2022

With Natalie.

https://en.wikipedia.org/wiki/Propagation_of_uncertainty#Example_formulae

Look for f = A/B.  The hyperfine version 1.11.0 prints the ratio of the mean
speed of each command, A, against the mean of the fastest command, B.

But then it says "± s" where s is the standard deviation of f = A / B (the ratio
of the means).

The ± symbol suggests that this is a confidence interval, but it is not.  It is
a standard deviation.  It could be described as a confidence interval with 68%
confidence (one standard deviation), perhaps.

When hyperfine prints that program B was "x ± s times faster" than A, it should
be interpreted as follows.  If the same experiment was run 100 times, then we
expect that in 68 of those experiments, program B was between "x - s times
faster" and "x + s times faster" than A.

In some academic disciplines, the confidence that one mean is different from
another is expressed in terms of p-value.

NOTE: People expect 95% confidence as the default.  This is the p = 0.05 value
that is common in academic social science.  And 99% is p = 0.01.

NOTE: Talking about B being some percentage faster than A is terribly misleading
if the algorithms in A and B are not of the same order.  I.e. when input sizes
change, this percentage will change (if the algorithms do not share the same
asymptotic complexity).

If we plotted p values against input size, we might see that the better
algorithm in terms of asymptotic complexity becomes easily distinguished from
the others.


