# String block for match tree note for multiple packages

Make tree node prefix, type, and data slots 64 bits. If LSB is 0, this is a 0-7
character string with a null terminator.  Otherwise, the first 3 bytes are an
offset into the string block where the data can be found.

The string block is built using an auxiliary array of 64-bit integers.  When we
encounter symbol table entry j in package number k, we construct a key where the
first two bytes are k and the next 3 bytes are j.  Each array entry is 64 bits
containing this key and a 3-byte string block offset as data. We do a linear
search of this array comparing the key bytes of each entry.  If we find the key,
we have the offset to put into the tree node.  If not, we copy the data from the
symbol table block to the tree block, and add an entry in the table.

Requires 16-bit limit on number of packages and 24-bit limit on number of
symbols in a package.

# String block for match tree note for one package

We should consider whether it would be a better strategy to remove multi-package
support from the vm, i.e. require the package table to be just a single package.
Then when constructing the match tree, we could just copy the entire string
block from the one package’s symbol table into the tree struct.  It wastes some
space because surely we don’t need all the strings in the block.  But it’d be
fast.
