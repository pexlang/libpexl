## The reparse concept

Backtracking algorithms have an established relationship with continuations.  In
the file of notes `cont.txt`, Jennings points out that the non-possessive choice
and repetition operators in regex translate effectively into undelimited
continuations in a backtracking implementation.

Such continuations pose an obstacle to composability in programs (e.g. in Scheme
programs that use `call/cc`), and we see the analogous effect in regex in the
obstacles to composing them.  Indeed, composition requires global analysis and
transformation.  

> One way to see this is to examine the subset construction algorithm for
> converting an NFA into a DFA.  We can construct an NFA that is structurally
> isomorphic to a regex, and through the conversion to a DFA we can see that
> "local" structure in the NFA affects the global structure of the resulting
> DFA.  This is, in short, why there is no version of Thompson's Construction
> for DFAs -- composing DFAs cannot be done locally.

> A related observation is that the algorithm of Meideros et al. for converting
> a regular expression into a PEG demonstrates clearly that each regular
> expression choice and repetition has a global effect on the structure of the
> resulting PEG.

By contrast, _delimited continuations_ in programs do allow composition.  The
notes in `cont.txt` suggest that we could invent an analogous parsing expression
construct.  That is, we could define choice and repetition operators which are
neither possessive (as in PEG) nor globally non-possessive (as in regex), but
instead are non-possessive only within a delimited scope.

### What is a scope in a parsing expression?

First, we are using _parsing expression_ generically, encompassing regular
expressions, (modern) regex, PEG expressions, and even CFG expressions.  Second,
it is not clear whether our concept of a parsing expression _scope_ has appeared
in the literature before.  In the area of formal languages, however, there is a
broad and deep body of existing work, and so we must emphasize that we may be
rediscovering and not inventing the idea.

Nonetheless, Jennings' notes define a notion of scope, syntactically denoted
with curly braces, that circumscribes the effect of non-possessive regex
operators like choice.

In the following examples parentheses are used only for grouping, not to
indicate captures.  Extensions to captures are relatively straightforward (c.f.
unpublished manuscripts by Jennings and by Jennings and Nehapetyan, as well as
the M.S. thesis of Neeraj Shukla).

E.g. Consider the regex `a(b|c)`, which has the PEG equivalent `a(b/c)`.  The
slightly altered variant `a(b|c)d` is equivalent to the PEG `a((bd)/(cd))`.  The
remainder of the original expression after the choice (here, just `d`) ends up
incorporated into the PEG's possessive choice.

This simple example illustrates how composing `a(b|c)` with `d` has nonlocal
effects.  But we could define a new semantics for non-possessive choice that is
bounded by a scope, e.g. `{a(b|c)}` where the curly braces delimit the effect of
`|`.  The equivalent PEG is `a(b/c)` as before; the impact of the delimited
scope appears when we compose expressions.

E.g. Composing (in sequence) `{a(b|c)}` with `d`, which we might write as
`{a(b|c)}d`, has the following PEG equivalent: `a(b/c)d`.  When the choice
`(b|c)` goes out of scope, the choice becomes possessive.

E.g. Composing (in sequence) `{a(b|c)d}` with `e`, which we might write as
`{a(b|c)d}e`, has the following PEG equivalent: `a((bd)/(cd))e`.  Within the
scope indicated by the curly braces, the choice `(b|c)` is not possessive.  This
lets us write a locally non-possessive parsing expression that composes well
(i.e. via local transformations) with other expressions.

### How do delimited scopes interact with look-around operators?

Both regex and PEG support lookahead.  Although lookbehind was not part of the
original PEG (formal) definition, many PEG implementations (including Rosie
Pattern Language) support it to a degree.  Typically, the target of a
look-behind must be a fixed-length pattern.  The PEXL project removes this
restriction and provides this general definition: `lookbehind(X)` is a predicate
that succeeds at the first position before the current input position at which
the pattern `X` succeeds **and** the match of `X` ends at the position before
`lookbehind(X)` was invoked.  Recall that a predicate consumes no input;
therefore, if `lookbehind(X)` succeeds, the current input position will remain
unchanged.

The PEXL definition of lookbehind suggests an iterative search for an
implementation, in which a series of input positions are considered, each one
further before the start position than the last one.  Without any notion of
scope, this search is bounded only by the start of the input.  In other words,
the search for an input position that matches `X` might continue until the start
of the input is reached.  This is unfortunate from a performance perspective,
but any definition of `lookbehind(X)` for arbitrary parsing expression `X` would
seem to require the same property.

We could use the concept of a parsing scope to prevent a lookbehind search from
continuing all the way back to the start of the input.

In the (above) exploration of parsing scope as a technique for delimiting the
effect of non-possessive operations, we used a simple syntax (for illustration),
namely the curly braces that delimit scopes in programming languages with C-like
syntax.  We shall attempt to continue in that vein, while acknowledging that
this is a first draft, and a more palatable concrete syntax may be adopted later
to better convey the idea.

### A contrived example

E.g. Consider the parsing expression `ab*(<ab)`, where `<ab` means "look behind
for the pattern `ab`".  On input "abbb", the parsing expression fails.  The
lookbehind `<ab` is invoked at the end of the input (due to `ab*` consuming the
entire input); although `<ab` matches at the start of the input, the end of that
match occurs before the position where the lookbehind was invoked.  We can
imagine a situation in which we want to prevent the search for `ab` from
continuing all the way to the input start.  

Maybe we have a pattern already written, e.g. `T=ab*`, though perhaps `T` is
something more complicated, and it may be given as part of a library of
patterns.  Then we want to write `T<ab` to match only when `T` matches **and**
the last two characters of the match are `ab`.

But suppose our expression `T<ab` is preceded by some other expression,
e.g. `ST<ab`, meaning "match nonterminal `S`, then nonterminal `T`, then look
behind for the two-terminal sequence `ab`.

Conceivably, we may wish to limit the lookbehind to inspect only the span of
input matched by `T`.  We could write something like `S{T<ab}` to achieve this,
by putting `T` and the lookbehind into a parsing expression scope (here shown
with the same curly brace syntax as earlier in our notes).

This example is unsatisfying for at least two reasons, one of which is that the
use case is so contrived.  The other is that our lookbehind target, `ab` has a
fixed length, and so the pattern compiler can emit code that searches for a
match of `ab` at exactly one position, which is two characters before the
position where the lookbehind is invoked.

A better contrived example would use a pattern with a non-fixed length, perhaps
even with an unbounded length.  But instead, we will proceed to a more realistic
example, one that we see in practice in RPL usage.  And, this more practical use
case suggests that the look-around concept may not be the best approach at all!

### A practical use for look-around (perhaps)

Our simple examples are necessarily contrived, but we have a practical
motivation for limiting lookbehind scope.  When parsing expressions can be named
and packaged into libraries, as in Rosie Pattern Language, a common use case for
lookbehind is to facilitate reuse of library patterns.  For instance, the RPL
pattern `net.fqdn` matches a domain name (partially qualified, actually, despite
the name).  It matches inputs like "www.ncsu.edu" and "duckduckgo.com".

When a user wants to match domain names that start with "www", or ones that end
in ".com", it seems better to allow them to reuse `net.fqdn` and impose
conditions on the match.  (That is, it seems better than the alternative of
writing custom versions of `net.fqdn`, one for each variant use case.)  In the
first case, perhaps the user should first look ahead for "www" and then match
`net.fqdn`.  In the second case, perhaps after matching `net.fqdn`, they would
look behind for ".com".

Yet, this approach is fraught.  In the first case, what if the domain name
starts with "www-1"?  In the second case, what if the domain name ends with
".com." (because domain names are allowed to end with a dot)?

The first case seems easily solved by looking ahead for "www.", i.e. to include
the dot separator.  But this approach does not help a user who wants to match a
URL whose domain name starts with the segment "www".  They may want to use
`net.url` -- and this is a good idea because that pattern encodes a lot of
information culled from internet specifications.  Yet, it seems infelicitous to
ask the user to write a lookahead for "http://www." **or** "HTTP://www." **or**
"https://www." **or** any of the other allowable variations.

The issue highlighted in these cases is that the user must understand many of
the details of library pattern definitions for `net.fqdn` or `net.url` in order
to reuse them in this particular way, i.e. to apply conditions _by using
look-arounds_ to what these patterns match.

As programmers, we desire abstractions specifically to avoid knowing many of the
implementation details.  Of course, there are some aspects of library
implementations that we must understand in order to use them properly, such as
knowing what kinds of comparison functions may be passed to the C language
`qsort`. 

Sort routines nonetheless provide an example we may want to emulate.  The
comparison function compares two elements.  The programmer knows the element
type and can think about comparing elements without considering the particular
sorting algorithm.  The same comparison function may be passed to a merge sort,
a quick sort, or a bubble sort.  (An adaptive sort may choose dynamically which
algorithm it will use, and we can still pass our comparison function to such a
sort!)

Perhaps look-arounds are not the right way to solve these particular use cases.

### A different path

If a library pattern like `net.url` is analogous in any way to a C library
routine like `qsort`, then what inspiration can we take from the role of the
comparison function?

Maybe it is that a comparison function has a structural relationship with a
comparison-based sort algorithm, and we can observe that the suggested use of
lookahead or lookbehind has only a shallow relationship with a pattern like
`net.fqdn` or `net.url`.  The shallowness comes from the fact that the library
patterns take strings as input, and so do the lookahead and lookbehind
operators.  The look-around predicates have no connection to the structure of
`net.fqdn`, and yet we are suggesting that they be used to filter, in essence,
the resulting matches to `net.fqdn`.

Look-around patterns may well have compelling use cases, but a different kind of
predicate may be a better solution to the examples we consider here, in which we
want to extend the ways in which patterns like `net.fqdn` and `net.url` can be
reused. 

What if the user did not need to know very much about the details of how a
pattern like `net.url` was implemented?  What if we could exploit knowledge that
the user already possesses, such as the structure (template?) of the parse trees
that `net.url` can produce?

In both the current use cases (domain name starts with "www"; domain name ends
in "com"), we could parse a domain name or URL using the appropriate library
pattern, and then give the user a way to express a predicate on the result.  In
other words, the additional predicate should be on the resulting parse tree, not
on the original input string!

Many predicates on tree structures are implemented in other contexts -- the
concept is not new, and we can draw inspiration for a concrete syntax from any
number of other technologies.  In our context, which is during pattern matching,
what would such a predicate look like?

It would be a _predicate_ in PEG nomenclature, or a _zero width assertion_ in
regex, yet it would operate on the output structure of a match, not on the input
structure (which is merely a string).

Here is a concrete example using a not-very-well-considered concrete syntax:

`{net.url where: net.host.component[0]=="www"}`

We hypothesize in this example that a match for `net.url` contains a node
`net.host` which in turn contains a sequence of `component` nodes.  (The RPL
library, as of this writing in 2021, does not return the components of the host
name, though it easily could in the future.)

The concrete syntax here is ill-considered but is meant to evoke two ideas: (1)
there is a scope indicated by the curly braces, and (2) because of the scope,
the only parse tree considered by the "where predicate", which acts as a "look
behind", is the parse tree created inside the inner scope, i.e. the one created
by `net.url`.

Note that this approach lets the user express what they want in terms they
already understand.  That is, the user may reasonably be expected to know what
kinds of parse trees will be produced by `net.url`.  After all, they are using
this pattern not only to recognize a URL, but presumably to further process the
resulting parse tree.  (Search the internet for "parse, don't validate" for
additional motivation.)

Note also that the example predicate is declarative.  Perhaps the use of the
`where` keyword already brought to mind a comparison with SQL, even though we
are only considering simple predicates on trees here.  But the declarative
nature of the predicate is important, because in principle, the compiler has
room to optimize.  Given the expression above, the pattern compiler might be
able to emit code that aborts the match of `net.url` after the first
`net.host.component` element is found to not match "www".

Finally, we observe that regex languages have had (at least, since the 1980's) a
limited way to use previously collected match output during a match: the back
reference.

### How do delimited scopes interact with the back-reference operator?

A regex produces a linear array of captures, not a tree structure like a PEG or
CFG parser would.  As the regex matches, it fills in entries in a fixed-length
array of captures.  (The size is known at compile time.)  The regex back
reference operator says "match again the string we captured earlier", and which
of the strings to match again is denoted by a capture number or capture name.

The generalized parsing expression predicates that we propose here could subsume
the back reference concept.  First, we are proposing _predicates_ on previously
matched items (trees), which is a concept that does not exist in regex.
(Indeed, it makes little sense there.)  Our example predicate in the previous
section was an equality predicate, but one could imagine others.  Second, we
could extend the ability to refer to previously matched items beyond
predicates.  We could design a pattern construct which embeds a reference to
something already matched.

In a simple example, we may want to match the same URL twice:

`{net.url again: net.url}`

Again, the concrete syntax here is surely ill-considered.  But again, it is
meant to evoke that (1) there is a scope delimiting the effects of certain
operators, and (2) the `again:` operator suggests matching again what was
previously matched with the pattern it precedes, `net.url`.

The Rosie project has had an experimental back-reference feature for some time
now.  It remains experimental because, to our knowledge, no one has put forward
a definition of back references for PEG languages.  Other than the source code,
which is a rough draft, the only published definition exists in [this issue
thread](https://gitlab.com/rosie-pattern-language/rosie/-/issues/121).

Though this implementation remains unpolished and not formally (or even
carefully) described, its behavior corresponds well to what developers expect.
See, for example, `extra/examples/backref.rpl` and `extra/examples/html.rpl`.
A simple command-line example is:

```shell 
$ rosie --rpl 'url=net.url' match 'url backref:url' <<< "https://www.ncsu.edu https://www.ncsu.edu"
https://www.ncsu.edu https://www.ncsu.edu
$ rosie --rpl 'url=net.url' match 'url backref:url' <<< "https://www.ncsu.edu https://csc.ncsu.edu"
$ 
```

## Summary

* Look-around operators were added to regex as a convenience feature.  In a
  language with composable parsing expressions, look-arounds serve the
  additional function of promoting reuse of pre-defined "library" patterns. 
  
* Relatively straightforward examples of reuse (with modification) of library
  patterns suggest that look-around operators are rather ill-suited for these
  use cases.  They operate on the same textual input as do the library patterns
  that a user would like to "modify" during reuse.  Instead, we should consider
  supporting predicates (and other operators) that reference the _output_ of
  previous matches.  In the PEG and CFG cases, that output has the shape of a
  tree.  
  
* Predicates on trees are common in other contexts, and both the concepts and
  concrete syntaxes there may serve as inspiration for our efforts.  
  
* As with back references, predicates on previous match output will surely
  present performance challenges.  Still, regex back references are supported in
  many regex dialects, apparently because they solve actual problems, and so
  developers do not wish to do without them.
  
* If references to previous match output are suitably designed, a pattern
  compiler may be able to optimize their execution.  In some cases -- hopefully
  including the most common use cases -- the compiler may be able to optimize
  out not only extra work (e.g. redundant comparisons) but also reduce the
  asymptotic complexity from the naive worst case.
  
* In the PEXL project, our goal is to provide an ergonomic replacement for regex
  in which parsing expressions compile to linear time matching code when
  possible, while allowing expressivity beyond the regular languages at the cost
  of super-linear run times.  We wish to design a parsing expression language
  that solves a wide range of input validation and simple parsing problems,
  while explicitly avoiding providing the expressivity of CFGs and the
  performance goals of specialized algorithms.

  * We do not wish to support arbitrary CFGs because:

	(1) designing a correct and efficient context-free grammar requires
        specialized skills and knowledge that should not be needed for routine
        developer use cases like input validation and the parsing of simple
        formats like configuration files or structured log file messages;
		
    (2) the use cases that require the expressiveness of a CFG tend to be those
        that also require features like grammar analysis/factoring tools (at
        development time) and parsing actions (at run time), neither of which
        are goals for PEXL.
		
  * We do not aim to equal the performance possible with specialized algorithms,
    such as those whose parsing expressions are limited to a subset of modern
    regex operations.  Nor do we expect to compete with parsers built for fixed
    formats like JSON, XML, or the FASTA format for nucleotide sequences, to
    name a few examples.  That said, there is no reason why PEXL should not
    parse those formats and others like them, albeit more slowly.  In some
    situations, having a single parsing/validation language/tool is a better
    solution than maintaining a collection of specialized tools, even at the
    cost of some performance.

-----------------------------------------------------------------------------

## Towards an implementation of 'reparse'

The notes below, by Andrew Farkas, concern an implementors view of the "reparse
concept", which posits that we should begin with a straightforward
implementation in order to test a variety of language features related to scopes
and predicates.  Our initial ideas, described below, focus on the ability to
specify two (or more) parsing passes on the same input.

We envision this as part of an experimental apparatus.  A parsing expression
scope ultimately corresponds to a span of matched input text and an associated
fragment of match output (a tree).  To experiment with predicates, generalized
back references, and other uses of parsing expression scopes, we might first
build a naive implementation that reifies the match output (which itself encodes
an input span) for a scoped expression.  Then we can experiment with a variety
of operations on that output.

The most obvious operation, perhaps, is simply to match the span again, but with
a different pattern.  That is, we could use pattern R to produce at run-time the
input span that is subsequently matched against pattern X.  Pattern X might be a
predicate: if X matches, then the overall match succeeds and our expression
evaluates to the parse tree produced by R.

Or, pattern R might exist simply to define a span that acts as the input to
pattern X.  This use case derives from a generalization of the line-by-line
processing that is so common in text matching applications.  Regex libraries
provide (multiple, sometimes confusing) ways of achieving multi-line matching,
but we can surely do better.  We could define a pattern R that matches the broad
outline of a "record" in our input stream.  It might be a single line, but it
might be a longer span that includes line breaks.  (Consider how "here docs"
look in shell scripts, or "long comments" in programming languages, for simple
examples.)

A pattern R could match from the start to the end of a logical "record", i.e. a
span of text that logically forms a document or document fragment (such as a
truncated document that ends because we saw the "start of document" indicator in
the input).  R might match balanced delimiters, for example, as an efficient way
to detect a JSON object without fully parsing it.  In a situation in which there
may be incomplete JSON objects in the input, or text that we want to ignore
although it contains curly braces and square brackets, the user may want to
identify candidate JSON objects by their delimiter shape before attempting a
proper parse.

Of course, a single line may just be the most common definition for a pattern
like R that is used to define a span to be matched using another pattern, X.  In
this use case, with R and X, the desired parse tree output is that of X, not R.

Because (in this scenario) X can only see the input defined by R, this is a use
case for the general notion of scope.  A scope in a parsing expression is a
language construct, but it has a corresponding run-time object, which is parse
tree (and input span).  Both ways of looking at scope give insight into
different use cases.

Below, we begin to explore the implementation issues of the most straightforward
place to start: Provide the ability to parse some input with R, and then parse
it again with X.  Later, we can decide whether the parse tree returned is what R
produced (in which case X acted as a predicate on the match of R) or is what X
produced (in which case R served to define the input for X).

-----------------------------------------------------------------------------

## Lookaround and reparse semantics

Many regex engines support lookahead and lookbehind. These features allow
parsing text at the beginning or end of the match without consuming it, or
parsing the same text multiple times. While lookahead is straightforward to
implement performantly, lookbehind is impossible to implement without
superlinear runtime characteristics or restrictions on pattern contents (such as
requiring only fixed-length lookbehind expressions). libPEXL opts for the latter
so that all patterns may be used in a lookbehind without concern for their
internal properties. To mitigate the need for potentially-slow lookbehind
expressions, libPEXL introduces a new feature: reparsing. The semantics of
lookahead, lookbehind, and reparsing in libPEXL are described below.

### Fences

There are four "fences" in the string, defined as follows:

- `BOF` and `EOF` (Beginning/End Of File) are located at the beginning and end
  of the string, respectively
- `BOI` and `EOI` (Beginning/End Of Input) are located at the beginning and end
  of the input, respectively

The input is a substring of the whole string. While `BOF` and `EOF` never
change, `BOI` and `EOI` change during lookaround and reparsing.

Parsing begins at `BOI`. All expressions other than lookahead, lookbehind, and
reparse never scan match any characters past `EOI`.

### Lookahead

At the start of a lookahead expression, the parser saves the current `BOI` and
`EOI` positions and assigns new values to them:

- `BOI` ← current cursor position
- `EOI` ← `EOF`

Then the parser attempts to match the contents of the lookahead (with captures
disabled), starting from the current position.

- For positive lookahead: If the match succeeds, the lookahead **succeeds**;
  otherwise, the lookahead **fails**.
- For negative lookahead: If the match succeeds, the lookahead **fails**;
  otherwise, the lookahead **succeeds**.

After the lookahead succeeds or fails, `BOI` and `EOI` are reset to their
original values.

### Lookbehind

At the start of a lookbehind expression, the parser saves the current `BOI` and
`EOI` positions and assigns new values to them:

- `BOI` ← `BOF`
- `EOI` ← current cursor position

Then for each position from `EOI` to `BOI`, the parser attempts to match the
contents of the lookbehind (with captures disabled) up to `EOI`. The parser may
skip some or all of these attempts if the pattern is guaranteed to fail (e.g.,
because it is a fixed length).

- For positive lookbehind: If any match succeeds and ends at `EOI`, the
  lookbehind **succeeds**; otherwise, the lookbehind **fails**.
- For negative lookbehind: If any match succeeds and ends at `EOI`, the
  lookbehind **fails**; otherwise, the lookbehind **succeeds**.

After the lookbehind succeeds or fails, `BOI` and `EOI` are reset to their
original values.

### Reparsing

A reparse expression has a left subexpression and a right subexpression.

At the start of a reparse expression, the parser saves the current cursor
position and matches the left subexpression as normal. Then the parser saves the
current `BOI` and `EOI` positions and assigns new values to them:

- `BOI` ← saved cursor position from beginning of reparse expression
- `EOI` ← current cursor position

Then the parser attempts to match the right subexpression (with captures
disabled) starting from `BOI`.

- For positive reparse: If the match succeeds and ends at `EOI`, the reparse
  **succeeds**; otherwise the reparse **fails**.
- For negative reparse: If the match succeeds and ends at `EOI`, the reparse
  **fails**; otherwise the reparse **succeeds**.

After the reparse succeeds or fails, `BOI` and `EOI` are reset to their original
values.

## Authors

* Andrew Farkas (reparse definition and semantics) 
* Jamie Jennings (the reparse concept and pattern scope motivation)
