# Libpexl

# libpexl is a work-in-progress library for text matching and simple parsing

My long term goal is to move software developers away from regular expressions
by providing a better alternative.  Regex libraries are well-documented sources
of a great variety of bugs, some that are implementation-specific while most (in
my opinion) are design problems.  The most significant design problem is due to
the agglutination of features like zero-width assertions, back references, and
grammar-like structures.  Furthermore, Unicode support is essential today, and
is either lacking or severely compromised in most regex libraries. 

The result that regex, when viewed as a DSL, has no standard syntax and no clear
semantics.  Academic research confirms what developers already know, which is
that regexes are sometimes hard to write, always hard to read/maintain, rarely
easy to test, and consequently the source of many bugs in production.

My first attempt at a viable alternative is the [Rosie Pattern
Language](https://rosie-lang.org), which embodies many of my ideas of what is
important in a regex alternative.

A concrete (non-embedded) DSL, RPL reads like code, with named patterns,
comments, and unit tests.  Patterns can be collected into shared packages.
Based on the PEG formalism, the operation of RPL patterns is fairly
straightforward, having the character of imperative code in a language
supporting recursion.  (I believe this character derives crucially from the
possessive nature of PEG expressions, and makes RPL easier to understand than
regexes, which have a more declarative character due to their default
non-possessive nature.  But this is speculation without evidence.)

Over the last 8 years, using several PEG implementations (but mostly Rosie/RPL),
I have become disappointed with some aspects of the PEG formalism.  First,
implementations inevitably extend the formalism, which suggests that the PEG
approach does not, in fact, solve a suitable set of use cases.  Second, PEG
implementations tend to either be Packrat-style (parsers requiring a lot of
memory to run in linear time) or backtracking algorithms (worst-case exponential
time, with output-sensitive space needs).  Each technique is aimed at its own
use cases.  Packrat parsers rely on memoization, and are suitable for relatively
small inputs where the input is expected to parse correctly.  Backtracking can
be fast in practice, making it possible to efficiently _search_ through a large
input for a match to a PEG pattern, though the worst-case behavior is awful.

What attracted me, and likely others, to the PEG formalism was that it remixed
old parsing ideas into something with simple operational semantics.  Alas, PEG
implementations follow the historic trajectory of regex implementations:
Backtracking is the dominant algorithm used, and new features are added without
regard to the formal specification.  Rosie/RPL is not an exception, though I
believe we advanced the state of the art in readable, maintainable, testable,
and sharable pattern definitions.

# What software development needs

What I think we need to replace regex is considered by some to be radical, and
should be the subject of its own manifesto.  In brief, though:
1. A readable, maintainable, testable pattern syntax that is programming
   language independent (a concrete external DSL);
2. Full Unicode support;
3. Efficient search algorithms for large inputs, with time and space complexity
   guarantees and trade-offs under user control;
4. Efficient parsing algorithms for "simple" parsing use cases such as
   configuration languages, JSON, and HTML (programming language parsing is out
   of scope); and
5. A collection of text manipulation operations expressible in a language that
   is a superset of the pattern language.

The last item is needed to enshrine text processing in its own domain.  Programs
today that mix regexes and string operations are on shaky ground due to
mismatches in the way strings are handled by programming languages and regex
libraries.  It can be tricky to replace a simple regex invocation with an
equivalent sequence of string operations, because such equivalence is elusive. 

Moreover, strings as data structures are dead, a relic of history best left
behind.  Today we have text, mostly (certainly not exclusively) Unicode-encoded
text.  To handle text correctly is difficult at best.

The most common feature request for Rosie/RPL is _substitution_: for each match
of a pattern, substitute a (fixed) replacement.  I have resisted going in this
direction with the Rosie project, because even simple concatenation of text is
fraught.  (Look up Unicode Normal Forms for a primer.)

However, the need is there.  I hear software developers discuss removing regex
uses in favor of using language-specific string search and manipulation.  But
this means learning too many details of text encodings, in order to understand
the limitations of string operations in your programming language.

It's time to lift text to the level of an abstract data type.  It's time for
pattern-based text search and simple text parsing to be available in a uniform
way across programming languages.  And it's time to specify text manipulation
operations abstractly, so these too can work consistently across languages.

# The libpexl library evolved from the Rosie project

As the Rosie/RPL project evolved, its C code (which began as a modified
[lpeg](http://www.inf.puc-rio.br/~roberto/lpeg)) needed restructuring.  To
support new features, it needed to be redesigned and reimplemented.  That is
what `libpexl` is.  A significant benefit to the redesign is that we are able to
more easily experiment with ideas for new language features as well as new
implementation techniques.

The `libpexl` library will become the basis for a "Rosie 2.0" at some point,
perhaps in 2024.  As we develop `libpexl`, we are using it in our academic
research on pattern language design, usability, semantics, and performance.

# This is a work in progress

As of Sunday, May 14, 2023, the state of this code is roughly this:

* Currently, there is no concrete DSL.  Expressions are built using an API.
  This API is likely to change, as it was modeled on RPL, which has vestiges of
  less ergonomic aspects of the `lpeg` design.

* Expressions are built by explicitly creating a tree of environments which
  implement lexical scope rules.  Within an environment, recursive definitions
  are allowed.
  
* The PEXL compiler is invoked through a clunky API that is likely to change.
  The primary APIs allow compiling (1) a single anonymous expression; (2) an
  expression bound in an environment, possibly anonymously; or (3) an entire
  environment.  The output is a _package_ containing bytecode and a symbol
  table.
  
* The vm has been optimized somewhat from its prior incarnation in the Rosie
  project, where it had been a heavily modified version of the `lpeg` virtual
  machine.  Like the Rosie vm, the PEXL vm has no dependence on Lua.  It is a
  pure C99 implementation with some new instructions, many new data structures
  (including the parse tree representation), and notably a _module system_ that
  is designed to permit separate ahead-of-time compilation of pattern
  collections. 

* The PEXL expression builder API (pexle_*) allows construction of expressions
  using the features that underly Rosie RPL: literal strings, sequences, ordered
  choices, repetitions, look-ahead, negative look-ahead, and look-behind.
  * Back references are currently not supported, but nothing prevents adding
    them.
  * Captures and so-called "constant captures" must be used explicitly.
  * The vm (which does the matching) produces a parse tree in a flat array-based
    encoding that is easy to traverse.
  * PEXL supports repetitions of nullable patterns, unlike Rosie.
  * PEXL supports look-behind of variable-length patterns, unlike Rosie.
  * PEXL supports arbitrary nested environments, with static scope rules.  A
    symbol table provides an index into the tree-structured environment.  Among
    other things, this design allows a concrete DSL ("RPL 2.0") on top of PEXL
    that allows multiple entry points into a grammar of mutually recursive
    definitions.  The JSON grammar is a good use case: We can now define the
    grammar once, but expose direct access to the rules for matching not just
    any JSON value, but a JSON array or object (or any JSON value type).


* We have implemented rudimentary optimizations in the PEXL compiler, including:
  * **Function inlining.**  Patterns in `lpeg`, Rosie, and PEXL can "call" other
    patterns.  (In `lpeg` this facility is used only in recursive grammars;
    otherwise, inlining occurs unconditionally, leading to an explosion in byte
    code size in many real-world use cases.)  In PEXL, a pattern like `B C`
    compiles to a sequence of `call B`, then `call C`.  As an optimization, the
    definitions of `B` and `C` can replace the calls.  Tail call elimination
    converts recursion to iteration.
  * **Loop unrolling.**  There is currently (May, 2023) only one traditional loop
    produced by the PEXL code generator.  By traditional, we mean a loop with an
    explicit counter and a jump conditioned on that counter. When look-behind is
    invoked on a variable-length pattern, the vm must iterate through each
    possible start position for the look-behind's target pattern.  By unrolling
    such loops, we expect to see modest runtime (matching) performance
    enhancements.  In these early days, we do not yet have benchmarks that
    stress this optimizer.  Currently, repetitions of the form "at least _n_
    matches" are implemented in the `lpeg` style, by inserting _n_ copies of the
    pattern code.  To avoid code size explosion for large _n_, we plan to emit a
    traditional loop in this case, and to unroll the loop (to a bounded degree)
    during optimization.
  * **Peephole for jumps and calls.**  We retained the only byte code optimizer
    pass from `lpeg`, with modest changes.
	
* In PEXL, we are experimenting with new language features and implementation
  techniques. 
  * One example is the new `find` instruction in the virtual machine.  The
    argument is one or more byte values, and the `find` operation skips ahead in
    the input until a byte matches one in its target set.  Both SIMD and
    ordinary implementations are provided.  The SIMD implementation examines 16
    bytes at a time, and provides dramatic performance improvements when the
    next matching byte is more than 16 bytes away.  But it incurs a penalty when
    this is not the case.  The penalty may be due to the stateless nature of the
    implementation: a single invocation of `find` locates the nearest match and
    discards information about later matches found within its 16 byte window.
    In the worst case, every byte matches, and the vm examines 16 bytes simply
    to advance by 1 byte.  In a loop, the 16 byte window advances by 1, and 15
    bytes are re-examined in the next invocation of `find`.  Retaining and
    reusing the discarded information will require careful design.
  * We have designed PEXL such that its vm operates at the byte level, a design
    idea continued from Rosie.  Whereas Rosie supports only UTF-8, ASCII, and
    8-bit ASCII extensions, PEXL will support these and other Unicode
    encodings.  Because text data is typically not self-describing, the encoding
    of input text must be known by external means (e.g. user knowledge, a
    properties file).  Consequently, the PEXL compiler will take as arguments
    the pattern to be compiled and the input text encoding on which it will be
    used.  The "fat binary" approach can be used to compile PEXL patterns for
    multiple encodings, so that the encoding can be effectively selected at
    runtime instead of at compile time.
  * Another example of a language feature under development is a macro system.
    In the Rosie project, macros that transform an AST into another AST were
    implemented in Lua.  PEXL has no dependence on Lua, and so we have
    prototyped a new AST representation that we hope will facilitate macro-like
    transformations, including operations that should be implemented as
    de-sugaring but were not for historical reasons.
  * In work that remains on paper at present, we are designing a language
    feature inspired by delimited continuations.  An implementation feature of
    the Rosie vm that came directly from `lpeg` is the stack needed to
    backtrack.  Unconstrained backtracking can produce worst-case exponential
    run-time complexity.  Memoization is one way to eliminate backtracking at
    the cost of linear space, which is impractical for the large inputs typical of
    _searching_ use cases (as opposed to _parsing_ use cases).  However, there
    may be a form of language feature that can (1) meet user needs; (2) be
    understandable (with straightforward operational semantics); and (3) avoid
    exponential time complexity with constant (or output-sensitive) space
    complexity. 
  * We have done some prototyping of support for left recursion by adapting the
    "seed growing" technique described by Tratt and others.  Prior work assumed
    that parsing occurs by interpreting the given grammar, which is not our
    situation.  We devised a novel, if direct, adaptation of the technique to
    our ahead-of-time compiled pattern language.
  * Also still on paper are algorithms for:
    * Stochastic memoization, to avoid super-linear runtime but with reduced
      memory requirements compared to full memoization;
	* Runtime adaptation (inspired by CPU branch prediction) of pattern
      execution to properties of the input text such as letter frequency;
	* Left factoring in the presence of named captures: Consider `A/B` where
      `A=XY` and `B=XZ`, where left factoring produces `X(Y/Z)` and yet the
      resulting parse tree should indicate `A` if `XY` matched, and `B` if `XZ`
      matched;
	  


## Building libpexl

The makefile in the `src` directory builds `libpexl` in what we think of as a
"production" mode.  Debugging features are disabled, including the C language
`DEBUG` which controls whether assertions are executed, among other things.

The production build is the default `make` target, and it should succeed without
errors or warnings on gcc/clang, linux/macos, x86/arm64:

```
cd src
make clean && make
```

To use `libpexl` from your own program, it might be easiest to run `make
install`, provided the build succeeded.  This copies `libpexl.so` (or
`libpexl.dylib` on MacOS) and `libpexl.h` to `/usr/local/lib` and
`/usr/local/include`, respectively.

The install target of the Makefile respects the `DESTDIR` variable.  If not set
by the user, it defaults to `/usr/local/`.

### For developers adding code to libpexl

The dependencies between C source and header files are kept in
`Makefile.depends`.  If you add any code or change any `#include` declarations,
you'll need to run `make deps`, which will recalculate all dependencies and
overwrite `Makefile.depends` with new content.

### Running the unit tests

The makefile in the `test/unit` directory can do several things:
* Build `libpexl` from source with all debugging options enabled
* Build the test programs themselves
* Run the test programs
* Produce a code coverage report for the unit tests

The unit tests are written using a very simple set of test macros which cause
test programs to halt with a non-zero exit code when any test fails.  (The
programs in `test/unit` function as regression tests, and all are expected to
pass.)

See the README in the `test/unit` directory for more information; in short, to
run all the tests, do this:

```
cd test/unit
make clean && make build && make test
```

The tests generate a lot of output.  Running `make test > /dev/null` gets rid of
all extraneous output.  Important information is logged to `stderr`, so you'll
see that as well as the test coverage report that is generated at the end.

### Testing using podman or docker

To execute the unit tests in a podman container, run use the scripts provided in
the top level directory of the repository.  A sample transcript (edited for
brevity) is here:

```
~/Projects/libpexl$ podman machine start
Starting machine "podman-machine-default"
Waiting for VM ...
[SNIP]
~/Projects/libpexl$ ./podrun.sh ubuntu
STEP 1/14: FROM ubuntu:20.04
[SNIP]
Generating coverage data
make[2]: Entering directory '/opt/libpexl/src'
NOTE: Useful makefile targets, src directory: clean deps build config
NOTE: Detected architecture: aarch64
NOTE: Unknown CPU architecture: aarch64
NOTE: Platform detected: linux
NOTE: CC is set to cc, setting CC=gcc instead
NOTE: Using gcc as: gcc
gcov -n buf.c charset.c hashtable.c package.c symboltable.c astparse.c ast.c scc.c analyze.c codegen.c compile.c context.c env.c expression.c pattern.c capture.c tree.c find.c vm.c print.c astprint.c -o /opt/libpexl/test/unit/objects >> /opt/libpexl/test/unit/covreport.data
make[2]: Leaving directory '/opt/libpexl/src'

Coverage report
------------------------------------------
buf.c                 92.49%  of 253 lines
charset.c            100.00%  of  27 lines
hashtable.c           89.60%  of 202 lines
package.c             86.50%  of 237 lines
symboltable.c         86.21%  of 174 lines
astparse.c            82.14%  of 140 lines
ast.c                 77.26%  of 497 lines
scc.c                 96.35%  of 137 lines
analyze.c             84.31%  of 752 lines
codegen.c             83.33%  of 864 lines
compile.c             67.53%  of 465 lines
context.c             77.42%  of  62 lines
env.c                 88.72%  of 133 lines
expression.c          84.70%  of 379 lines
pattern.c             88.43%  of 242 lines
capture.c             75.15%  of 169 lines
tree.c                70.00%  of 140 lines
find.c                55.56%  of  36 lines
vm.c                  70.45%  of 484 lines
stack.c               58.37%  of 209 lines
print.c               77.55%  of 588 lines
astprint.c            76.92%  of 286 lines

22 files                        6476 lines

[SNIP]
STEP 14/14: RUN cat /etc/os-release
NAME="Ubuntu"
VERSION="20.04.5 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.5 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal
COMMIT pexl:ubuntu
[SNIP]
~/Projects/libpexl$ 
```

## Using libpexl

The API is not stable at all.  The best way to see how to use this library is to
read the test programs.

As the API evolves, the test programs are updated to reflect the changes.  When
we are happy with the API and it seems relatively stable, we will document it.


## License

This project is licensed under the
[BSD 3-clause license](https://opensource.org/licenses/BSD-3-Clause).

