# Rosie/PEXL Project: Overview and Architecture

This document describes the architecture of the Rosie/PEXL project.  At the
moment, it is more a collection of notes, but it will evolve with the project.

## Raison d'être

Regex are used very frequently in production code for input validation,
information extraction, and text mining.  Flawed input validation remains, year
after year, a major source of software vulnerabilities.  Bugs in information
extraction are also a security concern, though often the result is a bug of
another variety, such as a poor user experience or a very occasional
hard-to-trace failure.  Finally, for text mining, regex is a poor technology
choice.  Performance anomalies stall big data pipelines; semantic errors in
writing regex produce false positives and false negatives; severe undertesting
of regex results in many latent bugs, waiting for the right kind of data to
surface. 

What I think we need to replace regex is considered by some to be radical, and
should be the subject of its own manifesto.  In brief, though:
1. A readable, maintainable, testable pattern syntax that is programming
   language independent (a concrete external DSL);
2. Rich Unicode support;
3. Efficient search algorithms for large inputs, with time and space complexity
   guarantees and trade-offs under user control;
4. Efficient parsing algorithms for "simple" parsing use cases such as
   configuration languages, JSON, and HTML (programming language parsing is out
   of scope); and
5. A collection of text manipulation operations expressible in a language that
   is a superset of the pattern language.

The last item is needed to enshrine text processing in its own domain.  Programs
today that mix regexes and string operations are on shaky ground due to
mismatches in the way strings are handled by programming languages and regex
libraries.  It can be tricky to replace a simple regex invocation with an
equivalent sequence of string operations, because such equivalence is elusive. 

Moreover, strings as data structures are dead, a relic of history best left
behind.  Today we have text, mostly (certainly not exclusively) Unicode-encoded
text.  To handle text correctly is difficult at best.

The most common feature request for Rosie/RPL is _substitution_: for each match
of a pattern, substitute a (fixed) replacement.  I have resisted going in this
direction with the Rosie project, because even simple concatenation of text is
fraught.  (Look up Unicode Normal Forms for a primer.)

However, the need is there.  I hear software developers discuss removing regex
uses in favor of using language-specific string search and manipulation.  But
this means learning too many details of text encodings, in order to understand
the limitations of string operations in your programming language.

It's time to lift text to the level of an abstract data type.  It's time for
pattern-based text search and simple text parsing to be available in a uniform
way across programming languages.  And it's time to specify text manipulation
operations abstractly, so these too can work consistently across languages.

With our vision and our concrete goals stated above, we can now discuss the
current system, called Rosie/PEXL.  This system is under active development, and
will be released as Rosie 2.0, replacing Rosie 1.x.

PEXL is the project code name for a radical redesign of the Rosie 1.x
architecture.  The current (PEXL) code is the subject of many
performance-related research projects, looking to make the forthcoming
Rosie/PEXL release competitive with commonly used regex engines.  The current
code is also a testing ground for new grammar constructs, some of which are
sketched in brief below.

Finally, the PEXL code represents the culmination of a years-long effort to
align our implementation with our ideas about what a compiler and runtime for a
domain-specific language could be.  The compiler could look like one for a full
programming language, with recognizable stages of processing and familiar
optimization passes.  The runtime, or _matching engine_, could reflect the
lessons learned from the last two decades of design work on language runtimes,
which depart from older approaches because CPU architectures have evolved so
much, dramatically changing the techniques needed to boost performance.

## Background

### Rosie 1.x

The Rosie project was implemented in Lua (command line interface, RPL compiler
front-end) and C (compiler back-end, "matching virtual machine").  The C code
was originally based on the Lua `lpeg` project, but more and more of the `lpeg`
code was rewritten or removed with each Rosie release.

Around the time of Rosie v1.2, we redesigned the compiler in order to achieve
some new goals, as shown in the feature list below.  The rewrite of the back end
of the RPL compiler necessitated more changes to the vm, taking it further from
its `lpeg` roots.

The current release is Rosie 1.4, which has been stable for some time and
includes features such as 

* Namespaced packages of patterns
* Built-in unit testing
* Support for patterns and input text encoded in UTF-8
* Three kinds of case-insensitive matching: ASCII-only (as found in most regex
  implementations), Case-Fold Simple, and Case-Fold Full; the case folding
  algorithms are defined by the Unicode standard
* Hundreds of patterns provided in the Rosie installation, in the form of a
  "standard library"; any library package, or the entire library, can be
  replaced or ignored
* Matches may be output in a variety of formats: a compact binary encoding that
  a host language (C, Go, Python, Rust, for example) can easily decode; JSON
  (text); S-expressions (text); colorized text for human consumption; a one-bit
  status indicated a match or lack thereof; and others.

### Rosie 2.0

As we look ahead to future releases, we envision a Rosie 2.0 with:

* A pure C99 implementation
* An expanded grammar with some features of PEGs but more practical
* Ahead-of-time compilation of pattern libraries
* The ability to build the runtime without the compiler, for the typical
  scenario in which the compiler is not needed at runtime
* Performance comparable with commonly used regex and PEG implementations

The PEXL code is the result of redesign effort.  It contains a new compiler for
an extended PEG-like language and a new run-time engine (vm plus other
components).  But this code is unfinished as of Summer 2024.  We will say
"Rosie/PEXL" to refer to a forthcoming version of PEXL that can replace the
Rosie 1.x implementation and become Rosie 2.0.

### Terminology

* **The Rosie Project** is an umbrella term for Rosie, PEXL, and related work to
  come, including a coherent library of reliable and efficient text manipulation
  functions. 
  - **Rosie 1.x** versions are currently released.
  - **Rosie 2.0** is a vision (design) for the future. <!-- FUTURE UPDATE -->
  - **PEXL** is the name for the implementation of a new design for Rosie.
  - **Rosie/PEXL** refers to a future version of PEXL that meets our "minimum
    viable product" requirements to be released as Rosie 2.0.

* **Matching** is parsing **text** (the primary input) using a **pattern**.  The
  result is a (possibly null) _match tree_, traditionally known as a _parse
  tree_.

* **Searching** is a generalization of matching in which input is skipped until
  the first match is found, if any.  A pattern that searches for the next match
  to `P` may be written as `S = P / . P`.  A pattern that searches for all
  instances of `P` is, then, `S*`.

* A **span** is a slice of text, or more precisely a sequence of bytes.  Spans
  are defined using start and end positions that are offsets from the start of
  the text.  The first offset is 0.  End positions are numerically one beyond
  the last byte of the span.  (Spans are by convention half-closed intervals,
  though we don't write them with an open bracket and a closing parenthesis.)
  E.g. the span (0, 1) contains only the first character of the text, and (3, 3)
  is an empty span at offset 3 from the start of the text, so (3, 3) is a
  sub-span of (3, 5) in the sense that the former is contained in the latter.

* A **pattern** or **pattern expression** represents a _resultant grammar_, a
  term we introduce here to describe a grammar that is the product of attention
  to two often-competing forces: expressiveness and performance.  Regex achieved
  expressiveness through 40+ years of ad hoc extensions, yielding incompatible
  dialects of inscrutable features with myriad edge cases to consider.  PEGs
  were a step in this direction, away from regex, however:
  - The Packrat Parsing algorithm for PEG parsing requires too much space and
    the backtracking approach is subject to pathologically bad runtimes.
  - Few PEG implementations keep to the formalism.  Many add captures and
    look-behind in order to better serve their users.  The oft-touted formal
    basis for PEGs is, in practice, irrelevant.
	
* A **program** is a collection of pattern definitions and uses defined by the
  Rosie Pattern Language specification.
  
* A **module** is what a developer writes in order to group together a set of
  definitions.  A module is often meant to be imported by other modules.
  
* A **library** is emitted by the compiler as the instantiation of a module.
  Patterns in a library can be used directly for matching.  We have designed for
  dynamic linking, which may or may not ever be needed.  If it is implemented,
  then libraries may be dynamically linked against other libraries and
  dynamically loaded at runtime as well.

* The **compiler** converts a human-centric representation of a pattern or
  collection of patterns into form that is executable by an **engine**.

* The **engine** (or _virtual machine_) is the code that implements matching.
  It requires an **executable** pattern and some input text.

* An **executable** contains everything the **engine** needs to perform
  matching.  The PEXL pattern matching engine, like that in Rosie 1.x, is
  configurable in several ways, notably to produce various forms of output.  The
  most general form of output is a _match tree_ (traditionally, _parse tree_),
  which will be empty if no match is found.

### Resultant grammars

It is not necessary to understand _resultant grammars_ to comprehend the
Rosie/PEXL architecture, so this section can be skipped.

We coined the term _resultant grammar_ in our work on this project.  Our current
working definition is that each of the following is a resultant grammar:
  - string literal, including the empty string
  - sequence of patterns
  - ordered, possessive choice between patterns
  - repetition (both bounded and unbounded) of a pattern
  - generalized look-ahead (for) and look-behind (at) a pattern
  - back-reference to a capture
  - capture of a pattern, in which a pattern expression may be tagged with a
    names; the match tree nodes are captured expressions, each stored with its
    name and input span
  
Our work generalizes look-ahead (defined in the original Parsing Expression
Grammar work) and look-behind (implemented _ad hoc_ in `lpeg` and other PEG
implementations, but not formally defined).  A _resultant grammar_ provides
expansive alternatives to the look-ahead and look-behind (and their respective
negations) of PEGs and Regular Expressions.  Conceptually, they may be defined
this way:

  * `where(X, path, Y)` is an expression that matches pattern `X`, and then
    matches pattern `Y` against the span of the sub-match of `X` indicated by
    `path`.
	- The expression succeeds only if both matches succeed, and the expression
	  containing `where` will include the parse tree for `X`.  The input cursor
	  will be positioned at the first position after the span of `X`.
    - If either `X` or `Y` fail, the entire expression fails; the input cursor
      is restored to its position before `X` was attempted.
    - This expression expands the notion of look-behind to leverage the parse
      tree already constructed, rather than be restricted to only the text
      already matched.
    - A PEG-like look-behind, such as `P <Y` in RPL, is `where(P, /, Y)`, where
      `P` is the _entire pattern_ before the look-behind, and `/` denotes the
      absence of a path down into `P`.
    - Note that the scope of the look-behind, `P`, is part of the `where`
      expression.  PEG-like languages restrict this scope to `P`, the global
      pattern preceding the look-behind.  Further, the target of a PEG-like
      look-behind is limited to the entire span of text matched by `P`, forgoing
      the opportunity to look behind at the _parse tree_ generated by `P`.
    - The generalization of negative look-behind may be written conceptually as
      `where(X, path, !Y)` or perhaps `wherenot(X, path, Y)`.

  * `with(X, path, Y)` works similarly but produces a different result: it matches
    pattern `X`, then matches pattern `Y` against the span of the sub-match of
    `X` indicated by `path`.
	- The expression succeeds only if both matches succeed, and the expression
	  containing `with` will include the parse tree for `Y`.  The parse tree for
	  `X` is discarded.  The position of the input cursor after evaluating this
	  expression is at the first position following the span of `X`.
    - If either `X` or `Y` fails, the entire expression fails, and the position
	  of the input cursor is restored to its position before `X` was attempted.
    - This expression is a unusual twist on look-ahead.  It enables patterns
      that first establish a scope (using `X`) which could be a fixed number of
      characters or a balanced set of braces, for example.  Then the pattern `Y`
      is matched to the span of (possibly a sub-match of) `X`.
    - A negative version may be written conceptually as `with(X, path, !Y)` or
      perhaps `without(X, path, Y)`.

  * `if(X, path, Y)` is a pure predicate.  Unlike `where` and `with`, it
    consumes no input on success.  An `if` expression matches pattern `X`, then
    matches pattern `Y` against the span of the sub-match of `X` indicated by
    `path`.
	- If both matches succeed, the expression succeeds, and matching continues
      with the next expression after the `if`.  The input position is unchanged
      from when `if` was invoked.
    - This expression expands the notion of look-ahead to leverage the ability
      of the parser to produce a parse tree (for `X`), any node of which may be
      the input to pattern `Y`.
    - A PEG-like look-ahead, such as `>Y` in RPL, is `if(.*, /, Y)`.
    - Note that the scope of the look-ahead, `.*`, must be given as part of the
      `if` expression.  PEG-like languages restrict this scope to always be
      `.*`, providing only one kind of look-ahead: unbounded and applying only
      to text, forgoing the opportunity to look ahead at the _parse tree_ for an
      expression.
    - The generalization of negative look-ahead may be written conceptually as
      `if(X, path, !Y)` or perhaps `ifnot(X, path, Y)`.

The extension of back-references to grammars was defined and implemented in
Rosie 1.2, in 2019.  We now believe that finding the referent of a
back-reference is, essentially, the same as resolving a variable reference in a
dynamically-scoped language.  In this analogy, the match tree is akin to a
tree-structured environment of bindings in a programming language.

<!-- ----------------------------------------------------------------------------- -->

## Rosie/PEXL architecture

**TODO: Overview.**

### Compiling

#### Concrete Syntax Tree (CST)

There are 3 ways to generate a CST:

1. Parse Rosie Pattern Language (RPL) 2.0. **Not yet implemented.**  <!-- FUTURE UPDATE -->
2. Parse a low-level internal intermediate language (IIL) written in S-expression form.
3. Call the CST-builder functions directly, generating a CST via the API.

Note: The IIL (internal intermediate language) design uses S-expressions
(sexp.c).  Nested lists are a convenient way to represent trees and forests.  We
have an S-expression parser based on a Scheme-like syntax, e.g.

`(seq "Hello," #\space "world!")`

Concrete Syntax Trees closely resemble the original source, in the case of RPL
and the IIL.  And when the CST is built using the API, the pattern source is
effectively the program that called the expression-builder APIs, and the CST
directly reflects the intent of the developer.

The CST cannot represent illegal syntax.  The parsers for RPL and IIL must
either emit syntax errors or produce a CST, and never do both.  However, the CST
can represent code with other kinds of errors.

To simplify the code, the concrete syntax tree implementation **is** the
S-expression implementation.  The S-expression _atom types_ in PEXL are
_symbol_, _character_ (Unicode codepoint), _byte string_ (no encoding is
enforced), _true_, _false_, and _null_.  An S-expression that is not atomic is
often called a _form_.  It is a list with a symbol as its first element, like
`(foo)` or `(bar 1 2 3)`.  

Forms are used construct expressions.  E.g. `(seq #\0D #\newline)` constructs a
sequence of carriage return followed by linefeed, and `(backref "qux")` makes a
back reference.

An AST is made by starting with a CST.  The AST is then processed (examined,
rewritten) in a series of stages which, if successful, produce an AST suitable
for use in code generation.

#### Abstract Syntax Tree (AST)

An AST does not always represent a valid program (in our implementation).  It
may contain type errors, invalid references, or even invalid constructs.
Mostly, each kind of error is detected in its own stage of processing the AST.

A CST is merely a tree, but an AST structure has two parts:
* the tree itself
* a symbol table

When an AST is first constructed from a CST, every identifier in the CST is
resolved.  That is, each identifier is replaced by a _reference_, which is an
index into the symbol table.  Resolving references is where the lexical scope
rule is enforced.

Rosie/PEXL supports _modules_, which are namespaced collections of patterns.
Identifiers (names) may explicitly refer to another namespace, else they are
local to the current one.

* **Ordinary identifiers** (or "bare", or "unqualified") identifiers are simple
  names, like `email` or `ipv6`.  As in many programming languages, e.g. Java,
  the scope rules of the language determine what an ordinary identifier refers
  to.  By contrast, a qualifier denotes a namespace, a collection of names in
  which to resolve a reference.  Ordinary identifiers lack a qualifier.

* **Qualified identifiers** specify a namespace and a name, e.g. `net.ipv6`.
  These references are resolved during linking, when the implementation of the
  `net` module is available and we can find `ipv6` in its symbol table.
  - Under static linking, the `net` package is available at compile time, and we
    can not only resolve the reference to `net.ipv6`, but we can use metadata
    about this pattern (stored in the symbol table of the `net` package) to
    optimize compilation of patterns that refer to `net.ipv6`.  E.g. we will
    know if that pattern is nullable, if it has bounded length, and other
    properties.  More aggressive optimization, essentially _whole program
    optimization_, is also possible: Because static linking produces a single
    library (file), that file will contain the bytecode for the "main program
    (pattern)" plus all of the bytecode for patterns needed from other packages.
    Thus, all of the bytecode needed at runtime is in one file and that file can
    be optimized "globally".  E.g. pattern bytecode from other packages, like
    that for `net.ipv6` can be inlined, thus eliminated a call and return.
  - Under dynamic linking, but not dynamic loading, a program is compiled
	against a specific implementation of an external package, like `net`.  If a
	program refers to `net.ipv6`, all of the properties of the `ipv6` pattern
	may be used to compile the pattern that calls it, just like with static
	linking.  However, the code for the `net` package remains separate, in its
	own file.  Whole program optimization is not possible.  Another downside is
	that at runtime, a compatible implementation of the `net` module must be
	available when code is loaded into the engine (VM).  Compatible means it
	must have the same properties, because the code calling into the `net`
	package was compiled with knowledge of these.  Yet, there is a
	potential benefit to dynamic linking without dynamic loading:  In a large
	deployment (many instances of Rosie/PEXL running) that uses large packages,
	having each package in its own file can save memory, exactly as happens with
	shared libraries (`.so` on Linux, `.dylib` on macOS, `.dll` on Windows).  A
	library can be loaded into memory once, and used simultaneously by many
	processes.  (This is why `libc` is almost always dynamically linked.)
  - Under dynamic linking _and_ dynamic loading, the external package,
    e.g. `net` is not available at compile time.  When we compile a pattern that
    refers to, say, `net.ipv6`, all we can do is add this qualified name to a
    list of references that must be resolved later, when code is loaded into the
    engine (VM).  We cannot assume any properties of this pattern, e.g. whether
    it may have the "no fail" property or is of bounded length, so we forgo many
    optimization opportunities.  We do not expect to implement this technique,
    and include it here only for completeness in the context of design.

We are currently implementing static linking and doing some forms of whole
program optimization.  We have designed for the implementation of dynamic
linking in the future if it becomes a requirement.  In large systems, dynamic
linking may bring decreases in memory usage and increases in performance.  But
we are not there yet.  We do not see any advantages to dynamic _linking and
loading_ (analogous to `dlopen`/`dlsym`) at this time and have not designed for
it.

#### Resolving references

The AST transformations begin with a CST that is essentially an AST without a
symbol table.

The _resolver_ transforms it into an AST in which all references are resolved,
and a symbol table containing all of the identifiers.  As the AST is further
processed, the symbol table will store information about each identifier, such
as its type and other meta data.

Note that a qualified identifiers resolves to a value from an external package,
while an ordinary identifier resolves to a value defined inside the same module.

The requirement to resolve external references puts a constraint on compilation
order: A package must be compiled before any module that refers to its patterns.
The module system design, consequently, does not allow for circularity.  A graph
of module dependencies is a DAG, and a topological sort yields a viable
compilation order.

The resolver has these responsibilities:

1. Construct the symbol table, with an entry for each identifier in the AST
2. Resolve all references, emitting errors for unresolved references

Note that unresolved references can take two forms.  An ordinary identifier is
unresolved if it is not visible (under the lexical scope rule) at its point of
use.  A qualified identifier is unresolved if its package is not available, or
if the name is not visible outside of the package

#### Macro expansion

Our experience in the Rosie Project suggests that syntactic sugar is especially
useful in certain pattern matching situations.  For example, case-insensitive
matching can be achieved by applying a macro transformer to a pattern that
converts it into a case-insensitive one.  Similarly, a macro can be used to
convert a pattern `X` into a pattern that _searches_ for `X`.  In RPL, this is
written `find:X` and it expands (hygienically) into `S` where `S = X / {. S}`.

Macros and built-in functions in RPL are applied using the `:` operator,
e.g. `findall:P` (which finds every instance of `P`) or `cfs:"asap"` (which
finds "asap" under Unicode's _simple case folding_ form of case insensitivity).

_Syntactic sugar_ is syntax supported for convenience of the user (developer)
but not known to the core of the compiler.  Code is desugared early in the
compilation process, by converting it into (often larger) constructions made
from more primitive operations.  E.g. in ordinary programming, `i++` is
syntactic sugar for `i = i + 1`.

A macro is a user-defined code transformation that may work just like the
in-built desugaring phase.  In Rosie 1.4, the only macros are built into the
language.  These are `find` and `findall`, various forms of Unicode case
transformation like `cfs` and `cff`, and a few others.

One macro expansion implementation may be sufficient for syntactic sugar,
in-built macros, and (in future) user-defined macros.

Importantly, the input to macro expansion must be a valid AST, and the result
must also be a valid AST.

#### Typechecking

**TODO:**  Validate RPL semantics, emitting errors for invalid programs.


#### Lowering

An AST is comprised of a variety of node types.  To simplify later stages of
compilation, we rewrite the AST so that it uses fewer types of nodes, and
simpler types of nodes -- ideally both.

For example, if an AST node for matching a string refers to a one-character
string, this node can be replaced by a simpler one that matches a single
character. 

#### Simplification

The AST is simplified in other ways beyond lowering.  For example, dead code can
be eliminated. (E.g. in `P* / Q`, the pattern `Q` will never be attempted,
because `P*` cannot fail.)

Sequences containing only one pattern can be replaced with one node, for the
pattern itself.

Sequences of string and character patterns can be replaced with one node, a
single string matching node built from the concatenation of the original
sequence.

Right-associative sequence and choice expressions are converted to
left-associative during simplification.  E.g. `(A B) C` becomes `A (B C)`.  This
minor change streamlines some later processing.

Many other simplifications are possible, though some are best considered
optimizations, and are described separately.

#### Specialization

Users of our system should be able to write patterns once, and then use them to
match text in any one of several Unicode encodings.  To compile a pattern, the
user must specify the encoding of the text that will be used.  (Text encoding
cannot be reliably detected at run-time, so in general, developers must know how
their text is encoded.)

Conceptually, of course, we could generate code for all supported encodings, but
that is purely a convenience feature.

In the _specialization_ phase, an AST is transformed so that it will match input
text in a particular encoding.  The result is an AST that has no nodes
describing strings or characters.  All such nodes are replaced with nodes that
refer only to bytes.  (Our design is that the engine only needs to understand
bytes.) 

This phase is a form of lowering, but it is specific to character encodings and
happens late in the series of AST transformations.

Some simplification passes may be run again after specialization.

#### Optimization

The input to the various optimization phases is always a valid, lowered, and
specialized AST.  The validity requirement is obviously needed.  The lowered
requirement lets us consider a restricted set of AST node types instead of the
full set.  And after specialization, there are fewer and simpler node types,
because the AST concerns only bytes and not higher-level concepts like
characters or glyphs or encodings.

Some AST-based optimizations are described below.  The list is not ordered, in
part because some optimizations passes may be executed more than once.

* **Look-around elimination** can remove an entire look-ahead (or look-behind)
  expression.  First, `>P` (`<P`) can be replaced by a no-op if `P` has the
  `nofail` property; and second, `>P` (`<P`) can be replaced by `Fail` if `P`
  always fails.  A similar elimination analysis can be applied to negative
  look-arounds. 
  
* **String search introduction** replaces `find:(S P)`, where `S` is a
  sufficiently long string, with a sequence of `find:S P`.  Code generation for
  `find:S` will use a bytecode instruction that performs substring search.
  (Here, after specialization, a string is a sequence of bytes.)
  
* **Search conversion** replaces expressions like `(!P / .)* P`, which are
  "manual searches" for `P`, with `find:P`.  Another search expression appears
  as the definition `S = P / . S`, which can be replaced by `S = find:P`.

* **Left factoring** resembles hoisting code out of loops.  An expression like
  `AB / AC` can often be written as `A (B / C)`.

* **Choice optimization** consists of rewriting choices to take advantage of
  bytecode instructions whose purpose is to match one of a small set of short
  byte sequences.  Unicode text matching, particularly using simple or full case
  folding, produces many choices that have this property.  So does matching for
  Unicode look-alike glyphs.
  
* **String optimization** means rewriting nodes that match short strings to
  leverage bytecode instructions that match short byte sequences without a
  lookup in symbol table.  In other words, long strings are stored outside of
  the bytecode, and looked up at runtime, whereas short strings can be encoded
  within the instruction stream.

#### Code generation

The input to the code generator is an AST that is valid, lowered, and
specialized.  AST-level optimizations may or may not have been performed. 

At this point, the user has already committed to a text encoding, and the AST
deals only with bytes.

The code generator is (mostly) a straightforward recursive descent on each AST,
generating bytecode and managing symbol table entries.  Small optimizations
occur during code generation.

Provided that SIMD compilation is enabled, the code generator will emit the data
needed by the matching engine to leverage SIMD instructions supported by the CPU
at runtime.  We expect SIMD compilation to be enabled by default, except perhaps
for debugging or for embedded systems.  At run-time, the engine will use SIMD if
it was compiled on a platform supporting it, such as ARM CPUs with the Neon
feature, and essentially all x86-64 CPUs.

The generated bytecode is meant to be portable across machines with the same
endianness.  For instance, patterns compiled on the little-endian x86-64 should
work without modification on, say, ARM64.  Currently, this portability is
enabled due to the high level bytecode.  If the project ever changes to include
CPU-specific bytecode, then generated code will have to be tagged for a specific
platform. <!-- FUTURE UPDATE -->

Pattern bytecode is always compiled as a library, similar in concept to a Unix
shared library.  A library may be loaded by a Command Line Interface, at which
time all defined (and globally visible) patterns are available for the CLI to
use for matching.  Note that definitions may be declared `local` and are then
not visible outside of the module.

Similarly, the Rosie/PEXL API may be used to load a module within a program.  In
this case, an engine is instantiated, and then one or more libraries are loaded
into the engine.  Now a matching API may be called with an argument that is a
defined (and visible) pattern name from the library.  The loading process
resembles using `dlopen` and passing a pattern name to a Rosie/PEXL API is
reminiscent of using `dlsym`.

Upon loading, a library is assigned a name which is used as a prefix in a
qualified identifier.  E.g. when loading a library of network patterns, we might
call it `net`.  Then, the email address pattern defined there would be referred
to as `net.email`, and the ipv6 pattern as `net.ipv6`.

### Walking the AST

**TODO: Describe walking the AST, emitting bytecode as we go.  Mention the use
of the "guard instruction" pointer that, when set, points to an instruction that
makes some guarantee about the current byte of input.  When a guard is
available, we can sometimes emit a more efficient instruction than we would
otherwise.**


### Bytecode optimization

* A **tail recursion eliminator** converts ordinary recursive patterns into
  iterative ones.  It works on one pattern at a time.

* A **peephole optimizer** for eliminates unneeded jumps and calls.  It
  currently applies to an entire package of patterns, but could be adapted to
  work on a single pattern if needed. <!-- FUTURE UPDATE -->
  
* **Loop unrolling** converts fixed-length loops into a series of copies of the
  loop body, thus avoiding the overhead of incrementing and testing the loop
  counter, as well as branching back to the top of the loop body.  When the
  number of iterations is large or unbounded, the loop body is emitted a
  number of times, followed by a loop that handles the remaining iterations.

* **Inlining** replaces calls to patterns with actual pattern code, like
  function inlining in ordinary compilers.  Currently, it operates on bytecode,
  but changing to an AST-based approach may be beneficial.
  

<!-- ----------------------------------------------------------------------------- -->
### Linking

<!-- FUTURE UPDATE FOR THIS ENTIRE SECTION -->

We have designed for both static and dynamic linking of pattern libraries, but
only static linking is currently implemented.  This means that to compile a
module of patterns, all of the pattern source code must be available at compile
time.  The result is a single pattern library which is the only artifact needed
at runtime to match input text against any defined (and visible) pattern in the
library. 

Static linking allows the opportunity to perform whole program optimizations.

Dynamic linking (but not dynamic loading) is an alternative for which we have
designed.  Indeed, some support is already in the code.  Here, code that refers
to an external package is compiled against that package, giving the compiler
detailed knowledge of the properties of the external patterns.  If the source
for external packages has not changed since compilation, then (1) our
compilation remains valid, and (2) we can load the already-compiled external
library at runtime, possibly sharing it with other processes.

It is not clear at this time that dynamic linking is needed, but in a large
system with many processes running Rosie/PEXL and some large pattern libraries,
there may be benefits such as a reduction in memory used and an increase in
performance due to cache effects.


<!-- ----------------------------------------------------------------------------- -->
### PEXL Engine (Virtual Machine) Overview

The _matching engine_ is the runtime component.  It is implemented as a virtual
machine for the Rosie/PEXL bytecode, and takes these parameters:

* **Pattern name**
* **Pattern library** in which bytecode for the named pattern can be found
* **Input** text, in the same character encoding for which the named pattern was
  compiled, e.g. UTF-8
* **Options** that modify the default configuration, e.g. selecting the format
  for the match output data

#### Early design

The design of the engine is heavily influenced by Roberto Ierusalimschy's
`lpeg`, a PEG library for Lua.  The original implementation of Rosie was based
on `lpeg`, though the pattern compiler was gradually rewritten by the time Rosie
1.4 was released.  However, the Rosie 1.4 engine is essentially the `lpeg`
engine with a few enhancements and additions.  By contrast, the Rosie/PEXL
engine differs substantially from its ancestors.

Some aspects of the `lpeg` (and, therefore, Rosie 1.x) design remain in
Rosie/PEXL:

1. The matching algorithm essentially follows a backtracking approach, so the
   engine maintains a _backtrack stack_.  E.g. because choices are ordered and
   possessive, the choice `A/B` produces bytecode that pushes a stack frame
   containing information on how to execute `B`; after that stack push comes the
   instructions for matching `A`.
2. The backtrack stack functions as part of an exception-handing mechanism.  The
   choice `A/B` is like `try A; catch B`.  The failure of any instruction traps
   to a routine that finds a handler on top of the backtrack stack.

As Rosie evolved, we decided to decouple the pattern compiler from the matching
engine, so that the engine could be built and used separately in situations
where the compiler was not needed.  This work was only partially completed,
though the Rosie 1.4 code contains facilities for storing compiled patterns on
disk, and for loading them into an engine not integrated with the pattern
compiler.

The integrated Lua implementation was needed by the pattern compiler, which in
Rosie 1.x was written mostly in Lua.  We wanted a minimal matching engine, so we
refactored the engine to remove its dependence on Lua.  The implementation used
a few insubstantial Lua functions but, more important to this effort, used data
structures like Lua tables by calling the Lua API.

In 2019, the Rosie matching engine had been rewritten in pure C99, with no
external dependencies.  In a proof of concept that remains, unused, in the Rosie
distribution (in `src/rpeg/future/test`), the engine components are
compiled together with a driver program, `match.c`, which takes two command-line
arguments: a file containing a compiled pattern (bytecode), and a file of input
text against which the compiled pattern is matched, line by line.

Compiling `match.c` produced a macOS executable that was under 30KB on disk, and
dynamically links only to `libSystem` (the macOS aggregation of `libc`,
`libpthread` and a few other frequently needed libraries).  Rosie 1.4 has a CLI,
`rosie`, that is roughly 320KB on disk (and dynamically links to `libSystem` and
`libedit`).  Comparing the executables, it appears that the matching engine is
more than 10x smaller than the complete system of CLI, pattern compiler, and
engine.  A more apt comparison would account for the Lua code that is loaded by
the CLI and pattern compiler at startup time.  The files of Lua bytecode add up
to around 900KB on disk.  While comparing on-disk sizes is a limited measure,
the matching engine at 30KB is much, much smaller than the complete system at
roughly 1220KB (binary plus Lua bytecode), or 1.2MB.

> A note about the Unicode tables: Around 365KB of Unicode data tables are
> needed by Rosie 1.4 to execute Unicode case folding algorithms, which provide
> users with case-insensitive matching capabilities that follow each of the
> Unicode case-equivalence definitions.  The Rosie architecture compiles out all
> knowledge of Unicode so that the matching engine can work only with bytes.
> Consequently, data about Unicode case equivalence is not needed at runtime.

#### PEXL engine design points

In the PEXL project, the matching engine was thoroughly overhauled from its
previous form, the Rosie 1.x engine.  Some of the new features are discussed in
subsequent sections below.  Here, we mention briefly some design points that are
pervasive in the PEXL version of the matching engine.

1. **Arena-like memory management:** Allocating memory can be relatively
   expensive, so we do it sparingly.  An _arena_ is a chunk of memory, obtained
   via `malloc`, that we manage in our own code.  E.g. the backtrack stack is
   allocated once, as an array sized to hold a large number of frames.  If the
   stack hits its limit, it will be dynamically expanded, which is expensive.
   But if we provide a generous initial size, expansion is rare.  When it does
   occur, it doubles the size of the stack, making further expansion unlikely.
   Another performance benefit derives from the memory layout, which is an array
   of structures.  When code iterates through even a short a series of data
   structures, the CPU caches are more effective when those structures are
   adjacent in memory.  (Contrast this with "structure of arrays".)
2. **Computed GOTO:** Virtual machines are conventionally written as one large
   `switch` statement containing a `case` for each instruction the vm provides.
   It is generally more efficient to use the instruction opcode as the index
   into a "jump table", an array of addresses.  The C `goto` statement effects a
   jump to the engine routine that handles the next instruction.  In addition to
   avoiding the overhead of `switch`, which tests each case until a match, we
   may benefit also from CPU branch prediction.  In hardware, conditional
   branches are predicted and one branch is speculatively executed.  Programs
   execute much more quickly when prediction is accurate than when it is not.
   Hardware tables can hold the addresses of many jump instructions, predicting
   each one individually.  If our engine supports N instructions, and the code
   for each one ends with a `goto` (to an address computed from the next
   instruction's opcode), then the hardware effectively notices which
   instructions are likely to follow others.
3. **Small hot loop:** The engine is a virtual machine where the expected "hot
   loop" is the fetch-decode-execute cycle.  Ideally, this entire loop would fit
   into the CPU instruction cache (I-cache), so that its machine code is
   available as quickly as possible.  In reality, the engine's main loop is too
   large.  However, we can factor out complex (long) and infrequently used
   routines into functions marked `noinline`.  This keeps the main loop small.
   If we profile the engine on typical workloads and group the most frequently
   executed engine (bytecode) instructions together (adjacent in the C code), we
   may be able to achieve a desirable state in which long sequences of machine
   instructions are executed directly from the CPU I-cache.  Infrequently used
   engine bytecodes will very likely incur a cache miss.  And more rarely, when
   a complex and rarely used engine operation is performed, there will surely be
   cache misses as the I-cache fills with machine instructions for that
   operation.
4. **Branch and cache management:** Modern CPUs run programs dramatically faster
   when data and instructions are cached and when branches are correctly
   predicted.  We can code carefully to promote such a scenario.  First, we can
   eliminate branches when possible.  (There are many resources available for
   learning how to write "branchless code".)  Second, we can sometimes code a
   computation ahead of when its result will be used, so that while we wait the
   result, other work can get done.  While CPUs are capable of performing "out
   of order execution" to achieve exactly this goal, they work with a limited
   pool of instructions.  When we write the code for our engine, we can observe,
   for instance, that advancing the program counter by the current instruction
   size is best done early in the execution of a bytecode instruction, rather
   then immediately before jumping to the code for the next bytecode
   instruction, which requires the opcode of the next instruction.
5. **No C strings:** Our engine will be called from a variety of programming
   languages.  (The Rosie engine has interfaces from C, Java, Go, Rust, Python,
   and Haskell.)  Most languages do not use C strings, favoring a "pointer and
   length" internal representation.  Suppose that, like `lpeg`, we were to build
   our engine to rely on null-terminated byte arrays as input.  (This assumption
   about the representation of the input can speed up some operations.)  Imagine
   now that our engine is being called from, say, Rust or Go or Python.  In each
   case, a "foreign function interface" (FFI) provides a way to call the PEXL
   engine, which is a C library.  If the engine input is a string in, e.g. Go,
   then we have an unaligned pointer and a length.  If our engine needed a null
   at the end of the string, the only safe way to achieve this is to make a copy
   of the input.  One of our use cases is big data, so we would like to avoid
   such copies.  In fact, we would also like to avoid mutating the input.  If we
   neither copy nor modify the input, the engine can "immutably borrow" the
   input data, to use the Rust terminology.  This approach avoids a memory copy
   and an allocation to hold the copy, while preserving the ability of multiple
   threads to read the memory holding the input.  When processing large amounts
   of text, files are typically memory-mapped, and that memory may be marked as
   both read-only and shared, creating an opportunity for efficient sharing.


#### Notable PEXL engine features

The PEXL redesign of the matching engine (or _virtual machine_) adds these
features and incorporates other differences from the `lpeg` and Rosie 1.x
designs:

1. **Package support** enables a future scenario in which pre-compiled libraries
   are dynamically linked and loaded, but it neatly solves a current need as
   well, involving the separation of the compiler and runtime.
   - Ahead-of-time compilation is part of PEXL but is not supported in `lpeg` or
     Rosie 1.x.  A tiny library is constructed on the fly by the runtime in
     order to call a user-specified library pattern.
   - Not really a trampoline, this tiny library is more like a spring-board.  It
     calls the library routine, which returns to spring-board when it finishes.
     The spring-board then terminates the engine via a `Return` instruction.
   - Other ways of achieving this capability might be simpler, but what we have
     today is a proof of concept for supporting dynamically-loaded libraries one
     day.
   - Note that dynamically-loaded libraries could be memory-mapped as read-only,
     allowing them to be shared across processes.  While memory is plentiful,
     the benefit to sharing a single in-memory copy of a library is likely to be
     found in reduced cache usage when many processes are using the same
     libraries, as may be the case on a production server, particularly one that
     acts as a stage in a big data pipeline.
2. **Dynamic capture management** allows the engine to pause and resume
   capturing.  Because look-around patterns should not capture (according to RPL
   semantics), we must either limit the patterns that can be look-around targets
   (the `lpeg` approach) or we allow patterns that capture and we disable their
   captures dynamically.  We chose the latter, which enhances the developer
   (user) experience.  Crucially, it also bolsters pattern re-use, which is an
   important activity in a package-oriented solution like Rosie/PEXL.
3. **Look-behind loops** require engine support if look-behind is not restricted
   to fixed-length target patterns (as it is in `lpeg` and other systems).  The
   conventional text-based look-behind, as opposed to the generalized version
   planned for RPL 2.0, requires the engine to walk backwards in the input
   looking for matches to the target pattern, assuming that pattern has variable
   length.  Notably, Unicode's multi-byte encodings are a frequent cause of
   variable length look-behind:  A set of characters encoded in UTF-8 may
   include some that are 1-byte, and others that are 2-bytes.
4. **Fixed-iteration loops** exist to enable tunable loop unrolling as an
   optimization.  Both `lpeg` and Rosie compile `P{100}` into a sequence of 100
   copies of `P`.  For a pattern `P` that is complex (made of many bytecode
   instructions), the resulting code can be large -- with Rosie we have observed
   hundreds of MB in practice, as have `lpeg` users.  Instead, our compiler
   emits a fixed-iteration loop.  Then, our loop unrolling optimizer determines
   whether and how much to unroll that loop to achieve a good balance between
   code size and execution speed.
5. **SIMD support** leverages vector instructions, when supported by the CPU, in
   the implementation of a limited number of bytecode instructions.  An example
   is **Find**, an instruction that finds the next input byte belonging to an
   arbitrary set.  **Find** currently exists in 3 variations: (1) finding a
   single byte uses `memchr`; (2) finding a byte in a set of size 2-8 uses a
   SIMD algorithm; and (3) finding a byte in a set of size 9-255 uses a slightly
   slower SIMD algorithm.
6. **Backreference support** enables the back-reference specification that
   appeared in Rosie 1.2, in which the existing concept of back-references for
   regular expressions was extended to grammars, where the referent is a node
   in a parse tree, not an entry in an array.
7. **In-built search** comprises several capabilities provided directly by the
   engine and exposed as instructions that the compiler can use instead of
   emitting a (typically longer) series of less efficient instructions.
   - The **Find** instruction takes a set of byte values as a parameter, and
     skips ahead in the input to the nearest byte that is in that set.  Without
     this instruction, such a search requires a loop implemented in bytecode.
   - The **String** instruction takes a byte string (array) and matches it byte
     for byte.  Without this instruction, one byte-match instruction is needed
     for each byte in the string, resulting in a large code size and per-byte
     overhead for instruction decoding and dispatch.
   - The **FindString** instruction employs a "fast" string search algorithm to
     skip ahead in the input to the nearest match to the given string.  The
     current implementation uses the C library's `strstr`, but we have
     implementations of Boyer-Moore, Knuth-Morris-Pratt, and others to choose
     from. <!-- FUTURE UPDATE -->
8. **Generalized look-around support** is currently being implemented.	 
   <!-- FUTURE UPDATE -->

### PEXL engine operation

**TODO: Describe the engine's state: backtrack stack, capture stack, registers.**

### PEXL output generation

**TODO: Describe how output is produced from the capture stack after matching.**



