/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vm.c  the "matching virtual machine"                                     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "vm.h"
#include <time.h>
#include <alloca.h>

#include "find.h"
#include "print.h"
#include "stack.c"     /* We want this in the same TRANSLATION UNIT */
#include "bitflags.h"

#define FAIL 1        /* Must be > 0 to be distinct from OK and error values */

/*
   The use of VmState is done for vm performance gains.

   We want the values that the vm consults frequently to be kept in
   registers.  E.g. the current input position (c), the start/end of
   input (boi/eoi), and the PC.  This argues against storing those
   values in a VmState structure, and we verified this experimentally.

   We also want to keep the vm() function small, to take advantage of
   the CPU's instruction cache.  Moving infrequently used code to
   functions outside of vm() helps, if we mark those 'noinline'.  The
   do_lookbehind() function is an example.

   However, do_lookbehind() needs to read and change some of the vm
   state, like the current position.  So we take the address, e.g. &c,
   and pass it to do_lookbehind().  But taking the address forces this
   value to be stored in memory, instead of in a CPU register.

   To prevent the C compiler from storing frequently used values in
   memory instead of registers, we copy those values into a VmState
   structure, pass it to do_lookbehind(), and then copy the (changed)
   values back into local variables, the ones we hope can stay in
   registers.

   Note that do_unwind() does not cause the frequently used values to
   be stored in memory, because we marked it 'inline' and (so far) the
   compiler has agreed to this request.  Inlining allows direct use of
   register variables, so while the code appears to take the addresses
   of the current input position and other values, the generated code
   won't actually do this.

 */
typedef struct VmState
{
  const char *c;
  const char *boi;
  const char *eoi;
} VmState;

__attribute__((unused))
static void fprint_inst (const pexl_Binary *pkg, const Instruction *pc) {
  /*
    The IGiveup instruction prints strangely using the usual
    instruction printer, because it does not exist in a package.
  */
  if (opcode(pc) == IGiveup) {
    fprintf(stderr, "xxxx  %s\n", OPCODE_NAME(opcode(pc)));
  } else {
    fprint_instruction(stderr, pkg, pc);
  }
}
static Instruction giveup = {.i.code = IGiveup, .i.aux = 0};

static inline void pop_btentry (BTStack *stack,
				const char **c_ptr,
				bool *capturing_ptr,
				const char **boi_ptr,
				const char **eoi_ptr) {
  BTEntry *bt = btstack_pop(stack);
  assert(bt); /* BTStack has IGiveup at bottom */
  if (unlikely(BTLOOKAROUND(bt->frametype)))
    switch (bt->frametype)
      {
	case BTLookAhead:
	  *c_ptr = *boi_ptr;
	  *boi_ptr = bt->contents.lookaround.boi;
	  *eoi_ptr = bt->contents.lookaround.eoi;
	  *capturing_ptr = bt->contents.lookaround.capturing;
	  break;
	case BTLookBehind:
	  *c_ptr = *eoi_ptr;
	  *boi_ptr = bt->contents.lookaround.boi;
	  *eoi_ptr = bt->contents.lookaround.eoi;
	  *capturing_ptr = bt->contents.lookaround.capturing;
	  break;
	case BTNarrow:
	  *c_ptr = bt->contents.narrow.c;
	  *boi_ptr = bt->contents.narrow.boi;
	  *eoi_ptr = bt->contents.narrow.eoi;
	  *capturing_ptr = bt->contents.narrow.capturing;
	  break;
	default:
	  break;
      }
}

__attribute__((unused))
static void print_capstack (const pexl_PackageTable *pt,
			    const char *input,
			    CapStack *captures) {
  pexlEncoder fn = get_encoder_fn(PEXL_DEBUG_ENCODER);
  assert(fn);
  fn(pt, input, 0, captures->base, captures->next, NULL);
}

/*
  This is tricky to get right, but because we are inventing a
  backreference operator for a PEG-like grammar, we get to define what
  is the correct behavior. :)

  The definition pioneered in Rosie 1.x is "the most recent capture
  with the same name matches, but not within a finished recursive call
  to the same current open rule".  This allows extra/examples/html.rpl
  to work properly (from the Rosie repository).

  In Rosie, there was always one mandatory capture surrounding any
  pattern.  Any other captures were optional.  The mandatory capture
  provided an anchor of sorts: There was always an OPEN capture frame
  at the bottom of the capture stack.

  PEXL does not have this mandatory outermost capture, so we must
  adapt to that situation.

*/
static bool find_prior_capture (CapStack *stack, pexl_Index target_idx,
			       const char **s, const char **e) {
  pexlCapture *capture = stack->base;
  pexl_Index captop = capstack_size(stack);
  pexl_Index i;
  if (captop == 0) return false;

  /* Skip backwards past any immediate OPENs. */
  for (i = captop - 1; i > 0; i--) {
    if (!isopencap(&capture[i])) break;
  }
  trace("backref",
	"- passed over %d consecutive OPENs in stack", captop - i - 1);
  /* For our purposes, we are now looking at the end of the stack. */
  int cap_end = i;

  /* Scan backwards for the first OPEN without a CLOSE, if one exists. */
  int outer_cap = 0;
  int outer_capidx = 0;
  int balance = 0;
  for (; i > 0; i--) {
    trace("backref", "  - Looking for first unclosed open, i = %d\n", i);
    if (isopencap(&capture[i])) {
      if (balance == 0) break;
      balance += 1;
    } else {
      if (isclosecap(&capture[i])) {
	balance -= 1;
      } 
    }
  }
  if (balance != 0) {
    trace("backref",
	  "- Found no unclosed open, so there were no open recursive calls\n");
    outer_cap = 0;
    outer_capidx = -1;
  } else {
    trace("backref", "- Found FIRST unclosed open at frame %d, handle is %d\n",
	  outer_cap, outer_capidx);
    outer_cap = i;
    outer_capidx = capidx(&capture[i]);
  }  
  /*
    Now search backward from the end of the stack for the target,
    skipping any other instances of outer_capidx, because those are
    the results of inner (completed) recursive calls.
  */
  for (i = cap_end; i >= outer_cap; i--) {
    trace("backref",
	  "  - Looking for target at frame i=%d, which %s with handle %d\n",
	  i, isopencap(&capture[i]) ? "OPEN" : "CLOSE", capidx(&capture[i]));
    if (isopencap(&capture[i]) && (capidx(&capture[i]) == target_idx)) {
      if (outer_capidx != -1) {
	// outer_cap and its idx signify a recursive call (with handle
	// idx), and if we find a matching target within such a call,
	// we skip past it.
	trace("backref", "  - Found candidate... determining if it is inside "
	      "a closed recursive call to handle %d\n", outer_cap);
	balance = 0;
	int j;
	for (j = i - 1; j >= outer_cap; j--) {
	  if (isopencap(&capture[j])) {
	    trace("backref", "looking at open capture j = %d\n", j);
	    if ((balance >= 0) && capidx(&capture[j]) == outer_capidx) break;
	    balance += 1;
	  } else if (isclosecap(&capture[j])) {
	    balance -= 1;
	  }
	}
	if (j == outer_cap) {
	  trace("backref", "  - No other instances of outer_cap to skip over\n");
	  break; /* Nothing to skip over */
	}
      } else {
	// not inside a recursive call, so we found our match!
	break;
      }
    } // test for a candidate match
  } // for each frame in the stack
  if (i == outer_cap - 1) {
    trace("backref", "- Did not find target; continuing the backwards search\n");
    for (i = outer_cap; i >= 0; i--) {
      if (isopencap(&capture[i]) && capidx(&capture[i]) == target_idx) break;
    }
    if (i < 0) return false;
  }
  /* This the open capture we are looking for */
  assert(isopencap(&capture[i]) && capidx(&capture[i]) == target_idx);
  trace("backref", "- FOUND open capture at frame %d, handle is %d\n",
	i, capidx(&capture[i]));
  *s = capture[i].s;		/* start position */
  /* Now look for the matching close */
  i++;
  int j = 0;
  while (i <= captop) {
    trace("backref", "  - looking at i = %d (captop = %d)\n", i, captop);
    if (isclosecap(&capture[i])) {
      if (j == 0) {
	/* This must be the matching close capture */
	trace("backref", "i = %d: found close capture\n", i);
	*e = capture[i].s;               /* end position */
	return true;	                 /* success */
      } else {
	j--;
	assert(j >= 0);
      }
    } else {
      assert(isopencap(&capture[i]));
      j++;
    }
    i++;
  } /* while looking for matching close*/
  /* Did not find the matching close */
  trace("backref", "- Did not find matching close!\n\n");
  warn("vm", "searching for backreference and found unbalanced capture stack");
  return false;
}

/* Find the prior capture that we want to reference */
static int __attribute__((noinline)) find_backref (const char *c,
						   const char *eoi,
						   CapStack *captures,
						   int32_t target_capture) {
  const char *startptr = NULL;
  const char *endptr = NULL;
  size_t prior_len;
  trace("vm", "Entering find_backref for symtab idx #%d", target_capture);
  bool have_prior = find_prior_capture(captures, target_capture, &startptr, &endptr);
  if (have_prior) {
    assert(startptr && endptr);
    assert(endptr >= startptr);
    prior_len = endptr - startptr;
    assert(eoi >= c);
    if (((size_t)(eoi - c) >= prior_len) &&
        (memcmp(c, startptr, prior_len) == 0)) {
      return prior_len;
    }
  }
  return ERR;
}

#define sign_extend_aux(val) \
  (((val) & 0x800000) ? (int)((val) | 0xFF000000) : (val))

__attribute__((noinline)) static int
do_narrowtospan (BTStack *stack,
		 VmState *vm,
		 int32_t A,
		 int32_t X,
		 bool capturing) {
  trace("vm",
       "Entering INarrowToSpan with symtab index %d, path index %d", A, X);
  assert((A >= 0) && (X >= 0));

  // Push the current position, input fences and capturing state so we
  // can restore them later using IWiden
  if (unlikely(btstack_push_narrow(stack, vm->c, vm->boi, vm->eoi, capturing)))
    return PEXL_MATCH__ERR_BTLIMIT;

  // TODO: Look up the needed span in the capture stack
  // TEMP: Setting these to current boi and current pos
  const char *span_start = vm->boi;
  const char *span_end = vm->c;

  trace("vm",
	"Found the needed span: "
	"[%" pexl_Position_FMT ", %" pexl_Position_FMT ") ",
	span_start - vm->boi, span_end - vm->boi);

  vm->boi = span_start;
  vm->eoi = span_end;
  return OK; /* Success */
}

__attribute__((noinline)) static int
do_lookbehind (BTStack *stack,
	       VmState *vm,
	       int32_t A,
	       int32_t X,
	       bool capturing,
	       const Instruction *pc,
	       const char *bof) {
  A = sign_extend_aux(A);
  trace("vm",
       "Entering ILookBehind pos delta = %d, leftfence delta = %d",
	//signedaux(pc), addr(pc)
	A, X);
  /*
     pexl_Position delta is signedaux(pc).  We start searching at
     current position + delta.
  */
  assert(A <= 0);

  if (btstack_push_lookbehind(stack,
                              vm->boi, vm->eoi,
                              capturing))
    return PEXL_MATCH__ERR_BTLIMIT;

  vm->boi = bof;   /* Allow looking behind past BOI (but not past BOF) */
  vm->eoi = vm->c; /* Save cursor position; lookbehind ends at cursor */

  /*
     Set the current position (in the input) to be the first candidate
     position where the lookbehind target pattern could possibly
     match.  The compiler sets 'pos delta' to be the minimum length of
     the lookbehind pattern.
  */
  vm->c += A;
  if (vm->c < vm->boi)
  {
    trace("vm",
         "min length of lookbehind pattern (%d) "
         "exceeds chars before current position (%ld)",
	  A, vm->c - vm->boi);
    return FAIL;		/* goto fail */
  }
  /* Save current position */
  const char *leftfence;
  if (X < 0) {
    /*
       Clamp leftfence to start of input if it would have been before
       start of input
    */
    if (X < (vm->boi - vm->c))
      leftfence = vm->boi;
    else
      leftfence = vm->c + X;
  } else {
    /*
       When max len, i.e. addr(pc), is zero, the target pattern is
       unbounded
    */
    trace("vm", "max length of lookbehing pattern is unbounded");
    leftfence = vm->boi;
  }
  /* Push pc because looping continues at next instruction */
  if (unlikely (btstack_push_backloop(stack,
				      pc,
				      vm->c, leftfence)))
    return PEXL_MATCH__ERR_BTLIMIT;
  trace("vm",
       "lookbehind search pos is %lu (looking at %c)",
       (vm->c - bof), *vm->c);
  return OK; /* Success */
}

/*
   When unwinding the stack, there are two categories of stack frames:

   Call/XCall, LookAhead LookBehind ==> pop all consecutive frames in
   this category, until we get to a frame of the second category.

   Choice, BackLoop ==> pop this frame (conditionally, for
   BackLoop) but jump to the address saved in this frame.

   Note: We do not check if pop returns NULL here, because there is
   always a choice frame at bottom (pointing to IGiveup).
*/
static inline int do_unwind (BTStack *btstack,
			     CapStack *capstack,
			     const char **c_ptr,
			     bool *capturing_ptr,
			     const Instruction **pc_ptr,
			     const char **boi_ptr,
			     const char **eoi_ptr,
			     const char *bof) {
  BTEntry *btz = btstack_top(btstack);
  while (!BTUNWIND_STOP(btz->frametype)) {
    pop_btentry(btstack, c_ptr, capturing_ptr, boi_ptr, eoi_ptr);
    btz = btstack_top(btstack);
    assert(btz);
  }
  if (likely(btz->frametype == BTChoice)) {
    *c_ptr = btz->contents.choice.c;
    *pc_ptr = btz->contents.choice.pc;
    /* Discard captures from the part of the pattern that failed */
    capstack_settop(capstack, btz->contents.choice.capnext);
    btstack_pop(btstack);
    return OK;
  }
  else if (likely(btz->frametype == BTFindLoop)) {
    if (*c_ptr == *eoi_ptr) {
      btstack_pop(btstack);
      return FAIL;
    }
    *c_ptr = btz->contents.choice.c + 1; /* advance input position */
    *pc_ptr = btz->contents.choice.pc;
    /* Discard captures from the part of the pattern that failed */
    capstack_settop(capstack, btz->contents.choice.capnext);
    /* Do not pop the loop frame.  We reuse it. */
    return OK;
  }
  else if (btz->frametype == BTBackLoop) {
    if (btz->contents.backloop.c <= btz->contents.backloop.leftfence) {
      trace("vm",
           "lookbehind hit left fence (%lu): popping BT stack and failing",
           (btz->contents.backloop.leftfence - bof));
      btstack_pop(btstack);
      return FAIL;		/* goto fail */
    } else {
      *c_ptr = --(btz->contents.backloop.c);
      trace("vm",
           "lookbehind new search pos is %lu (looking at %c)",
           (*c_ptr - bof), **c_ptr);
    }
    *pc_ptr = btz->contents.backloop.pc;
    return OK;
  } else {
    warn("vm", "unexpected stack frame type '%s'", BTEntryName(btz->frametype));
    return PEXL_ERR_INTERNAL;
  }
}

#define JUMPBY(delta) pc = pc + (delta)
#define JUMPTO(dest) pc = (dest)

#define get_package(pt, i) \
  ((((i) >= 0) && ((i) < (pt)->next)) ? (pt)->packages[(i)] : NULL)

/* Check the top of the backtrack stack and return an error if the frame is not
   of the expected type */
#ifdef DEBUG
#define EXPECT_BTENTRY(expected_frametype)                     \
  do                                                           \
  {                                                            \
    if (btstack_top(stack) &&                                  \
        (btstack_top(stack)->frametype != expected_frametype)) \
    {                                                          \
      confess("vm",                                            \
              "expected " #expected_frametype                  \
              " stack frame; dumping stack:");                 \
      BTEntry_stack_print(stack,                               \
                          pkg->code, &giveup, c, bof);         \
      return PEXL_ERR_INTERNAL;				       \
    }                                                          \
  } while (0)
#else
#define EXPECT_BTENTRY(expected_frametype)
#endif

#define AT_EOI (c == eoi)
#define NOT_EOI (c < eoi)
#define CHARS_SEEN (c - boi)
#define ADVANCE (c++)
#define RETREAT(i) do {				\
    c -= (i);					\
  } while (0);

/* TODO: Examine the assertions in the vm, and find the ones that
   protect us against bad bytecode.  Throw an error in those cases.
*/

#define DISPATCH				       \
  do {						       \
    trace("vm", "In package %p: ", (const void *)pkg); \
    WHEN_TRACING fprint_inst(pkg, pc);		       \
    goto *vm_labels[(pc)->i.code & 0xFF];	       \
  } while (0)

#define OPCODE(name) LABEL_##name


// TODO: Use the fact that the address of the IFail opcode is the same
// as the 'fail' label, and change 'goto fail' into DISPATCH_IF(cond).
#define DISPATCH_IF(cond)					\
  do {								\
    WHEN_TRACING {						\
      trace("vm", "In package %p: ", (const void *)pkg);	\
      fprint_inst(pkg, pc);					\
    }								\
    goto *vm_labels[(((bool)(cond)) * (pc)->i.code) & 0xFF];	\
  } while (0)

#define DECODE0 do {				\
    assert(sizei(pc) == 1);			\
    pc++;					\
  } while (0);

#define DECODE1 do {				\
    assert(sizei(pc) == 1);			\
    A = aux(pc);				\
    pc++;					\
  } while (0);

#define DECODE2 do {				\
    assert(sizei(pc) == 2);			\
    X = getaddr(pc);				\
    A = aux(pc);				\
    pc += 2;					\
  } while (0);

/* 'stat's can be null; 'ptable' can be null; 'e' is exclusive */
static int
vm (const char  *os,                   /* IN: beginning of input byte array */
    const char  *c,                    /* IN: first input char to examine */
    const char  *oe,                   /* IN: one beyond last char to examine */
    const pexl_PackageTable *const pt, /* IN: package table */
    const char **r,	               /* OUT: first unmatched input byte  */
    BTStack     *stack,		       /* IN: initialized backtrack stack */
    CapStack    *captures,             /* IN/OUT: array where vm stores captures */
    pexl_Binary *trampoline_package,   /* IN: tiny code pkg that calls entrypoint in pt[0] */
    uint32_t     flags)		       /* IN: runtime configuration flags */
{
  VmState state;
  Charset *cs_ptr;
  Charset *simd_cs_ptr;
  BTEntry *bt;
  const Instruction *pc;
  bool capturing;
  int err;
  
  DECLARE_VM_LABELS;

  assert(IFail == 0);		/* This opcode must be zero */

  // Trampoline gets things started. Entrypoint is first instruction.
  const pexl_Binary *pkg = trampoline_package;
  pc = pkg->code;

  // FUTURE: See comment in vm_start() about a needed fix for multiple packages!

#if PEXL_USING_SIMD
  const bool use_simd =
    pexl_binary_has_simd(pt->packages[0]) &&   // This bytecode pkg has SIMD support,
    !HAS_BITFLAG(flags, PEXL_DISABLE_SIMD);    // ... and user has not disabled SIMD.

  const bool use_dynamic_simd =     
    pexl_binary_has_simd(pt->packages[0]) &&        // This bytecode pkg has SIMD support.
    !HAS_BITFLAG(flags, PEXL_DISABLE_DYNAMIC_SIMD); // User is not disabling dynamic SIMD.
#else
  UNUSED(flags);		// Suppress compiler warning
  const bool use_simd = false;
  const bool use_dynamic_simd = false;
#endif
    
  const char *const bof = os;	// Like "eof" but the beginning
  const char *const eof = oe;	// "End of file" i.e. real end of input
  const char *boi = os;		// Beginning of input
  const char *eoi = oe;		// End of input

  int32_t A = 0;		/* Aux register */
  int32_t X = 0;		/* Address/Offset register */

  capturing = YES;		/* Capturing starts off ACTIVE */

  // TODO: move JUMPBY(...) earlier when possible

  /* Start the vm */
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IFind): DECODE1;
  cs_ptr = charsettable_get_charset(pkg->cstab, A);
  if (unlikely(!cs_ptr)) {
    warn("vm", "invalid charset table index %d", aux(pc));
    return PEXL_ERR_INTERNAL;
  }
  if (use_simd || use_dynamic_simd) {
    simd_cs_ptr = charsettable_get_simd_charset(pkg->cstab, A);
    if (unlikely(!simd_cs_ptr)) {
      warn("vm", "invalid charset table index %d", A);
      return PEXL_ERR_INTERNAL;
    }
    if (!use_dynamic_simd) {
        c = find_simd(simd_cs_ptr, cs_ptr, c, eoi);
    } else {
        c = find_dynamic_simd(simd_cs_ptr, cs_ptr, c, eoi);
    }
  } else {
    c = find_charset(cs_ptr, c, eoi);
  }
  EXPECT_BTENTRY(BTFindLoop);
  btstack_top(stack)->contents.choice.c = c;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IFind8): DECODE1;
  cs_ptr = charsettable_get_charset(pkg->cstab, A);
  if (unlikely(!cs_ptr)) {
    warn("vm", "invalid charset table index %d", A);
    return PEXL_ERR_INTERNAL;
  }
  if (use_simd || use_dynamic_simd) {
    simd_cs_ptr = charsettable_get_simd_charset(pkg->cstab, A);
    if (unlikely(!simd_cs_ptr)) {
      warn("vm", "invalid charset table index %d", A);
      return PEXL_ERR_INTERNAL;
    }
    if (!use_dynamic_simd) {
        c = find_simd8(simd_cs_ptr, cs_ptr, c, eoi);
    } else {
        c = find_dynamic_simd8(simd_cs_ptr, cs_ptr, c, eoi);
    }
  } else {
    c = find_charset(cs_ptr, c, eoi);
  }
  EXPECT_BTENTRY(BTFindLoop);
  btstack_top(stack)->contents.choice.c = c;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IFind1): DECODE1;
  uint8_t auxchar = A & 0xFF;
  if (use_dynamic_simd) {
    c = find_dynamic_simd1(auxchar, c, eoi);
  } else if (use_simd) { 
    c = find_simd1(auxchar, c, eoi);
  } else {
    c = find_char(auxchar, c, eoi);
  }
  EXPECT_BTENTRY(BTFindLoop);
  btstack_top(stack)->contents.choice.c = c;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ITestSet): DECODE2;
  cs_ptr = charsettable_get_charset(pkg->cstab, A);
  if (unlikely(!cs_ptr)) return PEXL_ERR_INTERNAL;
  if (AT_EOI || !testchar(cs_ptr->cs, *c)) JUMPBY(X);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ISet): DECODE1;
  cs_ptr = charsettable_get_charset(pkg->cstab, A);
  if (unlikely(!cs_ptr)) return PEXL_ERR_INTERNAL;
  if (NOT_EOI && testchar(cs_ptr->cs, *c)) {
    ADVANCE;
    DISPATCH;
  }
  goto fail;

 // ------------------------------------------------------------------
 OPCODE(IAny): DECODE0;
  if NOT_EOI {
      ADVANCE;
      DISPATCH;
    }
  goto fail;

 // ------------------------------------------------------------------
 OPCODE(ITestAny): DECODE2;
  if AT_EOI JUMPBY(X);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IChar): DECODE1;
  if (NOT_EOI && ((uint8_t)*c == (A & 0xFF))) {
    ADVANCE;
    DISPATCH;
  }
  goto fail;

 // ------------------------------------------------------------------
 OPCODE(ITestChar): DECODE2;
  if (AT_EOI || ((uint8_t)*c != (A & 0xFF))) JUMPBY(X);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IRet): DECODE0;
  EXPECT_BTENTRY(BTCall);
  bt = btstack_top(stack);
  pc = bt->contents.call.pc;
  pkg = bt->contents.call.pkg;
  btstack_pop(stack);
  trace("vm",
       "IRet to pkg %p (#%d) entrypoint %d",
	(const void *)pkg, pkg->number, (int)(getoffset(pc) + pc - pkg->code));
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ICall): DECODE2;
  if (unlikely(btstack_push_call(stack, pc, pkg))) {
    err = PEXL_MATCH__ERR_BTLIMIT;
    goto done;
  }
  JUMPTO(pkg->code + X);	/* ABSOLUTE ADDRESS */
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ISpan): DECODE1;
  cs_ptr = charsettable_get_charset(pkg->cstab, A);
  if (unlikely(!cs_ptr)) {
    warn("vm", "invalid charset table index %d", A);
    return PEXL_ERR_INTERNAL;
  }
  c = span_charset(cs_ptr, c, eoi);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IJmp): DECODE2;
  JUMPBY(X);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IChoice): DECODE2;
  if (unlikely(btstack_push_choice(stack, pc + X, c, captures->next))) {
    err = PEXL_MATCH__ERR_BTLIMIT;
    goto done;
  }
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IFindLoop): DECODE2;
  if (unlikely(btstack_push_findloop(stack, pc + X, c, captures->next))) {
    err = PEXL_MATCH__ERR_BTLIMIT;
    goto done;
  }
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ICloseCapture): DECODE0;
  if (likely(capturing)) {
    assert(capstack_size(captures) > 0);
    if (unlikely(!capstack_push(captures, c, pkg->number, Cclose, 0))) {
      err = PEXL_MATCH__ERR_CAPLIMIT;
      goto done;
    }
  }
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IOpenCapture): DECODE2;
  if (likely(capturing)) {
    if (unlikely(!capstack_push(captures, c, pkg->number, A, X))) {
      err = PEXL_MATCH__ERR_CAPLIMIT;
      goto done;
    }
  }
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ICommit): DECODE2;
  assert(btstack_top(stack) && BTUNWIND_STOP(btstack_top(stack)->frametype));
  btstack_pop(stack);
  JUMPBY(X);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IPartialCommit): DECODE2;
  assert(btstack_top(stack));
  EXPECT_BTENTRY(BTChoice);
  /* Reuse BTChoice frame */
  bt = btstack_top(stack);
  if (!bt || bt->frametype != BTChoice) {
    err = PEXL_ERR_INTERNAL;
    goto done;
  }
  /*
     The behavior obtained by the conditional below allows us to
     accommodate repetitions of nullable patterns.
     FUTURE: Consider having two IPartialCommit instructions, in order
     to avoid the test of the flag stored in aux(pc).
  */
  if (A && (bt->contents.choice.c == c)) goto fail;
  bt->contents.choice.c = c;
  bt->contents.choice.capnext = captures->next;
  JUMPBY(X);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IEnd): DECODE0;
  *r = c;
  err = PEXL_OK;
  goto done;

 // ------------------------------------------------------------------
 OPCODE(IGiveup): 
  *r = NULL;
  err = PEXL_OK;
  goto done;

 // ------------------------------------------------------------------
 OPCODE(IBehind): DECODE1;
  if (A > CHARS_SEEN) goto fail;
  RETREAT(A);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ILookAhead): DECODE0;
  if (unlikely(btstack_push_lookahead(stack, boi, eoi, capturing))) {
    err = PEXL_MATCH__ERR_BTLIMIT;
    goto done;
  }
  boi = c;   /* Lookahead starts from cursor */
  eoi = eof; /* Allow looking ahead past EOI (but not past EOF) */
  capturing = NO;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ILookAheadCommit): DECODE0;
  EXPECT_BTENTRY(BTLookAhead);
  pop_btentry(stack, &c, &capturing, &boi, &eoi);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IXCall): DECODE2;
  trace("vm",
       "IXCall to package %d, entrypoint %d", A, X);
  if (unlikely(btstack_push_call(stack, pc, pkg))) {
    err = PEXL_MATCH__ERR_BTLIMIT;
    goto done;
  }
  pkg = get_package(pt, A);
  if (!pkg) {
    warn("vm", "get_package failed in IXCall");
      err = PEXL_ERR_INTERNAL;
      goto done;
  }
  JUMPTO(pkg->code + X);	/* ABSOLUTE ADDRESS */
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ILookBehindCommit): DECODE2;
  /* Current position in input should be equal to the right fence */
  if (c != eoi) goto fail;
  EXPECT_BTENTRY(BTBackLoop);
  pop_btentry(stack, &c, &capturing, &boi, &eoi);
  EXPECT_BTENTRY(BTLookBehind);
  pop_btentry(stack, &c, &capturing, &boi, &eoi);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IFailNot): DECODE0;
  /* Top of stack should be a CHOICE frame */
  EXPECT_BTENTRY(BTChoice);
  btstack_pop(stack); /* Discard choice frame */
  /* Next should be a LOOKAHEAD frame */
  EXPECT_BTENTRY(BTLookAhead);
  pop_btentry(stack, &c, &capturing, &boi, &eoi);
  /* fallthrough */

 // ------------------------------------------------------------------
 OPCODE(IFail): // No decode needed (no args, and no next instruction)
 fail:
  err = do_unwind(stack, captures, &c, &capturing, &pc, &boi, &eoi, bof);
  if (err == 0) DISPATCH;
  if (err == FAIL) goto fail;
  goto done;			/* error */

 // ------------------------------------------------------------------
 OPCODE(IBackref): DECODE2;
  WHEN_TRACING print_capstack(pt, os, captures);
  int len = find_backref(c, eoi, captures, X);
  if (len < 0) goto fail;
  c += len;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ICloseConstCapture): DECODE2;
  if (likely(capturing)) {
    assert(capstack_size(captures) > 0);
    if (unlikely(!capstack_push(captures, c, pkg->number, Ccloseconst, X))) {
      err = PEXL_MATCH__ERR_CAPLIMIT;
      goto done;
    }
  }
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(ILookBehind): DECODE2;
  /* See comment at declaration of VmState. */
  state = (VmState){c, boi, eoi};
  err = do_lookbehind(stack, &state, A, X, capturing, pc, bof);
  if (err == FAIL) goto fail;
  if (unlikely(err < 0)) goto done;
  c = state.c;
  boi = state.boi;
  eoi = state.eoi;
  capturing = NO;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(INarrowToSpan): DECODE2;
  /* See comment at declaration of VmState. */
  state = (VmState){c, boi, eoi};
  err = do_narrowtospan(stack, &state, A, X, capturing);
  if (unlikely(err == FAIL)) goto fail;
  if (unlikely(err < 0)) goto done;
  c = state.boi;
  boi = state.boi;
  eoi = state.eoi;
  capturing = NO;
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(IWiden): DECODE0;
  EXPECT_BTENTRY(BTNarrow);
  pop_btentry(stack, &c, &capturing, &boi, &eoi);
  DISPATCH;

 // ------------------------------------------------------------------
 OPCODE(INoop): DECODE0;
  DISPATCH;

 // ------------------------------------------------------------------
 BADOPCODE:
  warn("vm",
       "TRAP: illegal opcode (%d) at address %d",
       opcode(pc), (int)(pc - pkg->code));
  err = PEXL_MATCH__ERR_BADINST;

 done:
  return err;
}

/* -------------------------------------------------------------------------- */

/*
  Performance note: There is a performance penalty to using static
  data, and possibly a larger one when it is thread-local.  I doubt we
  will ever notice this regarding the trampoline, which is accessed
  only 3 times for each invocation of the vm.
*/

/* Not really a trampoline, more like a springboard (used once). */
#define trampoline_size 3
static pexl_Binary *new_trampoline(pexl_Index entrypoint)
{
  static __thread pexl_Binary *trampoline_package = NULL;
  static __thread Instruction code[trampoline_size];
  if (unlikely(!trampoline_package))
  {
    trampoline_package = malloc(sizeof(pexl_Binary));
    if (!trampoline_package) {
      warn("vm", "out of memory");
      return NULL;
    }
    trampoline_package->import_next = 0;
    trampoline_package->import_size = 0;
    trampoline_package->imports = NULL;
    trampoline_package->number = -1;
    trampoline_package->symtab = NULL;
    trampoline_package->cstab = NULL;
    /* Put instructions into the code vector */
    setopcode(&code[0], IXCall);
    setaux(&code[0], 0);	/* PackageTable entry 0 is the user package */
    setaddr(&code[0], 0);	/* Placeholder entrypoint */
    setopcode(&code[2], IEnd);
    trampoline_package->code = code;
    trampoline_package->codenext = trampoline_size;
    trampoline_package->codesize = trampoline_size;
  }
  /* Set the actual entrypoint */
  setaddr(&(trampoline_package->code[0]), entrypoint);
  return trampoline_package;
}

/*
   pexl_Positions are 0-based.  Endpos is exclusive, i.e. the vm will look
   at characters in [s, e).
*/
// TODO: Add const and restrict keywords where appropriate
int vm_start (pexl_Binary *pkg,
	      pexl_PackageTable *importtable,
	      pexl_Index entrypoint,
	      const char *input, size_t inputlen,
	      size_t startpos, size_t endpos,
	      uint32_t vm_flags,
	      /* in/out */
	      pexl_Stats *stats,
	      pexl_Match *match)
{
  int retval;
  int t0 = 0;
  const char *r, *s, *e;
  pexl_Binary *trampoline;
  CapStack *captures;
  BTStack *stack;
  pexl_PackageTable *pt;
  pexl_Binary *user[1] = {pkg};
  pexl_PackageTable no_imports_pt =
    {.packages=(pexl_Binary **)&user, .capacity=1, .next=1};
  
  assert(PEXL_NUM_OPCODES == 64);

  if (unlikely(!pkg || !match)) {
    warn("vm_start", "null argument");
    return PEXL_MATCH__ERR_NULL;
  }
  if (unlikely(!input)) {
    warn("vm_start", "null input argument");
    return PEXL_MATCH__ERR_NULL;
  }
  if (unlikely(startpos > inputlen)) {
    return PEXL_MATCH__ERR_STARTPOS;
  }
  if (unlikely((endpos > inputlen) || (startpos > endpos))) {
    return PEXL_MATCH__ERR_ENDPOS;
  }
  // On a system where a size_t can hold a number larger than our
  // pexl_Position type can, it is possible for startpos or endpos to be
  // too large to be stored as pexl_Positions.  
  if (SIZE_MAX > pexl_Position_MAX) {
    if (unlikely
	((startpos >= (size_t)INT64_MAX) || (endpos >= (size_t)INT64_MAX))) {
      return PEXL_MATCH__ERR_INPUT_LEN;
    }
  }

  /*
    The "user package" in slot 0 of the package table determines
    whether data needed for simd operations are in the compiled code
    for all packages in the package table.

    The user can disable SIMD when invoking the VM by setting a bit in
    vm_flags.  SIMD is used when this bit is not set AND the user
    package supports it.

    TODO: In vm_start(), determine that all packages in the table
    support SIMD, provided the user package does AND the user wants
    SIMD for this invocation of the vm.  ALTERNATIVELY, we could set
    the use_simd flag for each XCALL (i.e. on each change to current
    package).
  */


  /* ------------------------------------------------------------------ */
  /* Backtrack stack */
  /* ------------------------------------------------------------------ */

  // TEMP: Hardcoded to NOT track high water mark
  stack = btstack_new(NO);
  if (unlikely(!stack))
    return PEXL_ERR_OOM;
  /*
     Bottom of backtrack stack is IGiveup, so matching fails if we pop
     the last entry.
  */
  /* Note: IGiveup stack frame poses as a CHOICE */
  if (unlikely(btstack_push_choice(stack, &giveup, NULL, 0))) {
    return PEXL_MATCH__ERR_BTLIMIT;
  }

  assert(btstack_size(stack) == 1);

  if (!importtable) {
    pt = &no_imports_pt;
  } else {
    pt = importtable;
    pt->packages[0] = pkg;
  }

  captures = capstack_new(0); /* TEMP: Hardcoded to not track high water mark */
  if (unlikely(!captures)) {
    retval = PEXL_ERR_OOM;
    goto err1;
  }

  assert(capstack_size(captures) == 0);

  /* The entrypoint is an instruction in pkg */
  if ((unlikely(entrypoint < 0)) ||
      (unlikely(((size_t)entrypoint) >= pkg->codenext))) {
    warn("vm", "invalid entrypoint %d (instruction vector size = %u)",
           entrypoint, pkg->codenext);
    retval = PEXL_MATCH__ERR_ENTRYPOINT;
    goto err2;
  }

  pexlEncoder encoder_fn = get_encoder_fn(match->encoder_id);

  trampoline = new_trampoline(entrypoint);
  if (unlikely(!trampoline)) {
    retval = PEXL_ERR_OOM;
    goto err2;
  }

  s = input + startpos;	       /* start char for matching */
  e = input + endpos;	       /* one beyond last char for matching */
  if (stats) t0 = clock();
  retval = vm(input, s, e, pt, &r, stack, captures, trampoline, vm_flags);
  if (retval != PEXL_OK) goto err2;
  if (stats) stats->match_time = clock() - t0;

  if (r == NULL) {
    assert(btstack_size(stack) == 0);
    match->end = -1;
    goto done;
  }
  /*
     There was a match, so we need to encode the match output, i.e. to
     produce the data structure requested by the caller.
  */
  assert(btstack_size(stack) == 1); /* XCall frame, not popped by IEnd */
  match->end = r - input;
  assert(match->end >= 0);
  if (encoder_fn && capstack_size(captures)) {
    retval = encoder_fn(pt,
			input, inputlen,
			captures->base, capstack_size(captures),
			match);
  } else {
    assert(retval == PEXL_OK);
  }

done:
  if (stats) stats->total_time = clock() - t0;
err2:
  capstack_free(captures);
err1:
  btstack_free(stack);
  pt->packages[0] = NULL;
  return retval;
}

pexlEncoderID match_encoder (pexl_Match *m) {
  if (m)
    return m->encoder_id;
  /* Have to return something... */
  return PEXL_NO_ENCODER;
}

void print_vm_sizes(void) {
  printf("vm structure sizes, in bytes:\n");
  printf("  BTEntry: %zu\n", sizeof(BTEntry));
  printf("  pexlCapture: %zu\n", sizeof(pexlCapture));
  printf("number of opcodes: %d\n", PEXL_NUM_OPCODES);
}
