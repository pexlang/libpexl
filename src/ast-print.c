/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast-print.c  Print the Abstract Syntax Tree                              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, James Deucher                                */

#include "ast-print.h"
#include "sexp-writer.h"

static void print_string(String *s) {
  string *printable = String_escape(s);
  printf("%s", printable ?: "STRING ESCAPE FAILED");
  string_free(printable);
}

// Helper to print codepoint, used to print expressions.
static void print_codepoint (pexl_Codepoint cp) {
  string *tmp = write_codepoint(cp);
  printf("%s", tmp);
  free(tmp);
}

/* 
 * Pretty print a single expression.  This function is used by
 * ast_print_tree but also can be used independantly.
 */
static void ast_print_expression (ast_expression *exp) {
  ast_type type = exp->type;
  printf("%s ", ast_type_name(type));
  switch (type) {
    case AST_ANYBYTE:
    case AST_ANYCHAR:
    case AST_TRUE:
    case AST_FALSE:
    case AST_MODULE:
      puts(""); break;
    case AST_BYTE:
      printf("%d\n", exp->codepoint); break;
    case AST_BYTESET:
      printf("["); 
      int i, first;
      for (i = 0; i <= UCHAR_MAX; i++) {
	first = i;
	while (i <= UCHAR_MAX && byteset_member(exp->byteset, i)) i++;
	if ((i - 1) == first)		/* single byte */
	  printf(" %d", first);
	else if ((i - 1) > first)	/* contiguous range */
	  printf(" [%d, %d]", first, i-1);
      }
      printf("]\n"); 
      break;
    case AST_BYTERANGE:
      printf("[Min: %d, Max: %d]\n",
	     exp->low,
	     exp->high);
      break;
    case AST_BYTESTRING: {
      string *tmp = String_escape(exp->bytes);
      printf("[%s]\n", tmp);
      free(tmp);
      break;
    }
    case AST_FIND:
    case AST_BACKREF:
      puts("");
      break;
    case AST_CHAR:
      printf("[");
      print_codepoint(exp->codepoint);
      printf("]\n"); 
      break;
    case AST_SET:
      printf("["); 
      for (uint32_t k = 0; k < exp->codepointset->size; k++) {
	pexl_Codepoint val = *(exp->codepointset->codepoints + k);
	print_codepoint(val);
	printf(" ");
      }
      printf("]\n"); 
      break;
    case AST_RANGE:
      printf("[Min: ");
      print_codepoint(exp->low);
      printf(", Max: ");
      print_codepoint(exp->high);
      printf("]\n"); 
      break;
    case AST_EXPLIST:
      puts(""); break;
    case AST_SEQ:
      puts(""); break;
    case AST_CHOICE:
      puts(""); break;
    case AST_REPETITION: {
      printf("[Min: %" pexl_Index_FMT ", Max: %" pexl_Index_FMT "]\n",
	     exp->min, exp->max); 
    }
      break;
    case AST_LOOKAHEAD:
      printf("[LookAhead]\n");
      break;
    case AST_NEGLOOKAHEAD: 
      printf("[NegLookAhead]\n");
      break;
    case AST_LOOKBEHIND:
      printf("[LookBehind]\n");
      break;      
    case AST_NEGLOOKBEHIND:
      printf("[NegLookBehind]\n");
      break;
    case AST_NUMBER:
      printf("[%" PRId64 "]\n", exp->n); 
      break;
    case AST_IDENTIFIER:
      print_string(exp->name);
      printf(" (idx %d)\n", exp->idx);
      break;
    case AST_DEFINITION:
      print_string(exp->name);
      puts("");
      break;
    case AST_CAPTURE:
      printf("[");
      print_string(exp->name);
      puts("]");
      break;
    case AST_INSERT:
      printf("'%.*s'\n", (int) String_len(exp->name), String_ptr(exp->name));
      break;
    default:
      confess("ast_print_expression", "Unknown expression type %s (%d)",
	      ast_type_name(type), type);
  }
}

// This code was based off the other pretty printer in ast print (I think). It might be a good idea to make the MAX_TREE_DEPTH
// the same variable for both programs.
#define MAX_TREE_DEPTH 1024

// Deals with creating proper indends and parent's │ lines. Component in the ast_print_tree.
// depth is the measure of nodes into the tree. The parents pointer is an array that tracks at what depth there is a parent, requiring a line.
static void tree_indent (int depth, uint8_t *parents) {
  if (depth > MAX_TREE_DEPTH) {
    depth = MAX_TREE_DEPTH;
    printf("MAX TREE DEPTH EXCEEDED ");
  }
  for (int i=1; (i < depth); i++) {
    printf("%s", (parents[i] ? "│   " : "    "));
  }
}

// This is a recursive function to do most of the heavy lifting when
// printing a pretty version of the ast_expression trees.
static void do_print_ast (ast_expression *exp,
			  int depth,
			  uint8_t *parents,
			  bool hasSib) {
  // Indent appropriately
  tree_indent(depth, parents);

  // If current expression has siblings, print the tee, else the ell
  if (depth > 0) printf("%s", (hasSib ? "├── " : "└── "));
  
  // Print the current expression
  ast_print_expression(exp);
  
  // Record whether or not at this depth there are more siblings
  if (depth < MAX_TREE_DEPTH) parents[depth] = hasSib;

  // Print each child.  Modules have a list of definitions, and other
  // expressions have either one or two children
  if (ast_modulep(exp)) {
    ast_explist *defns;
    for (defns = exp->defns; defns; defns = defns->tail) 
      // The current defn has a sibling if defns->tail is not NULL
      do_print_ast(defns->head, depth+1, parents, defns->tail);
  } else {
    ast_expression *child = NULL;
    while ((child = ast_child_iter(exp, child))) {
      // The current child has a sibling if child->next is not NULL
      do_print_ast(child, depth+1, parents, ast_child_iter(exp, child));
    }
  }
}

// Pretty prints ast in the console. Simply pass the root node of the ast.
void ast_print_tree (ast_expression *exp) {
  // Special case for empty expression
  if (!exp) {
    printf("NULL AST tree\n");
    return;
  }
  uint8_t *parents = malloc(MAX_TREE_DEPTH);
  if (!parents) {
    warn("print", "out of memory");
    return;
  }
  memset(parents, 0, MAX_TREE_DEPTH);
  do_print_ast(exp, 0, parents, false);
  free(parents);
}
