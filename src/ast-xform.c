//  -*- Mode: C; -*-                                                       
// 
//  ast-xform.c
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jack Deucher, Jamie A. Jennings

#include "preamble.h"
#include "ast-xform.h"

#if PEXL_DEBUG
#include "ast-print.h"
#endif

// Codepoints are represented in UTF-32 and stored in a uint32_t.  A
// single codepoint represented in UTF 8 can be from 1-4 bytes long.
// We always return 4 bytes in a uint32_t.
static uint32_t utf32_to_utf8 (pexl_Codepoint cp) {
  union {
    uint8_t out[4];
    uint32_t val;
  } x;
  x.val = 0;
  if (cp <= 0x007F) {
    x.out[0] = cp & 0x007F;
  }
  else if (cp <= 0x07FF) {
    x.out[0] = 0xC0 | ((cp & 0x07C0) >> 6);
    x.out[1] = 0x80 | (cp & 0x003F);
  }
  else if (cp <= 0xFFFF) {
    x.out[0] = 0xE0 | ((cp & 0xF000) >> 12);
    x.out[1] = 0x80 | ((cp & 0x0FC0) >> 6);
    x.out[2] = 0x80 | (cp & 0x003F);
  }
  else if (cp <= 0x10FFFF) {
    x.out[0] = 0xF0 | ((cp & 0x70000) >> 18);
    x.out[1] = 0x80 | ((cp & 0x3F000) >> 12);
    x.out[2] = 0x80 | ((cp & 0x0FC0) >> 6);
    x.out[3] = 0x80 | (cp & 0x003F);
  }
  else {
    warn("utf32_to_utf8", "Codepoint out of range: %u", cp);
  }
  return x.val;
}

static ast_expression *utf8_exp_from_codepoint_value(pexl_Codepoint value) {
  union {
    uint8_t out[4];
    uint32_t val;
  } x;
  x.val = utf32_to_utf8(value);
  trace("astxform", "Codepoint %u in UTF-8 is %d %d %d %d (ignore trailing zeros)",
        value, x.out[0], x.out[1], x.out[2], x.out[3]);
  int len = 1;
  while ((len < 4) && x.out[len]) len++;
  switch (len) {
    case 1:
      return make_byte(x.out[0]);
    case 2:
      return make_seq(make_byte(x.out[0]), make_byte(x.out[1]));
    case 3:
      return make_seq(make_byte(x.out[0]),
		      make_seq(make_byte(x.out[1]),
			       make_byte(x.out[2])));
    default:
      return make_seq(make_byte(x.out[0]),
		      make_seq(make_byte(x.out[1]),
			       make_seq(make_byte(x.out[2]),
					make_byte(x.out[3]))));
  }
}

// Convert a single codepoint to UTF8 (sequence of bytes)
static ast_expression *convert_cp_UTF8 (ast_expression *cp) {
  return utf8_exp_from_codepoint_value(cp->codepoint);
}

// Convert a range of codepoints into a choice of UTF8 byte sequences
static ast_expression *convert_cpr_UTF8 (ast_expression *cp) {
  assert(cp->type == AST_RANGE);
  pexl_Codepoint i;
  if (codepointrange_emptyp(cp))
    return make_true();
  ast_expression *out = make_char(cp->low);
  for (i = cp->low + 1; i <= cp->high; i++)
    out = make_choice(make_char(i), out);
  return out;
}

// Convert set of codepoints into a choice of UTF8 byte sequences
static ast_expression *convert_cps_UTF8 (ast_expression *cp) {
  if (codepointset_emptyp(cp->codepointset))
    return make_true();
  pexl_Codepoint *codepoints = cp->codepointset->codepoints;
  ast_expression *out = utf8_exp_from_codepoint_value(codepoints[0]);
  for (uint32_t i = 1; i < cp->codepointset->size; i++) 
    out = make_choice(out, utf8_exp_from_codepoint_value(codepoints[i]));
  return out;
}

// Converts a codepoint into an equivalent UTF8 byte-based expression.
// Ignores AST types that do not contain codepoints.
static ast_expression *to_UTF8 (ast_expression *exp, void *context) {
  UNUSED(context);
  switch (exp->type) {
  case AST_CHAR:
    return convert_cp_UTF8(exp);
  case AST_SET:
    return convert_cps_UTF8(exp);
  case AST_RANGE:
    return convert_cpr_UTF8(exp);
  default:
    warn("convert_UTF8", "unexpected node type %s", ast_type_name(exp->type));
    return NULL;
  }
}

static bool has_codepoint (ast_expression *exp, void *context) {
  UNUSED(context);
  switch (exp->type) {
  case AST_CHAR:
  case AST_SET:
  case AST_RANGE:
    return true;
  default:
    return false;
  }
}

transformerReturn donotmuch(ast_expression * exp, void * context) {
  UNUSED(context);
#if PEXL_DEBUG
  ast_print_tree(exp);
  printf("\n");
#else
  printf("Visited node %p of type (%d) %s\n",
	 exp, exp->type, ast_type_name(exp->type));
#endif
  return TRet(NULL, AST_XFORM_NOREPLACE);
}

transformerReturn replaceFind(ast_expression *exp, void *context) {
  ast_expression *name = (ast_expression *)context;
  ast_type type = exp->type;

  if (type == AST_CHOICE) {

    ast_expression *choice1 = exp->child;
    ast_expression *choice2 = exp->child2;
    assert(choice1 && choice2);

    // Choice2 must be a sequence
    if (choice2->type == AST_SEQ) {
      ast_expression *seq1 = choice2->child;
      ast_expression *seq2 = choice2->child2;

      if ((seq1->type == AST_ANYBYTE)
	  && (seq2->type == AST_IDENTIFIER)) {
	String *call_target = seq2->name;

	if (String_cmp(call_target, name->name) == 0) {

	  ast_expression *copy;
	  transformerReturn ret = copyAST_function(choice1, &copy);

	  if (ret.code == AST_XFORM_ERROR) return TRet(exp, AST_XFORM_ERROR);
	  return TRet(make_find(copy), AST_XFORM_REPLACE);
	}
      }
    }
  }
  return TRet(NULL, AST_XFORM_STOP);
}

// Zero or more expressions
static ast_explist *copy_explist(ast_explist *ls) {
  ast_explist *new = NULL;
  for (; ls; ls = ls->tail)
    new = explist_cons(copyAST(ls->head), new);
  return explist_nreverse(new);
}

transformerReturn copyAST_function(ast_expression *exp, void *context) {
  ast_expression *new;
  ast_expression **output = (ast_expression **)context;
  assert(context);
  switch (exp->type) {
    case AST_CHAR:
      *output = make_char(exp->codepoint);
      break;
    case AST_RANGE:
      *output = make_range(exp->low, exp->high);
      break;
    case AST_BYTE:
      *output = make_byte((pexl_Byte) (exp->codepoint & 0xFF));
      break;
    case AST_BYTESTRING:
      *output = make_bytestring(String_dup(exp->bytes));
      break;
    case AST_ANYBYTE:
      *output = make_anybyte();
      break;
    case AST_NUMBER:
      *output = make_number(exp->n);
      break;
    case AST_SEQ:
      *output = make_seq(copyAST(exp->child), copyAST(exp->child2));
      break;
    case AST_CHOICE:
      *output = make_choice(copyAST(exp->child), copyAST(exp->child2));
      break;
    case AST_REPETITION:
    case AST_CAPTURE:
    case AST_FIND:
      new = copyAST(exp->child);
      if (!new) return TRet(exp->child, AST_XFORM_ERROR);
      switch (exp->type) {
	case AST_REPETITION:
	  *output = make_repetition(exp->min, exp->max, new);
	  break;
	case AST_CAPTURE:
	  *output = make_capture(String_dup(exp->name), new);
	  break;
	case AST_INSERT:
	  *output = make_insert_internal(String_dup(exp->name), new);
	  break;
	case AST_FIND:
	  *output = make_find(new);
	  break;
	default:
	  confess("ast-xform", "Missing case for %s in inner switch", ast_type_name(exp->type));
	  return TRet(NULL, AST_XFORM_ERROR);
      }
      break;
    case AST_DEFINITION:
      *output = make_definition(String_dup(exp->name), copyAST(exp->child));
      break;
    case AST_MODULE:
      *output = make_module(copy_explist(exp->defns));
      break;
    default:
      warn("analyze", "Unhandled expression type: %s (%d)",
	   ast_type_name(exp->type), exp->type);
      return TRet(NULL, AST_XFORM_ERROR);
  }
  return TRet(NULL, AST_XFORM_STOP);
}

ast_expression *copyAST (ast_expression *exp) {
  if (!exp) {
    warn("analyze", "null expression arg");
    return NULL;
  }
  ast_expression *copy;
  transformerReturn result = copyAST_function(exp, &copy);
  if (result.code == AST_XFORM_ERROR) {
    warn("analyze", "copy failed");
    if (result.exp) free_expression(result.exp);
    return NULL;
  }
  return copy;
}

// Returns NULL if the two ASTs are the same, or an expression within
// exp1 that differs from its counterpart in exp2.

// We could turn this into a transformer if we used context to
// (carefully) keep track of where we are in exp2.
ast_expression *compareAST(ast_expression *exp1, ast_expression *exp2) {
  assert(exp1 && exp2);

// #if PEXL_DEBUG
//    confess("TEMP", "---------- Args to compareAST:");
//    ast_print_tree(exp1); ast_print_tree(exp2);
//    confess("TEMP", "----------");
// #endif

  ast_expression *diff;
  ast_expression *same = NULL;

 tailcall:

  diff = exp1;
  if (exp1 == exp2) return same;
  if (exp1->type != exp2->type) return diff;
  switch (exp1->type) {
    case AST_CHAR:
    case AST_BYTE:
      return (exp1->codepoint == exp2->codepoint) ? same : diff;
    case AST_BYTERANGE:
    case AST_BYTESET:
    case AST_RANGE:
    case AST_SET:
      confess("compareAST", "AST type %s not implemented", ast_type_name(exp1->type));
      return diff;
    case AST_SEQ:
    case AST_CHOICE:
      diff = compareAST(exp1->child, exp2->child);
      if (diff != NULL) return diff;
      exp1 = exp1->child;
      exp2 = exp2->child;
      goto tailcall;
    case AST_REPETITION:
      if ((exp1->min != exp2->min)
	  && (exp1->max != exp2->max))
	return diff;
      exp1 = exp1->child;
      exp2 = exp2->child;
      goto tailcall;
    case AST_FIND:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD: 
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
      exp1 = exp1->child;
      exp2 = exp2->child;
      goto tailcall;
    case AST_NUMBER:
      return (exp1->n == exp2->n) ? same : diff;
    default:
      warn("analyze", "Unhandled expression type: %s (%d)",
	   ast_type_name(exp1->type), exp1->type);
      return diff;
  }
  return same;
}

ast_expression *transform_AST (ast_expression *exp,
                               transformerfn function,
                               void *context) {
  if (!exp) return NULL;

  transformerReturn processed = function(exp, context);
  ast_xform_code code = processed.code;

  if (code == AST_XFORM_ERROR) {
    warn("transform_AST", "error reported by transformer");
    return NULL;
  }

  if (code == AST_XFORM_STOP) return exp;

  if ((code == AST_XFORM_REPLACE) || (code == AST_XFORM_REPLACE_STOP)) {
    // Replace the current expression with the result of the transformer
    ast_expression *replacement = processed.exp;
    if (replacement == NULL) {
      warn("astxform", "replacement exp is NULL");
      return NULL;
    }
    free_expression(exp);
    exp = replacement;
  }

  if (code == AST_XFORM_REPLACE_STOP) return exp;
    
  if ((code != AST_XFORM_REPLACE) && (code != AST_XFORM_NOREPLACE)) {
    warn("ast", "Invalid result code %d", code);
    return NULL;
  }

  // Continue walking the AST, calling the transformer on each child
  ast_expression *child = NULL;
  ast_expression **childptr;
  while ((child = ast_child_iter(exp, child))) {
    childptr = &(exp->child);
    *childptr = transform_AST(*childptr, function, context);
  }

  return exp;
}

int map_AST (ast_expression *exp,
             transformerfn function,
             ast_expression **result,
             void *context) {
  if (!exp) {
    warn("map_AST", "NULL exp argument");
    return PEXL_ERR_INTERNAL;
  }
  transformerReturn processed = function(exp, context);
  ast_xform_code code = processed.code;
  if (result) *result = processed.exp;

  if (code == AST_XFORM_ERROR) {
    warn("map_AST", "error reported by transformer function");
    return PEXL_ERR_INTERNAL;
  }

  if (code == AST_XFORM_STOP) return false; // No result

  if (code == AST_XFORM_REPLACE) {
    // Replace the current expression with the result of the transformer
    if (result == NULL) {
      warn("map_AST", "Return argument pointer is NULL");
      return PEXL_ERR_INTERNAL;
    }
    // Convenience feature: If transformer returns its arg, we make a
    // copy so the transformer does not have to
    if (processed.exp == exp) 
      copyAST_function(exp, result);
    else
      *result = processed.exp;
    return true;                  // Have result
  }
  warn("map_AST", "Invalid return code reported by transformer function");
  return PEXL_ERR_INTERNAL;
}

// Wednesday, June 19, 2024
//
// Trying out a different API for tree transformations that copy (do
// not modify) the input tree

ast_expression *transform(ast_expression *exp,
			  bool matcher(ast_expression *, void *),
			  ast_expression *transformer(ast_expression *, void *),
			  void *context) {
  if (!exp || !matcher || !transformer) {
    warn("transform", "Required argument is NULL");
    return NULL;
  }
  
  ast_explist *ls;
  ast_expression *new;
  ast_explist *result = NULL;

  // Transform if necessary
  if (matcher(exp, context))
    return transformer(exp, context);

  // Else copy the current node, transforming its children
  switch (exp->type) {
    case AST_CHAR:
      return make_char(exp->codepoint);
    case AST_RANGE:
      return make_range(exp->low, exp->high);
    case AST_BYTE:
      return make_byte(exp->codepoint & 0xFF);
    case AST_BYTESTRING:
      return make_bytestring(String_dup(exp->bytes));
    case AST_ANYBYTE:
      return make_anybyte();
    case AST_NUMBER:
      return make_number(exp->n);
    case AST_SEQ:
      return make_seq(transform(exp->child, matcher, transformer, context),
		      transform(exp->child2, matcher, transformer, context));
    case AST_CHOICE:
      return make_choice(transform(exp->child, matcher, transformer, context),
			 transform(exp->child2, matcher, transformer, context));
    case AST_REPETITION:
      return make_repetition(exp->min, exp->max,
			     transform(exp->child, matcher, transformer, context));
    case AST_CAPTURE:
      return make_capture(String_dup(exp->name),
			  transform(exp->child, matcher, transformer, context));
    case AST_INSERT:
      return make_insert_internal(String_dup(exp->name),
				  transform(exp->child, matcher, transformer, context));
    case AST_FIND:
      return make_find(transform(exp->child, matcher, transformer, context));

    case AST_DEFINITION:
      return make_definition(String_dup(exp->name),
			     transform(exp->child, matcher, transformer, context));
    case AST_MODULE:
      for (ls = exp->defns; ls; ls = ls->tail) {
	new = transform(ls->head, matcher, transformer, context);
	if (!new) {
	  free_explist(result);
	  return NULL;
	}
	result = explist_cons(new, result);
      }
      return make_module(explist_nreverse(result));

    default:
      warn("transform", "Unhandled expression type: %s (%d)",
	   ast_type_name(exp->type), exp->type);
      return NULL;
  }
}

ast_expression *transform_codepoints(ast_expression *exp) {
  return transform(exp, has_codepoint, to_UTF8, NULL);
}
