/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  charset.c   Character set manipulation                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#include "charset.h"

void cs_complement (Charset *cs) {
  charset_not(cs->cs);
}

bool cs_equal (Charset *cs1, Charset *cs2) {
  return (memcmp(cs1->cs, cs2->cs, 32) == 0);
}

int cs_disjoint (Charset *cs1, Charset *cs2) {
  for (int i = 0; i < 8; i++)
    if ((cs1->cs[i] & cs2->cs[i]) != 0) return 0;
  return 1;
}

/*
  Check whether a charset is empty, singleton, full, etc.  When the
  set has only one element, 'c1' returns that character.  When the set
  has between 2 and 255 elements, 'c1' returns some character that is
  not in the set (needed for SIMD routines).
*/
CharSetType charsettype (Charset *cs, char *c1) {
  int count = 0;
  uint8_t save_char = '\0';
  for (int i = 0; i < 256; i++) {
    uint8_t c = (uint8_t) i & 0xFF;
    if (testchar(cs->cs, c)) {
      count++;
      if (count == 1) save_char = c; /* c \in cs */
    }
    if (count > 1) save_char = c; /* c \notin cs */
  }
  if (count == 0) return CharSetEmpty;
  if (count == 256) return CharSetFull;
  if (count == 1) {
    *c1 = (char) save_char;	/* the one char in cs */
    return CharSet1;
  }
  *c1 = (char) save_char;	/* some char not in cs */
  if (count <= 8) return CharSet8;
  assert(count <= 255);
  return CharSet;
}

/* ----------------------------------------------------------------------------- */
/* CharsetTable                                                                  */
/* ----------------------------------------------------------------------------- */

CharsetTable *charsettable_new (size_t initial_size) {
  CharsetTable *cst;
  assert(CHARSETTABLE_INITSIZE > 0);
  assert(CHARSETTABLE_MAXSIZE > 0);
  if (initial_size == 0) initial_size = CHARSETTABLE_INITSIZE;
  if (initial_size > CHARSETTABLE_MAXSIZE) initial_size = CHARSETTABLE_MAXSIZE;
  cst = malloc(sizeof(CharsetTable));
  if (!cst) {
    warn("charsettable", "out of memory");
    return NULL;
  }
  cst->charsets = malloc(initial_size * sizeof(Charset));
  if (!cst->charsets) {
    free(cst);
    warn("charsettable", "out of memory");
    return NULL;
  }
  cst->simd_charsets = malloc(initial_size * sizeof(Charset));
  if (!cst->simd_charsets) {
    free(cst->charsets);
    free(cst);
    warn("charsettable", "out of memory");
    return NULL;
  }
  cst->capacity = initial_size;
  cst->next = 0;
  return cst;			/* OK */
}

void charsettable_free (CharsetTable *cst) {
  if (!cst) return;
  if (cst->charsets) free(cst->charsets);
  if (cst->simd_charsets) free(cst->simd_charsets);
  free(cst);
  return;
}

static int ensure_space (CharsetTable *cst) {
  size_t newsize;
  void *temp;
  if (cst->next < cst->capacity) return 0; /* OK */
  if (cst->capacity >= CHARSETTABLE_MAXSIZE) {
    warn("charsettable", "charset table full");
    return CHARSETTABLE_ERR_FULL;
  }
  newsize = 2 * cst->capacity;
  if (newsize > CHARSETTABLE_MAXSIZE) newsize = CHARSETTABLE_MAXSIZE;

  /* Must expand charsets and simd_charsets together */
  temp = realloc(cst->charsets, newsize * sizeof(Charset));
  if (!temp) {
    warn("charsettable", "out of memory");
    return CHARSETTABLE_ERR_OOM;
  }
  cst->charsets = temp;
  /* Only expand simd_charsets if this table contains SIMD content */
  if (charsettable_has_simd(cst)) {
    temp = realloc(cst->simd_charsets, newsize * sizeof(Charset));
    if (!temp) {
      warn("charsettable", "out of memory");
      return CHARSETTABLE_ERR_OOM;
    }
    cst->simd_charsets = temp;
  }

  cst->capacity = newsize;
  inform("charsettable ensure_space",
	 "extending charset table to %ld slots (%ld in use)",
	 cst->capacity, cst->next);
  return OK;
}

// Call this to indicate that no SIMD charsets were generated
void charsettable_set_no_simd (CharsetTable *cst) {
  assert(cst);
  if (cst->simd_charsets) free(cst->simd_charsets);
  cst->simd_charsets = NULL;
}
  
int charsettable_has_simd (CharsetTable *cst) {
  assert(cst);
  return (cst->simd_charsets != NULL);
}

pexl_Index charsettable_add (CharsetTable *cst) {
  pexl_Index index;
  int err;
  if (!cst) {
    warn("charsettable", "null charset table arg");
    return CHARSETTABLE_ERR_NULL;
  }
  if ((err = ensure_space(cst))) {
    assert( err < 0 );
    return err;
  }
  index = cst->next++;
  return index;
}

/*
  Pointer returned is only valid when table is "frozen", because
  adding a new entry could cause a realloc.  Should assert at runtime
  that table is "frozen" (can no longer be modified).  We cannot check
  that here because get_charset() is used during code generation,
  before freezing the table.
*/
Charset *charsettable_get_charset (CharsetTable *cst, pexl_Index n) {
  if (!cst) {
    warn("charsettable", "null charset table arg");
    return NULL;
  }
  if ((n < 0) || (n >= cst->next)) {
    warn("charsettable",
	 "index %d out of range (%d entries in use)",
	 n, cst->next);
    return NULL;
  }
  return &(cst->charsets[n]);
}

// Call with n = PEXL_ITER_START to begin iterating
Charset *charsettable_iter(CharsetTable *cst, pexl_Index *n) {
  (*n)++;
  // If index n is too high, return NULL with no warnings 
  if (*n >= cst->next) return NULL; 
  return charsettable_get_charset(cst, *n);
}

/* Ideally, we would check at runtime that table is frozen before calling */
Charset *charsettable_get_simd_charset (CharsetTable *cst, pexl_Index n) {
  if (!cst) {
    warn("charsettable", "null charset table arg");
    return NULL;
  }
  if ((n < 0) || (n >= cst->next)) {
    warn("charsettable",
	 "index %d out of range (%d entries in use)",
	 n, cst->next);
    return NULL;
  }
  return &(cst->simd_charsets[n]);
}

int charsettable_set_charset (CharsetTable *cst, pexl_Index n, Charset *cs) {
  if (!cst) {
    warn("charsettable", "null charset table arg");
    return CHARSETTABLE_ERR_NULL;
  }
  if ((n < 0) || (n >= cst->next)) {
    warn("charsettable",
	 "index %d out of range (%d entries in use)",
	 n, cst->next);
    return CHARSETTABLE_ERR_INDEX;
  }
  charset_copy(&(cst->charsets[n]), cs->cs);
  return OK;
}

int charsettable_set_simd_charset (CharsetTable *cst, pexl_Index n, Charset *cs) {
  if (!cst) {
    warn("charsettable", "null charset table arg");
    return CHARSETTABLE_ERR_NULL;
  }
  if (!cst->simd_charsets) {
    warn("charsettable", "this charset table has no simd content");
    return CHARSETTABLE_ERR_FROZEN;
  }
  if ((n < 0) || (n >= cst->next)) {
    warn("charsettable",
	 "index %d out of range (%d entries in use)",
	 n, cst->next);
    return CHARSETTABLE_ERR_INDEX;
  }
  charset_copy(&(cst->simd_charsets[n]), cs);
  return OK;
}
  
