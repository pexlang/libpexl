/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyze.c   Analyze expressions and patterns                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/*
   These functions operate on the pattern->fixed field, which is an
   pexl_Expr *that has no open calls.  I.e. all the call targets have
   been fixed.
*/

#include "old-analyze.h"
#include "expression.h"
#include "print.h"		/* print_charset */

/* Predicates */
#define NULLABLE_P 0
#define NOFAIL_P 1

#define TO_BOOL(e) (e != 0)

/* ----------------------------------------------------------------------------- */
/* Misc                                                                          */
/* ----------------------------------------------------------------------------- */

/* Follow ref, extract binding field, return NULL if it is not a Pattern. */
static Pattern *binding_pattern_value (BindingTable *bt, pexl_Ref ref) {
  Binding *b = env_get_binding(bt, ref);
  if (!b || (b->val.type != Epattern_t)) return NULL;
  return (Pattern *)b->val.ptr;
}

static SymbolTableEntry *entry_from_xcall_node (Node *node, const char *caller) {
  pexl_Binary *pkg;
  pexl_Index symbol;
  if (!node->a.pkg || !node->a.pkg->symtab) {
    warn(caller, "null pkg or pkg->symtab field in TXCall node");
    return NULL;
  }
  set_xref_from_node(pkg, symbol, node);
  return symboltable_get(pkg->symtab, symbol);
}

/* ----------------------------------------------------------------------------- */
/* Does a pattern have captures?                                                 */
/* ----------------------------------------------------------------------------- */

/* Returns YES or NO. */
// Recursive
static int hascaptures(Node *node, int check_calls) {
  Pattern *p;
  int result;
  if (!node)
  {
    inform("hascaptures", "null node arg");
    return PEXL_ERR_NULL;
  }
tailcall:
  switch (node->tag)
  {
  case TCapture:
    return YES;
  case TFunction:
    return YES; /* Conservative */
  case TCall: {
    if (check_calls) {
      p = node->a.pat;
      if (!p)
      {
        warn("hascaptures", "null target of call at node %p", (void *)node);
        return PEXL_ERR_INTERNAL;
      }
      if (!pattern_has_flag(p, PATTERN_READY))
        return PEXL_EXP__ERR_NOTCOMPILED;
      return pattern_has_flag(p, PATTERN_CAPTURES);
    } else {
      /* When not following calls, the question that hascaptures answers
   is whether the tree rooted at 'node' itself has captures. */
      return NO;
    }
  }
  default: {
    switch (numchildren[node->tag]) {
    case 1: { /* return hascaptures(child1(node)); */
      node = child1(node);
      goto tailcall;
    }
    case 2: {
      result = hascaptures(child1(node), check_calls);
      if (result < 0)
        return result; /* error */
      if (result == YES)
        return YES;
      /* else return hascaptures(child2(node)); */
      node = child2(node);
      goto tailcall;
    }
    default: {
      assert(numchildren[node->tag] == 0);
      return NO;
    }
    } /* inner switch */
  }
  } /* switch */
}

int exp_hascaptures (Node *node) {
  return hascaptures(node, YES);
}

int exp_itself_hascaptures (Node *node) {
  return hascaptures(node, NO);
}

/*
   ADD_TO clamps the value of value+delta, and if the value was
   clamped, it sets a flag indicating this happened.
*/
static void ADD_TO (uint32_t *value, uint32_t delta, int *result) {
  if (((int64_t)(PATTERN_MAXLEN - *value)) >= (int64_t)delta)
  {
    *value += delta;
    return;
  }
  *value = PATTERN_MAXLEN;
  *result = EXP_UNBOUNDED;
  return;
}

/*
  Return min and max, where the pattern at 'node' requires at least
  min bytes to succeed, and at most max bytes.

  A nullable pattern has min = 0.
  A fixed-length pattern has min = max.

  Sets result to EXP_UNBOUNDED for unbounded length pattern:
     Implies that max value is meaningless.
     If min value is PATTERN_MAXLEN, it is conservative (actual min may be larger).
  Sets result to EXP_BOUNDED for bounded-length pattern.
     Both min and max are accurate.
  Returns < 0 for error.
*/
// Recursive
static int patlen_loop (Node *node, uint32_t *min, uint32_t *max, int *result) {
  int stat, result_choice;
  uint32_t min1, max1, min2, max2;
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node) {
    inform("exp_patlen", "null node arg");
    return PEXL_ERR_NULL;
  }
tailcall:
  /* Can stop counting once min passes the maximum allowed value. */
  if ((*min == PATTERN_MAXLEN) && (*result == EXP_UNBOUNDED)) {
    assert(*max == *min);
    return OK;
  }
  switch (node->tag) {
  case TBackref:
    (*max) = 67890;	       /* Suspicious value to aid debugging */
    *result = EXP_UNBOUNDED;
    return OK;
  case TFind:
    node = child1(node);
    /* fallthrough */
  case TBytes:
    /* byte string length is cached in the node struct */
    ADD_TO(min, node->b.n, result);
    ADD_TO(max, node->b.n, result);
    return OK;
  case TSet:
  case TAny:
    ADD_TO(min, 1, result);
    ADD_TO(max, 1, result);
    return OK;
  case TFalse:
  case TTrue:
  case TNot:
  case TAhead:
  case TBehind:
    return OK;
  case TFunction:
    /* Conservatively, we must declare unbounded. */
    /* And set max to a suspicious value, to aid debugging */
    (*max) = 12345;
    *result = EXP_UNBOUNDED;
    return OK;
  case TRep:
    /* TODO: Rename to TStar, since it's Kleene star? */
    /* TRep matches 0 or more occurrences, so always unbounded */
    /* Set max to a suspicious value, to aid debugging */
    (*max) = 54321;
    *result = EXP_UNBOUNDED;
    return OK;
  case TCapture: {
    /* Return patlen(child1(node), min, max); */
    node = child1(node);
    goto tailcall;
  }
  case TSeq: {
    stat = patlen_loop(child1(node), min, max, result);
    if (stat != OK)
      return stat; /* pexl_Error */
    /* Else patlen_loop(child2(node), min, max); */
    node = child2(node);
    goto tailcall;
  }
  case TChoice: {
    /* Check both branches. If either is UNBOUNDED, then result is UNBOUNDED. */
    min1 = 0;
    max1 = 0;
    result_choice = EXP_BOUNDED;
    stat = patlen_loop(child1(node), &min1, &max1, &result_choice);
    if (stat < 0)
      return stat; /* pexl_Error */
    if (result_choice == EXP_UNBOUNDED)
      *result = EXP_UNBOUNDED;
    min2 = 0;
    max2 = 0;
    result_choice = EXP_BOUNDED;
    stat = patlen_loop(child2(node), &min2, &max2, &result_choice);
    if (stat < 0)
      return stat; /* pexl_Error */
    if (result_choice == EXP_UNBOUNDED)
      *result = EXP_UNBOUNDED;
    ADD_TO(min, (min1 < min2) ? min1 : min2, result);
    ADD_TO(max, (max1 > max2) ? max1 : max2, result);
    return OK;
  }
  case TCall: {
    p = node->a.pat;
    if (!p) {
      warn("exp_patlen", "invalid target of call at node %p", (void *)node);
      return PEXL_ERR_INTERNAL; /* invalid ref? or non-pattern value? */
    }
    if (!pattern_has_flag(p, PATTERN_READY)) {
      return PEXL_EXP__ERR_NOTCOMPILED;
    }
    if (pattern_has_flag(p, PATTERN_UNBOUNDED))
      *result = EXP_UNBOUNDED;
    ADD_TO(min, pattern_min_length(p), result);
    ADD_TO(max, pattern_max_length(p), result);
    return OK;
  }
  case TXCall: {
    entry = entry_from_xcall_node(node, "exp_patlen");
    if (!entry) {
      return PEXL_ERR_INTERNAL; /* invalid reference somehow */
    }
    if (!HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_READY)) {
      return PEXL_EXP__ERR_NOTCOMPILED;
    }
    if (HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_UNBOUNDED))
      *result = EXP_UNBOUNDED;
    ADD_TO(min, metadata_minlen(entry->meta), result);
    ADD_TO(max, metadata_maxlen(entry->meta), result);
    return OK;
  }
  case TOpenCall:
    warn("exp_patlen", "encountered open call");
    return PEXL_EXP__ERR_OPENFAIL;
    case TWhere:
     {
     node = child1(node);
    goto tailcall;
  }

  default:
    warn("exp_patlen", "unexpected expression type (%d)", node->tag);
    return PEXL_ERR_INTERNAL;
  }
}

/* Returns EXP_BOUNDED, EXP_UNBOUNDED, or error < 0 */
int exp_patlen (Node *node, uint32_t *min, uint32_t *max) {
  int stat, result = EXP_BOUNDED;
  (*min) = 0;
  (*max) = 0;
  stat = patlen_loop(node, min, max, &result);
  return (stat != OK) ? stat : result;
}

/*
   Explanation of checknode from Roberto Ierusalimschy, who wrote the
   original version in lpeg:

    * Checks how a pattern behaves regarding the empty string, in one
    * of two different ways:
    *
    * A pattern is *nullable* if it can match without consuming any
    * character; A pattern is *nofail* if it never fails for any string
    * (including the empty string).
    *
    * The difference is only for predicates and run-time captures; for
    * other patterns, the two properties are equivalent.  (With
    * predicates, &'a' is nullable but not nofail.  Of course, nofail =>
    * nullable.)
    *
    * These functions are all conservative in the following way:
    *    p is nullable => nullable(p)
    *    nofail(p) => p cannot fail

    Returns 0 (false), 1 (true), or a value < 0 (error).
*/
// Recursive
static int checknode (Node *node, int pred) {
  Pattern *p;
  SymbolTableEntry *entry;
  int res = 0;
  if (!node)
  {
    inform("checknode", "null node arg");
    return PEXL_ERR_NULL;
  }
tailcall:
  switch (node->tag) {    
  case TBackref:
    /*
      Should we allow a backreference to a capture of an empty string?
      Right now, we say YES, which makes a backref expression nullable.
    */
    if (pred == NOFAIL_P)
      return NO; /* nullable, but can fail */
    else
      return YES; /* nullable */
  case TBytes:
  case TSet:
  case TAny:
  case TFalse:
    return NO; /* not nullable, not nofail */
  case TRep:
  case TTrue:
    return YES; /* nullable, no fail */
  case TNot:
  case TBehind:
    if (pred == NOFAIL_P)
      return NO; /* nullable, but can fail */
    else
      return YES; /* nullable */
  case TAhead:    /* nullable; nofail iff body is nofail */
    if (pred == NULLABLE_P)
      return YES;
    /* Else return checknode(child1(node), pred); */
    node = child1(node);
    goto tailcall;
  case TWhere:
    node = child1(node);
    goto tailcall;
  case TCall:
  {
    p = node->a.pat;
    if (!p)
    {
      /* Invalid ref or non-pattern value */
      return PEXL_ERR_INTERNAL;
    }
    if (!pattern_has_flag(p, PATTERN_READY))
    {
      return PEXL_EXP__ERR_NOTCOMPILED;
    }
    return (pred == NOFAIL_P) ? TO_BOOL(pattern_has_flag(p, PATTERN_NOFAIL)) : TO_BOOL(pattern_has_flag(p, PATTERN_NULLABLE));
  }
  case TXCall:
  {
    entry = entry_from_xcall_node(node, "checknode");
    if (!entry)
      return PEXL_ERR_INTERNAL; /* Invalid reference somehow */
    if (!HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_READY))
      return PEXL_EXP__ERR_NOTCOMPILED;
    return (pred == NOFAIL_P) ? TO_BOOL(HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_NOFAIL)) : TO_BOOL(HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_NULLABLE));
  }
  case TFunction:
    /* Conservative answer */
    if (pred == NOFAIL_P)
      return NO; /* nullable, but can fail */
    else
      return YES; /* nullable */
  case TSeq:
    res = checknode(child1(node), pred);
    if (res < 0 || res == NO)
      return res;
    assert(res == YES);
    /* Else return checknode(child2(node), pred); */
    node = child2(node);
    goto tailcall;
  case TChoice:
    res = checknode(child2(node), pred);
    if (res < 0 || res == YES)
      return res;
    assert(res == NO);
    /* Else return checknode(child1(node), pred); */
    node = child1(node);
    goto tailcall;
  case TCapture:
    /* Return checknode(child1(node), pred); */
    node = child1(node);
    goto tailcall;
  case TOpenCall:
    warn("checknode", "encountered open call");
    return PEXL_EXP__ERR_OPENFAIL;
  case TFind:
    if (pred == NOFAIL_P)
      return NO;
    else
      return YES;
  default:
    warn("checknode", "unexpected expression type (%d)", node->tag);
    return PEXL_ERR_INTERNAL;
  }
}

int exp_nofail (Node *node) {
  return checknode(node, NOFAIL_P);
}

/* FUTURE: exp is nullable if (1) it is not TFalse and (2) its min length is 0 */
int exp_nullable (Node *node) {
  return checknode(node, NULLABLE_P);
}

/*
 * If 'headfail(node)' true, then 'node' can fail ONLY depending on
 * the next character of the subject.  Note that a multi-byte string
 * cannot be headfail, because it may fail on a byte AFTER the first
 * one.
 *
 * Returns 0, 1, or error < 0.
 */
// Recursive
int exp_headfail (Node *node) {
  int stat;
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node) {
    inform("exp_headfail", "null node arg");
    return PEXL_ERR_NULL;
  }
tailcall:
  switch (node->tag) {
  case TSet:
  case TAny:
  case TFalse:
    return YES;
  case TBytes:
    /* Single byte is headfail */
    return (node->b.n == 1);
  case TTrue:
  case TRep:
  case TNot:
  case TBehind:
    return NO;
  case TCall: {
    p = node->a.pat;
    if (!p)
      return PEXL_EXP__ERR_NOTCOMPILED; /* or could be an invalid ref */
    if (!pattern_has_flag(p, PATTERN_READY))
      return PEXL_EXP__ERR_NOTCOMPILED;
    return pattern_has_flag(p, PATTERN_HEADFAIL);
  }
  case TXCall: {
    entry = entry_from_xcall_node(node, "headfail");
    if (!entry)
      return PEXL_ERR_INTERNAL; /* invalid reference somehow */
    if (!HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_READY))
      return PEXL_EXP__ERR_NOTCOMPILED;
    return HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_HEADFAIL);
  }
  case TCapture:
  case TAhead:
    node = child1(node);
    goto tailcall; /* return headfail(child1(node)); */
  case TSeq:
    /* A seq is headfail only when child1 is headfail and child2 is
       nofail, because even if child1 succeeds based on the next char,
       the seq itself can still fail if child2 fails.
    */
    stat = exp_nofail(child2(node));
    if (stat < 0)
      return stat; /* error */
    if (stat == NO)
      return NO;
    /* Else return headfail(child1(node)); */
    node = child1(node);
    goto tailcall;
  case TChoice:
    stat = exp_headfail(child1(node));
    if (stat < 0)
      return stat;
    if (stat == NO)
      return NO;
    /* Else return headfail(child2(node)); */
    node = child2(node);
    goto tailcall;
  case TFunction:
    return NO; /* Conservative */
  case TFind:
    return NO;
    case TWhere:
  
      node = child1(node);
      goto tailcall;
  
  case TOpenCall:
    warn("headfail", "encountered open call");
    return PEXL_EXP__ERR_OPENFAIL;
  default:
    warn("exp_headfail", "unexpected expression type (%d)", node->tag);
    return PEXL_ERR_INTERNAL;
  }
}

/*
  [lpeg] Computes the 'first set' of a pattern. The result is a
  conservative aproximation:

      match p ax -> x (for some x) ==> 'a' \in first(p)
  -OR-
     'a' \notin first(p) ==> match p ax -> fail (for all x)

  The set 'follow' is the first set of what follows the
  pattern (full set if nothing follows it).

  The function returns 0 when this resulting set can be used for test
  instructions that avoid the pattern altogether.

  A non-zero return can happen for two reasons:
  1) match p '' -> ''            ==> return has bit 0 set
     In this case, tests cannot be used because they would always fail
     for an empty input.
  2) There is a runtime capture ==> return has bit 1 set
     Optimizations should not bypass runtime captures. NOTE: run-time
     captures are not currently implemented in PEXL but may be in the
     future.
*/
// Recursive
int exp_getfirst (Node *node, const Charset *follow, Charset *firstset) {
  int e, e1, e2;
  Charset csaux;
  Pattern *p;
  SymbolTableEntry *entry;
  if (!node || !follow || !firstset) {
    inform("exp_getfirst", "null argument (node or followset or firstset)");
    return PEXL_ERR_NULL;
  }
tailcall:
  switch (node->tag) {
  case TFind: {
    charset_fill_one(firstset->cs);
    return 1;
  }
  case TSet:
  case TAny: {
    to_charset(node, firstset);
    return 0; /* k(0) */
  }
  case TBytes: {
    charset_fill_zero(firstset->cs);  /* erase all chars */
    setchar(firstset->cs, node->cap); /* add the first char of byte string */
    return 0;                         /* k(0) */
  }
  case TTrue: {
    charset_copy(firstset->cs, follow->cs);
    /* accepts the empty string */
    return 1; /* k(1) */
  }
  case TFalse: {
    charset_fill_zero(firstset->cs);
    return 0; /* k(0)                 TYPE0(k) */
  }
  case TChoice: {
    /* recur(child1,
       follow,
       firstset,
       \e1.if e1<0 k(e1)                         TYPE1(child2, follow, firstset, csaux, k)
                else recur(child2,
               follow,
               csaux,
               \e2.if e2<0 k(e2)           TYPE2(e1, firstset, csaux, k)
            else loopset(firstset, csaux); k(e1|e2)))
    */
    e1 = exp_getfirst(child1(node), follow, firstset);
    if (e1 < 0)
      return e1;
    e2 = exp_getfirst(child2(node), follow, &csaux);
    if (e2 < 0)
      return e2;
    charset_or_eq(firstset->cs, csaux.cs);
    return e1 | e2;
  }
  case TSeq: {
    if (!exp_nullable(child1(node))) {
      /* When p1 is not nullable, p2 has nothing to contribute;
	 return getfirst(child1(node), fullset, firstset); */
      node = child1(node);
      follow = fullset;
      goto tailcall;
      /* follow=first;
	 recur(child1, fullset, firstset, k)
      */
    } else {
      /* FIRST(p1 p2, fl) = FIRST(p1, FIRST(p2, fl)) */
      /* recur(child2,
         follow,
         csaux,
   \e2.if e2<0 k(e2)                         TYPE3(child1, firstset, csaux)
      else recur(child1,
                 csaux,
           firstset,
           \e1.if e1<=0 k(e1)          TYPE4(e2, k)
        else if (e1|e2)&2 k(2)
             else k(e2)
      */
      e2 = exp_getfirst(child2(node), follow, &csaux);
      if (e2 < 0)
        return e2;
      e1 = exp_getfirst(child1(node), &csaux, firstset);
      if (e1 < 0)
        return e1;
      if (e1 == 0)
        return 0;             /* 'e1' ensures that first can be used */
      else if ((e1 | e2) & 2) /* one of the children has a runtime capture? */
        return 2;             /* pattern has a runtime capture */
      else
        return e2; /* else depends on 'e2' */
    }
  }
  case TRep: {
    e = exp_getfirst(child1(node), follow, firstset);
    if (e < 0)
      return e; /* error */
    charset_or_eq(firstset->cs, follow->cs);
    return 1; /* accept the empty string */
  }
  case TCapture: {
    /* return getfirst(child1(node), follow, firstset); */
    node = child1(node);
    goto tailcall;
  }
  case TFunction: { /* function invalidates any follow info. */
    /* recur(child1,
       fullset,
       firstset,
       \e.if (e<0) k(e)           TYPE5(k)
          elseif (e) k(2)
    else k(0))
    */
    e = exp_getfirst(child1(node), fullset, firstset);
    if (e < 0)
      return e; /* error */
    if (e)
      return 2; /* function is not "protected"? */
    else
      return 0; /* pattern inside capture ensures first can be used */
  }
  case TCall: {
    /* Use the metadata about the target of the call. */
    p = node->a.pat;
    if (!p)
    {
      inform("exp_getfirst", "null call target in node %p", (void *)node);
      return PEXL_ERR_INTERNAL;
    }
    charset_copy(firstset->cs, metadata_firstset_ptr(p->meta)->cs);
    return pattern_has_flag(p, PATTERN_NULLABLE);
  }
  case TXCall: {
    entry = entry_from_xcall_node(node, "exp_getfirst");
    if (!entry)
      return PEXL_ERR_INTERNAL; /* invalid reference somehow */
    charset_copy(firstset->cs, metadata_firstset_ptr(entry->meta)->cs);
    return HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_NULLABLE);
  }
  case TAhead: {
    /* recur(child1,
             follow,
       firstset,
       \e.if (e<0) k(e)                          TYPE6(k, firstset, follow)
          else loopset(firstset, follow); k(e))
    */
    e = exp_getfirst(child1(node), follow, firstset);
    if (e < 0) return e;
    charset_and_eq(firstset->cs, follow->cs);
    return e;
  }
  case TNot: {
    if (to_charset(child1(node), firstset)) {
      cs_complement(firstset);
      return 1;
    }
    __attribute__((fallthrough)); /* same as below */
  }
  case TBehind: { 
    /* instruction gives no new information */
    /* call 'getfirst' here only to check for runtime captures */
    /* recur(child1,
       follow,
       firstset,
       \e.if (e<0) k(e)                         TYPE7(k, firstset, follow)
       else loopset(firstset, follow); k(e|1))
    */
    e = exp_getfirst(child1(node), follow, firstset);
    if (e < 0)
      return e;
    charset_copy(firstset->cs, follow->cs); /* uses follow */
    return e | 1;	                    /* always can accept the empty string */
  }
  case TOpenCall: {
    warn("getfirst", "encountered open call");
    return PEXL_EXP__ERR_OPENFAIL;
  }

  case TWhere:
   {
     node = child1(node);
    goto tailcall;
  }

  default:
    warn("exp_getfirst", "unexpected expression type (%d)", node->tag);
    return PEXL_ERR_INTERNAL;
  }
}

/*
** Check whether the code generation for the given node can benefit
** from a follow set (to avoid computing the follow set when it is
** not needed)
*/
int exp_needfollow (Node *node) {
  //  Pattern *p;
  //  SymbolTableEntry *entry;
  if (!node)
  {
    inform("exp_needfollow", "null node arg");
    return PEXL_ERR_NULL;
  }
tailcall:
  switch (node->tag) {
  case TBackref:
  case TBytes:
  case TSet:
  case TAny:
  case TFalse:
  case TTrue:
  case TAhead:
  case TNot:
  case TFunction:
  case TBehind:
  case TFind:
    /* Returning 0 is conservative for TCall, TBehind */
    return NO;
    /* We are not, at present, using the follow set in codeICall or codeXCall */
  case TCall:
  case TXCall:
    return NO;
//   case TCall:
//   {
//     /* Use the metadata about the target of the call. */
//     p = node->a.pat;
//     if (!p)
//       return PEXL_EXP__ERR_NOTCOMPILED; /* or could be an invalid ref */
//     if (!pattern_has_flag(p, PATTERN_READY))
//       return PEXL_EXP__ERR_NOTCOMPILED;
//     return pattern_has_flag(p, PATTERN_NEEDFOLLOW);
//   }
//   case TXCall:
//   {
//     entry = entry_from_xcall_node(node, "needfollow");
//     if (!entry)
//       return PEXL_ERR_INTERNAL; /* invalid reference somehow */
//     if (!HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_READY))
//       return PEXL_EXP__ERR_NOTCOMPILED;
//     return HAS_BITFLAG(metadata_flags(entry->meta), PATTERN_NEEDFOLLOW);
//   }
  case TChoice:
  case TRep:
    return YES;
  case TCapture:
    node = child1(node);
    goto tailcall;
  case TSeq:
    node = child2(node);
    goto tailcall;
  case TOpenCall:
    warn("needfollow", "encountered open call");
    return PEXL_EXP__ERR_OPENFAIL;
    case TWhere:
     {
     node = child1(node);
    goto tailcall;
  }

  default:
  {
    warn("exp_needfollow", "unexpected expression type (%d)", node->tag);
    return PEXL_ERR_INTERNAL;
  }
  }
}

/* Return YES if target is in the SCC [start, finish) */
static int in_SCC (CompState *cs, Pattern *target, pexl_Index start, pexl_Index finish) {
  pexl_Index i;
  for (i = start; i < finish; i++)
    if (cs->g->deplists[cs->g->order->elements[i].vnum][0] == target)
      return YES;
  return NO;
}

/*
  A leftcall is the first call in a sequence, where all the preceding elements
  of the sequence are nullable.  A choice may have two left calls, one for
  each child.
*/
// Recursive
static int follow_leftcalls (CompState *cs, pexl_Index start, pexl_Index finish,
			     Node *node, int *nullable, pexl_Index *npassed) {
  int stat, child_nullable;
  Pattern *target;
  pexl_Index nrules = finish - start;
  assert(nrules > 0);
  assert(cs);
  assert(npassed);
  assert(nullable);
tailcall:
  assert(node);
  switch (node->tag)
  {
  case TBytes:
  case TSet:
  case TAny:
  case TFalse:
    /* Answer is in *nullable already */
    return OK;
  case TTrue:
    *nullable = YES;
    return OK;
  case TBehind:
  case TNot:
  case TAhead:
  case TRep:
  case TCapture:
    /* Return follow_leftcalls(child1(node), YES); */
    *nullable = YES;
    node = child1(node);
    goto tailcall;
  case TSeq:
    /*
       If first child has a left call, follow it.  Else, if first
       child is nullable, then check second child.
    */
    child_nullable = 0;
    stat = follow_leftcalls(cs, start, finish, child1(node), &child_nullable, npassed);
    if (stat < 0)
      return stat;
    if (!child_nullable)
    {
      *nullable = NO;
      return OK;
    }
    /*
       Else first child is nullable, so 'node' is nullable IFF child2 is.
       Return leftcall(child2(node), nullable);
    */
    node = child2(node);
    goto tailcall;
  case TChoice:
    /*
       Must always check both children.  Either child1 or child2 may
       have a left call, and we need to follow both of them.
    */
    stat = follow_leftcalls(cs, start, finish, child1(node), nullable, npassed);
    if (stat < 0)
      return stat;
    /*
       Note: *nullable now indicates whether child1 is nullable.
       Return follow_leftcalls(child2(node), nullable);
    */
    node = child2(node);
    goto tailcall;
  case TCall:
    target = node->a.pat;
    if (!target) {
      warn("follow_leftcalls", "call target is null");
      return PEXL_ERR_INTERNAL;
    }
    if (target->env < 0) {
      warn("follow_leftcalls", "call target has invalid environment");
      return PEXL_ERR_INTERNAL;
    }
    /*
       If target of call is not in this package, it must be compiled
       already.  Use its nullable flag.  (We could add a separate test
       for this, even though the SCC test subsumes it, because a
       package identity test is fast -- just a pointer comparison.)

       Else the target is in this package but not in the SCC.  The
       topological sort ensures that this call target will have been
       compiled before this SCC, so we can use its nullable flag.
    */
    if (!in_SCC(cs, target, start, finish)) {
      if (!pattern_has_flag(target, PATTERN_READY))
        return PEXL_EXP__ERR_NOTCOMPILED;
      *nullable = pattern_has_flag(target, PATTERN_NULLABLE);
      return OK;
    }
    /* Else target is within this SCC, so count it and follow it. */
    (*npassed)++;
    /* But first check that we are not going in circles. */
    if (*npassed >= nrules)
      return PEXL_EXP__ERR_LEFT_RECURSIVE;
    node = target->fixed->node;
    goto tailcall;
  case TOpenCall:
    warn("follow_leftcalls", "encountered open call");
    return PEXL_EXP__ERR_OPENFAIL;
  case TFind:
    return NO;
  default:
    warn("follow_leftcalls", "unexpected node type: %d", node->tag);
    return PEXL_ERR_INTERNAL;
  }
}

static int exp_nullable_rec (CompState *cs, Pattern *p, pexl_Index start, pexl_Index finish) {
  int stat, nullable;
  pexl_Index npassed;
  npassed = 0;
  nullable = NO;
  /* Follows all left calls.  Sets nullable and updates the npassed count. */
  stat = follow_leftcalls(cs, start, finish, p->fixed->node, &nullable, &npassed);
  if (stat < 0) return stat; /* Could be any error, including LEFT_RECURSIVE */
  return nullable;
}

/* ----------------------------------------------------------------------------- */
/* Setting pattern metadata                                                      */
/* ----------------------------------------------------------------------------- */

/*
  Set metadata for a non-recursive pattern.  Dependencies are EXPECTED TO BE
  COMPILED ALREADY.  If they are not compiled (or not a pattern), then
  set_nonrecursive_metadata will return PEXL_EXP__ERR_NOTCOMPILED.
*/
static int set_nonrecursive_metadata1 (Pattern *p) {
  pexl_Expr *tree = p->fixed;
  int result;
  uint32_t min, max, flags = 0;

  assert(tree);
  trace("analyzer", "setting metadata for pattern %p", (void *)p);

  result = exp_hascaptures(tree->node);
  if (result < 0)
    return result;
  UPDATE_BITFLAG(flags, PATTERN_CAPTURES, result);
  trace("analyzer", "metadata: hascaptures %s", result ? "YES" : "NO");

  result = exp_nullable(tree->node);
  if (result < 0)
    return result;
  UPDATE_BITFLAG(flags, PATTERN_NULLABLE, result);
  trace("analyzer", "metadata: nullable %s", result ? "YES" : "NO");

  result = exp_nofail(tree->node);
  if (result < 0)
    return result;
  UPDATE_BITFLAG(flags, PATTERN_NOFAIL, result);
  trace("analyzer", "metadata: nofail %s", result ? "YES" : "NO");

  result = exp_headfail(tree->node);
  if (result < 0)
    return result;
  UPDATE_BITFLAG(flags, PATTERN_HEADFAIL, result);
  trace("analyzer", "metadata: headfail %s", result ? "YES" : "NO");

  result = exp_getfirst(tree->node, fullset, metadata_firstset_ptr(p->meta));
  if (result < 0)
    return result;
  trace("analyzer", "metadata: p->meta.firstset is:  ");
  WHEN_TRACING
  {
    fprint_charset(stderr, metadata_firstset_ptr(p->meta)->cs);
    fprintf(stderr, "\n");
  }

  result = exp_needfollow(tree->node);
  if (result < 0)
    return result; /* pexl_Error */
  UPDATE_BITFLAG(flags, PATTERN_NEEDFOLLOW, result);

  result = exp_patlen(tree->node, &min, &max);
  if (result < 0)
    return result; /* pexl_Error */
  if (result == EXP_UNBOUNDED)
  {
    UPDATE_BITFLAG(flags, PATTERN_UNBOUNDED, YES);
    trace("analyzer", "metadata: unbounded YES");
  }
  set_metadata_minlen(p->meta, min);
  set_metadata_maxlen(p->meta, max);

  UPDATE_BITFLAG(flags, PATTERN_READY, YES);
  set_patflags(p, flags);
  trace("analyzer", "metadata: minlen = %u, maxlen = %u, READY = %s",
	min, max, pattern_has_flag(p, PATTERN_READY) ? "YES" : "NO");
  return OK;
}

/*
   If any of the recursive set has captures, then they all do
   (conservative approx).  Also, mark the recursive patterns as READY.
   N.B. Use this ONLY on bindings that are mutually/self recursive.
*/
static int set_recursive_metadata (CompState *cs, pexl_Index start, pexl_Index finish) {
  int stat;
  pexl_Index i, vnum;
  Pattern *p;
  uint32_t flags;
  int hascaps = NO;
  assert(cs->g);
  assert(cs->g->order);
  assert(start >= 0);
  assert(finish <= cs->g->next);
  assert(start <= finish);

  /* Within this set of patterns, do any have captures? */
  /* While we are iterating, we also set the nullable flag for each. */
  for (i = start; i < finish; i++)
  {
    assert(i < cs->g->order->top);
    vnum = cs->g->order->elements[i].vnum;
    assert(vnum >= 0);
    assert(vnum < cs->g->order->top);
    p = cs->g->deplists[vnum][0];
    if (!p)
    {
      trace("set_recursive_metadata",
           "null pattern (vertex in graph) at order index %" pexl_Index_FMT, i);
      cs->err_index = i;
      return PEXL_ERR_INTERNAL;
    }
    stat = exp_itself_hascaptures(p->fixed->node);
    if (stat < 0)
    {
      cs->err_index = i;
      return stat; /* error */
    }
    hascaps = hascaps | stat;
  } /* for */

  /* Set the flags for captures (based on previous calculation),
     nofail, and fixedlen.

     FUTURE: Can we make a better approximation here?  E.g. in many
     cases we can compute HEADFAIL, right?
  */
  for (i = start; i < finish; i++)
  {
    vnum = cs->g->order->elements[i].vnum;
    assert(vnum >= 0);
    assert(vnum < cs->g->order->top);
    p = cs->g->deplists[vnum][0];
    if (!p)
    {
      warn("set_recursive_metadata",
           "null pattern (vertex in graph) at index %" pexl_Index_FMT, i);
      cs->err_index = i;
      return PEXL_ERR_INTERNAL;
    }
    flags = patflags(p);
    UPDATE_BITFLAG(flags, PATTERN_CAPTURES, hascaps);

    /* nofail should be set to false to be conservative */
    UPDATE_BITFLAG(flags, PATTERN_NOFAIL, NO);
    /* unbounded should be set to true (conservative) */
    UPDATE_BITFLAG(flags, PATTERN_UNBOUNDED, YES);
    /* headfail should be set to false (conservative) */
    UPDATE_BITFLAG(flags, PATTERN_HEADFAIL, NO);
    /* needfollow is NO only to avoid extra work; conservative answer is YES */
    UPDATE_BITFLAG(flags, PATTERN_NEEDFOLLOW, YES);
    set_patflags(p, flags);

    set_pattern_minlen(p, 0);    /* conservative */
    set_pattern_maxlen(p, 4096); /* meaningless, so set a conspicuous value */

    /* special nullable check that detects left recursion */
    stat = exp_nullable_rec(cs, p, start, finish);
    if (stat < 0)
    {
      cs->err_index = i;
      return stat; /* error, possibly a LEFT_RECURSIVE error */
    }
    UPDATE_BITFLAG(flags, PATTERN_NULLABLE, stat);

    stat = exp_getfirst(p->fixed->node, fullset, metadata_firstset_ptr(p->meta));
    if (stat < 0)
      return stat;
  } /* for */

  for (i = start; i < finish; i++)
  {
    vnum = cs->g->order->elements[i].vnum;
    assert(vnum >= 0);
    assert(vnum < cs->g->order->top);
    p = cs->g->deplists[vnum][0];
    if (!p)
    {
      warn("set_recursive_metadata",
           "null pattern (vertex in graph) at index %" pexl_Index_FMT, i);
      cs->err_index = i;
      return PEXL_ERR_INTERNAL;
    }
    /* Set the pattern to READY, because the metadata is now set. */
    flags = patflags(p);
    UPDATE_BITFLAG(flags, PATTERN_READY, YES);
    set_patflags(p, flags);
  } /* for */
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* Enforce scope rules, fix open calls, correct associativity                    */
/* ----------------------------------------------------------------------------- */

/* Almost unchanged from lpeg:
 *
 *  Transform left associative constructions into right
 *  associative ones, for sequence and choice; that is:
 *  (t11 + t12) + t2  =>  t11 + (t12 + t2)
 *  (t11 * t12) * t2  =>  t11 * (t12 * t2)
 *  That is, Op (Op t11 t12) t2 => Op t11 (Op t12 t2).
 */
static void correctassociativity (Node *node) {
  Node *t1 = child1(node);
  assert(node->tag == TChoice || node->tag == TSeq);
  while (t1->tag == node->tag) {
    int n1size = node->b.ps - 1; /* t1 == Op t11 t12 */
    int n11size = t1->b.ps - 1;
    int n12size = n1size - n11size - 1;
    memmove(child1(node), child1(t1), n11size * sizeof(Node)); /* move t11 */
    node->b.ps = n11size + 1;
    child2(node)->tag = node->tag;
    child2(node)->b.ps = n12size + 1;
  }
}

/*
   Walk expression tree rooted at 'node' to free the 'fixed' copy of
   the expression tree.

   N.B. This procedure also resets PATTERN_READY and the entrypoint
   field.
*/
// Recursive
static void free_all_fixed (BindingTable *bt, Node *node) {
  pexl_Ref ref;
  Pattern *target;
tailcall:
  switch (node->tag) {
  case TOpenCall: {
    set_ref_from_node(ref, node);
    target = binding_pattern_value(bt, ref);
    if (!target)
      return; /* ignore error */
    if (!target->fixed)
      return; /* already processed */
    trace("free_all_fixed",
         "recompiling call target pattern %p", (void *)target);
    pattern_set_flag(target, PATTERN_READY, NO);
    target->entrypoint = -1; /* NOT SET */
    pexl_free_Expr(target->fixed);
    target->fixed = NULL;
    node = target->tree->node;
    goto tailcall;
  }
  default:
    switch (numchildren[node->tag]) {
    case 1:
      node = child1(node);
      goto tailcall;
    case 2:
      free_all_fixed(bt, child1(node));
      node = child2(node);
      goto tailcall;
    default:
      assert(numchildren[node->tag] == 0);
      break;
    }
  }
  return;
}

/* Create a copy of the exp structure that will be used to fix open calls */
// Recursive
static void refresh_all_fixed (BindingTable *bt, Node *node) {
  pexl_Ref ref;
  Pattern *target;
tailcall:
  switch (node->tag) {
  case TOpenCall: {
    set_ref_from_node(ref, node);
    target = binding_pattern_value(bt, ref);
    if (!target)
      return; /* ignore error */
    if (target->fixed)
      return; /* processed this pattern already */
    target->fixed = pexl_copy_Expr(target->tree);
    assert(target->fixed);
    node = target->tree->node;
    goto tailcall;
  }
  default:
    switch (numchildren[node->tag])
    {
    case 1:
      node = child1(node);
      goto tailcall;
    case 2:
      refresh_all_fixed(bt, child1(node));
      node = child2(node);
      goto tailcall;
    default:
      assert(numchildren[node->tag] == 0);
      break;
    }
  }
  return;
}

/*
  Recursively (through dependencies) free all of the pat->fixed
  expressions, and then re-create them by copying each pat->tree.
*/
void reset_compilation_state (Pattern *pat) {
  pat->entrypoint = -1;
  if (pat->fixed) {
    trace("reset_compilation_state",
	  "recompiling call target pattern %p", (void *)pat);
    pexl_free_Expr(pat->fixed);
    pat->fixed = NULL;
  }
  free_all_fixed(pat->bt, pat->tree->node);
  refresh_all_fixed(pat->bt, pat->tree->node);
  pattern_set_flag(pat, PATTERN_READY, NO);
  if (!pat->fixed) {
    pat->fixed = pexl_copy_Expr(pat->tree);
    assert(pat->fixed);
  }
}

/*
   Verify that the target of every call (encoded a 'ref') is a
   pattern, and that it is in scope.  While we are walking the
   expression tree, we also correct the associativity of associative
   constructions (making them right associative).
*/
/*
   FUTURE: Factor this into separate transformations: correct
   associativity and fix open calls.  Can still do them in the same
   tree traversal if we want, but they are independent
   transformations.
*/
// Recursive
static int fixnode (BindingTable *bt, pexl_Env compilation_env, Node *node) {
  pexl_Ref ref;
  int stat;
  pexl_Env env, target_env;
  Pattern *target;
tailcall:
  switch (node->tag) {
  case TOpenCall: {
    set_ref_from_node(ref, node);
    /* Ensure that call target is a pattern */
    target = binding_pattern_value(bt, ref);
    if (!target) {
      if (pexl_Ref_notfound(ref))
        warn("fixnode", "target of call in node %p is invalid reference", (void *)node);
      else
        warn("fixnode", "target of call in node %p is not a pattern", (void *)node);
      return PEXL_EXP__ERR_NO_PATTERN;
    }
    /* target->env field _should_ be set already */
    target_env = target->env;
    if (target_env < 0) {
      warn("fixnode", "call target has invalid (or not set) environment field");
      return PEXL_ERR_INTERNAL;
    }
    /* Check that target is in scope */
    env = compilation_env;
    while (env && (env != target_env)) {
      env = env_get_parent(bt, env);
    }
    /* Chain of parents should end at root, which is env 0 */
    if (env != target_env) return PEXL_EXP__ERR_SCOPE;
    /* Fix TOpenCall => TCall */
    node->tag = TCall;
    node->a.pat = target;
    /* fixnode(target) */
    assert(target->fixed);
    node = target->fixed->node;
    goto tailcall;
  }
  case TSeq:
  case TChoice: {
    correctassociativity(node);
    /* fallthrough */
  }
  } /* switch */
  switch (numchildren[node->tag]) {
  case 1:
    node = child1(node);
    goto tailcall;
  case 2:
    stat = fixnode(bt, compilation_env, child1(node));
    if (stat < 0)
      return stat;
    node = child2(node);
    goto tailcall;
  default: {
    assert(numchildren[node->tag] == 0);
    break;
  }
  } /* switch */
  return OK;
}

/* Make final tests and adjustments to all expression trees that we
   need to compile.  Start at cs->pat, fix each TOpenCall node, and
   then recursively follow the resulting TCall node to ensure the call
   targets are all fixed.

   For convenience, we are populating pattern->fixed here, but
   probably we should factor this out and do it somewhere else?

   FUTURE: This computes reachability, so we could build the
   dependency graph at the same time.

*/
int fix_open_calls (CompState *cs) {
  if (!cs || !cs->pat || !cs->pat->bt || !cs->pat->fixed) {
    warn("fix_open_calls", "null arg");
    return PEXL_ERR_INTERNAL;
  }
  return fixnode(cs->pat->bt, cs->pat->env, cs->pat->fixed->node);
}

/* ----------------------------------------------------------------------------- */
/* Set metadata                                                                  */
/* ----------------------------------------------------------------------------- */

/* Set the metadata for the set of patterns in a strongly connected component */
static int set_metadata_component (CompState *cs,
				   pexl_Index start,
				   pexl_Index finish,
				   pexl_Index cnum) {
  int stat;
  void *vptr;
  SCC *order = cs->g->order;
  assert((finish - start) >= 0);
  if (cnum > 0) {
    trace("analyzer", "Setting recursive metadata for order indices %d..%d", start, finish - 1);
    stat = set_recursive_metadata(cs, start, finish);
    if (stat < 0)
      return stat;
  } else {
    trace("analyzer", "Setting nonrecursive metadata for order index %d", start);
    assert(order->elements[start].vnum >= 0);
    assert(((size_t)order->elements[start].vnum) < ((size_t)cs->g->next));
    vptr = (void *)*cs->g->deplists[order->elements[start].vnum];
    trace("analyzer", "  Vertex %d  %p", order->elements[start].vnum, vptr);
    stat = set_nonrecursive_metadata1((Pattern *)vptr);
    if (stat < 0)
      return stat;
  }
  return OK;
}

int set_metadata (CompState *cs) {
  int stat;
  SCC *order;
  pexl_Index i, prev_i;
  pexl_Index prev_cnum;
  if (!cs || !cs->pat || !cs->pat->bt || !cs->g || !cs->g->order) {
    warn("set_metadata", "null arg");
    return PEXL_ERR_INTERNAL;
  }
  order = cs->g->order;
  assert(((size_t)order->top) == ((size_t)cs->g->next));
  if (order->top == 0) return OK; /* empty graph */
  prev_i = 0;
  prev_cnum = order->elements[0].cnum;
  for (i = 1; i < order->top; i++) {
    if (order->elements[i].cnum != prev_cnum) {
      /* Process this component from [prev_i, i) */
      stat = set_metadata_component(cs, prev_i, i, prev_cnum);
      if (stat < 0) return stat;
      /* Set up for next component */
      prev_cnum = order->elements[i].cnum;
      prev_i = i;
    }
  } /* for */
  stat = set_metadata_component(cs, prev_i, i, prev_cnum);
  return stat;
}
