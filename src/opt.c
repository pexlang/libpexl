/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  opt.c   Common utilities for the various opt-*.c files                  */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Tristan Mullins, Jamie A. Jennings                             */

#include "opt.h"

pexl_Index relative_to_abs (const Instruction *op, const Instruction *p) {
  return (pexl_Index)(p - op + getoffset(p));
}

/* Find the final destination of a sequence of jumps */
int finaltarget (Instruction *code, int i) {
  Instruction *pc = &code[i];
  while (opcode(pc) == IJmp) {
    if (getoffset(pc) == 0) {
      warn("optimizer", "invalid jump target (self) at address %d", i);
      return pc - code;
    }
    pc += getoffset(pc);
  }
  return pc - code;
}

/* Patch 'instruction' to jump to 'target' */
void jumptothere (pexl_Binary *pkg, int instruction, int target) {
  if (instruction >= 0) {
    if (target == instruction) {
      trace("optimizer",
	    "jumptothere", "instruction == target in package %p at index %d",
	    (void *)pkg, instruction);
    } else {
      setoffset(&getinstr(pkg, instruction), target - instruction);
#ifdef DEBUG
      /* Consistency check */
      int op;
      op = opcode(&getinstr(pkg, instruction));
      assert(opcode(&getinstr(pkg, instruction)) == op);
      assert(getoffset(&getinstr(pkg, instruction)) == (target - instruction));
#endif
    }
  }
}

/* Patch 'instruction' to jump to current position */
void jumptohere (pexl_Binary *pkg, int instruction) {
  jumptothere(pkg, instruction, gethere(pkg));
}

/* ----------------------------------------------------------------------------- */
/* API                                                                           */
/* ----------------------------------------------------------------------------- */

/*
  Make an optimizaton pass, add it to the end optimlist with its
  fields set to params. 
*/
static pexl_Optims* make_pexl_Optims (pexl_Optims *optimlist, uint32_t *optim_params)
{
  // Allocate the new pass, which is a node in a linked list
  pexl_Optims *new_pass = (pexl_Optims *)malloc(sizeof(pexl_Optims));
  new_pass->params = optim_params;
  new_pass->next = NULL;

  if (optimlist) {
    // Find the last node in the current optimlist
    pexl_Optims *last = NULL;
    for (pexl_Optims *i = optimlist; i; i = i->next) last = i;
    // Add the new pass to the end, return the original list (head)
    last->next = new_pass;
    return optimlist;
  }
  // Else optimlist was NULL, so we can return the new node,
  // which is the new list
  return new_pass;
}

// Sets the last optimization to be of the given optimization type
static void set_last_optim( pexl_Optims *optimlist, enum pexl_OptimType optim_type) {
  pexl_Optims *cur = NULL;
  for (pexl_Optims *i = optimlist; i; i = i->next)
  {
    cur = i;
  }
  cur->type = optim_type;
}

static uint32_t *alloc_optim_params1 (uint32_t param) {
  uint32_t *params = malloc(sizeof(uint32_t));
  if (!params) {
    warn("optimizer", "out of memory while allocating optimizer configuration parameters");
    return NULL;
  }
  *params = param;
  return params;
}

pexl_Optims *addopt_simd (pexl_Optims *optims, uint32_t cache_size) {
  assert(cache_size > 0);
  uint32_t *optim_params = alloc_optim_params1(cache_size);
  pexl_Optims *updated_optimlist = make_pexl_Optims(optims, optim_params);
  set_last_optim(updated_optimlist, PEXL_OPT_SIMD);
  return updated_optimlist;
}

pexl_Optims *addopt_dynamic_simd (pexl_Optims *optims) {
  pexl_Optims *updated_optimlist = make_pexl_Optims(optims, NULL);
  set_last_optim(updated_optimlist, PEXL_OPT_DYNAMIC_SIMD);
  return updated_optimlist;
}

// TODO: Add parameter which defaults to PEXL_DEFAULT_INLINE_MAXSIZE
pexl_Optims *addopt_inline (pexl_Optims *optims) {
  pexl_Optims *updated_optimlist = make_pexl_Optims(optims, NULL);
  set_last_optim(updated_optimlist, PEXL_OPT_INLINE);
  return updated_optimlist;
}

// TODO: Add parameter which defaults to PEXL_DEFAULT_LOOP_UNROLL
pexl_Optims *addopt_unroll (pexl_Optims *optims, uint32_t max_times_to_unroll) {
  assert(max_times_to_unroll > 1);
  uint32_t *optim_params = alloc_optim_params1(max_times_to_unroll);
  pexl_Optims *updated_optimlist = make_pexl_Optims(optims, optim_params);
  set_last_optim(updated_optimlist, PEXL_OPT_UNROLL);
  return updated_optimlist;
}

pexl_Optims *addopt_peephole (pexl_Optims *optims) {
  pexl_Optims *updated_optimlist = make_pexl_Optims(optims, NULL);
  set_last_optim(updated_optimlist, PEXL_OPT_PEEPHOLE);
  return updated_optimlist;
}

pexl_Optims *addopt_tro (pexl_Optims *optims) {
  pexl_Optims *updated_optimlist = make_pexl_Optims(optims, NULL);
  set_last_optim(updated_optimlist, PEXL_OPT_TRO);
  return updated_optimlist;
}

bool optimlist_is_valid (pexl_Optims *opt) {
  bool saw_peephole = false;
  while( opt ) {
    switch (opt->type) {
    case PEXL_OPT_PEEPHOLE: {
      saw_peephole = true;
      break;
    }
    case PEXL_OPT_INLINE: case PEXL_OPT_UNROLL: {
      if (saw_peephole) {
	warn("optimizer",
	     "PEEPHOLE pass cannot be run before INLINE or before UNROLL");
	return false;
      }
      break;
    }
    default: break;
    }
    opt = opt->next;		// traverse to the next node
  }
  return true;
}

void free_optims (pexl_Optims *optims) {
  pexl_Optims *cur = optims;
  while (cur) {
    pexl_Optims *tmp = cur;		// keep access to the rest of the list
    cur = cur->next;		// traverse to the next node
    if (tmp->params)
      free(tmp->params);	// free the optional params
    free(tmp);			// free the target node
  }
}

bool optimlist_contains (pexl_Optims *optims, enum pexl_OptimType optim_type) {
  if (optims)
    for (pexl_Optims *i = optims; i; i = i->next)
      if(i->type == optim_type) return true;
  return false;
}

/*
  Adjust the SymbolTable after changing the code size of a pattern, by
  iterating through the symbol table.  For each entry, the 'value'
  field is the entrypoint.  The entrypoint for any pattern after
  the one we just expanded (or shrunk?) needs to be recalculated.
*/
void adjust_symbol_table (pexl_Binary *pkg, uint32_t i, int32_t delta) {
    
    SymbolTableEntry *ste = NULL;
    pexl_Index prev = PEXL_ITER_START;

    trace("opt", "Adjusting symbol table entries by %d around addr %u", delta, i);

    ste = symboltable_iter_ns(pkg->symtab, symbol_ns_id, &prev);

    while(ste != NULL){
      if (ste->value <= ((int32_t) i) && (ste->value + ste->size - 1 >= i)) {
      /* Case 1: Found the pattern we extended

         |---------|   <--original pattern
         |---------|++++++++++|   <--extended by optimizer
	           ^
	           i
      */ 
	ste->size += delta;
      }
      else if (ste->value > ((int32_t) i)) {
      /* Case 2: Found a pattern that starts after the original end,
	 i, of the pattern we extended

                   |------------|  <--this pattern
        |-------|+++++|      <--extended pattern
                ^
	        i
      */ 
	ste->value += delta;
      }
      ste = symboltable_iter_ns(pkg->symtab, symbol_ns_id, &prev);
    }
}

/*
  Adjust the offset of the Jump/Call type instructions in the code
  vector after extending, e.g. inlining or unrolling.  Jumps are
  RELATIVE.  Calls are to ABSOLUTE addresses.  Let delta be the amount
  of code we added due to inlining/unrolling.

 |-------------------------------|
 | 0: first instruction          | <--- 1. Jumps ahead (+) to after inline_start
 | ...                           |         need delta added to them
 | other code                    |      2. Jumps behind (-) are not modified
 | ...                           |      3. Calls to after inline_start need to have
 |-------------------------------|         delta added to them
  ** inline_start: (was ICall) **
  ** ...                       **  <--- Assert: jumps stay within inlined region
  ** last inlined instruction  **          (Basic block assumption!)
 |-------------------------------|
 | inline_start+delta:           | <--- 1. Jumps ahead (+) are not modified
 | ...                           |      2. Jumps behind (-) to last inlined instruction
 | other code                    |         or earlier need delta subtracted
 | ...                           |      3. Calls to after inline_start need to have
 |-------------------------------|         delta added to them
   pkg->codenext: (end of code)

*/
void adjust_ins_offset (pexl_Binary *pkg, uint32_t inline_start, int32_t delta) {
    Instruction *start = pkg->code; /* start address */
    Instruction *pc = NULL;
    uint32_t abs_addr, new_offset;
    uint32_t idx = 0;
    uint32_t size_of_ICall = OPCODE_SIZE[ICall];
    uint32_t inline_end = inline_start + delta + size_of_ICall; /* one PAST the end */

    assert(inline_end > inline_start);

    WHEN_TRACING {
      fprintf(stderr, "[adjust] Adjusting jumps due to newly extended block [%d, %d]\n",
	      inline_start, inline_start+delta+size_of_ICall);
      fprintf(stderr, "          Block ends with %s at addr %d\n",
	      OPCODE_NAME[opcode(&start[inline_start+delta+size_of_ICall])],
	      inline_start+delta+size_of_ICall);
    }
    while(idx < pkg->codenext){
        pc = &start[idx];
        switch(opcode(pc)){
	case ICall: {
	  /* Call to absolute address */
	  if (((uint32_t) getaddr(pc)) > inline_start)
	    setaddr(pc, getaddr(pc) + delta);
	  break;
	}
	  /* Jump relative to PC */
	case IJmp: case IChoice:
	case ICommit: case IPartialCommit:
	case ITestAny: case ITestChar: case ITestSet: {
	  abs_addr = relative_to_abs(start, pc);
	  /* CASE 1: Current index is BEFORE inline_start */
	  if (idx < inline_start) {
	    /* If the jump destination is after inline_start, adjust it */
	    if (abs_addr > inline_start) { 
	      new_offset = getoffset(pc) + delta; 
	      setoffset(pc, new_offset); 
	    }
	  } else if ((idx >= inline_start) && (idx < inline_end)) {
	    /* Case 2: Current index is WITHIN the inlined block.
	       Since we only inline patterns listed in the symbol
	       table, all jumps should remain within this inlined
	       block, because jumps are relative.
	    */
	    if ((abs_addr < inline_start) || (abs_addr > inline_end)) {
	      confess("inliner", "Jump out of inlined block [%d, %d] in %s (%d) at %d\n",
		      inline_start, inline_end, OPCODE_NAME[opcode(pc)],
		      getoffset(pc), idx);
	      fprint_binary(stderr, pkg);
	    }

	  } else {
	    /* Case 3: Current index is AFTER the inlined block */
	    assert(idx >= inline_end);
	    /* Jumping to an addr after or inside the inlined block:
	       Need to adjust the jump offset because jumps are relative. */
	    if (abs_addr < inline_end) {
	      new_offset = getoffset(pc) - delta; 
	      setoffset(pc, new_offset); 
	    } 
	  } /* cases of idx */
	  break;
	} /* all jump instructions */
	default: break;
	} /* switch on opcode */
	idx += sizei(pc);
    } /* while */
}

Instruction *copy_instructions (pexl_Binary *pkg, pexl_Index start, pexl_Index size) {
  if ((start < 0) || (size <= 0)) {
    warn("optimizer", "bad start or size arg to copy_instructions");
    return NULL;
  }
  Instruction *buffer = malloc(size * sizeof(Instruction));
  if (!buffer) {
    warn("optimizer", "out of memory");
    return NULL;
  }
  memcpy(buffer, pkg->code + start, size * sizeof(Instruction));
  return buffer;
}

// Return PEXL_OK (0) for success, else !PEXL_OK (1)
int move_instructions (pexl_Binary *pkg,
		       pexl_Index src,
		       pexl_Index dest,
		       pexl_Index size) {
  if ((src < 0) || (dest < 0) || (size <= 0)) {
    warn("optimizer", "bad src, dest, or size arg to move_instructions");
    return !PEXL_OK;
  }
  // Because the memory ranges may overlap, we use memmove.  Not worth
  // checking to see if using memcpy is possible.
  memmove(&pkg->code[dest],
	  &pkg->code[src],
	  size * sizeof(Instruction));
  return PEXL_OK;
}
