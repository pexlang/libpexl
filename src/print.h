/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  print.h                                                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef print_h
#define print_h

#include "preamble.h"
#include "libpexl.h"

#include "compile.h"
#include "match.h"
#include "symboltable.h" 
#include "env.h" 
#include "binary.h"
#include "expression.h" 
#include "pattern.h" 
#include "instruction.h"
#include "ast.h"
#include "context.h"
#include "vm.h"			/* pexl_Match type */

// See also the exported print functions in libpexl.h

void printnode(Node *node, int indent, pexl_Context *C);
void print_match_tree (pexl_MatchTree *m);

void print_symboltable_stats (SymbolTable *st);
void print_symboltable (SymbolTable *st);

void print_BindingTable (pexl_Context *C);
void print_BindingTable_stats (pexl_Context *C);

// Context arg is optional for the print_env_internal:
void print_env_internal (pexl_Env env, BindingTable *bt, pexl_Context *C);
void print_env_tree (pexl_Context *C);

void fprint_binary (FILE *channel, pexl_Binary *p);
void print_binary_attributes (pexl_BinaryAttributes *attrs);
void print_importtable (pexl_Binary *p);

void print_packagetable_stats (pexl_PackageTable *pt);
void print_packagetable (pexl_PackageTable *pt);

void print_charset (uint32_t *st);
void fprint_charset (FILE *channel, uint32_t *st);
void print_simdcharset(uint32_t *cs);
void fprint_simdcharset(FILE *channel, uint32_t *cs);
void print_charsettable(CharsetTable *cst);
void fprint_charsettable(FILE *channel, CharsetTable *cst);

void print_pattern (Pattern *p, int indent, pexl_Context *C);
void print_pattern_metadata (Pattern *p, int indent);

void print_cfgraph (CFgraph *g);
void print_sorted_cfgraph (CFgraph *g);

void print_value (Value V, int indent, pexl_Context *C);

int fprint_instruction (FILE *channel,
			const pexl_Binary *pkg,
			const Instruction *p);

int print_instructions (const pexl_Binary *pkg,
			const Instruction *p,
			int codelength);

int fprint_instructions (FILE *channel,
			 const pexl_Binary *pkg,
			 const Instruction *start,
			 int codelength);

/* Pretty printer for match tree is declared in libpexl.h */
void print_match_tree_flat (pexl_MatchTree *m);
void print_match_data (pexl_Match *m);
void print_match_summary (pexl_Match *match);

#endif
