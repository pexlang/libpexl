/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast-typecheck.h   Typecheck AST                                          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(ast_typecheck_h)
#define ast_typecheck_h

#include "preamble.h"
#include "libpexl.h"
#include "ast.h"

#define CODEPOINT_MAX 0x10FFFF

bool typecheck(ast_expression *exp, ast_expression **violation);
bool typecheck_exp(ast_expression *exp, ast_expression **violation);
bool typecheck_def(ast_expression *exp, ast_expression **violation);
bool typecheck_mod(ast_expression *exp, ast_expression **violation);


#endif
