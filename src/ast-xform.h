//  -*- Mode: C; -*-                                                         
//                                                                          
//  ast-xform.h                                                                    
//                                                                          
//  See files LICENSE and COPYRIGHT, which must accompany this file          
//  AUTHORS: Jamie A. Jennings, Jack Deucher                                        

#ifndef ast_xform_h
#define ast_xform_h

#include "preamble.h"
#include "libpexl.h"
#include "ast.h"

// Convenient way to generate a transformerReturn type:
#define TRet(exp, action) ((transformerReturn) {(exp), (action)})

typedef enum {
  AST_XFORM_REPLACE,            // replace and continue
  AST_XFORM_NOREPLACE,          // do not replace, but continue
  AST_XFORM_REPLACE_STOP,       // replace, do not continue
  AST_XFORM_STOP,               // do not replace, do not continue
  AST_XFORM_ERROR               // do not continue, results invalid
} ast_xform_code;


typedef struct transformerReturn {
  ast_expression *exp;
  ast_xform_code code;
} transformerReturn;

typedef transformerReturn (* transformerfn)(ast_expression *, void *);

//transformerReturn convert_UTF8 (ast_expression * ce_ptr, void * context);

transformerReturn donotmuch (ast_expression * exp, void * context);
transformerReturn copyAST_function (ast_expression * exp, void * context);
transformerReturn findNode (ast_expression * exp, void * context);
transformerReturn replaceFind (ast_expression * exp, void * context);

// A general purpose tree walker. Requires two components to work, a function that follows the transformer structure and context.
// Context is only needed if you want the tree walker to return something special or take special parameters.
ast_expression *transform_AST (ast_expression * exp, transformerfn function, void * context);

int map_AST (ast_expression *exp, transformerfn function, ast_expression **result, void *context);

ast_expression *copyAST (ast_expression *exp);
ast_expression *compareAST (ast_expression *exp1, ast_expression *exp2);

// New API to use for transforming one AST into another
ast_expression *transform(ast_expression *exp,
			  bool matcher(ast_expression *, void *),
			  ast_expression *transformer(ast_expression *, void *),
			  void *context);

ast_expression *transform_codepoints(ast_expression *exp);

#endif
