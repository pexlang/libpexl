/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp-writer.c   S-expression writer                                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "sexp-writer.h"
#include "k.h"

static int concat(StringBuffer *buf, const string *str) {
  return StringBuffer_add_string(buf, str);
}

/* ----------------------------------------------------------------------------- */
/* Printing                                                                      */
/* ----------------------------------------------------------------------------- */

static void concat_bytestring_escape(StringBuffer *buf, const string *str, size_t len) {
  string *tmp = string_escape(str, len);
  concat(buf, tmp);
  free(tmp);
}
			  
static string *write_byte(pexl_Byte b) {
  StringBuffer *buf = StringBuffer_new(0);
  StringBuffer_add_string(buf, "#\\");
  int i = 0;
  while (character_names[i]) {
    if (b == character_name_values[i]) {
      StringBuffer_add_string(buf, character_names[i]);
      break;
    }
    i++;
  } // while
  if (!character_names[i]) {
    if (printable_bytep(b)) {
      StringBuffer_add_char(buf, b);
    } else {
      StringBuffer_add_char(buf, 'x');
      StringBuffer_add_char(buf, to_hex1(b >> 4));
      StringBuffer_add_char(buf, to_hex1(b));
    }
  }
  string *s = StringBuffer_tostring(buf);
  StringBuffer_free(buf);
  return s;
}

// This is simple, maintainable, and fast.  Dependencies:
// (1) C will cast the argument type of to_hex1() to a char.
// (2) The pexl_Codepoint type is at least 32 bits.
string *write_codepoint(pexl_Codepoint cp) {
  if (cp < 256) return write_byte(cp);
  string *s = calloc(12, 1);
  if (!s) return NULL;
  s[0] = '#'; s[1] = '\\';
  char c = cp & 0xFF;
  if (cp < 0x100) {
    if (printablep(&c)) {
      s[2] = c;
    } else {
      s[2] = 'x';
      s[3] = to_hex1(cp >> 4);
      s[4] = to_hex1(c);
    }
  } else if (cp < 0x10000) {
    s[2] = 'u';
    s[3] = to_hex1(cp >> 12);
    s[4] = to_hex1(cp >> 8);
    s[5] = to_hex1(cp >> 4);
    s[6] = to_hex1(c);
  } else {
    s[2] = 'U';
    s[3] = to_hex1(cp >> 28);
    s[4] = to_hex1(cp >> 24);
    s[5] = to_hex1(cp >> 20);
    s[6] = to_hex1(cp >> 16);
    s[7] = to_hex1(cp >> 12);
    s[8] = to_hex1(cp >> 8);
    s[9] = to_hex1(cp >> 4);
    s[10] = to_hex1(c);
  }
  return s;
}

string *write_int64(int64_t val) {
  string *s = malloc(MAX_INT_LEN + 1);
  int stat = snprintf(s, MAX_INT_LEN + 1, "%" PRId64, val);
  return (stat < 0) ? NULL : s;
}

static const string *last_esc(const string *start, uint32_t len) {
  const string *end = start + len;
  while (end-- >= start)
    if (*end == '\\') return end;
  return start;
}

string *write_token_repr(Token tok) {
  StringBuffer *buf = StringBuffer_new(0);
  if (!buf) {
    warn("reader", "Out of memory");
    return NULL;
  }
  concat(buf, "[");
  concat(buf, TOKEN_NAMES[tok.type]);
  switch (tok.type) {
    case TOKEN_OPEN:
    case TOKEN_CLOSE:
    case TOKEN_EOF:
      break;
    case TOKEN_WS:
    case TOKEN_IDENTIFIER:
    case TOKEN_BYTESTRING:
    case TOKEN_COMMENT:
    case TOKEN_INT:
    case TOKEN_CHAR:
    case TOKEN_BAD_IDENTIFIER:
    case TOKEN_BAD_BYTESTRING: 	// Unterminated bytestring
    case TOKEN_BAD_CHAR:
    case TOKEN_BAD_INT:
    case TOKEN_BAD_LEN:
    case TOKEN_BAD_SHARP:
      concat(buf, " '");
      concat_bytestring_escape(buf, tok.start, tok.len);
      concat(buf, "'");
      break;
    case TOKEN_BAD_ESC: {
      const string *esc = last_esc(tok.start, tok.len);
      concat(buf, " ");
      concat_bytestring_escape(buf, esc, tok.len - (esc - tok.start));
      break;
    }
    case TOKEN_PANIC:		// Internal error -- this is a bug!
      break;
    default:
      concat(buf, " INVALID TOKEN TYPE");
  }
  concat(buf, "]");
  string *result = StringBuffer_tostring(buf);
  StringBuffer_free(buf);
  return result;
}

KTYPES(K_WRITE_CDR);
KDATATYPE(Sexp *, newstack, push, data, setdata);

/* ----------------------------------------------------------------------------- */
/* For debugging use only: print S-expression representation                     */
/* ----------------------------------------------------------------------------- */

void print_sexp_repr(Sexp *e) {
  string *tmp;
  if (!e) {
    printf("error: null argument\n");
    return;
  }
  printf("[%s", SEXP_NAMES[e->type]);
  switch (e->type) {
    case SEXP_ERROR:
      printf(" %s", token_type_name(e->error.type));
      tmp = String_escape(e->error.data);
      printf(" \"%s\"", tmp);
      free(tmp);
      break;
    case SEXP_IDENTIFIER:
      printf(" %.*s", (int) String_len(e->str), String_ptr(e->str));
      break;
    case SEXP_INT:
      printf(" %" PRId64, e->n);
      break;
    case SEXP_BYTESTRING:
      tmp = String_escape(e->str);
      printf(" %zu \"%s\"", String_len(e->str), tmp);
      free(tmp);
      break;
    case SEXP_CHAR:
      tmp = write_codepoint(e->codepoint);
      printf(" %s", tmp);
      free(tmp);
      break;
    case SEXP_CONS:
      printf(" ");
      print_sexp_repr(e->car);
      printf(" ");
      print_sexp_repr(e->cdr);
      break;
    case SEXP_INCOMPLETE:
    case SEXP_EXTRACLOSE:
    case SEXP_NULL:
    default:
      break;
  }
  printf("]");
}

static void write_sexp_k(Sexp *e, StringBuffer *buf, Kstack *ks) {
  string *tmp;
 loop:
  switch (e->type) {
    case SEXP_CONS:
      concat(buf, "(");
      push(ks, K_WRITE_CDR, e->cdr);
      e = e->car;
      goto loop;
    case SEXP_IDENTIFIER:
      StringBuffer_add_String(buf, e->str);
      break;
    case SEXP_INT:
      tmp = write_int64(e->n);
      concat(buf, tmp);
      free(tmp);
      break;
    case SEXP_BYTESTRING:
      tmp = String_escape(e->str);
      concat(buf, "\"");
      StringBuffer_add_string(buf, tmp);
      concat(buf, "\"");
      free(tmp);
      break;
    case SEXP_CHAR:
      tmp = write_codepoint(e->codepoint);
      concat(buf, tmp);
      free(tmp);
      break;
    case SEXP_NULL:
      concat(buf, "()");
      break;
    case SEXP_INCOMPLETE:
    case SEXP_EXTRACLOSE:
      concat(buf, SEXP_NAMES[e->type]);
      break;
    case SEXP_ERROR:
      concat(buf, "(error ");
      StringBuffer_add_string(buf, token_type_name(e->error.type));
      tmp = String_escape(e->error.data);
      concat(buf, " \"");
      StringBuffer_add_string(buf, tmp);
      concat(buf, "\")");
      free(tmp);
      break;
    default:
      concat(buf, "(error \"Unknown S-expression type\" ");
      tmp = write_int64(e->type);
      StringBuffer_add_string(buf, tmp);
      free(tmp);
      concat(buf, ")");
      break;
  }
  return;
}

/* ----------------------------------------------------------------------------- */
/* Public API                                                                    */
/* ----------------------------------------------------------------------------- */

string *write_sexp(Sexp *exp) {
  if (!exp) {
    warn("reader", "Null S-expression argument\n");
    return NULL;
  }
  StringBuffer *buf = StringBuffer_new(0);
  if (!buf) {
    warn("reader", "Out of memory");
    return NULL;
  }
  Kontinuation *k;
  Kstack *ks = newstack(NULL);
  write_sexp_k(exp, buf, ks);
  while ((k = K_top(ks))) {
    if (k->type != K_WRITE_CDR) {
      warn("reader", "Unknown continuation type %d", k->type);
      abort();
    }
    if (sexp_nullp(data(k))) {
      concat(buf, ")");
      K_pop(ks);
    } else if (sexp_consp(data(k))) {
      concat(buf, " ");
      exp = data(k);
      K_pop(ks);
      push(ks, K_WRITE_CDR, exp->cdr);
      write_sexp_k(exp->car, buf, ks);
    } else {
      // This will not happen if we build only proper lists.
      // Improper lists end in a "dotted pair" in Lisp/Scheme terminology.
      concat(buf, " . ");
      exp = data(k);
      K_pop(ks);
      write_sexp_k(exp, buf, ks);
      concat(buf, ")");
    }
  }
  string *result = StringBuffer_tostring(buf);
  StringBuffer_free(buf);
  Kstack_free(ks);
  return result;
}

void print_sexp(Sexp *e) {
  string *tmp = write_sexp(e);
  printf("%s", tmp);
  free(tmp);
}

