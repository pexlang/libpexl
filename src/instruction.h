/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  instruction.h   definitions needed by both compiler and runtime          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef instruction_h
#define instruction_h

#include "preamble.h"
#include "libpexl.h"
#include "charset.h"

/*
  An instruction is one or more 32-bit words.

  A one-word instruction contains an opcode and maybe an operand in
  the aux field.  Multi-word instructions start with a word containing
  the opcode (and maybe something in aux) but are followed by other
  words containing operands.

  Quick reference:

  IGiveup:             vm stops with match failure (used only in trampoline)
  IEnd:                end of pattern, vm stops with successful match
  IFail:               pop backtrack stack, jump to offset saved there
  INoop:               no operation

  IAny:                move current position ahead by 1 (consume any byte)
  IChar:               if char == 'aux' then advance else fail
  ISet:                if char not in charset indexed by 'aux', fail
  ISpan:               read a span of chars from charset indexed by 'aux'

  ITestAny:            if no chars left, jump to 'offset'
  ITestChar:           if char != 'aux', jump to 'offset'
  ITestSet:            if char not in charset indexed by 'aux', jump to 'offset'

  IBehind:             move current position back by 'aux' characters

  IChoice:             push choice frame incl. 'offset' (next fail jumps there)
  ICommit:             pop choice or findloop frame and jump to 'offset'
  IPartialCommit:      update top choice frame to current position and jump

  IOpenCapture:        push an open frame to capture stack
  ICloseCapture:       push close capture marker (no data) onto capture stack
  ICloseConstCapture:  push close const capture, operand is symbol table index of value

  ICall:               call 'offset' in current code vector
  IXCall:              jump to entrypoint 'offset' in package number 'aux'
  IRet:                return from a call or xcall
  IJmp:                jump to 'offset'

  ILookAhead:          push lookahead stack frame
  IFailNot:            fail negative lookahead
  ILookAheadCommit:    pop lookahead frame, restore position

  ILookBehind:         push lookbehind frame to backtrack stack
  ILookBehindCommit:   check backloop end position, fail or commit

  IFindLoop:           push findloop frame incl. 'offset' (next fail jumps there)
  IFind1:              skip ahead to given byte value; save position in BTFindLoop frame
  IFind8:              skip ahead to byte in given set; save position in BTFindLoop frame
  IFind:               skip ahead to byte in given set; save position in BTFindLoop frame

  IBackref:            match same data as prior capture (key in 'offset')

*/


/* ----------------------------------------------------------------------------- */
/* pexlCaptures                                                                      */
/* ----------------------------------------------------------------------------- */

/* We will use 8 bits to store the capture kind, with bit 8 reserved
 * to indicate a closing capture, leaving 0-127 for arbitrary captures
 * and 128-255 for close captures.
 */
typedef enum CapKind {
  /* Opening capture kinds start at 0 (high bit is clear) */
  Copen = 0,
  Cconstant,
  /* Closing capture kinds start at 128 (high bit is set) */
  Cclose = 0x80,
  Ccloseconst
} CapKind;

static const char *const OPEN_CAPTURE_NAMES[] = {
    "Common",
    "Constant",
    // Change pexlCaptureName definition after adding more names here
};
static const char *const CLOSE_CAPTURE_NAMES[] = {
    "CloseCommon",
    "CloseConstant",
    // Change pexlCaptureName definition after adding more names here
};

#define pexlCaptureName(c) (((c)&0x80) ? CLOSE_CAPTURE_NAMES[(c)&0x1] : OPEN_CAPTURE_NAMES[(c&0x1)])

/*
  The MAX was set due to size of the aux field in an instruction,
  which is 24 bits.  But we store addresses in a separate 32-bit word,
  i.e. we use a 2-word instruction when there is an address in it.

  TODO: Check to see if we ever store an address in a 24 bit field.
  Either way, rewrite this comment.
*/
#define MAX_INSTRUCTIONS ((1 << 24) - 1)

#define opcode(pc) ((pc)->i.code)
#define setopcode(pc, op) ((pc)->i.code) = (op)

/* Absolute addresses are stored "as is" */
#define getaddr(pc) ((pc + 1)->operand)
#define setaddr(pc, addr) do {			\
    (pc + 1)->operand = (addr);			\
  } while (0);
/* Jump offsets are stored off by 2 so that we don't have to do it at runtime */
#define getoffset(pc) (((pc + 1)->operand) + 2)
#define setoffset(pc, addr) do {		\
    (pc + 1)->operand = (addr) - 2;		\
  } while (0);

#define setinstword(pc, val) (pc)->operand = (val)

/* Convenient ways of accessing the aux field: */
#define aux(pc) ((pc)->i.aux)
#define signedaux(pc) ((aux((pc)) & 0x800000) ? (int)(aux((pc)) | 0xFF000000) : aux((pc)))
#define setaux(pc, idx) (pc)->i.aux = ((idx)&0x00FFFFFF)
#define ichar(pc) ((pc)->i.aux & 0xFF)
#define setichar(pc, c) (pc)->i.aux = ((pc)->i.aux & 0xFFFF00) | (c & 0xFF)

/* Prepare/test 32-bit and 64-bit signed values for storing in aux */
#define prepsignedaux(val) ((val)&0x00FFFFFF)
#define checksignedaux(val) \
  (((val) >= 0) ? (((val)&0x007FFFFF) == (val)) \
   : ((((int64_t)(val)) & 0xFFFFFFFFFF800000) == 0xFFFFFFFFFF800000))

typedef struct CodeAux
{
  uint8_t code;          /* 8-bit opcode */
  unsigned int aux : 24; /* 24-bit aux field: [0 .. 16,777,215] env index, or other operands, like chars */
} CodeAux;

typedef union Instruction
{
  CodeAux i;        /* opcode and aux field packed into a 32-bit word */
  int32_t operand;  /* follows an opcode that needs a 32-bit value */
} Instruction;

/* ----------------------------------------------------------------------------- */
/* Instructions                                                                  */
/* ----------------------------------------------------------------------------- */

#define _InstructionList(X)                                                 \
  /* No operands */                                                         \
  X(IFail, "fail", 1, &&LABEL_IFail)        /*** MUST BE FIRST! ***/        \
  X(IGiveup, "giveup", 1, &&LABEL_IGiveup)                                  \
  X(INoop, "noop", 1, &&LABEL_INoop)                                        \
  X(IAny, "any", 1, &&LABEL_IAny)                                           \
  X(IRet, "ret", 1, &&LABEL_IRet)                                           \
  X(IEnd, "end", 1, &&LABEL_IEnd)                                           \
  X(IFailNot, "failnot", 1, &&LABEL_IFailNot)                               \
  X(ILookAhead, "lookahead", 1, &&LABEL_ILookAhead)                         \
  X(ILookAheadCommit, "lookaheadcommit", 1, &&LABEL_ILookAheadCommit)       \
  X(ICloseCapture, "closecapture", 1, &&LABEL_ICloseCapture)		    \
  X(IWiden, "widen", 1, &&LABEL_IWiden)					    \
  /* Aux operand */                                                         \
  X(IBehind, "behind", 1, &&LABEL_IBehind)                                  \
  X(IChar, "char", 1, &&LABEL_IChar)                                        \
  X(IFind1, "find1", 1, &&LABEL_IFind1)					    \
  /* Charset (table index stored in aux field)  */                          \
  X(IFind, "find", 1, &&LABEL_IFind)                                        \
  X(IFind8, "find8", 1, &&LABEL_IFind8)                                     \
  X(ISet, "set", 1, &&LABEL_ISet)                                           \
  X(ISpan, "span", 1, &&LABEL_ISpan)                                        \
  /* Offset (second word) operand */                                        \
  X(ICall, "call", 2, &&LABEL_ICall)                                        \
  X(ICloseConstCapture, "closeconstcapture", 2, &&LABEL_ICloseConstCapture) \
  X(IBackref, "backref", 2, &&LABEL_IBackref)                               \
  X(ITestAny, "testany", 2, &&LABEL_ITestAny)                               \
  X(IJmp, "jmp", 2, &&LABEL_IJmp)                                           \
  X(IChoice, "choice", 2, &&LABEL_IChoice)                                  \
  X(IFindLoop, "findloop", 2, &&LABEL_IFindLoop)                            \
  X(ICommit, "commit", 2, &&LABEL_ICommit)                                  \
  X(ILookBehindCommit, "lookbehindcommit", 2, &&LABEL_ILookBehindCommit)    \
  /* Offset and aux operands */                                             \
  X(IPartialCommit, "partialcommit", 2, &&LABEL_IPartialCommit)             \
  X(IOpenCapture, "opencapture", 2, &&LABEL_IOpenCapture)                   \
  X(ITestChar, "testchar", 2, &&LABEL_ITestChar)                            \
  X(IXCall, "xcall", 2, &&LABEL_IXCall)                                     \
  X(ILookBehind, "lookbehind", 2, &&LABEL_ILookBehind)                      \
  X(INarrowToSpan, "narrow to span", 2, &&LABEL_INarrowToSpan) 		    \
  /* Offset and charset operands */                                         \
  X(ITestSet, "testset", 2, &&LABEL_ITestSet)                               \
  /* Offset, aux and charset operands */                                    \
  /* none (so far) */                                                       \
  X(Iunused34, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused35, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused36, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused37, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused38, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused39, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused40, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused41, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused42, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused43, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused44, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused45, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused46, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused47, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused48, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused49, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused50, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused51, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused52, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused53, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused54, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused55, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused56, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused57, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused58, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused59, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused60, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused61, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused62, "illegal opcode", 1, &&BADOPCODE)			    \
  X(Iunused63, "illegal opcode", 1, &&BADOPCODE)

#define _FIRST(a, b, c, d) a,
typedef enum Opcode
{
  _InstructionList(_FIRST)
      PEXL_NUM_OPCODES /* sentinel */
} Opcode;
#undef _FIRST

#define _SECOND(a, b, c, d) b,
static const char *const OPCODE_NAME[] = {
    _InstructionList(_SECOND)};
#undef _SECOND

#define OPCODE_NAME(code) (OPCODE_NAME[(code)])

#define _THIRD(a, b, c, d) c,
static const int OPCODE_SIZE[] = {
  _InstructionList(_THIRD)
};
#undef _THIRD

#define _EXTRACT_VM_LABELS(a, b, c, d) d,
#define DECLARE_VM_LABELS			\
  static void *vm_labels[] = {			\
    _InstructionList(_EXTRACT_VM_LABELS)	\
  }

/* ----------------------------------------------------------------------------- */
/* Utilities for other modules                                                   */
/* ----------------------------------------------------------------------------- */

#define sizei(pc) (OPCODE_SIZE[opcode(pc)])

#endif
