#include "pathtable.h"


PathTable *pathtable_new(pexl_Index initial_size) {
  if (initial_size < 1)
    initial_size = PATHTABLE_INIT_SIZE;
  if (initial_size > PATHTABLE_MAX_SIZE)
    initial_size = PATHTABLE_MAX_SIZE;
  PathTable *pathtab = malloc(sizeof(PathTable));
  if (!pathtab)
    return NULL;
  pathtab->paths = malloc(initial_size * sizeof(Path));
  if (!pathtab->paths) {
    free(pathtab);
    return NULL;
  }
  pathtab->capacity = initial_size;
  pathtab->next = 0;
  return pathtab;
}

/**Operation to grow a pathtable at capacity. May need to go deeper at some point
*/
int pathtable_grow(PathTable *pathtab) {
  if (pathtab->capacity <= 0) {
    warn("pathtable", "Invalid capacity value (%d)", pathtab->capacity);
    return PEXL_ERR_INTERNAL;
  }
  pexl_Index newcap = pathtab->capacity * 2;
  Path **newpaths = realloc(pathtab->paths, newcap * sizeof(Path));
  if (!newpaths)
    return PEXL_ERR_OOM;
  pathtab->paths = newpaths;
  pathtab->capacity = newcap;
  return PEXL_OK;
}

// The 'path' given as an argument will becomes owned by the path
// table (it is not copied).  Caller must not free 'path'.
pexl_Index pathtable_add(PathTable *pathtab, Path *path) {
  int err;
  if (pathtab->next == pathtab->capacity) {
    if ((err = pathtable_grow(pathtab)))
      return err;
  }
  pexl_Index retval = pathtab->next;
  pathtab->paths[pathtab->next] = path;
  pathtab->next++;
  return retval;
}

// Copy a path, with the strings (not just the pointers)
static Path *path_copy(Path *p) {
  Path *new_path = malloc(sizeof(Path));
  if (!new_path) return NULL;
  int n = 0;
  // Count the segments in the path
  while (p->segments[n]) n++;
  // Allocate a new array the same size
  char **new_segments = malloc((n+1) * sizeof(char *));
  if (!new_segments) {
    free(new_path);
    return NULL;
  }
  // Copy the segments, which are strings
  for (int i = 0; i < n; i++) 
    new_segments[i] = string_dup(p->segments[i], PEXL_MAX_STRINGLEN);

  new_segments[n] = NULL;
  // Put the segments into the Path and return it
  new_path->segments = new_segments;
  return new_path;
}

PathTable *pathtable_copy(PathTable *pathtab) {
  int err;
  PathTable *copied_tab = pathtable_new(pathtab->next);
  if (!copied_tab) return NULL;
  for (int i = 0; i < pathtab->next; i++) {
    Path *copied_path = path_copy(pathtab->paths[i]);
    if (!copied_path) {
      warn("pathtable", "Path copy failed");
      pathtable_free(copied_tab);
      return NULL;
    }
    err = pathtable_add(copied_tab, copied_path);
    if (err) {
      warn("pathtable", "Pathtable add failed");
      pathtable_free(copied_tab);
      return NULL;
    }
  }
  return copied_tab;
}

void path_free(Path *path) {
  if (!path) return;
  
  assert(path->segments);
  char **segment = path->segments;
  while (*segment) {
    printf("%s\n", *segment);
    free((void *)*segment);
    segment++;
  } 
  free(path->segments);
  free(path);
}

void pathtable_free(PathTable *pathtab) {
  if (!pathtab)
    return;
  if (pathtab->paths)
    for (pexl_Index i = 0; i < pathtab->next; i++)
      path_free(pathtab->paths[i]);
  free(pathtab->paths);
  free(pathtab);
}

Path *break_path(const char *pathstring) {
  int end = 0;
  int start = 0;
  int cap = 5; /* max number of path segments */
  int i = 0;   /* segment number (index into table) */
  Path *path = (Path *)malloc(sizeof(Path));
  // path->size=0;
  path->segments = (char **)malloc((cap + 1) * (sizeof(char *)));
  int max_cap = strnlen(pathstring, PEXL_MAX_STRINGLEN + 1);
  while (end < max_cap) {
    /* Find either first break point or last char of input */
    while ((pathstring[end] != '/') && (end < max_cap - 1)) {
      end++;
    }
    if ((pathstring[end] == '/') || (end == max_cap - 1)) {
      /* Current path segment is from start to end-1 */
      if (i >= cap) {
        cap = cap + 5;
        path->segments =
            (char **)realloc(path->segments, (cap + 1) * (sizeof(char *)));
      }
      if (i < cap) {
        if (pathstring[end] == '/') {
          path->segments[i] = (char *)malloc((end - start + 1) * sizeof(char));
          memcpy(path->segments[i], pathstring + start, end - start);
          path->segments[i][end - start] = '\0';
          i++;
        } else {
          path->segments[i] = (char *)malloc((end - start + 2) * sizeof(char));
          assert(path->segments[i]);
          memcpy(path->segments[i], pathstring + start, end - start + 1);
          path->segments[i][end - start + 1] = '\0';
          i++;
        }
      }
      start = ++end;
    }
  } /* while */
  //path->segments[i] = (char *)malloc(sizeof(char *));
  path->segments[i] = NULL;
  // path->size = i;
  return path;
}
pexl_Index pathtable_break_and_add(const char *str, PathTable *pathtab){
  return pathtable_add(pathtab, break_path(str));

}

void print_path(Path *p) {
  if (!p) {
    printf("Path: NULL POINTER");
  }
  printf("Path: ");
  int i = 0;
  while (p->segments[i]) printf("/%s", p->segments[i++]);
  puts("");
}
