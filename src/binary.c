/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  binary.c   PEXL binary packages (executables)                            */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "binary.h"
#include "pathtable.h"

/* ----------------------------------------------------------------------------- */
/* Table of packages (binaries with some additional metadata                     */
/* ----------------------------------------------------------------------------- */

pexl_PackageTable *packagetable_new (size_t initial_capacity) {
  pexl_PackageTable *pt;
  if (initial_capacity == 0)
    initial_capacity = PACKAGETABLE_INITSIZE;
  if (initial_capacity > PACKAGETABLE_MAXSIZE) {
    initial_capacity = PACKAGETABLE_MAXSIZE;
    inform("packagetable",
	   "requested capacity (%zu) exceeds maximum of %d;"
	   " creating with maximum allowed capacity",
	   initial_capacity, PACKAGETABLE_MAXSIZE);
  }
  pt = malloc(sizeof(pexl_PackageTable));
  if (!pt) return NULL;
  assert( initial_capacity > 0 );
  pt->packages = malloc(initial_capacity * sizeof(pexl_Binary *));
  if (!pt->packages) {
    free(pt);
    return NULL;
  }
  pt->capacity = initial_capacity;
  pt->packages[0] = NULL;
  pt->next = 1;			/* Entry 0 reserved for vm use (user package) */
  return pt;
}

void packagetable_free_packages (const pexl_PackageTable *pt) {
  uint32_t i;
  if (pt && pt->packages)
    for (i = 0; i < pt->next; i++) binary_free(pt->packages[i]);
}

void packagetable_free (pexl_PackageTable *pt) {
  if (!pt) return;
  if (pt->packages) free(pt->packages);
  free(pt);
}

static int ensure_package_space (pexl_PackageTable *pt) {
  size_t newsize;
  void *temp;
  if (pt->next < pt->capacity) return PACKAGE_OK;
  if (((size_t) pt->capacity) >= PACKAGETABLE_MAXSIZE) return PEXL_ERR_FULL;
  newsize = 2 * pt->capacity;
  if (newsize > PACKAGETABLE_MAXSIZE) newsize = PACKAGETABLE_MAXSIZE;
  temp = realloc(pt->packages, newsize * sizeof(pexl_Binary));
  if (!temp) return PEXL_ERR_OOM;
  pt->packages = temp;
  pt->capacity = newsize;
  trace("ensure_package_space",
       "extending to %d packages, %d used ", pt->capacity, pt->next);
  return PACKAGE_OK;
}

/* Returns index (>= 0), or an error code < 0. */
int packagetable_add (pexl_PackageTable *pt, pexl_Binary *p) {
  int index;
  const char *importpath;
  if (!pt) {
    inform("packagetable_add", "null package table arg");
    return PEXL_ERR_NULL;
  }
  if (!p) {
    inform("packagetable_add", "null package arg");
    return PEXL_ERR_NULL;
  }
  if (package_importpath(p) == -1)
    return PACKAGE_ERR_IMPORTPATH;
  importpath = symboltable_get_name(p->symtab, package_importpath(p));
  if (!importpath) {
    inform("packagetable_add", "package importpath not in symbol table (package %p)", 
	 (void *) p);
    return PEXL_ERR_INTERNAL;
  }

  if (packagetable_lookup(pt, importpath)) return PACKAGE_ERR_EXISTS;

  /* At this point we have a unique importpath, which should guarantee
     a unique package.  However, a possible user error would be to add
     a package, change its importpath, and add it again.  This
     presents a problem for packagetable_free(), which would then free
     the same package twice.  So we check for that situation here. */
  for (index = 0; index < pt->next; index++)
    if (p == pt->packages[index]) {
      inform("packagetable_add", "package in table already, with a different importpath");
      return PEXL_ERR_INTERNAL;
    }

  if ((index = ensure_package_space(pt))) {
    assert( index < 0 );
    return index; /* error */
  }
  pt->packages[pt->next] = p;
  p->number = pt->next;
  pt->next++;
  return PACKAGE_OK;
}

pexl_Binary *packagetable_get (const pexl_PackageTable *pt, pexl_Index package_number) {
  if (!pt) {
    inform("packagetable_get", "null package table argument");
    return NULL;
  }
  if ((package_number < 0) || (package_number >= pt->next)) {
    warn("packagetable",
	 "package number %d out of range (%d packages currently in this table)",
	 package_number, pt->next);
    return NULL;
  }
  return pt->packages[package_number];
}

int packagetable_remove (const pexl_PackageTable *pt, pexl_Binary *p) {
  if (!pt || !p) return PEXL_ERR_NULL;
  assert( pt->packages );
  if (!pt->packages[p->number]) return PACKAGE_OK; /* idempotent */
  if (pt->packages[p->number] != p) return PACKAGE_NOT_FOUND;
  pt->packages[p->number] = NULL;
  return PACKAGE_OK;
}

static int name_eq (const char *n1, const char *n2) {
  assert( n1 );
  assert( n2 );
  if (strncmp(n1, n2, SYMBOLTABLE_MAXLEN + 1) == 0) return 1;
  return 0; 			/* NOT EQUAL */
}

/* Linear search by importpath.  Returns index >= 0, or error < 0. */

//    Does NOT examine the user package.
//    Returns 0 if not found, else package number (index into package
//    table) which is > 0 because we do not check the user package.  
pexl_Index packagetable_lookup (const pexl_PackageTable *pt, const char *importpath) {
  size_t i;
  pexl_Binary *p;
  const char *existing_importpath;
  if (!pt) {
    warn("packagetable_lookup", "null package table arg");
    return PEXL_ERR_NULL;
  }
  if (!importpath) importpath = ""; /* synonym for empty string */
  for (i = 0; i < pt->next; i++) {
    p = pt->packages[i];
    /* Be sure to skip NULL entries! */
    if (p) {
      existing_importpath = symboltable_get_name(p->symtab, package_importpath(p));
      if (!existing_importpath) {
	warn("packagetable",
	     "importpath for existing package %p not in its symbol table (%p)",
	     (void *) p, (void *) p->symtab);
	return PEXL_ERR_INTERNAL;
      }
      if (name_eq(importpath, existing_importpath)) return i;
    } /* if p */
  } /* for each package in table */
  return 0; /* not found */
}

/* Linear search for package (by the address of the package structure) */
pexl_Index packagetable_getnum (const pexl_PackageTable *pt, pexl_Binary *pkg) {
  pexl_Index i;
  if (!pt) {
    warn("packagetable", "null package table argument");
    return PEXL_ERR_NULL;
  }
  if (!pkg) return PACKAGE_NOT_FOUND; /* cannot look up NULL */
  for (i = 0; i < pt->next; i++) 
    if (pt->packages[i] == pkg) return i;
  return PACKAGE_NOT_FOUND;
}

/* ----------------------------------------------------------------------------- */
/* pexl_Binary                                                                       */
/* ----------------------------------------------------------------------------- */

pexl_Binary *binary_new (void) {
  pexl_Binary *p;
  p = malloc(sizeof(pexl_Binary));
  if (!p) return NULL;

  SymbolTable *st = symboltable_new(SYMBOLTABLE_INITSIZE, SYMBOLTABLE_INITBLOCKSIZE);
  if (!st) {
    free(p);
    return NULL;		/* already logged */
  }
  CharsetTable *cst = charsettable_new(CHARSETTABLE_INITSIZE);
  if (!cst) {
    symboltable_free(st);
    free(p);
    return NULL;		/* already logged */
  }
  p->symtab = st;
  p->pathtab= NULL;
  p->cstab = cst;
  p->origin = -1;
  p->code = NULL;		/* created during code generation */
  p->codenext = 0;
  p->codesize = 0;
  p->main = -1;			/* uninitialized */
  p->number = 0; 
  p->major = 0;
  p->minor = 0;
  p->imports = malloc(IMPORTTABLE_INITSIZE * sizeof(Import));
  if (!p->imports) {
    warn("binary_new", "out of memory");
    free(p);
    symboltable_free(st);
    return NULL;
  }
  p->import_size = IMPORTTABLE_INITSIZE;
  p->import_next = 1;
  // Entry 0 in the imports table describes THIS package
  p->imports[0].pkg = NULL;
  p->imports[0].importpath = -1; /* not yet set */
  p->imports[0].prefix = -1;	 /* not yet set */
  return p;
}

void binary_free (pexl_Binary *p) {
  if (!p) return;
  if (p->symtab) symboltable_free(p->symtab);
  if (p->pathtab) pathtable_free(p->pathtab);
  if (p->cstab) charsettable_free(p->cstab);
  if (p->code) free(p->code);
  if (p->imports) free(p->imports);
  free(p);
}

// Resize the code vector to exactly 'newsize'.
// Return true on success, false otherwise.
bool resize_binary (pexl_Binary *pkg, size_t newsize) {
  if (newsize >= MAX_INSTRUCTIONS) {
    warn("resize_binary",
	 "reached max instruction limit of %zu", (size_t)MAX_INSTRUCTIONS);
    return false;
  }
  Instruction *temp = realloc(pkg->code, newsize * sizeof(Instruction));
  if (!temp) {
    warn("resize_binary", "out of memory");
    return false;
  }
  pkg->code = temp;
  pkg->codesize = newsize;
  return true;
}


// Ensure the code vector can hold 'additional_size' more instructions.
// Return true on success, false otherwise.
bool ensure_binary_size (pexl_Binary *pkg, pexl_Index additional_size) {
  // First a consistency check: vector is full when next == size, so
  // next should never exceed size.
  assert(pkg->codenext <= pkg->codesize); 
  if (additional_size <= 0) return true;
  if ((pkg->codenext + additional_size) > pkg->codesize)
    return resize_binary(pkg, pkg->codesize * 2);
  return true;			// Success
}

SymbolTableEntry *binary_lookup_symbol (pexl_Binary *pkg,
					 Symbol_Namespace ns,
					 const char *symbolname) {
  SymbolTableEntry *entry;
  pexl_Index i = PEXL_ITER_START;
  const char *entryname;
  if (!pkg || !symbolname) {
    warn("binary_lookup_symbol", "null argument");
    return NULL;
  }
  while ((entry = symboltable_iter_ns(pkg->symtab, ns, &i))) {
    entryname = symboltable_entry_name(pkg->symtab, entry);
    if (!entryname) {
      warn("binary_lookup_symbol", "corrupt symbol table?");
      return NULL;
    } else if (strncmp(symbolname, entryname, SYMBOLTABLE_MAXLEN + 1)== 0)
      return entry;
  } /* while */
  return NULL;
}

static int add_to_symtab (pexl_Binary *p, const char *str, Symbol_Type type) {
  int stat;
  stat = symboltable_add(p->symtab, str,
			 symbol_local, symbol_ns_data, type,
			 0, 0, NULL);
  if (stat >= 0) return stat;	/* index into symtab */
  /* The symbol table errors below have already been logged */
  if (stat == PEXL_ERR_OOM) return PEXL_ERR_OOM; 
  if (stat == PEXL_ERR_FULL) return PEXL_ERR_FULL;
  if (stat == PEXL_ST__ERR_STRINGLEN) return PACKAGE_ERR_STRLEN; 
  assert(stat < 0);
  return PEXL_ERR_INTERNAL;
}

static int addstring (pexl_Binary *p, const char *str) {
  return add_to_symtab(p, str, symbol_type_string);
}

static int addimport (pexl_Binary *p, const char *str) {
  return add_to_symtab(p, str, symbol_type_import);
}

static int ensure_import_table_space (pexl_Binary *p) {
  size_t newsize;
  void *temp;
  if (p->import_next == p->import_size) {
    if (((size_t) p->import_size) >= IMPORTTABLE_MAXSIZE) {
      inform("binary_add_import", "import table full");
      return PEXL_ERR_FULL;
    }
    newsize = 2 * p->import_size;
    if (newsize > UINT32_MAX) return PEXL_ERR_FULL;
    if (newsize > IMPORTTABLE_MAXSIZE) newsize = IMPORTTABLE_MAXSIZE;
    temp = realloc(p->imports, newsize * sizeof(Import));
    if (!temp) {
      warn("binary_add_import", "out of memory");
      return PEXL_ERR_OOM;
    }
    p->imports = temp;
    p->import_size = (uint32_t) newsize;
  }
  return OK;
}

/* 
   Add to the list of imported packages.  If prefix is NULL, then the
   prefix entry will be -1, meaning "unset".  Returns the symbol table
   index if OK, else < 0 if error.
*/
int binary_add_import (pexl_Binary *p, const char *importpath, const char *prefix) {
  SymbolTableEntry *entry;
  pexl_Index index;
  int stat;
  if (!p || !importpath) return PEXL_ERR_NULL;
  index = PEXL_ITER_START;
  while ((entry = symboltable_iter_ns(p->symtab, symbol_ns_data, &index))) {
    if (entry->type == symbol_type_import)
      if (name_eq(importpath, symboltable_entry_name(p->symtab, entry))) {
	inform("binary_add_import",
	     "package '%s' already in symbol table as IMPORT",
	     importpath);
	return PACKAGE_ERR_EXISTS;
      }
  } /* while */
  stat = ensure_import_table_space(p);
  if (stat < 0) return stat;
  index = addimport(p, importpath);
  if (index < 0) return index;
  p->imports[p->import_next].pkg = NULL;
  p->imports[p->import_next].importpath = index;
  stat = prefix ? addstring(p, prefix) : -1;
  p->imports[p->import_next].prefix = index;
  p->import_next++;
  return index;
}

int binary_add_string (pexl_Binary *p, const char *str) {
  SymbolTableEntry *entry;
  pexl_Index index;
  if (!p || !str) return PEXL_ERR_NULL;
  index = PEXL_ITER_START;
  while ((entry = symboltable_iter_ns(p->symtab, symbol_ns_data, &index))) {
    if (entry->type == symbol_type_string)
      if (name_eq(str, symboltable_entry_name(p->symtab, entry)))
	return index;
  } /* while */
  return addstring(p, str);
}

#define ADDSTRING(pkg, str, type, idx) do {			\
    idx = add_to_symtab(pkg, str, type);			\
    if (idx == PACKAGE_ERR_STRLEN)				\
      return PEXL_COMPILER__ERR_LEN;				\
    if (idx == PEXL_ERR_OOM)					\
      return PEXL_ERR_OOM;					\
    if (idx == PEXL_ERR_FULL)					\
      return PEXL_ERR_FULL;					\
    if (idx < 0) 						\
      return PEXL_ERR_INTERNAL;					\
  } while (0);

int binary_set_attributes (pexl_Binary *p,
			    const char *importpath,
			    const char *prefix,
			    const char *origin,
			    const char *doc,
			    uint8_t major,
			    uint8_t minor) {
  pexl_Index idx;
  if (!p) {
    warn("binary_set_attributes", "null package argument");
    return PEXL_ERR_NULL;
  }
  /* Path in file system, a UNIQUE key in package table */
  if (importpath) {
    ADDSTRING(p, importpath, symbol_type_import, idx);
    p->imports[0].importpath = idx;
  } else {
    p->imports[0].importpath = -1; /* not set */
  }
  /* pexl_Binary prefix, appears in output (match tree node names) */  
  if (prefix) {
    ADDSTRING(p, prefix, symbol_type_string, idx);
    p->imports[0].prefix = idx;
  } else {
    p->imports[0].prefix = -1;	/* not set */
  }
  /* Origin is e.g. URL or filename (to support debugging) */ 
  if (origin) {
    ADDSTRING(p, origin, symbol_type_string, idx);
    p->origin = idx;
  } else {
    p->origin = -1;		/* not set */
  }
  /* Doc string (user provided, to support UX) */ 
  if (doc) {
    ADDSTRING(p, doc, symbol_type_string, idx);
    p->doc = idx;
  } else {
    p->doc = -1;		/* not set */
  }
  /* Minimum required language MAJOR version */
  /* Minimum required language MINOR version */
  p->major = major;
  p->minor = minor;
  return PEXL_OK;
}

static const char *binary_getstring (pexl_Binary *p, pexl_Index index) {
  if (!p) {
    warn("package", "null package argument");
    return NULL;
  }
  if (!p->symtab) return NULL;
  return symboltable_get_name(p->symtab, index);
}

/* Caller owns the return value and must free it */
pexl_BinaryAttributes *binary_get_attributes (pexl_Binary *p) {
  if (!p) {
    warn("binary_get_attributes", "null package argument");
    return NULL;
  }
  pexl_BinaryAttributes *attrs = malloc(sizeof(pexl_BinaryAttributes));
  if (!attrs) {
    warn("binary_get_attributes", "out of memory");
    return NULL;
  }
  attrs->importpath = binary_getstring(p, package_importpath(p));
  attrs->prefix = binary_getstring(p, package_prefix(p));
  attrs->origin = binary_getstring(p, p->origin);
  attrs->doc = binary_getstring(p, p->doc);
  attrs->major = p->major;
  attrs->minor = p->minor;
  return attrs;
}

void binary_free_attrs (pexl_BinaryAttributes *attrs) {
  if (attrs) free(attrs);
}

pexl_Index main_entrypoint (pexl_Binary *pkg) {
  assert(pkg);
  if (pkg->main < 0) {
    warn("main_entrypoint", "package has no main");
    return PEXL_NO_MAIN;
  }
  SymbolTableEntry *entry = symboltable_get(pkg->symtab, pkg->main);
  if (!entry) {
    warn("main_entrypoint", "no such entry in symbol table");
    return PEXL_ERR_INTERNAL;
  }
  return entry->value;
}

