/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast.h                                                                    */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Jack Deucher                                 */

#ifndef ast_h
#define ast_h

#include "preamble.h"
#include "libpexl.h"
#include "pstring.h"

typedef struct ast_byteset {
  union {
    uint8_t  i8[64];
    uint32_t i32[4];
    uint64_t i64[8];
  };
} ast_byteset;

typedef struct ast_codepointset {
  pexl_Codepoint *codepoints;
  uint32_t capacity;
  uint32_t size;
} ast_codepointset;

#define XAST_NODE_TYPES(X)                                  \
  X(AST_REPETITION,    "Repetition")		            \
  X(AST_CHAR,          "Char(codepoint)")                   \
  X(AST_ANYCHAR,       "Any(codepoint)")                    \
  X(AST_SET,           "Set(codepoint)")                    \
  X(AST_RANGE,         "Range(codepoint)")                  \
  /* All syntactic sugar MUST be above this line */         \
  X(AST_SUGAR,         "SENTINEL")                          \
							    \
  X(AST_TRUE,          "True")                              \
  X(AST_FALSE,         "False")                             \
  X(AST_IDENTIFIER,    "Identifier")                        \
  X(AST_BYTE,          "Byte")                              \
  X(AST_ANYBYTE,       "AnyByte")                           \
  X(AST_BYTESET,       "ByteSet")                           \
  X(AST_BYTERANGE,     "ByteRange")                         \
  X(AST_BYTESTRING,    "ByteString")                        \
  X(AST_SEQ,           "Sequence")                          \
  X(AST_CHOICE,        "Choice")                            \
  X(AST_STAR,          "Star")				    \
  X(AST_ATMOST,        "AtMost")			    \
  X(AST_DOTIMES,       "DoTimes")			    \
  X(AST_FIND,          "Find")                              \
  X(AST_CAPTURE,       "Capture")                           \
  X(AST_INSERT,        "Insert")                            \
  X(AST_BACKREF,       "Backref")                           \
  X(AST_LOOKAHEAD,     "LookAhead")			    \
  X(AST_NEGLOOKAHEAD,  "NegLookAhead")                      \
  X(AST_LOOKBEHIND,    "LookBehind")			    \
  X(AST_NEGLOOKBEHIND, "NegLookBehind")			    \
  /* All pattern expressions MUST be above this line */	    \
  X(AST_EXPRESSION,    "SENTINEL")                          \
							    \
  X(AST_DEFINITION,    "Definition")			    \
  X(AST_MODULE,        "Module")			    \
  X(AST_EXPLIST,       "ExpList")			    \
  X(AST_NUMBER,        "Number")			    \
  X(AST_ERROR,         "Error")				    \
  							    \
  X(AST_N,             "SENTINEL")

#define _FIRST(a, b) a,
typedef enum {
  XAST_NODE_TYPES(_FIRST)
} ast_type;
#undef _FIRST

#define _SECOND(a, b) b,
static const char *const AST_TYPE_NAMES[] = {
    XAST_NODE_TYPES(_SECOND)};
#undef _SECOND

#define ast_type_name(type) \
  (((0 <= (type)) && (type) <= AST_N) ? AST_TYPE_NAMES[(type)] : "INVALID")

typedef struct ast_explist {
  struct ast_expression *head;
  struct ast_explist *tail;
} ast_explist;

typedef struct ast_expression {
  ast_type type;
  // Optional child node
  struct ast_expression *child;
  // Data for the current node
  union {
    struct {			// byterange and codepointrange
      pexl_Codepoint low;
      pexl_Codepoint high;
    };
    struct {			// repetition
      pexl_Index min;
      pexl_Index max;
    };
    struct ast_expression *child2;   // seq and choice
    struct ast_explist    *defns;    // module
    ast_byteset           *byteset;
    String                *bytes;    // arbitrary byte string
    struct {
      String              *name;     // defn, identifier, capture, insert
      pexl_Index           idx;      // defn (symtab index)
    };
    pexl_Codepoint         codepoint;
    ast_codepointset      *codepointset;
    pexl_Number            n;	     // number
  };
} ast_expression;

/* ----------------------------------------------------------------------------- */
/* Expressions                                                                   */
/* ----------------------------------------------------------------------------- */

void free_expression (ast_expression *exp);

// True (epsilon) and False (no match)
ast_expression *make_true(void);
ast_expression *make_false(void);

// Codepoint expressions, encoded in UTF-32
ast_expression *make_char (uint32_t c);
ast_expression *make_any(void);
ast_expression *make_range (pexl_Codepoint low, pexl_Codepoint high);
ast_expression *make_codepointset (uint32_t capacity);
int  codepointset_add (ast_codepointset *set, pexl_Codepoint c);
bool codepointset_mem (ast_codepointset *set, pexl_Codepoint c);
pexl_Codepoint codepointset_iter (ast_codepointset *set, pexl_Index *prev);
bool codepointset_validp (ast_codepointset *set);
bool codepointset_emptyp (ast_codepointset *set);
bool codepointrange_emptyp (ast_expression *range);

// Unicode strings encoded in UTF-8
ast_expression *make_literal (String *str);      // interpolated
ast_expression *make_raw_literal (string *str);  // uninterpolated

// Byte expressions, not necessarily conforming to any encoding
ast_expression *make_byte (uint8_t b);
ast_expression *make_anybyte (void);
ast_expression *make_bytestring (String *data);
ast_expression *make_bytestring_from (const string *data, size_t len);
ast_expression *make_byterange (pexl_Byte low, pexl_Byte high);
ast_expression *make_byteset (void);
void byteset_add (ast_byteset *set, pexl_Byte c);
bool byteset_member (ast_byteset *set, pexl_Byte c);
bool byteset_emptyp (ast_byteset *set);
bool byterange_emptyp (pexl_Byte low, pexl_Byte high);
bool bytestring_emptyp (String *bytes);

// Identifiers
ast_expression *make_identifier (String *name);
ast_expression *make_identifier_using (String *name);
ast_expression *make_identifier_from (const string *name);

// Cominators: functions that combine or operate on expressions
ast_expression *make_seq (ast_expression *first, ast_expression *second);
ast_expression *build_seq(ast_expression *exp, ast_expression *newchild);
ast_expression *make_choice (ast_expression *first, ast_expression *second);
ast_expression *build_choice(ast_expression *exp, ast_expression *newchild);
ast_expression *make_repetition (pexl_Index min, pexl_Index max, ast_expression *exp);
ast_expression *make_predicate (ast_type type, ast_expression *exp);
ast_expression *make_find (ast_expression *exp);
ast_expression *make_backref (ast_expression *id);
ast_expression *make_capture (String *name, ast_expression *exp);
ast_expression *make_insert (String *name, String *data);
ast_expression *make_insert_internal (String *name, ast_expression *child);
ast_expression *make_call (const string *module, const string *name);
ast_expression *make_application (ast_expression *operator, ast_expression *args);

// Non-pattern types
ast_expression *make_module (ast_explist *defns);
ast_expression *make_definition (String *id, ast_expression *exp);
ast_expression *make_number (pexl_Number n);

/* ----------------------------------------------------------------------------- */
/* Misc                                                                          */
/* ----------------------------------------------------------------------------- */

// Predicates on expressions
bool ast_expressionp(ast_expression *exp);
bool ast_sugarp(ast_expression *exp);
bool ast_definitionp(ast_expression *exp);
bool ast_identifierp(ast_expression *exp);
bool ast_modulep(ast_expression *exp);
bool ast_numberp(ast_expression *exp);
bool ast_bytestringp(ast_expression *exp);
bool ast_errorp(ast_expression *exp);

// Lists of expressions (e.g. the definitions in a module)
void         free_explist(ast_explist *ls);
ast_explist *explist_cons(ast_expression *head, ast_explist *tail);
int          explist_len(ast_explist *ls);
ast_explist *explist_nreverse(ast_explist *ls);
ast_explist *map_explist(ast_expression *mapper(ast_expression *, void *),
			 ast_explist *ls,
			 void *context);

// Iterators and queries
int             ast_child_count(ast_expression *exp);
ast_expression *ast_child_iter(ast_expression *exp, ast_expression *prev);
ast_expression *ast_list_iter(ast_explist *ls, ast_expression *prev);

#endif // ifndef ast_h
