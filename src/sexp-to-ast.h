/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp-to-ast.h                                                            */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef sexp_to_ast_h
#define sexp_to_ast_h

#include "preamble.h"
#include "ast.h"
#include "sexp.h"

ast_expression *sexp_to_ast(Sexp *s);
Sexp           *ast_to_sexp(ast_expression *e);

#endif  // ifndef sexp_to_ast_h
