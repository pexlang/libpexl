/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast.c  Abstract Syntax Tree                                              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Jack Deucher                                 */

#include "ast.h"

static ast_expression *new (ast_type type) {
  ast_expression *exp = calloc(1, sizeof(ast_expression));
  if (!exp) return NULL;
  exp->type = type;
  return exp;
}

/*
  Nodes with no children
*/

ast_expression *make_true(void) {
  return new(AST_TRUE);
}

ast_expression *make_false(void) {
  return new(AST_FALSE);
}

ast_expression *make_number (pexl_Number n) {
  ast_expression *exp = new(AST_NUMBER);
  return (exp ? (exp->n = n, exp) : NULL);
}

ast_expression *make_anybyte (void) {
  return new(AST_ANYBYTE);
}

ast_expression *make_byte(uint8_t b) {
  ast_expression *exp = new(AST_BYTE);
  return (exp ? (exp->codepoint = b, exp) : NULL);
}

ast_expression *make_byteset (void) {
  ast_byteset *set = calloc(1, sizeof(ast_byteset));
  if (!set) return NULL;
  ast_expression *exp = new(AST_BYTESET);
  if (!exp) return NULL;
  exp->byteset = set;
  return exp;
}

// Add byte 'c' to 'set'
void byteset_add (ast_byteset *set, pexl_Byte c) {
  (set->i8)[(c) >> 3] |= (1 << ((c) & 7));
}

// Test membership of byte 'c' in 'set'
bool byteset_member (ast_byteset *set, pexl_Byte c) {
  return (set->i8)[((c) >> 3)] & (1 << ((c) & 7));
}

// Predicate: Is 'set' empty?
bool byteset_emptyp (ast_byteset *set) {
  for (int i = 0; i < 8; i++)
    if (set->i64[i]) return false;
  return true;
}

// NOTE: Range is inclusive.  If low > high, range is empty.
ast_expression *make_byterange (pexl_Byte low, pexl_Byte high) {
  ast_expression *exp = new(AST_BYTERANGE);
  exp->low = low;
  exp->high = high;
  return exp;
}

// Predicate: Is 'range' empty?
bool byterange_emptyp (pexl_Byte low, pexl_Byte high) {
  return (low > high);
}

ast_expression *make_any(void) {
  return new(AST_ANYCHAR);
}

ast_expression *make_char(uint32_t cp) {
  ast_expression *exp = new(AST_CHAR);
  return (exp ? (exp->codepoint = cp, exp) : NULL);
}

// Codepoints in a set are stored in (ascending) sorted order in an
// array.  Adding to a set is efficient if codepoints are added in
// ascending order.
ast_expression *make_codepointset (uint32_t capacity) {
  // Arbitrary minimum size avoids some subsequent calls to malloc
  if (capacity < 20) capacity = 20;
  ast_expression *exp = new(AST_SET);
  if (!exp) return NULL;
  pexl_Codepoint *codepoints = malloc(capacity * sizeof(pexl_Codepoint));
  if (!codepoints) {
    free(exp);
    return NULL;
  }
  exp->codepointset = malloc(sizeof(ast_codepointset));
  if (!exp->codepointset) {
    free(codepoints);
    free(exp);
    return NULL;
  }
  exp->codepointset->codepoints = codepoints;
  exp->codepointset->capacity = capacity;
  exp->codepointset->size = 0;
  return exp;
} 

// Predicate: Is the codepoint set representation valid?
bool codepointset_validp (ast_codepointset *set) {
  if ((set->codepoints == NULL) ||
      (set->size > set->capacity))
    return false;
  // Check that set is stored in sorted order without duplicates
  for (uint32_t i = 1; i < set->size; i++)
    if (set->codepoints[i] < set->codepoints[i-1]) return false;
  return true;
}

bool codepointset_emptyp (ast_codepointset *set) {
  return (set->size == 0);
}
  
static void free_codepointset (ast_codepointset *cpset) {
  if (cpset->codepoints) free(cpset->codepoints);
  free(cpset);
}

/* 
   Add codepoint 'c' to set 'set', maintaining the ascending sorted
   order.  Slower if codepoints are not added in ascending order.
*/
int codepointset_add (ast_codepointset *set, pexl_Codepoint c) {
  if (set->size == set->capacity) {
    size_t cap = 2 * set->capacity;
    pexl_Codepoint *tmp = realloc(set->codepoints, cap * sizeof(pexl_Codepoint));
    if (!tmp) return PEXL_ERR_OOM;
    set->codepoints = tmp;
    set->capacity = cap;
  }
  size_t i = 0;
  while ((set->codepoints[i] <= c) && (i < set->size)) i++;
  if (i == set->size) {
    set->codepoints[i] = c;
    set->size++;
    return OK;
  }
  if (set->codepoints[i] == c) {
    return OK;
  }
  memmove(&(set->codepoints[i+1]),
	  &(set->codepoints[i]),
	  (set->size - i) * sizeof(pexl_Codepoint));
  set->codepoints[i] = c;
  set->size++;
  return OK;
}

// Predicate: Is codepoint 'c' in 'set'?
bool codepointset_mem (ast_codepointset *set, pexl_Codepoint c) {
  size_t lo = 0, hi = set->size, mid;
  while (lo < hi) {
    mid = lo + (hi - lo) / 2;
    if (set->codepoints[mid] < c) {
      lo = mid + 1;
    } else if (set->codepoints[mid] > c) {
      hi = mid;
    } else return true;
  }
  return false;
}

// Start by passing in a 'prev' value of -1.  End of set is signaled
// by a returned 'prev' that is < 0.
pexl_Codepoint codepointset_iter (ast_codepointset *set, pexl_Index *prev) {
  if (*prev == -1)
    *prev = 0;
  else if (*prev < 0)
    return 0;
  else
    (*prev)++;
  if (((size_t) *prev >= set->size) || (set->size == 0)) {
    *prev = -1;
    return 0;
  }
  return set->codepoints[*prev];
}

// NOTE: Range is inclusive.  If low > high, range is empty.
ast_expression *make_range (pexl_Codepoint low, pexl_Codepoint high) {
  ast_expression *exp = new(AST_RANGE);
  if (!exp) return NULL;
  exp->low = low;
  exp->high = high;
  return exp;
}

// Predicate: Is 'range' empty?
bool codepointrange_emptyp (ast_expression *exp) {
  if (exp->type != AST_RANGE) {
    warn("ast", "Arg is not a codepoint range");
    return false;
  }
  return (exp->low > exp->high);
}

// Takes ownership of 'data'
ast_expression *make_bytestring (String *data) {
  ast_expression *exp = new(AST_BYTESTRING);
  return exp ? (exp->bytes = data, exp) : NULL;
} 

// Convenience: can pass in a literal string
ast_expression *make_bytestring_from (const string *data, size_t len) {
  ast_expression *exp = new(AST_BYTESTRING);
  return exp ? (exp->bytes = String_from(data, len), exp) : NULL;
} 

bool bytestring_emptyp (String *bytes) {
  return (String_len(bytes) == 0);
}

// This function takes ownership of 'name'.  All other constructors
// for an identifier should call this one.
ast_expression *make_identifier_using (String *name) {
  if (!name) {
    warn("ast", "Required name arg is null");
    return NULL;
  }
  ast_expression *exp = new(AST_IDENTIFIER);
  if (!exp) return NULL;
  exp->name = name;
  exp->idx = -1;		// This value signals "not set"
  return exp;
}

// Takes ownership of 'name'
ast_expression *make_identifier (String *name) {
  return make_identifier_using(name);
}

// Convenience for testing: can pass in a literal string
ast_expression *make_identifier_from (const string *name) {
  return make_identifier_using(String_from_literal(name));
}

/*
  Expressions with exactly one child
*/

// Unbounded repetitions have max set to zero.
// E.g. Kleene star has min == max == 0.
ast_expression *make_repetition (pexl_Index min,
				 pexl_Index max, 
				 ast_expression *repexp) {
  if (!repexp) {
    warn("ast", "Target expression (for repetition) is null");
    return NULL;
  }
  ast_expression *exp =  new(AST_REPETITION);
  if (!exp) return NULL;
  exp->min = min;
  exp->max = max;
  exp->child = repexp;
  return exp;
}

ast_expression *make_predicate (ast_type type,
				ast_expression *target) {
  if (!target) {
    warn("ast", "Target expression (for predicate) is null");
    return NULL;
  }
  ast_expression *exp;
  switch(type) {
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD:
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
      exp = new(type);
      if (!exp) return NULL;
      exp->child = target;
      return exp;
    default:
      warn("make_predicate", "Unknown predicate type %s (%d)",
	   ast_type_name(type), type);
      return NULL;
  }
}

ast_expression *make_find (ast_expression *target) {
  if (!target) {
    warn("ast", "Target expression (for find) is null");
    return NULL;
  }
  ast_expression *exp = new(AST_FIND);
  return (exp ? (exp->child = target, exp) : NULL);
}

//ast_expression *make_identifier (String *name);

ast_expression *make_backref (ast_expression *target) {
  if (!target || (target->type != AST_IDENTIFIER)) {
    warn("ast", "Backref target is not an identifier");
    return NULL;
  }
  ast_expression *exp = new(AST_BACKREF);
  return exp ? (exp->child = target, exp) : NULL;
}

// Takes ownership of 'name' and 'capexp'
ast_expression *make_capture (String *name, ast_expression *capexp) {
  if (!name || !capexp) {
    warn("ast", "Required arg is null");
    return NULL;
  }
  ast_expression *exp = new(AST_CAPTURE);
  if (!exp) return NULL;
  exp->child = capexp;
  exp->name = name;
  return exp;
}

// This constructor does no checking (on purpose).  It can be used to
// copy an AST or otherwise construct an AST that may have type errors.
ast_expression *make_insert_internal (String *name, ast_expression *child) {
  ast_expression *exp = new(AST_INSERT);
  if (!exp) return NULL;  
  exp->child = child;
  exp->name = name;
  return exp;
}

// Insert a capture node into the match tree.
// In lpeg, this is called a "constant capture".
// Takes ownership of 'name'.
ast_expression *make_insert (String *name, String *data) {
  if (!name || !data) {
    warn("ast", "Required arg is null");
    return NULL;
  }
  if (String_len(name) == 0) {
    warn("ast", "Capture name cannot be an empty string");
    return NULL;
  }
  ast_expression *bytes = make_bytestring(data);
  if (!bytes) return NULL;
  return make_insert_internal(name, bytes);
}

// Takes ownership of 'id'.
ast_expression *make_definition (String *id, ast_expression *exp) {
  if (!id || !exp) {
    warn("ast", "null required arg to make_definition");
    return NULL;
  }
  ast_expression *def = new(AST_DEFINITION);
  if (!def) return NULL;
  def->name = id;
  def->child = exp;
  return def;
}

/*
  Expressions with exactly 2 children
*/

ast_expression *make_seq (ast_expression *first,
			  ast_expression *second) {
  if (!first || !second) {
    warn("ast", "Required arg to make_seq is null");
    return NULL;
  }
  if (first == second) {
    warn("ast", "Args to make_seq cannot share structure");
    return NULL;
  }
  ast_expression *exp = new(AST_SEQ);
  if (!exp) return NULL;
  exp->child = first;
  exp->child2 = second;
  return exp;
}

ast_expression *make_choice (ast_expression *first,
			     ast_expression *second) {
  if (!first || !second) {
    warn("ast", "Required arg to make_choice is null");
    return NULL;
  }
  if (first == second) {
    warn("ast", "Args to make_choice cannot share structure");
    return NULL;
  }
  ast_expression *exp = new(AST_CHOICE);
  if (!exp) return NULL;
  exp->child = first;
  exp->child2 = second;
  return exp;
}

/*
  Expressions with 0 or more children
*/

ast_expression *make_module (ast_explist *defns) {
  ast_expression *exp = new(AST_MODULE);
  if (!exp) return NULL;
  exp->defns = defns;
  return exp;
}

/*
  AST node predicates
*/

bool ast_sugarp (ast_expression *exp) {
  return exp
    && (exp->type >= 0)
    && (exp->type < AST_SUGAR);
}

bool ast_expressionp (ast_expression *exp) {
  return exp
    && (exp->type >= 0)
    && (exp->type != AST_SUGAR)
    && (exp->type < AST_EXPRESSION);
}

#define DEFINE_AST_PREDICATE(typename, predicate_name)	\
  bool predicate_name(ast_expression *exp) {		\
    return exp && (exp->type == typename);			\
  }

DEFINE_AST_PREDICATE(AST_IDENTIFIER, ast_identifierp);
DEFINE_AST_PREDICATE(AST_DEFINITION, ast_definitionp);
DEFINE_AST_PREDICATE(AST_MODULE, ast_modulep);
DEFINE_AST_PREDICATE(AST_NUMBER, ast_numberp);
DEFINE_AST_PREDICATE(AST_ERROR, ast_errorp);
DEFINE_AST_PREDICATE(AST_BYTESTRING, ast_bytestringp);

/*
  Utilities
*/

ast_explist *explist_cons(ast_expression *head, ast_explist *tail) {
  if (!head) {
    warn("ast", "Required arg to explist_cons is null");
    return NULL;
  }
  ast_explist *new = malloc(sizeof(ast_explist));
  if (!new) return NULL;
  new->head = head;
  new->tail = tail;
  return new;
}

int explist_len(ast_explist *ls) {
  if (!ls) return 0;
  int count = 1;
  while ((ls = ls->tail)) count++;
  return count;
}

// The mapper function takes an optional context arg, if needed
ast_explist *map_explist(ast_expression *mapper(ast_expression *, void *),
			 ast_explist *ls,
			 void *context) {
  ast_expression *new;
  ast_explist *result = NULL;
  for (; ls; ls = ls->tail) {
    new = mapper(ls->head, context);
    if (!new) {
      free_explist(result);
      return NULL;
    }
    result = explist_cons(new, result);
  }
  return explist_nreverse(result);
}

// Destructive (in place) reverse
ast_explist *explist_nreverse(ast_explist *ls) {
  // Length 0 (empty list) is already reversed:
  if (!ls) return NULL;
  // Length 1 list is already reversed:
  if (!ls->tail) return ls;
  // Do the usual "reverse a linked list in place" thing:
  ast_explist *curr = ls;
  ast_explist *next = curr->tail;
  while (next) {
    ast_explist *nextnext = next->tail;
    next->tail = curr;
    curr = next;
    next = nextnext;
  }
  ls->tail = next;
  return curr;
}

int ast_child_count(ast_expression *exp) {
  if (!exp) return 0;
  switch (exp->type) {
    case AST_SEQ:
    case AST_CHOICE:
      return exp->child2 ? 2 : 1;
    case AST_MODULE:
      return explist_len(exp->defns);
    default:
      return exp->child ? 1 : 0;
  }
}

ast_expression *ast_child_iter(ast_expression *exp, ast_expression *prev) {
  if (!exp) return NULL;
  switch (exp->type) {
    case AST_SEQ:
    case AST_CHOICE:
      if (!prev) return exp->child;
      if (prev == exp->child) return exp->child2;
      return NULL;
    default:
      return prev ? NULL : exp->child;
  }
}

ast_expression *ast_list_iter(ast_explist *ls, ast_expression *prev) {
  if (!ls) return NULL;
  if (!prev) 
    return ls->head;
  else
    return ls->tail ? ls->tail->head : NULL;
}


/*
  Build a sequence or choice: 'exp' is an existing expression, to
  which 'newchild' is added.

  Special cases, for caller's convenience:
  - If 'exp' is NULL, returns 'newchild' (seq/choice of length 1)
  - If 'exp' is not a seq/choice, returns seq/choice of exp and newchild
  
  In the usual case, 'exp' is a seq/choice, possibly a chain of them,
  and 'exp' is extended to include 'newchild'

  NOTE: Takes ownership of 'newchild' (does NOT copy it)
*/ 
static ast_expression *build(ast_expression *exp,
			     ast_expression *newchild,
			     ast_type X,
			     ast_expression *(make)(ast_expression *,
						    ast_expression *)) {
  if (!newchild) {
    warn("ast", "Required arg to ast_builder (%s) is null", ast_type_name(X));
    return NULL;
  }

  // A sequence (or choice) of one expression is just the expression
  if (!exp) return newchild;

  // Make a seq (or choice) of exp and newchild, where exp is not a
  // seq (or choice)
  if (exp->type != X) return make(exp, newchild);


  // Add onto existing chain of seqs or choices
  ast_expression *new, *last = exp;
  while (last->child2->type == X) last = last->child2;
  new = make(last->child2, newchild);
  if (!new) return NULL;
  last->child2 = new;
  return exp;
}

ast_expression *build_seq(ast_expression *exp, ast_expression *newchild) {
  return build(exp, newchild, AST_SEQ, make_seq);
}

ast_expression *build_choice(ast_expression *exp, ast_expression *newchild) {
  return build(exp, newchild, AST_CHOICE, make_choice);
}

static void free_identifier (String *id) {
  String_free(id);
}

void free_explist(ast_explist *ls);

void free_expression (ast_expression *exp) {
  ast_expression *prev;
 tailcall:
  if (!exp) return;
  trace("ast", "Freeing exp of type %s", ast_type_name(exp->type));
  switch (exp->type) {
    case AST_BYTESET:
      free(exp->byteset);
      break;
    case AST_IDENTIFIER:
      free_identifier(exp->name); 
      break;
    case AST_BYTESTRING:
      String_free(exp->bytes);
      break;
    case AST_SET:
      free_codepointset(exp->codepointset);
      break;
    case AST_INSERT:
    case AST_CAPTURE:
      String_free(exp->name);
      break;
    case AST_SEQ:
    case AST_CHOICE:
      // First child is freed below
      free_expression(exp->child2);
      break;
    case AST_DEFINITION:
      String_free(exp->name);
      break;
    case AST_MODULE:
      free_explist(exp->defns);
      break;
    default:
      break;
  }
  if (exp->child) {
    prev = exp;
    exp = exp->child;
    free(prev);
    goto tailcall;
  }
  free(exp);
}

void free_explist(ast_explist *ls) {
  ast_explist *next;
  while (ls) {
    free_expression(ls->head);
    next = ls->tail;
    free(ls);
    ls = next;
  }
}
