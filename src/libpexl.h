/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  libpexl.h   PEXL: a library for building PEG-like languages              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file.         */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef libpexl_h
#define libpexl_h

/* ----------------------------------------------------------------------------- */
/* Configurable defaults                                                         */
/* ----------------------------------------------------------------------------- */

#define PEXL_DEFAULT_SIMD_CACHESIZE (1)
#define PEXL_DEFAULT_LOOP_UNROLL (4)
#define PEXL_DEFAULT_INLINE_MAXSIZE (30)

#define PEXL_REPETITION_MAX (1 << 20)

/* ----------------------------------------------------------------------------- */
/* PEXL_API will be defined already if we are compiling libpexl itself           */
/* ----------------------------------------------------------------------------- */

#ifndef PEXL_API
  #define PEXL_API 
  /* Define API using function pointers so that user can use dlopen, dlsym */
  #ifdef PEXL_DYLIB
    #define PEXL_DYN(f) ((*f))
  #else
    #define PEXL_DYN(f) (f)
  #endif

  #include <inttypes.h>  /* int32_t, etc. and their printf formats */
  #include <sys/types.h> /* ssize_t on Linux */
  // #ifdef __linux__
  //   #include <inttypes.h>
  // #endif
  #include <stdlib.h>    /* malloc() and free() */
  #include <stdbool.h>   /* bool, true, false */
  #include <limits.h>    /* e.g. INT32_MAX */

  /* Note: assert.h defines _DEFAULT_SOURCE */
  #include <assert.h> /* Ordinary C assertions */

#endif // PEXL_API is not defined

/* ----------------------------------------------------------------------------- */
/* Misc checks and limits                                                        */
/* ----------------------------------------------------------------------------- */

/* We depend on bytes having 8 bits, which should be all of them these days. */
#if CHAR_BIT != 8
#error "Size of character is not 8 bits"
#endif

typedef unsigned char pexl_Byte;
typedef uint32_t      pexl_Codepoint;

/*
   The pexl_Index type is used for array subscripts.  pexl_Index must
   be a signed type, because we use negative numbers to signal
   unassigned values and errors.  IMPORTANT: If we change its size
   away from 32 bits, then we must redefine the pexl_Error type.
*/
typedef int32_t pexl_Index;
#define pexl_Index_MAX   INT32_MAX
#define pexl_Index_MIN   INT32_MIN
#define pexl_Index_FMT   PRId32
#define pexl_Index_UNDEF -1

#if (PEXL_REPETITION_MAX > pexl_Index_MAX)
#error PEXL_REPETITION_MAX does not fit into the pexl_Index type
#endif

/*
   The pexl_Position type is used to represent an offset in an input
   stream.  The size_t type is used for array subscripts, e.g. the i
   in input[i].  When we need to communicate offsets like i through
   the API, we need a fixed width.  We cannot use size_t because it
   varies across architectures.

   We choose int64_t because:
   - INT64_MAX is large enough for any practical input size
   - 64-bit architectures are common today, so calculations are fast
   - Signed integer arithmetic is faster than unsigned in C
   - Negative values are useful to indicate unassigned values or errors, 
     and our byte-coded match data format needs the sign bit for its
     own use.  (Though that compact byte-coding may go away.)
*/
typedef int64_t pexl_Position;
#define pexl_Position_MAX INT64_MAX
#define pexl_Position_MIN INT64_MIN
#define pexl_Position_FMT PRId64

typedef int64_t pexl_Number;
#define pexl_Number_MAX INT64_MAX
#define pexl_Number_MIN INT64_MIN
#define pexl_Number_FMT PRId64

/* Max length of an unqualified identifier, in bytes. */
#define PEXL_MAX_IDLEN 100

/*
   Max length of a string that we can store in the string table,
   i.e. pattern and package names.  See symboltable.h for limits on
   this value.
*/
#define PEXL_MAX_STRINGLEN ((1 << 12) - 1)

/*
   The compiler is implemented using a few recursive functions.  These
   will cause a stack overflow if a single expression gets too deep
   (e.g. getfirst, free_all_fixed).

   Current maximum depth is one that does not overflow an 8MB stack,
   which is the default main thread size on Linux, MacOS, and OpenBSD.

   FUTURE: Recode using iteration (with a private temporary stack) so
   that we are limited only by available memory, not by the stack size
   limit of the current thread.  Then we can remove this expression
   depth restriction.
 */
#define PEXL_MAX_EXPDEPTH 2000

/* Built-in output encoders (see capture.c) */
typedef enum {
  PEXL_NO_ENCODER,
  PEXL_DEBUG_ENCODER,
  PEXL_TREE_ENCODER,
} pexlEncoderID;

typedef struct pexl_Match pexl_Match;
typedef struct pexl_MatchNode pexl_MatchNode;

PEXL_API pexl_Match   *PEXL_DYN(pexl_new_Match) (pexlEncoderID e);
PEXL_API void          PEXL_DYN(pexl_free_Match) (pexl_Match *m);

PEXL_API bool          PEXL_DYN(pexl_Match_failed) (pexl_Match *match);
PEXL_API pexl_Position PEXL_DYN(pexl_Match_end) (pexl_Match *match);
PEXL_API pexl_Position PEXL_DYN(pexl_MatchNode_start) (pexl_MatchNode *node);
PEXL_API pexl_Position PEXL_DYN(pexl_MatchNode_end) (pexl_MatchNode *node);
						
/* Pre-order iteration */
PEXL_API int PEXL_DYN(pexl_MatchTree_iter) (pexl_Match *m, pexl_Index prev, pexl_MatchNode **node);
/* Visit first child of 'parent' */
PEXL_API int PEXL_DYN(pexl_MatchTree_child) (pexl_Match *m, pexl_Index parent, pexl_MatchNode **node);
/* Visit next sibling node of 'prev' */
PEXL_API int PEXL_DYN(pexl_MatchTree_next) (pexl_Match *m, pexl_Index prev, pexl_MatchNode **node);
/* Return number of nodes */
PEXL_API int PEXL_DYN(pexl_MatchTree_size) (pexl_Match *m);

/* References to bindings */
#define PEXL_NO_MAIN -1
typedef pexl_Index pexl_Ref;
PEXL_API bool PEXL_DYN(pexl_Ref_notfound) (pexl_Ref r);
PEXL_API bool PEXL_DYN(pexl_Ref_invalid)  (pexl_Ref r);

typedef uint64_t pexl_Error;
PEXL_API pexl_Index PEXL_DYN(pexl_Error_value) (pexl_Error err);
PEXL_API pexl_Index PEXL_DYN(pexl_Error_location) (pexl_Error err);

#define PEXL_ROOT_ENV 0
typedef pexl_Index pexl_Env;

typedef struct pexl_Stats pexl_Stats;
typedef struct pexl_Optims pexl_Optims;
typedef struct pexl_Binary pexl_Binary;
typedef struct pexl_Context pexl_Context;
typedef struct pexl_Expr pexl_Expr;
typedef struct pexl_PackageTable pexl_PackageTable;
typedef struct pexl_BinaryAttributes pexl_BinaryAttributes;

enum pexl_OptimType { PEXL_OPT_SIMD,
		      PEXL_OPT_DYNAMIC_SIMD,
		      PEXL_OPT_INLINE,
		      PEXL_OPT_UNROLL,
		      PEXL_OPT_TRO,
		      PEXL_OPT_PEEPHOLE };

enum pexl_CharEncoding { PEXL_CHAR_ASCII,
			 PEXL_CHAR_UTF8 };

/* ----------------------------------------------------------------------------- */
/* COMPILATION                                                                   */
/* ----------------------------------------------------------------------------- */

// New API as of Tuesday, July 25, 2023

PEXL_API pexl_Context *PEXL_DYN(pexl_new_Context) (void);
PEXL_API void          PEXL_DYN(pexl_free_Context) (pexl_Context *C);

PEXL_API pexl_Ref      PEXL_DYN(pexl_bind) (pexl_Context *C, pexl_Expr *exp, const char *name);
PEXL_API pexl_Ref      PEXL_DYN(pexl_bind_in) (pexl_Context *C, pexl_Env env, pexl_Expr *exp, const char *name);
PEXL_API int           PEXL_DYN(pexl_rebind) (pexl_Context *C, pexl_Ref ref, pexl_Expr *exp);

PEXL_API pexl_Binary  *PEXL_DYN(pexl_compile) (pexl_Context *C,
					       pexl_Ref main,
					       pexl_Optims *optims,
					       pexl_BinaryAttributes *attrs,
					       enum pexl_CharEncoding enc,
					       pexl_Error *err);
PEXL_API void          PEXL_DYN(pexl_free_Binary) (pexl_Binary *pkg);

PEXL_API pexl_Optims  *PEXL_DYN(pexl_addopt_inline) (pexl_Optims *optims);
PEXL_API pexl_Optims  *PEXL_DYN(pexl_addopt_peephole) (pexl_Optims *optims);
PEXL_API pexl_Optims  *PEXL_DYN(pexl_addopt_unroll) (pexl_Optims *optims, uint32_t max_times_to_unroll);
PEXL_API pexl_Optims  *PEXL_DYN(pexl_addopt_tro) (pexl_Optims *optims);
PEXL_API pexl_Optims  *PEXL_DYN(pexl_addopt_simd) (pexl_Optims *optims, uint32_t cache_size);
PEXL_API pexl_Optims  *PEXL_DYN(pexl_addopt_dynamic_simd) (pexl_Optims *optims);
PEXL_API pexl_Optims  *PEXL_DYN(pexl_default_Optims) (void);
PEXL_API void          PEXL_DYN(pexl_free_Optims) (pexl_Optims *optims);

PEXL_API pexl_Env PEXL_DYN(pexl_scope_push) (pexl_Context *C);
PEXL_API pexl_Env PEXL_DYN(pexl_scope_pop) (pexl_Context *C);
						  
/* ----------------------------------------------------------------------------- */
/* EXPRESSIONS                                                                   */
/* ----------------------------------------------------------------------------- */

/*
  The pexl_foo_f() functions create a 'foo' expression and then free
  their expression args.  These are pure convenience, mostly for testing.
  E.g.
      exp = pexl_choice_f(C,
                          pexl_match_string(C, "abc"), 
			  pexl_match_string(C, "def"));

  Without pexl_choice_f() we would have to store the two
  sub-expressions so we could free them ourselves, e.g.
  
      exp1 = pexl_match_string(C, "abc");
      exp2 = pexl_match_string(C, "def"));
      exp = pexl_choice_f(C, exp1, exp2);
      pexl_free_exp(exp1);
      pexl_free_exp(exp2);

*/

PEXL_API void       PEXL_DYN(pexl_free_Expr) (pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_copy_Expr) (pexl_Expr *exp);

/* Primitive patterns */
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_bytes)(pexl_Context *C, const char *ptr, size_t len);
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_string) (pexl_Context *C, const char *str);
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_epsilon) (pexl_Context *C);
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_any)(pexl_Context *C, int n);
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_set)(pexl_Context *C, const char *set, size_t len);
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_complement)(pexl_Context *C, const char *set, size_t len);
PEXL_API pexl_Expr *PEXL_DYN(pexl_match_range)(pexl_Context *C, pexl_Byte from, pexl_Byte to);
PEXL_API pexl_Expr *PEXL_DYN(pexl_fail)(pexl_Context *C);

/* Binary combinators */
PEXL_API pexl_Expr *PEXL_DYN(pexl_seq)(pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2);
PEXL_API pexl_Expr *PEXL_DYN(pexl_seq_f) (pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2);
PEXL_API pexl_Expr *PEXL_DYN(pexl_choice)(pexl_Context *C, pexl_Expr *t1, pexl_Expr *t2);
PEXL_API pexl_Expr *PEXL_DYN(pexl_choice_f) (pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2);

/* Unary combinators */
PEXL_API pexl_Expr *PEXL_DYN(pexl_repeat) (pexl_Context *C, pexl_Expr *tree,
					   pexl_Index min, pexl_Index max);
PEXL_API pexl_Expr *PEXL_DYN(pexl_repeat_f) (pexl_Context *C, pexl_Expr *exp,
					     pexl_Index min, pexl_Index max);
PEXL_API pexl_Expr *PEXL_DYN(pexl_lookahead)(pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_lookahead_f) (pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_neg_lookahead)(pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_neg_lookahead_f) (pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_lookbehind)(pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_lookbehind_f) (pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_neg_lookbehind)(pexl_Context *C, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_neg_lookbehind_f)(pexl_Context *C, pexl_Expr *exp);

/* Call */
PEXL_API pexl_Expr *PEXL_DYN(pexl_call)(pexl_Context *C, pexl_Ref ref);
PEXL_API pexl_Expr *PEXL_DYN(pexl_xcall)(pexl_Context *C, pexl_Binary *pkg, pexl_Index sym);

/* ----------------------------------------------------------------------------- */
/* Extensions to PEG grammars                                                    */
/* ----------------------------------------------------------------------------- */

/* pexlCaptures - Note that string arguments are NOT freed by these functions */
PEXL_API pexl_Expr *PEXL_DYN(pexl_capture)(pexl_Context *C, const char *name, pexl_Expr *tree1);
PEXL_API pexl_Expr *PEXL_DYN(pexl_capture_f) (pexl_Context *C, const char *name, pexl_Expr *exp);
PEXL_API pexl_Expr *PEXL_DYN(pexl_insert)(pexl_Context *C, const char *name, const char *value);

/* Find */
PEXL_API pexl_Expr *PEXL_DYN(pexl_find)(pexl_Context *Ctx, pexl_Expr *t1);
PEXL_API pexl_Expr *PEXL_DYN(pexl_find_f) (pexl_Context *C, pexl_Expr *exp);

/* Backreference (wip) */
PEXL_API pexl_Expr *PEXL_DYN(pexl_backref)(pexl_Context *C, const char *capname);

/* Where (a work in progress) */
PEXL_API pexl_Expr *PEXL_DYN(pexl_where) (pexl_Context *C, 
					  pexl_Expr *t1, char *pathstring, pexl_Expr *t2);
PEXL_API pexl_Expr *PEXL_DYN(pexl_where_f) (pexl_Context *C,
					    pexl_Expr *t1, char *pathstring, pexl_Expr *t2);
					   
/* ----------------------------------------------------------------------------- */
/* RUNTIME                                                                       */
/* ----------------------------------------------------------------------------- */

PEXL_API bool PEXL_DYN(pexl_binary_has_simd) (pexl_Binary *p);
PEXL_API bool PEXL_DYN(pexl_simd_available) (void);

// Must be powers of two. Do not want a dependency on bitflags.h here,
// else we would use BITFLAGS().
enum PEXL_VM_FLAGS {
  PEXL_DISABLE_SIMD =         0, // vm should NOT use SIMD even if available.
  PEXL_DISABLE_DYNAMIC_SIMD = 1	 // vm should NOT use dynamic SIMD.
};

PEXL_API int PEXL_DYN(pexl_run) (pexl_Binary *pkg,
				 pexl_PackageTable *pt,
				 const char *patname,
				 const char *input, size_t inputlen, size_t start,
				 uint32_t vm_flags,
				 /* in/out */
				 pexl_Stats *stats,
				 pexl_Match *match);

/* ----------------------------------------------------------------------------- */
/* PRINT FUNCTIONS                                                               */
/* ----------------------------------------------------------------------------- */

PEXL_API void PEXL_DYN(pexl_print_Env) (pexl_Env env, pexl_Context *C);
PEXL_API void PEXL_DYN(pexl_print_Expr) (pexl_Expr *tree, int indent, pexl_Context *C);
PEXL_API void PEXL_DYN(pexl_print_Binary) (pexl_Binary *p);
PEXL_API void PEXL_DYN(pexl_print_Match) (pexl_Match *match);
PEXL_API void PEXL_DYN(pexl_print_Match_summary) (pexl_Match *match);
PEXL_API void PEXL_DYN(pexl_print_Match_data) (pexl_Match *match);


/* ----------------------------------------------------------------------------- */
/* ERROR CODES */
/* ----------------------------------------------------------------------------- */

/*
   Every code is < 0.   Code 0 always means SUCCESS.

   IMPORTANT: The code PEXL_ERR_INTERNAL indicates a bug.  A user
              getting this code back from a pexl API should report it.

   The error values from -2 down to -99 are reserved for code that is
   meant to be easily usable outside of this project, like the
   implementation of Tarjan's algorithm for Strongly Connected
   Components in scc.c.

   Error values in that kind of generically useful code are allowed to
   overlap with each other, and so they should not escape beyond the
   PEXL API.  In other words, any user of a PEXL API should see only
   the error values given in this file and not any in the range [-99, -2].

   Also, the value -999999 is used as a generic error value in
   internal code, where it is called ERR, and this value should not be
   returned to the caller of a PEXL API.
*/

/* ----------------------------------------------------------------------------- */
/* COMPILER ERROR CODES                                                          */
/* ----------------------------------------------------------------------------- */

#define PEXL_OK                             0 /* Must be 0 to write 'if (err)..' */
#define PEXL_NOT_FOUND                     -1
#define PEXL_ITER_START                    -1					      
#define PEXL_ERR_INTERNAL                -100 /* Internal error: Indicates a bug */
#define PEXL_ERR_NULL                    -101 /* Null arg where one is required */
#define PEXL_ERR_OOM                     -102 /* Out of memory */
#define PEXL_ERR_FULL                    -103 /* Table or other storage is full */

/* env.c */
#define PEXL_ENV__NOT_FOUND              -151 /* Not an error, per se */
#define PEXL_ENV__ERR_EXISTS             -152 /* Name already bound */
#define PEXL_ENV__ERR_MAX_FIXEDLEN       -154 /* Fixed length of pattern is too long */
#define PEXL_ENV__ERR_NOT_PATTERN        -156 /* Pattern expected, other type received */
#define PEXL_ENV__ERR_NAMELEN            -157 /* Name cannot be empty nor too long */
#define PEXL_ENV__ERR_ENV                -159 /* Env id not valid (not in use) in binding table */

/* expression.c */
/* pattern.c */
#define PEXL_EXP__ERR_OPENFAIL           -202 /* Encountered TOpenCall node */
#define PEXL_EXP__ERR_DEPTH              -204 /* pexl_Expr *nesting too deep */
#define PEXL_EXP__ERR_LEN                -205 /* String/bytes argument too long */
#define PEXL_EXP__ERR_MINMAX             -206 /* Min or max args invalid */
#define PEXL_EXP__ERR_PATH               -207 /* Invalid path string */
#define PEXL_EXP__NOT_FOUND              -209 /* Not always an error */

/* analyze.c */
/* compile.c */
#define PEXL_EXP__ERR_NOTCOMPILED        -252 /* Dependent value has not been compiled */
#define PEXL_EXP__ERR_PATLEN             -253 /* Pattern length exceeds EXP_MAX_BEHIND */
#define PEXL_EXP__ERR_CAPTURES           -254 /* Pattern has captures where not allowed */
#define PEXL_EXP__ERR_NO_PATTERN         -255 /* No value, or value not a pattern */
#define PEXL_EXP__ERR_SCOPE              -256 /* Call target not in scope */
#define PEXL_EXP__ERR_LEFT_RECURSIVE     -257 /* Pattern is left-recursive */

/* symboltable.c */
#define PEXL_ST__ERR_STRINGLEN           -301  /* max string length exceeded */
#define PEXL_ST__ERR_BLOCKFULL           -302  /* reached limit for string block storage */ 
#define PEXL_ST__ERR_INITSIZE            -303  /* invalid initial size or blocksize requested */ 

/* compile.c */
#define PEXL_COMPILER__ERR               -501 /* Details will be in pexl_Error struct */
#define PEXL_COMPILER__ERR_BADREF        -502 /* Bad reference */
#define PEXL_COMPILER__ERR_REFNOTPATTERN -503 /* pexl_Reference value not a pattern */
#define PEXL_COMPILER__ERR_SCOPE         -506 /* pexl_Env not in env tree rooted in context */
#define PEXL_COMPILER__ERR_OPTIMS        -507 /* Optim list invalid */
#define PEXL_COMPILER__ERR_LEN           -508 /* String/bytes argument too long */

/* codegen.c */
#define PEXL_CODEGEN__ERR_IRTYPE         -403 /* Unknown intermediate rep (tree) type */
#define PEXL_CODEGEN__ERR_OPENFAIL       -404 /* OpenCall failed; Or, unexpected TOpenCall node */
#define PEXL_CODEGEN__ERR_NO_PACKAGE     -405 /* Missing binary in package table (XCall) */
#define PEXL_CODEGEN__ERR_NOTCOMPILED    -406 /* Encountered PATTERN_READY not set */
#define PEXL_CODEGEN__ERR_SYMTAB_FULL    -407 /* Symbol table full */
#define PEXL_CODEGEN__ERR_SYMTAB_BLOCK_FULL -408 /* Block storage in symbol table full */
#define PEXL_CODEGEN__ERR_WHERE_REQUIRES_CAPTURE_X -409 /*Pattern x(child1) of where node must be a capture*/

/* ----------------------------------------------------------------------------- */
/* RUNTIME ERROR CODES                                                           */
/* ----------------------------------------------------------------------------- */

/* pexl_Matching and capture processing errors begin at 1000 */
#define PEXL_MATCH__NOEXIST             -1001
#define PEXL_MATCH__ERR_BADINST         -1003
#define PEXL_MATCH__ERR_INPUT_LEN       -1004
#define PEXL_MATCH__ERR_STARTPOS        -1005
#define PEXL_MATCH__ERR_ENDPOS          -1006
#define PEXL_MATCH__ERR_BTLIMIT         -1007 /* reached backtrack limit */
#define PEXL_MATCH__ERR_BUFFER          -1008 /* corrupt match data buffer */
#define PEXL_MATCH__ERR_NULL            -1009 /* null argument where not allowed */
#define PEXL_MATCH__ERR_ENTRYPOINT      -1010 /* invalid entrypoint */
#define PEXL_MATCH__ERR_ENCODER         -1011 /* invalid encoder id */
#define PEXL_MATCH__ERR_CAPLIMIT        -1012 /* reached capture limit */
#define PEXL_MATCH__ERR_NOSYMBOL        -1013 /* symbol name not found */

#endif // ifndef (pexl_h)
