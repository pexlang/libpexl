/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  libpexl.c  API                                                           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"
#include "libpexl.h"
#include "context.h"
#include "binary.h"
#include "vm.h"
#include "opt.h"

extern bool PEXL_simd_available;

PEXL_API
void pexl_free_Expr (pexl_Expr *tree) {
  free_expr(tree);
}

PEXL_API 
pexl_Expr *pexl_copy_Expr (pexl_Expr *tree) {
  return copy_expr(tree);
}

PEXL_API
pexl_Expr *pexl_fail (pexl_Context *C) {
  if (!C) {
    warn("pexl_fail", "Null context arg");
    return NULL;
  }
  return fail_expr(&C->err);
}

PEXL_API
pexl_Expr *pexl_match_bytes (pexl_Context *C,
			     const char *ptr,
			     size_t len) {
  pexl_Index id = 0;
  if (!C) {
    warn("pexl_match_bytes", "Null context arg");
    return NULL;
  }
  if ((ptr == NULL) && (len != 0)){
    warn("pexl_match_bytes", "Null string but non-zero length");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  if (len > EXP_MAXSTRING) {
    C->err = PEXL_EXP__ERR_LEN;
    return NULL;
  }
  if (len != 0) {
    id = context_intern(C, ptr, len);
    if (id < 0) {
      C->err = id;	     /* PEXL_ error code from context_intern*/
      return NULL;
    }
  }
  return match_bytes(id, ptr, len, &C->err);
}

PEXL_API
pexl_Expr *pexl_match_any (pexl_Context *C, int n) {
  if (!C) {
    warn("pexl_match_any", "Null context arg");
    return NULL;
  }
  return match_any(n, &C->err);
}

PEXL_API
pexl_Expr *pexl_match_set (pexl_Context *C, const char *set, size_t len) {
  if (!C) {
    warn("pexl_match_set", "Null context arg");
    return NULL;
  }
  if (!set) {
    warn("pexl_match_set", "Null set arg");
    return NULL;
  }
  return match_set(set, len, false, &C->err);
}

PEXL_API
pexl_Expr *pexl_match_complement (pexl_Context *C, const char *set, size_t len) {
  if (!C) {
    warn("pexl_match_complement", "Null context arg");
    return NULL;
  }
  if (!set) {
    warn("pexl_match_complement", "Null set arg");
    return NULL;
  }
  return match_set(set, len, true, &C->err);
}

PEXL_API
pexl_Expr *pexl_match_range (pexl_Context *C, pexl_Byte from, pexl_Byte to) {
  if (!C) {
    warn("pexl_match_range", "Null context arg");
    return NULL;
  }
  if (from > to) {
    warn("pexl_match_range", "Args out of order (required: from <= to)");
    return NULL;
  }
  return match_range(from, to, &C->err);
}

PEXL_API
pexl_Expr *pexl_seq (pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2) {
  if (!C) {
    warn("pexl_seq", "Null context arg");
    return NULL;
  }
  if (!exp1 || !exp2) {
    warn("pexl_seq", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return seq(exp1, exp2, &C->err);
}

PEXL_API
pexl_Expr *pexl_choice (pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2) {
  if (!C) {
    warn("pexl_choice", "Null context arg");
    return NULL;
  }
  if (!exp1 || !exp2) {
    warn("pexl_choice", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return choice(exp1, exp2, &C->err);
}

PEXL_API
pexl_Expr *pexl_neg_lookahead (pexl_Context *C, pexl_Expr *exp) {
  if (!C) {
    warn("pexl_neg_lookahead", "Null context arg");
    return NULL;
  }
  if (!exp) {
    warn("pexl_neg_lookahead", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return neg_lookahead(exp, &C->err);
}

PEXL_API
pexl_Expr *pexl_lookahead (pexl_Context *C, pexl_Expr *exp) {
  if (!C) {
    warn("pexl_lookahead", "Null context arg");
    return NULL;
  }
  if (!exp) {
    warn("pexl_lookahead", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return lookahead(exp, &C->err);
}

PEXL_API
pexl_Expr *pexl_lookbehind(pexl_Context *C, pexl_Expr *exp) {
  if (!C) {
    warn("pexl_lookbehind", "Null context arg");
    return NULL;
  }
  if (!exp) {
    warn("pexl_lookbehind", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return lookbehind(exp, &C->err);
}

PEXL_API
pexl_Expr *pexl_repeat (pexl_Context *C, pexl_Expr *exp,
			pexl_Index min, pexl_Index max) {
  if (!C) {
    warn("pexl_lookbehind", "Null context arg");
    return NULL;
  }
  if (!exp) {
    warn("pexl_lookbehind", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  const char *errmsg = NULL;
  if ((max != 0) && (max < min))
    errmsg = "max is less than min";
  if ((min < 0) || (min > MAX_REPETITIONS))
    errmsg = "min arg out of range";
  if ((max < 0) || (max > MAX_REPETITIONS))
    errmsg = "max arg out of range";
  if (errmsg) {
    inform("pexl_repeat", errmsg);
    C->err = PEXL_EXP__ERR_MINMAX;
    return NULL;
  }
  return repeat(exp, min, max, &C->err);
}

PEXL_API
pexl_Expr *pexl_capture (pexl_Context *C, const char *name, pexl_Expr *exp) {
  size_t len;
  pexl_Index handle;
  if (!C) {
    warn("pexl_capture", "Null context arg");
    return NULL;
  }
  if (!name) {
    warn("pexl_capture", "Null capture name");
    return NULL;
  }
  if (!exp) {
    warn("pexl_capture", "Null expression arg");
    return NULL;
  }
  len = strnlen(name, PEXL_MAX_STRINGLEN + 1);
  if (len == 0) {
    warn("pexl_capture", "Capture name cannot be empty string");
    return NULL;
  } else if (len > PEXL_MAX_STRINGLEN) {
    warn("pexl_capture", "Capture name exceeds limit of %d bytes",
	 PEXL_MAX_STRINGLEN);
    return NULL;
  }
  if ((handle = context_intern(C, name, len)) < 0) {
    C->err = handle;
    return NULL;
  }
  return capture(handle, exp, &C->err);
}

PEXL_API
pexl_Expr *pexl_call (pexl_Context *C, pexl_Ref ref) {
  if (!C) {
    warn("pexl_call", "Null context arg");
    return NULL;
  }
  return call(ref, C->bt, &C->err);
}

PEXL_API
pexl_Expr *pexl_xcall (pexl_Context *C, pexl_Binary *pkg, pexl_Index symbol) {
  if (!C) {
    warn("pexl_xcall", "Null context arg");
    return NULL;
  }
  if (!pkg) {
    warn("pexl_xcall", "Null binary package arg");
    return NULL;
  }
  pexl_Index pt_num = packagetable_getnum(C->packages, pkg);
  if (pt_num < 0) {
    warn("pexl_xcall", "Package not in package table");
    C->err = PEXL_EXP__NOT_FOUND;
    return NULL;
  }
  SymbolTableEntry *entry = symboltable_get(pkg->symtab, symbol);
  if (entry->type != symbol_type_pattern) {
    warn("pexl_xcall", "Symbol at index %d of package is not a pattern", symbol);
    C->err = PEXL_EXP__ERR_NO_PATTERN;
    return NULL;
  }
  return xcall(pkg, symbol, &C->err);
}

PEXL_API
pexl_Expr *pexl_insert (pexl_Context *C, const char *name, const char *value) {
  pexl_Index name_handle, value_handle;
  if (!C) {
    warn("pexl_insert", "Null context arg");
    return NULL;
  }
  if (!name || !value) {
    warn("pexl_insert", "Neither name nor value can be null");
    return NULL;
  }
  /* Name cannot be the empty string, nor NULL */
  size_t namelen = strnlen(name, PEXL_MAX_STRINGLEN + 1);
  if (namelen == 0) {
    warn("pexl_insert", "Capture name cannot be empty string");
    return NULL;
  }
  if (namelen > PEXL_MAX_STRINGLEN) {
    warn("pexl_insert", "Capture name too long");
    return NULL;
  }
  size_t valuelen = strnlen(value, PEXL_MAX_STRINGLEN + 1);
  if (valuelen > PEXL_MAX_STRINGLEN) {
    warn("pexl_insert", "Capture value string too long");
    return NULL;
  }
  if ((name_handle = context_intern(C, name, namelen)) <= 0) {
    C->err = (int) name_handle;
    warn("pexl_insert", "Failed to intern the capture name");
    return NULL;
  }
  if ((value_handle = context_intern(C, value, valuelen)) < 0) {
    C->err = (int) value_handle;
    warn("pexl_insert", "Failed to intern the capture value");
    return NULL;
  }
  return insert(name_handle, value_handle, &C->err);
}

PEXL_API
pexl_Expr *pexl_find(pexl_Context *C, pexl_Expr *exp) {
  if (!C) {
    warn("pexl_find", "Null context arg");
    return NULL;
  }
  if (!exp) {
    warn("pexl_find", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return find(exp, &C->err);
}
PEXL_API
pexl_Expr *pexl_backref (pexl_Context *C, const char *name) {
  size_t len;
  pexl_Index handle; 
  if (!C) {
    warn("pexl_backref", "Null context arg");
    return NULL;
  }
  if (!name) {
    warn("pexl_backref", "Null backreference target");
    return NULL;
  }
  len = strnlen(name, PEXL_MAX_STRINGLEN + 1);
  if (len == 0) {
    warn("pexl_capture", "Capture name cannot be empty string");
    return NULL;
  } else if (len > PEXL_MAX_STRINGLEN) {
    warn("pexl_capture", "Capture name exceeds limit of %d bytes",
	 PEXL_MAX_STRINGLEN);
    return NULL;
  }
  if ((handle = context_intern(C, name, len)) < 0) {
    C->err = handle;
    return NULL;
  }
  return backreference(handle, &C->err);
}

PEXL_API
pexl_Expr *pexl_where (pexl_Context *C,
		       pexl_Expr *exp1,
		       char *pathstring,
		       pexl_Expr *exp2) {

  if (!C) {
    warn("pexl_where", "Null context arg");
    return NULL;
  }
  if (!exp1 || !exp2) {
    warn("pexl_find", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  if (!pathstring) {
    warn("pexle_where", "Null path arg");
    C->err = PEXL_ERR_NULL;
    return NULL;
  }
  return where(exp1, pathstring, exp2, C->pathtab, &C->err);
}

// True if package was compiled with SIMD support
PEXL_API
bool pexl_binary_has_simd (pexl_Binary *p) {
  if (!p || !p->cstab) return PEXL_ERR_NULL;
  return (p->cstab->simd_charsets != NULL);
}

// True if SIMD was available (and not disabled) at build time
PEXL_API
bool pexl_simd_available (void) {
  return PEXL_simd_available;
}

PEXL_API
int pexl_run (pexl_Binary *pkg,
	      pexl_PackageTable *imports,
	      const char *patname,
	      const char *input, size_t inputlen, size_t start,
	      uint32_t vm_flags,
	      /* in/out */
	      pexl_Stats *stats,
	      pexl_Match *match) {

  SymbolTableEntry *entry;
  pexl_Index entrypoint;

  if (!pkg) {
    warn("vm", "Null package arg");
    return PEXL_ERR_NULL;
  }
  if (patname) {
    entry = binary_lookup_symbol(pkg, symbol_ns_id, patname);
    if (!entry) {
      trace("vm", "Pattern name '%s' not in symbol table", patname);
      return PEXL_MATCH__ERR_NOSYMBOL;
    }
    entrypoint = entry->value;
  } else {
    entry = symboltable_get(pkg->symtab, pkg->main);
    if (!entry) {
      warn("vm", "Failed to locate main pattern in package");
      return PEXL_ERR_INTERNAL;
    }
    entrypoint = entry->value;
  }
  assert(entrypoint >= 0);
  return vm_start(pkg, imports, entrypoint, 
		  input, inputlen, start, inputlen,
		  vm_flags, stats, match);
}

PEXL_API
pexl_Match *pexl_new_Match (pexlEncoderID e) {
  pexl_Match *m = calloc(1, sizeof(pexl_Match));
  m->encoder_id = e;	     // value will be checked when used, in the vm
  return m;
}

PEXL_API
void pexl_free_Match (pexl_Match *m) {
  pexlEncoderDataFree free_fn;
  if (!m)
    return;
  if (m->data)   {
    free_fn = get_encoder_destructor(m->encoder_id);
    if (free_fn)
      free_fn(m->data);
  }
  free(m);
}

PEXL_API
pexl_Position pexl_Match_end (pexl_Match *match) {
  if (!match) {
    warn("tree", "Required arg is null");
    return PEXL_ERR_NULL;
  }
  return (pexl_Position) match->end;
}

PEXL_API
pexl_Position pexl_MatchNode_start (pexl_MatchNode *node) {
  if (!node) {
    warn("tree", "Required arg is null");
    return PEXL_ERR_NULL;
  }
  return (pexl_Position) node->start;
}

PEXL_API
pexl_Position pexl_MatchNode_end (pexl_MatchNode *node) {
  if (!node) {
    warn("tree", "Required arg is null");
    return PEXL_ERR_NULL;
  }
  return (pexl_Position) node->end;
}

PEXL_API
bool pexl_Match_failed (pexl_Match *match) {
  if (!match) {
    warn("tree", "Required arg is null");
    return true;
  }
  return (match->end == -1);
}

PEXL_API
pexl_Index pexl_MatchTree_iter (pexl_Match *match, pexl_Index prev, pexl_MatchNode **node) {
  if (!match || !node) {
    inform("tree", "Match or node pointer arg is null");
    return PEXL_ERR_NULL;
  }
  if (match->encoder_id != PEXL_TREE_ENCODER) {
    inform("tree",
	   "Match encoder (%d) is not PEXL_TREE_ENCODER (%d)",
	   match->encoder_id, PEXL_TREE_ENCODER);
    return PEXL_MATCH__ERR_ENCODER;
  }
  return matchtree_iter(match, prev, node);
}

PEXL_API
pexl_Index pexl_MatchTree_child (pexl_Match *match, pexl_Index parent, pexl_MatchNode **child) {
  if (!match || !child) {
    inform("tree", "Match arg or child node pointer arg is null");
    return PEXL_ERR_NULL;
  }
  if (match->encoder_id != PEXL_TREE_ENCODER) {
    inform("tree",
	   "Match encoder (%d) is not PEXL_TREE_ENCODER (%d)",
	   match->encoder_id, PEXL_TREE_ENCODER);
    return PEXL_MATCH__ERR_ENCODER;
  }
  if (!match->data) return PEXL_MATCH__NOEXIST;
  match_node *probe;
  pexl_Index idx = match_tree_child((pexl_MatchTree *)match->data, parent, &probe);
  if (idx < 0) return idx;
  *child = &(probe->data);
  return idx;
}

PEXL_API
pexl_Index pexl_MatchTree_next (pexl_Match *match, pexl_Index prev, pexl_MatchNode **sib) {
  if (!match || !sib) {
    inform("tree", "Match arg or sibling node pointer arg is null");
    return PEXL_ERR_NULL;
  }
  if (match->encoder_id != PEXL_TREE_ENCODER) {
    inform("tree",
	   "Match encoder (%d) is not PEXL_TREE_ENCODER (%d)",
	   match->encoder_id, PEXL_TREE_ENCODER);
    return PEXL_MATCH__ERR_ENCODER;
  }
  if (!match->data) return PEXL_MATCH__NOEXIST;
  match_node *probe;
  pexl_Index idx = match_tree_next((pexl_MatchTree *)match->data, prev, &probe);
  if (idx < 0) return idx;
  *sib = &(probe->data);
  return idx;
}

PEXL_API
pexl_Index pexl_MatchTree_size (pexl_Match *match) {
  if (!match) {
    inform("tree", "Match node arg is null");
    return PEXL_ERR_NULL;
  }
  if (match->encoder_id != PEXL_TREE_ENCODER) {
    inform("tree",
	   "Match encoder (%d) is not PEXL_TREE_ENCODER (%d)",
	   match->encoder_id, PEXL_TREE_ENCODER);
    return PEXL_MATCH__ERR_ENCODER;
  }
  pexl_MatchTree *mt = (pexl_MatchTree *) match->data;
  return mt ? mt->size : 0;
}

PEXL_API
pexl_Optims *pexl_addopt_simd (pexl_Optims *optims, uint32_t cache_size) {
  if (cache_size == 0) {
    cache_size = PEXL_DEFAULT_SIMD_CACHESIZE;
    warn("optimizer",
	 "SIMD cache size arg is 0. Using default size of %d",
	 cache_size);
  }
  return addopt_simd(optims, cache_size);
}

PEXL_API
pexl_Optims *pexl_addopt_dynamic_simd (pexl_Optims *optims) {
  return addopt_dynamic_simd(optims);
}

PEXL_API
pexl_Optims *pexl_addopt_inline (pexl_Optims *optims) {
  return addopt_inline(optims);
}

PEXL_API
pexl_Optims *pexl_addopt_unroll (pexl_Optims *optims, uint32_t max_times_to_unroll) {
  if (max_times_to_unroll < 2) {
    warn("optimizer", "Arg for unrolling maximum is < 2. Using value of 2");
    max_times_to_unroll = 2;
  }
  return addopt_unroll(optims, max_times_to_unroll);
}

PEXL_API
pexl_Optims *pexl_addopt_peephole (pexl_Optims *optims) {
  return addopt_peephole(optims);
}

PEXL_API
pexl_Optims *pexl_addopt_tro (pexl_Optims *optims) {
  return addopt_tro(optims);
}

PEXL_API
void pexl_free_Optims (pexl_Optims *optims) {
  free_optims(optims);
}

#define warn_and_fail(msg) do {			\
    warn("opt", (msg));				\
    return NULL;				\
  } while (0);

// TODO: Decide where to establish these, instead of hard-coding them here. 
PEXL_API
pexl_Optims *pexl_default_Optims (void) {
  pexl_Optims *optims = NULL;
  optims = pexl_addopt_simd(optims, 1);
  if (!optims) warn_and_fail("failed to create simd config");
  optims = pexl_addopt_dynamic_simd(optims);
  if (!optims) warn_and_fail("failed to create dynamic simd config");
  optims = pexl_addopt_tro(optims);
  if (!optims) warn_and_fail("failed to create tro config");
  optims = pexl_addopt_unroll(optims, 5);
  if (!optims) warn_and_fail("failed to create unrolling config");
  optims = pexl_addopt_inline(optims);
  if (!optims) warn_and_fail("failed to create inlining config");
  optims = pexl_addopt_peephole(optims);
  if (!optims) warn_and_fail("failed to create peephole config");
  return optims;
}

PEXL_API
pexl_Index pexl_Error_value (pexl_Error err) {
  return error_value(err);
}

PEXL_API
pexl_Index pexl_Error_location (pexl_Error err) {
  return error_location(err);
}

PEXL_API
pexl_Context *pexl_new_Context (void) {
  return context_new();
}

PEXL_API
void pexl_free_Context (pexl_Context *C) {
  context_free(C);
}

PEXL_API
pexl_Ref pexl_bind_in (pexl_Context *C, pexl_Env env, pexl_Expr *exp, const char *name) {
  if (!C) {
    warn("pexl_bind_in", "Null context arg");
    return PEXL_ERR_NULL;
  }
  if (!valid_path_to_root(env, C->bt)) {
    warn("pexl_bind_in", "Invalid env for this context");
    return PEXL_COMPILER__ERR_SCOPE;
  }
  size_t len = 0;
  pexl_Index handle;
  /* A null name arg is treated like the empty string here. */  
  if (name) {
    len = strnlen(name, PEXL_MAX_STRINGLEN + 1);
    handle = context_intern(C, name, len);
  } else {
    handle = context_intern(C, "", 0);
  }
  // context_intern will produce a PEXL_ error code
  if (handle < 0) return handle;
  return bind_in(exp, env, handle, C->bt);
}

// Bind in the current environment, C->env
PEXL_API
pexl_Ref pexl_bind (pexl_Context *C, pexl_Expr *exp, const char *name) {
  return pexl_bind_in(C, C->env, exp, name);
}


PEXL_API
int pexl_rebind (pexl_Context *C, pexl_Ref ref, pexl_Expr *exp) {
  if (!C) {
    warn("pexl_rebind", "Null context arg");
    return PEXL_ERR_NULL;
  }
  if (!exp) {
    warn("pexl_rebind", "Null expression arg");
    C->err = PEXL_ERR_NULL;
    return PEXL_ERR_NULL;
  }
  if (pexl_Ref_invalid(ref)) {
    warn("pexl_rebind", "Invalid ref");
    C->err = PEXL_COMPILER__ERR_BADREF;
    return PEXL_COMPILER__ERR_BADREF;
  }
  return rebind(ref, exp, C->bt);
}

// TODO: This does not set C->err which is a scalar, but perhaps should be an Error?
PEXL_API
pexl_Binary *pexl_compile (pexl_Context *C,
			   pexl_Ref main,	         // can be PEXL_NO_MAIN
			   pexl_Optims *optims,          // can be NULL
			   pexl_BinaryAttributes *attrs, // can be NULL
			   enum pexl_CharEncoding enc,
			   pexl_Error *err) {
  if (!err) {
    warn("pexl_compile", "Null error argument");
    return NULL;
  }
  else if (!C) {
    warn("pexl_compile", "Null context argument");
    *err = new_error(PEXL_ERR_NULL, -1);
    return NULL;
  }
  return compile(C, main, optims, attrs, enc, err);
}

PEXL_API
void pexl_free_Binary (pexl_Binary *pkg) {
  if (pkg) binary_free(pkg);
}

/* ----------------------------------------------------------------------------- */
/* Convenience functions                                                         */
/* ----------------------------------------------------------------------------- */
PEXL_API
pexl_Expr *pexl_match_epsilon (pexl_Context *C) {
  if (!C) return NULL;		// should 'warn' here, ideally
  return pexl_match_bytes(C, NULL, 0);
}

/* Arg is a null-terminated C string. */
PEXL_API
pexl_Expr *pexl_match_string (pexl_Context *C, const char *str) {
  size_t len = strnlen(str, PEXL_MAX_STRINGLEN + 1);
  return pexl_match_bytes(C, str, len);
}

PEXL_API
pexl_Expr *pexl_neg_lookbehind(pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *lookbehind = pexl_lookbehind(C, exp);
  return lookbehind ? pexl_neg_lookahead_f(C, lookbehind) : NULL;
}

PEXL_API
pexl_Expr *pexl_neg_lookbehind_f(pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *retval = pexl_neg_lookbehind(C, exp);
  pexl_free_Expr(exp);
  return retval;
}

PEXL_API
pexl_Expr *pexl_seq_f (pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2) {
  pexl_Expr *tree = pexl_seq(C, exp1, exp2);
  pexl_free_Expr(exp1);
  pexl_free_Expr(exp2);
  return tree;
}

PEXL_API
pexl_Expr *pexl_choice_f (pexl_Context *C, pexl_Expr *exp1, pexl_Expr *exp2) {
  pexl_Expr *tree = pexl_choice(C, exp1, exp2);
  pexl_free_Expr(exp1);
  pexl_free_Expr(exp2);
  return tree;
}

PEXL_API
pexl_Expr *pexl_neg_lookahead_f (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *tree = pexl_neg_lookahead(C, exp);
  pexl_free_Expr(exp);
  return tree;
}

PEXL_API
pexl_Expr *pexl_lookahead_f (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *tree = pexl_lookahead(C, exp);
  pexl_free_Expr(exp);
  return tree;
}

PEXL_API
pexl_Expr *pexl_lookbehind_f (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *tree = pexl_lookbehind(C, exp);
  pexl_free_Expr(exp);
  return tree;
}

PEXL_API
pexl_Expr *pexl_repeat_f (pexl_Context *C, pexl_Expr *exp,
			  pexl_Index min, pexl_Index max) {
  pexl_Expr *tree = pexl_repeat(C, exp, min, max);
  pexl_free_Expr(exp);
  return tree;
}

PEXL_API
pexl_Expr *pexl_capture_f (pexl_Context *C, const char *name, pexl_Expr *exp) {
  pexl_Expr *tree = pexl_capture(C, name, exp);
  pexl_free_Expr(exp);
  return tree;
}

PEXL_API
pexl_Expr *pexl_find_f (pexl_Context *C, pexl_Expr *exp) {
  pexl_Expr *tree = pexl_find(C, exp);
  pexl_free_Expr(exp);
  return tree;
}


PEXL_API
pexl_Expr *pexl_where_f (pexl_Context *C,
			 pexl_Expr *t1,
			 char *pathstring,
			 pexl_Expr *t2) {
  pexl_Expr *tree = pexl_where(C, t1, pathstring,  t2);
  pexl_free_Expr(t1);
  pexl_free_Expr(t2);
  return tree;
}

PEXL_API
void pexl_print_Match (pexl_Match *match) {
  pexl_print_Match_summary(match);
  pexl_print_Match_data(match);
}

PEXL_API
void pexl_print_Binary (pexl_Binary *pkg) {
  fprint_binary(stdout, pkg);
}

PEXL_API
void pexl_print_Env (pexl_Env env, pexl_Context *C) {
  if (!C) {
    confess("print", "Null context arg -- cannot print its environment\n");
    return;
  }
  print_env_internal(env, C->bt, C);
}

PEXL_API
void pexl_print_Expr (pexl_Expr *tree, int indent, pexl_Context *C) {
  Node *node;
  if (!tree)
    printf("NULL expression\n");
  else {
    printf("(%d node%s)\n", tree->len, tree->len == 1 ? "" : "s");
    node = &(tree->node[0]);
    printnode(node, indent, C);
  }
}

PEXL_API
void pexl_print_Match_summary (pexl_Match *match) {
  printf("Match %s\n", (match->end == -1) ? "failed" : "succeeded");
  printf("  end position    %ld\n", match->end);
  printf("  encoder_id      %d\n", match->encoder_id);
  printf("  match data      %p\n", match->data);
}

PEXL_API
void pexl_print_Match_data (pexl_Match *m) {
  switch (m->encoder_id) {
  case PEXL_NO_ENCODER:
    printf("No encoder, so no data\n");
    break;
  case PEXL_DEBUG_ENCODER:
    printf("Debug encoder, so no data\n");
    break;
  case PEXL_TREE_ENCODER:
    print_match_tree(m->data);
    break;
  default:
    confess("print", "No printer defined for encoder %d", m->encoder_id);
  }
}
