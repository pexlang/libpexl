/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  charset.h   Character set manipulation                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef charset_h
#define charset_h

#include "preamble.h"
#include "libpexl.h"

/* Decrease initial size if packages take too much memory */
#define CHARSETTABLE_INITSIZE       100
/*
  Maximum size limited by the table index, stored in instruction aux
  field, which is 24 bits unsigned.
*/
#define CHARSETTABLE_MAXSIZE        ((1 << 24) - 1)

/* Check platform characteristics.  We support only 8-bit bytes. */
#if CHAR_BIT != 8
  #error Size of character is not 8 bits
#endif

#define BITSPERCHAR CHAR_BIT
#define CHARSETSIZE ((UCHAR_MAX / BITSPERCHAR) + 1)

#if CHARSETSIZE != 32
  #error CHARSETSIZE is not 32 bytes
#endif

/*
  Alignment is required so we can cast to __m128i type for simd ops.
  FUTURE: CONSIDER having type aliases Charset_simd and Charset_simd8
  for this.
*/
typedef struct __attribute__((aligned(16))) Charset {
  uint32_t cs[8];
} Charset;

/* TODO: These charset ops could be functions; the compiler will inline them. */

/* access to charset */
#define treecharset(t) ((uint32_t *)((t->node) + 1))
#define nodecharset(t) ((uint32_t *)((t) + 1))
/* set 'b' bit in charset 'cs' */
#define setchar(cs, b) ((cs)[((uint8_t)b) >> 5] |= (((uint32_t) 1) << (((uint8_t)b) & 31)))
/* access the bit corresponding to character c (c is a byte) */
#define testchar(st, c) ((bool) (((st)[(((uint8_t)(c)) >> 5)] & (((uint32_t) 1) << ((uint8_t)(c) & 31)))))
/* fill st with 0 */
#define charset_fill_zero(st_ptr) do { 		\
    memset(st_ptr, 0, CHARSETSIZE);		\
  } while(0)
/* fill st with 0xFF */
#define charset_fill_one(st_ptr) do { 		\
    memset(st_ptr, 255, CHARSETSIZE);		\
  } while(0)
/* st = ~st */
#define charset_not(st) do {			\
    st[0] = ~st[0]; st[1] = ~st[1];		\
    st[2] = ~st[2]; st[3] = ~st[3];		\
    st[4] = ~st[4]; st[5] = ~st[5];		\
    st[6] = ~st[6]; st[7] = ~st[7];		\
  } while(0)
/* dest := src */
#define charset_copy(dest_ptr, src_ptr) do {	\
    memcpy(dest_ptr, src_ptr, 32);		\
  } while(0)
/* st1 |= st2 */
#define charset_or_eq(st1, st2) do { 		\
    st1[0] |= st2[0]; st1[1] |= st2[1];		\
    st1[2] |= st2[2]; st1[3] |= st2[3];		\
    st1[4] |= st2[4]; st1[5] |= st2[5];		\
    st1[6] |= st2[6]; st1[7] |= st2[7];		\
  } while(0)
/* st1 &= st2 */
#define charset_and_eq(st1, st2) do { 		\
    st1[0] &= st2[0]; st1[1] &= st2[1];		\
    st1[2] &= st2[2]; st1[3] &= st2[3];		\
    st1[4] &= st2[4]; st1[5] &= st2[5];		\
    st1[6] &= st2[6]; st1[7] &= st2[7];		\
  } while(0)
/* dest = st1 | st2 */
#define charset_or(dest, st1, st2) do {				\
    dest[0] = st1[0] | st2[0]; dest[1] = st1[1] | st2[1];	\
    dest[2] = st1[2] | st2[2]; dest[3] = st1[3] | st2[3];	\
    dest[4] = st1[4] | st2[4]; dest[5] = st1[5] | st2[5];	\
    dest[6] = st1[6] | st2[6]; dest[7] = st1[7] | st2[7];	\
  } while(0)

static Charset _fullset = {{-1, -1, -1, -1, -1, -1, -1, -1}};
__attribute__((unused)) static Charset *fullset = &_fullset;

typedef enum CharSetType
{
  CharSetEmpty,
  CharSetFull,
  CharSet1,
  CharSet8,			/* between 2 and 8 chars */
  CharSet,			/* between 9 and 255 chars */
  // MAYBE TODO: Add ranges as possible charset types
  // CharSetRange1,
  // CharSetRange2,
} CharSetType;

typedef struct CharsetTable
{
  Charset      *charsets;
  Charset      *simd_charsets; // NULL if no SIMD charsets
  pexl_Index    next;
  pexl_Index    capacity;
} CharsetTable;

void        cs_complement(Charset *cs);
bool        cs_equal (Charset *cs1, Charset *cs2);
int         cs_disjoint(Charset *cs1, Charset *cs2);

CharSetType charsettype (Charset *cs, char *c1);

CharsetTable *charsettable_new (size_t initial_size);
void          charsettable_free (CharsetTable *cst);
pexl_Index    charsettable_add (CharsetTable *cst);
void          charsettable_set_no_simd (CharsetTable *cst);
int           charsettable_has_simd (CharsetTable *cst);
int           charsettable_set_charset (CharsetTable *cst, pexl_Index n, Charset *cs);
int           charsettable_set_simd_charset (CharsetTable *cst, pexl_Index n, Charset *cs);
Charset      *charsettable_get_charset (CharsetTable *cst, pexl_Index n);
Charset      *charsettable_get_simd_charset (CharsetTable *cst, pexl_Index n);

Charset *charsettable_iter(CharsetTable *cst, pexl_Index *n);

/* ----------------------------------------------------------------------------- */
/* Error and other codes                                                         */
/* ----------------------------------------------------------------------------- */

#define CHARSETTABLE_ERR_OOM       -1  /* out of memory */
#define CHARSETTABLE_ERR_FULL      -2  /* reached max number of entries */ 
#define CHARSETTABLE_ERR_NULL      -3  /* null charset table arg */ 
#define CHARSETTABLE_ERR_FROZEN    -4  /* cannot add any more entries */ 
#define CHARSETTABLE_ERR_INDEX     -5  /* index out of range */

#endif
