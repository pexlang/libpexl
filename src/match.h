/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  match.h     Ordered n-ary tree for match results                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef match_h
#define match_h

#include "preamble.h"
#include "libpexl.h"
#include "capture.h"

/* 
   A MatchTree is an ordered tree, encoded in a single linear chunk of
   memory.  A node is followed directly by its first child, i.e. the
   tree nodes are stored in pre-order (root followed by subtrees).

   E.g.	   A
	  / \
	 B  D
	/
       C

   The tree is stored with the root first.  A leaf has nsubs=0.  Below
   is the node array for the tree above, where 'A' is the data for
   node 'A' and nsubs is shown in the diagram:

   Node array: [A 2][B 1][C 0][D 0]

*/

#define MATCHTREE_MAXNODES     (1 << 24)
#define MATCHTREE_INITCAPACITY 300

#if MATCHTREE_MAXNODES > pexl_Index_MAX
  #error "MATCHTREE_MAXNODES must fit into pexl_Index type"
#endif
#if MATCHTREE_MAXNODES > SIZE_MAX
  #error "MATCHTREE_MAXNODES must fit into size_t"
#endif

struct pexl_MatchNode {
  pexl_Position start;         /* Start position, 0-based */
  pexl_Position end;           /* End position, 0-based */
  pexl_Index    prefix;        /* -1 OR offset into match->block */
  pexl_Index    type;          /* -1 OR offset into match->block */
  pexl_Index    inserted_data; /* -1 OR offset into match->block */
};

// FUTURE:
// Maybe break out last_child into separate array used only during tree construction?
typedef struct match_node {
  struct pexl_MatchNode data;
  pexl_Index            sibling;    /* Index of next sibling, 0 for none */
  pexl_Index            last_child; /* Index of last child, 0 for none */
} match_node;

typedef struct pexl_MatchTree {
  match_node *nodes;    /* Resizeable array of nodes */
  size_t      size;     /* Number of nodes in use */
  size_t      capacity; /* Allocated space for 'capacity' nodes */
  char       *block;    /* Block of null-terminated strings */
  size_t      blocklen;
} pexl_MatchTree;

pexl_MatchTree *match_tree_new   (size_t capacity);
void            match_tree_reset (pexl_MatchTree *m);
void            match_tree_free  (pexl_MatchTree *m);

/* ----------------------------------------------------------------------------- */
/* Explore a tree                                                                */
/* ----------------------------------------------------------------------------- */

// The tree node pointers returned by the functions below will remain
// valid until the pexl_MatchTree is freed.

// Get the first child of 'parent'
pexl_Index match_tree_child (pexl_MatchTree *match, pexl_Index parent, match_node **node);
// Get the next sibling of 'prev'
pexl_Index match_tree_next (pexl_MatchTree *match, pexl_Index prev, match_node **node);
// Pre-order traversal, returning next node after 'prev'
pexl_Index matchtree_iter (pexl_Match *match, pexl_Index prev, pexl_MatchNode **node);

// Get an arbitrary tree node at 'index'.
// Internal function returning internal 'match_node' structure.
match_node *get_node (pexl_MatchTree *match, pexl_Index index);

/* ----------------------------------------------------------------------------- */
/* Build a tree, in pre-order                                                    */
/* ----------------------------------------------------------------------------- */

// Add the next node
pexl_Index match_tree_add (pexl_MatchTree *m, pexl_Index parent_idx,
			  pexl_Index prefix, pexl_Index type, pexl_Position start);

// Signal the end of the current list of children
// The next node added will be a sibling of the parent
int match_tree_close (pexl_MatchTree *match, pexl_Index node_idx,
		     pexl_Index data, pexl_Position end);

// REMOVED ON Sunday, May 19, 2024 BECAUSE capture.c IS NOT USING IT NOW:
// Close out the entire tree after the last node has been added
//int match_tree_close_all (pexl_MatchTree *m, pexl_Index node_idx, pexl_Position pos);


#define MATCHTREE_OK            0  /* Success must be code zero */
#define MATCHTREE_ERR_OOM      -1  /* Reallocation failed */
#define MATCHTREE_ERR_CORRUPT  -2  /* Corrupt input buffer */
#define MATCHTREE_ERR_VERSION  -3  /* Unsupported encoding version */
#define MATCHTREE_ERR_NULL     -4  /* Required arg is null */
#define MATCHTREE_ERR_INTERNAL -5  /* Bug */
#define MATCHTREE_ERR_FULL     -6  /* Structure is full */
#define MATCHTREE_ERR_NOEXIST  -7  /* Requested index does not exist */


#endif
