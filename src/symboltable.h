/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  symboltable.h  String/symbol table (ELF style)                           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef symboltable_h
#define symboltable_h

#include "preamble.h"
#include "libpexl.h"
#include "metadata.h"
#include "table.h"
#include "pstring.h"

/* FUTURE:
   
   - MAYBE use symtab index 0 to mean "undefined symbol" (as is done
     in ELF and other object formats, so that we can used an unsigned
     type as an index.

   - Would be useful (for efficiency) to use a hash table while
     building the symbol table.  We can trash it with some kind of
     symboltable_close() call that will close it to modifications and
     retain only the table entries and the string storage.
 */


/*
  String table:

  In the ELF executable format (and others), a STRING TABLE is a
  contiguous block of storage containing null-terminated strings.  A
  SYMBOL TABLE is an array of indexes into that block (usually with
  per-symbol meta-data).

  The string encoding is not specified here, because this code does
  not need to know -- as long as strings do not contain any null
  bytes, that is.

  By convention, the first byte of the block is always 0, so that
  index 0 uniquely stores the empty string (or the absence of a
  string, depending on the context).

  Valid block indexes are in the range: [0, SYMBOLTABLE_MAXBLOCKSIZE - 1].

  With a SYMBOL TABLE of size N:
   - Each stored string gets a number (array index) in [0, N-1].
   - Only logN bits are needed to store a string number, whereas the
     index into the block of the start of a string requires log(N+1)
     bits in the best case (1-character strings each with a NUL
     terminator) and log(N+M) bits in general, when stored strings
     have length M.
   - The empty string is always in the block, but may or may not be in
     the string table, depending on whether there is a k such that
     A[k] = 0.
   - As with the ELF symbol table format, having a separate index
     array makes it easy to store symbol meta-data.

*/

/*
  These INIT values can be tuned for performance
*/
#define SYMBOLTABLE_INITSIZE       500 /* number of symbols */
#define SYMBOLTABLE_INITBLOCKSIZE 4100 /* number of bytes */

/*
  Maximum number of symbol table entries MUST fit into 24 bits so that
  we can always encode a symbol table index in 3 bytes in intermediate
  representations and vm instructions.
*/
#define SYMBOLTABLE_MAXSIZE ((1 << 22) - 1)

/*
  The maximum value of a string handle must fit into an unsigned 24
  bit field.  (See SymbolTableEntry struct definition.)
*/
#define SYMBOLTABLE_MAXHANDLE ((1 << 24) - 1)

/*
  A single string in the string storage block can be up to
  SYMBOLTABLE_MAXLEN bytes long.
*/
#define SYMBOLTABLE_MAXLEN PEXL_MAX_STRINGLEN

typedef enum Symbol_Namespace {
  symbol_ns_id = 0,
  symbol_ns_data
} Symbol_Namespace;

typedef enum Symbol_Visibility {
  symbol_local = 0,
  symbol_global
} Symbol_Visibility;

typedef enum Symbol_Type {
  symbol_type_pattern = 0,
  symbol_type_string,		/* string literal */
  symbol_type_capname,		/* capture name */
  symbol_type_import,		/* importpath */
} Symbol_Type;

/*
  With a 24-bit limit on the number of symbols in a single symbol
  table, we can afford to use a signed int as an index, which means we
  can return error codes as negative numbers when needed.  So we can
  use pexl_Index, which is a synonym for int32_t.
*/

// FUTURE: Add to SymbolTableEntry a pointer or index to the package
// from which the entry comes?

typedef struct SymbolTableEntry {
  uint32_t        handle     : 24; // name (index into string block)
  uint32_t        visibility : 1;  // Symbol_Visibility
  uint32_t        namespace  : 1;  // Symbol_Namespace
  uint32_t        type       : 6;  // Symbol_Type
  uint32_t        size;	           // Size of the thing stored at 'value'
  pexl_Index      value;           // Meaning depends on Symbol_Type
  PatternMetaData meta;	           // See metadata.h
} SymbolTableEntry;

typedef struct SymbolTable {
  Table        *entries;
  StringBuffer *block;	   // Storage for strings (see 'handle' above)
} SymbolTable;

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

SymbolTable *symboltable_new (size_t initial_size, size_t initial_blocksize);
void         symboltable_free (SymbolTable *st);
pexl_Index   symboltable_add (SymbolTable *st,
			      const string *name,
			      bool visible,
			      Symbol_Namespace ns,
			      Symbol_Type type,
			      uint32_t size,
			      pexl_Index value,
			      PatternMetaData *meta);
pexl_Index symboltable_add_data (SymbolTable *st,
				 const string *name,
				 bool visibility,
				 Symbol_Type type,
				 uint32_t size,
				 pexl_Index value);
size_t symboltable_len (SymbolTable *st);
size_t symboltable_blocklen (SymbolTable *st);

SymbolTableEntry *symboltable_get (SymbolTable *st, pexl_Index index);
SymbolTableEntry *symboltable_iter (SymbolTable *st, pexl_Index *prev); 
SymbolTableEntry *symboltable_iter_ns (SymbolTable *st, Symbol_Namespace ns, pexl_Index *prev); 
SymbolTableEntry *symboltable_search(SymbolTable *st, Symbol_Namespace ns, const string *name_string, pexl_Index *index);
const string *symboltable_get_name (SymbolTable *st, pexl_Index index);
const string *symboltable_entry_name (SymbolTable *st, SymbolTableEntry *entry);
const string *symboltable_block_iter (SymbolTable *st, pexl_Index *prev);

#define index_is_emptystring(i) ((i)==0)

#endif
