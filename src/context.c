/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  context.c    Structures for building expressions and compiling           */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "context.h"
#include "env.h"
#include "pattern.h"		/* cfgraph_free() */
#include "compile.h"
#include "opt.h"

/* ----------------------------------------------------------------------------- */
/* Compilation state structure                                                   */
/* ----------------------------------------------------------------------------- */

CompState *compstate_new (void) {
  CompState *cs;
  cs = malloc(sizeof(CompState));
  if (!cs) {
    warn("compstate_new", "out of memory");
    return NULL;
  }
  cs->pat = NULL;
  cs->g = NULL;
  cs->err_index = 123456;	/* helps catch uninitialized usage */
  return cs;
}

void compstate_free (CompState *cs) {
  if (!cs) return;
  /* Leave pattern alone; free only the transient compilation state. */
  if (cs->g) cfgraph_free(cs->g);
  free(cs);
}

/* ----------------------------------------------------------------------------- */
/* pexl_Context structure for building and compiling expressions                      */
/* ----------------------------------------------------------------------------- */

pexl_Context *context_new (void) {
  pexl_Context *C = malloc(sizeof(pexl_Context));
  if (!C) goto oom1;
  
  C->bt = new_BindingTable();
  if (!C->bt) goto oom2;

  C->stringtab = hashtable_new(0, 0); // Use default sizes
  if (!C->stringtab) goto oom3;

  C->packages = packagetable_new(0);  // Use default size
  if (!C->packages) goto oom4;

  C->pathtab = pathtable_new(0);      // Use default size
  if (!C->pathtab) goto oom5;

  C->env = PEXL_ROOT_ENV;
  C->cst = NULL;
  C->err = PEXL_OK;

  return C;

 oom5:
  packagetable_free(C->packages);
 oom4:
  hashtable_free(C->stringtab);
 oom3:
  free_BindingTable(C->bt);
 oom2:
  free(C);
 oom1:
  return NULL;
}

/* Free all patterns, bindings, and table itself */
static void free_bt_and_contents (BindingTable *bt) {
  Binding *b;
  pexl_Index i;
  if (!bt) return;
  for (i = 0; i < bt->next; i++) {
    b = bt_get(bt, i);
    assert(b);
    free_bound_value(b);
  } /* for each binding */
  free_BindingTable(bt);
}

void context_free (pexl_Context *C) {
  if (!C) return;
  if (C->cst) compstate_free(C->cst);
  if (C->stringtab) hashtable_free(C->stringtab);
  if (C->packages) {
    packagetable_free_packages(C->packages);
    packagetable_free(C->packages);
  }
  if (C->pathtab) pathtable_free(C->pathtab);
  // IMPORTANT: Below, we free the bound objects (e.g. patterns),
  // which the lower-level function free_BindingTable() does not do.
  if (C->bt) free_bt_and_contents(C->bt);
  free(C);
}

/*
  Return a persistent unique identifier in the string table for this
  'name', or a value < 0 if an error occurred.  If the string has been
  interned, the existing handle will be returned.
*/

pexl_Index context_intern (pexl_Context *C, const string *name, size_t len) {
  assert(C && C->stringtab);
  if (len > PEXL_MAX_STRINGLEN) {
    warn("context", "string length exceeds PEXL_MAX_STRINGLEN");
    return PEXL_EXP__ERR_LEN;
  }
  HashTableData nodata = {.ptr = NULL};
  pexl_Index index = hashtable_add_update(C->stringtab, name, len, nodata);
  if (index < 0) {
    switch (index) {
      case HASHTABLE_ERR_NULL: return PEXL_ERR_NULL;
      case HASHTABLE_ERR_OOM: return PEXL_ERR_OOM;
      default:
	warn("context", "unexpected error %d from hash table", index);
	return PEXL_ERR_INTERNAL;
    }
  }
  HashTableEntry entry = hashtable_get(C->stringtab, index);
  pexl_Index handle = hashtable_get_keyhandle(entry);
  assert(handle >= 0);
  return handle;
}

const string *context_retrieve (pexl_Context *C, pexl_Index keyhandle, size_t *lenptr) {
  assert(C && C->stringtab && lenptr);
  int32_t len = 0;
  const string *ptr = hashtable_get_key(C->stringtab, keyhandle, &len);
  if (len < 0) {
    warn("context", "invalid string length returned from hashtable");
    return NULL;
  }
  *lenptr = (size_t) len;
  return ptr;
}




