/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  opt-inline.c                                                            */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Vivek Reddy Karri, Kiran Rao, Jamie A. Jennings                */

#include "opt.h"
#include "print.h"

/* ----------------------------------------------------------------------------- */
/* OPTIMIZERS                                                                    */
/* ----------------------------------------------------------------------------- */

// Wednesday, July 12, 2023:
// -------------------------
// Now that inlining optimizer has been fixed, it relies this
// property: The code for a pattern is self-contained in that all
// jumps within it have target addresses within it.  The peephole
// optimizer is global, i.e. it ignores pattern boundaries.  We cannot
// invoke the peephole optimizer before any optimizer that relies on
// this property.  (Generally, peephole should run last.)  The reason
// I added two rounds of peephole to kjv-libpexl.c, before and after
// the inliner, is because peephole removes tail recursion, and we
// need that because the inliner (currently) refuses to inline
// recursive patterns.  The fix is to implement tail recursion
// optimization (tail recursion elimination) as a separate optimizer.
// Because TRO would operate only within, not across, pattern code
// boundaries, it will preserve the property needed by the inliner.

/*
  Policy governs the amount of inlining.  It should return the size of
  the chunk to be inlined.  Return OK if nothing has to be inlined.
*/
uint32_t naive_inlining_policy (pexl_Binary *pkg, int i) {
  int vec_offset;
  SymbolTableEntry *ste = NULL;
  pexl_Index index = PEXL_ITER_START;

  if (opcode(&pkg->code[i]) != ICall)
    return OK;

  vec_offset = getaddr(&pkg->code[i]);

  while ((ste = symboltable_iter_ns(pkg->symtab, symbol_ns_id, &index))) 
    if (ste->value == vec_offset) return ste->size;
  
  return OK;
}

/*
  Do the inlining, if possible.  Here, i is the current address as we
  process the entire vector.  Return PEXL_OK (0) for success, or a
  PEXL error code otherwise.
*/
static int inliner (pexl_Binary *pkg, int i, uint32_t inline_size) {
    size_t num_shift;
    uint32_t new_idx;
    Instruction *temp_vector, *to, *from;
    uint32_t size_of_ICall = OPCODE_SIZE[ICall];
    
    if (opcode(&pkg->code[i]) != ICall) return PEXL_OK;
    if (inline_size == 0) return PEXL_OK;
   
    /* Offset from where the Instructions have to be inlined */
    new_idx = getaddr(&pkg->code[i]);

    inline_size--; /* Leave out the last Return Instruction */

    /* Print useful debugging info if this consistency check fails: */
    if (opcode(&pkg->code[new_idx+inline_size]) != IRet) {
      confess("inliner", "failed to find expected IRet: new_idx, inline_size = %d, %d\n", new_idx, inline_size);
      fprint_binary(stderr, pkg);
    }
    /* Confirm that the last instruction is a Return */
    assert(opcode(&pkg->code[new_idx+inline_size]) == IRet); 

    int extra_space = inline_size - size_of_ICall;
    if (!ensure_binary_size(pkg, extra_space))
      return PEXL_ERR_OOM;
    
    /* Have the vector (which is to be inlined) stored in a different address */
    temp_vector = (Instruction *)malloc(sizeof(Instruction)*inline_size);
    memcpy(temp_vector, &pkg->code[new_idx], inline_size*sizeof(Instruction));

    /* Shift the instructions */
    to = &pkg->code[i+inline_size]; /* Since the Call instruction also have to be replaced */ 
    from = &pkg->code[i+size_of_ICall]; /* From the next instruction after the call instruction at i*/
    num_shift = pkg->codenext-i-size_of_ICall;
    memmove(to, from, num_shift*sizeof(Instruction));

    assert(size_of_ICall == 2);
    int32_t delta = inline_size - size_of_ICall;

    /* Copy the inline Instructions */
    memcpy(&pkg->code[i], temp_vector, inline_size*sizeof(Instruction));
    
    if (delta != 0) {
      /* Adjust jump & call targets due to the newly inlined instructions in [i, i+delta] */
      pkg->codenext += delta;
      adjust_ins_offset(pkg, i, delta); 
      adjust_symbol_table(pkg, i, delta);
    }   
    free(temp_vector);

    return PEXL_OK;
} 

/*
   Check for Recursion: Return OK if no recursion and !OK if has
   recursion.

   TODO: Change this to check the symbol table after we add this info 
   to the flags stored there.
*/
// TODO: Simplify this function!
static int has_recursion (pexl_Binary *pkg, pexl_Index i, int inline_size)
{
  Instruction *start, *pc;
  int start_location, ind;

  trace("has_recursion", "looking at address %d with inline_size of %d",
	i, inline_size);

  /* && !(i >= getaddr(pc) && i < getaddr(pc)+inline_size) */
  if (opcode(&pkg->code[i]) != ICall)
    return OK;

  start = pkg->code; /* start address */
  pc = NULL;

  /* Location from which the instructions have to be inlined */
  start_location = getaddr(&pkg->code[i]);
  ind = start_location;

  while (ind < start_location + inline_size)
  {
    pc = &start[ind];
    switch (opcode(pc))
    {
    case ICall:
    {
      if (getaddr(pc) == start_location)
      { /* Found Recursion */
        return !OK;
      }
      break;
    }
    default:
      break;
    }
    ind += sizei(pc);
  }
  return OK;
}

/*
  Inline the instructions, when policy returns true for a call
  instruction.  We ignore the symbol table, and simply scan through
  the code vector from top (0) to bottom (pkg->codenext), looking for
  opportunities.
  Return PEXL_OK (0) for success, or a PEXL error otherwise.
*/
int inlining_optimizer (pexl_Binary *pkg, inlining_policy_fn policy) {

  uint32_t i = 0;
  Instruction *pc = NULL;
  uint32_t inline_size = 0;
  int recur_check;
  int stat;

  while (i < pkg->codenext) {
    pc = &pkg->code[i];
    if (opcode(pc) == ICall) {
      inline_size = policy(pkg, i);
      trace("optimizer", "inline policy function: code size of %u at addr %u",
	    inline_size, i);
      WHEN_TRACING fprint_binary(stderr, pkg);
      /* TODO: Allow Recursion for certain depth */
      recur_check = has_recursion(pkg, i, inline_size);
      if (recur_check)
	trace("codgen", "inliner detected recursive pattern called at address %d", i);
      if (recur_check == OK && inline_size > 1)
      {
        stat = inliner(pkg, i, inline_size);
	if (stat < 0) return stat;
        continue;
      }
    }
    i += sizei(pc);
  } // while
  return PEXL_OK;
}

