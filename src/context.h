/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  context.h      Compile expressions and environments                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef context_h
#define context_h

#include "preamble.h"
#include "libpexl.h"

#include "env.h"
#include "scc.h"
#include "pattern.h"
#include "hashtable.h"
#include "expression.h"

/* ----------------------------------------------------------------------------- */
/* Compilation state (internal)                                                  */
/* ----------------------------------------------------------------------------- */

typedef struct CompState {
  Pattern    *pat;	 // pattern being compiled
  CFgraph    *g;	 // control flow graph
  pexl_Index  err_index; // index in CFgraph of pattern causing an error
} CompState;

CompState *compstate_new (void);
void       compstate_free (CompState *cs);

/* ----------------------------------------------------------------------------- */
/* pexl_Context (structure needed for building and compiling expressions         */
/* ----------------------------------------------------------------------------- */

struct pexl_Context {
  CompState           *cst;       // set by compile_(), later freed by link_()
  BindingTable        *bt;        // created by context_new()
  HashTable           *stringtab; // created by context_new()
  PathTable           *pathtab;   // created by context_new()
  pexl_PackageTable   *packages;  // created by context_new()
  pexl_Env             env;       // current compilation environment
  int                  err;       // last error, sort of like errno
};

/* Interning strings */
pexl_Index  context_intern (pexl_Context *C, const char *name, size_t len);
const char *context_retrieve (pexl_Context *C, pexl_Index offset, size_t *len);

/* pexl_Context create/destroy */
pexl_Context *context_new (void);
void          context_free (pexl_Context *C);

/* Misc */
bool env_in_context (pexl_Env env, pexl_Context *C);

#endif
