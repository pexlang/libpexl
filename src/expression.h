/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  expression.h                                                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef expression_h
#define expression_h

#include "preamble.h"
#include "libpexl.h"
#include "charset.h"
#include "pathtable.h"

/* number of children for each kind of node */
static const pexl_Byte numchildren[] = {
    0, 0, 0, /* bytes, set, any */
    0, 0,    /* true, false */
    1, 2, 2, /* rep, seq, choice */
    1, 1,    /* not, ahead */
    0, 0, 0, /* call, external call, open xcall */
    1,       /* behind */
    1,       /* capture */
    2,       /* function (run-time execution) */
    1,       /* find */
    2,       /* where */
    0,       /* backref */
};

/* Types of nodes (stored in expression->tag) */
typedef enum TTag
{
  TBytes = 0, /* match a non-empty byte sequence */
  TSet,       /* match 1 char (byte) from a set */
  TAny,       /* skip ahead one char (byte) */
  TTrue,      /* no-op */
  TFalse,     /* fail */
  TRep,       /* 0 or more repetitions of expression */
  TSeq,       /* sequence of two expressions */
  TChoice,    /* ordered choice of two expressions */
  TNot,       /* negation: fail if expression succeeds & vice-versa */
  TAhead,     /* look-ahead at expression */
  TCall,      /* call to target in current package */
  TXCall,     /* call to target in a different package */
  TOpenCall,  /* call whose target has not yet been resolved */
  TBehind,    /* look-behind at expression */
  TCapture,   /* capture (various kinds) */
  TFunction,  /* dispatch to primitive function, e.g. backref */
  TFind,      /* 'find' operator */
  TWhere,     /* 'where' operator */
  TBackref    /* 'backref' operator */
} TTag;

/*
  A pexl_Expr has a tree structure made of Nodes.  A node has 0, 1, or
  2 children.  The entire tree is stored in an array of Nodes.

  The first child of a node, if there is one, is immediately after the
  node, i.e. at the node address + 1.  The second child of a node, if
  there is one, is found at the node address + ps.
*/

typedef struct Node {
  union {
    struct BindingTable *bt;  /* TOpencall, which uses 'index' also */
    struct pexl_Binary  *pkg; /* TXCall, which uses 'index' also */
    struct Pattern      *pat; /* TCall */
    pexl_Index           stringtab_handle; /* TStr, and TCapture when cap=Cconstant */
    pexl_Index           path_handle;      /* TWhere */
  } a;
  union {
    pexl_Index           index; /* occasional index into binding table */
    pexl_Index           ps;    /* occasional second child node index */
    pexl_Index           n;     /* occasional other value */
  } b;
  pexl_Byte              tag; /* one of the TTag entries */
  pexl_Byte              cap; /* kind of capture (if it is a capture) */
} Node;

struct pexl_Expr
{
  uint32_t len;   /* Number of Nodes, i.e. length of node[] */
  uint32_t depth; /* Tree depth of Node */
  Node node[1];   /* Must be last, because it will grow via realloc */
};

/* access to child nodes */
#define child1(t) ((t) + 1)
#define child2(t) ((t) + (t)->b.ps)

/* number of slots needed for 'n' bytes */
#define bytes2slots(n) (((n)-1) / sizeof(Node) + 1)

#define set_ref_from_node(r, node) do { \
    (r) = (node)->b.index;		\
  } while (0);

#define set_node_ref(node, r) do { \
    (node)->b.index = (r);	   \
  } while (0);

/* A NULL pkg and an index >= 0 is a corrupt reference (should not occur) */
#define xref_error(pkg, idx) (((pkg) == NULL) && ((idx) < 0))

#define set_xref_from_node(pkg, sym, node) do {	\
    (pkg) = (node)->a.pkg;			\
    (sym) = (node)->b.index;			\
  } while (0);

#define set_node_xref(node, pkg, sym) do {	\
    (node)->a.pkg = (pkg);			\
    (node)->b.index = (sym);			\
  } while (0);

/* Maximum length of strings that 'from_string' or 'from_bytes' will accept. */
#define EXP_MAXSTRING PEXL_MAX_STRINGLEN
#if EXP_MAXSTRING > PEXL_MAX_STRINGLEN
#error Maximum byte string length exceeds limit
#endif


/* ----------------------------------------------------------------------------- */
/* Expression builders */
/* ----------------------------------------------------------------------------- */

pexl_Expr *copy_expr (pexl_Expr *tree);
void       free_expr (pexl_Expr *tree);
pexl_Expr *fail_expr (int *err);
pexl_Expr *match_bytes (pexl_Index id, const char *ptr, size_t len, int *err);
pexl_Expr *match_any (int n, int *err);
pexl_Expr *match_set (const char *set, size_t len, bool complement, int *err);
pexl_Expr *match_range (pexl_Byte from, pexl_Byte to, int *err);
pexl_Expr *seq (pexl_Expr *exp1, pexl_Expr *exp2, int *err);
pexl_Expr *choice (pexl_Expr *exp1, pexl_Expr *exp2, int *err);
pexl_Expr *neg_lookahead (pexl_Expr *exp, int *err);
pexl_Expr *lookahead (pexl_Expr *exp, int *err);
pexl_Expr *lookbehind (pexl_Expr *exp, int *err);
pexl_Expr *repeat (pexl_Expr *exp, pexl_Index min, pexl_Index max, int *err);
pexl_Expr *capture (pexl_Index handle, pexl_Expr *exp, int *err);
pexl_Expr *insert (pexl_Index name_handle, pexl_Index value_handle, int *err);
pexl_Expr *find (pexl_Expr *exp, int *err);
pexl_Expr *backreference(pexl_Index handle, int *err);
pexl_Expr *where (pexl_Expr *exp1,
		  char *pathstring,
		  pexl_Expr *exp2,
		  PathTable *pathtab,
		  int *err);


#define MAX_REPETITIONS (1 << 10)
pexl_Expr *ntimes (pexl_Expr *exp, pexl_Index n, int *err);
pexl_Expr *atleast (pexl_Expr *exp, pexl_Index n, int *err);
pexl_Expr *atmost (pexl_Expr *exp, pexl_Index n, int *err);

pexl_Expr *call (pexl_Ref ref, struct BindingTable *bt, int *err);
pexl_Expr *xcall (pexl_Binary *pkg, pexl_Index symbol, int *err);

int to_charset(Node *node, struct Charset *cs);



/* TODO: Change backreference to use TFunction */
/* EXPERIMENTAL */
/* pexl_Expr *pexl_backreference (pexl_Env env, const char *capname); */

#endif
