/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  stringtable.h     For interning strings                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef stringtable_h
#define stringtable_h

#include "preamble.h"
#include "libpexl.h"

#include "hashtable.h"

/*
  Minimum "expected entries".  Table expands as needed.
*/
#define STRINGTABLE_MIN_ENTRIES 100
/*
  Average "expected string length" value. Note: This value is only
  used when creating a new string table, not when expanding one.
*/
#define STRINGTABLE_AVG_LEN 9

typedef HashTable StringTable;
typedef HashTableEntry StringTableEntry;
typedef HashTableData StringTableData;

StringTable *stringtable_new (size_t exp_entries, size_t exp_string_size);
void         stringtable_free (StringTable *st);

pexl_Index   stringtable_intern (StringTable *st, const char *name, size_t len);
const char  *stringtable_retrieve (StringTable *st, int32_t key, size_t *len);

#endif
