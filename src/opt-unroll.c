/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  opt-unroll.c                                                            */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Tristan Mullins, Jamie A. Jennings                             */

#include "opt.h"
#include "print.h"

/*
  Unrolls loops created by ILookBehind when the aux and offset fields
  are unequal.  These are the min and max positions in the input at
  which the vm must check to see if the lookbehind "target pattern"
  matches.  They correspond to the min and max length of the target
  pattern; an unbounded pattern has a max length set to 0.

  The ILookBehind operation loops by moving the current position back
  by the min amount, then decrementing the current position in the
  input by 1 after each failed attempt to match the given pattern. The
  ILookBehind fails if the pattern is not matched, and also if it is
  matched but the match does not end at the position at which the
  lookbehind was invoked.

  The method of unrolling an ILookBehind loop is to break it into a
  set of IChoices. The IChoice for iteration n causes the vm to jump
  to the start of iteration n+1 on any instruction failure.

  Each iteration is an ILookBehind with both fields of the ILookBehind
  set to be the current position to analyze for a match (i.e
  non-looping).  On success, the lookbehind is committed, and then the
  IChoice guarding this iteration is committed.  That ICommit includes
  a jump to the first address after the unrolled loop, where execution
  continues.

  TODO: Each unrolled iteration, except possibly the last, has an
  ILookBehind instruction with min == max.  That is, it examines only
  one input position at a known offset from the current position
  (where the lookbehind was invoked).  ILookBehind sets up some
  structure on the stack for a loop, but in these cases, A LOOP IS NOT
  NEEDED.  WE MUST REPLACE THESE WITH SIMPLER INSTRUCTIONS, ELSE THE
  UNROLLED LOOP IS UNLIKELY TO PERFORM BETTER.  E.g. Use 'IBehind n'
  where n = min.  We will have to disable captures at the start of the
  lookbehind (once, at the top), however, and then re-enable them
  afterwards (at the Continue label in the example below).

  Example of lookbehind loop unrolling:
  -------------------------------------
  Before Unrolling:

      ILookBehind min -1 max -3
      <TARGET>
      ILookBehindCommit
    Continue:
      Remaining pattern code

  After Unrolling:

    Iter1:
      IChoice Iter2
      ILookBehind min_delta min_delta
      <TARGET>             // On fail, jump to Iter2       
      ILookBehindCommit
      ICommit and jump to Continue

    Iter2:
      IChoice Iter3
      ILookBehind min_delta + 1 min_delta + 1
      <TARGET>            // On fail, jump to Iter3
      ILookBehindCommit
      ICommit and jump to Continue

    Iter3:
      ILookBehind min_delta + 2 min_delta + 2
      <TARGET>            // On fail, pattern fails
      ILookBehindCommit

    Continue:
      Remaining pattern code

*/


/*
  Adjust the offsets of Jump/Call instructions in the entire code
  vector after loop unrolling.  Jumps are RELATIVE.  Calls are to
  ABSOLUTE addresses. 

 |-------------------------------| CASE 1:
 | 0: first instruction          | <--- - Jumps ahead (+) to after 'first'
 |                               |        need delta added to them
 | other code                    |      - Jumps behind (-) are not modified
 | ...                           |      - Calls to after 'first' need to have
 |                               |        delta added to them
 |-------------------------------| CASE 2:
  ** first: (was ILookBehind)  **  <--- Assert: jumps in this region are correct
  ** ...                       **       Assert: call targets are NOT in this region
  ** newly generated code      **       - Calls to after 'first' need to have 
  ** ...                       **         delta added to them
  ** last loop instruction     **       
 |-------------------------------| CASE 3:
 | next = start+delta:          | <--- - Jumps ahead (+) are not modified
 | ...                           |      - Jumps behind (-) to last loop instruction
 | other code                    |        or earlier need delta subtracted
 | ...                           |      - Calls to after 'first' need to have
 |                               |        delta added to them
 | pkg->codenext: (end of code)  |
 |-------------------------------|         
*/

// TODO: Use this function (generalized as needed) for inlining, to
// replace adjust_ins_offset defined there.
static void adjust_targets (pexl_Binary *pkg, uint32_t first, int32_t delta) {
    Instruction *start = pkg->code;
    Instruction *pc = NULL;
    uint32_t abs_addr, new_offset;
    uint32_t idx = 0;
    uint32_t size_of_ICall = OPCODE_SIZE[ICall];
    uint32_t next = first + delta;

    assert(next > first);

    WHEN_TRACING {
      fprintf(stderr, "[adjust] Adjusting jumps due to newly extended block [%d, %d]\n",
	      first, first+delta+size_of_ICall);
      fprintf(stderr, "          Block ends with %s at addr %d\n",
	      OPCODE_NAME[opcode(&start[first+delta+size_of_ICall])],
	      first+delta+size_of_ICall);
    }
    while(idx < pkg->codenext){
        pc = &start[idx];
        switch(opcode(pc)){
	case ICall: {
	  /* Call to absolute address */
	  if (((uint32_t) getaddr(pc)) > first)
	    setaddr(pc, getaddr(pc) + delta);
	  break;
	}
	  /* Jump relative to PC */
	case IJmp: case IChoice:
	case ICommit: case IPartialCommit:
	case ITestAny: case ITestChar: case ITestSet: {
	  abs_addr = relative_to_abs(start, pc);
	  /* CASE 1: Current index is BEFORE first */
	  if (idx < first) {
	    /* If the jump destination is after first, adjust it */
	    if (abs_addr > first) { 
	      new_offset = getoffset(pc) + delta; 
	      setoffset(pc, new_offset); 
	    }
	  } else if ((idx >= first) && (idx < next)) {
	    /*
	      Case 2: Current index is WITHIN the extended block.
	      Since we generated this block, we assume that any jumps
	      inserted here have correct jump amounts.
	      TODO: Correct calls in here, or not necessary?
	    */
	  } else {
	    /* Case 3: Current index is AFTER the inlined block */
	    assert(idx >= next);
	    /* Jumping to an addr after or inside the inlined block:
	       Need to adjust the jump offset because jumps are relative. */
	    if (abs_addr < next) {
	      new_offset = getoffset(pc) - delta; 
	      setoffset(pc, new_offset); 
	    } 
	  } /* cases of idx */
	  break;
	} /* all jump instructions */
	default: break;
	} /* switch on opcode */
	idx += sizei(pc);
    } /* while */
}

/*  FUTURE: Add a policy that will conditionally unroll loops based on
    the code size vs loop overhead.
*/

/*
  Helper function that determines how much to unroll the given loop.
  @param pkg - code vector struct
  @param instr - instruction to apply the policy to
  @param unroll_num - max number of times to unroll any loop
  @return number of times to unroll
*/
uint32_t unroll_policy (pexl_Binary *pkg, Instruction *instr, uint32_t unroll_num)
{
  UNUSED(pkg);			/* Remove when pkg is used below */
  if (unroll_num == 0) return 0;
  
  // Unbounded lookbehind loops have max set to 0.
  // Note that the instruction fields store -min and -max.
  pexl_Index min = - signedaux(instr);
  pexl_Index max = - getaddr(instr);
  assert((max == 0) || (max >= min));

  if (max == 0) return unroll_num;

  // How many times will the loop iterate AFTER the first time?
  uint32_t n = max - min;
  if (n < unroll_num) return n;

  return unroll_num;
}

#define INST(i) (&pkg->code[i])

#define LOOKBEHINDCOMMIT(addr) do {		\
    setopcode(INST(addr), ILookBehindCommit);	\
    setaddr(INST(addr), 0);			\
    setaux(INST(addr), 0);			\
    addr += OPCODE_SIZE[ILookBehindCommit];	\
  } while(0);

// Insert a copy of the loop body and the part immediately after that
// does not vary depending on whether we are creating the last loop
// iteration.
static int lookbehind_body (pexl_Binary *pkg,
			    Instruction *body,
			    uint32_t body_size,
			    uint32_t *cur_addr) {

  memcpy(INST(*cur_addr), body, body_size * sizeof(Instruction));
  *cur_addr += body_size;

  // Loop iteration epilogue.
  LOOKBEHINDCOMMIT(*cur_addr);
  return OK;
}

#define CHOICE(addr, offset) do {		\
    setopcode(INST(addr), IChoice);		\
    setoffset(INST(addr), (offset));		\
    addr += OPCODE_SIZE[IChoice];		\
  } while(0);

#define LOOKBEHIND(addr, min, max) do {		\
    setopcode(INST(addr), ILookBehind);		\
    setaux(INST(addr), (min));			\
    setaddr(INST(addr), (min));			\
    addr += OPCODE_SIZE[ILookBehind];		\
  } while(0);

#define COMMIT(addr, offset) do {		\
    setopcode(INST(addr), ICommit);		\
    setoffset(INST(addr), (offset));		\
    addr += OPCODE_SIZE[ICommit];		\
  } while(0);

// Unroll the loop that spans addresses 'start' to 'end', where 'end'
// is the address of the first instruction AFTER the loop.
//
// We create 'unroll_num' copies of the loop body in addition to the
// original one.
//
// Return the address of the NEXT instruction after the last one emitted.
static int lookbehind_unroll (pexl_Binary *pkg,
			      uint32_t start,
			      uint32_t end,
			      uint32_t unroll_num) {

  // Each unrolled loop will consist of these five instructions
  // in addition to the loop body itself:
  const uint32_t lookbehind_overhead =
    OPCODE_SIZE[IChoice] +
    OPCODE_SIZE[ILookBehind] + 
    OPCODE_SIZE[ILookBehindCommit] +
    OPCODE_SIZE[ICommit];

  assert(unroll_num > 0);

  Instruction *pc = INST(start);
  assert(opcode(pc) == ILookBehind);
  int32_t min = signedaux(pc);

  const uint32_t loop_body_start = start + OPCODE_SIZE[ILookBehind];
  const uint32_t loop_body_end = end - OPCODE_SIZE[ILookBehindCommit];
  const uint32_t loop_body_size = loop_body_end - loop_body_start;

  // The total size of the code that replaces the original loop is the
  // size of the loop body plus the overhead per loop, repeated.
  // There is no overhead for the last iteration.
  const uint32_t total_unroll_size =
    (loop_body_size + lookbehind_overhead) * unroll_num + (end - start);

  // Calculate the address in the code vector of the first instruction
  // AFTER the unrolled (replacement) loop.
  const uint32_t after_addr = start + total_unroll_size;

  // Calculate how much extra code we are adding
  const pexl_Index delta = (pexl_Index) (after_addr - end);
  if (!ensure_binary_size(pkg, delta)) return PEXL_ERR_OOM;

  // How much code needs to move to make room for the unrolled loop?
  const uint32_t amount_to_move = pkg->codenext - end;

  // Move from 'end' (of current loop) to 'after_addr', which is the
  // end of the unrolled section.
  if (move_instructions(pkg, end, after_addr, amount_to_move))
    return PEXL_ERR_INTERNAL;

  // Save the loop body to make it easier to insert copies
  Instruction *body = copy_instructions(pkg, loop_body_start, loop_body_size);

  uint32_t cur_addr = start;
  uint32_t i;
  for (i = 0; i < unroll_num; i++) {

    // Loop iteration prologue.
    // IChoice allows a failure to skip ahead to next (unrolled) iteration
    CHOICE(cur_addr, loop_body_size + lookbehind_overhead);

    LOOKBEHIND(cur_addr, min - i, min - i);

    // Insert the loop body, advancing cur_addr
    lookbehind_body(pkg, body, loop_body_size, &cur_addr);

    // On successful execution of loop body, commit the IChoice that
    // guards this iteration, and jump to the instruction AFTER the
    // unrolled loop
    COMMIT(cur_addr, after_addr - cur_addr);
  }
  // The last loop iteration must loop from current position to the
  // end of the original lookbehind.
  LOOKBEHIND(cur_addr, min - i, min - i);

  // Last instance of the unrolled loop.  First, insert the loop body.
  lookbehind_body(pkg, body, loop_body_size, &cur_addr);

  free(body);

  // Consistency check
  assert(delta == (pexl_Index) (total_unroll_size - (end - start)));

  if (delta != 0) {
    pkg->codenext += delta;
    adjust_symbol_table(pkg, start, delta);
    adjust_targets(pkg, start, delta);
  }   
  // Check our "prediction" about the address we'd end at
  assert(cur_addr = after_addr);

  return cur_addr;
}

/*
  'lookbehind_search' finds all lookbehind loops within a given region
  (start to end).  It unrolls those that pass the 'policy' test.

  When loops are nested, it unrolls the innermost one first, and works
  its way outward.

  Return value is the address of the last instruction examined, so
  that searching can continue there.

*/
static int32_t lookbehind_search (pexl_Binary *pkg,
				  int32_t start,
				  int32_t search_end, 
				  unroll_policy_fn policy,
				  uint32_t param) {
  pexl_Index i = start;
  pexl_Index loop_end, unroll_num;
  pexl_Index skip_commit = 0;

  // N.B. The size of current pattern (stored in symbol table) will
  // change as we unroll the loop
  while (i < search_end) {
    switch (opcode(INST(i))) {
    case (ILookBehind):
      if (signedaux(INST(i)) == getaddr(INST(i))) {
	// This LookBehind doesn't need to be unrolled, because the
	// min and max (delta) input search positions are the same.
	// Skip to the next commit.
        skip_commit++;
        break;
      }
      // Else we have a candidate for unrolling.  The loop starts at
      // the current instruction.  
      start = i; 
      // Since the min and max deltas are different, the next
      // ILookBehindCommit instruction marks the loop body end.
      skip_commit = 0;
      break;
    case (ILookBehindCommit):
      if (skip_commit > 0) {
        skip_commit--;
        break;
      }
      // We found a lookbeind loop
      assert(opcode(INST(start)) == ILookBehind);
      assert(opcode(INST(i)) == ILookBehindCommit);
      loop_end = i + OPCODE_SIZE[ILookBehindCommit];

      // First unroll nested loops
      i = lookbehind_search(pkg,
			    start + OPCODE_SIZE[ILookBehind], 
			    i - OPCODE_SIZE[ILookBehindCommit],
			    policy, param);
      if (i < 0) return i;
      if (i == search_end) return i;
      
      // Unroll the current loop

      // TODO: Change arglist to have loop_start, loop_end
      unroll_num = policy(pkg, INST(start), param);
      if (unroll_num == 0) break; // policy says do not unroll this one

      // Unroll this loop.  'unroll_loop' returns the address just
      // after the last instruction it emitted, which is where we will
      // continue searching.
      i = lookbehind_unroll(pkg, start, loop_end, unroll_num);
      if (i < 0) return i;

    default:
      break; // If the current instruction is any other opcode, skip
    }
    i += sizei(INST(i));
  } // While there are more instructions to search for ILookBehind
  // No more loops to unroll.  Return the last address we examined.
  return i;
}

/*
  Unrolls the loops created by ILookBehind instruction when min and max delta are different.

  @param pkg - code vector structure
  @param policy - function that determines how many times to unroll a loop
  @return status

*/
int loop_optim (pexl_Binary *pkg,
		unroll_policy_fn policy,
		uint32_t param) {
  pexl_Index addr;
  SymbolTableEntry *entry;
  pexl_Index idx = PEXL_ITER_START;

  while ((entry = symboltable_iter_ns(pkg->symtab, symbol_ns_id, &idx))) {

    // Initial search starts at entry->value (pattern entry point),
    // and ends at the end of the pattern.  But the end of the pattern
    // can change if unrolling occurs.  So we check for that by
    // looking to see if there is now more of the current pattern to
    // search.
    addr = entry->value;
    while (addr < (int32_t) (entry->value + entry->size)) {
      addr = lookbehind_search(pkg,
			       addr,                       // search start
			       entry->value + entry->size, // search end
			       policy, param);
      if (addr < 0) {
	warn("opt", "error %d in loop unroller", addr);
	return addr;
      }
    } // while still working on one pattern
  } // while there are more patterns in the symbol table

  return OK;
}
