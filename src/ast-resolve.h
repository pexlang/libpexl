/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast-resolve.h   Resolve references in AST, building the symbol table     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#if !defined(ast_resolve_h)
#define ast_resolve_h

#include "preamble.h"
#include "libpexl.h"
#include "ast.h"
#include "hashtable.h"
#include "symboltable.h"

int ast_resolve (ast_expression *exp, SymbolTable **stptr, ast_expression **violation);

#endif
