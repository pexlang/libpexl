/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  opt-tro.c   Tail Recursion Optimization                                 */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Jamie A. Jennings                                              */


#include "opt.h"

#define NOCHANGE 0
#define CHANGED  1

/*
  Tail recursion optimization changes tail recursive calls to jumps.
  It does not affect tail calls to other patterns.  Because the size
  of a call instruction is the same as a jump instruction, this
  optimization does not move any code.
*/

static int
maybe_convert (pexl_Binary *pkg, pexl_Index entrypoint, pexl_Index endpoint, pexl_Index i)
{
  pexl_Index next, final;
  Instruction *code = pkg->code;
  Instruction *pc = &code[i];

  if (opcode(pc) != ICall) return NOCHANGE;

  /* ICall uses Absolute Addressing */
  if (getaddr(pc) != entrypoint) return NOCHANGE;

  next = i + sizei(pc);

  if (opcode(&code[next]) == IJmp) {
    final = finaltarget(code, next);
    if (opcode(&code[final]) != IRet) return NOCHANGE;
  } else if (opcode(&code[next]) != IRet) return NOCHANGE;
  /* We can replace (ICall; IJmp) with (IJmp; IRet; INoop) */
  trace("optimizer",
	"Found tail recursive call at addr = %d in code block [%d, %d]",
	i, entrypoint, endpoint);

  assert((next > entrypoint) && (next < endpoint));

  assert(OPCODE_SIZE[ICall] == OPCODE_SIZE[IJmp]);
  assert(OPCODE_SIZE[IRet] == OPCODE_SIZE[INoop]);
  assert((OPCODE_SIZE[IJmp] == 2) && (OPCODE_SIZE[INoop] == 1));
  
  setopcode(pc, IJmp);		/* ICall ==> IJmp */
  setoffset(pc, entrypoint - i);	/* jumps use relative addresses */
  /*
    Important: Above, we leave the IRet in place because it may be the
    target of other jumps in this code block. Similarly, below we
    insert an IRet to replace the IJmp to IRet.  That way, if the IJmp
    at 'next' was the target of a jump, the IRet will be there.
  */
  if (opcode(&code[next]) == IJmp) {
    setopcode(&code[next], IRet);
    next += sizei(&code[next]);
    setopcode(&code[next], INoop);
  }
  return CHANGED;
}

// Return 0 if nothing changed, non-zero otherwise
static int
remove_tail_recursion (pexl_Binary *pkg, pexl_Index entrypoint, pexl_Index endpoint) {
  pexl_Index i;
  int changed = 0;
  Instruction *code = pkg->code;
  for (i = entrypoint; i < endpoint; i += sizei(&code[i]))
    changed |= maybe_convert(pkg, entrypoint, endpoint, i);
  return changed;
}

/*
  Tail recursion optimization
  Return PEXL_OK (0) on success, else a PEXL error.
*/

int tro (pexl_Binary *pkg) {
  assert(pkg);
  int changed;
  SymbolTable *st = pkg->symtab;
  SymbolTableEntry *entry = NULL;
  pexl_Index entrypoint, endpoint;
  pexl_Index index = PEXL_ITER_START;
  assert(st);
  while ((entry = symboltable_iter_ns(pkg->symtab, symbol_ns_id, &index))) {
    entrypoint = entry->value;
    endpoint = entry->value + entry->size;
    changed = remove_tail_recursion(pkg, entrypoint, endpoint);
    WHEN_TRACING {
      const string *name = symboltable_get_name(st, index);
      trace("optimizer", "Examined pattern '%s' [%d, %d]. %s tail recursion.",
	    name, entrypoint, endpoint,
	    changed ? "Removed" : "Did not find");
    }
  }
  return PEXL_OK;
}
