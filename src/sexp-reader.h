/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp-reader.h   S-expression reader                                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef sexp_reader_h
#define sexp_reader_h

#include "preamble.h"
#include "libpexl.h"
#include "sexp.h"

/* ----------------------------------------------------------------------------- */
/* S-expressions                                                                 */
/* ----------------------------------------------------------------------------- */

// Naming convention: read, write, free, and print are important and
// common enough to go first.  Other API functions start with 'sexp'.
Sexp *read_sexp(const string **s);
Token sexp_read_token(const string **s);

#endif
