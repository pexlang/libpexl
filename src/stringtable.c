/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  stringtable.c    A hash table with string keys                          */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Jamie A. Jennings                                              */

#include "stringtable.h"

StringTable *stringtable_new (size_t exp_entries, size_t exp_string_size) {
  return hashtable_new(exp_entries, exp_string_size);
}

void stringtable_free (StringTable *st) {
  hashtable_free(st);
}

pexl_Index stringtable_intern (StringTable *st, const string *name, size_t len) {
  StringTableEntry entry;
  StringTableData nodata;
  if (len > PEXL_MAX_STRINGLEN) {
    inform("stringtable", "length exceeds PEXL_MAX_STRINGLEN");
    return PEXL_EXP__ERR_LEN;
  }
  nodata.ptr = NULL;
  pexl_Index index = hashtable_add_update(st, name, len, nodata);
  if (index < 0) {
    switch (index) {
      case HASHTABLE_ERR_NULL: return PEXL_ERR_NULL;
      case HASHTABLE_ERR_OOM: return PEXL_ERR_OOM;
      default:
	warn("context", "unexpected error %d from hash table", index);
	return PEXL_ERR_INTERNAL;
    }
  }
  entry = hashtable_get(st, index);
  assert(entry.block_offset >= 0);
  return hashtable_keyhandle(entry);
}

const string *stringtable_retrieve (StringTable *st, int32_t key, size_t *lenptr) {
  if (!st || !lenptr) return NULL;
  int32_t len = 0;
  const string *ptr = hashtable_get_key(st, key, &len);
  *lenptr = (size_t) len;
  if (len < 0) return NULL;
  return ptr;
}


