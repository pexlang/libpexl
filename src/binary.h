/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  binary.h                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef binary_h
#define binary_h

#include "preamble.h"
#include "libpexl.h"
#include "symboltable.h"
#include "pathtable.h"
#include "instruction.h"

/*
   A pexl_Binary, or "package", is a structure with a code vector
   (bytecode instructions), a symbol table, and a table of character
   sets (which are arguments for instructions like ISet and IFind).  A
   package is the output of compilation.

   A PACKAGE TABLE is an array of packages.  To do matching, the
   Virtual Machine requires input to process and code to execute.  To
   simplify the vm, all the packages it needs are gathered into a
   single pexl_Binary Table.  The first entry is reserved for vm use,
   where it will hold the "user package".  The other packages are
   "imported packages" (ahead-of-time compiled packages that the user
   code is linked against).

   The IXCall instruction calls a pattern in a different package.  The
   IRet instruction returns to the caller's address and restores its
   package as the current one.

   Multiple packages are not being used right now.  Yes, we know that
   premature optimization is the root of all evil, and YAGNI.  Still,
   we're pretty sure we ARE going to need it, based on our experience
   with the Rosie Project.

*/

#define PACKAGETABLE_INITSIZE 100

/*
  To have large package tables, the symbol table implementation must
  be improved (e.g. to use a hash table) so that new symbols can be
  added quickly instead of in linear time.  See IMPORTTABLE_MAXSIZE
  comment below.
*/

/* The PackageNumber must be an UNSIGNED type */
typedef uint16_t pexl_PackageNumber;
/* Max number of packages in a PackageTable is 2^16 - 1 */
#define PACKAGETABLE_MAXSIZE (1 << 13)

struct pexl_Binary;			       /* forward reference */

typedef struct Import {
  struct pexl_Binary *pkg;	  /* NULL until imported pkg is "loaded" */
  pexl_Index          importpath; /* Unique identifier (offset in symboltable block) */
  pexl_Index          prefix;     /* Prefix string for captures (offset also) */
} Import;

/*
  NOTE: binary_add_import() performs 2 insertions into the symbol
  table, and each one searches the block storage to see if the string
  already exists.  This is expensive!  To support a number of imports
  beyond, say, 1000, change the symbol table implementation to include
  a hash table.
*/
#define IMPORTTABLE_INITSIZE 16
#define IMPORTTABLE_MAXSIZE  1000    /* Must be < INT32_MAX/2 */

struct pexl_Binary {
  SymbolTable        *symtab;
  CharsetTable       *cstab;
  PathTable          *pathtab;
  Instruction        *code;	        
  uint32_t            codenext;	   /* number of instructions in 'code' */
  uint32_t            codesize;	   /* size of 'code' */
  pexl_Index          main;	   /* symtab index of "main" pattern */
  pexl_Index          origin;      /* symbol storage offset, e.g. filename or URL */ 
  pexl_Index          doc;         /* symbol storage offset, e.g. doc string */ 
  pexl_PackageNumber  number;	   /* index into global package table (set at runtime) */ 
  uint8_t             major;	   /* required language major version */
  uint8_t             minor;	   /* minimum language minor version */
  uint32_t            import_next; /* > 0 (always one pkg already there) */
  uint32_t            import_size; /* size of import table */
  Import             *imports;	   /* imports[0] is THIS CURRENT PACKAGE */
};

#define package_importpath(p) ((p)->imports[0].importpath)
#define package_prefix(p)     ((p)->imports[0].prefix)

/*
  A pexl_BinaryAttributes struct (or NULL) is passed to the compiler.  On
  successful compilation, the attributes are copied into the resulting
  package.  The pexl_BinaryAttributes struct must then be freed by the
  caller.
*/
struct pexl_BinaryAttributes {
  const char *importpath; /* path in file system, UNIQUE key in package table */
  const char *prefix;     /* package prefix, appears in output (match tree node names) */
  const char *origin;     /* e.g. URL or filename (to support debugging) */ 
  const char *doc;        /* doc string (user provided, to support UX) */ 
  uint8_t     major;      /* minimum required language major version */
  uint8_t     minor;      /* minimum required language minor version */
};

/* When size==capacity, the table is full */
struct pexl_PackageTable {
  pexl_Binary       **packages;	/* package table */
  pexl_PackageNumber  capacity;	/* capacity of packages array */
  pexl_PackageNumber  next;	/* next available */
};

/* ----------------------------------------------------------------------------- */
/* Errors                                                                        */
/* ----------------------------------------------------------------------------- */

#define PACKAGE_OK              0	/* OK (must be 0) */
#define PACKAGE_ERR_SIZE       -2	/* initial table size arg out of range */
#define PACKAGE_ERR_ARG        -5	/* invalid argument (see log message) */
#define PACKAGE_ERR_EXISTS     -6	/* importpath already in package table */
#define PACKAGE_NOT_FOUND      -7	/* not found in package table */
#define PACKAGE_ERR_STRLEN     -9	/* string arg too long (see log message) */
#define PACKAGE_ERR_IMPORTPATH -10	/* importpath not set */

/* ----------------------------------------------------------------------------- */
/* pexl_PackageTable                                                                  */
/* ----------------------------------------------------------------------------- */

pexl_PackageTable *packagetable_new (size_t initial_capacity);
void          packagetable_free (pexl_PackageTable *pt);
void          packagetable_free_packages (const pexl_PackageTable *pt);

pexl_Binary  *packagetable_get (const pexl_PackageTable *pt, pexl_Index package_number);
int           packagetable_remove (const pexl_PackageTable *pt, pexl_Binary *package);

/* These APIs currently require LINEAR TIME in the size of the package table */
int          packagetable_add    (pexl_PackageTable *pt, pexl_Binary *package);
pexl_Index   packagetable_lookup (const pexl_PackageTable *pt, const char *importpath);
pexl_Index   packagetable_getnum (const pexl_PackageTable *pt, pexl_Binary *pkg);

/* ----------------------------------------------------------------------------- */
/* pexl_Binary                                                                       */
/* ----------------------------------------------------------------------------- */

int binary_has_simd (pexl_Binary *p);

pexl_Binary *binary_new (void);
void         binary_free (pexl_Binary *package);
  
pexl_Index main_entrypoint (pexl_Binary *pkg);

bool resize_binary (pexl_Binary *pkg, size_t newsize);
bool ensure_binary_size (pexl_Binary *pkg, pexl_Index additional_size);

int binary_set_attributes (pexl_Binary *p,
			   const char *importpath,
			   const char *prefix,
			   const char *origin,
			   const char *doc,
			   uint8_t major,
			   uint8_t minor);
pexl_BinaryAttributes *binary_get_attributes (pexl_Binary *p);
void                   binary_free_attrs (pexl_BinaryAttributes *attrs);
				 
/* These APIs currently require LINEAR TIME in the size of the symbol table */
int binary_add_import (pexl_Binary *p, const char *importpath, const char *prefix);
int binary_add_string (pexl_Binary *p, const char *str);
SymbolTableEntry *binary_lookup_symbol (pexl_Binary *pkg, Symbol_Namespace ns, const char *symbolname);

#endif
