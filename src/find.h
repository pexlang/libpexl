/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  find.h                                                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef find_h
#define find_h

#include "preamble.h"
#include "libpexl.h"
#include "simd.h"
#include "charset.h"

#if PEXL_USING_SIMD

#ifdef __x86_64__
   #include <immintrin.h>
#else
  #pragma GCC diagnostic push
  #if defined(__clang__)
    #pragma GCC diagnostic ignored "-Wunknown-warning-option"
  #endif
  #pragma GCC diagnostic ignored "-Wunused-parameter"
  #pragma GCC diagnostic ignored "-Wstrict-prototypes"
  #pragma GCC diagnostic ignored "-Wundef"
  #pragma GCC diagnostic ignored "-Wcast-align"
  #pragma GCC diagnostic ignored "-Wbad-function-cast"
  #pragma GCC diagnostic ignored "-Wold-style-declaration"
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
  #pragma GCC diagnostic ignored "-Wnested-externs"
  #pragma GCC diagnostic ignored "-Wimplicit-function-declaration"
  #include "sse2neon.h"
  #define __SSE4_1__ 1
  #pragma GCC diagnostic pop
#endif


typedef struct SetMask_t
{
  __m128i bitmap_0_7;  /* valid when charset size not 0 or 256 */
  __m128i bitmap_8_15; /* valid when charset size not 0 or 256 */
} SetMask_t;

#else

/* Dummy definition for when SIMD is not supported */
typedef struct SetMask_t
{
  uint8_t dummy[CHARSETSIZE];
} SetMask_t;

#endif

int simd_available (void);     /* Was SIMD available at build time? */

void simd_cache_initialize (void);
void simd_cache_print_stats (void);

int charset_to_simd_mask (const Charset *cs, SetMask_t *set, uint8_t *one_char);

const char *find_dynamic_simd(const Charset *set, const Charset *cs, const char *ptr, const char *end);
const char *find_dynamic_simd8(const Charset *set, const Charset *cs, const char *ptr, const char *end);
const char *find_dynamic_simd1(const uint8_t c, const char *ptr, const char *end);


const char *find_simd(const Charset *set, const Charset *cs, const char *ptr, const char *end);
const char *find_simd8(const Charset *set, const Charset *cs, const char *ptr, const char *end);
const char *find_simd1(const uint8_t c, const char *ptr, const char *end);

const char *span_simd (const Charset *simd_cs, const Charset *cs,
		       const char *ptr, const char *end);
const char *span_simd8 (const Charset *simd_cs, const Charset *cs,
			const char *ptr, const char *end);
const char *span_simd1(const uint8_t c, const char *ptr, const char *end);

const char *find_char(const uint8_t, const char *input, const char *end);
const char *span_char(const uint8_t c, const char *ptr, const char *end);

const char *find_charset(const Charset *cs, const char *ptr, const char *end);
const char *span_charset(const Charset *cs, const char *ptr, const char *end);

#endif
