/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  preamble.h                                                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/*

  This preamble defines the "feature macros" that this code base
  relies upon, and it includes a set of standard headers that
  practically all of our modules need.

  Important: The only C modules that need to include "libpexl.h" are
  the ones that define functions that are declared in "libpexl.h".
  When this applies, this preamble must be included BEFORE libpexl.h.

 */

#ifndef preamble_h
#define preamble_h

/* ----------------------------------------------------------------------------- */
/* Ensure that API functions are visibile in libpexl.so / libpexl.dylib.         */
/* Recall that "default" means public, i.e. visible.                             */
/* ----------------------------------------------------------------------------- */
#define PEXL_API __attribute__((visibility("default")))
#define PEXL_DYN(f) (f)

/* ----------------------------------------------------------------------------- */
/* FEATURE TEST MACROS (which affect the contents of header files)               */
/* ----------------------------------------------------------------------------- */

// The _GNU_SOURCE definition enables access to a variety of functions
// if we define it before we include certain system headers.  E.g. It
// gives us asprintf from stdio.h.  It also defines _POSIX_C_SOURCE to
// the value 200809L, which is needed to access strnlen from string.h.
#define _GNU_SOURCE

/* ----------------------------------------------------------------------------- */
/* INCLUDES                                                                      */
/* ----------------------------------------------------------------------------- */

#include <stdio.h>
#include <inttypes.h>  /* int32_t, etc. and their printf formats */
#include <stdlib.h>    /* malloc() and free() */
#include <stddef.h>    /* ptrdiff_t among other things */
#include <stdbool.h>   /* bool, true, false */
#include <limits.h>    /* e.g. INT32_MAX */
#include <string.h>
#ifdef __linux__
  #include <bsd/string.h>
#endif

/* Rudimentary logging/tracing */
#include "logging.h"

/* Note: assert.h defines _DEFAULT_SOURCE */
#include <assert.h> /* Ordinary C assertions */

/* ----------------------------------------------------------------------------- */
/* PEXL PROJECT-SPECIFIC DEFINITIONS                                             */
/* ----------------------------------------------------------------------------- */

// We have a project-specific debugging flag, which is currently set
// via NDEBUG.  It is convenient to assign it a truth value, so that
// we can write macros like WHEN_DEBUGGING (see logging.h).
// Otherwise, we'd be forced to use the preprocessor directive
// "#ifdef" to check the flag.

#if !defined (NDEBUG)
  #define PEXL_DEBUG 1
#else
  #define PEXL_DEBUG 0
#endif

#define UNUSED(x) (void)(x)

/* Generic status values, such that ERR is distinct from other error codes */
#define ERR -999999
#define OK 0

#define YES 1
#define NO 0

#define likely(x)    __builtin_expect(!!(x), 1)
#define unlikely(x)  __builtin_expect(!!(x), 0)

#endif // preamble.h
