/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast-typecheck.c                                                          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "ast-typecheck.h"

// TODO: Convert to defunctionalized continuation passing style (see k.c).

// NOTE: The returned violation pointer is only useful if the
// typecheck function returns false.

bool typecheck(ast_expression *exp, ast_expression **violation);

#define MAKE_TYPECHECKER(test, name)					\
  bool name(ast_expression *exp, ast_expression **violation) {		\
    if (violation) *violation = exp;					\
    if (!test(exp)) return false;					\
    return typecheck(exp, violation);					\
  }

MAKE_TYPECHECKER(ast_expressionp, typecheck_exp);
MAKE_TYPECHECKER(ast_definitionp, typecheck_def);
MAKE_TYPECHECKER(ast_modulep, typecheck_mod);

static bool no_child(ast_expression *exp) {
  return !exp->child;
}

static bool typecheck_explist(ast_explist *ls,
			      bool assert_type(ast_expression *),
			      ast_expression **violation) {
  for (; ls; ls = ls->tail) {
    assert(ls->head);
    if (violation) *violation = ls->head;
    if (!assert_type(ls->head) || !typecheck(ls->head, violation))
      return false;
  }
  return true;
}

static bool mod_or_definitionp(ast_expression *exp) {
  return ast_definitionp(exp) || ast_modulep(exp);
}

bool typecheck(ast_expression *exp, ast_expression **violation) {
 tailcall:
  trace("typecheck", "Checking %s", exp ? ast_type_name(exp->type) : "NULL ARG");
  if (violation) *violation = exp;
  if (!exp) return false;
  switch (exp->type) {
    // 
    // These nodes have no child nodes
    //
    case AST_NUMBER:
    case AST_TRUE:
    case AST_FALSE:
    case AST_BYTE:
    case AST_ANYBYTE:
    case AST_ANYCHAR:
      return no_child(exp);
    case AST_BYTESET:
      return no_child(exp) && exp->byteset;
    case AST_SET:
      return no_child(exp) && codepointset_validp(exp->codepointset);
    case AST_IDENTIFIER:
      return no_child(exp) && exp->name;
    case AST_BYTESTRING:
      return no_child(exp) && (exp->bytes != NULL);
    case AST_CHAR:
      if (exp->codepoint <= CODEPOINT_MAX)
	return no_child(exp);
      warn("typecheck", "Char codepoint out of range: %u", exp->codepoint);
      return false;
      //
      // Ranges
      // Note that ranges can be empty (i.e. low < high).
      //
    case AST_RANGE:
      if ((exp->low <= CODEPOINT_MAX) && (exp->high <= CODEPOINT_MAX))
	return no_child(exp);
      warn("typecheck", "Byte range boundary not in 0-255: [%u, %u]",
	   exp->low, exp->high);
      return false;
    case AST_BYTERANGE:
      if ((exp->low <= 255) && (exp->high <= 255))
	return no_child(exp);
      warn("typecheck", "Byte range boundary not in 0-255: %u", exp->codepoint);
      return false;
      // 
      // The following nodes have exactly one child node
      //
    case AST_REPETITION:
      if ((exp->min < 0) || (exp->min > PEXL_REPETITION_MAX)) {
	warn("typecheck", "Min argument to repetition out of range: %d", exp->min);
	return false;
      }
      if ((exp->max < 0) || (exp->max > PEXL_REPETITION_MAX)) {
	warn("typecheck", "Max argument to repetition out of range: %d", exp->max);
	return false;
      }
      if ((exp->max == 0) || (exp->max >= exp->min))
	goto exp_tailcall;
      else
	return false;
    case AST_FIND:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD:
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
      goto exp_tailcall;
    case AST_BACKREF:
      if (ast_identifierp(exp->child)) {
	exp = exp->child;
	goto tailcall;
      }
      if (violation) *violation = exp->child;
      return false;
    case AST_CAPTURE:
      if (exp->name) goto exp_tailcall;
      return false;
    case AST_INSERT:
      if (exp->name) goto exp_tailcall;
      return false;
    case AST_DEFINITION:
      if (exp->name) goto exp_tailcall;
      return false;
      //
      // The following nodes have exactly two children
      //
    case AST_SEQ:
    case AST_CHOICE:
      if (!ast_expressionp(exp->child)) return false;
      if (!ast_expressionp(exp->child2)) return false;
      if (!typecheck_exp(exp->child, violation)) return false;
      exp = exp->child2;
      goto tailcall;
      //
      // The following nodes can have any number of children
      //
    case AST_MODULE:
      return typecheck_explist(exp->defns, mod_or_definitionp, violation);
    default:
      warn("typecheck", "Unhandled ast type %s", ast_type_name(exp->type));
      return false;
  }
 exp_tailcall:
  // If the child is an expression, then tail call the typechecker,
  // else indicate the violation and fail
  if (ast_expressionp(exp->child)) {
    exp = exp->child;
    goto tailcall;
  }
  if (violation) *violation = exp->child;
  return false;
}

