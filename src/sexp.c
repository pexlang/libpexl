/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  sexp.c  S-expressions                                                   */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Jamie A. Jennings                                              */

// This file contains two kinds of code:
// (1) code for constructing and freeing S-expressions, and
// (2) any code needed by both the sexp-reader and sexp-writer.

#include "sexp.h"

PREDICATE(sharpp, '#');
PREDICATE(openp, '(');
PREDICATE(closep, ')');
PREDICATE(commentp, ';');
PREDICATE(minusp, '-');
PREDICATE(plusp, '+');

bool delimiterp(const string *c) {
  return whitespacep(c) || openp(c) || closep(c) || commentp(c) || eofp(c);
}

// Determine whether c is in set, provided c is not '\0'
static bool containsp(const string *set, const string *c) {
  const string *ptr = strchr(set, *c);
  return ptr && (*ptr != '\0');
}

const string *id_special_start_chars = ".@^_~%&!*/:<=>?~$";
const string *id_special_more_chars = "+-";

bool id_startp(const string *c) {
  return alphap(c) || containsp(id_special_start_chars, c);
}

bool id_charp(const string *c) {
  return id_startp(c) || digitp(c) || containsp(id_special_more_chars, c);
}

bool hexp(const string *c) {
  return digitp(c) || containsp("ABCDEFabcdef", c);
}

bool hexletterp(const string *c) {
  return containsp("ABCDEFabcdef", c);
}

/* ----------------------------------------------------------------------------- */
/* Parsing individual token contents (strings)                                   */
/* ----------------------------------------------------------------------------- */

// The libc strtol and strtoll functions do not seem to reliably
// detect range errors, so we avoid them here and do it the obvious way.
//
// INPUT: optional sign, followed only by decimal digits 0-9.
// RETURNS: true only if conversion succeeded (retval will be set).
static bool int64_value(const string *start, uint32_t len, int64_t *retval) {
  assert(len <= MAX_INT_LEN);
  if (!retval || !start) {
    warn("sexp", "Missing required arg to int64_value");
    return false;
  }

  if (len == 0) goto badint;

  int sign = 0;
  if (plusp(start)) sign = 1;
  if (minusp(start)) sign = -1;
  if ((sign != 0) && (len < 2)) goto badint;

  uint32_t i = (sign != 0);
  uint64_t n = *(start + i) - '0';
  uint64_t oldn = n;
  for (++i; i < len; i++) {
    n = 10*n + (*(start + i) - '0');
    if (n < oldn) goto badint;	// overflow
    oldn = n;
  }
  
  assert(INT64_MAX + INT64_MIN == -1);
  if ((sign > -1) && (n > INT64_MAX)) goto badint;
  if ((sign == -1) && (n > (uint64_t) INT64_MAX + 1)) goto badint;
  *retval = (sign == -1) ? -n : n;
  return true;
      
 badint:
  warn("sexp", "Error parsing the number: '%.*s'", (int) len, start);
  return false;
}

// ON ENTRY: The span from 'start' (with length 'len') either begins
// with a codepoint sigil or the first letter of a named character.
// The sigils are:
//   'x', 'u', or 'U', followed by 2, 4, or 8 hex digits, or
//   '\' followed by a single byte
static uint32_t codepoint_value(const string *start, uint32_t len) {
  assert(((*start == 'u') && (len == 5)) ||
	 ((*start == 'U') && (len == 9)) ||
	 ((*start == 'x') && (len == 3)) ||
	 ((*start == '\\') && (len == 2)) ||
	 ((*start >= 'a') && (*start <= 'z')));
  int i;
  char buf[9] = "\0\0\0\0\0\0\0\0\0";
  switch (*start) {
    // Case: A sigil followed by some hex digits
    case 'x': case 'u': case 'U':
      memcpy(buf, start+1, len-1);
      return strtol(buf, NULL, 16);      
    // Case: A literal one-byte char like \1 or \x or \+
    case '\\':
      assert(delimiterp(start+len));
      return start[1];
    default:
    // Case: We have a named character like '\newline'
      assert(delimiterp(start+len));
      i = 0;
      // Slow search, but small table
      while (character_names[i]) {
	if ((string_cmp(start, character_names[i], character_name_lengths[i]) == 0)
	    && delimiterp(start + character_name_lengths[i])) 
	  return character_name_values[i];
	i++;
      }
      warn("reader", "Error: invalid character -- tokenizer failed");
      return 0;
  }
}
    
/* ----------------------------------------------------------------------------- */
/* Constructing S-expressions                                                    */
/* ----------------------------------------------------------------------------- */

Sexp *sexp(enum Sexp_type type) {
  // Could replace with bump allocator if performance becomes an issue
  Sexp *e = malloc(sizeof(Sexp));
  if (!e) return NULL;
  e->type = type;
  return e;
}

Sexp *sexp_null(void) {
  return sexp(SEXP_NULL);
}

Sexp *sexp_true(void) {
  return sexp(SEXP_TRUE);
}

Sexp *sexp_false(void) {
  return sexp(SEXP_FALSE);
}

// Sexp *sexp_identifier(const string *start, uint32_t len) {
//   assert(len <= MAX_IDLEN);
//   Sexp *e = sexp(SEXP_IDENTIFIER);
//   if (!e) return NULL;
//   e->str = String_from(start, (size_t) len);
//   return e;
// }

Sexp *sexp_int(const string *start, uint32_t len) {
  int64_t n = 0;
  bool ok = int64_value(start, len, &n);
  if (!ok) return NULL;
  Sexp *e = sexp(SEXP_INT);
  if (!e) return NULL;
  e->n = n;
  return e;
}

Sexp *sexp_codepoint_from_value(pexl_Codepoint cp) {
  Sexp *e = sexp(SEXP_CHAR);
  if (!e) return NULL;
  e->codepoint = cp;
  return e;
}

Sexp *sexp_codepoint(const string *start, uint32_t len) {
  return sexp_codepoint_from_value(codepoint_value(start, len));
}

static Sexp *sexp_from_ASCII_string(const string *start, uint32_t len, Sexp_type t) {
  if (!start) {
    warn("sexp", "Null required arg");
    return NULL;
  }
  const string *err;
  String *bytes = string_unescape_const(start, (size_t) len, &err);
  if (!bytes) {
    warn("sexp", "Null return from string_unescape");
    return NULL;
  }
  if (err) {
    warn("sexp", "Unable to unescape string, err is '%c'", *err);
    return NULL;
  } 
  Sexp *e = sexp(t);
  if (!e) return NULL;
  e->str = bytes;
  return e;
}

Sexp *sexp_bytestring(const string *start, uint32_t len) {
  return sexp_from_ASCII_string(start, len, SEXP_BYTESTRING);
}

Sexp *sexp_bytestring_from_literal(const string *s) {
  return sexp_from_ASCII_string(s, PEXL_MAX_STRINGLEN, SEXP_BYTESTRING);
}

Sexp *sexp_identifier(const string *start, uint32_t len) {
  return sexp_from_ASCII_string(start, len, SEXP_IDENTIFIER);
}

// -----------------------------------------------------------------------------
// Predicates
// -----------------------------------------------------------------------------

/*
  Note: When a predicate is given a NULL argument, it issues a warning
  but returns a value anyway.  It's just too awkward to have every
  predicate return one of 3 values: true, false, error.
*/

#define SEXP_PREDICATE(predname, asttype)				\
  bool predname(Sexp *obj) {						\
    if (obj) return (obj->type == (asttype));				\
    warn("sexp", "Null S-expression arg to " STRINGIFY(predname));	\
    return false;							\
  }

SEXP_PREDICATE(sexp_charp, SEXP_CHAR);
SEXP_PREDICATE(sexp_bytestringp, SEXP_BYTESTRING);
SEXP_PREDICATE(sexp_identifierp, SEXP_IDENTIFIER);
SEXP_PREDICATE(sexp_intp, SEXP_INT);
SEXP_PREDICATE(sexp_consp, SEXP_CONS);
SEXP_PREDICATE(sexp_nullp, SEXP_NULL);
SEXP_PREDICATE(sexp_truep, SEXP_TRUE);
SEXP_PREDICATE(sexp_falsep, SEXP_FALSE);

// Notes:
//
// (1) SEXP_ERROR indicates an unrecoverable error, and the nature
// of the error is indicated by the Token it contains.
//
// (2) By contrast, SEXP_EXTRACLOSE simply indicates an extra closing
// paren, which an interactive UI may choose to warn about but
// otherwise ignore.  (Many Scheme and Lisp REPLs do this.)  When
// reading from a file, it coul be treated as an error that halts
// processing, or ignored with a warning.
//
// (3) A list ending in SEXP_INCOMPLETE could be a recoverable error,
// e.g. in a UI that can prompt the user to continue providing the
// rest of the input.  When reading from a file, it must be treated
// as an error that halts processing.

SEXP_PREDICATE(sexp_errorp, SEXP_ERROR);
SEXP_PREDICATE(sexp_extraclosep, SEXP_EXTRACLOSE);
SEXP_PREDICATE(sexp_incompletep, SEXP_INCOMPLETE);

// An S-expression is either an atom, a list, or some kind of error.
// 
// Below the Scheme/Lisp definition of a list, which is either null or
// a pair of values (called a 'cons').  This predicate does not
// examine the entire list, so it cannot tell if the list is properly
// formed.
bool sexp_listp(Sexp *obj) {
  return sexp_nullp(obj) || sexp_consp(obj);
}

// The list of S-expression types has all the atomic values at the top.
bool sexp_atomp(Sexp *obj) {
  return (obj->type >= 0) && (obj->type < SEXP_NATOMS);
}

/* ----------------------------------------------------------------------------- */
/* Some operations below are written in defunctionalized continuation-passing    */
/* style (see defcon.c).  They need a continuation struct and stack operations.  */
/* ----------------------------------------------------------------------------- */

// Data stored in a continuation structure
typedef struct kdata {
  Sexp *retval;
} kdata;

// Declare continuation data type and names type-specific functions
KDATATYPE(kdata, newstack, push, data, setdata);

// Continuation types needed for the functions defined below
KTYPES(K_FREE,
       K_WELL_FORMED_CDR,    // Save cdr for later (car also a list)
       K_DEEPCOPY_CDR,	     // Need to copy the cdr
       K_DEEPCOPY_CONS);     // Need to cons(car, cdr)

static void pushk_well_formed_cdr(Kstack *ks, Sexp *exp) {
  push(ks, K_WELL_FORMED_CDR, (kdata){exp}); 
}

/* ----------------------------------------------------------------------------- */
/* Common operations on S-expressions, which are lists                           */
/* ----------------------------------------------------------------------------- */

Sexp *sexp_cons(Sexp *item, Sexp *ls) {
  if (!item || !ls) return NULL;
  Sexp *e = sexp(SEXP_CONS);
  if (!e) return NULL;
  e->car = item;
  e->cdr = ls;
  return e;
}

Sexp *sexp_car(Sexp *ls) {
  if (!ls || !sexp_consp(ls)) {
    warn("sexp", "Attempt to access car of %s",
	 ls ? sexp_type_name(ls->type) : "NULL ARG");
    return NULL;
  }
  return ls->car;
}

Sexp *sexp_cdr(Sexp *ls) {
  if (!ls || !sexp_consp(ls)) {
    warn("sexp", "Attempt to access cdr of %s",
	 ls ? sexp_type_name(ls->type) : "NULL ARG");
    return NULL;
  }
  return ls->cdr;
}

void *sexp_reduce(SexpReducer *fn,
		  void *initial_value,
		  Sexp *ls) {
  void *result = initial_value;
  while (ls && !sexp_nullp(ls)) {
    assert(sexp_consp(ls));
    result = fn(result, sexp_car(ls));
    ls = sexp_cdr(ls);
  }
  return result;
}

int sexp_length(Sexp *obj) {
  int len = 0;
  while (obj && sexp_consp(obj)) {
    len++;
    obj = obj->cdr;
   }
  return len;
}

// Destructive (in place) reverse
Sexp *sexp_nreverse(Sexp *ls) {
  // Null arg is an error:
  if (!ls) return NULL;
  // Length 0 (empty list) is already reversed:
  if (sexp_nullp(ls)) return ls;
  // Not null and not a cons cell, so cannot reverse it:
  if (!sexp_consp(ls)) return NULL;
  // Length 1 list is already reversed:
  if (sexp_nullp(ls->cdr)) return ls;
  // Do the usual "reverse a linked list in place" thing:
  Sexp *curr = ls;
  Sexp *next = curr->cdr;
  while (!sexp_nullp(next)) {
    if (sexp_consp(next)) {
      // The usual case: we have a proper list
      Sexp *nextnext = next->cdr;
      next->cdr = curr;
      curr = next;
      next = nextnext;
    } else {
      // Improper list: dotted pair, error, incomplete
      Sexp *tmp = curr->car;
      curr->car = next;
      curr->cdr = tmp;
      return curr;
    }
  }
  ls->cdr = next;
  return curr;
}

// Returns a token type and optional source string indicating what
// kind of error occurred.  Valid only when sexp_errorp(e) is true.
// A non-negative return value can be passed to token_type_name().
// On error, returns a value < 0.
int sexp_error_info(Sexp *e, String *data) {
  if (e && sexp_errorp(e)) {
    if (data) data = e->error.data;
    return e->error.type;
  }
  warn("reader", "Expected S-expression error type, not %s",
       e ? sexp_type_name(e->type) : "NULL ARG");
  return -1;
}

// The 'listp' predicate is true if we have a (possibly NULL) list.
//
// Because S-expressions are so flexible, 3 kinds of list can be
// built.  The usual kinds, as seen in Scheme/Lisp, are defined by the
// cdr of the list's last pair.  The 'examine_list' function returns:
//
// - SEXP_NULL for a proper list, which ends in NULL
// - Most other values indicate an improper list (** see below **)
//
// Our implementation allows "incomplete lists" which are needed for a
// good interactive UI.  A list may contain the special value
// SEXP_INCOMPLETE in the position where additional input should be
// inserted.
//
// IMPORTANT! Two other return values are important to check for:
// 
// - SEXP_INCOMPLETE for a list containing this special value anywhere
// - A negative value indicates an error
//
static int examine_list(Sexp *obj) {
  if (!obj || !sexp_listp(obj)) {
    warn("reader", "Null arg, or arg is not a list");
    return -1;
  }
  Kstack *ks = newstack(NULL);
  Kontinuation *k = NULL;
  while (true) {
    if (sexp_consp(obj)) {
      // 3 sub-cases if we have a cons cell
      if (sexp_consp(obj->car)) {
	// (1) car is another cons cell: we need to recursively examine it
	pushk_well_formed_cdr(ks, obj->cdr);
	obj = obj->car;
      } else if (sexp_incompletep(obj->car)) {
	// (2) car is SEXP_INCOMPLETE: we are done (no recursion needed)
	Kstack_free(ks);
	return SEXP_INCOMPLETE;
      } else {
	// (3) car is any other value: examine cdr (no recursion needed)
	obj = obj->cdr;
      }
      continue;
    }
    if (sexp_incompletep(obj)) {
      Kstack_free(ks);
      return SEXP_INCOMPLETE;
    }
    // Else apply the current continuation
    k = K_top(ks);
    if (!k) {
      // No more list elements to examine, so we have our answer
      Kstack_free(ks);
      return obj->type;
    }
    obj = data(k).retval;
    k = K_pop(ks);
  }
}

bool sexp_proper_listp(Sexp *ls) {
  return (examine_list(ls) == SEXP_NULL);
}

bool sexp_improper_listp(Sexp *ls) {
  int status = examine_list(ls);
  return (status > 0) && (status != SEXP_NULL) && (status != SEXP_INCOMPLETE);
}

bool sexp_incomplete_listp(Sexp *ls) {
  return (examine_list(ls) == SEXP_INCOMPLETE);
}

static void pushk_free(Sexp *e, Kstack *ks) {
  push(ks, K_FREE, (kdata){e});
}

void free_sexp(Sexp *e) {
  if (!e) return;
  Kstack *ks = newstack(NULL);
  Kontinuation *k = NULL;
  while (true) {
    switch (e->type) {
      case SEXP_ERROR:
	String_free(e->error.data);
	break;
      case SEXP_IDENTIFIER:
      case SEXP_BYTESTRING:
	String_free(e->str);
	break;
      case SEXP_INT:
      case SEXP_CHAR:
      case SEXP_INCOMPLETE:
      case SEXP_EXTRACLOSE:
      case SEXP_NULL:
	break;
      case SEXP_CONS:
	pushk_free(e->cdr, ks);
	k = K_top(ks);
	Sexp *conscell = e;
	e = e->car;
	free(conscell);
	continue;
      default:
	warn("reader", "Ignoring unknown S-expression type %d", e->type);
    }
    free(e);
    if (!k) {
      Kstack_free(ks);
      return;
    }
    assert(k->type == K_FREE);
    e = data(k).retval;
    k = K_pop(ks);
  }
}

static void pushk_deepcopy_cdr(Sexp *exp, Kstack *ks) {
  push(ks, K_DEEPCOPY_CDR, (kdata){exp});
}

static void pushk_deepcopy_sexp_cons(Sexp *car_result, Kstack *ks) {
  push(ks, K_DEEPCOPY_CONS, (kdata){car_result});
}

// Dive into the expression until we have an actual value to copy.
// For each CONS, push a continuation for the CDR, which we'll later
// copy.
static Sexp *partial_copy_k(Sexp *e, Kstack *ks) {
  assert(e);
  while (e->type == SEXP_CONS) {
    pushk_deepcopy_cdr(sexp_cdr(e), ks);
    e = sexp_car(e);
  }
  assert(e);
  Sexp *new = sexp(e->type);
  if (!new) K_oom();
  switch (e->type) {
    case SEXP_IDENTIFIER:
      new->str = String_dup(e->str);
      break;
    case SEXP_INT:
      new->n = e->n;
      break;
    case SEXP_BYTESTRING:
      assert(e->str);
      new->str = String_dup(e->str);
      break;
    case SEXP_CHAR:
      new->codepoint = e->codepoint;
      break;
    case SEXP_NULL:
    case SEXP_INCOMPLETE:
    case SEXP_EXTRACLOSE:
      break;
    case SEXP_ERROR:
      new->error.type = e->error.type;
      new->error.data = String_dup(e->error.data);
      break;
    default:
      warn("reader", "Unknown S-expression type %d", e->type);
  }
  return new;
}

static Sexp *deepcopy_k(Kstack *ks, Sexp *e) {
  Kontinuation *k = NULL;
  Sexp *new = NULL;
  new = partial_copy_k(e, ks);
  while ((k = K_top(ks))) {
    switch (k->type) {
      case K_DEEPCOPY_CDR:
	e = data(k).retval;
	K_pop(ks);
	pushk_deepcopy_sexp_cons(new, ks);
	new = partial_copy_k(e, ks);
	break;
      case K_DEEPCOPY_CONS:
	new = sexp_cons(data(k).retval, new);
	K_pop(ks);
	break;
      default:
	warn("reader", "Unknown continuation type %d", k->type);
	abort();
    }
  }
  return new;
}

Sexp *sexp_deepcopy(Sexp *ls) {
  if (!ls) return NULL;
  Kstack *ks = newstack(NULL);
  Sexp *copy = deepcopy_k(ks, ls);
  Kstack_free(ks);
  return copy;
}
