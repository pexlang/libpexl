## -*- Mode: Makefile; -*-                                              
##
## PEXL Project -- build libpexl library
##

default: build

help:
	@echo "Useful makefile targets (src directory)"
	@echo "  clean   Deletes old compilation files"
	@echo "  deps    Rebuilds dependency info in Makefile.depends"
	@echo "  build   Builds libpexl"
	@echo "  tags    Rebuilds tag files e.g. for Emacs"
	@echo "  config  Prints the important settings"

ifdef FROM
$(info Called from $(FROM))
endif

# -----------------------------------------------------------------------------
# OPTIONS that can be set on the 'make' command line

# To build without SIMD, even when the CPU supports it
# Usage: make PEXL_NO_SIMD=true build
ifdef PEXL_NO_SIMD
SIMD_FLAG= -DPEXL_NO_SIMD
endif

# To use DTRACE (for flamegraphs), we must add -fno-omit-frame-pointer
# Usage: make DTRACE=true build
ifdef DTRACE
DTRACE_FLAG= -fno-omit-frame-pointer
endif

# -----------------------------------------------------------------------------

OBJDIR ?= $(shell pwd)

RELEASE_MODE ?= true
include Makefile.defines
include $(OBJDIR)/Makefile.depends

LIBPEXL = libpexl

# Log levels: 0=confess, 1=warn, 2=info, 3=gush
LOGLEVEL ?= 1
COPT = -O3 -DNDEBUG -DLOGLEVEL=$(LOGLEVEL)

CWARNS = -Wall -Wextra \
	 -Wcast-align \
	 -Wcast-qual \
	 -Wdisabled-optimization \
	 -Wpointer-arith \
	 -Wshadow \
	 -Wsign-compare \
	 -Wundef \
	 -Wwrite-strings \
	 -Wbad-function-cast \
	 -Wmissing-prototypes \
	 -Wnested-externs \
	 -Wstrict-prototypes \
         -Wunreachable-code \
         -Wno-missing-declarations \
	 -Wno-variadic-macros

# ------------------------------------------------------------------

# Only define CFLAGS if not already set, e.g. by a Makefile in test dir
CFLAGS?= -fvisibility=hidden $(CWARNS) $(COPT) -std=c99 -fPIC

# Always add these compilation flags
CFLAGS+= $(DTRACE_FLAG) $(SIMD_FLAG) $(SYSCFLAGS) $(ASAN_FLAGS)

RM= rm -f

# -----------------------------------------------------------------------------

build: 	TAGS \
	$(OBJDIR)/$(LIBPEXL).a \
	$(OBJDIR)/$(LIBPEXL).$(DYLIB)

# -----------------------------------------------------------------------------

# Combining all object files into all.o is necessary for LTO
# (link-time optimization) to occur. The 'ar' step does not perform
# LTO, and neither does shared/dynamic library creation.

$(OBJDIR)/all.o: $(ALL_O:%=$(OBJDIR)/%)
	$(CC) -r -o $@ $(ALL_O:%=$(OBJDIR)/%)

$(OBJDIR)/$(LIBPEXL).a: $(OBJDIR)/all.o
	$(AR) rc $@ $(OBJDIR)/all.o
	$(RANLIB) $@

$(OBJDIR)/$(LIBPEXL).$(DYLIB): $(OBJDIR)/all.o
	$(CC) $(CFLAGS) $(DYLDFLAGS) -o $@ $(OBJDIR)/all.o

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) $(LTOFLAGS) -c -o $@ $<

# ------------------------------------------------------------------

install: $(OBJDIR)/$(LIBPEXL).$(DYLIB)
	cp "$(OBJDIR)/$(LIBPEXL).$(DYLIB)" "$(DESTDIR)/lib/$(LIBPEXL).$(DYLIB)"
	cp "$(OBJDIR)/$(LIBPEXL).a" "$(DESTDIR)/lib/$(LIBPEXL).a"
	cp "$(LIBPEXL).h" "$(DESTDIR)/include/$(LIBPEXL).h"

# ------------------------------------------------------------------

deps $(OBJDIR)/Makefile.depends:
	printf '# Automatically generated by "make deps"\n' >$(OBJDIR)/Makefile.depends && \
	$(CC) -MM $(ALL_O:.o=.c) | sed 's|[a-zA-Z0-9_-]*\.o|$(OBJDIR)/&|' >>$(OBJDIR)/Makefile.depends

coverage:
ifdef coverage_data_file
	$(COV) -b $(patsubst %.o,%.c,$(ALL_O)) -o $(OBJDIR) >> $(coverage_data_file) && \
	mv *.c.gcov $(OBJDIR)
else
	$(error To generate coverage report, run 'make coverage' from test/unit directory)
endif

covclean:
	-rm -f *.gcda *.gcov *.gcno

clean:
	-rm -rf $(OBJDIR)/*.o $(OBJDIR)/*.so $(OBJDIR)/*.dylib $(OBJDIR)/*.a
	-rm -f *.gcda *.gcov *.gcno

tags TAGS: *.[ch]
	@if ! type "etags" > /dev/null 2>&1; then \
	echo "INFO: etags not found, skipping TAGS file update"; \
	else \
	  echo "INFO: running etags to update TAGS file"; \
	  etags -o TAGS *.[ch]; \
	fi

.PHONY: default build coverage covclean clean deps install help


