//  -*- Mode: C; -*-                                                       
// 
//  bitflags.h       Generic bitflag macros
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

#ifndef bits_h
#define bits_h

// Bit flag macros for up to a maximum of 16 values (fits in uint16_t).
//
//  BITFLAGS(FLAGNAME1, FLAGNAME2, etc) ==> produces an anonymous enum
//  HAS_FLAG(flags, FLAGNAME) ==> true if flags has FLAGNAME set
//  WITH_FLAG(flags, FLAGNAME, b) ==> flags but with FLAGNAME set to b
//  UPDATE_FLAG(var, FLAGNAME, b) ==> assign var to var with FLAGNAME set to b
//
// Note: 'b' is the test in a conditional, so a true value sets a flag bit
//       and a false value (zero) clears a flag bit.

// The implementation is ugly because C does not support iterating
// over macro arguments.  That is also why the implementation supports
// flags with up to 16 values.


#define _GET_NTH(_1, _2,  _3,  _4,  _5,  _6,  _7,  _8,			\
		 _9, _10, _11, _12, _13, _14, _15, _16, N, ...) N

// TODO: Use COUNT_ARGS to ensure there are no more than 16 flags
/*
#define COUNT_ARGS(...) _GET_NTH(__VA_ARGS__,				\
				 16, 15, 14, 13, 12, 11, 10, 9,		\
				 8,   7,  6,  5,  4,  3,  2, 1, 0)
*/

#define _each0(_call, X) 
#define _each1(_call, X) _call(X)
#define _each2(_call, X, ...) _call(X) _each1(_call, __VA_ARGS__)
#define _each3(_call, X, ...) _call(X) _each2(_call, __VA_ARGS__)
#define _each4(_call, X, ...) _call(X) _each3(_call, __VA_ARGS__)
#define _each5(_call, X, ...) _call(X) _each4(_call, __VA_ARGS__)
#define _each6(_call, X, ...) _call(X) _each5(_call, __VA_ARGS__)
#define _each7(_call, X, ...) _call(X) _each6(_call, __VA_ARGS__)
#define _each8(_call, X, ...) _call(X) _each7(_call, __VA_ARGS__)
#define _each9(_call, X, ...) _call(X) _each8(_call, __VA_ARGS__)
#define _each10(_call, X, ...) _call(X) _each9(_call, __VA_ARGS__)
#define _each11(_call, X, ...) _call(X) _each10(_call, __VA_ARGS__)
#define _each12(_call, X, ...) _call(X) _each11(_call, __VA_ARGS__)
#define _each13(_call, X, ...) _call(X) _each12(_call, __VA_ARGS__)
#define _each14(_call, X, ...) _call(X) _each13(_call, __VA_ARGS__)
#define _each15(_call, X, ...) _call(X) _each14(_call, __VA_ARGS__)

#define _EACH(X, ...)						\
  _GET_NTH("ignored", ##__VA_ARGS__,				\
	   _each15, _each14, _each13, _each12,			\
	   _each11, _each10, _each9, _each8,			\
	   _each7, _each6, _each5, _each4,			\
	   _each3, _each2, _each1, _each0)(X, ##__VA_ARGS__)

#define _PREFIX(item) _ORDINAL_##item,
#define _BITPOS(item) item = ((uint16_t)1)<<_PREFIX(item)

#define BITFLAGS(...)			 \
  enum {_EACH(_PREFIX, __VA_ARGS__)};	 \
  enum {_EACH(_BITPOS, __VA_ARGS__)}

// Expression: Test to see if 'flags' has 'flagname'
#define HAS_BITFLAG(flags, flagname) ((flags) & (flagname))

// Expression: The value of 'flags' but with 'flagname' set if 'val'
// is true, or clear if 'val' is not true
#define WITH_BITFLAG(flags, flagname, val)			\
  ((val) ? ((flags) | (flagname)) : ((flags) & ~(flagname)))

// Statement: Assign the variable 'flags' such that 'flagname' is
// set if 'val' is true, and clear if 'val' is not true
#define UPDATE_BITFLAG(flagvar, flagname, val) do {		\
    (flagvar) = WITH_BITFLAG((flagvar), (flagname), (val));	\
  } while (0)

#define NOFLAGS 0

#endif
