//  -*- Mode: C; -*-                                                       
// 
//  ast-resolve.c   Resolve references while building the symbol table
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

#include "ast-resolve.h"

static string *find_dot(string *s) {
  while (*s && (*s != '.')) s++;
  return s;
}

// Returns newly allocated strings in 'module' and 'name'
static bool split_identifier(string *s, string **module, string **name) {
  *module = NULL;
  *name = NULL;
  if (!s) goto invalid;
  string *ptr = find_dot(s);
  ptrdiff_t dot_pos = ptr - s;
  if (*ptr) {
    ptr++;
    // A dot cannot START an identifier unless it is the only char
    if (dot_pos == 0) {
      // Is there a char after it?
      if (*ptr) goto invalid;
      // Is the module name too long to be an identifier?
      if (dot_pos > PEXL_MAX_IDLEN) goto invalid;
    } else {
      // The first dot does not START the identifier
      // But a dot cannot END an identifier unless it is the only char
      if (!*ptr) goto invalid;
    }
    // It's invalid to have a second dot
    string *dot2 = find_dot(ptr);
    if (*dot2) goto invalid;
    *module = string_dup(s, dot_pos);
    *name = string_dup(ptr, PEXL_MAX_IDLEN);
    return true;
  } 
  // No dots means an unqualified identifier (no module name)
  *name = string_dup(s, PEXL_MAX_IDLEN);
  return true;

 invalid:
  inform("ast-resolve", "Invalid identifier: '%s'", s);
  return false;
}

// -----------------------------------------------------------------------------
// Walk the AST, building the contents of the symbol table
// -----------------------------------------------------------------------------

// TODO: Make this dynamically growable
#define ENVMAX 5
typedef struct Env {
  HashTable *env[ENVMAX];
} Env;

static int resolve (ast_expression *exp,
		    SymbolTable *symtab,
		    Env *env,
		    pexl_Index current,
		    ast_expression **violation);

// Same return values as resolve() below
static pexl_Index process_definition (ast_expression *defn,
				      SymbolTable *symtab,
				      Env *env,
				      pexl_Index current,
				      ast_expression **violation) {
  assert(defn && symtab && env && violation && (current >= 0));
  assert(ast_definitionp(defn));
  assert(defn->name);
  assert(defn->child);
  HashTable *ht = env->env[current];
  HashTableData data;
  const char *name;
  pexl_Index idx;
  size_t len;
  
  // An AST_IDENTIFIER can have any sequence of bytes as the name,
  // because it has type String.  But the symbol table requires an
  // ASCII string that is NUL-terminated.
  name = String_escape(defn->name);
  len = string_len(name, PEXL_MAX_STRINGLEN);
  
  // Add name to symbol table
  // TODO: local/global distinction
  idx = symboltable_add(symtab, name,
			symbol_global,
			symbol_ns_id,
			symbol_type_pattern,
			0,	// uint32 size
			0,	// pexl_Index value,
			NULL);	// PatternMetaData *meta
  if (idx < 0) {
    string_const_free(name);
    return idx;
  }
  //confess("TEMP", "Added '%.*s' to symbol table at index %d", (int) len, name, idx);

  // Add name to current env, along with its symbol table index
  // Note: hashtable_add ensures that name is not there already
  data.int32.a = idx;
  // Note reuse of 'idx' variable for hashtable index
  idx = hashtable_add(ht, name, (int32_t) len, data);

  // If no errors, we have the happy path and can process the
  // expression (the right-hand side of the definition)
  if (idx >= 0) {
    //confess("TEMP", "Added '%.*s' to current env at index %d", (int) len, name, data.int32.a);
    string_const_free(name);
    return resolve(defn->child, symtab, env, current, violation);
  } 

  // The hashtable error "key already found" tells us that we have two
  // definitions for the same name, which is an error.
  if (idx == HASHTABLE_ERR_FOUND) {
    // Re-definition of a name in the same scope
    *violation = defn;
    string_const_free(name);
    return false;
  }

  // Else propagate any other error by issuing a warning and returning
  // an appropriate PEXL error code.
  warn("ast-resolve", "Error %d from hashtable", idx);
  string_const_free(name);
  *violation = defn;
  switch (idx) {
    case HASHTABLE_ERR_OOM:
      return PEXL_ERR_OOM;
    case HASHTABLE_ERR_NULL:
      return PEXL_ERR_NULL;
    case HASHTABLE_ERR_FULL:
    case HASHTABLE_ERR_BLOCKFULL:
      return PEXL_ERR_FULL;
    default:
      return PEXL_ERR_INTERNAL;
  }
}

// Return values:
//   True ==> all identifiers in 'exp' are (now) in 'st'
//   False ==> there is an unresolved reference and 'violation' points to it
//   Any value < 0 ==> error code found in libpexl.h
// NOTE that violation, when set, is a pointer into 'exp'.
static int resolve (ast_expression *exp,
		    SymbolTable *symtab,
		    Env *env,
		    pexl_Index current,           // Index into env->env[]
		    ast_expression **violation) {
  size_t len;
  pexl_Index result;
  ast_explist *defns;
  HashTableEntry binding;
  string *s = NULL, *modname = NULL, *idname = NULL;

  // Now walk the AST.  There is an implicit "module" around 'exp'
  // that represents the top level environment, env->env[0].

 tailcall:
  *violation = exp;

  switch (exp->type) {

    case AST_MODULE:

      // Introduce a new scope (a new environment inside the current one)
      current++;
      if (current == ENVMAX) {
	warn("TEMP", "Fixed env depth -- cannot proceed");
	result = PEXL_ERR_INTERNAL;
	goto fail;
      }

      env->env[current] = hashtable_new(0, 0);
      
      // Process the explist, which should contain definitions
      // and possibly other modules
      for (defns = exp->defns; defns; defns = defns->tail) {
	if (ast_modulep(defns->head)) {
	  // Handle nested modules (nested scopes)
	  result = resolve(defns->head, symtab, env, current, violation);
	  if (result != true) return result; // violation is already set
	} else if (ast_definitionp(defns->head)) {
	  // Handle ordinary definitions
	  result = process_definition(defns->head, symtab, env, current, violation);
	  // Propagate any error (violation is set by process_definition)
	  if (result != true) goto fail;
	} else {
	  // Typechecking should have caught the fact that we have
	  // something other than a definition or another module here
	  warn("ast-resolve", "Unexpected module contents: %s",
	       ast_type_name(defns->head->type));
	  *violation = defns->head;
	  result = PEXL_ERR_INTERNAL;
	  goto fail;
	}
      };

      // Now the module goes out of scope, so free the current env
      hashtable_free(env->env[current]);
      env->env[current] = NULL;
      current--;
      assert(current >= 0);
      break;

    case AST_DEFINITION:
      result = process_definition(exp, symtab, env, current, violation);
      if (result != true) goto fail;
      break;
      
    case AST_IDENTIFIER:
      if (!exp->name) {
	warn("ast-resolve", "Identifier has NULL name field");
	result = PEXL_ERR_INTERNAL;
	goto fail;
      }
      s = String_escape(exp->name);
      len = string_len(s, PEXL_MAX_STRINGLEN);
      if (!split_identifier(s, &modname, &idname)) {
	// Return an error because an invalid name should have been
	// caught before this code runs
	inform("ast-resolve", "Invalid identifier '%s'", s);
	result = PEXL_ERR_INTERNAL;
	goto fail;
      }
      if (modname) {
	warn("TEMP", "TEMPORARILY NOT HANDLING qualified identifiers like '%s'", s);
	result = false;
	goto fail;
      }

      // Find the referent by looking in the Env
      binding = hashtable_search(env->env[current], s, len);
      if (hashtable_error(binding)) {
	result = PEXL_ERR_INTERNAL;
	goto fail;
      }
      if (hashtable_notfound(binding)) {
	inform("ast-resolve", "Unknown identifier '%s'", s);
	result = false;
	goto fail;
      }
      
      // Add the symbol table index, which we stored in the hash table
      // earlier, to the identifier node of the AST
      exp->idx = binding.data.int32.a;
      // TODO: Could do fewer allocations if we changed modname and
      // idname to string slices held in local vars (not malloc'd)
      string_free(s); string_free(modname); string_free(idname);
      s = NULL; modname = NULL; idname = NULL;
      break;
      
    case AST_SEQ:
    case AST_CHOICE:
      result = resolve(exp->child, symtab, env, current, violation);
      if (result != true) goto fail;		
      exp = exp->child2;
      goto tailcall;

    default:
      if (exp->child) {
	exp = exp->child;
	goto tailcall;
      }
  }

  *violation = NULL;
  return true;

 fail:
  if (s) string_free(s);
  if (modname) string_free(modname);
  if (idname) string_free(idname);
  return result;
}

int ast_resolve (ast_expression *exp, SymbolTable **stptr, ast_expression **violation) {
  int result = ERR;
  
  if (!exp || !stptr || !violation) {
    warn("ast-resolve", "Required arg is null");
    return PEXL_ERR_NULL;
  }

  // Set up a stack of nested environments to model lexical scope
  Env *env = malloc(sizeof(Env));
  for (int i=1; i < ENVMAX; i++) env->env[i] = NULL;
  env->env[0] = hashtable_new(0, 0);
  if (!env->env[0]) return PEXL_ERR_OOM;
  
  // Allocate a symbol table to hold the bound identifiers
  *stptr = symboltable_new(0, 0);
  if (!*stptr) {
    result = PEXL_ERR_OOM;
    goto done;
  }

  // Walk the AST, inserting bound identifiers into the symbol table,
  // and looking up identifiers there
  result = resolve(exp, *stptr, env, 0, violation);
  if (result < 0) {
    symboltable_free(*stptr);
    *stptr = NULL;
  }

 done:
  // Free the env, and return
  for (int i=0; i < ENVMAX; i++) hashtable_free(env->env[i]);
  free(env);
  return result;
}
