/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  env.c  Environment and value types                                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* 
   Goals (beyond what Rosie/RPL 1.x can do):

   - Control inlining (always, sometimes, never)
   - Ahead of time compilation
   - Separate compilation (in units of a package or a non-package file)

   To address these needs, we are replacing Rosie's Lua-based
   environment (environment.lua) with this C-based implementation
   (env.c).

   This new set of structures replaces the lpeg ktable (which was only
   a string table), and the Rosie Project's use of the ktable to hold
   pattern information for patterns that are bound to names.  

   Also, a Rosie "pattern matching engine" maintains a "package table"
   indexed by the tuple: (importpath, prefix).  Given an indentifier
   in an RPL program that has a package prefix, e.g. "net.ip", there
   must be an import of that package ("net") in that RPL program
   (really: in that compilation unit, which today is a single file).

   With the import path and the prefix from the import declaration, we
   can look up in the package table the environment containing all the
   bindings for that package.  In our example, we can see if "ip" is
   an exported (visible) binding there, so that "net.ip" has a
   meaning.

   Concepts:

   An ENVIRONMENT is a tree in which each node is a collection of
   bindings. 

   A BINDING maps a name to a value, though a binding may be
   anonymous.  An existing binding has a unique pexl_Ref; it can also
   be found by searching for its name.

   A VALUE has a type and some data.  E.g. a string value's data is a
   pointer to a null-terminated byte sequence.  A pattern consists of
   compiled byte-code and some meta-data, all in a single structure.
   A pattern value contains a pointer to such a structure.

   The root of an env tree is the TOP LEVEL ENVIRONMENT.  Lexical
   scope is implemented by looking up names first in the local env,
   then in its parent, and so on, up to the root env.  If a name is
   not found there, it may be found in an (optional) PRELUDE, which
   contains "built in" bindings.

   A PRELUDE is a read-only environment that sits above an env tree,
   serving as the last place to search for a binding.  A name in the
   top level or another environment may shadow the same name in the
   PRELUDE; bindings in the PRELUDE cannot be deleted or altered.

 */

#include "env.h"

#if (PEXL_ROOT_ENV != 0)
#error "PEXL_ROOT_ENV is not set correctly!"
#endif

/* ----------------------------------------------------------------------------- */
/* Values                                                                        */
/* ----------------------------------------------------------------------------- */

Value env_new_value (int32_t type, int32_t data, void *ptr) {
  Value val;
  val.type = type;
  val.data = data;
  val.ptr = ptr;
  return val;
}

Value env_new_value_unspecified (void) { 
  return env_new_value(Eunspecified_t, 0, NULL); 
} 

/* Value env_new_value_error (int number) { */
/*   return env_new_value(Eerror_t, number, NULL); */
/* } */

/* Value env_new_value_int32 (int32_t i) { */
/*   return env_new_value(Eint32_t, i, NULL); */
/* } */

/* Value env_new_value_string (SymbolTablepexl_Index entry) { */
/*   return env_new_value(Estring_t, entry, NULL); */
/* } */

/* ----------------------------------------------------------------------------- */
/* Environment                                                                   */
/* ----------------------------------------------------------------------------- */

BindingTable *new_BindingTable (void) {
  BindingTable *bt;
  int initial_size = ENV_INIT_SIZE;
  bt = malloc(sizeof(BindingTable));
  if (!bt) {
    warn("env", "out of memory");
    return NULL;
  }
  bt->capacity = initial_size;
  bt->next = 0;
  bt->bindings = calloc(bt->capacity, sizeof(Binding));
  if (!bt->bindings) {
    warn("env", "out of memory");
    free(bt);
    return NULL;
  }
  /* A -1 in the parent array indicates that slot is unused. */
  memset(bt->parent, -1, MAX_ENV_NODES * sizeof(pexl_Env));
  bt->parent[0] = PEXL_ROOT_ENV;
  return bt;
}

void free_BindingTable (BindingTable *bt) {
  trace("free_BindingTable", "freeing %p", (void *) bt);
  if (bt) {
    if (bt->bindings) free(bt->bindings);
    free(bt);
  }
}

/*
  Iterate through all bindings in 'env'.  To start the iterator, pass
  in an index value of PEXL_ITER_START.  Returns NULL after last
  binding.  Skips bindings that are marked as deleted.
*/
Binding *env_local_iter (BindingTable *bt, pexl_Env env, pexl_Index *index) {
  Binding *binding;
  if (!bt || !index) {
    warn("env", "null arg to iterator");
    return NULL;
  }
  pexl_Index i = (*index == PEXL_ITER_START) ? -1 : *index;
  if (i < -1) {
    warn("env", "invalid iterator index");
    return NULL;
  }
  for (i++; i < bt->next; i++) {
    binding = bt_get(bt, i);
    if (!binding) return NULL;
    if ((binding->env == env) && !binding_has(binding, Eattr_deleted)) {
      *index = i;	     // index of binding that we are returning
      return binding;
    }
  } /* for loop */
  return NULL;
}

Binding *env_get_binding (BindingTable *bt, pexl_Ref ref) {
  Binding *binding = bt_get(bt, ref);
  if (binding && binding_has(binding, Eattr_deleted)) return NULL;
  return binding;
}

/*
  Linear search by name of one local env (does not follow parent
  pointer).  Note that names (strings) are represented by a handle,
  which is an index into a string table.  Also, cannot look up the
  empty string, which is the "name" of every anonymous binding.

  Returns err < 0 for "not found" else the pexl_Index into binding table.
*/
static pexl_Index local_lookup (BindingTable *bt, pexl_Env env, pexl_Index handle) {
  pexl_Index i;
  if (handle <= 0) return PEXL_ENV__NOT_FOUND;
  for (i = 0; i < bt->next; i++) 
    if ((bt->bindings[i].namehandle == handle)
	&& (bt->bindings[i].env == env)
	&& (!binding_has(&(bt->bindings[i]), Eattr_deleted)))
      return i;
  return PEXL_ENV__NOT_FOUND;
} 

pexl_Index env_local_lookup (BindingTable *bt, pexl_Env env, pexl_Index handle) {
  if (!bt || invalid_env(env)) {
    warn("env", "null arg or invalid env id");
    return PEXL_ERR_INTERNAL;
  }
  return local_lookup(bt, env, handle);
}

/*
  Returns YES if env has a path of parent pointers to the root of the
  given Binding Table.  This is true if the arg, env, is either the
  root env (id 0) in C or a descendant of it.
*/
bool valid_path_to_root (pexl_Env env, BindingTable *bt) {
  while (env > 0) {
    env = env_get_parent(bt, env);
  }
  if (env != PEXL_ROOT_ENV) return false;
  return true;
}

pexl_Env env_new (BindingTable *bt, pexl_Env parent) {
  if (!valid_path_to_root(parent, bt))
    return PEXL_ENV__ERR_ENV;
  pexl_Index new;
  for (new = 1; new < MAX_ENV_NODES; new++)
    if (bt->parent[new] < 0) {
      bt->parent[new] = parent;
      return new;
    }
  return PEXL_ERR_FULL;
}

/* Is this useful? */
// void env_free (BindingTable *bt, pexl_env env) {
//   bt->parent[env] = -1;		/* mark as unused */
// }

// Return size of env tree, i.e. number of nodes in use. 
// ASSUMES that envs have been created sequentially, so the count
// stops at the first empty entry.
pexl_Env env_size (BindingTable *bt) {
  assert(bt);
  // Always have a root node, and parent[root] is invalid
  for (pexl_Env i = 1; i < MAX_ENV_NODES; i++)
    if (invalid_env(env_get_parent(bt, i))) return i;
  return MAX_ENV_NODES;
}

pexl_Env env_get_parent (BindingTable *bt, pexl_Env env) {
  assert(bt);
  if ((env == PEXL_ROOT_ENV) || (invalid_env(env)))
    return PEXL_ENV__NOT_FOUND;
  return bt->parent[env];
}

pexl_Env env_first_child (BindingTable *bt, pexl_Env env) {
  assert(bt);
  if (invalid_env(env)) return PEXL_ENV__ERR_ENV;
  for (pexl_Env i = env + 1; i < MAX_ENV_NODES; i++)
    if (env_get_parent(bt, i) == env) return i;
  return PEXL_ENV__ERR_ENV;
}

pexl_Env env_next_sibling (BindingTable *bt, pexl_Env env) {
  assert(bt);
  if (invalid_env(env)) return PEXL_ENV__ERR_ENV;
  pexl_Env parent = env_get_parent(bt, env);
  for (pexl_Env i = env + 1; i < MAX_ENV_NODES; i++)
    if (env_get_parent(bt, i) == parent) return i;
  return PEXL_ENV__ERR_ENV;
}
/*
  Search 'env' and its ancestors.  Ignore bindings marked for
  deletion.  Cannot look up an anonymous binding (whose name is the
  empty string).
  FUTURE: Implement prelude.
*/
pexl_Ref env_lookup (BindingTable *bt, pexl_Env env, pexl_Index handle) {
  pexl_Ref ref;
  if (handle < 0) return PEXL_ENV__NOT_FOUND;
  while (!invalid_env(env)) {
    ref = env_local_lookup(bt, env, handle);
    if (!pexl_Ref_invalid(ref)) return ref;
    // Once we have searched the root, there's nowhere else to look,
    // until we implement a PRELUDE
    if (env == PEXL_ROOT_ENV) return PEXL_ENV__NOT_FOUND;
    env = env_get_parent(bt, env);
  }
  return PEXL_ERR_INTERNAL;	// corrupt parent table?
}

static int ensure_binding_table_space (BindingTable *bt) {
  size_t newcapacity;		/* number of bindings */
  void *temp;
  if (bt->next < bt->capacity) return PEXL_OK;
  assert((bt->next >= 0) && (bt->capacity > 0));
  newcapacity = 2 * (size_t) bt->capacity;
  if (newcapacity > ENV_MAX_SIZE) {
    if (((size_t) bt->capacity) >= ENV_MAX_SIZE)
      return PEXL_ERR_FULL;
    else
      newcapacity = ENV_MAX_SIZE;
  }
  temp = realloc(bt->bindings, newcapacity * sizeof(Binding));
  if (!temp) {
    warn("env", "out of memory");
    return PEXL_ERR_OOM;
  }
  bt->bindings = temp;
  bt->capacity = newcapacity;
  /*
    Note: realloc does not fill new space with zeros; we made
    bt->bindings with calloc, so we must memset new area to zeros
    ourselves, here.
  */
  memset(&(bt->bindings[bt->capacity]), 0, (newcapacity - bt->capacity) * sizeof(Binding));
  trace("ensure_binding_table_space",
	"extending to %d slots (%d in use)", bt->capacity, bt->next);
  return PEXL_OK;
}

/* Create new binding record.  Return index or err < 0 for OOM. */
static pexl_Index create_new_binding (BindingTable *bt, pexl_Env env, Binding **binding) {
  int err = ensure_binding_table_space(bt);
  if (err < 0) return err;
  assert(bt->next < bt->capacity);
  pexl_Index index = bt->next;
  *binding = &(bt->bindings[index]);
  bt->next++;
  (*binding)->env = env;
  (*binding)->val = env_new_value(Eunspecified_t, 0, NULL);
  (*binding)->meta = 0;
  (*binding)->deplist = NULL;
  return index;
}

/*
  Returns index >= 0 for success, or error < 0.  Note that 'handle'
  can be 0 for anonymous binding.
*/
pexl_Ref env_bind (BindingTable *bt, pexl_Env env, pexl_Index handle, Value val) {
  Binding *binding = NULL;
  pexl_Index index = -1;
  if (!bt || invalid_handle(handle)) {
    warn("env", "null arg to bind");
    return PEXL_ERR_NULL;
  }
  if (invalid_env(env)) {
    warn("env", "invalid env id (%d)", env);
    return PEXL_ENV__ERR_ENV;
  }
  if (handle) {
    /* Check to see if binding with this name exists */
    index = local_lookup(bt, env, handle);
    if (index >= 0) {
      /* Found a binding with this name */
      binding = bt_get(bt, index);
      assert(binding);
      if (!binding_has(binding, Eattr_deleted))
	return PEXL_ENV__ERR_EXISTS;
    }
  }
  if (index < 0) {
    /* Have anonymous binding, or failed to find one with this name */
    index = create_new_binding(bt, env, &binding);
    if (index < 0) return index; /* e.g. FULL or OOM */
  }
  assert(binding);
  binding->namehandle = handle;
  binding->meta = 0;		/* no attributes set */
  binding->val = val;
  return index;
}

/*
  Use 'rebind' to create recursive patterns.  Beyond that, it is
  intended only to support debugging and REPL use cases.  The caller
  should free the old value before calling 'rebind' if that is
  appropriate.  Return 0 for success, or an error < 0.
*/
int env_rebind (BindingTable *bt, pexl_Ref ref, Value val) {
  Binding *binding;
  binding = env_get_binding(bt, ref);
  if (!binding) return PEXL_ENV__NOT_FOUND;
  binding->val = val;
  return PEXL_OK;
}

/*
  Use 'unbind' for use cases in pattern debugging, and perhaps to have
  a fully-featured REPL.  It marks the binding as deleted.

  TODO: Add some of this to env_unbind():

  The CALLER should free the old value and maybe rebind with the
  unspecified value before calling 'unbind', if that is appropriate.
  The caller may also want to change the binding name to anonymous so
  that it will not be reused by env_bind().
*/
void env_unbind (BindingTable *bt, pexl_Ref ref) {
  Binding *binding;
  binding = env_get_binding(bt, ref);
  if (!binding) return;
  set_binding_flag(binding, Eattr_deleted, YES);
  return;
}

