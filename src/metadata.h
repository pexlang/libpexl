/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  metadata.h  Pattern metadata used by compiler and runtime                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#ifndef metadata_h
#define metadata_h

#include "preamble.h"
#include "libpexl.h"
#include "charset.h"
#include "bitflags.h"

typedef struct PatternMetaData {
  uint32_t           flags_;	    /* pattern flags (see below) */
  uint32_t           minlen_;	    /* min bytes consumed in a match */
  uint32_t           maxlen_;	    /* max bytes consumed in a match, unless UNBOUNDED */
  Charset            firstset_;	    /* firstset of pattern (256 bits = 64 bytes) */
} PatternMetaData;

/* Field accessors */

#define metadata_flags(meta) ((meta).flags_)
#define set_metadata_flags(meta, flags) do {				\
    (meta).flags_ = (uint32_t) (flags);					\
  } while (0) 

#define metadata_minlen(meta) ((meta).minlen_)
#define set_metadata_minlen(meta, len) do {				\
    (meta).minlen_ = (uint32_t) (len);					\
  } while (0)

#define metadata_maxlen(meta) ((meta).maxlen_)
#define set_metadata_maxlen(meta, len) do {				\
    (meta).maxlen_ = (uint32_t) (len);					\
  } while (0)

#define metadata_firstset_ptr(meta) (&((meta).firstset_))

/*
  When calculating a pattern's length, PATTERN_MAXLEN is the maximum
  number of bytes we are willing to count.  If the minimum number of
  bytes a pattern must consume to succeed exceeds PATTERN_MAXLEN, then
  we stop counting and consider the pattern effectively "unbounded".
  Same goes for the maximum number of bytes that a pattern can consume.
*/
#define PATTERN_MAXLEN ((uint32_t) 0x0000FFFF) /* 2^16 */

/*
  Pattern flags, (stored in Pattern struct).
  The flag values are valid *only* when PATTERN_READY is set.
*/
BITFLAGS(PATTERN_READY,
	 PATTERN_NULLABLE,
	 PATTERN_NOFAIL,
	 PATTERN_HEADFAIL,
	 PATTERN_UNBOUNDED,
	 PATTERN_NEEDFOLLOW,
	 PATTERN_CAPTURES);

/* Convenience */
#define metadata_fixedlen(meta)			     \
  (!HAS_BITFLAG((meta).flags_, PATTERN_UNBOUNDED)    \
   &&						     \
   ((meta).minlen_ == (meta).maxlen_))

#endif
