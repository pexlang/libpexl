/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  arena.h   General-purpose arena allocator                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "preamble.h"
#include "bitflags.h"

// Must be an even positive number
#define ARENA_MINSIZE 16

                                // Effect when set:
BITFLAGS(ARENA_SOFTFAIL,	// Do not abort() if out of memory
	 ARENA_NOZERO);		// Do not zero the allocated memory

typedef struct {
  char *start;
  char *end;		        // One byte beyond the last usable byte
} arena;

arena arena_create(ptrdiff_t capacity);
void  arena_destroy(arena *a);
void *arena_allocate(arena *a,
		     ptrdiff_t size, ptrdiff_t count, ptrdiff_t align,
		     int flags);


ptrdiff_t arena_size(arena a);
