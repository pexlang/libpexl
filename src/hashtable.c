/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  hashtable.c  Hash table that interns its string keys                     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "hashtable.h"

/*
  Implements a map, i.e. a key/value store.  Expected constant time to
  search the map for a given key.

  A key is any byte sequence, from length 0 to HASHTABLE_MAX_KEYLEN,
  inclusive.  The keys are interned (saved) so they can be rehashed
  when the table expands.

  Each interned key is stored with a 4-byte (signed) length prefix,
  where the length is always >= 0.  And there is always a NUL byte
  after each byte string, to facilitate use cases where the caller
  uses only NUL-terminated strings.  In that specific case, the caller
  can treat each key as an ordinary C string.

  The 'value' fields are 8 bytes; hash indices have type pexl_Index
  (int32_t, or in the future a larger SIGNED integer type).

   - A valid hash table index is >= 0, limiting the hash table array
     to 2^31 - 1 entries.

   - The table size must always be a power of 2 in this design.

   - We use open addressing with quadratic probing.  

  Experiments using many small string keys suggest that a simple hash
  function, a simple compression scheme, and quadratic probing works
  great for load factors around 20% and below.

  This implementation is NOT THREAD SAFE.
*/

static uint32_t hashcode (const char *key, int32_t len) {
  uint32_t h = 17;
  for (int32_t i = 0; i < len; i++) h = (h * 733) + (*key++);
  return h;
}

static pexl_Index hashindex (uint32_t hashcode, pexl_Index tablesize, int32_t i) {
  pexl_Index idx;
  assert(tablesize > 0);
  /* Quadratic probing, using successive values of i starting at 0 */
  idx = ((hashcode & (((uint32_t) tablesize) - 1)) + 3*i*i) & ((uint32_t) tablesize - 1);
  assert(idx >= 0);
  return idx;
}

static int next_power2 (int n) {
  int i = 0;
  assert( n > 0 );
  while((n = n >> 1)) i++;
  return 1 << (i+1);
}

HashTableEntry hashtable_get (HashTable *ht, pexl_Index i) {
  HashTableEntry entry;
  void *probe = Table_get(ht->entries, i);
  if (probe) return *(HashTableEntry *)probe;
  // Error, distinct from "not found".  The returned entry may be an
  // unused hash table entry, in which case 
  entry.block_offset = HASHTABLE_ERR_NULL;
  return entry;
}

// NOTE: This code ASSUMES that table entries are stored contiguously.
static void initialize_entries(HashTable *ht, pexl_Index start, pexl_Index count) {
  // We are using ALL of the table entries, and initializing them here.
  Table_set_size(ht->entries, start + count);
  memset(Table_get(ht->entries, start),
	 HASHTABLE_NOT_FOUND,
	 count * sizeof(HashTableEntry));
}

// The table size MUST be a power of 2 in this implementation.a
static HashTable *new_table (size_t size, size_t blocksize) {
  HashTable *ht;
  ht = malloc(sizeof(HashTable));
  if (!ht) goto oom1;

  // Set up the block of storage for the string keys
  ht->block = StringBuffer_new(blocksize);
  if (!ht->block) goto oom2;

  // String block always contains the empty string at handle 0
  char buf[5] = {0};
  if (StringBuffer_add(ht->block, (string *)&buf, 5)) goto oom3;

  // Set up the hash table entries
  ht->entries = Table_new(size,
			  HASHTABLE_MAX_SLOTS,
			  sizeof(HashTableEntry),
			  NOFLAGS);
  // Table_new() will abort if OOM, due to NOFLAGS.  Double checking:
  assert(ht->entries);
  initialize_entries(ht, 0, Table_capacity(ht->entries));
  ht->count = 0;
  return ht;

 oom3:
  StringBuffer_free(ht->block);
 oom2:
  free(ht);
 oom1:
  warn("hashtable", "out of memory");
  return NULL;
}

/*
  Array is sized based on having HASHTABLE_MAX_LOAD at the number of
  expected entries.  Block storage is sized based on that and the
  expected (average) string size.  When either value is 0, a default
  is used.
*/
HashTable *hashtable_new (size_t exp_entries, size_t exp_string_size) {
  size_t size, blocksize;
  HashTable *ht;
  if (exp_entries == 0) exp_entries = HASHTABLE_MIN_ENTRIES;
  if (exp_string_size == 0) exp_string_size = HASHTABLE_AVG_LEN;
  size = next_power2(exp_entries * 100 / HASHTABLE_MAX_LOAD);
  assert( size > 0 );
  if (size > HASHTABLE_MAX_SLOTS) size = HASHTABLE_MAX_SLOTS;
  blocksize = exp_entries * exp_string_size;
  assert( blocksize > 0 );
  if (blocksize > HASHTABLE_MAX_BLOCKSIZE) blocksize = HASHTABLE_MAX_BLOCKSIZE;
  ht = new_table(size, blocksize);
  if (!ht) warn("hashtable_new", "out of memory");
  return ht;
}

void hashtable_free (HashTable *ht) {
  trace("hashtable", "freeing hashtable %p", (void *)ht);
  if (!ht) return;
  StringBuffer_free(ht->block);
  Table_free(ht->entries);
  free(ht);
}

/*
  Return values:
   0 if otherkey matches the key at keyhandle
   1 if they do not match
  -1 if keyhandle is not in the table
*/
static int compare_keys (HashTable *ht, pexl_Index keyhandle,
			 const char *otherkey, pexl_Index otherlen) {
  assert(ht && otherkey && (otherlen >= 0));
  int32_t storedlen;
  const char *storedkey = hashtable_get_key(ht, keyhandle, &storedlen);
  assert(storedkey);
  if (!storedkey) return -1;
  return (storedlen != otherlen) ||
    (memcmp(storedkey, otherkey, storedlen) != 0);
}

static const char *keyhandle_ptr (HashTable *ht, int32_t keyhandle) {
  if (keyhandle < 0) return NULL;
  // We cannot know if keyhandle is valid.  It should point to the
  // start of a string (stored with its length prefixed), but the
  // caller could supply any byte index in the string buffer.
  if (keyhandle < 0) return NULL;
  if (((size_t) keyhandle) >= StringBuffer_len(ht->block)) return NULL;
  return StringBuffer_ptr(ht->block) + keyhandle;
}

/*
  Return pointer into internal key storage.  Key length is already
  known to caller as entry.key_length.

  NOTE: The pointer is only valid until the next insertion into the
  hash table, which may cause a resize.
*/
const char *hashtable_get_key (HashTable *ht, pexl_Index keyhandle, pexl_Index *lenptr) {
  assert(ht);
  assert(ht->block);
  if (!lenptr) {
    warn("hashtable", "null length (pointer) argument");
    return NULL;
  }
  const char *ptr = keyhandle_ptr(ht, keyhandle);
  if (!ptr) {
    trace("hashtable", "invalid key handle (%d)", keyhandle);
    return NULL;
  }
  memcpy((void *)lenptr, ptr, 4); // 4 byte length prefix
  return ptr+4;
}

int32_t hashtable_get_keyhandle (HashTableEntry entry) {
  return entry.block_offset;
}

static int32_t store_string (HashTable *ht, const char *str, int32_t len) {
  // Save the offset into the buffer where the new string will go
  size_t start = StringBuffer_len(ht->block);
  // Special case: null string always stored at block[0].
  if (len == 0) return 0;
  if ((len < 0) || (len > HASHTABLE_MAX_KEYLEN)) return HASHTABLE_ERR_KEYLEN;
  // Write the length
  if (StringBuffer_add(ht->block, (string *)&len, 4)) return PEXL_ERR_OOM;
  // Write the key string itself, and a trailing NUL byte
  StringSlice s = {.ptr = str, .len = len};
  if (StringBuffer_add_slice(ht->block, s)) return PEXL_ERR_OOM;
  if (StringBuffer_add_char(ht->block, '\0')) return PEXL_ERR_OOM;
  return start;
}

bool hashtable_error (HashTableEntry entry) {
  return (entry.block_offset != HASHTABLE_NOT_FOUND) && (entry.block_offset < 0);
}

bool hashtable_notfound (HashTableEntry entry) {
  return (entry.block_offset == HASHTABLE_NOT_FOUND);
}

/*
  Returns index into hash table.  Either the key exists at this index,
  or the key should be added at this index.  Return value is always a
  valid index.  Assumes that the hashtable is not full.
*/
static pexl_Index compute_index (HashTable *ht, const char *key, int32_t len) {
  HashTableEntry *entry;
  uint32_t code, i = 0;
  pexl_Index index;
  assert(ht && key && (len >= 0));
  code = hashcode(key, len);
  do {
    index = hashindex(code, Table_capacity(ht->entries), i++);
    entry = Table_get(ht->entries, index);
  } while ((entry->block_offset != HASHTABLE_NOT_FOUND) &&
	   compare_keys(ht, entry->block_offset, key, len));
  assert(index < Table_capacity(ht->entries));
  return index;
}

/*
  The block_offset field in HashTableEntry is an integer type.  Search
  returns an entry in which the block_offset field is -1 for "not
  found", < 0 for an error, or > 0 (a valid index, i.e. entry found).
*/
HashTableEntry hashtable_search (HashTable *ht, const char *key, int32_t len) {
  HashTableEntry entry;
  if (!key) {
    entry.block_offset = HASHTABLE_ERR_NULL; /* error */
    return entry;
  }
  if (len < 0) {
    entry.block_offset = HASHTABLE_ERR_KEYLEN; /* error */
    return entry;
  }
  return hashtable_get(ht, compute_index(ht, key, len));
}

/*
  Put into the new table (ht) one of the entries (entry) from the old
  table.  This requires rehashing of each entry.
*/
static void put_entry (HashTable *ht, HashTableEntry *entry) {
  const char *key;
  pexl_Index keylen = -1;
  // Skip unused entries
  if (entry->block_offset == HASHTABLE_NOT_FOUND) return;
  // Rehash the key, which is a string (ptr and len)
  key = hashtable_get_key(ht, entry->block_offset, &keylen);
  assert(key && (keylen >= 0));
  // compute_index returns the hash table index for 'key'
  pexl_Index index = compute_index(ht, key, keylen);
  HashTableEntry *newentry = Table_get(ht->entries, index);
  assert(newentry);
  assert(newentry->block_offset == HASHTABLE_NOT_FOUND);
  *newentry = *entry; 
}

/*
  Rehash existing entries, put them into new table.  It's possible
  that a pathological situation could cause this loop to take
  quadratic time, but only if certain keys were inserted in a certain
  sequence into the original hashtable, creating longer and longer
  collision sequences.
*/
static void rehash_entries(HashTable *ht, pexl_Index new_cap) {
  trace("hashtable", "expanding table to %" pexl_Index_FMT " entries"
	" (count is %" pexl_Index_FMT ") (block bytes used: %zu/%zu)",
	new_cap, ht->count,
	StringBuffer_len(ht->block), StringBuffer_capacity(ht->block));
  Table *oldentries = ht->entries;
  ht->entries = Table_new(new_cap,
			  HASHTABLE_MAX_SLOTS,
			  sizeof(HashTableEntry),
			  NOFLAGS);
  // Table_new() will abort if OOM, due to NOFLAGS.  Check it anyway.
  assert(ht->entries);
  initialize_entries(ht, 0, new_cap);
  pexl_Index i, old_cap = Table_capacity(oldentries);
  assert(old_cap > 0);
  for (i = 0; i < old_cap; i++)
    put_entry(ht, Table_get(oldentries, i));
  Table_free(oldentries);
}

/*
  Expand if needed, and if so, re-hash all the keys.
  Return 0 if no expansion happened,
         1 if table was expanded, or
         < 0 to indicate an error.
*/
static int expand_table (HashTable *ht) {

  pexl_Index load = (ht->count * 100) / Table_capacity(ht->entries);
  if (load <= HASHTABLE_EXPAND_LOAD) return 0;

  // Double the table capacity
  pexl_Index old_cap = Table_capacity(ht->entries);
  // We should always have a current capacity that is a power of 2
  assert(old_cap % 2 == 0);
  pexl_Index new_cap = 2 * old_cap;
  if (new_cap > HASHTABLE_MAX_SLOTS) {
    trace("hashtable", "cannot expand table to %" pexl_Index_FMT
	  " entries", new_cap);
    if (load <= HASHTABLE_MAX_LOAD)
      // Load factor can increase, so allow new additions to table.
      return 0;
    else    
      // Load factor has no room to increase.  Error.
      return HASHTABLE_ERR_FULL;
  }    
  // Rehash current entries into a table of size 'new_cap'
  rehash_entries(ht, new_cap);
  return 1;
}

/*
  Unconditionally add key and data to the hastable at 'index'. Assumes
  key is not already in table.
*/
static pexl_Index add(HashTable *ht, int32_t index,
		      const char *key, int32_t len,
		      HashTableData data) {
  int status;
  int32_t offset;
  HashTableEntry *entry;
  assert(index >= 0); assert(key); assert(len >= 0);

  status = expand_table(ht);
  if (status < 0) return status; // Error
  if (status) {
    // Rehash the new key (necessary after table resizing).
    index = compute_index(ht, key, len);
  }
  if ((offset = store_string(ht, key, len)) < 0) {
    return offset;		// Error
  }
  entry = (HashTableEntry *)Table_get(ht->entries, index);
  entry->block_offset = offset;
  entry->key_length = len;
  // Write the payload (the value field)
  memcpy(&(entry->data.chars), data.chars, 8);
  ht->count++;
  return index;
}

/* 
   Adds a key to the hash table.  Returns the index in the hash table
   for the new entry or error < 0.  A key that is already in table is
   an error.
*/
pexl_Index hashtable_add (HashTable *ht,
			  const char *key, int32_t len,
			  HashTableData data) {
  int32_t index;
  HashTableEntry entry;

  if (!ht || !key) {
    inform("hashtable_add", "null required argument");
    return HASHTABLE_ERR_NULL;
  }
  if (len < 0) {
    inform("hashtable_add", "invalid length argument");
    return HASHTABLE_ERR_NULL;
  }

  /* Find where this string is, or where it should go */
  index = compute_index(ht, key, len);
  entry = hashtable_get(ht, index);
  if (entry.block_offset == HASHTABLE_NOT_FOUND) 
    return add(ht, index, key, len, data);
  /* Else index is the index into the hash table where 'key' exists */
  return HASHTABLE_ERR_FOUND;
}

/* 
   Adds an entry to the hash table if not present, else updates the
   data field.  Returns the index of the hash table entry (>= 0) or a
   value < 0 to signal an error.
*/
pexl_Index hashtable_add_update (HashTable *ht,
				 const char *key, int32_t len,
				 HashTableData data) {
  int32_t index;
  HashTableEntry *entry;
  if (!ht || !key) {
    warn("hashtable_add_update", "null required argument");
    return HASHTABLE_ERR_NULL;
  }
  if (len < 0) {
    warn("hashtable_add_update", "invalid length argument");
    return HASHTABLE_ERR_KEYLEN;
  }
  index = compute_index(ht, key, len);
  entry = (HashTableEntry *) Table_get(ht->entries, index);
  assert(entry);
  if (entry->block_offset == HASHTABLE_NOT_FOUND) {
    return add(ht, index, key, len, data);
  }
  if (entry->block_offset >= 0) {
    /* Key exists, so update the data */
    memcpy(entry->data.chars, data.chars, 8);  
    return index;
  } 
  warn("hashtable_add_update",
       "unexpected block offset value found in hash table (%d)",
       entry->block_offset);
  return HASHTABLE_ERR_INTERNAL;
}

/* 
   Start iterating by calling with prev == -1.  PEXL_ITER_START is
   provided for this purpose.  Note that the empty string will be one
   of the strings returned iff it was explicitly added to the hash
   table.
 */
HashTableEntry hashtable_iter (HashTable *ht, int32_t *prev) {
  HashTableEntry entry;
  pexl_Index cap = Table_capacity(ht->entries);
  pexl_Index i = *prev + 1;

  entry.block_offset = HASHTABLE_NOT_FOUND;
  if (i < 0) return entry;
  
  while ((i < cap) &&
	 (entry.block_offset == HASHTABLE_NOT_FOUND)) {
    entry = hashtable_get(ht, i++);
  }
  if (i == cap) {
    assert(entry.block_offset == HASHTABLE_NOT_FOUND);
    return entry;
  }
  *prev = i - 1;
  return entry;
}
