/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp-to-ast.c  S-expression to AST and back                              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "sexp-to-ast.h"
#include "sexp-writer.h"

// -----------------------------------------------------------------------------
// Convert an S-expression into an AST
// -----------------------------------------------------------------------------

/* 
   Processing an S-expression is like evaluating an expression.
   Atomic types, like numbers and identifiers, evaluate to themselves.
   Lists denote applications, and in our case, all operators are
   "special forms", i.e. the car of the list must be an identifier AND
   a known one that we can dispatch on.
*/

typedef ast_expression *Ctor (Sexp *);

// Constructors for atomic types, e.g. id, int, bytestring

static ast_expression *sexp_id_to_ast(Sexp *s) {
  assert(s->type == SEXP_IDENTIFIER);
  return make_identifier(String_dup(s->str));
}

static ast_expression *sexp_int_to_ast(Sexp *s) {
  assert(s->type == SEXP_INT);
  return make_number(s->n);
}

static ast_expression *sexp_bytestring_to_ast(Sexp *s) {
  assert(s->type == SEXP_BYTESTRING);
  return make_bytestring(String_dup(s->str));
}
 
static ast_expression *sexp_char_to_ast(Sexp *s) {
  assert(s->type == SEXP_CHAR);
  return make_char(s->codepoint);
}

static ast_expression *eval_apply(Sexp *s);

static ast_expression *eval(Sexp *s) {
  // Handle expressions like: (choice "a" "b")
  if (sexp_consp(s)) return eval_apply(s);
  // Below, we handle atoms and catch errors
  switch (s->type) {
    case SEXP_INT: return sexp_int_to_ast(s);
    case SEXP_IDENTIFIER: return sexp_id_to_ast(s);
    case SEXP_CHAR: return sexp_char_to_ast(s);
    case SEXP_BYTESTRING: return sexp_bytestring_to_ast(s);
    default:
      warn("sexp-ast", "No AST constructor for S-expression type %s (%d)",
	   sexp_type_name(s->type), s->type);
      return NULL;
  }
}

// Builds an X (a seq or choice) using the elements of ls.
// Special cases:
// - 'ls' is empty ==> returns NULL to indicate error
// - 'ls' has one element ==> returns the element,
//   because a seq or choice of one exp is just that exp
static ast_expression *sexp_to_X(Sexp *ls,
				 ast_expression *build(ast_expression *,
						       ast_expression *),
				 ast_type X) {
  assert(sexp_listp(ls));
  ast_expression *child;
  ast_expression *exp = NULL;
  while (!sexp_nullp(ls)) {
    child = eval(sexp_car(ls));
    if (!child) return NULL;
    exp = build(exp, child);
    ls = sexp_cdr(ls);
  }
  if (!exp) warn("sexp-ast", "Empty %s", ast_type_name(X));
  return exp;
}

static ast_expression *sexp_to_seq(Sexp *ls) {
  return sexp_to_X(ls, build_seq, AST_SEQ);
}

static ast_expression *sexp_to_choice(Sexp *ls) {
  return sexp_to_X(ls, build_choice, AST_CHOICE);
}

static ast_expression *sexp_to_module(Sexp *ls) {
  assert(ls && sexp_listp(ls));
  ast_expression *exp = make_module(NULL);
  if (!exp) return NULL;
  ast_explist *defns = NULL;
  while (!sexp_nullp(ls)) {
    defns = explist_cons(eval(sexp_car(ls)), defns);
    ls = sexp_cdr(ls);
    assert(ls);
  }
  defns = explist_nreverse(defns);
  exp->defns = defns;
  return exp;
}

static ast_expression *sexp_to_byteset(Sexp *ls) {
  Sexp *elt;
  pexl_Codepoint cp;
  ast_expression *set_exp = make_byteset();
  if (!set_exp) return NULL;
  if (sexp_nullp(ls)) {
    warn("sexp-ast", "Empty byteset");
    return set_exp;
  }
  assert(sexp_consp(ls));
  while (ls && !sexp_nullp(ls)) {
    elt = sexp_car(ls);
    if (sexp_charp(elt)) {
      cp = elt->codepoint;
      byteset_add(set_exp->byteset, cp);
      ls = sexp_cdr(ls);
      continue;
    }
    warn("sexp-ast", "Invalid argument to byteset: %s is not a byte",
	 sexp_type_name(elt->type));
    free_expression(set_exp);
    return NULL;
  }
  return set_exp;
}

static ast_expression *sexp_to_set(Sexp *ls) {
  Sexp *elt;
  ast_expression *set_exp = make_codepointset(sexp_length(ls));
  if (!set_exp) return NULL;
  if (sexp_nullp(ls)) {
    warn("sexp-ast", "Empty char (codepoint) set");
    return set_exp;
  }
  assert(sexp_consp(ls));
  while (ls && !sexp_nullp(ls)) {
    elt = sexp_car(ls);
    if (sexp_charp(elt)) {
      codepointset_add(set_exp->codepointset, elt->codepoint);
      ls = sexp_cdr(ls);
      continue;
    } else {
      warn("sexp-ast", "Invalid argument to set: %s is not a codepoint",
	   sexp_type_name(elt->type));
      free_expression(set_exp);
      return NULL;
    }
  }
  return set_exp;
}

static ast_expression *sexp_to_repetition(Sexp *ls) {
  if (sexp_length(ls) != 3) {
    warn("sexp-ast", "Wrong number of arguments to repetition");
    return NULL;
  }
  Sexp *minexp = sexp_car(ls);
  Sexp *maxexp = sexp_car(sexp_cdr(ls));
  if ((minexp->type != SEXP_INT) || (maxexp->type != SEXP_INT)) {
    warn("sexp-ast", "Min/max arguments to repetition must be ints");
    return NULL;
  }
  int64_t min = minexp->n;
  int64_t max = maxexp->n;
  ast_expression *target = eval(sexp_car(sexp_cdr(sexp_cdr(ls))));
  ast_expression *repetition = make_repetition((pexl_Index) min,
					       (pexl_Index) max,
					       target);
  if (!repetition) free(target);
  return repetition;
}

static ast_expression *sexp_to_char(Sexp *e) {
  assert(e && (e->type == SEXP_CHAR));
  return make_char(e->codepoint);
}

static ast_expression *sexp_to_byte(Sexp *e) {
  assert(e && (e->type == SEXP_CONS));
  Sexp *b = sexp_car(e);
  assert(b && (b->type == SEXP_CHAR));
  return make_byte(b->codepoint);
}

static ast_expression *sexp_to_byterange(Sexp *ls) {
  if (sexp_length(ls) != 2) {
    warn("sexp-ast", "Wrong number of arguments to byte range");
    return NULL;
  }
  Sexp *start = sexp_car(ls);
  Sexp *end = sexp_car(sexp_cdr(ls));
  if ((start->type != SEXP_CHAR) || (end->type != SEXP_CHAR)) {
    warn("sexp-ast", "One argument to byte range is not a byte: [%s, %s]",
	 sexp_type_name(start->type), sexp_type_name(end->type));
    return NULL;
  }
  return make_byterange(start->codepoint, end->codepoint);
}

static ast_expression *sexp_to_range(Sexp *ls) {
  if (sexp_length(ls) != 2) {
    warn("sexp-ast", "Wrong number of arguments to range");
    return NULL;
  }
  Sexp *start = sexp_car(ls);
  Sexp *end = sexp_car(sexp_cdr(ls));
  if (sexp_charp(start) && (sexp_charp(end)))
    return make_range(start->codepoint, end->codepoint);
  warn("sexp-ast", "One argument to range is not a codepoint: [%s, %s]",
       sexp_type_name(start->type), sexp_type_name(end->type));
  return NULL;
}

static ast_expression *sexp_to_capture(Sexp *ls) {
  if (sexp_length(ls) != 2) {
    warn("sexp-ast", "Wrong number of arguments in capture");
    return NULL;
  }
  if (!sexp_bytestringp(sexp_car(ls))) {
    warn("sexp-ast", "First argument to capture is not a bytestring");
    return NULL;
  }
  ast_expression *exp = eval(sexp_car(sexp_cdr(ls)));
  if (!exp) return NULL;	// warning already issued

  String *name = String_dup(sexp_car(ls)->str);
  return make_capture(name, exp);
}

static ast_expression *sexp_to_insert(Sexp *ls) {
  if (sexp_length(ls) != 2) {
    warn("sexp-ast", "Wrong number of arguments to insert capture");
    return NULL;
  }
  if (!sexp_bytestringp(sexp_car(ls))) {
    warn("sexp-ast", "First argument to insert capture is not a bytestring");
    return NULL;
  }
  ast_expression *value = eval(sexp_car(sexp_cdr(ls)));
  if (!value) return NULL;		// warning already issued
  if (value->type != AST_BYTESTRING) {
    warn("sexp-ast", "Second argument to insert capture is not a bytestring");
    free_expression(value);
    return NULL;
  }
  String *name = String_dup(sexp_car(ls)->str);
  return make_insert_internal(name, value);
}

static ast_expression *sexp_to_definition(Sexp *ls) {
  if (sexp_length(ls) != 2) {
    warn("sexp-ast", "Wrong number of arguments in definition");
    return NULL;
  }
  ast_expression *id = eval(sexp_car(ls));
  if (!id) return NULL;		// warning already issued
  if (id->type != AST_IDENTIFIER) {
    warn("sexp-ast", "First argument to definition is not an identifier");
    return NULL;
  }
  ast_expression *exp = eval(sexp_car(sexp_cdr(ls)));
  if (!exp) {
    free_expression(id);
    return NULL;		// warning already issued
  }
  ast_expression *def = make_definition(id->name, exp);
  // The contents of the identifier were copied into the definition so
  // we need to free the struct itself and not the contents (strings).
  free(id);
  return def;
}

#define AST_PREDICATE_MAKER(type, pred_type)			\
  static ast_expression *make_##type(ast_expression *exp) {	\
    return make_predicate(pred_type, exp);			\
  }

AST_PREDICATE_MAKER(lookahead, AST_LOOKAHEAD);
AST_PREDICATE_MAKER(neglookahead, AST_NEGLOOKAHEAD);
AST_PREDICATE_MAKER(lookbehind, AST_LOOKBEHIND);
AST_PREDICATE_MAKER(neglookbehind, AST_NEGLOOKBEHIND);

#define MAKE_AST(ast_type, constructor)				\
  static ast_expression *sexp_to_##ast_type(Sexp *ls) {		\
    assert(sexp_consp(ls) || sexp_nullp(ls));			\
    if (sexp_length(ls) != 1) {					\
      warn("sexp-ast", "Wrong number of arguments in "		\
	   STRINGIFY(ast_type) " expression");			\
      return NULL;						\
    }								\
    ast_expression *target = eval(sexp_car(ls));		\
    if (!target) return NULL;					\
    ast_expression *exp = constructor(target);			\
    if (!exp) free_expression(target);				\
    return exp;							\
  }

MAKE_AST(lookahead, make_lookahead)
MAKE_AST(neglookahead, make_neglookahead)
MAKE_AST(lookbehind, make_lookbehind)
MAKE_AST(neglookbehind, make_neglookbehind)
MAKE_AST(find, make_find)
MAKE_AST(backref, make_backref)

// Helper
static Sexp *sexp_from(const string *op, Sexp *operands) {
  Sexp *operator = sexp_identifier(op, string_len(op, PEXL_MAX_STRINGLEN));
  if (!operator) return NULL;
  return sexp_cons(operator, operands);
}

// No operands, just operator name in a list
// static Sexp *simple_ctor(const string *op, ast_expression *exp) {
//   UNUSED(exp);
//   return sexp_from(op, sexp_null());
// }

static Sexp *definition_ctor(const string *op, ast_expression *exp) {
  assert(ast_definitionp(exp));
  Sexp *name = sexp_identifier(String_ptr(exp->name), String_len(exp->name));
  if (!name) return NULL;
  Sexp *rhs = ast_to_sexp(exp->child);
  if (!rhs) {
    free_sexp(name);
    return NULL;
  }
  Sexp *args = sexp_cons(name, sexp_cons(rhs, sexp_null()));
  if (!args) {
    free_sexp(name);
    free_sexp(rhs);
    return NULL;
  }
  return sexp_from(op, args);
}

static Sexp *module_ctor(const string *op, ast_expression *exp) {
  assert(ast_modulep(exp));
  Sexp *defn, *defns = sexp_null();
  ast_explist *ls = exp->defns;
  while (ls) {
    defn = ast_to_sexp(ls->head);
    if (!defn) {
      free_sexp(defns);
      return NULL;
    }
    defns = sexp_cons(defn, defns);
    ls = ls->tail;
  }
  defns = sexp_nreverse(defns);
  return sexp_from(op, defns);
}

static Sexp *form_ctor(const string *op, ast_expression *exp, ast_type t) {
  Sexp *arg1, *args;
  assert(exp->child);
  arg1 = ast_to_sexp(exp->child);
  if (!arg1) return NULL;
  args = sexp_cons(arg1, sexp_null());
  while (exp->child2->type == t) {
    exp = exp->child2;
    arg1 = ast_to_sexp(exp->child);
    if (!arg1) goto fail;
    args = sexp_cons(arg1, args);
  }
  arg1 = ast_to_sexp(exp->child2);
  if (!arg1) goto fail;
  args = sexp_cons(arg1, args);
  args = sexp_nreverse(args);
  return sexp_from(op, args);

 fail:
  free_sexp(args);
  return NULL;
}

static Sexp *sequence_ctor(const string *op, ast_expression *exp) {
  return form_ctor(op, exp, AST_SEQ);
}

static Sexp *choice_ctor(const string *op, ast_expression *exp) {
  return form_ctor(op, exp, AST_CHOICE);
}

static Sexp *repetition_ctor(const string *op, ast_expression *exp) {
  Sexp *arg, *args = sexp_null();
  assert(exp->child);
  arg = ast_to_sexp(exp->child);
  if (!arg) return NULL;
  args = sexp_cons(arg, args);
  arg = sexp(SEXP_INT);
  if (!arg) goto fail;
  arg->n = exp->max;
  args = sexp_cons(arg, args);
  arg = sexp(SEXP_INT);
  if (!arg) goto fail;
  arg->n = exp->min;
  args = sexp_cons(arg, args);
  return sexp_from(op, args);

 fail:
  free_sexp(args);
  return NULL;
}

static Sexp *one_arg_ctor(const string *op, ast_expression *exp) {
  assert(exp->child);
  Sexp *arg, *args;
  arg = ast_to_sexp(exp->child);
  if (!arg) return NULL;
  args = sexp_cons(arg, sexp_null());
  return sexp_from(op, args);
}

static Sexp *sexp_char_from_value(pexl_Codepoint cp) {
  Sexp *e = sexp(SEXP_CHAR);
  if (!e) return NULL;
  e->codepoint = cp;
  return e;
}

static Sexp *byteset_ctor(const string *op, ast_expression *exp) {
  bool b;
  Sexp *arg, *args = sexp_null();
  ast_byteset *set = exp->byteset;
  if (!set) return NULL;
  for (int i = 255; i >= 0; i--) {
    b = byteset_member(set, i & 0xFF);
    if (b) {
      arg = sexp_char_from_value(i);
      if (!arg) return NULL;
      args = sexp_cons(arg, args);
    }
  }
  return sexp_from(op, args);
}

static Sexp *byterange_ctor(const string *op, ast_expression *exp) {
  Sexp *arg, *args;
  arg = sexp_char_from_value(exp->high);
  if (!arg) return NULL;
  args = sexp_cons(arg, sexp_null());
  arg = sexp_char_from_value(exp->low);
  if (!arg) return NULL;
  args = sexp_cons(arg, args);
  return sexp_from(op, args);
}

static Sexp *byte_ctor(const string *op, ast_expression *exp) {
  UNUSED(op); UNUSED(exp); //TEMP!
  return sexp_null();
}

static Sexp *char_ctor(const string *op, ast_expression *exp) {
  UNUSED(op); UNUSED(exp); //TEMP!
  return sexp_null();
}

static Sexp *set_ctor(const string *op, ast_expression *exp) {
  pexl_Codepoint cp;
  Sexp *arg, *args = sexp_null();
  ast_codepointset *set = exp->codepointset;
  if (!set) return NULL;
  pexl_Index i = -1;
  while (1) {
    cp = codepointset_iter(set, &i);
    if (i < 0) break;
    arg = sexp_codepoint_from_value(cp);
    if (!arg) { free_sexp(args); return NULL; }
    args = sexp_cons(arg, args);
  }
  args = sexp_nreverse(args);
  return sexp_from(op, args);
}

static Sexp *range_ctor(const string *op, ast_expression *exp) {
  UNUSED(op); UNUSED(exp); //TEMP!
  return sexp_null();
}

// Works for capture and the "insert capture" also
static Sexp *capture_ctor(const string *op, ast_expression *exp) {
  assert(exp->child);
  Sexp *arg, *args;
  arg = ast_to_sexp(exp->child);
  if (!arg) return NULL;
  args = sexp_cons(arg, sexp_null());
  arg = sexp_bytestring(String_ptr(exp->name), String_len(exp->name));
  if (!arg) {
    free_sexp(args);
    return NULL;
  }
  args = sexp_cons(arg, args);
  return sexp_from(op, args);
}

typedef ast_expression *Operator (Sexp *);

// The table below maps S-expression identifiers to constructor
// functions.  In PL terms, this is an *environment* with which we
// will evaluate S-expressions.  Our S-expressions have no binding
// forms, so this env is immutable.

#define SEXP_OPERATORS(X)						\
  X("def",     AST_DEFINITION,    sexp_to_definition,    definition_ctor) \
  X("mod",     AST_MODULE,        sexp_to_module,        module_ctor)	\
  X("seq",     AST_SEQ,           sexp_to_seq,           sequence_ctor)	\
  X("choice",  AST_CHOICE,        sexp_to_choice,        choice_ctor)	\
  X("repeat",  AST_REPETITION,    sexp_to_repetition,    repetition_ctor) \
  X("find",    AST_FIND,          sexp_to_find,          one_arg_ctor)	\
  X("byte",    AST_BYTE,          sexp_to_byte,          byte_ctor)	\
  X("bset",    AST_BYTESET,       sexp_to_byteset,       byteset_ctor)	\
  X("brange",  AST_BYTERANGE,     sexp_to_byterange,     byterange_ctor) \
  X("char",    AST_CHAR,          sexp_to_char,          char_ctor)	\
  X("set",     AST_SET,           sexp_to_set,           set_ctor)	\
  X("range",   AST_RANGE,         sexp_to_range,         range_ctor)	\
  X("capture", AST_CAPTURE,       sexp_to_capture,       capture_ctor)	\
  X("insert",  AST_INSERT,        sexp_to_insert,        capture_ctor)	\
  X("backref", AST_BACKREF,       sexp_to_backref,       one_arg_ctor)	\
  X(">",       AST_LOOKAHEAD,     sexp_to_lookahead,     one_arg_ctor)	\
  X("!>",      AST_NEGLOOKAHEAD,  sexp_to_neglookahead,  one_arg_ctor)	\
  X("<",       AST_LOOKBEHIND,    sexp_to_lookbehind,    one_arg_ctor)	\
  X("!<",      AST_NEGLOOKBEHIND, sexp_to_neglookbehind, one_arg_ctor)	\
  X(NULL, -1, NULL, NULL)

#define _FIRST(a, b, c, d) a,
static const string *const OP_NAME[] = { SEXP_OPERATORS(_FIRST) };
#undef _FIRST

#define _SECOND(a, b, c, d) b,
static int OP_TYPE[] = { SEXP_OPERATORS(_SECOND) };
#undef _SECOND

#define _THIRD(a, b, c, d) c,
static Operator *OP_ENV[] = { SEXP_OPERATORS(_THIRD) };
#undef _THIRD

typedef Sexp *sexp_ctor_type(const string *, ast_expression *);
#define _FOURTH(a, b, c, d) d,
static sexp_ctor_type *OP_CTOR[] = { SEXP_OPERATORS(_FOURTH) };
#undef _FOURTH

#define ID_EQ(a, b) ((strncmp(a, b, MAX_IDLEN+1) == 0))

// Asymptotically slow, but the table is really really short
static int lookup_operator(string *id) {
  for (int i = 0; OP_NAME[i]; i++)
    if (ID_EQ(id, OP_NAME[i])) return i;
  return -1;
}

// TODO: CPS+defunctionalize in conjunction with eval
static ast_expression *eval_apply(Sexp *s) {
  assert(sexp_listp(s) && sexp_proper_listp(s));
  // Look up car of S-expression, which is the operation to apply
  // Apply the operation to the cdr of the S-expression, which are the args
  if (!s->car || !s->cdr) {
    warn("sexp-ast", "Invalid cons cell in eval_apply");
    return NULL;
  }
  Sexp *operator = s->car;
  Sexp *operands = s->cdr;
  if (!sexp_identifierp(operator)) {
    string *tmp = write_sexp(s); 
    warn("sexp-ast", "Operator is not an identifier: %s", tmp); 
    free(tmp); 
    return NULL;
  }
  string *printable = String_escape(operator->str);
  int op_number = lookup_operator(printable);
  if (op_number < 0) {
    warn("sexp-ast", "Unknown operator: %s", printable);
    string_free(printable);
    return NULL;
  }
  string_free(printable);
  Operator *op = OP_ENV[op_number];
  return op(operands);
}

ast_expression *sexp_to_ast(Sexp *s) {
  if (!s) {
    warn("sexp-ast", "Null S-expression arg in sexp_to_ast");
    return NULL;
  }
  // If we have a list, ensure it is a proper list (ending in NULL)
  // and that the list is not incomplete.
  if (sexp_listp(s) && sexp_proper_listp(s)) return eval(s);
  // Or we have an atom (in Scheme/Lisp terminology) or an error
  if (sexp_atomp(s)) return eval(s);
  // Else 'e' must be one of the various expression errors
  warn("sexp-ast",
       "Invalid S-expression (type %s)",
       sexp_type_name(s->type));
  return NULL;
}

// -----------------------------------------------------------------------------
// Convert an AST into its equivalent S-expression format
// -----------------------------------------------------------------------------

// Asymptotically slow, but the table is really really short
static int sexp_ctor_lookup(int id) {
  for (int i = 0; OP_TYPE[i] >= 0; i++)
    if (id == OP_TYPE[i]) return i;
  return -1;
}

Sexp *ast_to_sexp(ast_expression *e) {
  if (!e) return sexp_null();
  Sexp *s;
  // First determine if we need to produce an atom
  switch (e->type) {
    case AST_TRUE:
      s = sexp(SEXP_TRUE);
      return s;
    case AST_FALSE:
      s = sexp(SEXP_FALSE);
      return s;
    case AST_IDENTIFIER:
      s = sexp(SEXP_IDENTIFIER);
      if (!s) return NULL;
      s->str = String_dup(e->name);      
      return s;
    case AST_BYTESTRING:
      s = sexp(SEXP_BYTESTRING);
      if (!s) return NULL;
      s->str = String_dup(e->name);      
      return s;
    case AST_CHAR:
      s = sexp(SEXP_CHAR);
      if (!s) return NULL;
      s->codepoint = e->codepoint;
      return s;
    case AST_NUMBER:
      s = sexp(SEXP_INT);
      if (!s) return NULL;
      s->n = e->n;
      return s;
    default: {
      // Need to produce a list of operator and args
      int i = sexp_ctor_lookup(e->type);
      if (i < 0) {
	warn("sexp-ast", "Unhandled expression type: %s (%d)",
	     ast_type_name(e->type), e->type);
	return NULL;
      }
      sexp_ctor_type *make_sexp = OP_CTOR[i];
      const string *op = OP_NAME[i];
      assert(make_sexp && op);
      return make_sexp(op, e);
    }
  }
}
