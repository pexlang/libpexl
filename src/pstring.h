//  -*- Mode: C; -*-                                                       
// 
//  pstring.h  General-purpose string functions
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

/*
  This package consolidates the string data types and operations that
  we use in the PEXL project:

  - 'string' is an ordinary C string with a NUL terminator.

  - 'String' is the usual pointer-and-length, and is meant to store
    immutable contents.  There are no setters for its pointer or len.

  - 'StringBuffer' builds a string by appending; when finished,
    convert it to a 'string' or 'String' to use it.

  - 'StringSlice' is a view into a 'String'.  Since a 'String' is
    immutable, a slice, which is a pointer and length, is valid as
    long as 'String' has not been freed.
*/


#ifndef pstring_h
#define pstring_h

#include "preamble.h"

#define STRINGBUFFER_MINSIZE (105)
#define STRINGBUFFER_MAXCONCAT (1LL << 24)

/* ----------------------------------------------------------------------------- */
/* String types                                                                  */
/* ----------------------------------------------------------------------------- */

// Ordinary C string, null terminated
typedef char string;

// 'Strings' are immutable, while lowercase 'strings' may be const or
// not.  A 'String' is a pointer-and-length structure that owns its
// storage space: The 'String' record is not separate from the
// contents, the actual data.
typedef struct String {
  size_t len;			// Constant
  string ptr[1];		// Must be last
} String;

// Buffer that grows as needed.  NOTE: Taking a slice of a buffer is
// not allowed, because the value of 'ptr' can change.
typedef struct StringBuffer {
  size_t  len;		     // Number of bytes in use
  size_t  capacity;	     // Current capacity in bytes
  string *ptr;	             // Points to staticbuf OR to a malloc'd region
  string  staticbuf[1];      // Data storage, at least initially
} StringBuffer;

// Pointer-and-length structure that DOES NOT OWN ITS STORAGE
typedef struct StringSlice {
  const string *ptr;
  size_t  len;
} StringSlice;

/* ----------------------------------------------------------------------------- */
/* Character tests for printable ASCII string representations                    */
/* ----------------------------------------------------------------------------- */

#define STRING_ESCAPE_CHARS "\\\"rnt"
#define STRING_ESCAPE_VALUES "\\\"\r\n\t"
char escape_char(const char *c);
char to_hex1(char n);

/* ----------------------------------------------------------------------------- */
/* Predicate maker (to test a single byte)                                       */
/* ----------------------------------------------------------------------------- */

#define PREDICATE(name, chr)                            \
  bool name(const char *c) { return (*c == chr); }

#define PREDICATE_DECL(name)				\
  bool name(const char *c);

PREDICATE_DECL(eofp);
PREDICATE_DECL(quotep);
PREDICATE_DECL(newlinep);
PREDICATE_DECL(returnp);
PREDICATE_DECL(spacep);
PREDICATE_DECL(tabp);
PREDICATE_DECL(digitp);
PREDICATE_DECL(alphap);
PREDICATE_DECL(whitespacep);
PREDICATE_DECL(printablep);
PREDICATE_DECL(hexp);
PREDICATE_DECL(hexletterp);

bool printable_bytep(char c);

/* ----------------------------------------------------------------------------- */
/* Operations on NUL-terminated C strings, using libc when possible              */
/* ----------------------------------------------------------------------------- */

size_t        string_len(const string *str, size_t max);
string       *string_dup(const string *str, size_t max);
string       *string_chr(const string *haystack, char c, size_t max);
const string *string_str(const string *haystack, const string *needle, size_t max);
void          string_free(string *str);
void          string_const_free(const string *str);
int           string_cmp(const string *s1, const string *s2, size_t max);
string       *string_escape(const string *str, size_t len);
string       *string_from_String(String *str);

/* ----------------------------------------------------------------------------- */
/* String operations                                                             */
/* ----------------------------------------------------------------------------- */

// Create a String

String *String_from(const string *source, size_t len);
String *String_from_literal(const char *source);
String *String_fill(char c, size_t len);
String *String_dup(String *str);
void    String_free(String *str);

// Query a String

size_t        String_len(String *str);
string       *String_ptr (String *str);
int           String_cmp (String *s1, String *s2);
int           String_string_cmp (String *s1, const char *ptr, size_t len);
int           StringSlice_cmp (StringSlice s1, StringSlice s2);

// Get, check, and query a slice

StringSlice   String_slice(String *str, size_t start, size_t len);
bool          StringSlice_is_valid(StringSlice slice);
const string *StringSlice_ptr(StringSlice slice);
size_t        StringSlice_len(StringSlice slice);

// Convert between a string of arbitrary bytes and an "escaped" format
// containing only printable ASCII characters.
  
string *String_escape(String *str);
String *String_unescape(String *str, const string **err);
String *string_unescape(string *src, size_t maxlen, const string **err);
String *string_unescape_const(const string *src, size_t maxlen, const string **err);

/* ----------------------------------------------------------------------------- */
/* String buffer operations                                                      */
/* ----------------------------------------------------------------------------- */

StringBuffer *StringBuffer_new(size_t initial_capacity);
void          StringBuffer_free(StringBuffer *b);
void          StringBuffer_reset(StringBuffer *b);
string       *StringBuffer_tostring(StringBuffer *buf);
String       *StringBuffer_toString(StringBuffer *buf);

// Write into a String Buffer

int     StringBuffer_add(StringBuffer *b, string *ptr, size_t len);
int     StringBuffer_add_string(StringBuffer *b, const string *s);
int     StringBuffer_add_char(StringBuffer *b, const char c);
int     StringBuffer_add_slice(StringBuffer *b, StringSlice slice);
int     StringBuffer_add_String(StringBuffer *buf, String *str);

// Query a String Buffer

size_t      StringBuffer_len(StringBuffer *buf);
size_t      StringBuffer_capacity(StringBuffer *buf);
const char *StringBuffer_ptr(StringBuffer *buf);

#endif // pstring_h
