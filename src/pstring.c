//  -*- Mode: C; -*-                                                       
// 
//  pstring.c  General-purpose string functions
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

#include "pstring.h"

/*
  NOTE: This implementation is NOT THREAD-SAFE.
*/


/* ----------------------------------------------------------------------------- */
/* Operations on NUL-terminated C strings, using libc when possible              */
/* ----------------------------------------------------------------------------- */

// Hide the representation, in case we later change it.
void string_free(string *str) {
  if (str) free(str);
}

// Constant strings that are not literals must be freed at some point,
// but we can't pass them to free() without a compiler warning.  Using
// string_const_free() produces no warning.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
void string_const_free(const string *str) {
  if (str) free((void *) str);
}
#pragma GCC diagnostic pop

// Returns actual length or a size larger than max
size_t string_len(const string *str, size_t max) {
  return str ? strnlen(str, max+1) : 0;
}

// Returns a copy of str, only up to max chars, always NUL-terminated
string *string_dup(const string *str, size_t max) {
  return str ? strndup(str, max) : NULL;
}

// Returns:
//  1 if s1 not NULL and s2 NULL
// -1 if s2 not NULL and s1 NULL
//  0 if both s1 and s2 NULL
// else both are non-null and result is strncmp(s1, s2, max)
int string_cmp(const string *s1, const string *s2, size_t max) {
  if (s1)
    return s2 ? strncmp(s1, s2, max) : 1;
  else
    return s2 ? -1 : 0;
}

// Requires two passes through str, but it's safe
string *string_chr(const string *str, char c, size_t max) {
  return str ? memchr(str, c, string_len(str, max)) : NULL;
}

// Returns a newly allocated string.  Copies str and adds a NUL
// terminator, even if str contains embedded NUL bytes.
string *string_from_String(String *str) {
  size_t len = String_len(str);
  string *s = malloc(len + 1);
  if (!s) return NULL;
  memcpy(s, String_ptr(str), len);
  s[len] = '\0';
  return s;
}

// We want 'strnstr', but it is not portable.  This implemenetation is
// not the fastest, but it is safe.
const string *string_str(const string *haystack, const string *needle, size_t max) {
  if (!haystack || !needle) return NULL;
  size_t len = string_len(haystack, max);
  if (len <= max) return strstr(haystack, needle);
  // Else str has more chars than we are told to search, i.e. 'max'.
  size_t needle_len = string_len(needle, max);
  if (needle_len > len) return NULL;
  const string *ptr = haystack;
  while (ptr) {
    ptr = memchr(ptr, needle[0], len - needle_len);
    if (ptr && (memcmp(ptr, needle, needle_len) == 0))
      return ptr;
  }
  return NULL;
}

// For internal use only:

static size_t length(const string *s) {
  return strnlen(s, STRINGBUFFER_MAXCONCAT+1);
}

static int lengthOK(size_t len) {
  return (len <= STRINGBUFFER_MAXCONCAT);
}

/* ----------------------------------------------------------------------------- */
/* String operations                                                             */
/* ----------------------------------------------------------------------------- */

// Copies from 'source' if 'source' is not NULL; else fills with 'c'
static String *make_String(const char *source, char c, size_t len) {
  String *str = malloc(sizeof(String) + len - 1);
  if (!str) {
    warn("pstring", "Out of memory");
    return NULL;
  }
  str->len = len;
  if (source)
    memcpy(str->ptr, source, len);
  else 
    memset(str->ptr, c, len);
  return str;
}

String *String_fill(char c, size_t len) {
  return make_String(NULL, c, len);
}

String *String_from(const string *source, size_t len) {
  return source ? make_String(source, 0, len) : NULL;
}

String *String_from_literal(const char *source) {
  if (source) {
    size_t len = length(source);
    if (lengthOK(len)) 
      return make_String(source, 0, len);
  }
  warn("pstring",
       "Literal string length exceeds STRINGBUFFER_MAXCONCAT (%zu)",
       (size_t) STRINGBUFFER_MAXCONCAT);
  return NULL;
}

String *String_dup(String *str) {
  if (!str) return NULL;
  size_t totalsize = sizeof(String) + str->len - 1;
  String *new = malloc(totalsize);
  if (!new) {
    warn("pstring", "Out of memory");
    return NULL;
  }
  memcpy(new, str, totalsize);
  return new;
}

// We know for sure that a String was malloc'd, so we can free it.
// But the signature of free() requires a non-const pointer.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"

void String_free(String *str) {
  if (str) free((void *)str);
}

// This actually checks that the slice is not invalid, because a NULL
// 'ptr' field invalidates a slice.  No way to test that a pointer is
// valid, i.e. that it points to an allocated object in memory.
bool StringSlice_is_valid(StringSlice slice) {
  return (slice.ptr != NULL);
}

#pragma GCC diagnostic pop

size_t String_len (String *str) {
  return str ? str->len : 0;
}

string *String_ptr (String *str) {
  return str ? str->ptr : NULL;
}

const string *StringSlice_ptr (StringSlice slice) {
  return slice.ptr;
}

size_t StringSlice_len (StringSlice slice) {
  return slice.len;
}

static int compare(const string *ptr1,
		   const size_t len1,
		   const string *ptr2,
		   const size_t len2) {
  int s1_shorter = (len1 < len2);
  size_t len = s1_shorter ? len1 : len2;
  int cmp = strncmp(ptr1, ptr2, len);
  if (cmp) return cmp;
  if (len1 == len2) return 0;
  return 1 - (2 * s1_shorter);
}

int String_cmp (String *s1, String *s2) {
  if (!s1 || !s2) return 0;
  return compare(s1->ptr, s1->len, s2->ptr, s2->len);
}

int String_string_cmp (String *s1, const char *ptr, size_t len) {
  if (!s1 || !ptr) return 0;
  return compare(s1->ptr, s1->len, ptr, len);
}

int StringSlice_cmp (StringSlice s1, StringSlice s2) {
  if (!StringSlice_is_valid(s1) || !StringSlice_is_valid(s2)) return 0;
  return compare(s1.ptr, s1.len, s2.ptr, s2.len);
}

StringSlice String_slice (String *str, size_t start, size_t len) {
  StringSlice slice = {.ptr = NULL, .len = 0};
  if (!str) return slice;
  if ((start >= str->len) || (start+len > str->len)) {
    warn("pstring", "String slice out of bounds");
    return slice;
  }
  return (StringSlice) {.len = len, .ptr = str->ptr + start};
}

/* ----------------------------------------------------------------------------- */
/* String buffer general operations                                              */
/* ----------------------------------------------------------------------------- */

StringBuffer *StringBuffer_new(size_t initial_capacity) {
  if (initial_capacity < STRINGBUFFER_MINSIZE)
    initial_capacity = STRINGBUFFER_MINSIZE;
  StringBuffer *buf = malloc(sizeof(StringBuffer) + initial_capacity - 1);
  if (!buf) {
    warn("pstring", "Out of memory");
    return NULL;
  }
  buf->ptr = buf->staticbuf;
  buf->len = 0;
  buf->capacity = initial_capacity;
  return buf;
}

void StringBuffer_free(StringBuffer *buf) {
  if (!buf) return;
  // If we are using a malloc'd region instead of the
  // statically-allocated buffer, we have to free that region
  if (buf->ptr != buf->staticbuf) free(buf->ptr);
  free(buf);
}

size_t StringBuffer_len(StringBuffer *buf) {
  return buf ? buf->len : 0;
}

size_t StringBuffer_capacity(StringBuffer *buf) {
  return buf ? buf->capacity : 0;
}

// NOTE: The returned pointer is only valid until the next addition to
// 'buf'.  And, the data at the pointer must not be modified!
const char *StringBuffer_ptr(StringBuffer *buf) {
  return buf ? buf->ptr : NULL;
}

// Return a null-terminated string.  Not responsible if buf contains
// any NUL characters.
string *StringBuffer_tostring(StringBuffer *buf) {
  if (!buf || !buf->ptr) return NULL;
  string *result = malloc(buf->len + 1);
  if (!result) {
    warn("pstring", "Out of memory");
    return NULL;
  }
  memcpy(result, buf->ptr, buf->len);
  result[buf->len] = '\0';
  return result;
}

String *StringBuffer_toString(StringBuffer *buf) {
  if (!buf || !buf->ptr) return NULL;
  return make_String(buf->ptr, 0, buf->len);
}

void StringBuffer_reset(StringBuffer *buf) {
  if (!buf) return;
  buf->len = 0;
}

/* ----------------------------------------------------------------------------- */
/* Write into a string buffer                                                    */
/* ----------------------------------------------------------------------------- */

// Allocates new storage if necessary, so that 'size' bytes can be
// added to 'buf'.
static int prepare(StringBuffer *buf, size_t size) {
  assert(buf);
  assert(buf->capacity > 0);
  if (!lengthOK(size)) {
    warn("pstring",
	 "Cannot concatenate more than STRINGBUFFER_MAXCONCAT (%zu) bytes",
	 (size_t) STRINGBUFFER_MAXCONCAT);
    return ERR;
  }
  size_t needed = buf->len + size;
  if (needed < buf->capacity) return OK;
  size_t newsize = 2 * buf->capacity;
  if (newsize < needed) newsize = 2 * needed;
  // If we are still using the statically-allocated buffer, then we
  // need to switch to a malloc'd one.
  string *tmp;
  if (buf->ptr == buf->staticbuf) {
    tmp = malloc(newsize);
    if (!tmp) {
      warn("pstring", "Out of memory");
      return ERR;
    }
    memcpy(tmp, buf->staticbuf, buf->capacity);
  } else {
    trace("pstring", "Expanding dynamic string space, newsize = %zu", newsize);
    tmp = realloc(buf->ptr, newsize);
    if (!tmp) return ERR;
  }
  buf->ptr = tmp;
  buf->capacity = newsize;
  return OK;
}

static int buffer_add(StringBuffer *buf, const string *ptr, size_t len) {
  assert(buf);
  assert(ptr);
  if (prepare(buf, len)) return ERR;
  memcpy(buf->ptr + buf->len, ptr, len);
  buf->len += len;
  assert(buf->len <= buf->capacity);
  return OK;
}

// Add anything using a pointer and a length
int StringBuffer_add(StringBuffer *buf, string *ptr, size_t len) {
  if (!buf || !ptr) return ERR;
  return buffer_add(buf, ptr, len);
}

// Convenience: Add a null-terminated string 's'
int StringBuffer_add_string(StringBuffer *buf, const string *s) {
  if (!buf || !s) return ERR;
  size_t len = length(s);
  return buffer_add(buf, s, len);
}

// Convenience: Add a single byte
int StringBuffer_add_char(StringBuffer *buf, const char c) {
  if (!buf) return ERR;
  return buffer_add(buf, &c, 1);
}

// Add the contents of a String
int StringBuffer_add_String(StringBuffer *buf, String *str) {
  if (!buf || !str) return ERR;
  return buffer_add(buf, str->ptr, str->len);
}

// Add the contents of a Slice
int StringBuffer_add_slice(StringBuffer *buf, StringSlice slice) {
  if (!buf || !StringSlice_is_valid(slice)) return ERR;
  return buffer_add(buf, slice.ptr, slice.len);
}

/* ----------------------------------------------------------------------------- */
/* Character predicates                                                          */
/*    The character tests below are named in lisp style, with a trailing         */
/*    'p' to mark them as PREDICATES.                                            */
/* ----------------------------------------------------------------------------- */

PREDICATE(eofp, '\0');
PREDICATE(quotep, '\"');
PREDICATE(newlinep, '\n');
PREDICATE(returnp, '\r');
PREDICATE(spacep, ' ');
PREDICATE(tabp, '\t');

bool whitespacep(const char *c) {
  return newlinep(c) || spacep(c) || tabp(c) || returnp(c);
}

bool digitp(const char *c) {
  return ((*c >= '0') && (*c <= '9'));
}

bool alphap(const char *c) {
  return (((*c >= 'A') && (*c <= 'Z'))
          || ((*c >= 'a') && (*c <= 'z')));
}

bool printable_bytep(char c) {
  return (c > 32) && (c < 127);
}

bool printablep(const char *c) {
  return c && printable_bytep(*c);
}

// Not a predicate: returns the escaped char value, or the NUL
// character to indicate an error.
char escape_char(const char *c) {
  char *pos = strchr(STRING_ESCAPE_VALUES, *c);
  return (pos && (!eofp(pos))) ?
    STRING_ESCAPE_CHARS[pos - STRING_ESCAPE_VALUES] : '\0';
}

char to_hex1(char n) {
  n = n & 0xF;
  return (n < 10) ? (n + '0') : (n - 10) + 'A';
}

// Returns a negative number on failure
static int from_hex1(const char *c) {
  return digitp(c) ? (*c - '0') : (hexletterp(c) ? ((*c & ~0x20) - 'A') + 10 : -1000);
}

static int unescape_hex(const char *sptr) {
  const char *high_nibble = sptr++;
  const char *low_nibble = sptr++;
  return (from_hex1(high_nibble)*16) + from_hex1(low_nibble);
}

static int unescape_char(const char *sptr) {
  const char *pos = strchr(STRING_ESCAPE_CHARS, *sptr);
  return (pos && (!eofp(pos))) ? 
    STRING_ESCAPE_VALUES[pos - STRING_ESCAPE_CHARS] : -1;
}

/* ----------------------------------------------------------------------------- */
/* Escaping/unescaping                                                           */
/* ----------------------------------------------------------------------------- */

/* 
  Allowed characters in a printable string are only printable ASCII.
  Anything else must be written using one of these escape sequences:
     \xHH  hex code, where HH are hex digits, e.g. \xFF
     \t    tab (ASCII 0x09 = 9)
     \n    linefeed (ASCII 0x0A = 10)
     \r    return (ASCII 0x0D = 13)
     \\    backslash (ASCII 0x5C = 92)
     \"    double quote (ASCII 0x22 = 34)
*/

// Returns a newly allocated string which caller must eventually free.
// The returned string is NUL terminated and contains only printable
// ASCII characters. 
string *string_escape(const string *str, size_t len) {
  char c;
  size_t i = 0;
  // Cheap error handling: received NULL, return NULL
  if (!str) return NULL;
  // Worst case: Every byte in str expands into \xHH (4 bytes).
  string *result = malloc(len * 4 + 1);
  if (!result) {
    warn("pstring", "Out of memory");
    return NULL;
  }
  const string *s = str;
  const string *end = str + len;
  while (s < end) {
    if ((printablep(s)) || spacep(s))
      result[i++] = *s;
    else {
      result[i++] = '\\';
      if ((c = escape_char(s)) != '\0') {
        result[i++] = c;
      } else {
        c = ((unsigned char) *s) >> 4;
        result[i++] = 'x';
        result[i++] = to_hex1(c);
        result[i++] = to_hex1(*s & 0xF);
      }
    }
    s++;
  } // while
  result[i] = '\0';
  return result;
}

string *String_escape(String *str) {
  return string_escape(String_ptr(str), String_len(str));
}

// Process up to 'maxlen' bytes of 'src'.  All bytes in 'src' must be
// printable ASCII, and 'src' must have a NUL terminator, which does
// not have to be before 'maxlen'.  All escape sequences in 'src' are
// translated into their actual byte values.
//
// IMPORTANT:
// 
// - On error, the partial results are returned, and *err points to
//   the character in 'src' that caused the error.
// - When no error has occurred, *err will be NULL.
//
static String *unescape(const string *src, size_t maxlen, const string **err) {
  if (!src || !err) {
    warn("pstring", "Null required argument");
    return NULL;
  }
  StringBuffer *buf = StringBuffer_new(maxlen); // an overestimate
  if (!buf) {
    warn("pstring", "Out of memory");
    return NULL;
  }
  int chr;
  *err = NULL;
  const char *end = src + maxlen;

  while (src < end) {

    //    confess("TEMP", "at index %zu: *src is '%c'", (src - (end - maxlen)), *src);
    // Check for end of input
    if (*src == '\0') break;

    // Check for escape sequence, hex or non-hex
    if (*src == '\\') {
      src++;
      if (*src == 'x') {
	if ((chr = unescape_hex(++src)) < 0) {
	  *err = --src;		// Error code 'x': bad hex escape
	  break;
	}
	// Successful hex escape, e.g. \x20
	StringBuffer_add_char(buf, chr & 0xFF);
	src += 2;		// Hex escape is 2 chars long
      } else {
	if ((chr = unescape_char(src)) < 0) {
	  *err = --src;		// Error code '\': bad non-hex escape
	  break;
	}
	// Successful non-hex escape, e.g. \t
	StringBuffer_add_char(buf, chr & 0xFF);
	src++;
      }
      continue;
    }

    // Check for unescaped quotation mark
    if (quotep(src)) {
      *err = src;	 // Error code '"': Unescaped quotation mark
      break;
    }

    // Check for other printable ASCII and ordinary whitespace
    if (printablep(src) || whitespacep(src)) {
      StringBuffer_add_char(buf, *src);
      src++;
      continue;
    }

    // Else a disallowed byte was found
    *err = src;
    break;
    
  } // while

  String *result = StringBuffer_toString(buf);
  StringBuffer_free(buf);
  return result;
}

String *string_unescape_const(const string *src, size_t maxlen, const string **err) {
  return unescape(src, maxlen, err);
}

String *string_unescape(string *src, size_t maxlen, const string **err) {
  return unescape(src, maxlen, err);
}

String *String_unescape(String *str, const string **err) {
  return unescape(String_ptr(str), String_len(str), err);
}
