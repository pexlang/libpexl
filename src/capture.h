/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  capture.c  The built-in capture processors                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef capture_h
#define capture_h

#include "preamble.h"
#include "libpexl.h"
#include "binary.h"
#include "match.h"

#define capidx(cap) ((cap)->data)
#define setcapidx(cap, newidx) do {		\
    (cap)->data = (newidx);			\
  } while (0)
#define capkind(cap) ((cap)->kind)
#define setcapkind(cap, kindval) do {		\
    (cap)->kind = (kindval);			\
  } while (0)

/* High bit zero means Open, one means Close */
#define isopencap(cap)	(!(capkind(cap) & 0x80))
#define isclosecap(cap)	((capkind(cap) & 0x80))

#define acceptable_capture(kind) (((kind) == Copen) || ((kind) == Cconstant))

typedef struct pexlCapture {
  const string *s;	    /* position in input data */
  pexl_Index  pkg_number;   /* package whose code is being executed */
  uint32_t    kind : 8;	    /* capture kind */
  uint32_t    data : 24;    /* offset into string block */
} pexlCapture;

/*
  A pexlEncoder processes the output of the vm (captures, cap_count)
  to produce whatever output format is needed, such as colorized text
  or a parse tree data structure or a JSON representation of the parse
  tree.
*/
typedef int (*pexlEncoder)(const struct pexl_PackageTable *pt,
			   const string *input, size_t len,
			   struct pexlCapture *captures, pexl_Index cap_count,
			   struct pexl_Match *match);

/*
  An pexlEncoder allocates a data structure, which must be freed by
  the user using a pexlEncoderDataFree function:
*/
typedef void (*pexlEncoderDataFree)(void *data);

pexlEncoder         get_encoder_fn (pexlEncoderID id);
pexlEncoderDataFree get_encoder_destructor (pexlEncoderID id);

#endif
