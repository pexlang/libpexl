/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ast-print.h  Print the Abstract Syntax Tree                              */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, James Deucher                                */

#ifndef ast_print_h
#define ast_print_h

#include "ast.h"

void ast_print_tree (ast_expression *exp);

#endif
