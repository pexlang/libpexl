/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  table.h   Dynamically expanding tables of records                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#ifndef table_h
#define table_h

#include "preamble.h"
#include "libpexl.h"
#include "bitflags.h"
                                // Effect when set:
BITFLAGS(TABLE_SOFTFAIL);	// Do not abort() if out of memory

typedef struct Table {
  char       *records;
  size_t      record_size;
  pexl_Index  next;
  pexl_Index  capacity;
  pexl_Index  max_capacity;
  uint16_t    flags;
} Table;

Table *Table_new(pexl_Index initial_capacity,
		 pexl_Index max_capacity,
		 size_t     record_size,
		 uint16_t   flags);

void Table_free(Table *tbl);

pexl_Index Table_add(Table *tbl, void **record);
void      *Table_get(Table *tbl, pexl_Index i);
void      *Table_iter(Table *tbl, pexl_Index *prev);
pexl_Index Table_size(Table *tbl);
pexl_Index Table_capacity(Table *tbl);
pexl_Index Table_set_size(Table *tbl, pexl_Index value);

pexl_Index Table_empty(Table *tbl);
pexl_Index Table_push(Table *tbl, void **record);
void *Table_pop(Table *tbl);
void *Table_top(Table *tbl);

#endif
