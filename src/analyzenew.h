/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyze.h   Analyze expressions                                          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Jack Deucher                                 */

#if !defined(analyze_h)
#define analyze_h

#include "preamble.h"
#include "libpexl.h"
#include "env.h"
#include "ast.h"
#include "ast-xform.h"
#include "context.h"

// /* ----------------------------------------------------------------------------- */
// /* pexl_Expr *API (external)                                                     */
// /* ----------------------------------------------------------------------------- */

// /* Return values for exp_patlen, distinguishable from errors because these are > 0 */
#define EXP_UNBOUNDED 101
#define EXP_BOUNDED 202

uint32_t exp_nodecount (ast_expression *exp);

int exp_hascaptures_new (ast_expression *exp);          /* checks calls made by node */
int exp_itself_hascaptures_new (ast_expression *exp);   /* does not check calls */
int exp_patlen_new (ast_expression *exp, uint32_t *min, uint32_t *max);
int exp_nofail_new (ast_expression *exp);
int exp_nullable_new (ast_expression *exp);
int exp_headfail_new (ast_expression *exp);
int exp_needfollow_new (ast_expression *exp);
int exp_getfirst_new (ast_expression *exp, const Charset *follow, Charset *firstset);

void reset_compilation_state (Pattern *pat);
// int  fix_open_calls (CompState *cs);
// int  set_metadata (CompState *cs);


#endif
