/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp-reader.c   S-expression reader                                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "sexp-reader.h"
#include "k.h"

/* 
  -----------------------------------------------------------------------------
  GRAMMAR
  -----------------------------------------------------------------------------

  The read_sexp() procedure works similarly to Scheme's 'read', which
  "converts external representations of Scheme objects into the
  objects themselves."  ASCII S-expressions conform to the following
  grammar, in which literals are enclosed in single quotes:

     <Sexp> := <atom> | <list>
     <atom> := <identifier> | <bytestring> | <codepoint> | <int>
     <list> := '(' <Sexp>* ')'
     <identifier> := <idstart> <idchar>*
     <bytestring> := '"' (<unambiguous> | <whitespace> | <escape>)* '"'
     <whitespace> := <space> | <tab> | <newline> | <return>

     <idstart> := [A-Z] | [a-z] | <id_special_start_chars>
     <idchar> := <idstart> | <digit> | <id_special_more_chars>

     <tab> := ASCII code 9
     <newline> := ASCII code 10
     <return> := ASCII code 13
     <space> := ASCII code 32
     <quote> := ASCII code 34
     <backslash> := ASCII code 92
     <unambiguous> := ASCII codes 33, 35-91, 93-126
     <printable> := ASCII codes 33-126

     <escape> := '\' ('x' <xdigit>{2} | <unix-escape>)
     <unix-escape> :=  <backslash> | <quote> | 't' | 'n' | 'r'

     <digit> := [0-9]
     <xdigit> := <digit> | [a-f] | [A-F]

     <codepoint> := '#\u' <xdigit>{4} | '#\U' <xdigit>{8} | <byte>
     <byte> := '#\x' <xdigit>{2}  | <named-char>
     <named-char> := '#\space' | '#\tab' | '#\newline' etc.

     <int> := ('+' | '-')? <digit>+    // note that sign is optional

  The read_sexp() parser is insensitive to whitespace except that
  comments, which begin with ';', extend to the end of the line (\n).
   
  See sexp.c for these definitions:
     <id_special_start_chars>
     <id_special_more_chars>

  -----------------------------------------------------------------------------
  IMPLEMENTATION
  -----------------------------------------------------------------------------

  The implementation tokenizes the input as needed by the parser.  The
  signature of read_sexp() is:

    Sexp *read_sexp(string **s)

  If 'string *input' is the string you want to parse, you might write:

    string *ptr = input;
    Sexp *exp = read_sexp(&ptr);

  And read_sexp() will advance 'ptr' as it parses, so you can call
  read_sexp() repeatedly.  The 'exp' returned will either be one of
  the <Sexp> variants mentioned above, or NULL, or an error indicator.

  A return value of NULL indicates EOF.  Error expressions include:
     SEXP_INCOMPLETE, signalling a list that is not properly closed
     SEXP_EXTRACLOSE, indicating an extraneous closing paren
     SEXP_ERROR, where the error is specified by an enclosed Token

  Note that SEXP_INCOMPLETE appears in the CDR position of a list,
  where SEXP_NULL should be if the list were properly formed.  The
  caller of read_sexp() could use SEXP_INCOMPLETE to prompt for more
  input and continue filling out the list, or nested lists, that are
  under construction.

  SEXP_EXTRACLOSE could be ignored, though typically a warning is
  issued to inform the user.

  -----------------------------------------------------------------------------
  RECURSION HAS BEEN REMOVED
  -----------------------------------------------------------------------------

  The implementation of several procedures, including read_sexp(),
  would ordinarily be written in a straightforward recursive fashion.
  Instead, they employ a defunctionalized continuation-passing style.
  See k.c for more.

*/

/* ----------------------------------------------------------------------------- */
/* Utilities                                                                     */
/* ----------------------------------------------------------------------------- */

static const string *scan(bool pred(const string *), const string *s) {
  while (*s && pred(s)) s++;
  return s;
}

static const string *until(bool pred(const string *), const string *s) {
  while (*s && !pred(s)) s++;
  return s;
}

static bool token_eofp(Token tok) {
  return tok.type == TOKEN_EOF;
}
    
static bool atmospherep(Token tok) {
  return (tok.type == TOKEN_COMMENT) || (tok.type == TOKEN_WS);
}

// Not a predicate, since it returns an int.  But can be used as one
// if the caller checks only 0 (false) versus non-zero (true).
static int codepoint_sigil(const string *s) {
  switch(*s) {
    case 'u': return 4;
    case 'U': return 8;
    case 'x': return 2;
    default: return 0;		// false
  }
}

// Having seen the starting quote, scan until closing quote (or
// error). On return, *sptr points to the byte past the closing quote,
// or it points at the char that caused an error.
// 
// Note: The input at *sptr should to contain only printable ASCII and
// to be enclosed in double quotes, else it is not a valid bytestring
// representation.
static int validate_bytestring(const string **sptr) {
  assert(quotep(*sptr));
  const string *err;
  // MAX_BYTES_LEN applies to the longest byte string we will store.
  // In the worst case, every byte in the string is written as a hex
  // escape in the input, with the form \xHH and taking up 4 bytes.
  // So here we must be willing to read 4 * MAX_BYTES_LEN chars from
  // the input (at most), plus one byte for the closing quote mark.
  String *tmp = string_unescape_const(++(*sptr), 4 * MAX_BYTES_LEN + 1, &err);
  // We expect an "error" in the sense that an unescaped double quote
  // marks the end of the string we are scanning.  We will not be
  // returning the result of string_unescape, so we can free it now.
  size_t len = String_len(tmp);
  String_free(tmp);
  if (err) {
    // Advance the input pointer to the closing quote or problem char
    *sptr = err;
    // Now check to see why the unescaping stopped
    if (quotep(err)) {
      if (len <= MAX_BYTES_LEN) return TOKEN_BYTESTRING;
      return TOKEN_BAD_LEN;
    }
    if (*err == '\\') return TOKEN_BAD_ESC;
    if (*err == 'x') return TOKEN_BAD_ESC;
    return TOKEN_BAD_CHAR;
  } 
  // The unescaping reached the end of the input (or the max len we
  // passed in) and did not find a closing quote.  Therefore, this
  // bytestring is "unterminated" and the code for that is TOKEN_BAD_BYTES.
  return TOKEN_BAD_BYTESTRING;
}

/* ----------------------------------------------------------------------------- */
/* Creating tokens                                                               */
/* ----------------------------------------------------------------------------- */

static Token eof = {.type = TOKEN_EOF};
static Token open = {.type = TOKEN_OPEN};
static Token close = {.type = TOKEN_CLOSE};

// Note: 'end' is allowed to point beyond the end of the
// null-terminated string that begins at 'start'.  The 'strndup'
// function stops at NUL, so we never read past the end of the string.
static Token error_token(Token_type err, const string *start, const string *end) {
  return (Token) {.type = err,
                  .start = start,
                  .len = end - start};
}

static bool token_errorp(Token tok) {
  return (tok.type >= TOKEN_PANIC);
}

static Token id(const string **sptr) {
  assert(id_charp(*sptr));          // id_charp contains id_startp
  const string *start = *sptr;
  *sptr = scan(id_charp, *sptr);
  if (!delimiterp(*sptr)) 
    return error_token(TOKEN_BAD_CHAR, start, ++(*sptr));
  uint32_t len = *sptr - start;
  if (len <= MAX_IDLEN) 
    return (Token){.type = TOKEN_IDENTIFIER,
                   .start = start,
                   .len = len};
  else
    return error_token(TOKEN_BAD_IDENTIFIER, start, *sptr);
}

// NOTE: An unprocessed bytestring contains the delimiting double
// quotes in its first and last bytes.
static Token bytestring(const string **sptr) {
  assert(quotep(*sptr));
  const string *start = *sptr;
  Token_type status = validate_bytestring(sptr);
  if (status == TOKEN_BYTESTRING) {
    assert(quotep(*sptr));
    (*sptr)++;
    return (Token){.type = TOKEN_BYTESTRING,
                   .start = start,
                   .len = *sptr - start};
  }
  return error_token(status, start, *sptr);
}

static Token integer(const string **sptr) {
  const string *s = *sptr;
  const string *digits = (plusp(s) || minusp(s)) ? s+1 : s;
  *sptr = scan(digitp, digits);
  if ((*sptr > digits) && ((*sptr - s) <= MAX_INT_LEN)) {
    if (delimiterp(*sptr))
      return (Token){.type = TOKEN_INT,
                     .start = s,
                     .len = *sptr - s};
    else
      return error_token(TOKEN_BAD_INT, s, ++(*sptr));
  }
  return error_token(TOKEN_BAD_INT, s, *sptr);
}

static Token parse_named_char(const string **sptr) {
  const string *s = *sptr;
  int i = 0;
  while (character_names[i]) {
    if ((string_cmp(s, character_names[i], character_name_lengths[i]) == 0)
	&& delimiterp(s + character_name_lengths[i])) {
      *sptr += character_name_lengths[i];
      return (Token){.type = TOKEN_CHAR,
                     .start = s,
                     .len = *sptr - s};
    }
    i++;
  } // while
  return error_token(TOKEN_BAD_CHAR, s, ++(*sptr));
}

// Parse a string of (ASCII) hex digits of length n that follows a
// one-byte sigil, returning a 'type' token or, on failure, an 'err'
// token.  IMPORTANT: The returned token includes the sigil at *sptr.
static Token parse_hex_digits(const string **sptr,
			      int n,
			      Token_type type,
			      Token_type err) {
  if (n <= 0) return panicToken;
  const string *s = *sptr;
  *sptr = scan(hexp, s+1);
  int len = *sptr - s - 1;
  if (len == n) {
    if (delimiterp(*sptr))
      return (Token){.type = type,
		     .start = s,
		     .len = *sptr - s};
  } else {
    return error_token(err, s, (*sptr)++);
  }
  return error_token(err, s, *sptr);
}

// Token returned starts with the sigil 'x' or 'u' or 'U'
static Token parse_codepoint(const string **sptr) {
  return parse_hex_digits(sptr,
			  codepoint_sigil(*sptr), // 2, 4, or 8 chars
			  TOKEN_CHAR,
			  TOKEN_BAD_CHAR);
}

// Token returned starts with a backslash.  Entire token span consists
// of a backslash and a single byte e.g. '\1' or '\A'.  (There is a
// delimiter after the byte, so we know we have '\x' not '\xFF' here.)
static Token char_token(const string *start, const string *end) {
  return (Token){.type = TOKEN_CHAR,
                 .start = start,
                 .len = end - start};
}

static Token sharp(const string **sptr) {
  Token tok;
  const string *s = *sptr;	// Save for error reporting
  (*sptr)++;			// Skip the sharp sign
  if (*(*sptr)++ == '\\') {
    // Various kinds of character constants 
    if (printablep(*sptr) && delimiterp(*sptr+1)) return char_token(s+1, ++(*sptr));
    if (codepoint_sigil(*sptr)) return parse_codepoint(sptr);
    // No sigil for this case, so either it succeeds or we have some
    // unrecognizable (invalid) sharp expression:
    tok = parse_named_char(sptr);
    if (!token_errorp(tok)) return tok;
  }
  return error_token(TOKEN_BAD_SHARP, s, *sptr);
}

static Token whitespace(const string **sptr) {
  const string *s = *sptr;
  *sptr = scan(whitespacep, s);
  return (Token){.type = TOKEN_WS,
                 .start = s,
                 .len = *sptr - s};
}

static Token comment(const string **sptr) {
  const string *s = *sptr;
  *sptr = until(newlinep, s);
  return (Token){.type = TOKEN_COMMENT,
                 .start = s,
                 .len = *sptr - s};
}

static Token read_token(const string **s) {
  if (!s || !*s) return panicToken;
  if (openp(*s)) {(*s)++; return open;}
  if (closep(*s)) return ((*s)++, close);
  if (whitespacep(*s)) return whitespace(s);
  if (id_startp(*s)) return id(s);
  if (digitp(*s) || plusp(*s) || minusp(*s)) return integer(s);
  if (sharpp(*s)) return sharp(s);
  if (quotep(*s)) return bytestring(s);
  if (commentp(*s)) return comment(s);
  if (eofp(*s)) return eof;
  const string *start = *s;
  return error_token(TOKEN_BAD_CHAR, start, ++(*s));
}

// Low-level API.  Can be used to peek at comments or whitespace.
Token sexp_read_token(const string **s) {
  return read_token(s);
}

// An extra close paren could be ignored, perhaps with a warning.  It
// is a recoverable situation.
static Sexp *sexp_extraclose(void) {
  return sexp(SEXP_EXTRACLOSE);
}

// An incomplete list is different from an unrecoverable error.  A
// parser could continue reading, and possibly complete the
// S-expression, if given more input.
static Sexp *sexp_incomplete(void) {
  return sexp(SEXP_INCOMPLETE);
}

// Unrecoverable error, as indicated by the enclosed token, which
// contains a COPY of the offending piece of input.
static Sexp *sexp_error(Token t) {
  Sexp *e = sexp(SEXP_ERROR);
  if (!e) return NULL;
  e->error.type = t.type;
  e->error.data = String_from(t.start, (size_t) t.len);
  return e;
}

/* ----------------------------------------------------------------------------- */
/* S-expressions (Lists)                                                         */
/* ----------------------------------------------------------------------------- */

// Data stored in a continuation structure
typedef struct kdata {
  Sexp *retval;
} kdata;

// Declare continuation data type and names type-specific functions
KDATATYPE(kdata, newstack, push, data, setdata);

// Continuation types needed for the functions defined below
KTYPES(K_READ_VALUE,	     // Have a return value
       K_READ_OPEN);	     // Build a list

static void update_retval(Kontinuation *k, Sexp *replacement) {
  kdata tmp = data(k);
  tmp.retval = replacement;
  setdata(k, tmp);
}

/* ----------------------------------------------------------------------------- */
/* Parsing S-expressions                                                         */
/* ----------------------------------------------------------------------------- */

static void pushk_read_value(Kstack *ks, Sexp *exp) {
  push(ks, K_READ_VALUE, (kdata){exp});
}

static void pushk_read_open(Kstack *ks) {
  push(ks, K_READ_OPEN, (kdata){sexp_null()});
}

static Token read_semantic_token(const string **s) {
  Token tok;
  do {
    tok = read_token(s);
  } while (atmospherep(tok));
  return tok;
}

static void read_sexp_k(Token current, const string **sptr, Kstack *ks);

static void read_list_k(Token tok, const string **sptr, Kstack *ks) {
  if (token_errorp(tok)) {
    pushk_read_value(ks, sexp_error(tok));
    return;
  }
  if (tok.type == TOKEN_CLOSE) {
    Sexp *result = data(K_top(ks)).retval;
    assert(result);
    K_pop(ks);
    pushk_read_value(ks, sexp_nreverse(result));
    return;
  }
  read_sexp_k(tok, sptr, ks);
}

// A TOKEN_BYTES contains the opening and closing quotation marks, so
// its length is always at least 2.  We strip these off when we create
// the S-expression.  Note that sexp_bytes() will unescape the string.
static Sexp *sexp_bytes_from_token(const string *start, uint32_t len) {
  assert(start);
  assert(len >= 2);
  if (!start || (len < 2)) {
    warn("reader", "Invalid token contents (TOKEN_BYTES)");
    return NULL;
  }
  return sexp_bytestring(start+1, len-2);
}

static void read_sexp_k(Token tok, const string **sptr, Kstack *ks) {
 tailcall:
  switch (tok.type) {
  case TOKEN_OPEN:
    pushk_read_open(ks); return;
  case TOKEN_CLOSE:
    pushk_read_value(ks, sexp_extraclose()); return;
  case TOKEN_IDENTIFIER:
    pushk_read_value(ks, sexp_identifier(tok.start, tok.len)); return;
  case TOKEN_BYTESTRING:
    pushk_read_value(ks, sexp_bytes_from_token(tok.start, tok.len)); return;
  case TOKEN_INT:
    pushk_read_value(ks, sexp_int(tok.start, tok.len)); return;
  case TOKEN_CHAR:
    pushk_read_value(ks, sexp_codepoint(tok.start, tok.len)); return;
  case TOKEN_WS:
  case TOKEN_COMMENT:
    tok = read_semantic_token(sptr);  
    goto tailcall;
  case TOKEN_EOF:
    pushk_read_value(ks, sexp_incomplete()); return;
  default:
    assert(token_errorp(tok));
    pushk_read_value(ks, sexp_error(tok));
    return;
  }
}

static void free_data(Kontinuation *k) {
  if (k->type == K_READ_OPEN) free_sexp(data(k).retval);
}

Sexp *read_sexp(const string **s) {
  Token tok = read_semantic_token(s);
  if (token_eofp(tok)) return NULL;
  Kstack *ks = newstack(free_data);
  Kontinuation *k;
  Sexp *exp = NULL;
  read_sexp_k(tok, s, ks);
  while ((k = K_top(ks))) {
    switch (k->type) {
      case K_READ_VALUE:
	exp = data(k).retval;
	if (sexp_errorp(exp)) goto bailout;
	k = K_pop(ks);
	if (!k) goto bailout;
	assert(k->type == K_READ_OPEN);
	update_retval(k, sexp_cons(exp, data(k).retval));
	if ((exp->type == SEXP_INCOMPLETE) || (exp->type == SEXP_EXTRACLOSE)) {
	  exp = sexp_nreverse(data(k).retval);
	  k = K_pop(ks);
	  goto bailout;
	}
	// fallthrough
      case K_READ_OPEN:
	tok = read_semantic_token(s);
	read_list_k(tok, s, ks);
	break;
      default:
	warn("reader", "Unexpected continuation type %d", k->type);
	abort();
    }
  }
 bailout:
  Kstack_free(ks);
  return exp;
}
  

