#ifndef pathtable_h
#define pathtable_h
#include "preamble.h"
#include "libpexl.h"
#include "pstring.h"

/*
  A path specifies the nodes to follow in a match subtree.  It is used
  in 'where' expressions specifically.
*/

typedef struct Path {
  char **segments; /* A null terminated array of strings */
} Path;

#define PATHTABLE_INIT_SIZE 20
#define PATHTABLE_MAX_SIZE (1 << 20)

typedef struct PathTable {
  Path       **paths;
  pexl_Index   capacity;
  pexl_Index   next;
} PathTable;


Path *break_path (const char *str);
void  path_free (Path *p);
void  print_path(Path *p);

PathTable  *pathtable_new (pexl_Index initial_size);
pexl_Index  pathtable_add (PathTable *pathtab, Path *path);
void        pathtable_free (PathTable *pathtab);
PathTable * pathtable_copy (PathTable *pathtab);
int         pathtable_grow(PathTable *pathtab);
pexl_Index  pathtable_break_and_add(const char *str, PathTable *pathtab);
#endif
