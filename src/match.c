/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  match.c     Ordered n-ary tree for match results                         */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "vm.h"			// pexl_Match
#include "match.h"

//  Dynamically expand the tree.  Returns 0 on success.
static pexl_Index resize (pexl_MatchTree *m, pexl_Index new_capacity) {
  assert(new_capacity > 0);
  if (m->capacity >= MATCHTREE_MAXNODES) {
    inform("byte_decoder", "MatchTree at capacity, cannot expand");
    return MATCHTREE_ERR_FULL;
  }
  if (new_capacity > MATCHTREE_MAXNODES) new_capacity = MATCHTREE_MAXNODES;
  trace("byte_decoder",
       "expanding MatchTree node array from %zu to %zu",
	m->size, new_capacity);
  match_node *temp = realloc(m->nodes, new_capacity * sizeof(match_node));
  if (temp == NULL) {
    warn("byte_decoder", "out of memory");
    return MATCHTREE_ERR_OOM;
  }
  m->nodes = temp;
  m->capacity = new_capacity;
  return MATCHTREE_OK;
}

/* ----------------------------------------------------------------------------- */
/* MatchTree operations                                                          */
/* ----------------------------------------------------------------------------- */

pexl_MatchTree *match_tree_new (size_t capacity) {
  if (capacity < MATCHTREE_INITCAPACITY) capacity = MATCHTREE_INITCAPACITY;
  if (capacity > MATCHTREE_MAXNODES) {
    inform("byte_decoder", "requested capacity exceeeds configured maximum");
    return NULL;
  }
  pexl_MatchTree *m = (pexl_MatchTree *) calloc(1, sizeof(pexl_MatchTree));
  if (!m) {
    warn("byte_decoder", "out of memory");
    return NULL;
  }
  m->nodes = (match_node *) malloc(capacity * sizeof(match_node));
  if (!m->nodes) {
    free(m);
    warn("byte_decoder", "out of memory");
    return NULL;
  }
  m->capacity = (pexl_Index) capacity;
  /* All other slots are 0/NULL due to calloc above */
  return m;
}

void match_tree_reset (pexl_MatchTree *m) {
  if (m) {
    if (!m->nodes) {
      confess("MatchTree", "missing node array in tree");
      return;
    }
    memset(m->nodes, 0, m->capacity * sizeof(match_node));
    m->size = 0;
    trace("MatchTree", "freeing string block during match_tree_reset");
    if (m->block) free((void *)(m->block));
    m->block = NULL;
    m->blocklen = 0;
  }
}

void match_tree_free (pexl_MatchTree *tree) {
  if (!tree) return;
  if (tree->nodes) free(tree->nodes);
  if (tree->block) free(tree->block);
  free(tree);
}

match_node *get_node (pexl_MatchTree *m, pexl_Index index) {
  assert(m->capacity > 0);
  assert(m->nodes);
  if ((index < 0) || ((size_t) index >= m->size)) return NULL;
  return &(m->nodes[index]);
}

/* 
   Add a new child to parent.  The tree must be filled in pre-order.
   Note that when adding a child, we know type and start.  Later, when
   we discover values for data and end, we set those in a separate
   step with 'close'.  To add the root, call with -1 for parent.

   Returns index to node that was added or < 0 for error.
*/
pexl_Index match_tree_add (pexl_MatchTree *m, pexl_Index parent_idx,
			  pexl_Index prefix, pexl_Index type, pexl_Position start) {
  pexl_Index err;
  match_node *slot, *parent;
  if (!m) {
    inform("byte_decoder", "required arg is null");
    return MATCHTREE_ERR_NULL;
  }
  if (m->size >= m->capacity) {
    if ((err = resize(m, 2 * m->capacity)))
      return err;
  }
  parent = get_node(m, parent_idx);
  if (parent) {
    /* Set sibling pointer in the last child, if any */
    if (parent->last_child) {
      slot = get_node(m, parent->last_child);
      if (slot) slot->sibling = m->size;
    }
    /* Update last child pointer */
    parent->last_child = m->size;
  } 
  /* 
     The data buffer holding the tree may be reused, so we zero it out
     every time we add a node.
  */
  slot = &(m->nodes[m->size]);
  memset(slot, 0, sizeof(match_node));
  slot->data.prefix = prefix;
  slot->data.type = type;
  slot->data.start = start;
  slot->data.end = -1;		/* Mark as NOT set */

  /* Other fields should be 0 already, due to memset above */
  m->size++;
  return m->size - 1;
}

int match_tree_close (pexl_MatchTree *match, pexl_Index node_idx, pexl_Index data, pexl_Position end) {
  if (!match) {
    inform("byte_decoder", "required arg is null");
    return MATCHTREE_ERR_NULL;
  }
  match_node *node = get_node(match, node_idx);
  if (!node) {
    inform("byte_decoder", "invalid node index");
    return MATCHTREE_ERR_NOEXIST;
  }
  assert((node->data.inserted_data == 0) && (node->data.end == -1));
  node->data.inserted_data = data;
  node->data.end = end;
  return MATCHTREE_OK;
}

/* ----------------------------------------------------------------------------- */
/* Traversals                                                                    */
/* ----------------------------------------------------------------------------- */

/* 
   Pre-order traversal.  Trivial because nodes stored in pre-order.
   Supply PEXL_ITER_START as the value of 'iter' to get started.  On
   success, fills in the 'node' structure with a copy of the tree node.
*/
pexl_Index matchtree_iter (pexl_Match *match, pexl_Index prev, pexl_MatchNode **node) {
  assert(match);
  assert(node);
  prev++;
  match_node *probe = get_node((pexl_MatchTree *)match->data, prev);
  if (likely(probe)) {
    *node = &(probe->data);
    return prev;
  }
  *node = NULL;
  return MATCHTREE_ERR_NOEXIST;
}

/* 
   Visit first child of parent, which can be PEXL_ITER_START.  
*/
pexl_Index match_tree_child (pexl_MatchTree *match, pexl_Index parent, match_node **node) {
  match_node *probe;
  pexl_Index child = 0;
  if (parent == PEXL_ITER_START) {
    probe = get_node(match, 0);		  /* Try for root */
    if (!probe) return PEXL_ERR_INTERNAL; /* Error already logged */
    *node = probe;
    return 0;			          /* Root index */
  }
  probe = get_node(match, parent);
  if (!probe) {
    inform("tree", "Invalid parent node %" pexl_Index_FMT, parent);
    return PEXL_ERR_INTERNAL;
  }
  if (!probe->last_child) return PEXL_MATCH__NOEXIST; /* No children */
  child = parent + 1;
  probe = get_node(match, child);
  if (!probe) {
    warn("tree", "Failed to find node %" pexl_Index_FMT, parent + 1);
    return PEXL_ERR_INTERNAL;
  }
  *node = probe;
  return child;
}

/* 
   Visit next sibling of prev, which must be a valid node index
*/
pexl_Index match_tree_next (pexl_MatchTree *match, pexl_Index prev, match_node **node) {
  match_node *probe = get_node(match, prev);
  if (!probe) {
    warn("tree", "Failed to find node %" pexl_Index_FMT, prev);
    return PEXL_MATCH__ERR_NULL;
  }
  if (!probe->sibling) return PEXL_MATCH__NOEXIST; /* No more siblings */
  pexl_Index sibling = probe->sibling;
  probe = get_node(match, sibling);
  if (!probe) {
    warn("tree", "failed to find node %" pexl_Index_FMT, sibling);
    return PEXL_MATCH__ERR_NULL;
  }
  *node = probe;
  return sibling;
}

