/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  arena.c   General-purpose arena allocator                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "arena.h"
#include "bitflags.h"

// Chris Wellons has an elegant approach to arena allocation from
// which we are borrowing heavily.
// https://nullprogram.com/blog/2023/09/27/

// Arenas are passed by copy and they contain a "start" pointer to an
// available region.  It is easy to accidentally free a pointer that
// has not been allocated when destroying an arena.  The only arena
// that can be destroyed using free() is the original one returned
// from arena_create().
//
// To better catch errors, we tag "original" arenas by setting the low
// bit of the arena's end address.  This technique relies on two things:
// (1) malloc always returns aligned pointers (always even)
// (2) we create arenas in sizes that are multiples of ARENA_MINSIZE
//
// We require ARENA_MINSIZE to be even, which means both the start and
// end addresses of every arena are even.  An odd "end" address is our
// flag.  (Look up "pointer tagging" for more about this technique.)

static bool has_original_flag(arena a) {
  return (uintptr_t)a.end & 1;
}
static arena add_original_flag(arena a) {
  return (arena) {a.start, a.end + 1};
}
static arena ignore_original_flag(arena a) {
  return (arena) {a.start, (char *)((uintptr_t)a.end & -2)};
}
static bool invalid_arena(arena *a) {
  return !a || !a->start;
}

// Allocate in multiples of ARENA_MINSIZE
arena arena_create(ptrdiff_t capacity) {
  arena a = {NULL, NULL};
  if (capacity <= 0) return a;
  ptrdiff_t padding = -(uintptr_t)capacity & (ARENA_MINSIZE - 1);
  capacity += padding;
  a.start = malloc(capacity);
  if (!a.start) {
    warn("arena", "out of memory");
    return a;
  }
  a.end = a.start + capacity;
  return add_original_flag(a);
}

void arena_destroy(arena *a) {
  if (invalid_arena(a)) return;
  if (!has_original_flag(*a)) {
    warn("arena", "cannot free derived arena");
    return;
  }
  free(a->start);
  a->start = NULL;
  a->end = NULL;
  return;
}

ptrdiff_t arena_size(arena a) {
  arena aa = ignore_original_flag(a);
  return (aa.end - aa.start);
}

// NOTE: Oddly, dividing by 0 returns 0 for ptrdiff_t types.
// We avoid doing that here, in case that behavior is not portable.
//
// The 'malloc' attribute enables the C compiler to do some
// optimizations that depend on this fact: The pointer returned does
// not alias other active pointers.
__attribute__((malloc, alloc_size(2, 3), alloc_align(4)))
void *arena_allocate(arena *a,
		     ptrdiff_t size, ptrdiff_t count, ptrdiff_t align,
		     int flags) {
  if (invalid_arena(a)) {
    warn("arena", "invalid arena arg");
    return NULL;
  }
  if ((size < 1) || (count < 1) || (align < 1)) {
    warn("arena", "invalid arg for size, count, or alignment");
    return NULL;
  }
  arena copy = ignore_original_flag(*a);
  ptrdiff_t avail = copy.end - copy.start;
  ptrdiff_t padding = -(uintptr_t)copy.start & (align - 1);
  if (count > (avail - padding)/size) {
    warn("arena", "out of space in arena");
    if (HAS_BITFLAG(flags, ARENA_SOFTFAIL)) return NULL;
    abort();
  }
  ptrdiff_t total = size * count;
  char *p = copy.start + padding;
  a->start = p + total;		// advance the start of available space
  a->end = copy.end;		// clear the ORIGINAL flag
  return HAS_BITFLAG(flags, ARENA_NOZERO) ? p : memset(p, 0, total);
}
