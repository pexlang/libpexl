/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  opt.h  One header file for opt-*.c                                      */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Jamie A. Jennings                                              */


#ifndef opt_h
#define opt_h

#include "preamble.h"
#include "libpexl.h"
#include "instruction.h"
#include "binary.h"
#include "context.h"
#include "print.h"		/* for debugging/logging output */
#include <alloca.h>

/* ----------------------------------------------------------------------------- */
/* COMPILER OPTIMIZATION and RUNTIME CONFIGURATION                               */
/* ----------------------------------------------------------------------------- */

/* Compiler options: ordered list of optimization passes with parameters */
struct pexl_Optims {
  enum pexl_OptimType  type;
  uint32_t            *params;
  struct pexl_Optims  *next;
}; 

/* COMMON UTILITIES */

#define gethere(compst) ((compst)->codenext)
#define getinstr(cs, i) ((cs)->code[i])

int finaltarget (Instruction *code, int i);

void jumptothere (pexl_Binary *pkg, int instruction, int target);
void jumptohere (pexl_Binary *pkg, int instruction);

bool optimlist_is_valid (pexl_Optims *optims);
bool optimlist_contains (pexl_Optims *optims, enum pexl_OptimType optim_type);

/* ----------------------------------------------------------------------------- */
/* Configuring optimizers                                                        */
/* ----------------------------------------------------------------------------- */

void free_optims (pexl_Optims *optims);
pexl_Optims *addopt_tro (pexl_Optims *optims);
pexl_Optims *addopt_simd (pexl_Optims *optims, uint32_t cache_size);
pexl_Optims *addopt_dynamic_simd (pexl_Optims *optims);
pexl_Optims *addopt_inline (pexl_Optims *optims);
pexl_Optims *addopt_peephole (pexl_Optims *optims);
pexl_Optims *addopt_unroll (pexl_Optims *optims, uint32_t max_times_to_unroll);
Instruction *copy_instructions (pexl_Binary *pkg, pexl_Index start, pexl_Index size);
int move_instructions (pexl_Binary *pkg, pexl_Index src, pexl_Index dest, pexl_Index size);

/* ----------------------------------------------------------------------------- */
/* Utilities shared by multiple optimizers                                       */
/* ----------------------------------------------------------------------------- */

void adjust_symbol_table (pexl_Binary *pkg, uint32_t i, int32_t delta);
void adjust_ins_offset (pexl_Binary *pkg, uint32_t inline_start, int32_t delta);
pexl_Index relative_to_abs (const Instruction *op, const Instruction *p);

/* ----------------------------------------------------------------------------- */
/* OPTIMIZERS                                                                    */
/* ----------------------------------------------------------------------------- */

/* Peephole only for jumps and calls */
int peephole (pexl_Binary *pkg);

/* Loop unrolling */
typedef uint32_t unroll_policy_fn(pexl_Binary *pkg, Instruction *instr, uint32_t num_unroll);
unroll_policy_fn unroll_policy;
int              loop_optim (pexl_Binary *pkg, unroll_policy_fn policy, uint32_t param);

/* Inlining */
typedef uint32_t   inlining_policy_fn(pexl_Binary *pkg, int i);
inlining_policy_fn naive_inlining_policy;
int                inlining_optimizer (pexl_Binary *pkg, inlining_policy_fn policy);

/* Tail recursion optimization */
int tro (pexl_Binary *pkg);

#endif
