/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  compile.h      Compile expressions and environments                      */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef compile_h
#define compile_h

#include "preamble.h"
#include "libpexl.h"
#include "context.h"

#if (PEXL_NO_MAIN >= 0)
#error "PEXL_NO_MAIN must be negative else it could be a valid binding index"
#endif

/* ----------------------------------------------------------------------------- */
/* Compiler (internal API)                                                       */
/* ----------------------------------------------------------------------------- */

pexl_Error   new_error (pexl_Index val, pexl_Index loc);
pexl_Index   error_value (pexl_Error err);
pexl_Index   error_location (pexl_Error err);
pexl_Ref     bind_in (pexl_Expr *exp, pexl_Env env, pexl_Index handle, BindingTable *bt);
int          rebind (pexl_Ref ref, pexl_Expr *exp, BindingTable *bt);
void         free_bound_value (Binding *b);


pexl_Binary *compile (pexl_Context *C,
		      pexl_Ref main,		    // can be PEXL_NO_MAIN
		      pexl_Optims *optims,          // can be NULL
		      pexl_BinaryAttributes *attrs, // can be NULL
		      enum pexl_CharEncoding enc,
		      pexl_Error *err);

// Returns true only if SIMD was available on the platform on which
// libpexl was compiled.  This allows (but does not require) pattern
// byte code to be built with SIMD -- this is controlled by a PEXL
// compiler optimization.  Note also that SIMD can be available in
// libpexl, and a pattern can be compiled with SIMD support, but the
// user can disable SIMD for a single invocation of the vm.
int simd_available (void);

#endif

