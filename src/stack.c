/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  stack.c   Stacks used by the vm                                          */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

/* 
   This file is INCLUDED by vm.c.  It is used only by the vm, and
   including it there makes it easy for the C compiler to inline the
   stack functions. 
*/

/* ----------------------------------------------------------------------------- 
 * Backtrack stack 
 * ----------------------------------------------------------------------------- 
 *
 * A Backtrack Stack is an array of frames, plus:
 * 
 *   base -- pointer to first entry in the array
 *   next -- pointer to next free entry (top is at next-1)
 *   limit -- pointer just beyond last allocated entry
 *         (when next==limit, the stack is full)
 *   maxtop -- high water mark, or -1 if not tracking this
 *
 * When the stack is full, it will be expanded automatically on the
 * next push, up to some defined maximum size.
 *
 * Because the stack struct values are actual pointers, and not
 * integer indices into the array, the user of a stack MUST NOT store
 * the values of base, next, or limit.  These pointers will CHANGE
 * when the stack is expanded.
 *
 */

typedef struct BTCallContents {
  const Instruction *pc;     /* Return address */
  const pexl_Binary     *pkg;    /* Saved package (has instruction vector) */
} BTCallContents;

typedef struct BTChoiceContents {
  const Instruction *pc;      /* First instruction of second choice */
  const char        *c;	      /* Saved cursor position */
  pexl_Index         capnext; /* Pointer to next free slot in capture stack */
} BTChoiceContents;

typedef struct BTLookAroundContents {
  const char *boi;           /* Old value for BOI */
  const char *eoi;	     /* Old value for EOI */
  bool        capturing;     /* Old state of capturing or not */
} BTLookAroundContents;

typedef struct BTNarrowContents {
  const char *c;             /* Saved cursor position */
  const char *boi;           /* Old value for BOI */
  const char *eoi;	     /* Old value for EOI */
  bool        capturing;     /* Old state of capturing or not */
} BTNarrowContents;

typedef struct BTBackLoopContents {
  const Instruction *pc;        /* First instruction of backloop pattern */
  const char        *c;	        /* Next position from which to attempt match */
  const char        *leftfence;	/* Final position from which to attempt match */
} BTBackLoopContents;

#define BTUNWIND_STOP(frametype) ((frametype) & 0x10)
#define BTLOOKAROUND(frametype) ((frametype) & 0x01)

// Note: BTEntry is 32 bytes
//
// Note: Low bit must be 1 if stack unwinding should stop there, and 0
// for frames that have saved the input fences, cursor position,
// and/or capturing state
typedef struct BTEntry {
  enum {
    BTCall = 0x0,               /* BTCallContents */
    BTLookAhead = 0x1,          /* BTLookAroundContents */
    BTLookBehind = 0x3,         /* BTLookAroundContents */
    BTNarrow = 0x05,            /* BTNarrowContents */
    BTChoice = 0x10,            /* BTChoiceContents */
    BTFindLoop = 0x12,          /* BTChoiceContents */
    BTBackLoop = 0x14,          /* BTBackLoopContents */
  } frametype;
  union {
    BTCallContents       call;
    BTChoiceContents     choice;
    BTLookAroundContents lookaround;
    BTNarrowContents     narrow;
    BTBackLoopContents   backloop;
  } contents;
} BTEntry;

typedef struct BTStack {
  BTEntry *base;
  BTEntry *next;
  BTEntry *limit;
  ssize_t  maxtop;	  /* High water mark, or -1 if not tracking */
} BTStack;

static __attribute__((unused))
const char *BTEntryName (int frametype) {
  switch (frametype) {
  case BTCall: return "BTCall";
  case BTChoice: return "BTChoice";
  case BTFindLoop: return "BTFindLoop";
  case BTLookAhead: return "BTLookAhead";
  case BTLookBehind: return "BTLookBehind";
  case BTNarrow: return "BTNarrow";
  case BTBackLoop: return "BTBackLoop";
  default: return "UNKNOWN";
  };
}

static
size_t btstack_capacity (BTStack *stack) {
  assert(stack->limit > stack->base);
  return stack->limit - stack->base;
}

static
size_t btstack_size (BTStack *stack) {
  assert(stack->next >= stack->base);
  return stack->next - stack->base;
}

static __attribute__((unused))
size_t btstack_highwater (BTStack *stack) {
  return stack->maxtop;
}

/* Empty stack returns NULL pointer */
static 
BTEntry *btstack_top (BTStack *stack) {
  if (unlikely(stack->next == stack->base))
    return NULL;
  return stack->next - 1;
}

static
void btstack_reset (BTStack *stack, bool track_highwater) {
  trace("btstack", "resetting backtrack stack");
  stack->next = stack->base;
  stack->maxtop = track_highwater ? 0 : -1;
}

static __attribute__((noinline))
BTStack *btstack_new (bool track_highwater) {
  trace("btstack", "allocating new backtrack stack");
  BTStack *stack = malloc(sizeof(BTStack));
  if (!stack) {
    warn("btstack", "out of memory");
    return NULL;
  }
  stack->base = (BTEntry *) malloc(sizeof(BTEntry) * BTSTACK_INITIAL_CAPACITY);
  stack->limit = stack->base + BTSTACK_INITIAL_CAPACITY;
  btstack_reset(stack, track_highwater);
  return stack;
}

static __attribute__((noinline))
void btstack_free (BTStack *stack) {
  trace("btstack", "freeing backtrack stack");
  free(stack->base);
  free(stack);
}

/* Return OK for success; < 0 on error */
static __attribute__((noinline))
int btstack_expand (BTStack *stack) {
  BTEntry *newstack;
  size_t currentsize, newsize;
  inform("btstack",
	 "expanding backtrack stack (current size %zu)",
	 btstack_capacity(stack));
  currentsize = btstack_capacity(stack);
  assert(currentsize > 0);
  if (currentsize >= BTSTACK_MAX_CAPACITY)
    return ERR;
  newsize = currentsize * 2;
  if (newsize > BTSTACK_MAX_CAPACITY)
    newsize = BTSTACK_MAX_CAPACITY;
  newstack = realloc(stack->base, newsize * sizeof(BTEntry));
  if (!newstack) return ERR;
  stack->base = newstack;
  stack->next = newstack + currentsize;
  stack->limit = newstack + newsize;
  return OK;
}

static inline
BTEntry *btstack_pop (BTStack *stack) {
  trace("btstack",
       "POP size was %ld, top frame type is %s",
       btstack_size(stack),
       btstack_top(stack) ? BTEntryName(btstack_top(stack)->frametype) : "ERROR");
  if (unlikely(stack->next == stack->base)) {
    warn("btstack", "POP of empty stack!");
    return NULL;
  }
  stack->next--;
  return stack->next;
}

static inline
BTEntry *btstack_push (BTStack *stack, int frametype) {
  if (unlikely(stack->next == stack->limit))
    if (unlikely(btstack_expand(stack))) {
      warn("btstack", "backtracking exceeds configured limit");
      return NULL;
    }
  trace("btstack",
       "PUSH size was %ld, capacity is %ld, pushing frame type %s",
       btstack_size(stack),
       btstack_capacity(stack),
       BTEntryName(frametype));
  stack->next->frametype = frametype;
  stack->next++;
  if (stack->maxtop >= 0) {
    ssize_t current = stack->next - stack->base;
    if (current > stack->maxtop) stack->maxtop = current;
  }
  return stack->next - 1;
}

static inline
int btstack_push_choice (BTStack *stack, 
			 const Instruction *pc,
			 const char *c,
			 const pexl_Index capnext) {
  BTEntry *frame = btstack_push(stack, BTChoice);
  if (unlikely(!frame)) return ERR;
  BTChoiceContents *contents = &(frame->contents.choice);
  contents->pc = pc;
  contents->c = c;
  contents->capnext = capnext;
  return OK;
}

static inline
int btstack_push_findloop (BTStack *stack, 
			   const Instruction *pc,
			   const char *c,
			   const pexl_Index capnext) {
  BTEntry *frame = btstack_push(stack, BTFindLoop);
  if (unlikely(!frame)) return ERR;
  BTChoiceContents *contents = &(frame->contents.choice);
  contents->pc = pc;
  contents->c = c;
  contents->capnext = capnext;
  return OK;
}

static inline
int btstack_push_call (BTStack *stack, 
		       const Instruction *pc,
		       const pexl_Binary *pkg) {
  BTEntry *frame = btstack_push(stack, BTCall);
  if (unlikely(!frame)) return ERR;
  BTCallContents *contents = &(frame->contents.call);
  contents->pc = pc;
  contents->pkg = pkg;
  return OK;
}

static
int btstack_push_lookahead (BTStack *stack, 
			    const char *boi,
			    const char *eoi,
			    bool capturing) {
  BTEntry *frame = btstack_push(stack, BTLookAhead);
  if (unlikely(!frame)) return ERR;
  BTLookAroundContents *contents = &(frame->contents.lookaround);
  contents->boi = boi;
  contents->eoi = eoi;
  contents->capturing = capturing;
  return OK;
}

static
int btstack_push_lookbehind (BTStack *stack, 
			     const char *boi,
			     const char *eoi,
			     bool capturing) {
  BTEntry *frame = btstack_push(stack, BTLookBehind);
  if (unlikely(!frame)) return ERR;
  BTLookAroundContents *contents = &(frame->contents.lookaround);
  contents->boi = boi;
  contents->eoi = eoi;
  contents->capturing = capturing;
  return OK;
}

static
int btstack_push_narrow (BTStack *stack, 
			 const char *c,
			 const char *boi,
			 const char *eoi,
			 bool capturing) {
  BTEntry *frame = btstack_push(stack, BTNarrow);
  if (unlikely(!frame)) return ERR;
  BTNarrowContents *contents = &(frame->contents.narrow);
  contents->c = c;
  contents->boi = boi;
  contents->eoi = eoi;
  contents->capturing = capturing;
  confess("TEMP", "BT stack frame just pushd with c = %lld",
	  contents->c - contents->boi);
  return OK;
}

static
int btstack_push_backloop (BTStack *stack, 
			   const Instruction *pc,
			   const char *c,
			   const char *leftfence) {
  BTEntry *frame = btstack_push(stack, BTBackLoop);
  if (unlikely(!frame)) return ERR;
  BTBackLoopContents *contents = &(frame->contents.backloop);
  contents->pc = pc;
  contents->c = c;
  contents->leftfence = leftfence;
  return OK;
}


/* -----------------------------------------------------------------------------
 * pexlCapture stack 
 * ----------------------------------------------------------------------------- 
 *
 * A pexlCapture Stack is an array of capture frames, plus:
 * 
 *   base -- pointer to first entry in the array
 *   next -- index of next available entry (0 when stack is empty)
 *   limit -- index one beyond the allocated capacity
 *         (when next==limit, the stack is full)
 *   maxtop -- high water mark, or -1 if not tracking this
 *
 * When the stack is full, it will be expanded automatically on the
 * next push, up to some defined maximum size.
 *
 * Because the stack struct values are actual pointers, and not
 * integer indices into the array, the user of a stack MUST NOT store
 * the value of base.  This pointer will CHANGE when the stack is
 * expanded.
 *
*/

typedef struct CapStack {
  pexlCapture *base;
  pexl_Index   next;
  pexl_Index   limit;
  ssize_t      maxtop;	  /* High water mark, or -1 if not tracking */
} CapStack;

static
size_t capstack_capacity (CapStack *stack) {
  return stack->limit;
}

static
size_t capstack_size (CapStack *stack) {
  assert(stack->next >= 0);
  assert(stack->next <= stack->limit);
  return stack->next;
}

static __attribute__((unused))
size_t capstack_highwater (CapStack *stack) {
  return stack->maxtop;
}

/* Empty stack returns NULL pointer */
static 
pexlCapture *capstack_top (CapStack *stack) {
  if (unlikely(stack->next == 0))
    return NULL;
  return stack->base + stack->next - 1;
}

static
void capstack_reset (CapStack *stack, bool track_highwater) {
  trace("capstack", "resetting capture stack");
  stack->next = 0;
  stack->maxtop = track_highwater ? 0 : -1;
}

static __attribute__((noinline))
CapStack *capstack_new (bool track_highwater) {
  trace("capstack", "allocating new capture stack");
  CapStack *stack = (CapStack *) malloc(sizeof(CapStack));
  if (!stack) {
    warn("capstack", "out of memory");
    return NULL;
  }
  stack->base = (pexlCapture *) malloc(sizeof(pexlCapture) * CAPSTACK_INITIAL_CAPACITY);
  stack->limit = CAPSTACK_INITIAL_CAPACITY;
  capstack_reset(stack, track_highwater);
  return stack;
}

static __attribute__((noinline))
void capstack_free (CapStack *stack) {
  trace("capstack", "freeing capture stack");
  free(stack->base);
  free(stack);
}

/* Return OK for success; < 0 on error */
static __attribute__((noinline))
int capstack_expand (CapStack *stack) {
  pexlCapture *newstack;
  size_t currentsize, newsize;
  inform("capstack",
	 "expanding capture stack (current size %zu)",
	 capstack_capacity(stack));
  currentsize = capstack_capacity(stack);
  assert(currentsize > 0);
  if (currentsize >= CAPSTACK_MAX_CAPACITY)
    return ERR;
  newsize = currentsize * 2;
  if (newsize > CAPSTACK_MAX_CAPACITY)
    newsize = CAPSTACK_MAX_CAPACITY;
  newstack = realloc(stack->base, newsize * sizeof(pexlCapture));
  if (!newstack) return ERR;
  stack->base = newstack;
  stack->limit = newsize;
  return OK;
}

static inline
pexlCapture *capstack_pop (CapStack *stack) {
  trace("capstack",
       "POP size was %ld, top capture type is %s",
       capstack_size(stack),
       capstack_top(stack) ? pexlCaptureName(capstack_top(stack)->kind) : "ERROR");
  if (unlikely(stack->next == 0)) {
    warn("capstack", "POP of empty stack!");
    return NULL;
  }
  stack->next--;
  return stack->base + stack->next;
}

static inline
void capstack_settop(CapStack *stack, pexl_Index new_next) {
  assert(new_next >= 0);
  assert(new_next <= stack->limit);
  ssize_t oldsize = capstack_size(stack);
  UNUSED(oldsize);
  stack->next = new_next;
  trace("capstack",
       "SETTOP size was %ld, size now %ld, top capture type is now %s",
       oldsize,
       capstack_size(stack),
       capstack_top(stack) ? pexlCaptureName(capstack_top(stack)->kind) : "(empty stack)");
}

static inline
int capstack_push (CapStack *stack,
		   const char *s,
		   pexl_Index pkg_number,
		   uint8_t kind,
		   uint32_t data) {
  if (unlikely(stack->next == stack->limit))
    if (unlikely(capstack_expand(stack))) {
      warn("capstack", "number of captures exceeds configured limit");
      return false;
    }
  trace("capstack",
       "PUSH size was %ld, capacity is %ld, pushing capture type %s",
       capstack_size(stack),
       capstack_capacity(stack),
       pexlCaptureName(kind));
  pexlCapture *entry = stack->base + stack->next;
  entry->s = s;
  entry->pkg_number = pkg_number;
  entry->kind = kind;
  entry->data = data;
  stack->next++;
  if (stack->maxtop >= 0) {
    if (stack->next > stack->maxtop) stack->maxtop = stack->next;
  }
  return true;
}

/* -----------------------------------------------------------------------------
 * Printing for debugging 
 * ----------------------------------------------------------------------------- 
 */
__attribute__((unused))
static void BTEntry_stack_print (BTStack *stack,
				 Instruction *code,
				 Instruction *giveup,
				 const char *c,
				 const char *bof) {
  for (BTEntry *top = btstack_top(stack); top >= stack->base; top--) {
    /* Print index of stack frame */
    fprintf(stderr, "[%ld] ", top - stack->base);
    /* Print frame type */
    switch (top->frametype) {
    case BTCall:       fprintf(stderr, "BTCall           "); break;
    case BTChoice:     fprintf(stderr, "BTChoice         "); break;
    case BTLookAhead:  fprintf(stderr, "BTLookAhead      "); break;
    case BTLookBehind: fprintf(stderr, "BTLookBehind     "); break;
    case BTFindLoop:   fprintf(stderr, "BTFindLoop       "); break;
    case BTBackLoop:   fprintf(stderr, "BTBackLoop       "); break;
    case BTNarrow:     fprintf(stderr, "BTNarrow         "); break;
    }
    /* Print contents specific to frame type */
    switch (top->frametype) {
    case BTCall:
      fprintf(stderr,
              "return addr %ld: %s (in package %p)",
              ((top->contents.call.pc == giveup) ?
	       -1 : (top->contents.call.pc - top->contents.call.pkg->code)),
              OPCODE_NAME(opcode(top->contents.call.pc)),
              (const void *)top->contents.call.pkg);
      break;
    case BTChoice:
    case BTFindLoop:
      fprintf(stderr,
              "dest pc %ld: %s, pos=%ld, captop=%" pexl_Index_FMT,
              ((top->contents.choice.pc == giveup) ?
	       -1 : (top->contents.choice.pc - code)),
              OPCODE_NAME(opcode(top->contents.choice.pc)),
              (top->contents.choice.c == NULL) ? -1 : (top->contents.choice.c - c),
	      top->contents.choice.capnext);
      break;
    case BTLookAhead:
    case BTLookBehind:
      fprintf(stderr,
              "capturing was %s, old boi=%ld, old eoi=%ld",
	      top->contents.lookaround.capturing ? "true" : "false",
              (bof - top->contents.lookaround.boi),
              (bof - top->contents.lookaround.eoi));
      break;
    case BTNarrow:
      fprintf(stderr,
              "capturing was %s, old c=%ld, old boi=%ld, old eoi=%ld",
	      top->contents.narrow.capturing ? "true" : "false",
              (bof - top->contents.narrow.c),
              (bof - top->contents.narrow.boi),
              (bof - top->contents.narrow.eoi));
      break;
    case BTBackLoop:
      fprintf(stderr,
              "pos=%ld, leftfence=%ld",
              ((top->contents.backloop.c == NULL) ?
	       -1 : (bof - top->contents.backloop.c)),
              ((top->contents.backloop.leftfence == NULL) ?
	       -1 : (bof - top->contents.backloop.leftfence)));
      break;
    default:
      fprintf(stderr, "UNKNOWN stack frametype: %d", top->frametype);
    } /* switch on frametype */
    fprintf(stderr, "\n");
  }  /* for each frame on BTEntry_stack */
}
