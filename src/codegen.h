/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  codegen.h                                                                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef codegen_h
#define codegen_h

#include "preamble.h"
#include "libpexl.h"
#include "simd.h"

#include "context.h"
#include "opt.h"

#define INITIAL_CODESIZE (40000)

int codegen (pexl_Context *C,
	     pexl_Optims *optims,
	     Pattern *entrypoint_pattern,
	     pexl_Binary *pkg);

#endif
