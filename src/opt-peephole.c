/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  opt-peephole.c                                                          */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Jamie A. Jennings                                              */


#include "opt.h"

/*
  Apply various optimization over ICall Instructions.
  Returns OK when an optimization is made.
*/
static int optimize_icall (pexl_Binary *pkg, int i)
{
  int stat;
  uint32_t final;
  Instruction *target_ins;
  Instruction *next_ins;
  Instruction *code = pkg->code; /* Code Vector */
  Instruction *pc = &code[i];

  if (opcode(pc) != ICall) return !OK;

  /* ICall uses Absolute Addressing */
  target_ins = code + getaddr(pc);
  if (target_ins == pc) {
    warn("codegen", "call to self is invalid code");
    return PEXL_ERR_INTERNAL;
  }
  if ((target_ins < code) || (target_ins >= code + pkg->codenext)) {
    warn("codegen", "call target address is invalid (outside package)");
    return PEXL_ERR_INTERNAL;
  }
  switch (opcode(target_ins)) {
  case INoop: {
    /*
      FUTURE: Scan forward to find the first instruction that is NOT
      INoop, and change the call target to that instruction.
    */
    // setopcode(pc, opcode(target_ins));
    // setopcode(pc+1, INoop);
    stat = !OK;
    break;
  }
  case ICall: {
    /*
      FUTURE: If the destination of a call is another call followed
      immediately by a return, then we can call the ultimate target
      directly.  (Eta conversion.)
    */
    stat = !OK;
    break;
  }
  case IJmp: {
    /*
      ICall where target is IJmp:
      Update the target of ICall to be the ultimate address.
    */
    final = finaltarget(code, getoffset(pc));
    trace("optimizer",
	  "Found ICall to IJmp at pc = %zu (call to %d, jump by %d ==> call to %d)",
	  (pc - code), getoffset(pc), getoffset(target_ins), final);
    setoffset(pc, final);
    stat = OK;
    break;
  }
  case IEnd: {
    /* Should not happen */
    warn("optimizer",
         "IEnd appeared after ICall instruction at Line: %d in pexl_Binary: %p",
         i, (void *)pkg);
    return !OK;
  }
  default:
    stat = !OK;
  }
  /*
     We have (possibly) adjusted the target of the ICall above.  Now
     we look at the instruction after the ICall, to see if it is IRet.
     ICall followed by IRet ==> Replace ICall with IJmp
  */
  next_ins = pc + sizei(pc);
  if ((next_ins < code + pkg->codenext) && (opcode(next_ins) == IRet)) {
    trace("optimizer",
	  "Found ICall+IRet at pc = %zu/%zu (call to %d ==> jump by %ld)",
	  (pc - code),
	  (size_t)pkg->codenext,
	  getaddr(pc),
	  getaddr(pc) - (pc - code));
    setopcode(pc, IJmp);
    setoffset(pc, getaddr(pc) - (pc - code));
    return OK;
  }
  return stat;
}

/*
  This peephole optimizer tries to make control flow more efficient by
  doing the following:

  (1) Optimize any instruction that jumps:

    - Update target address of jumps with their final destinations
      (e.g. choice L1; ... L1: jmp L2: becomes choice L2).

    - A jump to an instructions that does an unconditional jump
      becomes that instruction (e.g., jump to return becomes a return;
      jump to commit becomes a commit).

  (2) Optimize calls:

    - Eta conversion (see optimize_icall).
    - Call to an IJmp instruction becomes a call to the final
      destination of the IJmp.
    - Tail call optimization: call X; ret ==> jmp X
*/

/* FUTURE: Maybe modify peephole to take an instruction range so that
   we can run it on the code for one pattern at a time.  We should run
   it on the entire package when the package is the result of
   compiling a single pattern, though.
*/

// Perform peephole optimization on jumps and calls.  Process the
// entire code vector.  Return PEXL_OK (0) for success, and a PEXL
// error otherwise.
int peephole (pexl_Binary *pkg) {
  uint32_t final, ft;
  Instruction *code = pkg->code;
  Instruction *inst, *target_inst;
  uint32_t i;
  trace("optimizer",
	"Entering peephole optimizer, pkg = %p, %d/%d instructions",
	(void *)pkg, pkg->codenext, pkg->codesize);
  for (i = 0; i < pkg->codenext; i += sizei(&code[i])) {
  redo:
    inst = &code[i];
    switch (opcode(inst)) {
    case ICall:
      {
	/* Jumps are relative, but calls use an absolute address */
	if (getaddr(inst) < 0) {
	  warn("optimizer", "skipping ICall to invalid target %d", getaddr(inst));
	  return PEXL_ERR_INTERNAL;
	} else {
	  if (optimize_icall(pkg, i) == OK)
	    goto redo;		      /* reoptimize its address */
	  else
	    break;
	}
	break;
      }
    case IChoice:
    case ICommit:
    case IPartialCommit:
    case ITestAny:
    case ITestChar:
    case ITestSet: {
      int destination = getoffset(&code[i]);
      final = finaltarget(code, i + destination);
      jumptothere(pkg, i, final);
      break;
    }
    case IJmp: {
      ft = finaltarget(code, i);
      assert(ft < pkg->codenext);
      target_inst = &code[ft];
      /* Action we take depends on what this instruction is jumping to */
      switch (opcode(target_inst)) {
	/* Instructions with unconditional implicit jumps */
      case IRet:
      case IFail:
      case IFailNot:
      case IEnd: {
	code[i] = code[ft];         /* jump becomes this instruction */
	code[i + 1].i.code = INoop; /* 'no-op' for target position */
	break;
      }
      case ICommit:
      case IPartialCommit:
// TODO:
#if 0
	//int fft = finaladdr(code, ft);
	int destination = getoffset(&code[i]);
	int fft = finaltarget(code, i + destination);
	code[i] = code[ft];	      /* jump becomes this instruction */
	jumptothere(compst, i, fft);  /* but must correct its offset */
	goto redo;		      /* reoptimize its address */
#endif
      default:
	jumptothere(pkg, i, ft);
	break;
      } /* switch */
      break;
    } /* case IJmp */
    default:
      break;
    } /* switch */
  }   /* for */
  assert(opcode(inst) == IRet);
  return PEXL_OK;
}


