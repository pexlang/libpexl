/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp-writer.h   S-expression to string                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef sexp_writer_h
#define sexp_writer_h

#include "sexp.h"
#include "pstring.h"

string *write_sexp(Sexp *e);
void    print_sexp(Sexp *e);
void    print_sexp_repr(Sexp *e);

string *write_bytestring(string *str, size_t len);
string *write_codepoint(uint32_t cp);
string *write_int64(int64_t val);

string *write_token_repr(Token tok);

#endif
