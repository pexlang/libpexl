/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  analyze.c   Analyze expressions and patterns                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Jack Deucher                                 */



/*
   These functions operate on the pattern->fixed field, which is an
   pexl_Expr *that has no open calls.  I.e. all the call targets have
   been fixed.
*/

#include "analyzenew.h"
#include "print.h"		/* print_charset */

typedef struct hascapturesContext {
  int result;
  int checkCalls;
} hascapturesContext;

static transformerReturn hasCapturesFunction (ast_expression * exp, void * context){
  ast_type type = exp->type;
  hascapturesContext * capcontext = (hascapturesContext*) context;
  switch (type)
  {
    case AST_CAPTURE:
      capcontext->result = YES;
      return TRet(NULL, AST_XFORM_STOP);
//     case AST_CALL:
//       if(capcontext->checkCalls) {
//         // TEMP!
//         warn("analyze", "Not handling calls currently");
//         capcontext->result = YES;
//         return TRet(NULL, AST_XFORM_STOP);
//       }
//       else {
//         capcontext->result = NO;
//         return TRet(NULL, AST_XFORM_NOREPLACE);
//       }
    default:
      capcontext->result = NO;
      return TRet(NULL, AST_XFORM_NOREPLACE);
  }
}

static int hascaptures(ast_expression *exp, int check_calls) {
  int result;
  if (!exp) {
    warn("hascaptures", "null expressions arg");
    return PEXL_ERR_NULL;
  }
  hascapturesContext *context = malloc(sizeof(hascapturesContext));
  context->checkCalls = check_calls;
  transform_AST(exp, hasCapturesFunction, context);
  result = context->result;
  free(context);
  return result;
}

// int exp_hascaptures_new (ast_expression *exp) {
//   return hascaptures(exp, YES);
// }
int exp_hascaptures_new (ast_expression *exp) {
  if (!exp) {
    warn("hascaptures", "null expressions arg");
    return PEXL_ERR_NULL;
  }
  if (exp->type == AST_CAPTURE) return YES;
  // Walk the AST, checking each child
  ast_expression *child = NULL;
  while ((child = ast_child_iter(exp, child))) {
    if (child->type == AST_CAPTURE) return YES;
  }
  return NO;
}

int exp_itself_hascaptures_new (ast_expression *exp) {
  return hascaptures(exp, NO);
}

/*
   ADD_LEN clamps the value of value+delta, and if the value was
   clamped, it sets a flag indicating this happened.
*/
static void ADD_LEN (uint32_t *value, uint32_t delta, int *result) {
  if (((int64_t)(PATTERN_MAXLEN - *value)) >= (int64_t)delta) {
    *value += delta;
    return;
  }
  *value = PATTERN_MAXLEN;
  if (result) *result = EXP_UNBOUNDED;
  return;
}

typedef struct patlenContext {
  int result;
  uint32_t min;
  uint32_t max;
} patlenContext;

static patlenContext *new_patlenContext (void) {
  patlenContext *c = malloc(sizeof(patlenContext));
  c->min = 0;
  c->max = 0;
  c->result = EXP_BOUNDED;
  return c;
}

/*
  Return min and max, where the pattern 'exp' must consume at least
  min bytes to succeed, and will consume at most max bytes.  (This
  means that predicates have min = max = 0 because they consume
  nothing.)

  A nullable pattern has min = 0.
  A fixed-length pattern has min = max.

  When 'exp' is unbounded, result is EXP_UNBOUNDED:
     The returned 'max' value is meaningless.  Do not rely on it.
  When 'exp' is bounded, result is EXP_BOUNDED:
     Both min and max are accurate.
  Returns < 0 for error.
*/

// Note that the args will be evaluated twice!
#define MIN(a,b) ((a)<(b) ? (a) : (b))
#define MAX(a,b) ((a)>(b) ? (a) : (b))

static ast_expression *patlen(ast_expression *exp, void *context);

static transformerReturn patlenFunction (ast_expression *exp, void *context) {
  int result;
  uint32_t min, max;
  patlenContext *ctx = (patlenContext *)context;
  patlenContext *ctx1, *ctx2;

 tailcall:
  switch (exp->type) {
    case AST_TRUE:
    case AST_FALSE:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD: 
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
      return TRet(NULL, AST_XFORM_STOP);

    case AST_BYTE:
    case AST_ANYBYTE:
    case AST_BYTESET:
    case AST_BYTERANGE:
      ADD_LEN(&(ctx->min), 1, &(ctx->result));
      ADD_LEN(&(ctx->max), 1, &(ctx->result));
      return TRet(NULL, AST_XFORM_STOP);

    case AST_BYTESTRING:
      ADD_LEN(&(ctx->min), String_len(exp->bytes), &(ctx->result));
      ADD_LEN(&(ctx->max), String_len(exp->bytes), &(ctx->result));
      return TRet(NULL, AST_XFORM_STOP);

    case AST_SEQ:
      if (patlen(exp->child, ctx) == NULL)
	return TRet(NULL, AST_XFORM_ERROR);
      if (patlen(exp->child2, ctx) == NULL)
	return TRet(NULL, AST_XFORM_ERROR);
      return TRet(NULL, AST_XFORM_STOP);

    case AST_CHOICE:
      // If either option is UNBOUNDED, then result is UNBOUNDED
      ctx1 = new_patlenContext();
      if (patlen(exp->child, ctx1) == NULL)
	return TRet(NULL, AST_XFORM_ERROR);
      ctx2 = new_patlenContext();
      if (patlen(exp->child2, ctx2) == NULL)
	return TRet(NULL, AST_XFORM_ERROR);
      ADD_LEN(&(ctx->min), MIN(ctx1->min, ctx2->min), &(ctx->result));
      ADD_LEN(&(ctx->max), MAX(ctx1->max, ctx2->max), &(ctx->result));
      free(ctx1); free(ctx2);
      return TRet(NULL, AST_XFORM_STOP);

    case AST_CAPTURE:
      exp = exp->child;
      goto tailcall;

    case AST_REPETITION: 
      result = exp_patlen_new(exp->child, &min, &max);
      if (result < 0) return TRet(NULL, AST_XFORM_ERROR);
      if ((result == EXP_BOUNDED) && (exp->max > 0)) {
	ADD_LEN(&(ctx->max), exp->max * max, NULL);
	ctx->result = EXP_BOUNDED;
      } else {
	ctx->max = 0;
	ctx->result = EXP_UNBOUNDED;
      }
      // 'ctx->min' contains results from prior parts of the
      // pattern, before the repetition was encountered.  If
      // exp->min is not zero, we should update ctx->min.
      if (exp->min > 0) {
	if ((result != EXP_UNBOUNDED) && (min > 0))
	  ADD_LEN(&(ctx->min), exp->min * min, NULL);
      }
      return TRet(NULL, AST_XFORM_STOP);	

    default:
      warn("analyze", "Unhandled expression type: %s (%d)",
	   ast_type_name(exp->type), exp->type);
      // Suspicious-looking values can aid debugging
      ctx->min = 123456;
      ctx->result = EXP_UNBOUNDED;
      return TRet(NULL, AST_XFORM_ERROR);
  }
}

static ast_expression *patlen(ast_expression *exp, void *context) {
  return transform_AST(exp, patlenFunction, context);
}

/* Returns EXP_BOUNDED, EXP_UNBOUNDED, or error < 0 */
int exp_patlen_new (ast_expression *exp, uint32_t *min, uint32_t *max) {
  patlenContext *context = new_patlenContext();
  if (patlen(exp, context) == NULL) {
    free(context);
    return PEXL_ERR_INTERNAL;
  }
  *min = context->min;
  *max = context->max;
  int result = context->result;
  free(context);
  return result;       // EXP_BOUNDED or EXP_UNBOUNDED
}

/*
  The nullable and nofail tests are conservative in the following way:

    p is nullable => nullable(p)
    nofail(p) => p cannot fail

  Note that nofail(p) => nullable(p).
  These tests follow the lpeg approach from Roberto Ierusalimschy.
  
  Nullable and nofail return true, false, or error (a value < 0).
*/

//-------------------------------Nofail's Set------------------------------------

// Needed to declare here so that I could use this function in the transformerFunction below.
static ast_expression *nofail (ast_expression *exp, void *ctx);

static transformerReturn nofailFunction (ast_expression *exp, void *context) {
  bool *result = context;
 tailcall:
  switch (exp->type) {
    case AST_FALSE:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD: 
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
    case AST_BYTE:
    case AST_ANYBYTE:
    case AST_BYTESET:
    case AST_BYTERANGE:
      *result = false;          // can fail
      return TRet(NULL, AST_XFORM_STOP);
    case AST_BYTESTRING:
      // Only the empty string is nofail
      *result = (String_len(exp->bytes) == 0);
      return TRet(NULL, AST_XFORM_STOP);
    case AST_TRUE:
      *result = true;           // nofail
      return TRet(exp, AST_XFORM_STOP);
    case AST_SEQ:
    case AST_CHOICE:
      // A sequence or choice is nofail if both children are nofail
      nofail(exp->child, context);
      if (!*result) return TRet(NULL, AST_XFORM_STOP);
      exp = exp->child2;
      goto tailcall;
    case AST_CAPTURE:
      exp = exp->child;
      goto tailcall;
    case AST_REPETITION:
      *result = (exp->min == 0) || exp_nullable_new(exp->child);
      return TRet(NULL, AST_XFORM_STOP);
    default:
      warn("analyze", "Unhandled expression type (in nofail): %s (%d)",
	   ast_type_name(exp->type), exp->type);
      return TRet(exp, AST_XFORM_ERROR);
  }
}

// Define the actual functionality after nofailFunction so that I can pass it as param, probably a cleaner way to do this. 
// But this function needs the nofailFunction and nofailFunction needs this function. 
static ast_expression *nofail (ast_expression *exp, void *ctx) {
  return transform_AST(exp, nofailFunction, ctx);
}

// Nofail means the pattern never fails, even at end of input.  Note
// that no predicate is nofail because a predicate is assumed to be
// able to fail.  (Else it should have been converted into True or
// False by the compiler.)
int exp_nofail_new (ast_expression *exp) {
  if (!exp) {
    warn("analyze", "null expression arg");
    return PEXL_ERR_NULL;
  }
  bool result = false;
  nofail(exp, &result);
  return result;
}


//-------------------------------Nullable's Set------------------------------------

static ast_expression *nullable (ast_expression *exp, void *ctx);

// Nullable means it can match the empty string.  Note that every
// predicate is nullable because a predicate consumes no input.
static transformerReturn nullableFunction (ast_expression *exp, void *context) {
  bool *result = context;
 tailcall:
  switch (exp->type) {
    case AST_TRUE:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD: 
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
      *result = true;
      return TRet(NULL, AST_XFORM_STOP);
    case AST_FALSE:
    case AST_BYTE:
    case AST_ANYBYTE:
    case AST_BYTESET:		// ASSUMES NON-EMPTY SET
    case AST_BYTERANGE:		// ASSUMES NON-EMPTY RANGE
    case AST_BYTESTRING:	// ASSUMES NON-EMPTY STRING
      *result = false;
      return TRet(NULL, AST_XFORM_STOP);
    case AST_SEQ:
      // A sequence is nullable if both children are nullable
      nullable(exp->child, context);
      if (!*result) return TRet(NULL, AST_XFORM_STOP);
      exp = exp->child2;
      goto tailcall;
    case AST_CHOICE:
      // A choice is nullable if either child is nullable
      nullable(exp->child, context);
      if (*result) return TRet(NULL, AST_XFORM_STOP);
      exp = exp->child2;
      goto tailcall;
    case AST_CAPTURE:
      exp = exp->child;
      goto tailcall;
    case AST_REPETITION:
      if (exp->min > 0) {
	nullable(exp->child, context);
	return TRet(NULL, AST_XFORM_STOP);
      } else {
	assert(exp->min == 0);
	*result = true;
	return TRet(NULL, AST_XFORM_STOP);
      }
    default:
      warn("analyze", "Unhandled expression type (in nullable): %s (%d)",
	   ast_type_name(exp->type), exp->type);
      return TRet(exp, AST_XFORM_ERROR);
  }
}

static ast_expression *nullable (ast_expression *exp, void *ctx) {
  return transform_AST(exp, nullableFunction, ctx);
}

int exp_nullable_new (ast_expression *exp) {
  if (!exp) {
    warn("analyze", "null expression arg");
    return PEXL_ERR_NULL;
  }
  bool result = false;
  nullable(exp, &result);
  return result;
}

//-------------------------------NodeCount's Set------------------------------------

static ast_expression *nodecount (ast_expression *exp, void *ctx);

static transformerReturn nodecountFunction (ast_expression *exp, void *context) {
  ast_explist *ls;
  uint32_t *result = context;
  (*result)++;
  switch (exp->type) {
    case AST_SEQ:
    case AST_CHOICE:
      nodecount(exp->child, context);
      nodecount(exp->child2, context);
      break;
    case AST_MODULE:
      ls = exp->defns;
      for (; ls; ls = ls->tail) nodecount(ls->head, context);
      break;
    default:
      if (exp->child) nodecount(exp->child, context);
  }
  return TRet(NULL, AST_XFORM_STOP);
}

static ast_expression *nodecount (ast_expression *exp, void *ctx) {
  return transform_AST(exp, nodecountFunction, ctx);
}

uint32_t exp_nodecount (ast_expression *exp) {
  if (!exp) {
    warn("analyze", "null expression arg");
    return PEXL_ERR_NULL;
  }
  uint32_t ctx = 0;
  nodecount(exp, &ctx);
  return ctx;
}

#if 0
// TODO: Use this to detect byte sets, ranges, and strings that should
// be converted to True.
static transformerReturn byteElisionFunction (ast_expression *exp, void *context) {
  UNUSED(context);
  ast_type type = exp->type;
  switch (type) {
    case AST_TRUE:
    case AST_FALSE:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD: 
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
    case AST_BYTE:
    case AST_ANYBYTE:
      return TRet(NULL, AST_XFORM_NOREPLACE);
    case AST_BYTESET:
      if (byteset_empty(exp->byteset))
        return TRet(make_true(), AST_XFORM_REPLACE);
      return TRet(NULL, AST_XFORM_NOREPLACE);
    case AST_BYTERANGE:
      if (byterange_empty(exp->byterange))
        return TRet(make_true(), AST_XFORM_REPLACE);
      return TRet(NULL, AST_XFORM_NOREPLACE);
    case AST_BYTESTRING:
      if (bytestring_empty(exp->bytes))
        return TRet(make_true(), AST_XFORM_REPLACE);
      return TRet(NULL, AST_XFORM_NOREPLACE);
    case AST_SEQUENCE:
      // call on exp->exp
      // and exp->cdr
      // TEMP:
      return TRet(NULL, AST_XFORM_ERROR);
    case AST_CHOICE:
      // call on exp->exp
      // and exp->cdr
      // TEMP:
      return TRet(NULL, AST_XFORM_ERROR);
    default:
      warn("analyze", "Unhandled expression type: %s (%d)", ast_type_name(type), type);
      return TRet(exp, AST_XFORM_ERROR);
  }
}
#endif
//-------------------------------Headfail's Set------------------------------------

/*
 * If 'headfail(node)' true, then 'node' can fail ONLY depending on
 * the next character of the subject.  Note that a multi-byte string
 * cannot be headfail, because it may fail on a byte AFTER the first
 * one.
 *
 * Returns 0, 1, or error < 0.
 */

int exp_headfail_new(ast_expression *exp) {
  if (!exp) {
    warn("headfail", "null expression arg");
    return PEXL_ERR_NULL;
  }
  int stat;
 tailcall:
  switch (exp->type) {
    case AST_BYTESET:
    case AST_FALSE:
    case AST_BYTE:
    case AST_ANYBYTE:
      return YES;

    case AST_BYTESTRING:
      return (String_len(exp->bytes) == 1);

    case AST_TRUE:
    case AST_FIND:
      return NO;

    case AST_REPETITION:
      // TODO:
      // A repetition is headfail if there's a non-zero minimum and the
      // target expression is headfail.
      warn("headfail", "AST_REPETITION not implemented (waiting for lowering stage impl)");
      return NO;
      
    case AST_SEQ:
      /*
	A seq is headfail only when its first child is headfail and
	the second child is nofail, because even if the first child
	succeeds based on the next char, the seq itself can still fail
	if the second child fails.
      */
      stat = exp_nofail_new(exp->child2);
      if (stat != YES) return stat; // Error case and NO case
      // Return value is now just headfail(first_child(node)); 
      exp = exp->child;
      goto tailcall;

    case AST_CHOICE:
      // A choice is headfail only if both children are headfail
      stat = exp_headfail_new(exp->child);
      if (stat != YES) return stat; // Error case and NO case
      exp = exp->child2;
      goto tailcall;

    case AST_CAPTURE:
      exp = exp->child;
      goto tailcall;

    case AST_LOOKAHEAD:
      exp = exp->child;
      goto tailcall;
    case AST_NEGLOOKAHEAD:
    case AST_LOOKBEHIND: 
    case AST_NEGLOOKBEHIND: 
      return NO;
      
    default:
      warn("exp_headfail", "unexpected expression type %s (%d)",
	   ast_type_name(exp->type), exp->type);
      return PEXL_ERR_INTERNAL;
  }
}

//-------------------------------------------------------------------

/*
  If 'exp' can be coerced to a character set (byte set, technically),
  do that and return true else return false.
*/
static bool to_charset_new (ast_expression *exp, Charset *cs) {
  if (!exp || !cs) {
    warn("to_charset", "null expression or charset arg");
    return false;
  }
  switch (exp->type) {
    case AST_ANYBYTE: 
      charset_fill_one(cs->cs);
      return true;
    case AST_BYTE:
      charset_fill_zero(cs->cs);
      setchar(cs->cs, exp->codepoint);
      return true;
    case AST_BYTESET:
      charset_copy(&(cs->cs[0]), exp->byteset);
      return true;
    case AST_BYTESTRING:
      if (!exp->bytes) {
	warn("to_charset", "null bytestring field in expression");
	return false;
      }
      if (String_len(exp->bytes) == 1) {
	charset_fill_zero(cs->cs);
	setchar(cs->cs, *String_ptr(exp->bytes));
	return true;
      } 
      return false;
    case AST_CHAR: 
    case AST_SET: 
      warn("to_charset", "codepoints should have been lowered to bytes already");
      return false;
    default:
      return false;
  }
}

/*
  [lpeg] Computes the 'first set' of a pattern. The result is a
  conservative aproximation:

      match p ax -> x (for some x) ==> 'a' \in first(p)
  -OR-
     'a' \notin first(p) ==> match p ax -> fail (for all x)

  The set 'follow' is the first set of what follows the
  pattern (full set if nothing follows it).

  The 'getfirst' function returns YES (1) when the first set can be
  used in 'test' instructions that avoid the pattern altogether.

  A NO (0) return will happen when the pattern can match the empty
  string, because in that case, 'test' instructions must not be used.
  (They fail on an empty input.)

  A return value < 0 indicates an error.
*/
int exp_getfirst_new (ast_expression *exp, const Charset *follow, Charset *firstset) {
  if (!exp || !follow || !firstset) {
    warn("exp_getfirst", "Required arg is null");
    return PEXL_ERR_NULL;
  }
  int e1, e2, stat;
  Charset csaux;
  
tailcall:
  switch (exp->type) {

    case AST_FIND:
      charset_fill_one(firstset->cs);
      return NO;

    case AST_BYTE:
    case AST_BYTERANGE:
    case AST_ANYBYTE:
      to_charset_new(exp, firstset);
      return YES;

    case AST_BYTESET:
      // A set containing only one byte should have been converted to
      // AST_BYTE before we get here
      to_charset_new(exp, firstset);
      return YES;

    case AST_BYTESTRING:
      assert(exp->bytes);
      // An empty bytestring is the same as TRUE, it's a no-op.
      // Should have been converted into TRUE before we get here.
      assert(String_len(exp->bytes) > 0);
      charset_fill_zero(firstset->cs);
      setchar(firstset->cs, *String_ptr(exp->bytes));
      return YES;

    case AST_TRUE:
      // True is a no-op, so the first set is the current follow set
      charset_copy(firstset->cs, follow->cs);
      return NO;

    case AST_FALSE:
      charset_fill_zero(firstset->cs);
      // A TestAny instruction can be used to check for end-of-input
      return YES;

    case AST_CHOICE:
      e1 = exp_getfirst_new(exp->child, follow, firstset);
      if (e1 < 0) return e1;
      e2 = exp_getfirst_new(exp->child2, follow, &csaux);
      if (e2 < 0) return e2;
      charset_or_eq(firstset->cs, csaux.cs);
      return e1 || e2;

    case AST_SEQ:
      // If the first child is non-nullable, the answer is the first
      // set of the first child, because the second is irrelevant.
      if (!exp_nullable_new(exp->child)) {
	exp = exp->child;
	goto tailcall;
      }
      e2 = exp_getfirst_new(exp->child2, follow, &csaux);
      if (e2 < 0) return e2;
      e1 = exp_getfirst_new(exp->child, &csaux, firstset);
      if (e1 < 0) return e1;
      if (e1) return YES; // 'e1' ensures that first can be used as a test
      return e2;	  // else the status depends on 'e2'

    case AST_REPETITION:
      e1 = exp_getfirst_new(exp->child, follow, firstset);
      if (e1 < 0) return e1;
      if (exp->min == 0) {
	// exp accepts the empty string
	charset_or_eq(firstset->cs, follow->cs);
	return NO;
      }
      assert(exp->min > 0);
      stat = exp_nullable_new(exp->child);
      if (stat < 0) return stat;
      if (stat == YES) {
	// exp->child accepts the empty string
	charset_or_eq(firstset->cs, follow->cs);
	return NO;
      }
      // !nullable(exp) ==> first(exp) == first(exp->child)
      exp = exp->child;
      goto tailcall;

    case AST_CAPTURE:
      exp = exp->child;
      goto tailcall;

    case AST_LOOKAHEAD:
      e1 = exp_getfirst_new(exp->child, follow, firstset);
      if (e1 < 0) return e1;
      charset_and_eq(firstset->cs, follow->cs);
      return e1;
    case AST_NEGLOOKAHEAD:
      if (to_charset_new(exp->child, firstset)) {
	cs_complement(firstset);
	return NO;
      }
      e1 = exp_getfirst_new(exp->child, follow, firstset);
      if (e1 < 0) return e1;
      charset_copy(firstset->cs, follow->cs);
      // Can always accept the empty string
      return NO;

    case AST_LOOKBEHIND: 
    case AST_NEGLOOKBEHIND: 
      // A look-behind instruction gives no new "first set" information.
      charset_copy(firstset->cs, follow->cs);
      return NO;

    case AST_CHAR:
    case AST_ANYCHAR:
    case AST_SET:
    case AST_RANGE:
      warn("getfirst", "Codepoints should have been lowered to bytes already");
      return PEXL_ERR_INTERNAL;

    case AST_IDENTIFIER:
    case AST_NUMBER:
    case AST_DEFINITION:
    case AST_MODULE:
      warn("getfirst", "Expression of type %s does not have a 'first set'",
	   ast_type_name(exp->type));
      return PEXL_ERR_INTERNAL;

    default:
      warn("analyze", "Unhandled expression type (in getfirst): %s (%d)",
	   ast_type_name(exp->type), exp->type);
      return PEXL_ERR_INTERNAL;
  }
}

static ast_expression *needfollow (ast_expression *exp, void *ctx);

// AST_CHOICE, AST_STAR, AST_CAPTURE, AST_SEQ

static transformerReturn needfollowFunction (ast_expression *exp, void *context) {
  uint32_t *result = context;
 tailcall:
  switch (exp->type) {
    // Returning NO is a conservative estimate for some of these AST
    // types, e.g. AST_CALL and some forms of AST_LOOKBEHIND.
    //
    // FUTURE: Determine if follow set could be useful for more AST types.
    case AST_TRUE:
    case AST_FALSE:
    case AST_ANYBYTE:
    case AST_BYTE:
    case AST_BYTESET:
    case AST_BYTERANGE:
    case AST_BYTESTRING:
    case AST_ANYCHAR:
    case AST_CHAR:
    case AST_SET:
    case AST_RANGE:
    case AST_BACKREF:
    case AST_FIND:
    case AST_LOOKAHEAD:
    case AST_NEGLOOKAHEAD: 
    case AST_LOOKBEHIND:
    case AST_NEGLOOKBEHIND:
      (*result) = NO;
      return TRet(NULL, AST_XFORM_STOP);
    case AST_CAPTURE:
      exp = exp->child;
      goto tailcall;
    case AST_SEQ:
      exp = exp->child2;
      goto tailcall;
    case AST_CHOICE:
      (*result) = YES;
      return TRet(NULL, AST_XFORM_STOP);
    case AST_IDENTIFIER:
      warn("exp_needfollow", "Expression (%d) should have been lowered by now", exp->type);
      (*result) = PEXL_ERR_INTERNAL;
      return TRet(exp, AST_XFORM_STOP);
    default:
      warn("exp_needfollow", "Unexpected expression type %s (%d)",
	   ast_type_name(exp->type), exp->type);
      (*result) = PEXL_ERR_INTERNAL;
      return TRet(exp, AST_XFORM_STOP);
  }
}

static ast_expression *needfollow (ast_expression *exp, void *ctx) {
  return transform_AST(exp, needfollowFunction, ctx);
}

/*
  Code generation for certain AST node types can benefit from a follow
  set calculation.  An 'exp' argument like that should return YES, and
  all others 'NO', so we can avoid computing the follow set when it
  won't be used.
*/
int exp_needfollow_new (ast_expression *exp) {
  if (!exp) {
    warn("exp_needfollow", "Required arg is null");
    return PEXL_ERR_NULL;
  }
  uint32_t ctx = 0;
  needfollow(exp, &ctx);
  return ctx;
}

// /* Return YES if target is in the SCC [start, finish) */
// static int in_SCC (CompState *cs, Pattern *target, pexl_Index start, pexl_Index finish) {
//   pexl_Index i;
//   for (i = start; i < finish; i++)
//     if (cs->g->deplists[cs->g->order->elements[i].vnum][0] == target)
//       return YES;
//   return NO;
// }

// /*
//   A leftcall is the first call in a sequence, where all the preceding elements
//   of the sequence are nullable.  A choice may have two left calls, one for
//   each child.
// */
// static int follow_leftcalls (CompState *cs, pexl_Index start, pexl_Index finish,
// 			     Node *node, int *nullable, pexl_Index *npassed) {
//   int stat, child_nullable;
//   Pattern *target;
//   pexl_Index nrules = finish - start;
//   assert(nrules > 0);
//   assert(cs);
//   assert(npassed);
//   assert(nullable);
// tailcall:
//   assert(node);
//   switch (node->tag)
//   {
//   case TBytes:
//   case TSet:
//   case TAny:
//   case TFalse:
//     /* Answer is in *nullable already */
//     return OK;
//   case TTrue:
//     *nullable = YES;
//     return OK;
//   case TBehind:
//   case TNot:
//   case TAhead:
//   case TRep:
//   case TCapture:
//     /* Return follow_leftcalls(child1(node), YES); */
//     *nullable = YES;
//     node = child1(node);
//     goto tailcall;
//   case TSeq:
//     /*
//        If first child has a left call, follow it.  Else, if first
//        child is nullable, then check second child.
//     */
//     child_nullable = 0;
//     stat = follow_leftcalls(cs, start, finish, child1(node), &child_nullable, npassed);
//     if (stat < 0)
//       return stat;
//     if (!child_nullable)
//     {
//       *nullable = NO;
//       return OK;
//     }
//     /*
//        Else first child is nullable, so 'node' is nullable IFF child2 is.
//        Return leftcall(child2(node), nullable);
//     */
//     node = child2(node);
//     goto tailcall;
//   case TChoice:
//     /*
//        Must always check both children.  Either child1 or child2 may
//        have a left call, and we need to follow both of them.
//     */
//     stat = follow_leftcalls(cs, start, finish, child1(node), nullable, npassed);
//     if (stat < 0)
//       return stat;
//     /*
//        Note: *nullable now indicates whether child1 is nullable.
//        Return follow_leftcalls(child2(node), nullable);
//     */
//     node = child2(node);
//     goto tailcall;
//   case TCall:
//     target = node->a.pat;
//     if (!target) {
//       warn("follow_leftcalls", "call target is null");
//       return PEXL_ERR_INTERNAL;
//     }
//     if (target->env < 0) {
//       warn("follow_leftcalls", "call target has invalid environment");
//       return PEXL_ERR_INTERNAL;
//     }
//     /*
//        If target of call is not in this package, it must be compiled
//        already.  Use its nullable flag.  (We could add a separate test
//        for this, even though the SCC test subsumes it, because a
//        package identity test is fast -- just a pointer comparison.)

//        Else the target is in this package but not in the SCC.  The
//        topological sort ensures that this call target will have been
//        compiled before this SCC, so we can use its nullable flag.
//     */
//     if (!in_SCC(cs, target, start, finish)) {
//       if (!pattern_has_flag(target, PATTERN_READY))
//         return PEXL_EXP__ERR_NOTCOMPILED;
//       *nullable = pattern_has_flag(target, PATTERN_NULLABLE);
//       return OK;
//     }
//     /* Else target is within this SCC, so count it and follow it. */
//     (*npassed)++;
//     /* But first check that we are not going in circles. */
//     if (*npassed >= nrules)
//       return PEXL_EXP__ERR_LEFT_RECURSIVE;
//     node = target->fixed->node;
//     goto tailcall;
//   case TOpenCall:
//     warn("follow_leftcalls", "encountered open call");
//     return PEXL_EXP__ERR_OPENFAIL;
//   case TFind:
//     return NO;
//   default:
//     warn("follow_leftcalls", "unexpected node type: %d", node->tag);
//     return PEXL_ERR_INTERNAL;
//   }
// }

// static int exp_nullable_rec (CompState *cs, Pattern *p, pexl_Index start, pexl_Index finish) {
//   int stat, nullable;
//   pexl_Index npassed;
//   npassed = 0;
//   nullable = NO;
//   /* Follows all left calls.  Sets nullable and updates the npassed count. */
//   stat = follow_leftcalls(cs, start, finish, p->fixed->node, &nullable, &npassed);
//   if (stat < 0) return stat; /* Could be any error, including LEFT_RECURSIVE */
//   return nullable;
// }

