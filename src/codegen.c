/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  codegen.c   PEXL compiler back-end                                        */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "codegen.h"

#include "instruction.h"
#include "old-analyze.h"
#include "find.h"
#include "print.h"		/* for debugging/logging output */

#include <alloca.h>

/* NOINST must be < 0 but higher than all other ERR codes. */
#define NOINST -1

typedef struct CodegenState {
  // Context from earlier compilation phases
  pexl_Context *context;
  // The set of optimizations we are applying, which can be null (none):
  pexl_Optims *optims;
  // This is the package we are building here in codegen:
  pexl_Binary *pkg;
  // Current index into the sorted list of patterns to compile:
  pexl_Index current_order_index;
} CodegenState;

/*
  Notes on compiling with vector (SIMD) instructions:

  (1) On a CPU that does not support the instructions we use, we avoid
      any reference to vector types like __m128i and intrinsic
      functions like _mm_cmpeq_epi8().  If PEXL_USING_SIMD has a true
      (non-zero) value, then we can use those.

  (2) PEXL_USING_SIMD will be zero if this code (libpexl) was compiled
      with PEXL_NO_SIMD defined.  This allows someone to build libpexl
      without SIMD support, regardless of whether the CPU supports it.

  (3) There is a list of user-requested optimizations provided in the
      pexl_Context structure when compiling patterns or running the
      VM.  If the user did not ask for SIMD, then we do not compile
      with it.  This is what simd() checks.  It does no harm to
      compile for SIMD and then not use it later in the VM, however.
      We may remove the simd() check for that reason.

*/
#if PEXL_USING_SIMD
static bool simd (CodegenState *CS) {
  return optimlist_contains(CS->optims, PEXL_OPT_SIMD);
}
#endif

static int symtab_err_to_codegen_err (int symidx)
{
  switch (symidx)
  {
  case PEXL_ERR_OOM:
    return PEXL_ERR_OOM;
  case PEXL_ERR_FULL:
    return PEXL_CODEGEN__ERR_SYMTAB_FULL;
  case PEXL_ST__ERR_BLOCKFULL:
    return PEXL_CODEGEN__ERR_SYMTAB_BLOCK_FULL;
  default:
    warn("codegen", "symboltable_add returned unexpected code %d", symidx);
    return PEXL_ERR_INTERNAL;
  }
}

static const string *bound_name (CodegenState *CS, Pattern *pat)
{
  Binding *b;
  pexl_Index namehandle;
  size_t len;
  b = env_get_binding(CS->context->bt, pat->binding_index);
  namehandle = b ? b->namehandle : -1;
  return context_retrieve(CS->context, namehandle, &len);
}

/* ----------------------------------------------------------------------------- */
/* Charset table                                                                 */
/* ----------------------------------------------------------------------------- */

/* Note: n is the return value of charsettable_add() */
static int charsettable_error (pexl_Index n) {
  if (n < 0) {
    switch (n) {
    case CHARSETTABLE_ERR_OOM:
      warn("codegen", "out of memory");
      return PEXL_ERR_OOM;
    case CHARSETTABLE_ERR_FULL:
      warn("codegen", "out of space in charset table");
      return PEXL_ERR_OOM;
    default:
      warn("codegen", "internal error %d from charsettable", n);
      return PEXL_ERR_INTERNAL;
    }
  }
  return OK;
}

static int add_charset(CodegenState *CS, Charset *new) {
  assert(CS && new);
  int err;
  Charset *existing;
  CharsetTable *cst = CS->pkg->cstab;
  pexl_Index n = PEXL_ITER_START;
  while ((existing = charsettable_iter(cst, &n)))
    if (cs_equal(existing, new)) break;
  assert(existing || (n == cst->next));
  // Did we find charset 'new' in the table already?
  if (existing) return n;
  // Else need to add it
  n = charsettable_add(cst);
  if ((err = charsettable_error(n))) return err;
  assert(n >= 0);
  if (charsettable_set_charset(cst, n, new)) {
    warn("codegen", "Failed to set charset at table index %d", n);
    return PEXL_ERR_INTERNAL;
  }
  return n;
}

/* Note: n is the return value of charsettable_add() */
__attribute__((unused))
static int set_simd_charset (CodegenState *CS, pexl_Index n, Charset *cs) {
  int err;
  CharsetTable *cst = CS->pkg->cstab;
  assert(cst);
  if ((err = charsettable_error(n))) return err;
  if ((charsettable_set_simd_charset(cst, n, cs))) {
    warn("codegen", "failed to set simd charset at charset table index %d", n);
    return PEXL_ERR_INTERNAL;
  }
  return OK;
}

static void set_no_simd (CodegenState *CS) {
  charsettable_set_no_simd(CS->pkg->cstab);
}

static int nextinstruction (pexl_Binary *pkg) {
  if (ensure_binary_size(pkg, 1)) return pkg->codenext++;
  return PEXL_ERR_OOM;
}

static int addinstruction1 (pexl_Binary *pkg, Opcode op) {
  int i = nextinstruction(pkg);
  if (i < 0) return i; /* pexl_Error */
  setopcode(&getinstr(pkg, i), op);
  trace("codegen", "Adding instruction %d, opcode %s", i, OPCODE_NAME(op));
  return i;
}

static int addinstruction (pexl_Binary *pkg, Opcode op) {
  int i = addinstruction1(pkg, op);
  if (i < 0) return i; /* pexl_Error */
  /* Consistency check */
  assert(opcode(&getinstr(pkg, i)) == op);
  return i;
}

/* ----------------------------------------------------------------------------- */
/* Code generation                                                               */
/* ----------------------------------------------------------------------------- */

static int gen(CodegenState *CS, Node *node, int opt, int tt, Charset *fl);

static int addinstruction_char (pexl_Binary *pkg, Opcode op, char c) {
  int i = addinstruction(pkg, op); /* instruction */
  if (i < 0)
    return i; /* pexl_Error */
  setichar(&getinstr(pkg, i), c);
  assert(ichar(&getinstr(pkg, i)) == (c & 0xFF));
  assert(sizei(&getinstr(pkg, i)) == 1);
  return i;
}

static int addinstruction_addr (pexl_Binary *pkg, Opcode op, int address) {
  int err, i = addinstruction1(pkg, op);
  if (i < 0) return i;                 /* pexl_Error */
  trace("codegen", "  Setting instruction %d address to 0x%X (%d)", i, address);
  err = nextinstruction(pkg);	/* space for address word */
  if (err < 0) return err;	/* pexl_Error */
  setaddr(&getinstr(pkg, i), address);
  setaux(&getinstr(pkg, i), 0); /* default value */
  assert(getaddr(&getinstr(pkg, i)) == address);
  assert(aux(&getinstr(pkg, i)) == 0);
  return i;
}

// TODO: The magic adjustment by 2 is also in setoffset() and
// getoffset().  Unify these so that the adjustment appears in one
// place.
static int addinstruction_offset (pexl_Binary *pkg, Opcode op, int address) {
  int addr = addinstruction_addr(pkg, op, address - 2);
  return addr;
}

static int guard_for_char (CodegenState *CS, char c, int addr) {
  Instruction *inst = &getinstr(CS->pkg, addr);
  return ((addr >= 0) &&
	  (opcode(inst) == ITestChar || opcode(inst) == IFind1) &&
	  ichar(inst) == c);
}

static void codechar (CodegenState *CS, char chr, int tt) {
  if (guard_for_char(CS, chr, tt))
    addinstruction(CS->pkg, IAny);
  else 
    addinstruction_char(CS->pkg, IChar, chr);
}

/*
  Generate a series of IChar instructions.
  The first one will be IAny if there is an appropriate test dominating it.
*/
static int codebytes (CodegenState *CS, pexl_Index idx, int32_t len, int tt) {
  size_t sizelen;
  const string *bytes = context_retrieve(CS->context, idx, &sizelen);
  if (!bytes) {
    warn("codebytes", "byte string not found in string table at offset %d", idx);
    return PEXL_ERR_INTERNAL;
  }
  assert(((size_t)len) == sizelen);
  assert(sizelen != 0);
  codechar(CS, bytes[0], tt);
  for (int32_t i = 1; i < len; i++)
    addinstruction_char(CS->pkg, IChar, bytes[i]);
  return OK;
}

/*
  Code ICharset.
  When have a single char, emit IChar.
  When have a full charset, emit IAny.
  When have an empty charset, emit IFail.
  Also emit IAny when ICharset is dominated by an equivalent test.
*/
static int codecharset (CodegenState *CS, Charset *cs, int tt) {
  char c = 0;
  int guard_opcode;
  Charset *guard_set;
  Charset tmp;
  charset_copy(&tmp, cs);
  CharSetType type = charsettype(&tmp, &c);
  switch (type) {
  case CharSet1: {
    codechar(CS, c, tt);
    break;
  }
  case CharSet8: case CharSet: {
    /*
      FUTURE: Guard set is valid if it is a SUBSET of current set,
      not just if it is equal.  We should (1) replace cs_equal()
      with a subset test instead, and (2) check ITestChar, IFind1
      instructions as possible valid guards.
    */
    /* If there's a guard instruction, can we use it? */
    if (tt >= 0) {
      guard_opcode = opcode(&getinstr(CS->pkg, tt));
      if ((guard_opcode == ITestSet) ||
	  (guard_opcode == IFind8) ||
	  (guard_opcode == IFind)) {
	guard_set = charsettable_get_charset(CS->pkg->cstab, aux(&getinstr(CS->pkg, tt)));
	if (!guard_set) {
	  warn("codegen", "invalid charsettable index in guard at %d", tt);
	  return PEXL_ERR_INTERNAL;
	}
	trace("codegen",
	      "codecharset: tt=%d, opcode(tt)=%d, cs_equal=%d",
	      tt,
	      guard_opcode,
	      cs_equal(cs, guard_set));
	if (cs_equal(cs, guard_set)) {
	  addinstruction(CS->pkg, IAny);
	  break;
	} /* Found a valid guard */
      } /* The guard instruction has relevant opcode */
    } /* A guard instruction (address) was passed into codecharset */

    int iset = addinstruction(CS->pkg, ISet);
    pexl_Index cst_index = add_charset(CS, &tmp); // TEMP: unaligned alias for cs
    if (cst_index < 0) {
      warn("codegen", "Failed to add to charset table");
      return PEXL_ERR_INTERNAL;
    }
    /* No simd impl of ISet, so no need to build a simd bitmask */
    /* Insert the charset table index into the instruction */
    assert((cst_index & 0xFF000000) == 0); /* Ensure fits in aux field */
    setaux(&getinstr(CS->pkg, iset), cst_index);
    break;
  } /* Cases for charsets of size 8 and up */
  case CharSetEmpty: {
    addinstruction(CS->pkg, IFail);
    break;
  }
  case CharSetFull: {
    addinstruction_char(CS->pkg, IAny, c);
    break;
  }
  default:
    warn("codegen", "unexpected charsettype (%d)", type);
    return PEXL_ERR_INTERNAL;
  }
  return OK;
}

/*
  Code a test set, optimizing unit sets for ITestChar, "complete"
  sets for ITestAny, and empty sets for IJmp (always fails).
  'e' is true iff the test should accept the empty string. 
*/
static int codetestset (CodegenState *CS, Charset *cs, int e) {
  CharSetType type;
  int itestset;
  char c = 0;
  if (e) return NOINST;		/* no test */
  else {
    type = charsettype(cs, &c);
    switch (type) {
      case CharSetEmpty: {
	/* A test for an empty set of chars always fails */
	return addinstruction_offset(CS->pkg, IJmp, 0);
      }
      case CharSetFull:
	/* ITestAny is cheap: it merely tests for end of input */
	return addinstruction_offset(CS->pkg, ITestAny, 0);
      case CharSet1: {
	/* Faster to look for a single char than a set */
	itestset = addinstruction_offset(CS->pkg, ITestChar, 0);
	setichar(&getinstr(CS->pkg, itestset), c);
	return itestset;
      }
      case CharSet8: case CharSet: {
	/* No optimizations possible.  Use the ITestSet instruction. */
	itestset = addinstruction_offset(CS->pkg, ITestSet, 0);
	pexl_Index cst_index = add_charset(CS, cs);
	if (cst_index < 0) {
	  warn("codegen", "Failed to add to charset table");
	  return PEXL_ERR_INTERNAL;
	}
	/* No simd impl of ITestSet, so no need to generate bit mask */
// 	Charset cset;
// 	/* TODO: Pass around Charset instead of uint32_t[] */
// 	charset_copy(&(cset.cs), cs);
// 	charsettable_set_charset(CS->pkg->cstab, cst_index, &cset);
	/* Insert the charset table index into the instruction */
	assert((cst_index & 0xFF000000) == 0); /* Ensure fits in aux field */
	setaux(&getinstr(CS->pkg, itestset), cst_index);
	return itestset;
      }
      default:
	warn("codegen", "unexpected result from charsettype (%d)", type);
	return PEXL_ERR_INTERNAL;
    }
  }
}

/*
** Not predicate; optimizations:
** In any case, if first test fails, 'not' succeeds, so it can jump to
** the end. If pattern is headfail, that is all (it cannot fail
** in other parts); this case includes 'not' of simple sets. Otherwise,
** use the default code (a choice plus a failtwice).
*/
static int codenot (CodegenState *CS, Node *node)
{
  int test, stat, pchoice, err;
  Charset st;
  int e = exp_getfirst(node, fullset, &st);
  if (e < 0) return e; 		   // error
  addinstruction(CS->pkg, ILookAhead);
  test = codetestset(CS, &st, e);
  if (test < NOINST) return test;  // error
  stat = exp_headfail(node);
  if (stat < 0) return stat; 	   // error
  if (stat)
    /* test (fail(p1)) -> L1; fail; L1:  */
    addinstruction(CS->pkg, IFail);
  else {
    /* test(fail(p))-> L1; choice L1; <p>; failnot; L1: lookaheadcommit  */
    pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
    err = gen(CS, node, 0, NOINST, fullset);
    if (err) return err;
    addinstruction(CS->pkg, IFailNot);
    jumptohere(CS->pkg, pchoice);
  }
  jumptohere(CS->pkg, test);
  addinstruction(CS->pkg, ILookAheadCommit);
  return OK;
}

/*
** Code first child of a sequence
** (second child is called in-place to allow tail call)
** Return 'tt' for second child
*/
static int codeseq1 (CodegenState *CS, Node *p1, Node *p2,
		     int *tt, const Charset *fl) {
  uint32_t min, max;
  Charset fl1;
  int l, err;
  if (exp_needfollow(p1)) {
    err = exp_getfirst(p2, fl, &fl1); /* p1 follow is p2 first */
    if (err < 0)
      return err;
    err = gen(CS, p1, 0, *tt, &fl1);
    if (err)
      return err;
  } else {
    /* Use 'fullset' as the follow set */
    err = gen(CS, p1, 0, *tt, fullset);
    if (err) return err;
  }
  l = exp_patlen(p1, &min, &max);
  if (l < 0) return l; /* pexl_Error */
  if (min > 0) {
    /* will 'p1' always consume something? */    
    *tt = NOINST; /* invalidate test */
    return OK;
  } else {
    /* else 'tt' still guards child2 */
    return OK;
  }
}

/*
** Add a capture instruction:
** 'op' is the capture instruction; 'cap' the capture kind;
** 'idx' the key into ktable;
*/
static int addinstcap (pexl_Binary *pkg, Opcode op, int cap, int idx) {
  int i;
  i = addinstruction_addr(pkg, op, idx);
  setaux(&getinstr(pkg, i), cap);
  assert(aux(&getinstr(pkg, i)) == cap);
  return i;
}

static int codecapture (CodegenState *CS, Node *node, int tt, Charset *fl) {
  int err;
  int symidx;
  size_t len = 0;
  const string *name = context_retrieve(CS->context, node->b.n, &len);
  symidx = symboltable_add_data(CS->pkg->symtab,
				name,
				symbol_local,
				symbol_type_capname,
				len,
				0);
  if (symidx < 0)
    return symtab_err_to_codegen_err(symidx);
  addinstcap(CS->pkg, IOpenCapture, node->cap, symidx);
  if (node->cap == Cconstant) {
    assert(child1(node)->tag == TTrue);
    name = context_retrieve(CS->context, node->a.stringtab_handle, &len);
    symidx = symboltable_add_data(CS->pkg->symtab,
                                  name,
				  symbol_local,
				  symbol_type_capname,
				  len,
				  0);
    if (symidx < 0) return symtab_err_to_codegen_err(symidx);
    addinstruction_addr(CS->pkg, ICloseConstCapture, symidx);
  } else {
    err = gen(CS, child1(node), 0, tt, fl);
    if (err) return err;
    addinstruction(CS->pkg, ICloseCapture);
  }
  return OK;
}


/*
*/
static int codewhere(CodegenState *CS, Node *where) {

  confess("codewhere gen", "We are trying not ignoring path and child2");
  int err = gen(CS, child1(where), 0, NOINST, fullset);

  if (err){
    printf(" err from gen x \n");
    return err;
  }
  if (child1(where)->tag != TCapture) {
    confess("codewhere codegen", "Pattern X must be a caputre");
    err = PEXL_CODEGEN__ERR_WHERE_REQUIRES_CAPTURE_X;
    return err;
  }
  confess("codewhere gen", "Ok so X must be a capture cool ");

  // we are attempting to do the stuff inbetween mostly getting this symidx
  int symidx;
  size_t len = 0;
  const char *name = context_retrieve(CS->context, child1(where)->b.n, &len);
  symidx = symboltable_add_data(CS->pkg->symtab, name, symbol_local,
                                symbol_type_capname, len, 0);
  if (symidx < 0)
    return symtab_err_to_codegen_err(symidx);

  

  confess("codewhere gen", "hope you made it through the recapture");

  addinstruction_addr(CS->pkg, INarrowToSpan, 0);
  setaux(&getinstr(CS->pkg, 0), symidx);
  confess("codewhere gen", "Can we gen y?");
 err = gen(CS, child2(where), 0, NOINST, fullset);

  if (err){
    printf(" err from gen y \n");
    return err;
  }
  addinstruction(CS->pkg, IWiden);

 
    // pop off stack

    return OK;
  
}
// devise an outline for code it will generate
//  in where call gen for x  /done
// assert its a capture /done I think
// do code capt from code gen
//  insert cap name and get number of index into table which name
// the number becomes the arg to an inst that narrows the span by looking
// through cap stack for index of x to extract span (read back code in vm)
//  push onto call stack indicator for full span (special frame type)
//  gen for y
//  clean up including resetting span
//  find old indicator (should be top of stack )
//  reset span
//  match x then narrow input to span given by y
//  essentially rn this allows us to test only the begining of the span of x

// check seq or choice but essentially recursively call code gen on x then get
// span from y somehow vm that says given path (root for now) find node in parse
// tree and narrow input to given span

static int32_t lookup_placeholder (CodegenState *CS, Pattern *target)
{
  pexl_Index i;
  SCC_Component component;
  pexl_Index cnum;
  Pattern *p;
  // Currently compiling pattern i in the ordered elements of the cfgraph
  i = CS->current_order_index;
  // cnum is the current SCC component number
  cnum = CS->context->cst->g->order->elements[i].cnum;
  do {
    if (i >= CS->context->cst->g->next) {
      warn("lookup_placeholder",
           "reached end of graph from strongly connected component %d", cnum);
      return PEXL_ERR_INTERNAL;
    }
    component = CS->context->cst->g->order->elements[i];
    if (component.cnum != cnum) {
      warn("lookup_placeholder",
           "reached end of strongly connected component %d", cnum);
      return PEXL_ERR_INTERNAL;
    }
    p = CS->context->cst->g->deplists[component.vnum][0];
    i++;
  } while (p != target);
  // Here, i is one higher than the actual index, which is what we want,
  // because placeholders are stored negated, so we can't use zero.  Instead,
  // we use the actual index plus one, ensuring a positive value.
  trace("codegen",
	"Target pattern %p has placeholder %d (index %d in ordered elements).",
	target, i, i-1);
  WHEN_TRACING
    print_sorted_cfgraph(CS->context->cst->g);
  return i;
}

/* Generate ICall instruction for local calls (within the current package). */
static int codeICall (CodegenState *CS, Node *node)
{
  int stat;
  pexl_Index target_address;
  Pattern *target;
  assert(node);
  target = node->a.pat;
  assert(target);

  /* Examine the target pattern of the call.  If the code of the
     target has been generated already, it will have a valid
     entrypoint (!= -1).  This is the easy path: we can emit an ICall
     instruction containing that entrypoint.

     If the target has not had code generated yet, we emit an ICall
     instruction containing a placeholder value where the entrypoint
     should be.  A placeholder is < 0, so it cannot be confused with a
     valid entrypoint.

     Note that a placeholder is only needed in the following
     situation: We are generating code for a strongly connected
     component with 2 or more vertices (a set of mutually recursive
     patterns) or with 1 recursive pattern.  The reason we do not need
     placeholders in other situations is that we are generating code
     for the patterns in topological-sort order.  As a result, we
     almost always generate the code for all of a pattern's
     dependencies before we generate code for the pattern itself.

     To choose a value for the placeholder, we look in the
     topologically ordered graph structure and find the current
     strongly connected component.  The call target must be one of the
     patterns in that SCC at or after the pattern being compiled.  The
     absolute value of the placeholder is the 1-based index into the
     SCC.  I.e. the placeholder value is the actual array index plus
     1, so that we can store it negated.

     In fixupSCC(), we post-process the code generated for each
     pattern in a recursive SCC.  We scan the code for an ICall with a
     negative target address (i.e. a placeholder), which we convert
     into an index into the ordered graph.  The pattern at that index
     is the target of the call.
 */

  target = node->a.pat;
  assert(target);
  target_address = target->entrypoint;

  if (target_address < 0) {
    if (target_address == -1) {
      stat = lookup_placeholder(CS, target);
      trace("codegen",
           "In codeICall, pattern %p not yet compiled, placeholder value is %d",
           (void *)target, stat);
      if (stat <= 0) return PEXL_ERR_INTERNAL; /* already logged */
      target_address = -stat;	/* placeholder < 0 */
    } else {
      warn("codeICall", "invalid call target %" pexl_Index_FMT, target_address);
      return PEXL_ERR_INTERNAL;
    }
  }
  addinstruction_addr(CS->pkg, ICall, target_address);
  return OK;
}

static pexl_Index get_package_num (CodegenState *CS, pexl_Binary *pkg)
{
  pexl_Index index;
  if (!CS->context->packages)
  {
    warn("codeXCall", "null package table field in codegen state");
    return PEXL_ERR_INTERNAL;
  }
  index = packagetable_getnum(CS->context->packages, pkg);
  if (index < 0)
  {
    warn("codeXCall", "no entry in package table for package %p", (void *)pkg);
    return PEXL_CODEGEN__ERR_NO_PACKAGE;
  }
  return index;
}

/* Generate code for a call to a pattern in a compiled package */
static int codeXCall (CodegenState *CS, Node *node)
{
  int i;
  pexl_Binary *pkg;
  pexl_Index package_num, symbol;
  assert(node);
  set_xref_from_node(pkg, symbol, node);
  package_num = get_package_num(CS, pkg);
  if (package_num < 0) return package_num; /* error already logged */
  /* aux field (24 bits) holds index into package table */
  /* addr fields (32 bits) holds index into that package's symtab */
  i = addinstruction_addr(CS->pkg, IXCall, symbol);
  setaux(&getinstr(CS->pkg, i), package_num);
  return OK;
}

/*
** The PEG 'and' predicate, i.e. unbounded lookahead
** Optimization:
**     fixedlen(p) = n ==> <&p> == <p>; behind n
** pexlCaptures are suspended during the lookahead.
*/
/* TODO: Make these transformations at IR (tree) level:
   - If target pattern is False, compile to simply False
   - If target pattern is nofail (implies nullable), this is a no-op
*/
#define FIXEDLEN(res, min, max) ((res != EXP_UNBOUNDED) && (min == max))
static int codeahead (CodegenState *CS, Node *node, int tt) {
  int err;
  /* LookAhead; p1; LookAheadCommit */
  addinstruction(CS->pkg, ILookAhead);
  err = gen(CS, node, 0, tt, fullset);
  if (err) return err;
  addinstruction(CS->pkg, ILookAheadCommit);
  return OK;
}

/*
  A semantically equivalent expression to 'find:e' is '{!e .}* e'.  In
  other words, advance until 'e' matches (else fail if it never does).

  Contrast with the backtracking approach of 'S = e / {. S}', which is
  less efficient because it builds up a call stack with one frame for
  each byte that does not match 'e'.  (However, our TRO optimizer
  converts this tail recursive pattern into an iterative one that uses
  constant stack space.)

  The expression 'find:e' compiles into this: (excepting optimization,
  shown below)

        findloop L1           // push a BTFindLoop frame
    L1: find firstset(e)      // saves position in BTFindLoop frame
        <e>
        commit and jmp to L2  // pop the BTFindLoop frame
    L2:

  The BTFindLoop frame is has the same structure as BTChoice.  A new
  ILoopUpdate expects BTFindLoop at the top of the stack, and updates
  the position field with the current position.

  On failure, the vm unwinds the stack until it encounters a
  "stopping" frame, like BTChoice or BTFindLoop.  On seeing this
  frame, the vm restores the capture stack top, restores the input
  position while advancing it by one and checking for EOI, and jumps
  to the stored address (top of the loop).

  Optimization TODO: If e starts with a long enough sequence of bytes,
  like a literal bytestring, then we can use the (new) findstr
  instruction:
  
  The expression 'find:e', when e = b e' and b is a bytestring
  compiles into:

        findloop L1           // push a BTFindLoop frame
    L1: findstr b             // saves position in BTFindLoop frame
        <e'>
        commit and jmp to L2  // pop the BTFindLoop frame
    L2:

*/
static int codefind (CodegenState *CS, Node *node, int tt) {
  int err, pfindloop, pfind, pcommit, status;

  Charset st;
  charset_fill_zero(st.cs);
  status = exp_getfirst(node, fullset, &st);
  if (status < 0) return status; /* error */

  char c;
  int charsetsize = -1;
  CharSetType type = charsettype(&st, &c);

  // Optimization
  if (type == CharSetFull) {
    /*
      Finding (searching for) any char at all is a no-op.  The next
      char, if any, will always match.  We need only emit code to
      match the target pattern.
    */
    err = gen(CS, node, NOINST, NOINST, fullset);
    if (err < 0) return err;
    return OK;
  }

  // Optimization
  if ((type == CharSetEmpty) && (!exp_nullable(node))) {
    /*
      Searching for an empty charset is immediate fail when the
      pattern is non-nullable, i.e. when it needs at least one char to
      match.
    */
    addinstruction(CS->pkg, IFail);
    return OK;
  } 

  // What follows is the implementation of a "find loop"

  pfindloop = addinstruction_offset(CS->pkg, IFindLoop, 0);
  jumptohere(CS->pkg, pfindloop);

  switch (type) {
  case CharSetEmpty:
    /*
      If the target pattern is nullable and the firstset is empty, the
      pattern is searching for the end of the input.  If we had a vm
      instruction for that, we'd use it.  But we don't.  Instead we'll
      search for a NUL byte, because those are uncommon.  Each time we
      find one, we'll try to match the target pattern.  Eventually,
      we'll reach EOI.
    */
    c = '\0';
    /* fallthrough */
  case CharSet1:
    charsetsize = 1;
    pfind = addinstruction(CS->pkg, IFind1);
    // Insert the target char into the instruction
    setaux(&getinstr(CS->pkg, pfind), ((uint8_t) c));
    /*
      FUTURE: If tt is the address of a valid guard instruction, then
      the current input char is the char that IFind1 is supposed to
      find.  In that case, we can omit the IFind1 instruction
      altogether.  The warning below is to alert us to these use
      cases, which appear rare (if they are possible at all).  If such
      use cases turn out to be common enough or important enough, we
      should use the presence of the guard to skip the IFind1.
    */
    if (guard_for_char(CS, c, tt)) {
      warn("codegen", "Missed opportunity to optimize out IFind1 "
	   "at address %" pexl_Index_FMT, pfind);
    }
    break;
  case CharSet8:
    charsetsize = 8;
    pfind = addinstruction(CS->pkg, IFind8);
    break;
  case CharSet:
    charsetsize = 255;
    pfind = addinstruction(CS->pkg, IFind);
    break;
  default:
    warn("codegen", "unexpected result from charsettype (%d)", type);
    return PEXL_ERR_INTERNAL;
  }
  /* When we need to search for a char that is a set of several chars,
     we need to create an entry in the charset table.  We emit a Find
     instruction with an operand that is the charset table index.
     (When the set size is 1, we don't need a set.  See case CharSet1,
     above.)
  */
  if (charsetsize > 1) {
    // Always put the plain charset into the charset table
    pexl_Index cst_index = add_charset(CS, &st);
    if (cst_index < 0) {
      warn("codegen", "failed to add to charset table");
      return PEXL_ERR_INTERNAL;
    }
#if PEXL_USING_SIMD
    if (simd(CS)) {
      // Make a simd bitmask from the target character set
      SetMask_t set_mask;
      uint8_t one_char;
      int simd_set_size = charset_to_simd_mask(&st, &set_mask, &one_char);
      if (simd_set_size != charsetsize) {
	warn("codegen", "simd charset size (%d) not expected value (%d)",
	     simd_set_size, charsetsize);
	return PEXL_ERR_INTERNAL;
      }
      /* Insert the charset bitmask into the charset table */
      set_simd_charset(CS, cst_index, (Charset *)&set_mask);
    } else {
      set_no_simd(CS);
    }
#else
    /* CPU lacks the necessary capabilities to use our SIMD code */
    set_no_simd(CS);
#endif
    /* Insert the charset table index into the instruction */
    assert((cst_index & 0xFF000000) == 0); /* ensure fits in aux field */
    setaux(&getinstr(CS->pkg, pfind), cst_index);
  } // if charsetsize > 1

  // Generate the code for the target pattern
  if ((err = gen(CS, node, NOINST, pfind, fullset))) return err;

  /*
    The ICommit instruction unwinds the BT stack looking for a
    "stopping frame" like BTChoice or BTFindLoop.  For BTBackLoop, the
    vm will first restore the current input position from the
    BTBackLoop frame.  Then it will advance it and either fail (if at
    EOI) or jump to the address stored in the BTBackLoop frame.
  */
  pcommit = addinstruction_offset(CS->pkg, ICommit, 0);
  jumptohere(CS->pkg, pcommit);

  return OK;
}

/* FUTURE:

   Consider whether to allow captures in lookbehind/lookahead
   - It would break the property that the parse tree output (of a
     match) has a rope/span structure: the start value of a node is <=
     the start value of its leftmost child, and the end value of a
     node is >= the end value of its rightmost child.
   - The color output encoder relies on this property, but does
     anything else?

   TODO: Make the following transformations at IR (tree) level.
   - If target pattern is False, compile to simply False
   - If target pattern is nullable AND nofail, this is a no-op
*/

/* Lookbehind:

   The lookbehind operator finds a place before the current input
   position at which the target pattern matches, AND that match ends
   at the current input position.

   The ILookBehind instruction pushes two stack frames:

   (1) a BTLookBehind frame saves the current "fences" (positions in
   the input) so they can be restored

   (2) a BTBackLoop frame saves the iteration parameters for the
   search

   The ILookBehind instruction has these parameters:

     pos_delta = min_length(p) i.e. where to start searching
     left_fence_delta = max_length(p) i.e. where to stop searching

   The BTBackLoop frame holds the 'loop counter' which is the current
   search position.  It also holds the address of the top of the loop.

   At runtime, the IBackLoop instruction creates a stack frame that
   contains the following data:

      c (current pos) - The current "search position" where we start
      matching the target pattern, p.  If we fail to match p, we
      decrement c and try again, in a (naive) linear search leftward
      from where the lookbehind started.

      left_fence - The input position beyond which we must not search
      for pattern p.  The left_fence is either the maximum length of p
      (if it is known by the compiler) or a value indicating that p is
      unbounded, in which case the stack frame holds the pointer to
      the start of the input.  I.e. left_fence is "clamped" to Start.

      right_fence - The input position when we entered lookbehind.
      For p to succeed, it must match every character from position c
      up to (the char before) right_fence.

    INPUT STRING:                      PATTERN: "abb?" min=2, max=3
      | x x x x x x x x x x x x |
    Start   ^ |   ^            End
           LF |   RF
              |
        c (first search candidate)

   Because we are looking for the "nearest" position at which p
   matches, c starts off 'min' chars left of current position (RF
   above).  The last c we will ever check is when c = LF.

   For p to succeed, the current position after p must be RF.

  <behind(p)> ==>
         ILookBehind pos_delta, left_fence_delta
   <p>
         ILookBehindCommit

  If <p> fails, the failure code unwinds the stack of calls, finding
  an IBackLoop frame, at which point the vm decrements the saved
  search position and jumps to the start of <p> to search again.  Or,
  if the new position exceeds the left_fence, the vm pops the
  IBackLoop frame and fails.

*/
static int codebehind (CodegenState *CS, Node *node)
{
  int min, max;
  int err, info, loop;
  pexl_Binary *pkg = CS->pkg;
  /* FUTURE: Cache patlen the first time it's computed to speed up compiler. */
  info = exp_patlen(node, (uint32_t *)&min, (uint32_t *)&max);
  if (info < 0) return info;	/* pexl_Error */

  if (info == EXP_UNBOUNDED) max = 0;
  /* Range check to ensure the negated patlen values fit into 24 bits */
  assert(checksignedaux(-min));
  assert(checksignedaux(-max));

  /* TODO: If pattern is nullable AND nofail, emit no-op */

  /* TODO: Re-examine the predicates.  They are all nullable of
     course.  But should they be nofail IFF their target pattern is
     nofail?
  */

  loop = addinstruction_addr(pkg, ILookBehind, -max); /* prepsignedoffset(-max) ??? */
  setaux(&getinstr(pkg, loop), prepsignedaux(-min));
  /* code target pattern <p> */
  err = gen(CS, node, 0, NOINST, fullset);
  if (err < 0) return err;

  // Not using operand (address) field for the moment
  addinstruction_addr(pkg, ILookBehindCommit, 0);

  WHEN_TRACING { 
    fprint_instructions(stderr,
			pkg,
			&pkg->code[loop], 
			gethere(pkg) - loop); 
  } 
  
  return OK;
}

/*
 * [lpeg] Repetition optimizations:
 * (1) When pattern is a charset, can use special instruction ISpan.
 * (2) When pattern is head fail, or if it starts with characters that
 *     are disjoint from what follows the repetitions, a simple test
 *     is enough (a fail inside the repetition would backtrack to fail
 *     again in the following pattern, so there is no need for a choice).
 * (3) When 'opt' is true, the repetition can reuse the Choice already
 *     active in the stack.
 *
 * Returns 0 for OK, or error < 0
 */
static int coderep (CodegenState *CS, Node *node, int opt, Charset *fl)
{
  Charset st;
  int err, stat, e1, jmp, test, commit, l2, pchoice;
  if (to_charset(node, &st)) {
    /* Case (1) from above */
    int ispan = addinstruction(CS->pkg, ISpan);
    /* No simd impl of ISet, so only regular charset is needed */
    pexl_Index cst_index = add_charset(CS, &st);
    if (cst_index < 0) {
      warn("codegen", "Failed to add to charset table");
      return PEXL_ERR_INTERNAL;
    }
    /* Insert the charset table index into the instruction */
    assert((cst_index & 0xFF000000) == 0); /* ensure fits in aux field */
    setaux(&getinstr(CS->pkg, ispan), cst_index);
  } else {
    /* Cannot use case (1) from above */
    e1 = exp_getfirst(node, fullset, &st);
    if (e1 < 0) return e1; /* pexl_Error */
    stat = exp_headfail(node);
    if (stat < 0) return stat; /* pexl_Error */
    if (stat || (!e1 && cs_disjoint(&st, fl))) {
      /* Case (2) from above */
      /* L1: test (fail(p1)) -> L2; <p>; jmp L1; L2: */
      test = codetestset(CS, &st, 0);
      if (test < NOINST) return test;
      err = gen(CS, node, 0, test, fullset);
      if (err) return err;
      jmp = addinstruction_offset(CS->pkg, IJmp, 0);
      jumptohere(CS->pkg, test);
      jumptothere(CS->pkg, jmp, test);
    } else {
      /* test(fail(p1)) -> L2; choice L2; L1: <p>; partialcommit L1; L2:
	 -OR-
	 Case (3) from above (if 'opt'):
	 partialcommit L1; L1: <p>; partialcommit L1; */
      test = codetestset(CS, &st, e1);
      if (test < NOINST)
        return test; /* pexl_Error */
      pchoice = NOINST;
      if (opt)
        jumptohere(CS->pkg, addinstruction_offset(CS->pkg, IPartialCommit, 0));
      else
        pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
      l2 = gethere(CS->pkg);
      err = gen(CS, node, 0, NOINST, fullset);
      if (err) return err;
      commit = addinstruction_offset(CS->pkg, IPartialCommit, 0);
      setaux(&getinstr(CS->pkg, commit), 1); /* Special IPartialCommit */
      jumptothere(CS->pkg, commit, l2);
      jumptohere(CS->pkg, pchoice);
      jumptohere(CS->pkg, test);
    }
  }
  return OK;
}

/*
** Choice; optimizations:
**
** N.B. This is the ONLY procedure that can set the 'opt' parameter to true.
**
** - When p1 is headfail or when first(p1) and first(p2) are disjoint,
**   then a character not in first(p1) cannot go to p1, and a character
**   in first(p1) cannot go to p2 (as it is not in first(p2)).
**   (The optimization is not valid if p1 accepts the empty string,
**   as then there is no character at all...)
** - When p2 is empty and opt is true; a IPartialCommit can reuse
**   the Choice already active in the stack.
*/
/* Returns 0 for OK, or error < 0 */
static int codechoice (CodegenState *CS, Node *p1, Node *p2, int opt, Charset *fl)
{
  int test, jmp, headfailp1, pcommit, pchoice, err = 0;
  int emptyp2 = (p2->tag == TTrue);
  Charset cs1, cs2;
  int e1 = exp_getfirst(p1, fullset, &cs1);
  if (e1 < 0)
    return e1;
  headfailp1 = exp_headfail(p1);
  if (headfailp1 < 0) return headfailp1; /* error */

  if (headfailp1 ||
      (!e1 &&
       ((err = exp_getfirst(p2, fl, &cs2)), cs_disjoint(&cs1, &cs2)))) {
    if (err < 0) return err;
    /* <p1 / p2> == test (fail(p1)) -> L1 ; p1 ; jmp L2; L1: p2; L2: */
    test = codetestset(CS, &cs1, 0);
    if (test < NOINST) return test;
    jmp = NOINST;
    err = gen(CS, p1, 0, test, fl);
    if (err) return err;
    if (!emptyp2)
      jmp = addinstruction_offset(CS->pkg, IJmp, 0);
    jumptohere(CS->pkg, test);
    err = gen(CS, p2, opt, NOINST, fl);
    if (err) return err;
    if (!emptyp2) jumptohere(CS->pkg, jmp);
  }
  else if (opt && emptyp2) {
    /* p1? == IPartialCommit; p1 */
    jumptohere(CS->pkg, addinstruction_offset(CS->pkg, IPartialCommit, 0));
    err = gen(CS, p1, 1, NOINST, fullset);
    if (err)
      return err;
  } else {
    /* <p1 / p2> ==
        test(first(p1)) -> L1; choice L1; <p1>; commit L2; L1: <p2>; L2: */
    test = codetestset(CS, &cs1, e1);
    if (test < NOINST)
      return test;
    pchoice = addinstruction_offset(CS->pkg, IChoice, 0);
    err = gen(CS, p1, emptyp2, test, fullset);
    if (err)
      return err;
    pcommit = addinstruction_offset(CS->pkg, ICommit, 0);
    jumptohere(CS->pkg, pchoice);
    jumptohere(CS->pkg, test);
    err = gen(CS, p2, opt, NOINST, fl);
    if (err) return err;
    jumptohere(CS->pkg, pcommit);
  }
  return OK;
}

/* TODO: What conditions can or should be tested here? */
static int codebackref (CodegenState *CS, Node *node) {  
  size_t len;
  const string *name = context_retrieve(CS->context, node->a.stringtab_handle, &len);
  if (!name || (len == 0)) return PEXL_ERR_INTERNAL;
  pexl_Index idx = symboltable_add_data(CS->pkg->symtab,
					name,
					symbol_local,
					symbol_type_capname,
					len,
					0);
  if (idx < 0) {
    warn("codegen", "error adding to symbol table");
    return PEXL_CODEGEN__ERR_SYMTAB_FULL;
  }
  //SymbolTableEntry *entry = symboltable_get(CS->pkg->symtab, idx);
  //assert(entry);
  //addinstruction_addr(CS->pkg, IBackref, entry->handle);
  addinstruction_addr(CS->pkg, IBackref, idx);
  return OK;
} 

/* ----------------------------------------------------------------------------- */

// `fixup_pattern` iterates through the code for a single pattern,
// looking for ICall instructions with target addresses that are
// placeholders.  Each placeholder is replaced by the actual address
// (entrypoint) of the call target.
static void fixup_pattern (CodegenState *CS, Pattern *p) {
  pexl_Index vnum;
  pexl_Index actual;
  pexl_Index i, last;
  Pattern *target;
  pexl_Binary *pkg = CS->pkg;
  Instruction *inst, *code = pkg->code;
  DepList *deplists = CS->context->cst->g->deplists;
  SCC_Component *ordered_elements = CS->context->cst->g->order->elements;
  last = p->entrypoint + p->codesize;

  trace("codegen", "Entering fixup for pattern %p", (void *)p);

  for (i = p->entrypoint; i < last; i += sizei(&code[i])) {
    inst = &code[i];
    trace("codegen", "fixup PC = %d, opcode = %s (%d)",
         i, OPCODE_NAME(opcode(inst)), opcode(inst));
    if (opcode(inst) == ICall) {
      if (getaddr(inst) < 0) {
	// Call target index must be > 0, so we calculate it by adding
	// 1 to the actual array index, then negating it.  Here, we
	// undo that to index into ordered_elements[].
        vnum = ordered_elements[- getaddr(inst) - 1].vnum;
        target = deplists[vnum][0];
        actual = target->entrypoint;
        assert(actual >= 0);
        trace("codegen",
             "fixup ICall target is %d, fixing to vnum %d == target %p == addr %d",
             getaddr(inst), vnum, (void *)target, actual);
        setaddr(&getinstr(pkg, i), actual);
      }
    }
  } /* for each instruction generated for pattern p */
}

// `fixupSCC` iterates through all patterns in the cfgraph, which are
// all patterns being compiled, and fixes up call targets as needed.
static void fixupSCC (CodegenState *CS, pexl_Index start, pexl_Index finish)
{
  Pattern *p;
  pexl_Index i, n;
  SCC_Component element;
  DepList *deplists = CS->context->cst->g->deplists;
  SCC_Component *ordered_elements = CS->context->cst->g->order->elements;
  n = finish - start + 1;
  assert(n > 0);
  assert(start >= 0);
  for (i = 0; i < n; i++)
  {
    element = ordered_elements[start + i];
    p = deplists[element.vnum][0];
    fixup_pattern(CS, p);
  }
}

/* ----------------------------------------------------------------------------- */
/* Code generation dispatch                                                      */
/* ----------------------------------------------------------------------------- */

/*
  Returns 0 for OK, or error < 0.
  On success, sets ep->code, ep->codesize.
  Runs the optimizing passes configured in CS->context->optims.
*/
int codegen (pexl_Context *C,
	     pexl_Optims *optims,	  // NULL for none
	     Pattern *main_pattern,	  // NULL for "main"
	     pexl_Binary *pkg) {
  int stat;
  int symidx;
  pexl_Index i;
  Pattern *pat;
  CodegenState *CS;
  SCC_Component element;
  pexl_Index cnum = 0; /* the only SCC component number NOT used */
  pexl_Index cnum_start = 0;

  if (!C || !pkg) {
    warn("codegen", "null required argument");
    return PEXL_ERR_INTERNAL;
  }
  CS = (CodegenState *)alloca(sizeof(CodegenState));
  if (!CS) {
    warn("codegen", "out of memory");
    return PEXL_ERR_OOM;
  }
  CS->context = C;
  CS->pkg = pkg;
  CS->optims = optims;

  trace("codegen", "-------------------------------------------------");
  trace("codegen", "-- Entering codegen for package %p --", (void *)pkg);
  trace("codegen", "-------------------------------------------------");

  if (!C->cst) {
    warn("codegen", "no compilation state");
    return PEXL_CODEGEN__ERR_NOTCOMPILED;
  }
  if (!C->cst->g) {
    warn("codegen", "compilation state has no graph");
    return PEXL_ERR_INTERNAL;
  }
  if (!C->cst->g->order) {
    inform("codegen", "no patterns bound in root environment");
    return PEXL_OK;
  }

  if (C->cst->g->order->top <= 0) {
    warn("codegen", "compilation state has empty ordered graph");
    return PEXL_ERR_INTERNAL;
  }

  for (i = 0; i < C->cst->g->order->top; i++) {
    CS->current_order_index = i;
    element = C->cst->g->order->elements[i];
    /* Are we starting a new strongly-connected component? */
    if (element.cnum != cnum) {
      /* Was there a previous one, and was it recursive?  */
      if (cnum > 0) fixupSCC(CS, cnum_start, i - 1);
      /* Set up to track the next SCC */
      cnum = element.cnum;
      cnum_start = i;
    }
    pat = C->cst->g->deplists[element.vnum][0];
    if (!pat->fixed) {
      warn("codegen_pattern",
	   "pattern %p has no 'fixed' expression tree", (void *)pat);
      return PEXL_CODEGEN__ERR_NOTCOMPILED;
    }
    if (!pattern_has_flag(pat, PATTERN_READY)) {
      warn("codegen_pattern", "expected pattern %p to be already compiled", (void *)pat);
      return PEXL_CODEGEN__ERR_NOTCOMPILED;
    }
    /* Allocate initial space for code, if needed */
    assert(pkg->codenext <= pkg->codesize);
    if ((pkg->codesize < INITIAL_CODESIZE) && (!resize_binary(pkg, INITIAL_CODESIZE)))
      return PEXL_ERR_OOM;
    assert(pkg->codenext <= pkg->codesize);
    /* Save the entrypoint for the code we are going to generate */
    pat->entrypoint = pkg->codenext;
    /* Generate the code, writing into the code vector in pkg */
    stat = gen(CS, pat->fixed->node, 0, NOINST, fullset);
    if (stat != PEXL_OK) return stat;
    /* Finally, add a return instruction */
    addinstruction(pkg, IRet);
    pat->codesize = pkg->codenext - pat->entrypoint;
    trace("codegen",
	 "Pattern %p has entrypoint %u, codesize %d and name '%s'",
	  (void *) pat, pat->entrypoint, pat->codesize, bound_name(CS, pat)); 
    symidx = symboltable_add(pkg->symtab,
			     bound_name(CS, pat),
			     symbol_global,
			     symbol_ns_id,
			     symbol_type_pattern,
			     pat->codesize,
			     pat->entrypoint,
			     &pat->meta);
    if (symidx < 0) return symtab_err_to_codegen_err(symidx);
    /* If the caller specified a "main" pattern, save its symtab index */
    if (pat == main_pattern) pkg->main = symidx;
    trace("codegen", "Compiled main pattern %p, in symbol table at index %u",
	  (void *) main_pattern, symidx);
  } /* for each pattern in the ordered control flow graph */

  if (element.cnum > 0) fixupSCC(CS, cnum_start, i - 1);

  /* Apply optimizations in order specified by optim_list */

  trace("codegen", "BEFORE OPTIMIZERS:");
  WHEN_TRACING fprint_binary(stderr, pkg);
  pexl_Optims *cur_optim = CS->optims;
  while (cur_optim) {
    switch (cur_optim->type) {
    case PEXL_OPT_INLINE:
      inform("codegen", "Running inline optimizer");
      stat = inlining_optimizer(pkg, &naive_inlining_policy);
      if (stat != PEXL_OK) return stat;
      trace("codegen", "After INLINING:");
      WHEN_TRACING fprint_binary(stderr, pkg);
      break;
    case PEXL_OPT_PEEPHOLE:
      /* Best to run this BEFORE inlining and again for last optimization pass. */
      inform("codegen", "Running peephole optimizer");
      stat = peephole(pkg);
      if (stat != PEXL_OK) return stat;
      trace("codegen", "After PEEPHOLE:");
      WHEN_TRACING fprint_binary(stderr, pkg);
      break;
    case PEXL_OPT_UNROLL:
      inform("codegen", "Running loop unrolling optimizer");
      stat = loop_optim(pkg, &unroll_policy, *(cur_optim->params));
      if (stat != PEXL_OK) return stat;
      trace("codegen", "After LOOP UNROLLING:");
      WHEN_TRACING fprint_binary(stderr, pkg);
      break;
    case PEXL_OPT_TRO:
      inform("codegen", "Running tail recursion optimization");
      stat = tro(pkg);
      if (stat != PEXL_OK) return stat;
      trace("codegen", "After TRO:");
      WHEN_TRACING fprint_binary(stderr, pkg);
      break;
    case PEXL_OPT_SIMD:
      inform("codegen", "Noted: SIMD is enabled");
      break;
    case PEXL_OPT_DYNAMIC_SIMD:
      inform("codegen", "Noted: Dynamic SIMD is enabled");
      break;
    default: // This is an error as all documented passes have a defined value ^
      warn("codegen", "Attempted to run an unknown optimization pass.");
      return PEXL_ERR_INTERNAL;
    }
    cur_optim = cur_optim->next;
  }

  trace("codegen", "AFTER OPTIMIZERS:");
  WHEN_TRACING fprint_binary(stderr, pkg);

  /* Trim code vector to final size */
  if (!resize_binary(pkg, pkg->codenext))
    return PEXL_ERR_OOM;
  assert(pkg->codesize == pkg->codenext);

  return PEXL_OK;
}

/*
  The technique and some of this code comes directly from lpeg (though
  it has been modified to reflect Rosie tree types, opcodes, and
  auxiliary data structures).
 
  From 'lpeg':
  Code generation is recursive; 'opt' indicates that the code is being
  generated as the last thing inside an optional pattern (so, if that
  code is optional too, it can reuse the 'IChoice' already in place
  for the outer pattern). 'tt' points to a previous test protecting
  this code (or NOINST). 'fl' is the follow set of the pattern.

  Main code-generation function: dispatch to auxiliary functions
  according to kind of node. ('exp_needfollow' should return true only
  for constructions that use 'fl'.)
*/
static int gen (CodegenState *CS, Node *node, int opt, int tt, Charset *fl)
{
  int err = OK;
  Charset tmp_charset;
  assert(node);
  assert(CS->pkg);
tailcall:
  switch (node->tag) {
  case TBytes:
    err = codebytes(CS, node->a.stringtab_handle, node->b.n, tt);
    break;
  case TAny:
    addinstruction(CS->pkg, IAny);
    break;
  case TSet:
    // TEMP: Fixing alignment problem with old lpeg AST design
    charset_copy(&tmp_charset, nodecharset(node));
    err = codecharset(CS, &tmp_charset, tt);
    break;
  case TTrue:
    break;
  case TFalse:
    addinstruction(CS->pkg, IFail);
    break;
  case TChoice:
    return codechoice(CS, child1(node), child2(node), opt, fl);
    break;
  case TRep:
    return coderep(CS, child1(node), opt, fl);
    break;
  case TBehind:
    return codebehind(CS, child1(node));
    break;
  case TNot:
    return codenot(CS, child1(node));
    break;
  case TAhead:
    return codeahead(CS, child1(node), tt);
    break;
  case TFind:
    return codefind(CS, child1(node), tt);
    break;
  case TCapture:
    return codecapture(CS, node, tt, fl);
    break;
  case TBackref:
    return codebackref(CS, node);
    break;  
  case TCall:
    err = codeICall(CS, node);
    break;
  case TXCall:
    err = codeXCall(CS, node);
    break;
  case TOpenCall: {
    /* All TOpenCall nodes should have been converted to TCall nodes. */
    return PEXL_CODEGEN__ERR_OPENFAIL;
  }
  case TSeq: {
    err = codeseq1(CS, child1(node), child2(node), &tt, fl); /* code 'p1' */
    if (err)
      return err;
    /* gen(pkg, p2, opt, tt, fl); */
    node = child2(node);
    goto tailcall;
  }
  case TWhere: {
    return codewhere(CS, node);
  }

  default: {
    warn("codegen", "illegal AST node type %d", node->tag);
    return PEXL_CODEGEN__ERR_IRTYPE;
  } }
  return err;
}
