/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  capture.c  The built-in capture processors                               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "capture.h"
#include "vm.h"			// pexl_Match
#include "pstring.h"

/* FUTURE:

   - Consider setjmp (in the vm) and longjmp (here) to abandon capture
     processing when there's an error.  Log it with warn() and then
     longjmp out of here.  The question is under what workloads will
     the overhead of calling setjmp be smaller than generating and
     checking return codes from many small encoding functions.

   - Add memoization to speed things up.  Particularly when looking up
     package prefix string -- the string table is immutable.
*/

typedef struct CapState {
  pexl_Match              *match;    /* output of vm */
  const pexl_PackageTable *pt;	     /* for symbol tables */
  const pexlCapture       *captures; /* capture stack */
  const char              *input;    /* input string */
  size_t                   startpos; /* offset into input */
  size_t                   endpos;   /* offset into input */
  pexl_Index               capcount; /* number of capture frames at ocap */

  char pos_size; /* TEMP: TEMP */

} CapState;

static const string *string_ref (CapState *cs, const pexlCapture *cap) {
  pexl_Index index = capidx(cap);
  pexl_Binary *p = packagetable_get(cs->pt, cap->pkg_number);
  if (!p) {
    warn("string_ref",
	 "invalid package number %d while encoding match results",
	 cap->pkg_number);
    return NULL;
  }
  return symboltable_get_name(p->symtab, index);
}

/* -----------------------------------------------------------------------------
 *
 * The 'debug' output encoder essentially prints each frame of the
 * capture stack as it is encountered.
 *
 * -----------------------------------------------------------------------------
 */

static void print_capture (CapState *cs, const pexlCapture *cap) {
  const string *prefix = NULL;
  const string *name;
  pexl_Binary *p;
  p = packagetable_get(cs->pt, cap->pkg_number);
  printf("  kind = %s\n", pexlCaptureName(capkind(cap)));
  printf("  pos (0-based) = %lu\n", cap->s ? (cap->s - cs->input) : 0);
  name = string_ref(cs, cap);
  prefix = symboltable_get_name(p->symtab, package_prefix(p));
  if (name) {
    printf("  package prefix     '%s'\n", prefix);
    printf("  idx = %2u           '%s'\n", capidx(cap), name);
  }
}

/* The 'debug' output encoder simply prints the details of the match results. */
static int
debug_Close (CapState *cs, const pexlCapture *cap, StringBuffer *buf, const int count) {
  UNUSED(buf); UNUSED(count); 
  if (isopencap(cap)) {
    warn("debug_Close",
	 "expected open capture, found %d (%s)",
	 capkind(cap), pexlCaptureName(capkind(cap)));
    return PEXL_ERR_INTERNAL;
  }
  printf("CLOSE: %s (%d)\n", pexlCaptureName(capkind(cap)), capkind(cap));
  if (capkind(cap) == Ccloseconst)
    print_capture(cs, cap);
  else
    printf("  pos (0-based) = %lu\n", cap->s ? (cap->s - cs->input) : 0);
  return PEXL_OK;
}

static int
debug_Open (CapState *cs, const pexlCapture *cap, StringBuffer *buf, const int count) {
  UNUSED(buf); UNUSED(count); 
  if (!acceptable_capture(capkind(cap))) {
    warn("debug_Open",
	 "expected open capture, found %d (%s)",
	 capkind(cap), pexlCaptureName(capkind(cap)));
    return PEXL_ERR_INTERNAL;
  }
  printf("OPEN:\n");
  print_capture(cs, cap);
  return PEXL_OK;
}

/* 
 * The caploop() uses a pair of functions, open and close, because the
 * PEXL capture stack is built as a balanced sequence of open and
 * close frames.
 *
 * The open/close encoder functions will always BOTH be called, and
 * they will be called in the nested way that one would expect from
 * the fact that the captures form a tree.
 */


/* caploop() processes the sequence of captures created by the vm.

   This sequence encodes a (possibly truncated) nested, balanced list
   of Opens and Closes.

   caploop() would naturally be written recursively, but a few years
   ago, I rewrote it in the iterative form it has now, where it
   maintains its own stack.  I no longer recall why.

   The stack is used to match up a Close capture (when we encounter it
   as we march along the capture sequence) with its corresponding Open
   (which we have pushed on our stack).

   The 'count' parameter contains the number of captures inside the
   Open at the top of the stack.  When it is not zero, the JSON
   encoder starts by emitting a comma "," because it is encoding a
   capture that is within a list of nested captures (but is not the
   first in that list).  Without 'count', a spurious comma would
   invalidate the JSON output.

   Note that the stack grows with the nesting depth of captures.  As
   of this writing (Friday, July 27, 2018), this depth rarely exceeds
   7 in the patterns we are seeing.
 */

#define capstart(cs) (capkind((cs)->cap)==Cconstant ? NULL : (cs)->cap->s)

typedef int (*enc)(CapState *cs, const pexlCapture *cap, StringBuffer *buf, const int count);

static int caploop (CapState *cs, enc Open, enc Close, StringBuffer *buf) {
  int err;
  const pexlCapture *cap;		/* points to current capture */
  pexl_Index count = 0;
  pexl_Index depth = 0;		/* incr at each Open, decr at each Close */
  pexl_Index i;			/* current capture frame */

  for (i = 0; i < cs->capcount; i++) {
    cap = &(cs->captures[i]);
    if (isopencap(cap)) {
      depth +=2;
      count = 0;
    } else if (isclosecap(cap)) {
      depth -= 2;
      if (depth < 0) {
	warn("caploop", "corrupt capture stack (depth < 0)");
      }
      count++;
    } else {
      warn("caploop", "corrupt capture stack (unexpected close capture)");
    }
  } /* for each capture stack entry */
  depth = 0;
  count = 0;

  for (i = 0; i < cs->capcount; i++) {
    cap = &(cs->captures[i]);
    if (isopencap(cap)) {
      depth++;
      if ((err = Open(cs, cap, buf, count))) return err;
      count = 0;
    } else if (isclosecap(cap)) {
      depth--;
      if (depth < 0) {
	warn("caploop", "corrupt capture stack (depth < 0)");
	return PEXL_ERR_INTERNAL;
      }
      if ((err = Close(cs, cap, buf, count))) return err;
      count++;
    } else {
      warn("caploop", "corrupt capture stack (unexpected close capture)");
      return PEXL_ERR_INTERNAL;
    }
  } /* for each capture stack entry */

  if (depth != 0) {
    warn("caploop", "corrupt capture stack");
    return PEXL_ERR_INTERNAL;
  }
  return PEXL_OK;
}

__attribute__((unused))
static int
debug_encoder (const struct pexl_PackageTable *pt,
	       const string *input, size_t inputlen,
	       struct pexlCapture *captures, pexl_Index num_captures,
	       /* in/out */
	       struct pexl_Match *match) {
  UNUSED(match);
  UNUSED(inputlen);
  CapState cs;
  if (num_captures == 0) return PEXL_OK;
  cs.pos_size = 0;
  cs.captures = captures;
  cs.input = input;
  cs.capcount = num_captures;
  cs.pt = pt;
  return caploop(&cs, &debug_Open, &debug_Close, NULL);
}

/* -----------------------------------------------------------------------------
 *
 * The 'tree' output encoder produces an n-ary ordered tree
 * represented using an array of nodes.  Each node has a flag
 * indicating if it has children.  The first child of the node at
 * index i is found at index i+1.  Each node stores the index of its
 * sibling, if any.
 *
 * -----------------------------------------------------------------------------
 */

static pexl_Index offset (SymbolTableEntry *entry) {
  if (!entry) return -1;
  return entry->handle;
}

/* FUTURE: Memoize prefix and (maybe) package pointer and (maybe) name. */
static pexl_Position decode (pexlCapture             *captures,
			     pexl_Index               num_captures,
			     pexl_Index              *next_frame,
			     const pexl_PackageTable *pt,
			     const char              *input,
			     pexl_MatchTree          *tree,
			     pexl_Index               parent_idx) {
  int err;
  pexl_Position pos;
  pexl_Binary *pkg;
  pexl_Index node_idx;
  const pexlCapture *cap;		/* points to current capture */
  char constant_capture;
  SymbolTableEntry *prefix, *typename, *data;

  trace("decode", "entering at frame %" pexl_Index_FMT " of %" pexl_Index_FMT, *next_frame, num_captures);
  
  assert(captures); assert(tree); assert(next_frame);
  assert(num_captures > 0);
  assert(*next_frame >= 0);
  assert(*next_frame < num_captures);

  cap = &captures[*next_frame];
  if (!isopencap(cap)) {
    warn("decode", "corrupt capture stack (expected open frame)");
    return PEXL_ERR_INTERNAL;
  }
  pkg = packagetable_get(pt, cap->pkg_number);
  if (!pkg) {
    warn("decode", "corrupt capture stack (invalid package number)");
    return PEXL_ERR_INTERNAL;
  }
  pos = cap->s - input;
  prefix = symboltable_get(pkg->symtab, package_prefix(pkg));
  typename = symboltable_get(pkg->symtab, capidx(cap));
  constant_capture = (capkind(cap) == Cconstant);
  node_idx = match_tree_add(tree, parent_idx, offset(prefix), offset(typename), pos);
  if (node_idx < 0) return PEXL_ERR_INTERNAL;

  (*next_frame)++;
  if (*next_frame >= num_captures) {
    warn("decode", "corrupt capture stack (truncated)");
    return PEXL_ERR_INTERNAL;
  }
  cap = &captures[*next_frame];

  /* While there are child nodes to node_idx ... */
  while (isopencap(cap)) {
    trace("decode", "examining frame %" pexl_Index_FMT " of %" pexl_Index_FMT, *next_frame, num_captures);
    pos = decode(captures, num_captures, next_frame,
		 pt, input, tree, node_idx);
    if (pos < 0) return pos;	/* error */
    if (*next_frame >= num_captures) {
      warn("decode", "corrupt capture stack (truncated at %d)", *next_frame);
      return PEXL_ERR_INTERNAL;
    }
    cap = &captures[*next_frame];
  }
  /* Next frame will be a Close */
  if (isclosecap(cap)) {
    trace("decode", "at close frame %" pexl_Index_FMT " of %" pexl_Index_FMT, *next_frame, num_captures);
    if (capkind(cap) == Ccloseconst) {
      if (!constant_capture) {
	warn("decode", "corrupt capture stack (mismatched const cap open/close)");
	return PEXL_ERR_INTERNAL;
      }
      data = symboltable_get(pkg->symtab, capidx(cap));
    } else {
      if (constant_capture) {
	warn("decode", "corrupt capture stack (mismatched const cap open/close)");
	return PEXL_ERR_INTERNAL;
      }
      data = NULL;
    }
    pos = cap->s - input;
    err = match_tree_close(tree, node_idx, offset(data), pos);
    if (err) return err;
  } else {
    warn("decode", "corrupt capture stack (missing close frame)");
    return PEXL_ERR_INTERNAL;
  }

  (*next_frame)++;
  if (*next_frame > num_captures) {
    warn("decode", "corrupt capture stack (excess frames)");
    return PEXL_ERR_INTERNAL;
  }
  /* 
     Return the last endpos we put in the tree, so we can close the
     root node with it
  */
  return pos;
}

/* Returns 0 for success or error < 0. */
__attribute__((unused))
static int
tree_encoder (const struct pexl_PackageTable *pt,
	      const string *input, size_t inputlen,
	      struct pexlCapture *captures, pexl_Index num_captures,
	      /* in/out */
	      struct pexl_Match *match) {
  UNUSED(inputlen);
  pexl_Position last_pos;
  pexl_Index index = 0;
  int err;
  if ((!pt) || (!captures) || (!input) || (!match)) {
    inform("tree_encoder", "required arg is null");
    return PEXL_ERR_NULL;
  }
  assert(match->encoder_id == PEXL_TREE_ENCODER);
  if (pt->next != 1) {
    confess("tree_encoder",
	    "not implemented yet: package table with %u > 1 packages", pt->next);
    return PEXL_ERR_INTERNAL;
  }
  /* If match->data is set already, re-use it. */
  if (match->data)
    match_tree_reset(match->data);
  else {
    /* 
       Estimated capacity of tree is half number of capture frames,
       which are in open/close pairs, plus one for root
    */
    match->data = match_tree_new(((size_t) num_captures) / 2 + 1);
    if (!match->data) {
      warn("tree_encoder", "out of memory");
      return PEXL_ERR_OOM;
    }
  }
  /* Add a root node over the ordered forest of captures:
       parent node = -1 (we are adding the root)
       prefix = -1 (no prefix)
       typename = -1 (no type name)
       position = 0 (because matching begins at first char)
    Since we are adding the root node, returned node index will be 0.
   */
  last_pos = match_tree_add(match->data, -1, -1, -1, 0);
  if (last_pos != 0) return PEXL_ERR_INTERNAL;

  WHEN_TRACING {
    printf("** pexlCapture stack with %d frames:\n", num_captures);
    if (num_captures > 10) {
      printf("   ... too many frames to print\n");
    }
    else {
      for (int i=0; i < num_captures; i++) {
	pexlCapture *cap = &captures[i];
	printf("%4d: ", i);
	if (isopencap(cap)) printf("OPEN  ");
	else if (isclosecap(cap)) printf("CLOSE ");
	else printf("????  ");
	puts("");
      }
    }
  }

  while (index < num_captures) {
    last_pos = decode(captures, num_captures, &index,
		      pt, input, match->data, 0);
    if (last_pos < 0) return last_pos; /* error */
  }
  /* Node to close = 0 (root); data field = -1 (no data) */
  err = match_tree_close(match->data, 0, -1, last_pos);
  if (err != MATCHTREE_OK) return err;

  assert(pt->next == 1);	// TEMP: Remove after enhancement
  pexl_Binary *pkg = packagetable_get(pt, 0);
  assert(pkg);
  SymbolTable *st = pkg->symtab;
  assert(st);
  /* TODO: What if there's more than one package?  See capture-notes.md */
  size_t blocklen = StringBuffer_len(st->block);
  const string *blockptr = StringBuffer_ptr(st->block);
  ((pexl_MatchTree *)match->data)->block = malloc(blocklen);
  memcpy(((pexl_MatchTree *)match->data)->block, blockptr, blocklen);
  ((pexl_MatchTree *)match->data)->blocklen = blocklen;
  return MATCHTREE_OK;
}

/* ----------------------------------------------------------------------------- */

__attribute__((unused))
static void _free_buffer (void *data) {
  if (!data) return;
  StringBuffer_free((StringBuffer *)data);
}

__attribute__((unused))
static void _free_matchtree (void *data) {
  if (!data) return;
  match_tree_free((pexl_MatchTree *)data);
}

/* Each encoder name is defined in an enum in libpexl.h */
pexlEncoder get_encoder_fn (pexlEncoderID id) {
  switch (id) {
  case PEXL_TREE_ENCODER: return tree_encoder;
  case PEXL_DEBUG_ENCODER: return debug_encoder;
  case PEXL_NO_ENCODER: return NULL;
  default:
    warn("vm", "invalid encoder id (%d)", id);
    return NULL;
  }
}

pexlEncoderDataFree get_encoder_destructor (pexlEncoderID id) {
  switch (id) {
  case PEXL_TREE_ENCODER: return _free_matchtree;
  case PEXL_DEBUG_ENCODER: return NULL;
  case PEXL_NO_ENCODER: return NULL;
  default:
    warn("vm", "invalid encoder id (%d)", id);
    return NULL;
  }
}

