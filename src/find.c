/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  find.c  Find a matching byte or one of a set of bytes                    */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "find.h"

/*
  TODO:

  - Make two low-level compile-time optimizations for SIMD:
    (1) Whether or not to compile the bitmasks needed to use SIMD
    (2) Whether to convert expressions like P / {. P} into find:P

  - Pre-compute the searchvec for in_set_1().

  - Write in_set_2() and in_set_3() using _mm_or_si128() on result of
    one call to _mm_cmpeq_epi8() for each byte

  - Write in_range1() and in_range2() which look for chars that lie
    within either one or two ranges, respectively.
    E.g. in_range1(input, 'A', 'Z'); in_range2(input, 'A', 'Z', 'a', 'z')
    See http://0x80.pl/articles/simd-byte-lookup.html.

  - Define instructions IRange1, IRange2.

  - Find the constant vectors and test to see if defining them as
    globals (or static?) has an effect on performance.  (Could access
    could be slower due to GOT?  Does the current impl really
    construct these vectors every time the function runs, or is the
    compiler inlining the data already, eliding the constructor?)
*/

/* CTZ: c=count t=trailing (l=leading) z=zeros */
#ifdef __GNUC__
  #define CTZ(bitmask) __builtin_ctz(bitmask)
#else
  #define CTZ(bitmask) _tzcnt_u32(bitmask)
#endif

/*
  Implementation of algorithm "Universal2" from
  http://0x80.pl/articles/simd-byte-lookup.html.
  Intel Intrinsics pexl_Reference:
  https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html

  Find: Search for char (or any of a set of chars), returning either a
  pointer to the first occurrance or to the 'end' parameter (meaning
  that no occurrance was found).  Return value of NULL indicates an
  error.

  Span: Advance ptr while it matches char (or any of a set of chars),
  returning either a pointer to the first mismatch, or the value of
  the 'end' parameter (meaning that no mismatch was found).  Return
  value of NULL indicates an error.

  NOTE: The 'end' parameter holds an address one BEYOND the last byte
  to be searched.
*/

/* Memchr searcher that is local to this file, so able to be inlined */
static inline const char *_find_char (const uint8_t c,
				      const char *ptr, const char *end) {
  
  ptr = memchr(ptr, (int)((char)c), end - ptr);
  return (ptr == NULL) ? end : ptr;
}

#if PEXL_USING_SIMD

#if defined (DEBUG)
#include <stdalign.h>
__attribute__((unused))
static void print128_hex_u8 (__m128i in) {
    alignas(16) uint8_t v[16];
    _mm_store_si128((__m128i*)v, in); /* SSE2 */
    printf("v16_u8: %x %x %x %x | %x %x %x %x | %x %x %x %x | %x %x %x %x\n",
           v[0], v[1],  v[2],  v[3],  v[4],  v[5],  v[6],  v[7],
           v[8], v[9], v[10], v[11], v[12], v[13], v[14], v[15]);
}
#endif

/* ----------------------------------------------------------------------------- */
/* Functions to process 128-bit result vectors                                   */
/* ----------------------------------------------------------------------------- */

static inline bool zero_p (__m128i v) {
#if __SSE4_1__
    return _mm_testz_si128(v, v);         /* SSE4.1 */
#else
    uint32_t mask = _mm_movemask_epi8(v); /* SSE2 */
    return (mask == 0);
#endif
}

static inline bool all_bytes_nonzero_p (__m128i v) {
  const __m128i cmp = _mm_cmpeq_epi8(v, _mm_setzero_si128()); /* SSE2 (all) */
  return zero_p(cmp);
}

/*
  Use CTZ() on the return value to get first nonzero byte position.
  Returns 16 if all bytes in 'v' are zero.
*/
static inline uint32_t first_nonzero_byte_bitmask (__m128i v) {
  const __m128i cmp = _mm_cmpeq_epi8(v, _mm_setzero_si128()); /* SSE2 (all) */
  return ~(_mm_movemask_epi8(cmp)); /* SSE2 */
}

/* Return index 0..15 of first zero byte, or 16 if all non-zero. */
static inline uint32_t first_zero_byte (__m128i v) {
  const __m128i cmp = _mm_cmpeq_epi8(v, _mm_setzero_si128()); /* SSE2 (all) */
  const uint32_t bitmask = _mm_movemask_epi8(cmp);            /* SSE2 */
  if (!bitmask) return 16;
  uint32_t pos = CTZ(bitmask);
  return pos;
}

static inline __m128i popcnt8 (__m128i n) {
  __m128i popcount_mask = _mm_set1_epi8(0x0F);
  __m128i popcount_table = _mm_setr_epi8(0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4);
  __m128i pcnt0 = _mm_shuffle_epi8(popcount_table, _mm_and_si128(n, popcount_mask));
  __m128i pcnt1 = _mm_shuffle_epi8(popcount_table, _mm_and_si128(_mm_srli_epi16(n, 4), popcount_mask));
  return _mm_add_epi8(pcnt0, pcnt1);
}

static inline __m128i popcnt64(__m128i n) {
   __m128i cnt8 = popcnt8(n);
   return _mm_sad_epu8(cnt8, _mm_setzero_si128());
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
#pragma GCC diagnostic ignored "-Wpointer-sign"
static inline __m128i load_unaligned_128i_char (const char *ptr) {
  return _mm_loadu_si128((const __m128i *) (const uint8_t *) ptr);
}
// static inline __m128i load_aligned_128i_char (const char *ptr) {
//   return _mm_load_si128((const __m128i *) (const uint8_t *) ptr);
// }
static inline __m128i load_unaligned_128i_pexl_Byte (const pexl_Byte *ptr) {
  return _mm_loadu_si128((const __m128i *) (const uint8_t *) ptr);
}
static inline __m128i load_unaligned_128i_uint32 (const uint32_t *ptr) {
  return _mm_loadu_si128((const __m128i *) (const uint8_t *) ptr);
}
#pragma GCC diagnostic pop

/* 'ptr' must point to 128 bits (16 bytes) */
static inline int popcnt128 (const uint32_t *ptr) {
  __m128i n = load_unaligned_128i_uint32(ptr);
  __m128i cnt64 = popcnt64(n);
  __m128i cnt64_hi = _mm_unpackhi_epi64(cnt64, cnt64);
  __m128i cnt128 = _mm_add_epi32(cnt64, cnt64_hi);
  return _mm_cvtsi128_si32(cnt128);
}

/* ----------------------------------------------------------------------------- */
/* Convert a charset into a form suitable for 128-bit simd searching             */
/* ----------------------------------------------------------------------------- */

/*
  Iterate through each of the 256 bits in the charset, adding each
  character (byte) to the simd bitmap.  We have to create the bitmap
  differently depending on whether the set contains < 8 characters or
  not. 

  We also look for the special cases of a single character (which
  needs no bitmap), the empty charset (==> fail), and the full charset
  (==> the 'any' instruction).

  N.B. Caller must pay attention to the return value, which is a
  coarse measure of number of characters in the Charset cs:

    0 ==> cs is empty; emit IFail instruction; 'set' and 'one_char' do NOT
          contain valid values.
    1 ==> cs contains one char; emit IChar instruction; 'one_char' contains
          the one char, and 'set' does NOT contain a valid value.
    8 ==> cs contains between 2 and 8 chars; emit appropriate instruction;
          'one_char' does NOT contain a valid value.
  255 ==> cs contains between 9 and 255 chars; emit appropriate instruction
          'one_char' does NOT contain a valid value.
  256 ==> cs is full; emit IAny instruction; 'set' and 'one_char' do NOT
          contain valid values.
*/
int charset_to_simd_mask (const Charset *cs, SetMask_t *set, uint8_t *one_char) {
  WHEN_DEBUGGING
    if (!cs || !set || !one_char) {
      warn("charset_to_simd_mask", "null required arg");
      return PEXL_ERR_NULL;
    }
  int count = popcnt128(cs->cs) + popcnt128(cs->cs+4);
  if (count == 0) return 0;
  if (count == 256) return 256;
  uint8_t l1[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  uint8_t l2[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int n = -1;			/* track the nth char that is IN charset */
  for (int i = 0; i < 256; i++) {
    uint8_t c = (uint8_t) i & 0xFF;
    if (testchar(cs->cs, c)) {
      if (count == 1) {
	*one_char = c; /* the one char in the set */
	return 1;
      }
      n++;
      uint8_t low = c & 0x0f;       /* low nibble */
      uint8_t high = c >> 4;	    /* high nibble */
      if (count <= 8) {
	uint8_t mask = 1 << n;
	l1[low] |= mask;
	l2[high] |= mask;
      } else {
	if (high < 8) {
	  l1[low] |= (1 << high);
	} else {
	  l2[low] |= (1 << (high - 8));
	}
      }	/* if count <= 8 or >8 */
    } /* if testchar() */
  } /* for each char in charset */

  /* Set contents get converted into these two bitmaps */
  set->bitmap_0_7 = load_unaligned_128i_pexl_Byte(l1);  /* SSE2 */
  set->bitmap_8_15 = load_unaligned_128i_pexl_Byte(l2); /* SSE2 */

  if (count == 1) return 1;
  return (count <= 8) ? 8 : 255;
}

/* ----------------------------------------------------------------------------- */
/* Use simd intrinsics to search 16 bytes at a time                              */
/* ----------------------------------------------------------------------------- */

static const uint8_t lowmask_array[16] __attribute__((aligned(16))) =
  {0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f,
   0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f};
static const __m128i *lowmask = (const __m128i *) lowmask_array;

static const uint8_t mask8f_array[16] __attribute__((aligned(16))) =
  {0x8f, 0x8f, 0x8f, 0x8f, 0x8f, 0x8f, 0x8f, 0x8f,
   0x8f, 0x8f, 0x8f, 0x8f, 0x8f, 0x8f, 0x8f, 0x8f};
static const __m128i *mask8f = (const __m128i *) mask8f_array;

static const uint8_t bitmask_lookup_array[16] __attribute__((aligned(16))) =
  {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80,
   0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
static const __m128i *bitmask_lookup = (const __m128i *) bitmask_lookup_array;

/* Use in_set_* ONLY when there are at least 16 bytes at ptr to search */
static inline __m128i in_set_full (const char *ptr, const SetMask_t *set) {
  const __m128i input = load_unaligned_128i_char(ptr); /* SSE2 */
  const __m128i higher_nibbles = _mm_and_si128(_mm_srli_epi16(input, 4), *lowmask);
  const __m128i i_0_7 = _mm_and_si128(input, *mask8f);
  const __m128i i_8_15 = _mm_xor_si128(i_0_7, *bitmask_lookup);
  /* fetch row 0..7 and row 8..15 */
  const __m128i row_0_7 = _mm_shuffle_epi8(set->bitmap_0_7, i_0_7); /* SSSE3 */
  const __m128i row_8_15 = _mm_shuffle_epi8(set->bitmap_8_15, i_8_15); /* SSSE3 */
  /* choose row halves */
  const __m128i bitsets = _mm_or_si128(row_0_7, row_8_15);              /* SSE2 */
  /* calculate bitmask (index into bitsets) */
  const __m128i bitmask = _mm_shuffle_epi8(*bitmask_lookup, higher_nibbles); /* SSSE3 */
  /* check which bytes of input are in the set */
  return _mm_cmpeq_epi8(_mm_and_si128(bitsets, bitmask), bitmask); /* SSE2 */
}

/* Use this when set has 8 or fewer elements and there are 16+ bytes at ptr */
static inline __m128i in_set_8 (const char *ptr, const SetMask_t *set) {
  const __m128i input = load_unaligned_128i_char(ptr); /* SSE2 */
  const __m128i lower_nibbles = _mm_and_si128(input, _mm_set1_epi8(0x0f)); /* SSE2 (both) */
  const __m128i higher_nibbles = _mm_and_si128(_mm_srli_epi16(input, 4), _mm_set1_epi8(0x0f)); /* SSE2 (all) */
  const __m128i t0 = _mm_shuffle_epi8(set->bitmap_0_7, lower_nibbles);   /* SSSE3 */
  const __m128i t1 = _mm_shuffle_epi8(set->bitmap_8_15, higher_nibbles); /* SSSE3 */
  return _mm_and_si128(t0, t1); /* SSE2 */
}

static inline __m128i in_set_1 (const char *ptr, const __m128i searchvec) {
  const __m128i input = load_unaligned_128i_char(ptr);
  return _mm_cmpeq_epi8(input, searchvec);
}

/* ----------------------------------------------------------------------------- */
/* SIMD results cache impl.                                                      */
/* ----------------------------------------------------------------------------- */

#define CACHE_SIZE (1)

#define NOT_FOUND (-1)

#define CACHE_NAME(kind) cache_ ## kind
#define CACHE(kind) (&(CACHE_NAME(kind)))

typedef struct _cache_entry {
  const char    *position; 
  const char    *end;		/* always position + 16 */
  const Charset *charset; 
  uint32_t       result;
  char           c;
} _cache_entry;
  
typedef struct _cache {
  _cache_entry entries[CACHE_SIZE];
  uint32_t     probes;
  uint32_t     hits;
  uint8_t      last;		/* most recently used entry */
} _cache;

#define MAKE_CACHE(kind)			\
  static _cache CACHE_NAME(kind)

static void cache_init (_cache *cache) {
  cache->last = CACHE_SIZE - 1;
  cache->probes = 0;
  cache->hits = 0;
  for (int i=0; i < CACHE_SIZE; i++) {
    cache->entries[i].position = NULL;
  }
}

#define CACHED_AT(cache, input_ptr, charset_ptr, i)		\
  ((cache)->entries[i].position &&				\
   (input_ptr < (cache)->entries[i].end) &&			\
   (input_ptr > (cache)->entries[i].position) &&		\
   ((cache)->entries[i].charset == charset_ptr))

static inline int cache_query (_cache *cache,
			       const char *input,
			       const Charset *cs) {
  cache->probes++;
  for (int i=cache->last; i < CACHE_SIZE; i++)
    if (CACHED_AT(cache, input, cs, i)) {
      cache->hits++;
      return i;
    }
  for (int i=0; i < cache->last; i++)
    if (CACHED_AT(cache, input, cs, i)) {
      cache->hits++;
      return i;
    }
  return NOT_FOUND;
}

#define CACHED1_AT(cache, input_ptr, c, i)			\
  ((cache)->entries[i].position &&				\
   (input_ptr < (cache)->entries[i].end) &&			\
   (input_ptr > (cache)->entries[i].position) &&		\
   ((cache)->entries[i].c == c))

static inline int cache1_query (_cache *cache,
				const char *input,
				const char c) {
  cache->probes++;
  for (int i=cache->last; i < CACHE_SIZE; i++)
    if (CACHED1_AT(cache, input, c, i)) {
      cache->hits++;
      return i;
    }
  for (int i=0; i < cache->last; i++)
    if (CACHED1_AT(cache, input, c, i)) {
      cache->hits++;
      return i;
    }
  return NOT_FOUND;
}

#define CACHED_POSITION(kind, i) (CACHE(kind)->entries[(i)].position)
#define CACHED_CHARSET(kind, i) (CACHE(kind)->entries[(i)].charset)
#define CACHED_RESULT(kind, i) (CACHE(kind)->entries[(i)].result)

static inline int cache_next_slot (_cache *cache) {
#if CACHE_SIZE == 1
  UNUSED(cache);
  return 0;
#elif CACHE_SIZE == 2
  cache->last = !cache->last;
  return cache->last;
#else
  cache->last++;
  if (cache->last == CACHE_SIZE) cache->last = 0;
  return cache->last;
#endif
}

static inline int cache_set (_cache *cache,
			     const char *input,
			     const Charset *cs,
			     uint32_t result) {
  int i = cache_next_slot(cache);
  cache->entries[i].position = input;
  cache->entries[i].end = input + 16; /* set happens less often then query */
  cache->entries[i].charset = cs;
  cache->entries[i].result = result;
  return i;
}

static inline int cache1_set (_cache *cache,
			      const char *input,
			      const char c,
			      uint32_t result) {
  int i = cache_next_slot(cache);
  cache->entries[i].position = input;
  cache->entries[i].end = input + 16; /* set happens less often then query */;
  cache->entries[i].c = c;
  cache->entries[i].result = result;
  return i;
}

static void print_cache_stats (const char *name, _cache *cache) {
  int used = 0;
  for (int i=0; i < CACHE_SIZE; i++)
    if (cache->entries[i].position) used++;
  fprintf(stderr, "%s: Cache usage = %d / %d.  Probes = %d, hits = %d, hit ratio %4.1f%%\n",
	  name,
	  used,
	  CACHE_SIZE,
	  cache->probes, cache->hits,
	  (float) 100.0 * cache->hits / cache->probes);
}

__attribute__((unused))
static void cache_dump (_cache *cache) {
  print_cache_stats("", cache);
  fprintf(stderr, "pexl_Index      Input    Charset  Result\n");
  for (int i=0; i < CACHE_SIZE; i++) {
    fprintf(stderr, "  %2d: %10p %10p    0x%02X\n", i,
	    cache->entries[i].position,
	    cache->entries[i].charset,
	    cache->entries[i].result & 0xFF);
  }
}

/* ----------------------------------------------------------------------------- */
/* Actual caches */
/* ----------------------------------------------------------------------------- */

MAKE_CACHE(simd1);
MAKE_CACHE(simd8);
MAKE_CACHE(simd);

void simd_cache_initialize (void) {
  cache_init(CACHE(simd1));
  cache_init(CACHE(simd8));
  cache_init(CACHE(simd));
}

void simd_cache_print_stats (void) {
  print_cache_stats("simd1", CACHE(simd1));
  print_cache_stats("simd8", CACHE(simd8));
  print_cache_stats("simd", CACHE(simd));
}

/* ----------------------------------------------------------------------------- */
/* find_simd()                                                                   */
/* ----------------------------------------------------------------------------- */

/* Return pointer to first match, else 'end'. */
#define MAKE_FINDER(kind, in_set_test, bitmask_generator, bitmask_to_offset, fallback) \
  const char *find_ ## kind (const Charset *simd_cs, const Charset *cs,	\
			     const char *ptr, const char *end) {	\
  WHEN_DEBUGGING {							\
    if (!simd_cs || !cs || !ptr || !end) {				\
      warn("finder", "null required arg");				\
      return NULL;							\
    } }									\
  __m128i result; uint32_t bitmask; uint32_t pos;			\
  int index = cache_query(CACHE(kind), ptr, simd_cs);			\
  if (index != NOT_FOUND) {						\
    ptrdiff_t diff = ptr - CACHED_POSITION(kind, index);		\
    assert(diff > 0); assert(diff < 16);				\
    /* Clear the bits we need to skip over: the ones before diff  */	\
    bitmask = CACHED_RESULT(kind, index) & ~((1<<diff)-1);		\
    pos = bitmask_to_offset(bitmask);					\
    assert(pos <= 16);							\
    ptr = CACHED_POSITION(kind, index) + pos;				\
    if (pos != 16) return ptr;						\
  }									\
  /* No cache hit, so do the search */					\
  while (ptr < (end - 15)) {						\
    result = in_set_test(ptr, (const SetMask_t *) simd_cs);		\
    if (zero_p(result)) {						\
      ptr += 16;							\
    } else {								\
      bitmask = bitmask_generator(result);				\
      cache_set(CACHE(kind), ptr, simd_cs, bitmask);			\
      return ptr + bitmask_to_offset(bitmask);				\
    }									\
  }									\
  if (ptr > (end - 16)) ptr = fallback(cs, ptr, end);			\
  return ptr;								\
}

MAKE_FINDER(simd, in_set_full, first_nonzero_byte_bitmask, CTZ, find_charset)
MAKE_FINDER(simd8, in_set_8, first_nonzero_byte_bitmask, CTZ, find_charset)

/* ----------------------------------------------------------------------------- */
/* find_simd1()                                                                  */
/* ----------------------------------------------------------------------------- */

/* Return pointer to first match, else 'end'. */
const char *find_simd1 (const uint8_t c,
			const char *ptr, const char *end) {
  WHEN_DEBUGGING
    if (!ptr || !end) {
      warn("find_simd1", "null required arg");
      return NULL;
    }
  __m128i result; uint32_t bitmask; uint32_t pos;
  int index = cache1_query(CACHE(simd1), ptr, c);
  if (index != NOT_FOUND) { 
    ptrdiff_t diff = ptr - CACHED_POSITION(simd1, index);
    assert(diff > 0); assert(diff < 16);
    /* Clear the bits we need to skip over: the ones before diff  */ 
    bitmask = CACHED_RESULT(simd1, index) & ~((1<<diff)-1);
    pos = CTZ(bitmask);
    assert(pos <= 16);
    ptr = CACHED_POSITION(simd1, index) + pos;
    if (pos != 16) return ptr;
  }
  /* No cache hit, so do the search */
  const __m128i searchvec = _mm_set1_epi8(c);
  while (ptr < (end - 15)) {
    result = in_set_1(ptr, searchvec);
    if (zero_p(result)) {
      ptr += 16;
    } else {
      bitmask = first_nonzero_byte_bitmask(result);
      cache1_set(CACHE(simd1), ptr, c, bitmask);
      return ptr + CTZ(bitmask);
    }
  }
  if (ptr > (end - 16)) ptr = _find_char(c, ptr, end);
  return ptr;
}

/* ----------------------------------------------------------------------------- */
/* span_simd()                                                                   */
/* ----------------------------------------------------------------------------- */

/* Does NOT check the last < 16 bytes.  Caller must do that. */
static inline const char *_span_simd (const SetMask_t *set,
				      const char *ptr, const char *end) {
  __m128i result;
  while (ptr < (end - 15)) {
    result = in_set_full(ptr, set); 
    if (all_bytes_nonzero_p(result)) {
      ptr += 16;
    } else {
      return ptr + first_zero_byte(result);
    }
  }
  return ptr;
}

/* Return pointer to first non-match, possibly 'end'. */
const char *span_simd (const Charset *simd_cs, const Charset *cs,
		       const char *ptr, const char *end) {
  WHEN_DEBUGGING
    if (!simd_cs || !cs || !ptr || !end) {
      warn("span_simd", "null required arg");
      return NULL;
    }
  ptr = _span_simd((const SetMask_t *) simd_cs, ptr, end);
  if (ptr > (end - 16)) {
    return span_charset(cs, ptr, end);
  } else {
    return ptr;
  }
}

/* ----------------------------------------------------------------------------- */
/* span_simd8()                                                                  */
/* ----------------------------------------------------------------------------- */

/* Does NOT check the last < 16 bytes.  Caller must do that. */
static inline const char *_span_simd8 (const SetMask_t *set,
				       const char *ptr, const char *end) {
  __m128i result;
  while (ptr < (end - 15)) {
    result = in_set_8(ptr, set);
    if (all_bytes_nonzero_p(result)) {
      ptr += 16;
    } else {
      return ptr + first_zero_byte(result);
    }
  }
  return ptr;
}

/* Return pointer to first non-match, possibly 'end'. */
const char *span_simd8 (const Charset *simd_cs, const Charset *cs,
			const char *ptr, const char *end) {
  WHEN_DEBUGGING
    if (!simd_cs || !cs || !ptr || !end) {
      warn("span_simd8", "null required arg");
      return NULL;
    }
  ptr = _span_simd8((const SetMask_t *) simd_cs, ptr, end);
  if (ptr > (end - 16)) {
    return span_charset(cs, ptr, end);
  } else {
    return ptr;
  }
}

/* ----------------------------------------------------------------------------- */
/* span_simd1()                                                                  */
/* ----------------------------------------------------------------------------- */

/* Return pointer to first non-match, possibly 'end'. */
const char *span_simd1 (uint8_t c, const char *ptr, const char *end) {
  __m128i result;
  const __m128i searchvec = _mm_set1_epi8(c);
  WHEN_DEBUGGING
    if (!ptr || !end) {
      warn("span_simd1", "null required arg");
      return NULL;
    }
  while (ptr < (end - 15)) {
    result = in_set_1(ptr, searchvec);
    if (all_bytes_nonzero_p(result)) {
      ptr += 16;
    } else {
      return ptr + first_zero_byte(result);
    }
  }
  /* Search the last < 16 bytes of the input */
  while (ptr < end) {
    if (*ptr != c) return ptr;
    ptr++;
  }
  return end;
}

const bool PEXL_simd_available = true;

#else   /* Compiling without SIMD */

const bool PEXL_simd_available = false;

void simd_cache_initialize (void) {
  /* It's not an error if we want to call this and have it be a NOOP */
  inform("simd_cache_initialize", "compiled without SIMD support");
}

void simd_cache_print_stats (void) {
  confess("simd_cache_print_stats", "compiled without SIMD support");
}

int charset_to_simd_mask (const Charset *cs, SetMask_t *set, uint8_t *one_char) {
  UNUSED(cs); UNUSED(set); UNUSED(one_char);
  confess("charset_to_simd_mask", "compiled without SIMD support");
  return PEXL_ERR_INTERNAL;
}
  
const char *find_simd (const Charset *simd_cs, const Charset *cs,
		       const char *ptr, const char *end) {
  UNUSED(simd_cs); UNUSED(cs); UNUSED(ptr); UNUSED(end);
  confess("find_simd", "compiled without SIMD support");
  return NULL;
}

const char *find_simd8 (const Charset *simd_cs, const Charset *cs,
			const char *ptr, const char *end) {
  UNUSED(simd_cs); UNUSED(cs); UNUSED(ptr); UNUSED(end);
  confess("find_simd8", "compiled without SIMD support");
  return NULL;
}

const char *find_simd1 (const uint8_t c, const char *ptr, const char *end) {
  UNUSED(c); UNUSED(ptr); UNUSED(end);
  confess("find_simd1", "compiled without SIMD support");
  return NULL;
}

const char *span_simd (const Charset *simd_cs, const Charset *cs,
		       const char *ptr, const char *end) {
  UNUSED(simd_cs); UNUSED(cs); UNUSED(ptr); UNUSED(end);
  confess("span_simd", "compiled without SIMD support");
  return NULL;
}

const char *span_simd8 (const Charset *simd_cs, const Charset *cs,
			const char *ptr, const char *end) {
  UNUSED(simd_cs); UNUSED(cs); UNUSED(ptr); UNUSED(end);
  confess("span_simd8", "compiled without SIMD support");
  return NULL;
}

const char *span_simd1 (const uint8_t c, const char *ptr, const char *end) {
  UNUSED(c); UNUSED(ptr); UNUSED(end);
  confess("span_simd1", "compiled without SIMD support");
  return NULL;
}

#endif	/* Whether or not we are including SIMD support */


/* ----------------------------------------------------------------------------- */
/* Always provide all of the non-SIMD routines                                   */
/* ----------------------------------------------------------------------------- */

/* Return pointer to first match, else 'end'. */
const char *find_char (const uint8_t c,
		       const char *ptr, const char *end) {
  WHEN_DEBUGGING 
    if (!ptr || !end) {
      warn("find_char", "null required arg");
      return NULL;
    }
  return _find_char(c, ptr, end);
}

/* Return pointer to first mismatch, else 'end'. */
const char *span_char (const uint8_t c,
		       const char *ptr, const char *end) {
  for (; ptr < end; ptr++) {
    if (c != (uint8_t)*ptr) return ptr;
  }
  return end;
}

/* Return pointer to first match, else 'end'. */
const char *find_charset (const Charset *cs,
			  const char *ptr, const char *end) {
  WHEN_DEBUGGING
    if (!cs || !ptr || !end) {
      warn("find_charset", "null required arg");
      return NULL;
    }
  for (; ptr < end; ptr++) {
    if (testchar(cs->cs, (int)((uint8_t)*ptr))) return ptr;
  }
  return end;
}

/* Return pointer to first mismatch, else 'end'. */
const char *span_charset (const Charset *cs,
			  const char *ptr, const char *end) {
  WHEN_DEBUGGING
    if (!cs) {
      warn("span_charset", "null charset arg");
      return NULL;
    }
  for (; ptr < end; ptr++) {
    if (!testchar(cs->cs, (int)((uint8_t)*ptr))) return ptr;
  }
  return end;
}


/**
    Calculates the index to start using SIMD. Ensures that, when
    SIMD search is started, the first index loaded will be aligned
    to 16-bytes

    The result will be a number between 0 and 16. The returned value
    is the number of additional checks that need to be made before
    the address will be 16-byte aligned

    The function is commented out because the compiler was giving warnings about
    it (no prototype) and it isn't used at the moment.
 */

/*
const char* calculate_start_index(const char* c) {
    
    // This table does the same thing as (16 - last_four_bits) % 16, but doesn't require
    // a modulo operator or a conditional statement
    int lookup_table[] = { 0, 15, 14, 13, 12, 11, 10,  9,  8,  7,  6,  5,  4,  3,  2,  1 };

    int last_four_bits = (unsigned long)c & 0b1111;

    return c + lookup_table[last_four_bits];
}
*/

#define DYNAMIC_SIMD_MAX_TRIES 16
const char *find_dynamic_simd(const Charset *set, 
                const Charset *cs, const char *ptr,
                const char *end) {
  
  // Search character-by-character for 16 characters. If you find
  // a match, return early
  const char* max_ptr = ptr + DYNAMIC_SIMD_MAX_TRIES;
  for (; ptr < end && ptr < max_ptr; ptr++) {
    if (testchar(cs->cs, (int)((uint8_t)*ptr))) return ptr;    
  }
    
  // The below code ensures that ptr is aligned to 16-bytes before
  // calling the SIMD search function. It is commented out because
  // aligning to 16 bytes didn't seem to give any performance increase

  /*
  const char* SIMD_start = calculate_start_index(ptr);
  for (int i = 0; ptr < end && ptr < SIMD_start; ptr++, i++) {
    if (testchar(cs->cs, (int)((uint8_t)*ptr))) return ptr;
  }
  */

  // Check whether we exited the loop because we hit the end
  // or because it's time to start the SIMD search
  if (ptr == end) return end;
  return find_simd(set, cs, ptr, end);
}

const char *find_dynamic_simd8(const Charset *set,
                const Charset *cs, const char *ptr, 
                const char *end) {
  // Search character-by-character for 16 characters. If you find
  // a match, return early 
  const char* max_ptr = ptr + DYNAMIC_SIMD_MAX_TRIES;
  for (; ptr < end && ptr < max_ptr; ptr++) {
    if (testchar(cs->cs, (int)((uint8_t)*ptr))) return ptr;      
  }

  // The below code ensures that ptr is aligned to 16-bytes before
  // calling the SIMD search function. It is commented out because
  // aligning to 16 bytes didn't seem to give any performance increase

  /*
  const char* SIMD_start = calculate_start_index(ptr);
  for (; ptr < end && ptr < SIMD_start; ptr++) {
    if (testchar(cs->cs, (int)((uint8_t)*ptr))) return ptr;
  }
  */ 

  // Check whether we exited the loop because we hit the end
  // or because it's time to start the SIMD search
  if (ptr == end) return end;
  return find_simd8(set, cs, ptr, end);
}

const char *find_dynamic_simd1(const uint8_t c, 
                const char *ptr, const char *end) { 
  return _find_char(c, ptr, end);
}

