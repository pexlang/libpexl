/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  vm.h                                                                     */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */


#ifndef vm_h
#define vm_h

#include "preamble.h"
#include "libpexl.h"
#include "capture.h"

/*
  Note: Each capture uses two frames, so the capacities below must be
  set to twice the number of actual captures we want to support.
 */
#define CAPSTACK_INITIAL_CAPACITY 200
#define CAPSTACK_MAX_CAPACITY     (1 << 23)

/*
  A pattern like Rosie's all.things needs at most 12 backtrack stack
  frames in typical usage.  We allocate a larger initial capacity to
  avoid reallocations.
*/
#define BTSTACK_INITIAL_CAPACITY 200
#define BTSTACK_MAX_CAPACITY     (1 << 24)

struct pexl_Stats {
  unsigned int total_time;
  unsigned int match_time;
};

struct pexl_Match {
  void *data;               /* Varies with encoder_id */
  ssize_t end;              /* 0-based index of first byte after match (-1 no match) */
  pexlEncoderID encoder_id; /* See libpexl.h */
};

pexl_Match    *match_new (void);
void           match_free (pexl_Match *m);
pexlEncoderID  match_encoder (pexl_Match *m);

int vm_start (pexl_Binary *pkg,
	      pexl_PackageTable *pt,
	      pexl_Index entrypoint,
	      const char *input, size_t inputlen,
	      size_t startpos, size_t endpos,
	      uint32_t vm_flags,
	      /* in/out */
	      pexl_Stats *stats,
	      pexl_Match *match);

void print_vm_sizes (void);

#endif

