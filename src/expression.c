/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  expression.c                                                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings, Kayla Sanderson                              */

#include "expression.h"
#include "env.h"
#include "symboltable.h"
#include "instruction.h"
#include "pathtable.h"

/* ----------------------------------------------------------------------------- */
/* Utility functions for interrogating pexl_Ref structs                           */
/* ----------------------------------------------------------------------------- */

bool pexl_Ref_notfound (pexl_Ref r) {
  return r == PEXL_ENV__NOT_FOUND;
}

/* Could be not found or an error code */
bool pexl_Ref_invalid (pexl_Ref r) {
  return r < 0;
}

/* ----------------------------------------------------------------------------- */
/* pexl_Expr tree structures                                                      */
/* ----------------------------------------------------------------------------- */

static pexl_Expr *newexp (uint32_t len, int *err_ptr) {
  size_t size = (len - 1) * sizeof(Node) + sizeof(pexl_Expr);
  pexl_Expr *tree = (pexl_Expr *)malloc(size);
  if (!tree) {
    warn("newexp", "out of memory");
    *err_ptr = PEXL_ERR_OOM;
    return NULL;
  }
  tree->len = len;
  tree->depth = 1;
  return tree;
}

pexl_Expr *copy_expr (pexl_Expr *tree) {
  pexl_Expr *new;
  int err;
  if (!tree) return NULL;
  assert(tree->len > 0);
  new = newexp(tree->len, &err);
  if (!new) return NULL;
  new->depth = tree->depth;
  memcpy(new->node, tree->node, (new->len) * sizeof(Node));
  return new;
}

void free_expr (pexl_Expr *tree) {
  /* Do not free tree->env, because it is shared. */
  if (tree) free(tree);
}

/*
 * If 'tree' is a 'char' pattern (TSet, TAny, or TBytes of length 1),
 * convert it into a charset and return YES (True); else return NO
 * (False).
 */
int to_charset (Node *node, Charset *cs) {
  if (!node) {
    warn("to_charset", "null node arg");
    return NO; /* What would the caller do if we returned ERR? */
  }
  if (!cs) {
    warn("to_charset", "null charset arg");
    return NO; /* What would the caller do if we returned ERR? */
  }
  switch (node->tag) {
  case TSet: { /* copy set */
    charset_copy(&(cs->cs[0]), nodecharset(node));
    return YES;
  }
  case TBytes: {
    /* We have cached the length of the byte string in the node itself. */
    if (node->b.n == 1) {
      /* We have cached the first char of the byte string, too. */
      charset_fill_zero(cs->cs);  /* erase all chars */
      setchar(cs->cs, node->cap); /* add the first char of byte string */
      return YES;
    } else {
      return NO;
    }
  }
  case TAny: {
    charset_fill_one(cs->cs);	/* add all characters to the set */
    return YES;
  }
  default:
    return NO;
  }
}

static pexl_Expr *newleaf (int tag, int *err_ptr) {
  pexl_Expr *tree = newexp(1, err_ptr);
  if (!tree) return NULL;
  tree->node->tag = tag;
  tree->node->a.bt = NULL;
  return tree;
}

/*
 * Build a sequence of 'n' nodes, each with tag 'tag' and 'n' from
 * the array 's' (or 0 if array is NULL). (TSeq is binary, so it must
 * build a sequence of sequence of sequence...)
 */
static void fillseq (Node *node, int tag, int n, const char *s) {
  int i;
  for (i = 0; i < n - 1; i++) {
    /* initial n-1 copies of Seq tag; Seq ... */
    node->tag = TSeq;
    node->b.ps = 2;
    child1(node)->tag = tag;
    child1(node)->b.n = s ? (pexl_Byte)s[i] : 0;
    node = child2(node);
  }
  node->tag = tag; /* last one does not need TSeq */
  node->b.n = s ? (pexl_Byte)s[i] : 0;
}

/* ----------------------------------------------------------------------------- */
/* Create primitive patterns                                                     */
/* ----------------------------------------------------------------------------- */

/*
  The argument to 'from_bytes' can contain NUL characters, '\0'.  No
  particular encoding is assumed.  Arg can be ascii, utf-8, some other
  string encoding, or just binary data.

  Caller can pass NULL for ptr as synonym for the empty string.
*/

pexl_Expr *match_bytes (pexl_Index id, const char *ptr, size_t len, int *err) {
  assert(ptr || (len == 0));
  assert(len <= EXP_MAXSTRING);
  assert(((pexl_Index) len) >= 0);
  if (len == 0)
    return newleaf(TTrue, err); /* always match epsilon */

  pexl_Expr *tree = newleaf(TBytes, err);
  if (!tree) return NULL;	      /* error already set */
  tree->node->a.stringtab_handle = id;
  tree->node->b.n = (pexl_Index) len; /* cache the length */
  tree->node->cap = ptr[0];           /* cache the first byte */
  return tree;
}

/*
 * Numbers as patterns:
 *  0 == true (always match)
 *  n == TAny repeated 'n' times
 * -n == not (TAny repeated 'n' times)
 */
/*
  FUTURE: Maybe give TAny an integer argument that is the repeat
  count, instead of producing a tree that has n copies of TAny?
*/
pexl_Expr *match_any (int n, int *err) {
  pexl_Expr *tree;
  Node *node;
  if (n == 0) {
    return newleaf(TTrue, err);
  } else {
    if (n > 0) {
      tree = newexp(2 * n - 1, err);
      if (!tree)
        return NULL;
      node = tree->node;
    } else {
      /* negative: code it as !(-n) */
      n = -n;
      tree = newexp(2 * n, err);
      if (!tree)
        return NULL;
      node = tree->node;
      node->tag = TNot;
      node = child1(node);
    }
    fillseq(node, TAny, n, NULL); /* sequence of 'n' any's */
    return tree;
  }
}

pexl_Expr *fail_expr (int *err) {
  return newleaf(TFalse, err);
}

static pexl_Expr *newexp_Tset (int *err_ptr) {
  pexl_Expr *tree = newexp(bytes2slots(CHARSETSIZE) + 1, err_ptr);
  if (!tree) return NULL;
  tree->node->tag = TSet;
  charset_fill_zero(treecharset(tree));
  return tree;
}

/*
   Note: The argument string is interpreted as a set of bytes, not
   characters in some encoding.
*/
pexl_Expr *match_set (const char *set, size_t len, bool complement, int *err) {
  pexl_Expr *tree;
  assert(set);
  tree = newexp_Tset(err);
  if (!tree) return NULL;
  while (len--) {
    setchar(treecharset(tree), (pexl_Byte)(*set));
    set++;
  }
  if (complement) charset_not(treecharset(tree));
  return tree;
}

/* 
   FUTURE: Optimize a one-character range into a TChar node.

   FUTURE: Optimize range execution by adding a new tree type TRange,
   with a corresponding instruction.  Steve Poole (IBM) suggests it
   will be faster, based on his JIT experiments.
 */
pexl_Expr *match_range (pexl_Byte from, pexl_Byte to, int *err) {
  assert(from <= to);
  pexl_Expr *tree;
  int c;
  tree = newexp_Tset(err);
  if (!tree) return NULL;
  for (c = from; c <= to; c++)
    setchar(treecharset(tree), (pexl_Byte)c);
  return tree;
}

/* Create a new tree, with a new root and 2 siblings (exp1, exp2). */
static pexl_Expr *newroot2sib (int tag,
                               pexl_Expr *exp1,
                               pexl_Expr *exp2,
                               int *err_ptr) {
  if ((exp1->depth >= PEXL_MAX_EXPDEPTH) ||
      (exp2->depth >= PEXL_MAX_EXPDEPTH)) {
    warn("expression",
           "requested expression exceeds maximum depth");
    *err_ptr = PEXL_EXP__ERR_DEPTH;
    return NULL;
  }
  int s1 = exp1->len;
  int s2 = exp2->len;
  pexl_Expr *tree = newexp(1 + s1 + s2, err_ptr);
  Node *node;
  if (!tree) return NULL;
  node = tree->node;
  node->tag = tag;
  node->a.bt = NULL;
  node->b.ps = 1 + s1;
  tree->depth = (exp1->depth > exp2->depth) ? exp1->depth + 1 : exp2->depth + 1;
  memcpy(child1(node), exp1->node, s1 * sizeof(Node));
  memcpy(child2(node), exp2->node, s2 * sizeof(Node));
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Sequence                                                                      */
/* ----------------------------------------------------------------------------- */

/*
 * Sequence optimizations:
 * false x => false
 * x true => x
 * true x => x
 *
 * Cannot convert 'x false => false' because x may have runtime captures.

 * Runtime captures are not currently supported, but are a potential
 * enhancement.  If we decide against this enhancement, we should add
 * the optimization above.
 *
 */
pexl_Expr *seq (pexl_Expr *exp1, pexl_Expr *exp2, int *err) {
  assert(exp1 && exp2);
  pexl_Expr *tree;
  if (exp1->node->tag == TFalse || exp2->node->tag == TTrue)
    tree = copy_expr(exp1); /* false x => false;  x true => x */
  else if (exp1->node->tag == TTrue)
    tree = copy_expr(exp2); /* true x => x */
  else
    tree = newroot2sib(TSeq, exp1, exp2, err);
  if (!tree) return NULL;
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* Choice                                                                        */
/* ----------------------------------------------------------------------------- */

/*
  Optimizations:
    charset / charset => charset
    true / x => true
    x / false => x
    false / x => x
 
  Note that 'x / true' is not equivalent to 'true' in PEG semantics.
 
*/
pexl_Expr *choice (pexl_Expr *exp1, pexl_Expr *exp2, int *err) {
  assert(exp1 && exp2);
  pexl_Expr *tree;
  Charset st1, st2;
  if (to_charset(exp1->node, &st1) && to_charset(exp2->node, &st2)) {
    tree = newexp_Tset(err);
    if (!tree) return NULL;
    charset_or(treecharset(tree), st1.cs, st2.cs);
    return tree;
  }

  /* TODO: Reintroduce Choice optimizations in (future) tree optimization stage.

     - Need to introduce an explicit stage for doing tree optimizations
       AFTER we have fixed all the open calls.

     - Re-introduce the optimization below that checks for nofail() in
       that tree optimization stage.

     - Consider each of the charset optimizations, to see which of them
       would be best done during tree optimization (working bottom up).

     - If we want to remove pexle_subtract() from the API, we could
       retain its charset optimization by looking for 'Seq(Not t1, t2)'
       during the tree optimization phase: If t1 and t2 are charsets,
       merge them into a Tset tree using their set difference t2-t1.
  */

  /*   res = nofail(t1->node, env); */
  /*   if (res == ERR) return NULL; */
  /*   if (res || t2->node->tag == TFalse) */
  /*     return copy_expr(t1);       /\* true / x => true, x / false => x *\/  */
  /*   if (t1->node->tag == TFalse) */
  /*     return copy_expr(t2);       /\* false / x => x *\/  */
  /* else */

  /* true / x => true, x / false => x */
  if ((exp1->node->tag == TTrue) || (exp2->node->tag == TFalse))
    return copy_expr(exp1);
  /* false / x => x */
  if (exp1->node->tag == TFalse)
    return copy_expr(exp2); 

  return newroot2sib(TChoice, exp1, exp2, err);
}

/* ----------------------------------------------------------------------------- */
/* Not                                                                           */
/* ----------------------------------------------------------------------------- */

/*
 * Create a new tree, with a new root and one sibling.
 */
static pexl_Expr *newroot1sib (int tag,
				pexl_Expr *sibling,
				int *err_ptr) {
  if (sibling->depth >= PEXL_MAX_EXPDEPTH) {
    warn("expression",
           "requested expression exceeds maximum depth");
    *err_ptr = PEXL_EXP__ERR_DEPTH;
    return NULL;
  }
  pexl_Expr *tree = newexp(1 + sibling->len, err_ptr);
  if (!tree) return NULL;
  tree->node->tag = tag;
  tree->node->a.bt = NULL;
  tree->depth = sibling->depth + 1;
  memcpy(child1(tree->node), sibling->node, sibling->len * sizeof(Node));
  return tree;
}

/* Not (!p in the original PEG presentation) */
pexl_Expr *neg_lookahead (pexl_Expr *exp, int *err) {
  assert(exp);
  return newroot1sib(TNot, exp, err);
}

/* ----------------------------------------------------------------------------- */
/* Unbounded lookahead                                                           */
/* ----------------------------------------------------------------------------- */

/* Lookahead (&p in the original PEG presentation) */
pexl_Expr *lookahead (pexl_Expr *exp, int *err) {
  assert(exp);
  return newroot1sib(TAhead, exp, err);
}

/* ----------------------------------------------------------------------------- */
/* Unbounded lookbehind                                                          */
/* ----------------------------------------------------------------------------- */

pexl_Expr *lookbehind (pexl_Expr *exp, int *err) {
  assert(exp);
  return newroot1sib(TBehind, exp, err);
}

/* ----------------------------------------------------------------------------- */
/* Repetition                                                                    */
/* ----------------------------------------------------------------------------- */

/*
  Add to tree a sequence where first child is 'sib' (with size
  'sibsize').  Return position for second child.
*/
static Node *seqaux (Node *node, Node *sib, int sibsize) {
  node->tag = TSeq;
  node->b.ps = sibsize + 1;
  memcpy(child1(node), sib, sibsize * sizeof(Node));
  return child2(node);
}

pexl_Expr *repeat (pexl_Expr *exp, 
		   pexl_Index min, pexl_Index max,
		   int *err) {
  assert(exp);
  assert((min >= 0) && (min <= MAX_REPETITIONS));
  assert((max >= 0) && (max <= MAX_REPETITIONS));
  assert((min <= max) || (max == 0));
  if (min == 0) {
    if (max == 0) {
      // Zero or more repetitions of 'exp': exp*
      return atleast(exp, 0, err);
    } else {
      // At most max repetitions of 'exp': exp{0,max}
      return atmost(exp, max, err);
    }
  } else {
    if (max == 0) {
      // Match at least min repetitions: exp{min,inf}
      return atleast(exp, min, err);
    } else {
      // Match min repetitions, then up to max-min: exp{min,max}
      // Build: seq exp (seq exp ... (atmost exp))
      pexl_Expr *ntimes_exp = ntimes(exp, min, err);
      if (min == max) return ntimes_exp;
      pexl_Expr *atmost_exp = atmost(exp, max-min, err);
      pexl_Expr *finalexp = seq(ntimes_exp, atmost_exp, err);
      free_expr(ntimes_exp);
      free_expr(atmost_exp);
      return finalexp;
    }
  }
}

pexl_Expr *ntimes (pexl_Expr *exp, pexl_Index n, int *err) {
  assert(exp);
  assert((n > 0) && (n <= MAX_REPETITIONS));
  Node *node;
  uint32_t size = exp->len;
  /* Build: seq exp (seq exp ... (seq exp exp)) */
  pexl_Expr *ntimes_exp = newexp(n * (size + 1), err);
  if (!ntimes_exp) return NULL;
  node = ntimes_exp->node;
  n--;
  while (n--)
    node = seqaux(node, exp->node, size);
  memcpy(node, exp->node, size * sizeof(Node));
  return ntimes_exp;
}

pexl_Expr *atleast (pexl_Expr *exp, pexl_Index n, int *err) {
  assert(exp);
  assert((n >= 0) && (n <= MAX_REPETITIONS));
  Node *node;
  uint32_t size = exp->len;
  /* Build: seq exp (seq exp ... (seq exp (rep exp))) */
  pexl_Expr *atleast_exp = newexp((n + 1) * (size + 1), err);
  if (!atleast_exp) return NULL;
  node = atleast_exp->node;
  while (n--)
    node = seqaux(node, exp->node, size);
  node->tag = TRep;
  memcpy(child1(node), exp->node, size * sizeof(Node));
  return atleast_exp;
}

pexl_Expr *atmost (pexl_Expr *exp, pexl_Index n, int *err) {
  /* choice(seq exp ... choice exp true ...) true */
  /* size = (choice + seq + exp + true) * n, but the last has no seq */
  assert(exp);
  assert((n > 0) && (n <= MAX_REPETITIONS));
  Node *node;
  uint32_t size = exp->len;
  pexl_Expr *atmost_exp = newexp(n * (size + 3) - 1, err);
  if (!atmost_exp) return NULL;
  node = atmost_exp->node;
  for (; n > 1; n--) {
    /* repeat (n - 1) times */
    node->tag = TChoice;
    node->b.ps = n * (size + 3) - 2;
    child2(node)->tag = TTrue;
    node = child1(node);
    node = seqaux(node, exp->node, size);
  }
  node->tag = TChoice;
  node->b.ps = size + 1;
  child2(node)->tag = TTrue;
  memcpy(child1(node), exp->node, size * sizeof(Node));
  return atmost_exp;
}

/* ----------------------------------------------------------------------------- */
/* Capture                                                                       */
/* ----------------------------------------------------------------------------- */

pexl_Expr *capture (pexl_Index handle, pexl_Expr *exp, int *err) {
  assert(exp);
  pexl_Expr *tree = newroot1sib(TCapture, exp, err);
  if (!tree) return NULL;
  tree->node->cap = Copen;
  tree->node->b.n = handle;
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* PEXL call - call a pattern by pexl_Ref                                             */
/* ----------------------------------------------------------------------------- */

/* A TOpenCall expression converts to TCall during binding */
pexl_Expr *call (pexl_Ref ref, BindingTable *bt, int *err) {
  assert(bt);
  // The expression code did not know about environments or bindings
  // until now, when we added this validity test for the ref arg.
  Binding *b = env_get_binding(bt, ref);
  if (!b) {
    warn("pexl_call", "no value bound at ref %d", ref);
    *err = PEXL_EXP__NOT_FOUND;
    return NULL;
  }
  pexl_Expr *tree = newleaf(TOpenCall, err);
  if (!tree) return NULL;	/* newleaf already set the error code */
  tree->node->a.bt = bt;
  set_node_ref(tree->node, ref);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* PEXL external call - call a pattern in another package                        */
/* ----------------------------------------------------------------------------- */

pexl_Expr *xcall (pexl_Binary *pkg, pexl_Index symbol, int *err) {
  assert(pkg);
  pexl_Expr *tree = newleaf(TXCall, err);
  if (!tree) return NULL;
  set_node_xref(tree->node, pkg, symbol);
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* "Constant capture" consumes no input but captures a predetermined string      */
/* ----------------------------------------------------------------------------- */

pexl_Expr *insert (pexl_Index name_handle, pexl_Index value_handle, int *err) {
  pexl_Expr *tree, *temp;
  temp = newleaf(TTrue, err);
  if (!temp) return NULL;
  tree = newroot1sib(TCapture, temp, err);
  free_expr(temp);   /* because newroot1sib makes a copy of its arg */
  if (!tree) return NULL;
  tree->node->cap = Cconstant;
  tree->node->a.stringtab_handle = value_handle;
  tree->node->b.n = name_handle;
  return tree;
}

/* ----------------------------------------------------------------------------- */
/* PEXL Find                                                                     */
/* ----------------------------------------------------------------------------- */
/**
 * @brief the pexl_find( * pexl_Context, * pexl_Expr ) function is responsible for
 * tagging Expresion nodes in the pexl_Expr tree as TFind nodes. The function
 * accepts 2 arguments, a pexl_Context pointer and an pexl_Expr pointer.  The function
 * fails if the pexl_Context parameter is NULL, the pexl_Expr parameter is NULL or the
 * pexl_Expr tree could not be updated due to some error.  If the function is successful
 * the pexl_Expr tree is updated with a TFind node and a pointer to this new tree is
 * returned to the caller.
 *
 * @param C a pointer to a pexl_Context struct.  The context must not be NULL.
 * @param exp a pointer to an pexl_Expr to add to tag and add to the pexl_Expr tree.
 * the pexl_Expr pointer exp cannot be NULL.
 * @return the pexl_find() function returns a pointer to a new pexl_Expr tree
 * with the tagged node added to the tree.  The pexl_find() function returns NULL if the
 * pexl_Context pointer, pexl_Expr pointer exp, or the new pexl_Expr tree failed to be updated.
 */

pexl_Expr *find (pexl_Expr *exp, int *err) {
  assert(exp);
  return newroot1sib(TFind, exp, err);
}

/* ----------------------------------------------------------------------------- */
/* Rosie/PEXL backreference                                                      */
/* ----------------------------------------------------------------------------- */

/* TODO: Re-code this using TFunction (store a "primitive function
   number" in the TFunction node) */

/*
  The Rosie/PEXL backreferences are different from lpeg
  backreferences.  Arguments:

  Capture name (used at runtime).
  
  FUTURE: Add an arg that is a symbol table entry for a pattern
  (pexl_Ref).  When compiling, we should examine the pattern at Ref to
  ensure that it captures the backref arg (capture name).  This will
  require recursive descent through patterns called by the one at Ref.
  We will have to take care to notice recursion here.  BENEFIT: The
  ability to produce a list of captures that could be seen in the
  parse tree for a pattern will have other uses.  It could be used,
  e.g. to produce a schema of sorts.

*/

pexl_Expr *backreference (pexl_Index handle, int *err) {
  pexl_Expr *tree = newleaf(TBackref, err); 
  if(!tree) return NULL;
  tree->node->a.stringtab_handle=handle;
  return tree;
 }

/* ----------------------------------------------------------------------------- */
/* Where                                                                          */
/* ----------------------------------------------------------------------------- */

pexl_Expr *where (pexl_Expr *exp1,
		  char *pathstring,
		  pexl_Expr *exp2,
		  PathTable *pathtab,
		  int *err) {
  assert(exp1 && pathstring && exp2);
  assert(pathtab);
  if (exp2->node->tag!=TBytes) {
    confess("where", "TEMPORARILY only supporting string comparisons");
    *err = PEXL_ERR_INTERNAL;
    return NULL;
  }
  Path *path = break_path(pathstring);
  if (!path) {
    inform("where", "invalid path string '%s'", pathstring);
    *err = PEXL_EXP__ERR_PATH;
    return NULL;
  }
  pexl_Index pindex = pathtable_add(pathtab, path);
  if (pindex < 0) {
    warn("where", "failed to add path to path table");
    *err = PEXL_ERR_INTERNAL;
    return NULL;
  }
  /*
    where(path)
    / \
    net.url "www.google.com"
  */   
  pexl_Expr *where = newroot2sib(TWhere, exp1, exp2, err);
  if (!where) {
    path_free(path);
    return NULL;
  }
  where->node->a.path_handle = pindex;
  return where;
}

