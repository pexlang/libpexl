/*  -*- Mode: C; -*-                                                        */
/*                                                                          */
/*  table.c   Dynamically expanding tables of records                       */
/*                                                                          */
/*  See files LICENSE and COPYRIGHT, which must accompany this file         */
/*  AUTHORS: Jamie A. Jennings                                              */

/*

  The table implementation is low-level.  It is not thread-safe and
  does minimal error checking.  When calling these functions, be sure
  to:
  - Never pass NULL for a table or other pointer parameter
  - Use returned pointers as instructed below

  We can implement thread-safe tables later, if needed.

  WARNING: NOT THREAD SAFE.  E.g. Table_add() returns a pointer that
  is valid only until the next call to Table_add(), which may trigger
  realloc() and change the addresses of all the table entries.
  
*/

#include "table.h"

// The 'malloc' attribute enables the C compiler to do some
// optimizations that depend on this fact: The pointer returned does
// not alias other active pointers.
__attribute__((malloc, alloc_size(1, 3)))
Table *Table_new(pexl_Index initial_capacity,
		 pexl_Index max_capacity,
		 size_t     record_size,
		 uint16_t   flags) {

  if ((initial_capacity <= 0) ||
      (max_capacity < 0) ||
      (record_size == 0) ||
      ((max_capacity != 0) && (max_capacity < initial_capacity))) {
    warn("table", "invalid arg to Table_new");
    return NULL;
  }
  Table *tbl = malloc(sizeof(Table));
  if (!tbl) {
  oom:
    warn("table", "out of memory");
    if (HAS_BITFLAG(flags, TABLE_SOFTFAIL)) return NULL;
    abort();
  }
  tbl->records = malloc(initial_capacity * record_size);
  if (!tbl->records) {
    free(tbl);
    goto oom;
  }
  tbl->next = 0;
  tbl->capacity = initial_capacity;
  tbl->max_capacity = max_capacity; // Zero == no limit
  tbl->record_size = record_size;
  tbl->flags = flags;
  return tbl;
}

void Table_free(Table *tbl) {
  if (tbl) {
    if (tbl->records) free(tbl->records);
    free(tbl);
  }
  return;
}

/*
  NOTE: The pointer returned is valid only until the next call to
  Table_add().  Proper usage looks like this:

    pexl_Index n = Table_add(tbl, &ptr);
    ptr->field1 = value1;
    ptr->field2 = value;
    // Do not store 'ptr' anywhere or use it further.
    return n;
*/
pexl_Index Table_add(Table *tbl, void **record) {
  assert(tbl && tbl->records && record);
  assert((tbl->next >= 0) && (tbl->capacity > 0));
  assert(tbl->next <= tbl->capacity);
  if (tbl->next == tbl->capacity) {
    if (tbl->max_capacity && (tbl->capacity >= tbl->max_capacity)) {
      warn("table", "table full at capacity %" pexl_Index_FMT,
	   tbl->max_capacity);
      return PEXL_ERR_FULL;
    }
    size_t newcap = 2 * tbl->capacity;
    if (tbl->max_capacity &&
	(newcap > (size_t) tbl->max_capacity)) newcap = tbl->max_capacity;
    void *temp = realloc(tbl->records, newcap * tbl->record_size);
    if (!temp) {
      warn("table", "out of memory");
      if (HAS_BITFLAG(tbl->flags, TABLE_SOFTFAIL)) return PEXL_ERR_OOM;
      abort();
    }
    tbl->records = temp;
    tbl->capacity = newcap;
    inform("table", "extended table to capacity %" pexl_Index_FMT
	   " records (%" pexl_Index_FMT " currently in use)",
	   tbl->capacity, tbl->next);
  }
  pexl_Index index = tbl->next++;
  *record = tbl->records + (index * tbl->record_size);
  return index;
}

/*
  NOTE: The pointer returned is valid only until the next call to
  Table_add().  Proper usage looks like this:

    my_struct *s = (my_struct) Table_get(tbl, index);
    var1 = s->field1;
    var2 = s->field2;
    // Do not store 's' anywhere or use it further.
*/
void *Table_get(Table *tbl, pexl_Index i) {
  assert(tbl);
  if ((i < 0) || (i >= tbl->next)) return NULL;
  return tbl->records + (i * tbl->record_size);
}

pexl_Index Table_set_size(Table *tbl, pexl_Index value) {
  if ((value < 0) || (value > tbl->capacity)) return PEXL_NOT_FOUND;
  tbl->next = value;
  return value;
}

/*
  On the first call, supply PEXL_ITER_START for '*prev'.  Same
  restrictions on use of returned pointer as for Table_get().
 */
void *Table_iter (Table *tbl, pexl_Index *prev) {
  assert(tbl && prev);
  return Table_get(tbl, ++(*prev));
}

pexl_Index Table_size(Table *tbl) {
  assert(tbl);
  return tbl->next;
}

pexl_Index Table_capacity(Table *tbl) {
  assert(tbl);
  return tbl->capacity;
}

/* ----------------------------------------------------------------------------- */
/* Stack interface                                                               */
/* ----------------------------------------------------------------------------- */

pexl_Index Table_empty(Table *tbl) {
  return (Table_size(tbl) == 0);
}
  
// Synonym, for when we use the table like a stack
pexl_Index Table_push(Table *tbl, void **record) {
  return Table_add(tbl, record);
}

// NOTE: Returned pointer is only valid until the next Table_add().
void *Table_pop(Table *tbl) {
  if (tbl->next < 1) return NULL;
  tbl->next--;
  return tbl->records + (tbl->next * tbl->record_size);
}

// NOTE: Returned pointer is only valid until the next Table_add().
void *Table_top(Table *tbl) {
  if (tbl->next < 1) return NULL;
  return tbl->records + ((tbl->next - 1) * tbl->record_size);
}

