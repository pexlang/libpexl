/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  sexp.h   S-expressions                                                   */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef sexp_h
#define sexp_h

#include "preamble.h"
#include "pstring.h"
#include "k.h"
#include "libpexl.h"

// Configurable:
// 
//   Max length, in bytes, of an identifier, an integer, and a
//   bytestring.  Note that the bytestring length includes the opening
//   and closing quote characters.
//
#define MAX_IDLEN PEXL_MAX_IDLEN
#define MAX_INT_LEN 20
#define MAX_BYTES_LEN 1023

// Need to limit the size of how much we are willing to append (in one
// step) onto an existing buffer.  To append a string longer than this
// limit, you must call StringBuffer_add* repeatedly.
//
// This limit is quite arbitrary.
#define STRING_APPEND_LIMIT (UINT32_MAX >> 8)

/* ----------------------------------------------------------------------------- */
/* Validation tests on configuration                                             */
/* ----------------------------------------------------------------------------- */
#if (MAX_IDLEN > UINT32_MAX)
#error MAX_IDLEN too large
#endif
#if (MAX_BYTES_LEN > UINT32_MAX)
#error MAX_BYTES_LEN too large
#endif
#if (MAX_INT_LEN > UINT32_MAX)
#error MAX_INT_LEN too large
#endif
#if (STRING_APPEND_LIMIT > UINT32_MAX)
#error STRING_APPEND_LIMIT too large
#endif

PREDICATE_DECL(delimiterp);
PREDICATE_DECL(id_startp); 
PREDICATE_DECL(id_charp); 
PREDICATE_DECL(sharpp);
PREDICATE_DECL(quotep);
PREDICATE_DECL(openp);
PREDICATE_DECL(closep);
PREDICATE_DECL(commentp);
PREDICATE_DECL(minusp);
PREDICATE_DECL(plusp);

#define _STRINGIFY(arg) #arg
#define STRINGIFY(arg) _STRINGIFY(arg)

/* ----------------------------------------------------------------------------- */
/* Tokens                                                                        */
/*   Reader: Input is consumed one token at a time.                              */
/*   Writer: The SEXP_ERROR type contains a token specifying the error           */
/* ----------------------------------------------------------------------------- */

// TOKEN_PANIC indicates a bug in our code, and is not recoverable,
// unlike TOKEN_BAD_*, which indicates bad user input.

#define _TOKENS(X)                                              \
       X(TOKEN_OPEN,           "OPEN")			        \
       X(TOKEN_CLOSE,          "CLOSE")			        \
       X(TOKEN_COMMENT,        "COMMENT")                       \
       X(TOKEN_WS,             "WS")                            \
       X(TOKEN_IDENTIFIER,     "ID")                            \
       X(TOKEN_INT,            "INT")                           \
       X(TOKEN_BYTESTRING,      "BYTESTRING")		        \
       X(TOKEN_CHAR,           "CHAR")   		        \
       X(TOKEN_EOF,            "EOF")				\
       /* Error types: (List must start with PANIC.) */		\
       X(TOKEN_PANIC,          "PANIC")				\
       X(TOKEN_BAD_IDENTIFIER, "INVALID_IDENTIFIER")		\
       X(TOKEN_BAD_BYTESTRING, "UNTERMINATED_STRING")		\
       X(TOKEN_BAD_LEN,        "BYTESTRING_TOO_LONG")		\
       X(TOKEN_BAD_ESC,        "INVALID_BYTESTRING_ESCAPE_SEQ")	\
       X(TOKEN_BAD_SHARP,      "INVALID_SHARP_EXPRESSION")	\
       X(TOKEN_BAD_INT,        "INVALID_INT")			\
       X(TOKEN_BAD_CHAR,       "INVALID_CHAR")			\
       X(TOKEN_NTOKENS,        "SENTINEL")

#define _FIRST(a, b) a,
typedef enum Token_type {_TOKENS(_FIRST)} Token_type;
#undef _FIRST

#define _SECOND(a, b) b,
static const char *const TOKEN_NAMES[] = {_TOKENS(_SECOND)};
#undef _SECOND

#define token_type_name(type) \
  (((0 <= (type)) && (type) < TOKEN_NTOKENS) ? TOKEN_NAMES[(type)] : "INVALID")

typedef struct Token {
  enum Token_type type;
  uint32_t len;
  const string *start;
} Token;

// Obtaining the panic token indicates a bug in our lexer/parser.  It
// means "this should not happen".
__attribute__((unused)) static
Token panicToken = (Token) {.type = TOKEN_PANIC};

// A Byte is specified in hex as #\xHH or using one of the names
// below, e.g. #\space.  IMPORTANT: No name starts with 'x'.
__attribute__((unused)) static const
char  *character_names[] = {"space", "tab", "newline", "return", "nul", "null", NULL};
__attribute__((unused)) static const
size_t character_name_lengths[] = {5, 3,  7,  6, 3, 4, 0};
__attribute__((unused)) static const
size_t character_name_values[] = {32, 9, 10, 13, 0, 0, 0};

/* ----------------------------------------------------------------------------- */
/* S-expression definitions                                                      */
/* ----------------------------------------------------------------------------- */

#define _SEXPS(X)					   \
       X(SEXP_TRUE,         "TRUE")                        \
       X(SEXP_FALSE,        "FALSE")                       \
       X(SEXP_IDENTIFIER,   "IDENTIFIER")                  \
       X(SEXP_INT,          "INT")                         \
       X(SEXP_CHAR,         "CHAR")                        \
       X(SEXP_BYTESTRING,   "BYTESTRING")		   \
       /* Declare all valid atoms above this line. */      \
       X(SEXP_NATOMS,       "SENTINEL")			   \
       /* Below: NULL, CONS, and various error types. */   \
       X(SEXP_NULL,         "NULL")			   \
       X(SEXP_CONS,         "CONS")                        \
       X(SEXP_INCOMPLETE,   "INCOMPLETE")                  \
       X(SEXP_EXTRACLOSE,   "EXTRACLOSE")                  \
       X(SEXP_ERROR,        "ERROR")                       \
       X(SEXP_NTYPES,       "SENTINEL")
       
#define _FIRST(a, b) a,
// Each Sexp_type must be in [0, SEXP_NTYPES) with no gaps in the
// numbering, because the code in sexp-ast.c relies on this. 
typedef enum Sexp_type {_SEXPS(_FIRST)} Sexp_type;
#undef _FIRST

#define _SECOND(a, b) b,
static const char *const SEXP_NAMES[] = {_SEXPS(_SECOND)};
#undef _SECOND

#define sexp_type_name(type) \
  (((0 <= (type)) && (type) < SEXP_NTYPES) ? SEXP_NAMES[(type)] : "INVALID")

// Individual fields are valid only for the indicated types
typedef struct Sexp {
  enum Sexp_type type;
  union {
    struct {			// For CONS
      struct Sexp *car;
      struct Sexp *cdr;
    };
    struct {			// For ERROR
      enum Token_type type;
      String *data;
    } error;
    String  *str;		// For ID, BYTESTRING
    int64_t  n;			// For INT
    uint32_t codepoint;		// For CHAR
  };
} Sexp;

/*
   NOTES about strings:

   (1) The string in TOKEN_BYTES contains the delimiting double
   quotes, and its contents are as written by the user, with escape
   sequences and no NUL bytes.

   (2) The string in SEXP_BYTESTRING does not contain the delimiting
   quotes, nor any escape sequences.  E.g. if the user writes "\n",
   the token struct will contain exactly that 4-byte string, but the
   conversion to SEXP_BYTESTRING produces a 1-byte string.  That one
   byte has numerical value 10 (ASCII for newline).

   (3) To print the contents of SEXP_BYTESTRING, we 'escape it',
   meaning we convert all bytes outside the printable ASCII range into
   a valid escape sequence.  (An escape sequence is valid if, when it
   is read, it produces the same byte value we started with.)  E.g. a
   string of one byte that is ASCII 10 could be printed as \n (2
   bytes) or \x0A (4 bytes).

   (4) Procedures for unescaping and escaping bytestrings are in the
   pstring package.

*/  

/* ----------------------------------------------------------------------------- */
/* S-expression operations                                                       */
/* ----------------------------------------------------------------------------- */

// The usual for lists: build 'em up, tear 'em down

Sexp *sexp_null(void);
Sexp *sexp_cons(Sexp *item, Sexp *list);
Sexp *sexp_car(Sexp *list);
Sexp *sexp_cdr(Sexp *list);
void  free_sexp(Sexp *e);

// S-expression predicates (ending in 'p' per Lisp tradition)

bool sexp_nullp(Sexp *s);
bool sexp_truep(Sexp *s);
bool sexp_falsep(Sexp *s);
bool sexp_consp(Sexp *s);
bool sexp_listp(Sexp *s);	// cons or null
bool sexp_atomp(Sexp *s);	// excludes the various error types
bool sexp_identifierp(Sexp *obj);
bool sexp_charp(Sexp *obj);
bool sexp_bytestringp(Sexp *obj);
bool sexp_intp(Sexp *obj);
bool sexp_errorp(Sexp *s);
bool sexp_extraclosep(Sexp *obj);
bool sexp_incompletep(Sexp *obj);
// The following predicates examine the *entire* list
bool sexp_proper_listp(Sexp *ls);
bool sexp_improper_listp(Sexp *ls);
bool sexp_incomplete_listp(Sexp *ls);

// Constructing S-expression structures

Sexp *sexp_null(void);
Sexp *sexp_true(void);
Sexp *sexp_false(void);
Sexp *sexp_int(const string *start, uint32_t len);
Sexp *sexp_codepoint(const string *start, uint32_t len);
Sexp *sexp_codepoint_from_value(pexl_Codepoint cp);
Sexp *sexp_bytestring(const string *start, uint32_t len);
Sexp *sexp_bytestring_from_literal(const string *s);
Sexp *sexp_identifier(const string *start, uint32_t len);
Sexp *sexp(enum Sexp_type type);

// Utilities

Sexp *sexp_deepcopy(Sexp *ls);
Sexp *sexp_nreverse(Sexp *ls);
int   sexp_length(Sexp *ls);

//typedef void *SexpMapper(Sexp *old, Sexp *new);
//Sexp *sexp_map(SexpMapper *fn, Sexp *ls);

// To 'reduce' a list is to apply a binary (2-arg) function repeatedly
// until all the values in the list have been consumed.  An initial
// value is combined with the first item in the list to produce a
// result.  That result is then combined with the next item in the
// list, and so on.  The function that combines 2 items is a 'reducer'
// and it has the type
//
//    fn(<R>, <T>) --> <R>
//
// where <R> is the result type and <T> is the type of items in the
// list.  Note that the initial value must have type <R>.
//
// This operation is sometimes called 'fold'.

typedef void *SexpReducer(void *acc, Sexp *s);

void *sexp_reduce(SexpReducer *fn, 
		  void *initial_value,
		  Sexp *ls);

// Error handling

// The 'data' return value is optional.  If supplied, then the caller
// must free it.  The primary return value is either a valid token
// type, or < 0 for error.
int   sexp_error_info(Sexp *s, String *data);

#endif
