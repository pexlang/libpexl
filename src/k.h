//  -*- Mode: C; -*-                                                       
// 
//  k.h     Defunctionalized continuation support
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

#ifndef k_h
#define k_h

#include "preamble.h"
#include "table.h"

/*
  'k' is the conventional variable name for a continuation.

  USAGE:

  (1) Define a type name for the data to be stored in a continuation, e.g.

      typedef struct kdata {
	Sexp *retval;
	const string **sptr;
	Token tok;
      } kdata;

  (2) Declare type of continuation data, and names for the various
      types, constructors, getters, setters, etc.  For example:

      KDATATYPE(kdata, newstack, push, data, setdata);

      This macro also defines a static function of type:
        void K_push(Kontinuation *k, kdata d);

  (3) Declare the names of the continuation types you'll use, e.g.

      KTYPES(K_READ_VALUE,	     // Have a return value
	     K_READ_OPEN,	     // Build a list
	     K_FREE);

*/

/* ----------------------------------------------------------------------------- */
/* Configurable items                                                            */
/* ----------------------------------------------------------------------------- */

#define KSTACK_INIT 2000

/* ----------------------------------------------------------------------------- */

#define KTYPES(...) enum {__VA_ARGS__}

#define KDATATYPE(type, make_stack, pusher, getter, setter)	\
  __attribute__((unused))					\
  static type getter(Kontinuation *k) {				\
    type data;							\
    memcpy(&data, &((k)->data), sizeof(type));			\
    return data;						\
  }								\
  __attribute__((unused))					\
  static void setter(Kontinuation *k, type data) {		\
    memcpy(&((k)->data), &(data), sizeof(type));		\
  }								\
  static void pusher(Kstack *ks, int ktype, type data) {	\
    K_push_internal((ks), (ktype), &(data));			\
  }								\
  static Kstack *make_stack(Kfree_fn free_data) {		\
    return Kstack_new_internal(sizeof(type), (free_data));	\
  }

typedef struct Kontinuation {
  int  type;
  char data[1];
} Kontinuation;

typedef void (Kfree_fn)(Kontinuation *);

typedef struct Kstack {
  void     *(*new_data)(void);
  Kfree_fn *free_data;
  size_t    data_size;
  Table    *ks;
} Kstack;

Kstack *Kstack_new_internal(size_t data_size, Kfree_fn *free_k);
void    Kstack_free(Kstack *ks);

Kontinuation *K_top(Kstack *ks);
Kontinuation *K_pop(Kstack *ks);
void          K_push_internal(Kstack *ks, int type, void *data);
void          K_replace_data(Kstack *kstk, Kontinuation *k, void *newdata);

// Handle out of memory situations (rather dramatically)
void K_oom(void);

// For debugging.  The print function arg is optional.
void Kstack_print(Kstack *kstk, void print_data(void *));

#endif

