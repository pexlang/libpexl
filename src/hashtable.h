/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  hashtable.h     A hash table that interns its string keys                */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef hashtable_h
#define hashtable_h

#include "preamble.h"
#include "libpexl.h"
#include "table.h"
#include "pstring.h"

// Parameters that can be tuned for performance

#define HASHTABLE_EXPAND_LOAD 23  // Table will expand at this load factor (%)
#define HASHTABLE_MAX_LOAD 28     // Maximum allowed load factor (%)
#define HASHTABLE_MIN_ENTRIES 50  // Minimum "expected entries" value
#define HASHTABLE_AVG_LEN 9	  // Used to estimate string storage needed

/* ----------------------------------------------------------------------------- */
/* Constants that should not be changed without careful review                   */
/* ----------------------------------------------------------------------------- */

// Max key length in bytes; must be < 2^31 
#if (PEXL_MAX_STRINGLEN >= (1 << 31))
   #error "PEXL_MAX_STRINGLEN is too high for hashtable implementation"
#endif
#define HASHTABLE_MAX_KEYLEN    PEXL_MAX_STRINGLEN 

// Number of hashtable slots must be < 2^31 to fit into int32_t
#define HASHTABLE_MAX_SLOTS     (1 << 22)

// The string storage "block" must be < 2^31 so that any string
// "handle" (its position in the block) will fit into an int32_t
#define HASHTABLE_MAX_BLOCKSIZE (1 << 26)

/* ----------------------------------------------------------------------------- */
/* Data types                                                                    */
/* ----------------------------------------------------------------------------- */

typedef union HashTableData {
  void *ptr;			/* .data.ptr */
  struct int32 {
    int32_t a;			/* .data.int32.a */
    int32_t b;			/* .data.int32.b */
  } int32;
  char chars[8];		/* .data.chars[] */
} HashTableData;

typedef struct HashTableEntry {
  pexl_Index    block_offset; /* offset into block storage; < 0 if UNUSED or ERROR */
  pexl_Index    key_length;   /* number of bytes in the string key */
  HashTableData data;	      /* value (payload) */
} HashTableEntry;

typedef struct HashTable {
  Table        *entries;      // Array 
  StringBuffer *block;	      // Block storage for strings
  pexl_Index    count;        // Number of entries in use >= 0
} HashTable;

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

HashTable      *hashtable_new (size_t exp_entries, size_t exp_string_size);
void            hashtable_free (HashTable *ht);

pexl_Index      hashtable_add (HashTable *ht,
			       const char *key, int32_t len,
			       HashTableData data);
pexl_Index      hashtable_add_update (HashTable *ht,
				      const char *key, int32_t len,
				      HashTableData data);
bool            hashtable_error (HashTableEntry entry);
bool            hashtable_notfound (HashTableEntry entry);
HashTableEntry  hashtable_search (HashTable *ht, const char *key, int32_t len);
HashTableEntry  hashtable_iter (HashTable *ht, int32_t *prev);

HashTableEntry  hashtable_get (HashTable *ht, pexl_Index i);
const char     *hashtable_get_key (HashTable *ht, int32_t keyhandle, int32_t *lenptr);
pexl_Index      hashtable_get_keyhandle (HashTableEntry entry);

/* ----------------------------------------------------------------------------- */
/* Error and other codes                                                         */
/* ----------------------------------------------------------------------------- */

#define HASHTABLE_NOT_FOUND     -1  /* must be < 0 */
#define HASHTABLE_ERR_OOM       -2  /* out of memory */
#define HASHTABLE_ERR_KEYLEN    -3  /* max key length exceeded, or length < 0 */
#define HASHTABLE_ERR_NULL      -4  /* name is NULL */ 
#define HASHTABLE_ERR_BLOCKFULL -5  /* reached limit of HASHTABLE_MAX_BLOCKSIZE bytes */ 
#define HASHTABLE_ERR_FULL      -6  /* reached limit of HASHTABLE_MAX_SLOTS */ 
#define HASHTABLE_ERR_INTERNAL  -7  /* this is a bug */ 
#define HASHTABLE_ERR_FOUND     -8  /* entry already exists */

#endif
