/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  print.c                                                                  */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "print.h"
#include <ctype.h>
#include "bitflags.h"

#define INDENT(amt) {	    \
    printf("%*s", amt, ""); \
  } while (0);

/* ----------------------------------------------------------------------------- */
/* Print env, package table, patterns                                            */
/* ----------------------------------------------------------------------------- */

/* Number of chars to use for a length string:
   Two 4-char lengths plus a separator character between them.
*/
#define chars_in_lengths_string (2 * 4 + 1)

/* Justify left or right side */
#define LEFT 2
#define RIGHT 1

/* Write exactly 4 chars.  Cheap hack to show "human readable" values. */
static void write_length(uint32_t len, string *str, bool left_justify) {
  uint32_t printlen;
  if (len < 1024)
    /* snprintf needs one extra byte for a NUL terminator */
    snprintf(str, 5, (left_justify ? "%4u" : "%-4u"), len);
  else
  {
    printlen = (len + (1 << 9)) >> 10;
    if (printlen < 1000)
      snprintf(str, 5, (left_justify ? "%3uK" : "%-3uK"), printlen);
    else
    {
      printlen = (len + (1 << 19)) >> 20;
      if (printlen < 1000)
        snprintf(str, 5, (left_justify ? "%3uM" : "%-3uM"), printlen);
      else
      {
        printlen = (len + (1 << 29)) >> 30;
        assert(printlen < 1000);
        snprintf(str, 5, (left_justify ? "%3uG" : "%-3uG"), printlen);
      }
    }
  }
}

static string *pattern_lengths_to_string(PatternMetaData meta, string *str) {
  /* Always show min length */
  write_length(metadata_minlen(meta), str, RIGHT);
  /* If fixed length, show only one number, right justified */
  if (metadata_fixedlen(meta))
    return str;
  /* If unbounded, show slash but not the max value, which is meaningless */
  str[4] = '/';
  if (HAS_BITFLAG(metadata_flags(meta), PATTERN_UNBOUNDED)) {
    str[5] = '-';
    return str;
  }
  write_length(metadata_maxlen(meta), &str[5], LEFT);
  return str;
}

static char SYM[] = "C0NhfUF";

static void print_metadata_legend(void) {
  printf("  [%c=captures %c=nullable %c=nofail %c=headfail %c=follow %c=unbounded %c=fixedlen]\n",
         SYM[0], SYM[1], SYM[2], SYM[3], SYM[4], SYM[5], SYM[6]);
}

static void print_compact_pattern_metadata (PatternMetaData meta, Symbol_Namespace ns) {
  char lengths_string[chars_in_lengths_string + 1];
  /* Fill the output string with spaces */
  snprintf(lengths_string, chars_in_lengths_string+1, "%*s", chars_in_lengths_string, "");
  if (HAS_BITFLAG(metadata_flags(meta), PATTERN_READY)) {
    printf("  %c%c%c%c%c%c  ",
           HAS_BITFLAG(metadata_flags(meta), PATTERN_CAPTURES) ? SYM[0] : '-',
           HAS_BITFLAG(metadata_flags(meta), PATTERN_NULLABLE) ? SYM[1] : '-',
           HAS_BITFLAG(metadata_flags(meta), PATTERN_NOFAIL) ? SYM[2] : '-',
           HAS_BITFLAG(metadata_flags(meta), PATTERN_HEADFAIL) ? SYM[3] : '-',
           HAS_BITFLAG(metadata_flags(meta), PATTERN_NEEDFOLLOW) ? SYM[4] : '-',
           HAS_BITFLAG(metadata_flags(meta), PATTERN_UNBOUNDED) ? SYM[5] : metadata_fixedlen(meta) ? SYM[6]
                                                                                                : '-');
    printf(" %-*s", chars_in_lengths_string, pattern_lengths_to_string(meta, lengths_string));
  } else {
    printf("   %s   %*s",
	   (ns == symbol_ns_id) ? "unset" : "     ",
	   chars_in_lengths_string, "");
  }
}

void print_symboltable_stats (SymbolTable *st) {
  printf("Symbol table %p: %" pexl_Index_FMT "/%" pexl_Index_FMT " used."
	 "  Block %zu/%zu bytes used.\n",
         (void *)st,
	 Table_size(st->entries), Table_capacity(st->entries),
	 StringBuffer_len(st->block), StringBuffer_capacity(st->block));
}

void print_symboltable (SymbolTable *st) {
  const string *name;
  SymbolTableEntry *entry;
  print_symboltable_stats(st);
  print_metadata_legend();
  if (Table_size(st->entries) == 0) {
    printf("   Empty symbol table\n");
    return;
  }
  printf("  Index   Offset Vis  Type Size  Value  MetaData   PatLen   Name\n");
  pexl_Index i = PEXL_ITER_START;
  while ((entry = symboltable_iter(st, &i))) {
    printf("  %5" pexl_Index_FMT ": ", i);
    printf("[%5d] ", entry->handle);
    printf("%4s ", (entry->visibility == symbol_global) ? "Glob" : "Loc ");
    if (entry->namespace == symbol_ns_id)
      printf("%4s", "Id");
    else switch (entry->type) {
      case symbol_type_string:
	printf("%4s", "Str"); break;
      case symbol_type_capname:
	printf("%4s", "Cap"); break;
      case symbol_type_import:
	printf("%4s", "Imp"); break;
      default:
	printf("%4s", "???"); break;
      }
    printf("%5d ", entry->size);
    printf("%6d ", entry->value);
    print_compact_pattern_metadata(entry->meta, entry->namespace);
    name = symboltable_get_name(st, i);
    printf(" %s\n", name);
  }
}

/* ----------------------------------------------------------------------------- */
/* Print package, package table                                                  */
/* ----------------------------------------------------------------------------- */

void fprint_binary (FILE *channel, pexl_Binary *pkg) {
  if (pkg) {
    SymbolTable *st = pkg->symtab;
    CharsetTable *cst = pkg->cstab;
    fprintf(channel, "pexl_Binary %p: size %d/%d instructions, main pattern index %u\n",
            (void *)pkg, pkg->codenext, pkg->codesize, pkg->main);
    if (st) print_symboltable(st);
    if (cst) print_charsettable(cst);
    fprint_instructions(channel, pkg, pkg->code, pkg->codenext);
  } else {
    fprintf(channel, "NULL package\n");
  }
}

void print_packagetable_stats (pexl_PackageTable *pt) {
  printf("pexl_PackageTable %p: size %d, capacity %d entries\n",
         (void *)pt, pt->next, pt->capacity);
}

void print_packagetable (pexl_PackageTable *pt) {
  const string *importpath, *prefix, *origin;
  pexl_Binary *current;
  size_t i;
  print_packagetable_stats(pt);
  printf("    pexl_Binary        ImportPath   Prefix     Source       Lang     Code           (Size)\n");
  for (i = 0; i < pt->next; i++)
  {
    current = pt->packages[i];
    if (!current)
    {
      printf("%2ld: %-14p\n", i, (void *)current);
      continue;
    }
    importpath = symboltable_get_name(current->symtab, package_importpath(current));
    prefix = symboltable_get_name(current->symtab, package_prefix(current));
    origin = symboltable_get_name(current->symtab, current->origin);
    printf("%2ld: %-14p %-12.12s %-10.10s %-12.12s %3d.%-3d  %-14p (%u)\n",
           i, (void *)current,
           importpath, prefix, origin, current->major, current->minor,
           (void *)current->code, current->codesize);
  }
}

void print_importtable (pexl_Binary *p) {
  const string *importpath, *prefix;
  uint32_t i;
  if (!p) {
    printf("NULL package passed to print_importtable\n");
    return;
  } else if (!p->imports) {
    printf("NULL imports field in package passed to print_importtable\n");
    return;
  }
  printf("Import Table for package %p  Import table entries %d/%d\n", (void *)p, p->import_next, p->import_size);
  printf("    pexl_Binary         ImportPath               Prefix\n");
  for (i = 0; i < p->import_next; i++) {
    printf("%2d  %-14p", i, (void *)p->imports[i].pkg);
    importpath = symboltable_get_name(p->symtab, p->imports[i].importpath);
    prefix = symboltable_get_name(p->symtab, p->imports[i].prefix);
    printf("  (%3d) %-12.12s (%3d) %-10.10s\n",
           p->imports[i].importpath, importpath,
           p->imports[i].prefix, prefix);
  } /* for */
}

void print_binary_attributes (pexl_BinaryAttributes *attrs) {
  printf("  importpath = %s\n", attrs->importpath);
  printf("  prefix = %s\n", attrs->prefix);
  printf("  origin = %s\n", attrs->origin);
  printf("  doc = %s\n", attrs->doc);
  printf("  required language version (major, minor) = (%d, %d)\n",
	 attrs->major, attrs->minor);
}

/* ----------------------------------------------------------------------------- */
/* Print env, patterns                                                           */
/* ----------------------------------------------------------------------------- */

void print_BindingTable_stats (pexl_Context *C) {
  if (!C) {
    printf("Null context arg -- cannot print BindingTable stats\n");
    return;
  }
  BindingTable *bt = C->bt;
  if (!bt) {
    printf("Null binding table\n");
    return;
  }
  pexl_Env parent;
  printf("BindingTable %p size: %d/%d entries\n",
         (void *)bt, bt->next, bt->capacity);
  printf("       Parent table for environments:\n");
  for (pexl_Env i = 0; i < MAX_ENV_NODES; i++) {
    parent = env_get_parent(bt, i);
    if (parent < 0) continue;
    printf("       %3d -> %-3d\n", i, parent);
  }
}

static void print_compact_pattern(Pattern *ep) {
  if (!ep) {
    printf("Error: no pattern!  ");
    return;
  }
  printf(" %p", (void *)ep);
  printf(" EP=%4d", ep->entrypoint);
  print_compact_pattern_metadata(ep->meta, symbol_ns_id);
}

static const string *typename(Value val) {
  switch (val.type)
  {
  case Eerror_t:
    return "<pexl_ErrorType>";
  case Eunspecified_t:
    return "<Unspecified>";
  case Estring_t:
    return "<String>";
  case Eint32_t:
    return "<Int32>";
  case Epackage_t:
    return "<pexl_Binary>";
  case Epattern_t:
    return "<Pattern>";
  case Eexpression_t:
    return "<pexl_Expr *>";
  case Efunction_t:
    return "<Function>";
  case Emacro_t:
    return "<Macro>";
  default:
    return "*ERROR*";
  }
}

static void print_env_value(Value val) {
  printf("%-12s ", typename(val));
  switch (val.type) {
  case Estring_t:
    printf("   #%04d", val.data);
    printf("%27s", "");
    break;
  case Eint32_t:
    printf("%8d", val.data);
    printf("%27s", "");
    break;
  case Eerror_t:
    printf("   %-5d", val.data);
    printf("%27s", "");
    break;
  case Epackage_t:
  case Efunction_t:
  case Emacro_t:
    printf(" %-p", val.ptr);
    printf("%21s", "");
    break;
  case Epattern_t:
    print_compact_pattern((Pattern *)val.ptr);
    break;
  default:
    printf("%35s", "");
  }
}

#define NUMFLAGS 4
static const string *binding_attribute_flags(Binding *b, string *flagbuf) {
  int i;
  for (i = 0; i < NUMFLAGS; i++)
    flagbuf[i] = ' ';
  if (binding_has(b, Eattr_deleted))
    flagbuf[0] = 'D';
  if (binding_has(b, Eattr_visible))
    flagbuf[1] = 'V';
  return flagbuf; /* for convenience */
}

static void print_binding_name(Binding *binding, pexl_Context *C) {
  size_t len = 0;
  const string *ptr = NULL;
  if (C) {
    ptr = context_retrieve(C, binding->namehandle, &len);
    printf("%.*s", (int)len, ptr);
  } else {
    printf("<interned string %d>", binding->namehandle);
  }
}

/*
  External libpexl API does not know the type BindingTable, so this
  internal API is needed for unit testing.
  Set env to -1 to get ALL envs, i.e. the complete table.
 */
void print_env_internal (pexl_Env env, BindingTable *bt, pexl_Context *C) {
  Binding *binding;
  char flags[NUMFLAGS];
  pexl_Index i;
  if (!bt) {
    printf("Null binding table (environment)\n");
    return;
  }
  print_BindingTable_stats(C);
  printf("Index  Env Attr  Value                                PFlags   PLen        Name\n");
  for (i = 0; i < bt->next; i++) {
    binding = &bt->bindings[i];
    assert(binding);
    if ((env != -1) && (binding->env != env)) continue;
    printf("%4d: %3d", i, binding->env);
    printf("  %-.*s  ", NUMFLAGS, binding_attribute_flags(binding, flags));
    print_env_value(binding->val);
    if (binding->namehandle) {
      printf("  ");
      print_binding_name(binding, C);
    } else {
      printf("  anon/%d", i);
    }
    printf("\n");
  }
}

void print_BindingTable (pexl_Context *C) {
  if (!C) {
    confess("print", "Null context arg -- cannot print its environment\n");
    return;
  }
  if (!C->bt)
    printf("Null binding table\n");
  else
    print_env_internal(-1, C->bt, C);
}

/* ----------------------------------------------------------------------------- */
/* Print character sets and charset tables                                       */
/* ----------------------------------------------------------------------------- */

static void fprint_char (FILE *channel, int c) {
  assert(c >= 0); assert(c <= 0xff);
  if ((c == ',') || (c == '\\'))
    fprintf(channel, "\\%c", c);
  else if ((c > 32) && (c < 127))
    fprintf(channel, "%c", c);
  else 
    fprintf(channel, "0x%02x", c);
}

void fprint_charset(FILE *channel, uint32_t *st) {
  int i, first;
  if (!st) {
    fprintf(channel, "NULL CHARSET");
    return;
  }
  int prev = 0;
  fprintf(channel, "[");
  for (i = 0; i <= UCHAR_MAX; i++) {
    first = i;
    while (i <= UCHAR_MAX && testchar(st, i)) i++;
    if (i - 1 == first) {	/* unary range */
      if (prev) fputs(", ", channel);
      else prev = 1;
      fprint_char(channel, first);
    } else if (i - 1 > first) {	/* non-empty range */
      if (prev) fputs(", ", channel);
      else prev = 1;
      fprint_char(channel, first);
      fprintf(channel, "-");
      fprint_char(channel, i - 1);
    }
  }
  fprintf(channel, "]");
}

void print_charset(uint32_t *st)
{
  fprint_charset(stdout, st);
}

void fprint_simdcharset(FILE *channel, uint32_t *cs)
{
  int i;
  fprintf(channel, "[");
  /* Hex */
  for (i = 0; i < 8; i++) {
    if (i) fputc(' ', channel);
    fprintf(channel, "0x%04X", cs[i]);
  }
  fprintf(channel, "]");
}
  
void print_simdcharset (uint32_t *cs) {
  fprint_simdcharset(stdout, cs);
}

void fprint_charsettable(FILE *channel, CharsetTable *cst) {
  if (!cst) {
    fprintf(channel, "NULL charset table\n");
    return;
  }
  fprintf(channel, "Charset table %p: %d/%d entries, %s SIMD.\n",
	  (void *)cst,
	  cst->next,
	  (cst->capacity == 0) ? cst->next : cst->capacity,
	  cst->simd_charsets ? "with" : "without");

  for (pexl_Index i = 0; i < cst->next; i++) {
    fprintf(channel, "%3d plain: ", i);
    fprint_charset(channel, cst->charsets[i].cs);
    if (cst->simd_charsets) {
      fprintf(channel, "\n     simd: ");
      fprint_simdcharset(channel, cst->simd_charsets[i].cs);
    }
    fprintf(channel, "\n");
  }
}

void print_charsettable(CharsetTable *cst) {
  fprint_charsettable(stdout, cst);
}

/* ----------------------------------------------------------------------------- */
/* Print intermediate representation (trees)                                     */
/* ----------------------------------------------------------------------------- */

static const string *tagnames[] = {
    "bytes", "set", "any",
    "true", "false",
    "rep", "seq", "choice",
    "not", "ahead",
    "call", "xcall",
    "opencall",
    "behind",
    "capture",
    "function",
    "find",
    "no-tree",
    "backreference"};

typedef struct
{
  int indent;
  pexl_Context *C;
} print_context;

/*
  Pre-order traversal.
*/
static void walk_exp(Node *node,
                     print_context (*visit)(Node *, print_context),
                     print_context context)
{
  int sibs = numchildren[node->tag];
  print_context child_context = visit(node, context);
  if (sibs >= 1)
    walk_exp(child1(node), visit, child_context);
  if (sibs >= 2)
    walk_exp(child2(node), visit, child_context);
  return;
}

static void print_context_string (pexl_Context *C, pexl_Index handle) {
  const string *str;
  size_t len;
  char c;
  if (C) {
    str = context_retrieve(C, handle, &len);
    if (!str) {
      warn("print",
	   "Failed to retrieve string at offset (handle) %d",
	   handle);
    } else {
      printf("'");
      for (size_t i = 0; i < len; i++) {
	c = str[i];
	if (isprint(c))
	  printf("%c", c);
	else
	  printf("\\x%02X", c);
      } /* for each byte in the byte string */
      printf("'");
    }
    return;
  }
  printf("cannot print string (null context argument)");
  return;
}

static print_context printnode_visitor(Node *node, print_context context)
{
  int i;
  int indent = context.indent;
  pexl_Context *C = context.C;
  if (!node) {
    printf("NULL node\n");
    return context;
  }
  for (i = 0; i < indent; i++) printf(" ");
  printf("%s ", tagnames[node->tag]);
  switch (node->tag) {
  case TBytes:
    print_context_string(C, node->a.stringtab_handle);
    printf("\n");
    break;
  case TSet:
    print_charset(nodecharset(node));
    printf("\n");
    break;
  case TCapture:
    printf("kind: %s, handle: %d, name: ",
	   pexlCaptureName(node->cap), node->b.n);
    print_context_string(C, node->b.n);
    if (node->cap == Cconstant) {
      printf("constant handle: %d, value: ", node->a.stringtab_handle);
      print_context_string(C, node->a.stringtab_handle);
    }
    printf("\n");
    break;
  case TCall:
    printf("%p\n", (void *)node->a.pat);
    break;
  case TOpenCall:
    printf("to ref(%p, %d)\n", (void *)node->a.bt, node->b.index);
    break;
  case TBackref:
    printf("to capture handle: %d, name: ", node->a.stringtab_handle);
    print_context_string(C, node->a.stringtab_handle);
    printf("\n");
    break;
  default:
    printf("\n");
  }
  if (numchildren[node->tag] > 0)
  {
    print_context new_context;
    new_context.indent = indent + 2;
    new_context.C = C;
    return new_context;
  }
  return context;
}

void printnode(Node *node, int indent, pexl_Context *C) {
  print_context context;
  context.indent = indent;
  context.C = C;
  walk_exp(node, printnode_visitor, context);
}

void print_value(Value V, int indent, pexl_Context *C) {
  switch (V.type) {
  case Epattern_t:
    printf("Value type: PATTERN\n");
    print_pattern((Pattern *)V.ptr, indent, C);
    break;
  case Eunspecified_t:
    printf("Value type: UNSPECIFIED\n");
    break;
  default:
    printf("Error: no print function for value type: %d\n", V.type);
  }
}

void print_cfgraph(CFgraph *g) {
  pexl_Index i, j;
  Pattern **deplist;

  if (!g) {
    printf("print_cfgraph: NULL graph\n");
    return;
  }
  printf("CFgraph %p of size %d\n", (void *)g, g->next);
  for (i = 0; ((size_t)i) < ((size_t)g->next); i++)
  {
    deplist = g->deplists[i];
    printf("[%2d] %p: ", i, (void *)deplist[0]);
    j = 1;
    while (deplist[j])
    {
      printf(" %p", (void *)deplist[j]);
      j++;
    }
    printf("\n");
  } /* for */
}

void print_sorted_cfgraph(CFgraph *g)
{
  void *vptr;
  pexl_Index i, prev;
  if (!g) {
    printf("print_sorted_cfgraph: NULL graph argument\n");
    return;
  }
  if (!g->deplists) {
    printf("print_sorted_cfgraph: NULL deplists field in graph %p\n", (void *)g);
    return;
  }
  if (!g->order) {
    printf("print_sorted_cfgraph: NULL order field in graph %p\n", (void *)g);
    return;
  }
  printf("Sorted CFgraph %p\n", (void *)g);
  assert(((size_t)g->order->top) == ((size_t)g->next));
  prev = 0;		       /* 0 is not a valid component number */
  for (i = 0; i < g->next; i++) {
    if (g->order->elements[i].cnum != prev) {
      prev = g->order->elements[i].cnum;
      printf("Component #%d:\n", prev);
    }
    if (g->order->elements[i].vnum < 0)
      printf("error! vnum %d out of range\n", g->order->elements[i].vnum);
    else if (((size_t)g->order->elements[i].vnum) >= ((size_t)g->next))
      printf("error! vnum %d out of range\n", g->order->elements[i].vnum);
    else
    {
      vptr = (void *)*g->deplists[g->order->elements[i].vnum];
      printf("[%d]  Vertex %d  %p\n", i, g->order->elements[i].vnum, vptr);
    }
  } /* for */
}

#define FLAG(name) (pattern_has_flag(p, PATTERN_##name) ? "Yes" : "No")

void print_pattern_metadata(Pattern *p, int indent) {
  INDENT(indent);
  printf("Ready..... %s\n", FLAG(READY));
  if (pattern_has_flag(p, PATTERN_READY)) {
    INDENT(indent);
    printf("pexlCaptures.. %s\n", FLAG(CAPTURES));
    INDENT(indent);
    printf("Nullable.. %s\n", FLAG(NULLABLE));
    INDENT(indent);
    printf("Nofail.... %s\n", FLAG(NOFAIL));
    INDENT(indent);
    printf("Headfail.. %s\n", FLAG(HEADFAIL));
    INDENT(indent);
    printf("NeedFollow %s\n", FLAG(NEEDFOLLOW));
    INDENT(indent);
    printf("Unbounded  %s\n", FLAG(UNBOUNDED));
    INDENT(indent);
    printf("Min len    %d\n", metadata_minlen(p->meta));
    INDENT(indent);
    printf("Max len    %d\n", metadata_maxlen(p->meta));
    INDENT(indent);
    printf("Firstset:  ");
    print_charset(metadata_firstset_ptr(p->meta)->cs);
    printf("\n");
  }
}

void print_pattern(Pattern *p, int indent, pexl_Context *C) {
  INDENT(indent);
  printf("Pattern %p: ", (void *)p);
  if (p) {
    pexl_print_Expr(p->tree, indent, C);
    print_pattern_metadata(p, indent);
  } else {
    INDENT(indent);
    printf("(null)\n");
  }
}

/* ----------------------------------------------------------------------------- */
/* Print instruction vectors                                                     */
/* ----------------------------------------------------------------------------- */

static void fprintjmp(FILE *channel, const Instruction *op, const Instruction *p) {
  fprintf(channel, "jmp to %d", (int)(p + getoffset(p) - op));
}

static void fprintcapkind(FILE *channel, int kind) {
  fprintf(channel, "%s ", pexlCaptureName(kind));
}

typedef struct Instruction_printing_context {
  FILE *channel;
} Instruction_printing_context;

/*
 * Print instructions with their absolute addresses, showing jump
 * destinations with their computed target addresses.
 */
static int _fprint_instruction(const pexl_Binary *pkg,
			       const Instruction *p,
                               void *ctx) {
  Instruction_printing_context *context = (Instruction_printing_context *)ctx;
  FILE *channel = context->channel;
  SymbolTable *symtab = pkg->symtab;
  CharsetTable *cstab = pkg->cstab;
  const Instruction *op = pkg->code;
  Charset *cs_ptr;
  
  const string *name = NULL;
  if (opcode(p) >= PEXL_NUM_OPCODES)
    fprintf(channel, "ILLEGAL OPCODE: %d\n", opcode(p));
  else
    fprintf(channel, "%4ld  %s ", (long)(p - op), OPCODE_NAME(opcode(p)));
  switch (opcode(p)) {
  case ILookBehind: {
    fprintf(channel, "min delta = %d, max delta = %d", signedaux(p), getaddr(p));
    break;
  }
  case ICall: {
    fprintf(channel, "to %d", (int) getaddr(p));
    break;
  }
  case IXCall: {
    fprintf(channel, "to pkg #%d, symtab index %d", aux(p), getaddr(p));
    break;
  }
  case IChar: {
    fprint_char(channel, aux(p) & 0xff);
    break;
  }
  case ITestChar: {
    fprint_char(channel, aux(p) & 0xff);
    fprintf(channel, " - on failure ");
    fprintjmp(channel, op, p);
    break;
  }
  case IOpenCapture: {
    fprintcapkind(channel, aux(p));
    fprintf(channel, "#%d", getaddr(p));
    if (symtab) {
      name = symboltable_get_name(symtab, getaddr(p));
      fprintf(channel, " '%s'", name);
    }
    break;
  }
  case ICloseConstCapture: {
    fprintf(channel, "#%d", getaddr(p));
    if (symtab) {
      name = symboltable_get_name(symtab, getaddr(p));
      fprintf(channel, " '%s'", name);
    }
    break;
  }
  case IBackref: {
    fprintf(channel, "handle %d", getaddr(p));
    if (symtab) {
      name = symboltable_get_name(symtab, getaddr(p));
      fprintf(channel, " '%s'", name);
    }
    break;
  }
  case ISet: {
    fprintf(channel, "(cs #%d) ", aux(p));
    cs_ptr = charsettable_get_charset(cstab, aux(p));
    fprint_charset(channel, cs_ptr->cs);
    break;
  }
  case ITestSet: {
    fprintf(channel, "(cs #%d) ", aux(p));
    cs_ptr = charsettable_get_charset(cstab, aux(p));
    fprint_charset(channel, cs_ptr->cs);
    fprintf(channel, " - on failure ");
    fprintjmp(channel, op, p);
    break;
  }
  case ISpan: {
    fprintf(channel, "(cs #%d) ", aux(p));
    cs_ptr = charsettable_get_charset(cstab, aux(p));
    fprint_charset(channel, cs_ptr->cs);
    break;
  }
  case IFind: {
    fprintf(channel, "(cs #%d) ", aux(p));
    fprintf(channel, "(9-255 chars) ");
    if (pexl_simd_available() && charsettable_has_simd(cstab))
      fprintf(channel, "(have SIMD) ");
    cs_ptr = charsettable_get_charset(cstab, aux(p));
    fprint_charset(channel, cs_ptr->cs);
    break;
  }
  case IFind8: {
    fprintf(channel, "(cs #%d) ", aux(p));
    cs_ptr = charsettable_get_charset(cstab, aux(p));
    fprintf(channel, "(2-8 chars) ");
    if (pexl_simd_available() && charsettable_has_simd(cstab))
      fprintf(channel, "(have SIMD) ");
    fprint_charset(channel, cs_ptr->cs);
    break;
  }
  case IFind1: {
    fprintf(channel, "(1 char) ");
    if (pexl_simd_available()) fprintf(channel, "(have SIMD) ");
    fprint_char(channel, aux(p) & 0xFF);
    break;
  }
  case IBehind: {
    fprintf(channel, "%d", aux(p));
    break;
  }
  case ITestAny: {
    fprintf(channel, " - on failure ");
    fprintjmp(channel, op, p);
    break;
  }
  case IJmp: {
    fprintf(channel, "to %d", (int)(p + getoffset(p) - op));
    break;
  }
  case IChoice: {
    fprintf(channel, "- on failure ");
    fprintjmp(channel, op, p);
    break;
  }
  case IFindLoop: {
    fprintf(channel, "- loop starts at %d", (int)(p + getoffset(p) - op));
    break;
  }
  case ICommit: {
    fprintf(channel, "and ");
    fprintjmp(channel, op, p);
    break;
  }
  case IPartialCommit: {
    if (aux(p))
      fprintf(channel, "w/progress check ");
    fprintjmp(channel, op, p);
    break;
  }
  case ILookBehindCommit: {
    fprintf(channel, "** not using addr field ** ");
    //fprintjmp(channel, op, p);
    break;
  }
  default:
    break;
  }
  fprintf(channel, "\n");
  return 0; /* OK */
}

int fprint_instruction (FILE *channel,
			const pexl_Binary *pkg,
			const Instruction *p) {
  Instruction_printing_context context = {channel};
  return _fprint_instruction(pkg, p, &context);
}

static int walk_instructions (const pexl_Binary *pkg,
			      const Instruction *p, /* instruction to start at */
			      int codelength,       /* amount of code to walk */
			      /* Args below: package, current instruction, context */
			      int (*operation)(const pexl_Binary *pkg, const Instruction *, void *),
			      void *context) {
  int stat;
  const Instruction *last = p + codelength;
  while (p < last) {
    if ((stat = (*operation)(pkg, p, context)) != 0)
      return stat;
    p += sizei(p);
  }
  return OK;
}

int fprint_instructions (FILE *channel,
			 const pexl_Binary *pkg,
			 const Instruction *p,
			 /* amount of code to print */
			 int codelength) {
  Instruction_printing_context context = {channel};
  fprintf(channel, "Code vector (printing %d/%d instructions):\n",
	  codelength,
	  pkg->codenext);
  assert(codelength >=0);
  return walk_instructions(pkg, p, codelength, &_fprint_instruction, &context);
}

int print_instructions (const pexl_Binary *pkg,
			const Instruction *p,
			/* amount of code to print: */
			int codelength) {
  return fprint_instructions(stdout, pkg, p, codelength);
}

/* ----------------------------------------------------------------------------- */
/* Printing                                                                      */
/* ----------------------------------------------------------------------------- */

static void print_match_node (match_node *node, int indent) {
  printf("%*sstart:         %" pexl_Position_FMT "\n", indent, "", node->data.start);
  printf("%*send:           %" pexl_Position_FMT "\n", indent, "", node->data.end);
  printf("%*sprefix:        %" pexl_Index_FMT "\n", indent, "", node->data.prefix);
  printf("%*stype:          %" pexl_Index_FMT "\n", indent, "", node->data.type);
  if (node->data.inserted_data)
    printf("%*sinserted_data: %" pexl_Index_FMT "\n", indent, "", node->data.inserted_data);
  printf("%*ssibling:       %" pexl_Index_FMT "\n", indent, "", node->sibling);
  printf("%*slast_child:    %" pexl_Index_FMT "\n", indent, "", node->last_child);
}

void print_match_tree_flat (pexl_MatchTree *m) {
  if (!m) {
    printf("NULL match tree\n");
    return;
  }
  printf("MatchTree (size=%zu, capacity=%zu, block=%p, blocklen=%zu):\n",
	 m->size, m->capacity, (const void *)m->block, m->blocklen);
  for (size_t i = 0; i < m->size; i++) {
    printf("[%zu]\n", i);
    print_match_node(&(m->nodes[i]), 2);
  }
}

static const string *lookup (pexl_MatchTree *tree, pexl_Index idx) {
  if (!tree || !tree->block || !tree->blocklen) return NULL;
  if ((idx < 0) || (((size_t) idx) > tree->blocklen)) return NULL;
  return &(tree->block[idx]);
}

/*
  A tree up to this deep will be printed "correctly", i.e. with the
  indended visual style.  Deeper trees will be printed, but the
  deepest nodes will not be indented correctly.
*/
#define MAX_TREE_DEPTH 1024

static void tree_indent (int depth, uint8_t *parents) {
  if (depth > MAX_TREE_DEPTH) {
    depth = MAX_TREE_DEPTH;
    printf("MAX TREE DEPTH EXCEEDED ");
  }
  for (int i=1; (i < depth); i++) {
    printf("%s", (parents[i] ? "│   " : "    "));
  }
}

static void do_print_match_tree (pexl_MatchTree *tree,
				 pexl_Index nidx,
				 int depth,
				 uint8_t *parents) {
  match_node *node;
  node = get_node(tree, nidx);
  if (!node) {
    warn("match tree printer", "Error at node index %d", nidx);
    return;
  }
  const string *prefix = lookup(tree, node->data.prefix);
  const string *type = lookup(tree, node->data.type);
  const string *data = lookup(tree, node->data.inserted_data);
  const bool has_sibling = (node->sibling != 0);
  /*
    How to indent the current node: 
     ├── if the last child of its parent
     └── if not last child of its parent

    How the current node's level will be indented later:
                                                          parent[i] 
     │   for printing the current node's descendants         1 
         (spaces) no more descendants of current node        0 

   */
  tree_indent(depth, parents);
  if (depth > 0) printf("%s", (has_sibling ? "├── " : "└── "));
  if (prefix) printf("%s.", prefix);
  printf("%s", type ? type : "(null)");
  if (data) printf(", %s", data);
  printf(" (%" pexl_Position_FMT ", %" pexl_Position_FMT ")",
	 node->data.start, node->data.end);
  printf("\n");
  /* Process children of current node recursively */
  if (depth < MAX_TREE_DEPTH) parents[depth] = has_sibling;
  nidx = match_tree_child(tree, nidx, &node);
  while (nidx >= 0) {
    if ((nidx != MATCHTREE_ERR_NOEXIST) && (nidx < 0)) {
      warn("match tree printer", "Error accessing child node %d", nidx);
      return;
    }
    do_print_match_tree(tree, nidx, depth+1, parents);
    nidx = match_tree_next(tree, nidx, &node);
  };
  if (depth < MAX_TREE_DEPTH) parents[depth] = 0;
}

void print_match_tree (pexl_MatchTree *m) {
  if (!m) {
    printf("NULL match tree\n");
    return;
  }
  printf("MatchTree (size=%zu, capacity=%zu, block=%p, blocklen=%zu):\n",
	 m->size, m->capacity, (const void *)m->block, m->blocklen);
  if (m->size) {
    uint8_t *parents = malloc(MAX_TREE_DEPTH);
    if (!parents) {
      warn("print", "Out of memory");
      return;
    }
    memset(parents, 0, MAX_TREE_DEPTH);
    do_print_match_tree(m, 0, 0, parents);
    free(parents);
  }
}

void print_match_summary (pexl_Match *match) {
  printf("Match %s\n", (match->end == -1) ? "failed" : "succeeded");
  printf("  end position    %ld\n", match->end);
  printf("  encoder_id      %d\n", match->encoder_id);
  printf("  match data      %p\n", match->data);
}

void print_match_data (pexl_Match *m) {
  switch (m->encoder_id) {
  case PEXL_NO_ENCODER:
    printf("No encoder, so no data\n");
    break;
  case PEXL_DEBUG_ENCODER:
    printf("Debug encoder, so no data\n");
    break;
  case PEXL_TREE_ENCODER:
    print_match_tree(m->data);
    break;
  default:
    confess("print", "No printer defined for encoder %d", m->encoder_id);
  }
}

/* ----------------------------------------------------------------------------- */
/* Print env tree, where each env is represented by a non-negative integer       */
/* ----------------------------------------------------------------------------- */

static void do_print_env_tree (BindingTable *bt,
			       pexl_Index env,
			       int depth,
			       uint8_t *parents) {
  /*
    How to indent the current node: 

     ├── if the last child of its parent
     └── if not last child of its parent

    How the current node's level will be indented later:
                                                          parent[i] 
     │   for printing the current node's descendants         1 
         (spaces) no more descendants of current node        0 

   */
  tree_indent(depth, parents);
  bool last_sibling = invalid_env(env_next_sibling(bt, env));
  if (depth > 0) printf("%s", (last_sibling ?  "└── " : "├── "));
  printf("%" pexl_Index_FMT "\n", env);
  /* Process children of current node recursively */
  if (depth < MAX_TREE_DEPTH) parents[depth] = !last_sibling;
  env = env_first_child(bt, env);
  while (!invalid_env(env)) {
    do_print_env_tree(bt, env, depth+1, parents);
    env = env_next_sibling(bt, env);
  };
  if (depth < MAX_TREE_DEPTH) parents[depth] = 0;
}

void print_env_tree (pexl_Context *C) {
  if (!C) {
    warn("print_env_tree", "Null context arg");
    return;
  }
  if (!C->bt) {
    warn("print_env_tree", "Null binding table in context -- internal error\n");
    return;
  }

  uint8_t *parents = malloc(MAX_TREE_DEPTH);
  if (!parents) {
    warn("print", "Out of memory");
    return;
  }
  memset(parents, 0, MAX_TREE_DEPTH);
  printf("Environment tree (capacity=%zu):\n", (size_t) MAX_ENV_NODES);
  do_print_env_tree(C->bt, 0, 0, parents);
  free(parents);
}
