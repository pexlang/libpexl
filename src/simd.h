//  -*- Mode: C; -*-                                                       
// 
//  simd.h
// 
//  See files LICENSE and COPYRIGHT, which must accompany this file
//  AUTHORS: Jamie A. Jennings

/*
  To prevent use of SIMD (vector) instructions, even when the CPU
  supports them, define PEXL_NO_SIMD at compile time.  E.g.

    make PEXL_NO_SIMD=true build

  In PEXL code, to conditionally compile based on whether SIMD should
  be used, check the value of PEXL_USING_SIMD.  The value will be
  non-zero if (1) SIMD is not blocked by PEXL_NO_SIMD, and (2) the CPU
  supports Neon or SSE4.1.

*/

/* ----------------------------------------------------------------------------- */

// Here we check for both clang and gcc preprocessor definitions, resp.

#if defined (__ARM64_ARCH_8__) || defined (__ARM_ARCH_8A)
  #if defined (__ARM_NEON__) || defined (__ARM_NEON)
    #define _PEXL_CPU_ACTUALLY_HAS_SIMD
  #endif
#elif defined (__x86_64__)
  #if defined (__SSE4_1__) || defined (__SSSE3__)
    #define _PEXL_CPU_ACTUALLY_HAS_SIMD
  #endif
#endif

#if !defined (PEXL_NO_SIMD) && defined (_PEXL_CPU_ACTUALLY_HAS_SIMD)
  #define PEXL_USING_SIMD 1
#else
  #define PEXL_USING_SIMD 0
#endif

/* Announce the one unusual SIMD situation during compilation */
// #if !PEXL_USING_SIMD && defined (_PEXL_CPU_ACTUALLY_HAS_SIMD)
//   #pragma message ("Not using available SIMD because PEXL_NO_SIMD is defined")
// #endif
