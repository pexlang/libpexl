/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  compile.c      Compile expressions and entire environments               */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "compile.h"
#include "opt.h"
#include "old-analyze.h"
#include "codegen.h"
//#include "pathtable.h"

/* ----------------------------------------------------------------------------- */
/* Misc                                                                          */
/* ----------------------------------------------------------------------------- */

pexl_Error new_error (pexl_Index val, pexl_Index loc) {
  return (((uint64_t) val) << 32) | (((uint64_t) loc) & 0xFFFFFFFF);
}

static void set_error_value (pexl_Error *err, pexl_Index val) {
  *err &= ((uint64_t) 0) << 32;
  *err |= ((uint64_t) val) << 32;
}

pexl_Index error_value (pexl_Error err) {
  return ((uint64_t) err) >> 32;
}

__attribute__((unused))
static void set_error_location (pexl_Error *err, pexl_Index loc) {
  *err &= (uint32_t) 0;
  *err |= (uint64_t) loc;
}

pexl_Index error_location (pexl_Error err) {
  return (pexl_Index) (err & 0xFFFFFFFF);
}

/* ----------------------------------------------------------------------------- */
/* Compile                                                                       */
/* ----------------------------------------------------------------------------- */

/* To compile expressions, E:

   Convert each E into Pattern(E).

     Pattern(E) is a compile-time data structure that wraps E.
     When E is bound, Pattern(E) replaces E in the binding.
     ** To simplify: Never bind an expression; always bind Pattern(E).

   Create a CompState.

     A single CompState holds compile-time data for a compilation
     unit, which can be a single expression or a collection of them.
     The only collection we support is an environment.

   Compute dependencies.

     Follow all the CALL nodes in each E, building a control flow
     graph.  (Today, this is merely a call graph, as it lacks flow
     information for "blocks" within a pattern.)

   Topologically sort the dependency graph.

     The sort order is the order in which we compile the expressions,
     so that when E calls F, we will have all of the meta data for F
     available to use when compiling E.

     The topological sort result may inform us that some bindings are
     (mutually) recursive.  Our environment model lets us declare an
     environment to be non-recursive, so we use the sort result to
     determine if recursion is found where it has been prohibited.

   Compute meta-data.

     Each Pattern(E) has a "type" that we call its meta-data.  This is
     a set of statically determined properties about the pattern, such
     as whether it is nullable (can match the empty string), whether
     it includes any captures, and more.

     The type of Pattern(E) generally depends on the types of all of
     the dependencies of E.  It is conceivable that during this phase
     we will detect a type error, though currently no such errors are
     possible.

     Regarding type errors: Compare to lpeg, where a nullable pattern
     cannot be the target of a repeat operator, and where a pattern
     that captures cannot be used in a look-behind -- to name two
     examples.  PEXL does not have these particular restrictions.

     In the future, when PEXL functions and macros are implemented,
     they will make use of more value types than just Pattern.  At
     that time, this stage of compilation will detect type errors.

   Generate code.

     Create a pexl_Binary which has a code vector and a symbol table.

     In addition, a pexl_Binary contains an explicit list of any
     dependencies on other packages (because a package can IMPORT
     other packages).  The only way, currently, to refer to an
     identifier in another package is to call it using XCALL.

     When an expression, E, contains 'XCALL F', the meta-data for the
     call target, F, must be known in order to compile E.  Therefore,
     the package containing F must be already compiled.  pexl_Binary
     dependencies cannot be circular -- they must form a DAG.
     Consequently, a topological sort of package dependencies produces
     a compilation order for those packages.

   Return a pexl_Binary.

     The result of compilation is called a pexl_Binary, even if it is the
     result of compiling a single, anonymous expression.  A pexl_Binary is
     executable by the pexl_Matching Virtual Machine.

   Returning to what it means to compile an expression, E, or a
   collection of expressions, we need the following:

   - E, or an environment (acting as a collection of expressions)
   - An environment in which to look up CALL targets
   - A package table in which to look up XCALL targets

   Due solely to implementation decisions, every expression is
   associated with a table of strings.  By interning strings, e.g.
   capture names, expressions can be freed without traversing them.
   The pointers in the expression Node structure are merely
   references; they are never the "owners" of the pointed-to data.  (A
   capture expression, for example, contains an index into the string
   table, not a pointer to a private string.)

*/
static int add_pattern (CompState *cst, Pattern *pat) {
  int stat;
  //trace("compile", "adding pattern '%s' (%p)", bound_name(C, pat), (void *)pat);
  if (!cst) {
    warn("add_pattern", "null compstate argument");
    return PEXL_ERR_INTERNAL;
  }
  if (!pat) {
    warn("add_pattern", "binding with pattern type but no null pattern");
    return PEXL_ERR_INTERNAL;
  }
  /* Set cst->pat to pattern being compiled */
  cst->pat = pat;
  /* Fix open calls in cst->pat */
  stat = fix_open_calls(cst);
  if (stat) {
    /* No warning: could be user error, e.g. left recursive pattern */
    inform("add_pattern", "fix_open_calls returned %d", stat);
    return stat;
  }
  /* Extend the existing cfgraph starting at the newly-added pat */
  stat = add_to_cfgraph(pat, cst->g);
  if (stat) {
    warn("add_pattern", "add_to_cfgraph returned %d", stat);
    return stat;
  }
  return OK;
}

static int compile_init (pexl_Context *C) {
  assert(C);
  if (C->cst) {
    trace("compile_init", "freeing prior compilation state");
    compstate_free(C->cst);
  }
  C->cst = compstate_new();
  if (!C->cst) return PEXL_ERR_OOM;      /* already logged */
  C->cst->g = cfgraph_new();
  if (!C->cst->g) return PEXL_ERR_OOM;   /* already logged */
  return OK;
}

/*
  Analyze patterns, setting their metadata, and produce control flow
  graph and the topological sort needed for code generation.  When
  'main' is a valid binding (table index), the pattern generated there
  is returned, so it can be passed to codegen() which will set
  pkg->main, the symbol table for the "main" pattern.
*/
static int analyze (pexl_Context *C,
		    pexl_Ref main,
		    /* Output: */
		    Pattern **main_pattern,
		    pexl_Index *errbinding) {
  int stat;
  Binding *b;
  Pattern *p;
  pexl_Index i;
  pexl_Index count = 0;
  const pexl_Env env = PEXL_ROOT_ENV;
  
  /* Clear all prior compilation state */
  i = PEXL_ITER_START;
  while ((b = env_local_iter(C->bt, env, &i))) {
    if (b->val.type != Epattern_t)
      continue;
    p = (Pattern *)b->val.ptr;
    if (!p) {
      *errbinding = i;
      return PEXL_ERR_INTERNAL;
    }
    trace("analyze",
         "resetting compilation state for pattern %p",
         (void *)p);
    reset_compilation_state(p);
  } /* while */

  stat = compile_init(C);
  if (stat < 0) return stat; /* already logged */

  /* Compile all bound expressions */
  i = PEXL_ITER_START;
  while ((b = env_local_iter(C->bt, env, &i))) {
    if (b->val.type != Epattern_t) {
      /* FUTURE: handle any other value types */
      continue;
    }
    trace("analyze", "adding pattern in binding #%d", i);
    p = (Pattern *)b->val.ptr;
    if (!p) {
      *errbinding = i;
      return PEXL_ERR_INTERNAL;
    }
    /* Already compiled? */
    if (pattern_has_flag(p, PATTERN_READY)) continue;
    stat = add_pattern(C->cst, p);
    if (stat < 0) {
      *errbinding = i;
      return stat;
    }
    if (i == main) {
      *main_pattern = p;
      trace("analyze", "found main pattern %p at binding index %d",
	    (void *) p, main);
    }
    count++;
  } /* while */

  if (count == 0) {
    inform("analyze", "no patterns to compile in root environment");
    return OK;
  } else {
    inform("analyze", "found %d patterns to compile in root environment", count);
  }

  /* Now sort the dependency graph, and set the metadata */
  assert(C->cst && C->cst->g);
  stat = sort_cfgraph(C->cst->g);
  if (stat) {
    trace("analyze", "sort_cfgraph returned %d", stat);
    return stat;
  }
  assert(C->cst->g->order);
  stat = set_metadata(C->cst);
  if (stat) {
    trace("analyze", "set_metadata returned %d", stat);
    return stat;
  }
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* Link                                                                          */
/* ----------------------------------------------------------------------------- */

static int generate_code (pexl_Context *C,
			  pexl_Optims *optims,
			  Pattern *main_pattern,
			  /* Output: */
			  pexl_Index *errbinding,
			  pexl_Binary **retval)  {
  if (!C || !errbinding || !retval) {
    warn("generate_code", "null context, errbinding, or retval arg");
    return PEXL_ERR_NULL;
  }
  assert(C->cst && C->packages && C->bt && C->pathtab && C->stringtab);
  int stat;
  pexl_Binary *pkg = binary_new();
  if (!pkg) {
    warn("generate_code", "out of memory");
    return PEXL_ERR_OOM;
  }
  stat = codegen(C, optims, main_pattern, pkg);
  if (stat) {
    warn("generate_code",
	 "error %d from codegen at pattern bound at index %d",
	 stat, C->cst->err_index);
    binary_free(pkg);
    return stat;
  }
  *retval = pkg;
  return OK;
}

/* ----------------------------------------------------------------------------- */
/* Interface                                                                     */
/* ----------------------------------------------------------------------------- */

#define NODATA 0

// Create a pattern using a COPY of 'exp', and bind it in 'env'
pexl_Ref bind_in (pexl_Expr *exp, pexl_Env env, pexl_Index handle, BindingTable *bt) {
  assert(bt);
  assert(valid_path_to_root(env, bt));
  int stat;
  Value value;
  Pattern *p = NULL;
  if (exp) {
    /* Use -1 for env id to indicate "unset" */
    p = pattern_new(exp, bt, -1, &stat);
    if (!p) return stat;
    p->env = env;
    value = env_new_value(Epattern_t, NODATA, (void *)p);
  } else {
    // When exp is NULL, bind an "unspecified value" 
    value = env_new_value_unspecified();
  }    
  pexl_Ref ref = env_bind(bt, env, handle, value);
  if (pexl_Ref_invalid(ref)) return ref;
  if (p) p->binding_index = ref;
  return ref;
}

void free_bound_value (Binding *b) {
  assert(b);
  if (b->deplist) deplist_free((struct Pattern **)b->deplist);
  switch (bindingtype(b)) {
  case Epattern_t:
    pattern_free((Pattern *)b->val.ptr);
    //    b->val.ptr = NULL;	// in case it is bound twice
    break;
  case Eexpression_t:
    pexl_free_Expr((pexl_Expr *)b->val.ptr);
    //    b->val.ptr = NULL;	// in case it is bound twice
    break;
  case Epackage_t:
    /* We should not free other packages! */
    break;
  default:
    break;
  } /* switch on binding type */
}

// Free whatever value is currently stored at 'ref'.
// Create a pattern using a COPY of 'exp', and bind it at 'ref'.
int rebind (pexl_Ref ref, pexl_Expr *exp, BindingTable *bt) {
  int stat;
  Value value;
  Binding *b;
  Pattern *p = NULL;
  if (!bt) {
    warn("rebind", "null binding table arg");
    return PEXL_ERR_INTERNAL;
  }
  b = env_get_binding(bt, ref);
  if (!b) return PEXL_ENV__NOT_FOUND;
  
  free_bound_value(b);
  
  if (exp) {
    p = pattern_new(exp, bt, -1, &stat); /* -1 is temporary env id */
    if (!p) return stat;
    value = env_new_value(Epattern_t, NODATA, (void *)p);
  } else {
    // When exp is NULL, bind an "unspecified value" 
    value = env_new_value_unspecified();
  }    
  stat = env_rebind(bt, ref, value);
  if (stat) {
    if (p) pattern_free(p);
    return stat;
  }
  if (p) {
    p->binding_index = ref;
    p->env = b->env;
  }
  return PEXL_OK;
}

/*
  Compile all of the bindings in 'bt'.  The expressions bound there
  may reference bindings in other environments (which are descendants
  in the tree under PEXL_ROOT_ENV).

  Return value is a package or NULL.  If NULL, consult the pexl_Error
  structure for details.
 */
pexl_Binary *compile (pexl_Context *C,
		      pexl_Ref main_ref,	    // -1 for none
		      pexl_Optims *optims,          // can be NULL
		      pexl_BinaryAttributes *attrs, // can be NULL
		      enum pexl_CharEncoding enc,
		      pexl_Error *err) {
  if (!err) {
    warn("compile", "null error argument");
    return NULL;
  }
  else if (!C) {
    warn("compile", "null context argument");
    set_error_value(err, PEXL_ERR_NULL);
    return NULL;
  }
  assert(C->packages && C->bt && C->pathtab && C->stringtab);
  int stat;
  Pattern *main_pattern = NULL;
  pexl_Binary *pkg = NULL;
  pexl_Index errbinding = new_error(PEXL_ERR_INTERNAL, -1);

  if (!optimlist_is_valid(optims)) {
    warn("pexl_compile", "invalid optims");
    *err = new_error(PEXL_COMPILER__ERR_OPTIMS, -1);
    return NULL;
  }

  UNUSED(enc);		 // TEMP: next phase of project will need this arg

  stat = analyze(C, main_ref, &main_pattern, &errbinding);
  if (stat < 0) {
    trace("pexl_compile",
	  "analysis returned %d at index %" pexl_Index_FMT,
	  stat, errbinding);
    *err = new_error(stat, errbinding);
    return NULL;
  }

  stat = generate_code(C, optims, main_pattern, &errbinding, &pkg);
  if (stat < 0) {
    trace("pexl_compile",
	  "code generator returned %d at index %" pexl_Index_FMT,
	  stat, errbinding);
    *err = new_error(stat, errbinding);
    return NULL;
  }
  assert(pkg);

  if (attrs) {
    stat = binary_set_attributes(pkg,
				 attrs->importpath, attrs->prefix,
				 attrs->origin, attrs->doc,
				 attrs->major, attrs->minor);
    if (stat) {
      warn("pexl_compile", "error while setting binary attributes (%d)", stat);
      *err = new_error(PEXL_ERR_INTERNAL, -1);
      return NULL;
    }
  }
  pkg->pathtab=pathtable_copy(C->pathtab);//create a deep copy of pathtable so if we free context its all good. 
  *err = new_error(0, 0);	/* OK */
  return pkg;
}

/* ----------------------------------------------------------------------------- */
/* Environment management, for lexical scope                                     */
/* ----------------------------------------------------------------------------- */

pexl_Env pexl_scope_push (pexl_Context *C) {
  if (!C) {
    warn("pexl_compile", "null context arg");
    return PEXL_ERR_NULL;
  }
  if (!C->bt) return PEXL_ERR_INTERNAL;
  pexl_Env new = env_new(C->bt, C->env);
  if (invalid_env(new)) return new; // error code
  C->env = new;
  return new;
}

pexl_Env pexl_scope_pop (pexl_Context *C) {
  if (!C) {
    warn("pexl_compile", "null context arg");
    return PEXL_ERR_NULL;
  }
  if (!C->bt) return PEXL_ERR_INTERNAL;
  pexl_Env parent = env_get_parent(C->bt, C->env);
  if (invalid_env(parent)) return parent; // error code
  C->env = parent;
  return parent;
}
