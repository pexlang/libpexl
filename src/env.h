/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  env.h  Environment and value types                                       */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#ifndef env_h
#define env_h

#include "preamble.h"
#include "libpexl.h"
#include "expression.h"
#include "bitflags.h"

/* 
   ENVIRONMENT

   An ENVIRONMENT is a tree in which each node is a collection of
   bindings.  A binding is either anonymous or it has a name that is
   locally unique.  (The same name can appear in multiple nodes in the
   environment tree.)

   The representation consists of a single table of bindings, holding
   all the bindings for the entire environment tree.  Each binding has
   an 'env' field that reveals which node it belongs to.  The root
   environment is node 0 (PEXL_ROOT_ENV).

   A pexl_Ref uniquely identifies a binding.  It is simply the index
   of the binding in the single table of bindings.  Binding a value
   returns a pexl_Ref for that value.

   Since a binding can have a name, one can get a pexl_Ref to a
   binding by looking up a name.  Lookup can be confined to a single
   node in the environment tree, or it can be allowed to follow the
   chain of parent nodes, up to the root environment.  This
   facilitates implementation of lexical scope in RPL.

   Each binding holds an explicitly typed value. 

   The maximum number of entries in a single pexl_Env (node) is
   subject to these limits:

   - Must fit in 24 bits (unsigned) due to how vm instructions are
     encoded.

   See binary.h for PACKAGE and MACHINE definitions.

*/

/* ----------------------------------------------------------------------------- */
/* Parameters that can be tuned for performance                                  */
/* ----------------------------------------------------------------------------- */

#define ENV_INIT_SIZE 200            /* Default allocation of entries in new env */

/* ----------------------------------------------------------------------------- */
/* Constants that should not be changed without careful consideration            */
/* ----------------------------------------------------------------------------- */

#define ENV_MAX_SIZE (1<<22)   /* Max number of bindings, must fit in pexl_Index */

/* ----------------------------------------------------------------------------- */
/* Value types                                                                   */
/* ----------------------------------------------------------------------------- */

/* When adding a new ValueType that holds a pointer, be sure to free
   it in the free_env() in interface.c
*/
typedef enum ValueTypes {
  Eerror_t = 0,		   /* error */
  Eunspecified_t,          /* unspecified "placeholder", pointer field is NULL */
  Epattern_t,		   /* pointer to Pattern */
  Eexpression_t,           /* pointer to tree IR */
  Epackage_t,		   /* pointer to pexl_Binary */
  Estring_t,		   /* data is handle into string table */
  Eint32_t,		   /* data is int value */
  Efunction_t,		   /*  */
  Emacro_t		   /*  */
} ValueTypes;

/* Due to 8-byte alignment, there's no reason to pack type & data together */
typedef struct Value {
  void       *ptr;
  int32_t     type;
  int32_t     data;
} Value;

/* ----------------------------------------------------------------------------- */
/* Bindings and pexl_Refs                                                        */
/* ----------------------------------------------------------------------------- */

typedef struct Binding {
  struct DepList   *deplist;    /* NULL-terminated list of dependencies */
  Value             val;
  pexl_Index        namehandle;	/* binding name, an index into string table */
  pexl_Env          env;        /* binding is in this env node */
  uint32_t          meta;
} Binding;

#define bindingtype(binding) ((binding)->val.type)

BITFLAGS(Eattr_deleted,		// Marked for deletion
	 Eattr_visible);	// Visible to parent env

#define binding_has(b, flagname) (HAS_BITFLAG((b)->meta, (flagname)))
#define set_binding_flag(b, flagname, val) do {				\
    UPDATE_BITFLAG((b)->meta, (flagname), (val));			\
  } while(0)

/* ----------------------------------------------------------------------------- */
/* An BindingTable is a collection of bindings holding an environment tree       */
/* ----------------------------------------------------------------------------- */

#define MAX_ENV_NODES 1000

#define invalid_env(env) ((env < 0) || (env >= MAX_ENV_NODES))
#define invalid_handle(handle) (handle < 0)

typedef struct BindingTable {
  Binding          *bindings;		   /* bindings table */
  pexl_Index        capacity;	           /* capacity of bindings array */
  pexl_Index        next;	           /* next available is bindings[next] */
  pexl_Env          parent[MAX_ENV_NODES]; /* parent table */
} BindingTable;

#define bt_get(bt, i) \
  ((((i) < 0) || ((i) >= (bt)->next)) ? NULL : &((bt)->bindings[(i)]))

/* ----------------------------------------------------------------------------- */
/* Interfaces                                                                    */
/* ----------------------------------------------------------------------------- */

pexl_Env env_new (BindingTable *bt, pexl_Env parent);
pexl_Env env_size (BindingTable *bt);
pexl_Env env_get_parent (BindingTable *bt, pexl_Env env);
pexl_Env env_next_sibling (BindingTable *bt, pexl_Env env);
pexl_Env env_first_child (BindingTable *bt, pexl_Env env);

BindingTable *new_BindingTable (void);
void          free_BindingTable (BindingTable *bt);

/* In first call to iter(), set index to ITER_START. Returns NULL after last binding. */
#if PEXL_ITER_START != -1
#error "PEXL_ITER_START not set to -1"
#endif
Binding *env_local_iter(BindingTable *bt, pexl_Env env, pexl_Index *index);

/* Search by name only in local env, not ancestors; a deleted entry is "not found" */
pexl_Ref env_local_lookup (BindingTable *bt, pexl_Env env, pexl_Index entry);

/* Search by name locally then up through ancestors; a deleted entry is "not found" */
pexl_Ref env_lookup (BindingTable *bt, pexl_Env env, pexl_Index entry);

/* Direct access, returns NULL if binding is marked deleted */
Binding *env_get_binding (BindingTable *bt, pexl_Ref ref);

pexl_Ref env_bind (BindingTable *bt, pexl_Env env, pexl_Index entry, Value val);
int      env_rebind (BindingTable *bt, pexl_Ref ref, Value val);
void     env_unbind (BindingTable *bt, pexl_Ref ref);

bool valid_path_to_root (pexl_Env env, BindingTable *bt);

/* ----------------------------------------------------------------------------- */
/* Values (good to have)                                                         */
/* ----------------------------------------------------------------------------- */

/* Values */

Value env_new_value (int32_t type, int32_t data, void *ptr);
Value env_new_value_unspecified (void);

/* Other value type makers are defined elsewhere */
/* Value env_new_value_error (int number); */
/* Value env_new_value_int32 (int32_t i); */
/* Value env_new_value_string (SymbolTablepexl_Index entry); */

#endif
