/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  symboltable.c  String table (modeled on ELF)                             */
/*                                                                           */
/*  See files LICENSE and COPYRIGHT, which must accompany this file          */
/*  AUTHORS: Jamie A. Jennings                                               */

#include "symboltable.h"

SymbolTable *symboltable_new (size_t initial_size, size_t initial_blocksize) {
  SymbolTable *st;
  if (initial_size > SYMBOLTABLE_MAXSIZE)
    // Clamp initial size at maximum, if necessary
    initial_size = SYMBOLTABLE_MAXSIZE;
  if (initial_size == 0)
    // 0 means "use default size"
    initial_size = SYMBOLTABLE_INITSIZE;
  if (initial_blocksize == 0)
    // 0 means "use default size"
    initial_blocksize = SYMBOLTABLE_INITBLOCKSIZE;

  st = malloc(sizeof(SymbolTable));
  if (!st) goto oom1;
  st->entries = Table_new(initial_size,
			  SYMBOLTABLE_MAXSIZE,
			  sizeof(SymbolTableEntry),
			  TABLE_SOFTFAIL);
  if (!st->entries) goto oom2;
  st->block = StringBuffer_new((size_t) initial_blocksize);
  if (!st->block) goto oom3;
  // The empty string is always in the block, and always at index 0.
  StringBuffer_add_char(st->block, '\0'); 
  return st;

 oom3:
    Table_free(st->entries);
 oom2:
    free(st);
 oom1:
  warn("symboltable", "out of memory");
  return NULL;
}

void symboltable_free (SymbolTable *st) {
  if (!st) return;
  Table_free(st->entries);
  StringBuffer_free(st->block);
  free(st);
  return;
}

/* Return number of entries (how many entries are in use) */
size_t symboltable_len (SymbolTable *st) {
  assert(st);
  return Table_size(st->entries);
}

/* Return length in bytes (how much of the block is in use) */
size_t symboltable_blocklen (SymbolTable *st) {
  assert(st);
  return StringBuffer_len(st->block);
}

/*
  Would like to use memmem, but it's not supported everywhere.  Also,
  the naive implementation of memmem is worst case O(n^2).  We can
  achieve linear time because our haystack has some structure:
  - The first byte is NUL
  - Each string ends with a NUL

  Note that this search is intended to find suffixes.  E.g. if we have
  already stored "substring", and we're now looking for "string", we
  will find and reuse the last 6 letters of "substring".

  When name is NULL or length is 0, the search returns the empty
  string (at position 0).
*/

static pexl_Index block_search (SymbolTable *st, const string *name, size_t len) {
  if ((len == 0) || (!name)) return 0;

  const string *blockptr = StringBuffer_ptr(st->block);
  size_t blocklen = StringBuffer_len(st->block);

  const string *prior = blockptr;
  const string *pos = blockptr;
  const string *end = blockptr + blocklen;
  int stat;

  // Find each NUL terminator, and then check to see if the 'len'
  // bytes before it match 'name'.
  while (pos++ < end) {
    pos = memchr(pos, '\0', end - pos);
    if (!pos) return PEXL_NOT_FOUND;
    if (((ssize_t) (pos - prior)) < (ssize_t) len) {
      prior = pos;
      continue;
    }
    stat = memcmp(pos - len, name, len);
    if (stat == 0) return (pos - blockptr - len);
    prior = pos;
  }
  return PEXL_NOT_FOUND;
}

/* 
   N.B. There is only ONE instance of a given string in the storage
   block.  No duplicates!  This property allows fast string equality
   checking by comparing string 'handles' (offsets into the string
   storage block).
*/
static pexl_Index add_to_block (SymbolTable *st, const string *name, size_t len) {
  // Search block to see if string already exists
  pexl_Index index = block_search(st, name, len);
  if (index >= 0) return index;

  int stat;
  index = StringBuffer_len(st->block);

  if (index >= SYMBOLTABLE_MAXHANDLE)
    return PEXL_ST__ERR_BLOCKFULL;
  
  stat = StringBuffer_add_string(st->block, name);
  if (stat) return PEXL_ERR_INTERNAL;
  stat = StringBuffer_add_char(st->block, '\0');
  if (stat) return PEXL_ERR_INTERNAL;

  // Return the position in the buffer where the string starts
  return index;
}
  
/* See also symboltable_add_data, below */
pexl_Index symboltable_add (SymbolTable *st,
			    const string *name,
			    bool visibility,
			    Symbol_Namespace ns,
			    Symbol_Type type,
			    uint32_t size,
			    pexl_Index value,
			    PatternMetaData *meta) {
  if (!st) {
    warn("symboltable", "null symbol table arg");
    return PEXL_ERR_NULL;
  }
  size_t len = name ? strnlen(name, SYMBOLTABLE_MAXLEN + 1) : 0;
  if (len > SYMBOLTABLE_MAXLEN) {
    warn("symboltable_add", "symbol name exceeds max length of %zu",
	 SYMBOLTABLE_MAXLEN);
    return PEXL_ST__ERR_STRINGLEN;
  }

  pexl_Index index, handle;
  SymbolTableEntry *entry;

  if ((handle = add_to_block(st, name, len)) < 0)
    return handle;		// Error

  // A string handle must fit into an unsigned 24-bit field
  assert(handle < SYMBOLTABLE_MAXHANDLE);

  index = Table_add(st->entries, (void *)&entry);
  if (index < 0) return index;	// Error
  assert(entry);
  entry->handle = handle;
  entry->visibility = visibility;
  entry->namespace = ns;
  entry->type = type;
  entry->size = size;
  entry->value = value;
  if (meta) entry->meta = *meta;
  else memset((void *) &(entry->meta), 0, sizeof(PatternMetaData));
  return index;
}

SymbolTableEntry *symboltable_get (SymbolTable *st, pexl_Index index) {
  if (!st) {
    warn("symboltable", "null symbol table arg");
    return NULL;
  }
  return Table_get(st->entries, index);
}

const string *symboltable_entry_name (SymbolTable *st, SymbolTableEntry *entry) {
  if (!st || !entry) {
    warn("symboltable", "null argument");
    return NULL;
  }
  pexl_Index offset = entry->handle;
  if ((offset < 0) || (((size_t) offset) >= StringBuffer_len(st->block))) {
    warn("symboltable", "offset %" pexl_Index_FMT " out of range 0..%zu",
	 offset, StringBuffer_len(st->block) - 1);
    return NULL;
  }
  return StringBuffer_ptr(st->block) + entry->handle;
}

const string *symboltable_get_name (SymbolTable *st, pexl_Index index) {
  if (!st) {
    warn("symboltable", "null symbol table arg");
    return NULL;
  }
  SymbolTableEntry *entry = Table_get(st->entries, index);
  if (!entry) {
    warn("symboltable", "index %d out of range 0..%zu",
	 index, Table_size(st->entries) - 1);
    return NULL;
  }
  return symboltable_entry_name(st, entry);
}

// Start iterating by calling with *prev == PEXL_ITER_START
SymbolTableEntry *symboltable_iter (SymbolTable *st, pexl_Index *prev) {
  if (!st || !prev) {
    warn("symboltable", "null required arg");
    return NULL;
  }
  return Table_iter(st->entries, prev);
}

SymbolTableEntry *symboltable_iter_ns (SymbolTable *st, Symbol_Namespace ns, pexl_Index *prev) {
  SymbolTableEntry *entry;
  if (!st || !prev) {
    warn("symboltable", "null required arg");
    return NULL;
  }
  while (true) {
    entry = Table_iter(st->entries, prev);
    if (!entry) return NULL;
    if (entry->namespace == ns) return entry;
  }
}


/* Start iterating by calling with prev == PEXL_ITER_START.*/
const string *symboltable_block_iter (SymbolTable *st, pexl_Index *prev) {
  if (!st || !prev) {
    warn("symboltable", "null required arg");
    return NULL;
  }
  size_t blocklen = StringBuffer_len(st->block);
  const string *blockptr = StringBuffer_ptr(st->block);
  if (*prev == PEXL_ITER_START) *prev = 0;
  if ((*prev < 0) || (((size_t) *prev) >= blocklen)) return NULL;
  *prev += 1 + strnlen(blockptr + *prev, SYMBOLTABLE_MAXLEN + 1);
  if ((*prev < 1) || (((size_t) *prev) >= blocklen)) return NULL;
  return blockptr + *prev;
}

/*
  LINEAR search of symbol table entries.  To start the search, set
  'index' to PEXL_ITER_START (or provide NULL for 'index' if you
  don't need that return value).
*/
SymbolTableEntry *symboltable_search (SymbolTable *st,
				      Symbol_Namespace ns,
				      const string *name_string,
				      pexl_Index *index) {
  const string *str;
  SymbolTableEntry *entry;
  pexl_Index i = (index == NULL) ? PEXL_ITER_START : *index;
  if (!st || !name_string) {
    warn("symboltable", "null required arg");
    return NULL;
  }
  while ((entry = symboltable_iter_ns(st, ns, &i))) {
    str = symboltable_entry_name(st, entry);
    if (str && (strncmp(str, name_string, SYMBOLTABLE_MAXLEN + 1) == 0)) {
      if (index) *index = i;
      return entry;
    }
  }
  return NULL;
}

/*
  Data entries in the table can be shared.  If the name and
  attributes are the same, we can reuse an existing entry.
*/
pexl_Index symboltable_add_data (SymbolTable *st,
				 const string *name,
				 bool visibility,
				 Symbol_Type type,
				 uint32_t size,
				 pexl_Index value) {
  SymbolTableEntry *entry;
  pexl_Index idx = PEXL_ITER_START;
  while (true) {
    entry = symboltable_search(st, symbol_ns_data, name, &idx);
    if (!entry)
      return symboltable_add(st, name, visibility, symbol_ns_data, type, size, value, NULL);
    // TODO: Check the other attributes of the returned 'entry'?
    if (entry->type == type) return idx;
  }
}

