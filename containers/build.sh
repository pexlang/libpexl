#!/bin/bash
#
# Build a container (image) with everything needed to compile and test
# our codebase.  The build and test operations are in the bash script
# scripts/build-and-test.sh, which should be the ENTRYPOINT declared
# in the container definition.

dockerfile=$1
if [ "$dockerfile" == "" ]; then
    echo "Usage $0 <name-of-dockerfile> [fresh]"
    exit -1
fi

name=`basename $dockerfile`
cachearg=''
if [ "$2" == "fresh" ]; then
    echo 'Building a fresh image using --no-cache'
    cachearg='--no-cache'
fi

# Trying to fix clock skew error reported by 'make'
datetime=$(date +'%Y-%m-%dT%H:%M:%S')
podman machine ssh sudo date --set $datetime

# Build the image

cd ..

podman build --security-opt label=disable \
       --progress plain \
       $cachearg \
       -t pexl:$name \
       -f "containers/$dockerfile" . 
